//
//  FirstLaunchViewController.swift
//  OONA
//
//  Created by Jack on 10/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import AVFoundation
import Foundation
import CoreMotion
import RxSwift
import GhostTypewriter
import SwiftyGif
import SwiftyJSON
class FirstLaunchViewController: BaseViewController {
    var playerBag = DisposeBag()
    var bumperFinished = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initPlayerView()
        
//        self.checkOriantation()
        
        
        self.bumperFinished = UserDefaults.standard.bool(forKey: "first-bumper-finished")
        
        self.checkRegion()
        
        self.playSplash()
        
        DeviceHelper.shared.inviteCodeEntered
            .subscribe(onNext: { [weak self] (_) in
            self?.registerDevice()
        }).disposed(by: self.disposeBag)
    }
    func checkRegion() {
        
        if ( (UserDefaults.standard.object(forKey: "user-region") != nil) && (UserDefaults.standard.object(forKey: "user-apiUrl") != nil) ){
            OONAApi.shared.setDomain(url: UserDefaults.standard.string(forKey: "user-apiUrl")! , region: UserDefaults.standard.string(forKey: "user-region")!)
            UserHelper.shared.getSavedUserInfo(success: { userProfile in
            }, failure: { error,message  in })
         self.registerDevice()
        }else{
            OONAApi.shared.getRequest(url: APPURL.UserRegion, parameter: [:])
                .responseJSON { (response) in
                    print("[regionCheck]",response.value)
                    switch response.result {
                        
                    case .success(let value):
                        let json = JSON(value)
                        if let code = json["code"].int,
                            let status = json["status"].string {
                            UserHelper.shared.getSavedUserInfo(success: { userProfile in
                                    }, failure: { error,message  in })
                            APIResponseCode.checkResponse(withCode: code.description,
                                                          withStatus: status,
                                                          completion: { (message, isSuccess) in
                                                            if(isSuccess){
                                                                
                                                                if let userRegion = json["userRegion"].string,
                                                                    let apiDomain = json["apiDomain"].string {
                                                                print ("check region",apiDomain,userRegion)
                                                                
                                                                OONAApi.shared.setDomain(url: apiDomain, region: userRegion)
                                                                UserDefaults.standard.set(userRegion, forKey: "user-region")
                                                                UserDefaults.standard.set(apiDomain, forKey: "user-apiUrl")
                                                                
                                                                self.registerDevice()
                                                                   
                                                                }else{
                                                                    self.checkRegion()
                                                                }
                                                            } else {
                                                                print ("region error",code.description)
                                                                if (code == 1008) {
                                                                    self.showInviteCodeEnterVC()
                                                                }
                                                                print("Failure on check response with message: \(message ?? "unknown error")")
                                                            }

                            })
                        }
                        
                    case .failure(let error):
                        print("region check network error : \n",error)
                        
                    }
                    
            }
        }
    }
    func checkOriantation() {
        print("current oriantation",UIDevice.current.orientation.rawValue)
        if UIDevice.current.orientation.isLandscape{
            self.rotateImg.isHidden = true
            self.playSplash()
            print("landscape")
        }else if UIDevice.current.orientation.isPortrait{
            print("portrait")
            self.initializeMotionManager()
            self.rotateImg.isHidden = false
        }else if UIDevice.current.orientation == .faceUp{
            print("face up")
            self.initializeMotionManager()
            self.rotateImg.isHidden = false
        }else if UIDevice.current.orientation == .faceDown{
            print("face up")
            self.initializeMotionManager()
            self.rotateImg.isHidden = false
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.checkOriantation()
            }
        }
    }
    var orientationLast = UIInterfaceOrientation(rawValue: 0)!
    var motionManager: CMMotionManager?
    var player = AVPlayer()
    var playerItem : AVPlayerItem = .init(url: URL.init(string: "a")!)
    var playerLayer = AVPlayerLayer()
    
    @IBOutlet weak var rotateImg: UIImageView!
    
    func outputAccelertionData(_ acceleration: CMAcceleration) {
        var orientationNew: UIInterfaceOrientation
        if acceleration.x >= 0.75 {
            orientationNew = .landscapeLeft
            print("landscapeLeft")
            viewDidEnterLandscape()
            
        }
        else if acceleration.x <= -0.75 {
            orientationNew = .landscapeRight
            print("landscapeRight")
            viewDidEnterLandscape()
        }
        else if acceleration.y <= -0.75 {
            orientationNew = .portrait
            print("portrait")
            
        }
        else if acceleration.y >= 0.75 {
            orientationNew = .portraitUpsideDown
            print("portraitUpsideDown")
        }
        else {
            // Consider same as last time
            return
        }
        
        if orientationNew == orientationLast {
            return
        }
        orientationLast = orientationNew
    }
    func viewDidEnterLandscape() {
        motionManager?.stopDeviceMotionUpdates()
        DispatchQueue.once(token: "first-launch") {
            rotateImg.isHidden = true
            self.playSplash()
        }
        
    }
    
    func initializeMotionManager() {
        motionManager = CMMotionManager()
        motionManager?.accelerometerUpdateInterval = 0.2
        motionManager?.gyroUpdateInterval = 0.2
        motionManager?.startAccelerometerUpdates(to: (OperationQueue.current)!, withHandler: {
            (accelerometerData, error) -> Void in
            if error == nil {
                self.outputAccelertionData((accelerometerData?.acceleration)!)
            }
            else {
                print("\(error!)")
            }
        })
    }
    
    public func initPlayerView() {
        
        
        //        initBcoPlayerView()
        player = AVPlayer.init(playerItem: playerItem)
        playerLayer = AVPlayerLayer (player: player)
        playerLayer.videoGravity = .resizeAspect
        //        playerLayer.backgroundColor = UIColor.gray.cgColor
        playerLayer.frame = self.view.bounds
        playerLayer.zPosition = -1
        
        
        self.view.layer.addSublayer(playerLayer)
        
        
    }
    
    var splashDoneObservation: NSKeyValueObservation?
    
    func playSplash() {
        if !self.isSplashDone {
            playerBag = DisposeBag()
            let path = Bundle.main.path(forResource: "splash-black", ofType:"mp4")
            
            //        player.replaceCurrentItem(with: AVPlayerItem.init(asset: AVAsset.))
            player.replaceCurrentItem(with: AVPlayerItem.init(url: URL(fileURLWithPath: path!)))
            player.rate = 1.5
            player.play()
            
            NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification).skip(1).subscribe(onNext: { [weak self] (_) in
                /// put skip(1) in the statements due this function ealier than first didBecomeActiveNotification event
                if let checkedSelf = self, !checkedSelf.isSplashDone {
                    if let _ = checkedSelf.player.currentItem {
                        checkedSelf.player.play()
                    } else {
                        checkedSelf.playSplash()
                    }
                }
            }).disposed(by: playerBag)
            
            let viewModel = PlayerViewModel.init(player: player)
            viewModel.didPlayToEnd
                .drive(onNext: { [unowned self] _ in
                    self.splashDone()
                    //                let storyBoard: UIStoryboard = UIStoryboard(name: "FirstLaunch", bundle: nil)
                    //                let newViewController : UIViewController = storyBoard.instantiateViewController(withIdentifier: "AfterSplashViewController")
                    //                newViewController.modalTransitionStyle = .crossDissolve
                    //                self.present(newViewController, animated: true, completion: nil)
                    //                NSLog("item did play to end")
                    //                self.backToStart()
                })
                .disposed(by: playerBag)
        }
        
    }
    
    var isSplashDone = false
    var isRegDone = false
    func splashDone() {
        print(#function)
        isSplashDone = true
        playerBag = DisposeBag()
        player.replaceCurrentItem(with: nil)
        twoStepCheck()
    }
    
    func regDeviceDone() {
        isRegDone = true
        twoStepCheck()
    }
    
    func twoStepCheck() {
        if (isSplashDone && isRegDone) {
           twoStepFinish()
        }
    }
    
    func twoStepFinish() {
        AnalyticHelper.shared.appOpen()
//        if (!self.bumperFinished) {
//
//            AnalyticHelper.shared.firstLaunch()
//
//            UserDefaults.standard.set(true, forKey: "first-bumper-finished")
//            let path = Bundle.main.path(forResource: "firsttime-bumper", ofType:"mp4")
//            //        player.replaceCurrentItem(with: AVPlayerItem.init(asset: AVAsset.))
//            player.replaceCurrentItem(with: AVPlayerItem.init(url: URL(fileURLWithPath: path!)))
//            player.rate = 1.5
//            player.play()
//
//            NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification).subscribe(onNext: { [weak self] (_) in
//                if let checkedSelf = self, !checkedSelf.bumperFinished {
//                    if let _ = checkedSelf.player.currentItem,
//                        checkedSelf.player.status == .readyToPlay {
//                        checkedSelf.player.play()
//                    }
//                }
//            }).disposed(by: playerBag)
//
//            let viewModel = PlayerViewModel.init(player: player)
//            viewModel.didPlayToEnd
//                .drive(onNext: { [unowned self] _ in
//                    self.bumperFinished = true
//                    self.player.replaceCurrentItem(with: nil)
//                    self.goToMain()
//                    //                let storyBoard: UIStoryboard = UIStoryboard(name: "FirstLaunch", bundle: nil)
//                    //                let newViewController : UIViewController = storyBoard.instantiateViewController(withIdentifier: "AfterSplashViewController")
//                    //                newViewController.modalTransitionStyle = .crossDissolve
//                    //                self.present(newViewController, animated: true, completion: nil)
//                    //                NSLog("item did play to end")
//                    //                self.backToStart()
//                })
//                .disposed(by: playerBag)
//
//
//        }else{
        
            self.goToMain()
//        }
    }
    
    func firstTimeBumper() {
        
    }
    func goToMain() {
        playerBag = DisposeBag()
        let storyBoard: UIStoryboard = UIStoryboard(name: "PlayerViewController", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        self.present(newViewController, animated: false, completion: nil)
    }
    func registerDevice() {
        DispatchQueue.global().async {
            OONAFirebase.shared.fetchConfig(complete: { (result, error) in
                //after calling firebasehelper , call register device
                
                if let _result = result{
                    DeviceHelper.shared.registerDevice(regSecret: _result, success: { response in
                        //handle notification in ui
                        /* production version : TOTEST_OONA
                         if let notice : NSDictionary = response["notice"] as? NSDictionary{
                         DispatchQueue.main.async {
                         let imageUrl : String = notice["imageUrl"] as! String
                         let action : String = notice["action"] as! String
                         self.showNotification(url: URL(string: imageUrl) , action:URL(string: action))
                         }
                         }*/
                        
                        //for testing : url
                        let url : String = "https://www.oonatv.com/assets/img/freechannels.png"
                        let action : String = "https://www.oonatv.com"
                        let type = NotificationDisplayType.Image
                        
                        //for testing : webview
                        //let url : String = "https://www.oonatv.com"
                        //let action : String = "https://www.oonatv.com"
                        //let type = NotificationDisplayType.Webview
                        
                        //self.showNotification(url: URL(string: url) , action:URL(string: action), type: type)
                        //self.showMenu()
                        self.regDeviceDone()
                    }, failure: { error in
                        self.registerDevice()
                    })
                }
                else if error != nil{
                    //TODO_OONA : error handling for cannot remote config from firebase.
                }
            })
            //            DispatchQueue.main.sync {
            //                self.playVideo(url: "https://i.imgur.com/9rGrj10.mp4")
            //            }
        }
        
        
        
    }
}

class AfterSplashViewController: BaseViewController {
    
    @IBOutlet weak var writingText: TypewriterLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        writingText.typingTimeInterval = 0.03
        writingText.startTypewritingAnimation {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                let storyBoard: UIStoryboard = UIStoryboard(name: "FirstLaunch", bundle: nil)
                let newViewController : UIViewController = storyBoard.instantiateViewController(withIdentifier: "FullScreenBotViewController")
                newViewController.modalTransitionStyle = .crossDissolve
                self.present(newViewController, animated: true, completion: nil)
            })
       
        }
        
    }
}

class FullScreenBotViewController:BaseViewController ,SwiftyGifDelegate{
    
    @IBOutlet weak var botChatLabel: TypewriterLabel!
    @IBOutlet weak var gestureImage: UIImageView!
    @IBOutlet weak var gestureImageHeight: NSLayoutConstraint!
    
    var doneFirstStep : Bool = false
    var doneSecondStep : Bool = false
    var doneThirdStep : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        botChatLabel.typingTimeInterval = 0.05
        botChatLabel.cancelTypewritingAnimation()
        botChatLabel.text = "Hi nice to meet you, \nI’m OONAbot"
        botChatLabel.startTypewritingAnimation {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.goToSecondStep()
            })
            
            
        }
    }
    
    
    func goToSecondStep() {
        if doneFirstStep {
            return
        }
        print("goToSecondStep")
        doneFirstStep = true
        botChatLabel.cancelTypewritingAnimation()
        botChatLabel.text = "I’m your assistant in OONA TV"
        botChatLabel.startTypewritingAnimation {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.goToThirdStep()
            })
            
        }

    }
    func goToThirdStep() {
        if doneSecondStep {
            return
        }
        
        print("goToThirdStep")
        
        doneSecondStep = true
        let attributedString = NSMutableAttributedString(string: "It’s easy to navigate in OONA TV\nTap & Swipe", attributes: [
            .font: UIFont(name: "Montserrat-Regular", size: 20.0)!,
            .foregroundColor: UIColor(white: 1.0, alpha: 1.0),
            .kern: 0.0
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "Montserrat-SemiBold", size: 24)!, range: NSRange(location: 33, length: 11))
        botChatLabel.attributedText = attributedString
        botChatLabel.cancelTypewritingAnimation()
        botChatLabel.startTypewritingAnimation {
          
            
        }
        do {
            let gif = try UIImage(gifName: "Gestures")
            gestureImage.setGifImage(gif)
            gestureImage.loopCount = 1
            gestureImage.delegate = self

        } catch  {
            print(error)
        }
        UIView.animate(withDuration: 0.3) {
            self.gestureImageHeight.constant = 100
            self.view.layoutIfNeeded()
        }
    }
    func gifURLDidFinish(sender: UIImageView) {
        print("gifURLDidFinish")
    }
    
    func gifURLDidFail(sender: UIImageView) {
        print("gifURLDidFail")
    }
    
    func gifDidStart(sender: UIImageView) {
        print("gifDidStart")
    }
    
    func gifDidLoop(sender: UIImageView) {
        print("gifDidLoop")
        
    }
    
    func gifDidStop(sender: UIImageView) {
        print("gifDidStop")
//        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.finish()
//        }
        
    }
    func finish() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
        let newViewController : UIViewController = storyBoard.instantiateInitialViewController()!
        newViewController.modalTransitionStyle = .crossDissolve
        self.present(newViewController, animated: true, completion: nil)
    }
}
