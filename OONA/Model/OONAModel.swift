//
//  OONAModel.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OONAModel {
    init?(json: JSON)
}

extension JSON {
    
    func oonaModelArray<T: OONAModel>() -> [T] {
        guard let array = array else { return [T]() }
        return array.compactMap {
            T(json: $0)
        }
    }
    
    func oonaModelArray<T: OONAModel>(action: ((T) -> T)?) -> [T] {
        guard let array = array else { return [T]() }
        guard let _ = action else { return oonaModelArray() }
        let mappedArray = array.compactMap { (json) -> T? in
            guard let item = T(json: json) else { return nil }
            if let editedItem = action?(item) { return editedItem }
            else { return item }
        }
        return mappedArray
    }
    
    // Convert JSON to any LalaModel
    func oonaModel<T: OONAModel>() -> T? {
        return T(json: self)
    }
}
