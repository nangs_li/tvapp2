//
//  OONAChannel.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

class OONAChannel: OONAMedia, Equatable {
    var id : Int = 0
    var isFavourite : Bool = false
    var imageUrl : String = ""
    var description : String = ""
    var url : String = ""
    var name : String = ""
    var isYoutube : Bool?
    var streamingUrl : String?
    var brightcoveId : String?
    var video : Video?
    var seasons : [String] = []
    
    required init?(json: JSON) {
        super.init(json: json)
    }
    
    static func == (lhs: OONAChannel, rhs: OONAChannel) -> Bool { return lhs.id == rhs.id }
}
