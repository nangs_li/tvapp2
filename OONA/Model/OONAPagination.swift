//
//  OONAPagination.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

enum ChunkSizePerPage: Int {
    case TwentyPerPage = 20
    case FourtyPerPage = 40
    case SixtyPerPage = 60
}

enum OONAFetchMode {
    case refresh
    case nextPage
    
}

class Pagination<T>: NSObject {
    /// A list of current episodes.
    var itemList: [T] = []
    
    /// The starting location of episodes should request.
    private var offset: Int { return itemList.count }
    
    /// The number of episodes per request.
    var chunkSize: ChunkSizePerPage = .TwentyPerPage
    
    /// The range of episodes to be requested for next request.
    var nextRequestRange: NSRange { return NSRange(location: offset, length: chunkSize.rawValue) }
    
    var hasNextPage: Bool = true
    
    override init() {
        super.init()
        self.itemList = []
    }
}
