//
//  OONABaseModel.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

class OONABaseModel: OONAModel {
    let status: String?
    let code: Int?
    required init?(json: JSON) {
        if let status = json["status"].string,
            let code = json["code"].int {
            self.status = status
            self.code = code
        } else { return nil }
    }
    
}
