//
//  OONAMedia.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

//TODO:
enum MediaType: String {
    case unknown
    case live
    case episode
    case linear
}

class OONAMedia: OONAModel {
    private(set) var type: String?
    private(set) var mediaType: MediaType = .unknown
    
    required init?(json: JSON) {
        if let _type = json["type"].string {
            self.type = _type
            self.mediaType = MediaType(rawValue: _type)!
        }
    }
    
    private func mapMediaType(_ typeString: String) -> MediaType {
        switch typeString {
        case "live": return .live
        case "episode": return .episode
        case "linear": return .linear
        default: return .unknown
        }
    }
    
    func createMedia(json: JSON) -> OONAMedia? {
        switch mediaType {
        case .live: return OONAChannel(json: json)
        case .episode: return OONAEpisode(json: json)
        case .linear: return OONAChannel(json: json)
        default:
            return OONAMedia(json: JSON())
        }
    }
}
