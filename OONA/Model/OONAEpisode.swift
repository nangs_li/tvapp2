//
//  OONAEpisode.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

class OONAEpisode: OONAMedia, Equatable {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var length: String = ""
    var no: String = ""
    var isFavourite: Bool = false
    var imageUrl: String = ""
    var url: String = ""
    var isYoutube: Bool = false
    var video: OONAVideo?
    
    required init?(json: JSON) {
        super.init(json: json)
    }
    
    static func == (lhs: OONAEpisode, rhs: OONAEpisode) -> Bool { return lhs.id == rhs.id }
}
