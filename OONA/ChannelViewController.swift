//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources

class ChannelViewController: BaseViewController{
    
    struct ChannelTask {
        var id :String
    }
    typealias channelSelectedBlock = (_ theId :Int) ->()
    var completionBlock:channelSelectedBlock?
    let disposeBag = DisposeBag()
    var channelList = [Channel]()
    
    @IBAction func iconClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var cv: UICollectionView! {
        didSet {
            
            self.cv.rx.itemSelected.subscribe(onNext: { indexPath in
                guard let cb = self.completionBlock else {return}
                let channel = self.channelList[indexPath.row]
                cb(channel.id)
            }).disposed(by: self.disposeBag)
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelHelper.shared.getChannelList(channelId: nil, sortBy: nil, success: { response in
            let channels = response
            self.channelList = channels
            self.cv.register(UINib(nibName: "ChannelCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCell")

            
            /*
            let dataSource = RxCollectionViewSectionedReloadDataSource
                <SectionModel<String, Channel>>(
                    configureCell: { (dataSource, cv, indexPath, element) in
                        let cell = cv.dequeueReusableCell(withReuseIdentifier: "ChannelCell",
                                                          for: indexPath) as! ChannelCell
                        cell.configure(with: element)
                        return cell
                }
            )*/
            
//            self.cv.rx.setDelegate(self).disposed(by: self.disposeBag)
            
            Observable.just(channels)
                .subscribeOn(MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .bind(to: self.cv.rx.items){ (collectionView, row, element) in
                    
                    let indexPath = IndexPath(row: row, section: 0)
                    let cell = self.cv.dequeueReusableCell(withReuseIdentifier: "ChannelCell",
                                                      for: indexPath) as! ChannelCell
                    cell.configure(with: element)

                    return cell
                    
                }
                .disposed(by: self.disposeBag)
        

            
        }, failure: { error in })
    }
    
}
