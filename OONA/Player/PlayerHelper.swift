//
//  PlayerHelper.swift
//  OONA
//
//  Created by Jack on 14/6/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
import RxSwift
import RxCocoa

class PlayerHelper: NSObject {
    static let shared = PlayerHelper()
    
    var playVideo: PublishRelay<Video?> = PublishRelay<Video?>()
    var resumeVideo: PublishRelay<Bool> = PublishRelay<Bool>()
    var showBidPage: PublishRelay<Bool> = PublishRelay<Bool>()
    
    var presentVC: PublishRelay<UIViewController> = PublishRelay<UIViewController>()
    var showChatbot: PublishRelay<Question> = PublishRelay<Question>()
    var showXplore: PublishRelay<Bool> = PublishRelay<Bool>()
    var showGames: PublishRelay<Bool> = PublishRelay<Bool>()
    var showTcoinHome: PublishRelay<Bool> = PublishRelay<Bool>()
    var exitTVMenu : PublishRelay<Bool> = PublishRelay<Bool>()
    var showRewardVideo: PublishRelay<RewardVideo> = PublishRelay<RewardVideo>()
    var showTutorialVideo: PublishRelay<Tutorial> = PublishRelay<Tutorial>()
    var playTutorial: PublishRelay<String> = PublishRelay<String>()
    var showSystemMessage: PublishRelay<String> = PublishRelay<String>()
    var showHTMLSystemMessage: PublishRelay<String> = PublishRelay<String>()
    var resetYoutubeView: PublishRelay<Bool> = PublishRelay<Bool>()
    
    var showGameAd: PublishRelay<Bool> = PublishRelay<Bool>()
    var hideGameAd: PublishRelay<Bool> = PublishRelay<Bool>()
    var startGameAdTimer : PublishRelay<Bool> = PublishRelay<Bool>()
    var pauseGameAdTimer : PublishRelay<Bool> = PublishRelay<Bool>()
//    var resetYoutubeView: PublishRelay<Bool> = PublishRelay<Bool>()
    
    public var currentVideo : Video?
    public var currentDuration : Int?
    var requestSnap: PublishRelay<Bool?> = PublishRelay<Bool?>()
    var receivedSnap: PublishRelay<UIImage?> = PublishRelay<UIImage?>()
    
    var brightcoveAccountId : String = NativeControlsConstants.AccountID
    var brightcovePolicyKey : String = NativeControlsConstants.PlaybackServicePolicyKey
    
    var videoControls: BehaviorRelay<[VideoControl]> = BehaviorRelay<[VideoControl]>(value: [.Default])
    
    public func setBrightcove(accountId : String, policyKey : String) {
        brightcoveAccountId = accountId
        brightcovePolicyKey = policyKey
        
    }
    
    /*
    func saveVideo(video:Video){
    
        var userDefaults = UserDefaults.standard
        //let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: video)
        userDefaults.set(video.streamingUrl, forKey: "video-streamingUrl")
        //userDefaults.set(video.videoAdsJson, forKey: "video-videoAds")
        //userDefaults.set(video.prerollAds, forKey: "video-prerollAds")
        //userDefaults.set(video.midrollAds, forKey: "video-midrollAds")
        userDefaults.set(video.channelName, forKey: "video-channelName")
        userDefaults.set(video.imageUrl, forKey: "video-imageUrl")
        
        userDefaults.synchronize()
    
    }
    
    func getVideo()->Video?{
        if let _currentVideo = currentVideo{
            return _currentVideo
        }else{
            if let streamingUrl  = UserDefaults.standard.string(forKey: "video-streamingUrl"), let imageUrl = UserDefaults.standard.string(forKey: "video-imageUrl"), let channelName = UserDefaults.standard.string(forKey: "video-channelName"){
                if let video : Video = Video.init(streamingUrl : streamingUrl ,channelName : channelName, imageUrl : imageUrl){
                    return video
                }else{
                    return  nil
                }
            }else{
                return nil
            }
        }
    
    }*/

    func getVideo() -> Video?{
        return currentVideo
    }
    public func presentVC(vc: UIViewController) {
        //saveVideo(video:video)
        
        //        getStreamingUrl(video:video)
        presentVC.accept(vc)
    }
    public func showChatbotView() {
//        showChatbot.accept(.TcoinTutorial)
    }
    public func resetYTView() {
        print("PH reset")
        self.resetYoutubeView.accept(true)
    }
    public func resumePlayer() {
        self.resumeVideo.accept(true)
    }
    
    public func pausePlayer() {
        self.resumeVideo.accept(false)
    }
    
    public func playVideo(video:Video) {
        //saveVideo(video:video)
        self.currentVideo = video
//        getStreamingUrl(video:video)
        self.playVideo.accept(video)
    }
    
    func getStreamingUrl(video:Video) -> Bool{
        OONAApi.shared.getRequest(url: APPURL.getStreamingUrl(brightcoveId: video.brightcoveId ?? "" ), parameter: [:]).responseJSON{ (response) in
            print("st url ",response)
            
        }
        return true
    }
    
}

