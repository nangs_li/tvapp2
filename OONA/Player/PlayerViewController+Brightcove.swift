//
//  PlayerViewController+Brightcove.swift
//  OONA
//
//  Created by Jack on 2/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//


import Foundation
import BrightcovePlayerSDK
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture
import AVKit

extension PlayerViewController {
    
    func playbackController(_ controller: BCOVPlaybackController!, didAdvanceTo session: BCOVPlaybackSession!) {
        print("ViewController Debug - Advanced to new session.")
//        playerLayer.player  = session.player
        session.player.currentItem!.preferredForwardBufferDuration = 5;
        playerLayer.removeFromSuperlayer()
        self.player.replaceCurrentItem(with: nil)
        self.player.cancelPendingPrerolls()
        self.player = session.player
        self.player.currentItem?.canUseNetworkResourcesForLiveStreamingWhilePaused = false;
        playerLayer = AVPlayerLayer (player: self.player)
        playerLayer.videoGravity = .resizeAspect
        //        playerLayer.backgroundColor = UIColor.gray.cgColor
        playerLayer.frame = self.view.bounds
        playerLayer.zPosition = -1
        
        
        self.view.layer.addSublayer(playerLayer)
 
        viewModel = PlayerViewModel(player: playerLayer.player!)
        bindPlayer()
        self.bcoAdvanceTo()
        if isFirstVideo {
            print("did adv to firstvideo pause")
            self.pauseVideo()
            self.player.pause()
            isFirstVideo = false
        }
    }
   
    func playbackController(_ controller: BCOVPlaybackController!, playbackSession session: BCOVPlaybackSession!, didProgressTo progress: TimeInterval) {
//        print("ViewController Debug - didProgressTo.2",progress.description)
        if let currentItem = self.player.currentItem,
            currentItem.isPlaybackLikelyToKeepUp {
            self.view.cancelLoading()
        }
    }

    public func initBcoPlayerView() {
        
        fairplayProxy = BCOVFPSBrightcoveAuthProxy.init(publisherId: nil, applicationId: nil)
        let basicSessionProvider :BCOVBasicSessionProvider = manager.createBasicSessionProvider(with: nil) as! BCOVBasicSessionProvider
        
        let fairplaySessionProvider = manager.createFairPlaySessionProvider(withApplicationCertificate: nil, authorizationProxy: fairplayProxy!, upstreamSessionProvider: basicSessionProvider)
        playbackController = manager.createPlaybackController(with: fairplaySessionProvider, viewStrategy: nil)
        playbackController?.delegate = self
        playbackController?.isAutoAdvance = true
        playbackController?.isAutoPlay = false
        
        // Prevents the Brightcove SDK from making an unnecessary AVPlayerLayer
        // since the AVPlayerViewController already makes one
        playbackController?.options = [ kBCOVAVPlayerViewControllerCompatibilityKey : true ]
        
        playbackService = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        
//        self.playbackController?.setVideos([BCOVVideo.init(hlsSourceURL: URL.init(string: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8")!)] as NSFastEnumeration)
//        self.playbackController?.play()
        self.playbackController?.delegate = self
        
    }
    public func playVideo(brightcoveId:String) {
        //        playerLayer.player = AVPlayer (url: URL.init(string: url)!)
        //        playerItem =  AVPlayerItem(url: URL.init(string: url)!)
        //        player.replaceCurrentItem(with:playerItem)
        
        let service = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        service?.findVideo(withVideoID: brightcoveId, parameters: [:], completion: { (video, jsonResponse, error) in
            //            video.
            if let _video = video {
                print(#function)
                self.playbackController?.setVideos([_video] as NSFastEnumeration)
                
                self.playbackController?.play()
                self.playbackController?.delegate = self
                if !self.isFirstVideo {
                    self.view.startLoading(above: self.youtubeView)
                }
//                if self.isFirstVideo {
//                    print("bco firstvideo pause")
//                    self.pauseVideo()
//                    self.isFirstVideo = false
//                }
                
            }
        })
        //        playerLayer.player?.play()
        
        //        self.bcoView?.delegate = self
        
        
    }
}
