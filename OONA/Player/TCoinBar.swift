//
//  TCoinBar.swift
//  OONA
//
//  Created by nicholas on 7/15/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

enum TCoinTabBarDisplayState {
    case none
    case displayForControlView
    case prepareTDisplayTabBar
    case displayForTabBar
    case displayForTabDetail
}

class TCoinBar: UIView {
    var currentBarState: TCoinTabBarDisplayState { get { return barState} }
    private var barState: TCoinTabBarDisplayState = .none
    @IBOutlet weak private var tcoinBar: UIImageView!
    @IBOutlet weak private var tcoinTab: UIImageView!
    @IBOutlet weak private var tcoinLabel: UILabel!
    @IBOutlet weak private var tcoinTabForControlView: UIView!
    @IBOutlet weak private var tcoinLabelForControlView: UILabel!
    
    
    @IBOutlet weak private var tcoinSlimBarTopConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        self.hideTabBar()
    }
    public func updateTcoinAmount(amount:Int ) {
        tcoinLabelForControlView.text = UtilityHelper.shared.formateredNumber(with: amount)
    }
    
    func hideTabBar() {
        print("Hide tcoin tab bar")
        tcoinSlimBarTopConstraint.constant = 0
        tcoinTabForControlView.isHidden = true
        tcoinBar.isHidden = true
        tcoinTab.isHidden = true
        tcoinLabel.isHidden = true
        tcoinLabel.text = "Slide for more"
        barState = .none
        self.setNeedsLayout()
    }
    
    func showBarForState(barState: TCoinTabBarDisplayState) {
        print("display TCoinBar for state: \(barState)")
        if barState == .displayForControlView {
            // This case handle slim bar need to move down when being used in control view
            tcoinSlimBarTopConstraint.constant = 0
            self.layoutIfNeeded()
            tcoinBar.isHidden = false
            tcoinTab.isHidden = false
            tcoinLabel.isHidden = true
            tcoinTabForControlView.isHidden = false
            
        } else if barState == .prepareTDisplayTabBar {
            // This case handle slim bar need to move up when not being used in control view
            tcoinSlimBarTopConstraint.constant = -5
            self.layoutIfNeeded()
            tcoinBar.isHidden = false
        }else if barState == .displayForTabBar || barState == .displayForTabDetail {
            tcoinBar.isHidden = false
            tcoinTab.isHidden = false
            tcoinTabForControlView.isHidden = true
            tcoinLabel.isHidden = false
            let labelText = barState == .displayForTabBar ? LanguageHelper.localizedString("Slide_for_more") : LanguageHelper.localizedString("Slide_Up_to_close")
            tcoinLabel.text = labelText
        } else {
            hideTabBar()
        }
        self.barState = barState
    }
    
    func showBarForState(barState: TCoinTabBarDisplayState,
                         animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           options: .transitionCrossDissolve,
                           animations: {
                self.showBarForState(barState: barState)
            },
                           completion: { (success: Bool) in
                
            })
        } else {
            self.showBarForState(barState: barState)
        }
    }
    
    
}
