//
//  PlayerViewController+Ads.swift
//  OONA
//
//  Created by Jack on 15/10/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture

extension PlayerViewController {
    func setEngagementAdInvisible() {
          self.engagementImage.alpha = 0
          self.engagementView.alpha = 0
          self.engagementSpinGif.alpha = 0
          self.engagementAdButton.alpha = 0
      }
      func setEngagementAdVisible() {
          self.engagementImage.alpha = 1
          self.engagementView.alpha = 1
          self.engagementSpinGif.alpha = 1
          self.engagementAdButton.alpha = 1
      }
     func showGameAd() {
            
            self.gameAdButton.isHidden = false
            self.gameAdView.isHidden  = false
            self.gameAdContainer.isHidden  = false
    //        self.engagementSpinGif.isHidden  = false
            UIView.animate(withDuration: 0.3, animations: {
                self.gameAdButtonLeading.constant = 0
                self.view.layoutIfNeeded()
            }) { (completed) in
                if GameHelper.shared.userFirstPlay {
                    GameHelper.shared.showGameAdTutorial.accept(true)
                }
            }
             AnalyticHelper.shared.gameAdView(game: GameHelper.shared.currentGame!)
        }
        func hideGameAd() {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.gameAdButtonLeading.constant = -200
                self.view.layoutIfNeeded()
                
            }) { (completed) in
                self.gameAdButton.isHidden = true
                self.gameAdView.isHidden  = true
            }
    //         AnalyticHelper.shared.gameAdView(game: GameHelper.shared.currentGame)
        }
        func showEngagementAd() {
           
            self.engagementAdButton.isHidden = false
            self.engagementImage.isHidden  = false
            self.engagementSpinGif.isHidden  = false
            UIView.animate(withDuration: 0.3, animations: {
                self.adButtonTrailing.constant = 0
                self.view.layoutIfNeeded()
            }) { (completed) in
                
            }
            AnalyticHelper.shared.displayAdView(video: currentVideo!, videoTime: self.getCurrentDuration())
        }
        func hideEngagementAd() {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.adButtonTrailing.constant = 1000
                self.view.layoutIfNeeded()
                
            }) { (completed) in
                self.engagementAdButton.isHidden = true
                self.engagementImage.isHidden  = true
                self.engagementSpinGif.isHidden  = true
                
            }
            
        }
    func slideEngagementButton() {
        if (!self.engagementStarted) {
            AnalyticHelper.shared.displayAdClick(video: currentVideo!, videoTime: self.getCurrentDuration())
            // should play engagement Ad
            self.engagementStarted = true
            // expand the engagementView
            //                    gestureRecognizer.state = .ended
           
                self.engagementView.isHidden = false
            
            UIView.animate(withDuration: 0.2, animations: {
                self.adButtonTrailing.constant = self.view.frame.size.width * -1 + self.view.safeAreaInsets.left
                self.resetPanDirection()
                self.view.layoutIfNeeded()
                
            },completion: { _ in
                self.pauseVideo()
                self.isPlayingAd = true
                self.engagementAdTriggered()
            })
        }
    }
    func slideGameAdButton() {
        if (!self.engagementStarted) {
            // should play engagement Ad
            GameHelper.shared.gameAdStart.accept(true)
            AnalyticHelper.shared.gameAdClick(game: GameHelper.shared.currentGame!)
            self.engagementStarted = true
            // expand the engagementView
            //                    gestureRecognizer.state = .ended
            
            self.gameAdContainer.isHidden = false
            
            UIView.animate(withDuration: 0.2, animations: {
                self.gameAdButtonLeading.constant = self.view.frame.size.width - self.view.safeAreaInsets.right
                self.resetPanDirection()
                self.view.layoutIfNeeded()
                
            },completion: { _ in
                self.pauseVideo()
                self.isPlayingAd = true
                self.gameAdTriggered()
            })
        }
    }
    func gameAdTriggered(){
            self.gameAdContainer.startShimmering()
            //        self.player.pause()
            
            adHelperDisposeBag = DisposeBag()
            adHelp = VideoAdHelper()
            adHelp.playerView = self.view
            self.addChild(adHelp)
            
            if GameHelper.shared.userFirstPlay {
                GameHelper.shared.showGameAdTutorial.accept(false)
            }
            
            // Listen on backgroud thread, and return to main thread.
            adHelp.adClose.asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
    //                self.tcoinRewardVideo()
                    self.tcoinRewardVideo(rewardAmount: .Award100)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
                        self.gameAdContainer.isHidden = true
                        print("player close ad")
    //                    self.resumeVideo()
                        self.isPlayingAd = false
                        self.engagementStarted = false
                    })
                    
                    
                    //                self.gettcoinView(addedTcoin: 100, totalTcoin: 28620)
                    
                }).disposed(by: adHelperDisposeBag)
            
            
            
            adHelp.rewardReceived.subscribe(onNext: { [weak self] (reward) in
                
                //            self.playerStartPlaying()
                print("show tcoin view")
                DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                    guard let _reward = reward else { return }
                    
                    self?.gettcoinView(addedTcoin: _reward.points, totalTcoin: _reward.balance)
                })
                
            }).disposed(by: self.disposeBag)
            
            adHelp.adStart.asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
                    print("player start game ad")
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        //                    self.adButtonTrailing.constant = 0
                        self.gameAdButtonLeading.constant = 0
    //                    self.hideEngagementAd()
    //                    self.resetYoutubeViewConstraints()
    //                    self.gameAdContainer.stopShimmering()
                    }
                    
                    
                }).disposed(by: adHelperDisposeBag)
            

            adHelp.triggerGameAd(game: GameHelper.shared.currentGame!)

            
        }
    func engagementAdTriggered(){
            self.engagementView.startShimmering()
    //        self.player.pause()
            
            
            adHelperDisposeBag = DisposeBag()
            adHelp = VideoAdHelper()
            adHelp.playerView = self.view
            self.addChild(adHelp)

            // Listen on backgroud thread, and return to main thread.
            adHelp.adClose.asObservable()
    //            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .ma))
    //            .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
    //                self.tcoinRewardVideo(rewardAmount: .Award100)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.5, execute: {
                        self.engagementView.isHidden = true
                        print("player close ad")
                        self.isPlayingAd = false
                        self.engagementStarted = false
    //                    self.resumeVideo()
                        if self.currentPlayer == .bco { self.player.play() }
                        if self.currentPlayer == .youtube { self.youtubePlayerView.playVideo() }
                        
                        self.engagementAdDismissTimer.resumeTimer()
                        self.engagementAdFirstIntervalTimer.resumeTimer()
                        self.engagementAdAfterwardIntervalTimer.resumeTimer()
                        
                    })
                   
                    
    //                self.gettcoinView(addedTcoin: 100, totalTcoin: 28620)
                    
                }).disposed(by: adHelperDisposeBag)
     
            
            
            adHelp.rewardReceived.subscribe(onNext: { [weak self] (reward) in
                
                //            self.playerStartPlaying()
                print("show tcoin view")
                DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                    guard let _reward = reward else { return }
                    
                    self?.gettcoinView(addedTcoin: _reward.points, totalTcoin: _reward.balance)
                })
              
            }).disposed(by: self.disposeBag)
            
            adHelp.adStart.asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
                    print("player start ad")
                    self.pauseVideo()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        //                    self.adButtonTrailing.constant = 0
                        self.hideEngagementAd()
                        self.resetYoutubeViewConstraints()
                        self.engagementView.stopShimmering()
                    }
            
                    
                }).disposed(by: adHelperDisposeBag)
            
    //        adHelp.triggerDemoAd()
            if (currentVideo != nil) {
                adHelp.triggerAd(video: currentVideo! ,adType: .engagement)

            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.adButtonTrailing.constant = 0


                        self.resetPanDirection()
                        self.view.layoutIfNeeded()
                        self.engagementStarted = false
                        self.engagementView.stopShimmering()
                    },completion: { _ in
    //                    self.gettcoinView(addedTcoin: 100, totalTcoin: 28420)
                    })
                }

            }

        }
        func setupMidrollAds() {
            
        }
         func setupPrerollAds() {
            adHelperDisposeBag = DisposeBag()
            adHelp = VideoAdHelper()
            adHelp.playerView = self.view
            self.addChild(adHelp)
            
            // Listen on backgroud thread, and return to main thread.
            adHelp.adClose.asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
                    print("player close preroll ad")
    //                self.player.play()
                    
                    self.isPlayingAd = false
                    self.player.play()
                    self.youtubePlayerView.playVideo()
                    self.engagementAdFirstIntervalTimer.resumeTimer()
                    self.engagementView.isHidden = true
    //                self.resumeVideo()
                }).disposed(by: adHelperDisposeBag)
            
            adHelp.adStart.asObservable()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
                .observeOn(MainScheduler.instance)
                .subscribe({ _ in
                    // ad Close callback here.
                    print("player start preroll ad")
    //                self.pauseVideo()
                    self.player.pause()
                    self.youtubePlayerView.pauseVideo()
                    self.engagementAdFirstIntervalTimer.pauseTimer()
                    self.isPlayingAd = true
                }).disposed(by: adHelperDisposeBag)
            
            if (currentVideo != nil) { adHelp.triggerAd(video: currentVideo! ,adType: .preroll) }

        }
         func setupEngagementAds() {
            displayAdHelper = DisplayAdHelper()
            if (currentVideo != nil) {
                displayAdHelper.requestDisplayAd(video: currentVideo!)
            }
    //        displayAdHelper.playerView = self.view
    //        self.addChild(adHelp)
        }
     func startNewMidRollInterval(duration:Int, interval:Int) {
            
            startMidrollAdTriggererTimer(duration:duration)
            midrollAdIntervalTimer.suspend()
            midrollAdIntervalTimer = OONATimer()
            midrollAdIntervalTimer.startTimer(seconds: interval) {
                self.startNewMidRollInterval(duration: duration, interval: interval)
                
            }
        }
         func startMidrollAdTriggererTimer(duration:Int) {
    //        adHelp.startTimer(seconds: 4, view: self.view)
            midrollAdTriggerTimer.suspend()
            midrollAdTriggerTimer = OONATimer()
            midrollAdTriggerTimer.startTimer(seconds: duration) {
                self.setupMidrollAds()
            }
            
            
        }
      
        func startNewEngagementInterval(adDuration:Int, firstInterval:Int, AfterwardInterval:Int) {
            
            self.engagementAdDismissTimer.suspend()
            self.engagementAdAfterwardIntervalTimer.suspend()
            engagementAdFirstIntervalTimer.suspend()
            
            
            
            engagementAdFirstIntervalTimer = OONATimer()
            engagementAdFirstIntervalTimer.identifier = "engagementAdFirstIntervalTimer"
            engagementAdFirstIntervalTimer.startTimer(seconds:firstInterval) {
                self.showEngagementAd()
                self.engagementAdDismissTimer.suspend()
                self.engagementAdDismissTimer = OONATimer()
                self.engagementAdDismissTimer.identifier = "engagementAdDismissTimer"
                self.engagementAdDismissTimer.startTimer(seconds: adDuration){
                    self.hideEngagementAd()
                    
                    self.startAfterwardEngagementInterval(adDuration:adDuration,interval:AfterwardInterval)
                    
                }
                
            }
            
        }
            func startAfterwardEngagementInterval(adDuration:Int, interval:Int) {
                self.engagementAdAfterwardIntervalTimer.suspend()
                 self.engagementAdAfterwardIntervalTimer = OONATimer()
               self.engagementAdAfterwardIntervalTimer.identifier = "engagementAdAfterwardIntervalTimer"
                self.engagementAdAfterwardIntervalTimer.startTimer(seconds: interval) {
                    self.showEngagementAd()
                    self.engagementAdDismissTimer.suspend()
                    self.engagementAdDismissTimer = OONATimer()
                   self.engagementAdDismissTimer.identifier = "engagementAdDismissTimer after "
                    self.engagementAdDismissTimer.startTimer(seconds: adDuration) {
        
                        
                        self.hideEngagementAd()
                        self.startAfterwardEngagementInterval(adDuration:adDuration,interval:interval)
                    }
                }
            }
}
