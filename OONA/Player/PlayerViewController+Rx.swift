//
//  PlayerViewController+Rx.swift
//  OONA
//
//  Created by Jack on 2/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import BrightcovePlayerSDK
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture
import AVKit
import MediaPlayer
extension PlayerViewController {
    enum PanIndicator {
        case volumne
        case brightness
    }
    
    func bindSlider() {
        progressSlider.rx.controlEvent([.touchDown])
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { _ in
                print("touchDragEnter")
                self.progressBarBeingSliding = true
                self.pauseVideo()
            })
            .disposed(by: disposeBag)
       
        //        progressSlider.rx.controlEvent(.valueChanged)
        //            .subscribe(onNext : { value in
        //
        //            })
        progressSlider.rx.controlEvent([.touchUpInside,.touchUpOutside])
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { _ in
                self.progressBarBeingSliding = false
                
                if (self.currentPlayer == .youtube){
                    self.youtubePlayerView.getDuration({ (duration, error) in
                        let targetSecond = duration * Double(self.progressSlider.value)
                        self.youtubePlayerView.seek(toSeconds: Float(targetSecond), allowSeekAhead: true)
                        self.youtubePlayerView.playVideo()
                    })
                    
                }else{
                    
                    let targetSecond = self.playerLayer.player!.currentItem!.duration.seconds * Double(self.progressSlider.value)
                    
                    self.playerLayer.player!.seek(to:CMTime.init(seconds: targetSecond, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero, completionHandler: { (completed) in
                        //                    self.playerLayer.player!.play()
                        self.playerLayer.player!.play()
                    })
                    
                }
                
              
            })
            .disposed(by: disposeBag)
        
    }
    func changePlayerTime() {
        
    }
    
    func bindPlayer() {
        
        playerVMDisposeBag = DisposeBag()
        
        self.playerLayer.player!.rx.timeControlStatus.subscribe(onNext: { status in
            if (self.currentPlayer == .bco) {
                switch status{
                case .paused:
                    if self.progressBarBeingSliding  {
                        self.playButton.imageView?.image = UIImage.init(named: "pause")
                    }else {
                        self.playButton.imageView?.image = UIImage.init(named: "play")
                    }
                    
                default:
                    self.playButton.imageView?.image = UIImage.init(named: "pause")
                }
            }
            
        }).disposed(by: playerVMDisposeBag)
        
        
        viewModel.isLoading.asDriver()
            .filter { !$0 }
            .drive(onNext: { [unowned self] _ in
                print("item ready to play. start")
                
            })
            .disposed(by: playerVMDisposeBag)
        
        
        Driver.combineLatest(viewModel.current, viewModel.duration)
            .map(progress)
            .drive(progressSlider.rx.value)
            
            .disposed(by: playerVMDisposeBag)
        
        viewModel.current.drive(onNext: { (progress) in
            // Check video duration here.
            if self.isPlayingAd {
                self.playerLayer.player!.pause()
            }
            self.videoPlaying()
        }).disposed(by: playerVMDisposeBag)
        
        viewModel.readyToPlayPrerollAd
            .drive(onNext: {
                NSLog("preroll ad ready to play: \($0)")
                self.playerStartPlaying()
                //                self.startAdTimer()
            })
            .disposed(by: playerVMDisposeBag)
        
        viewModel.error
            .drive(onNext: { _ in
                NSLog("item error")
                
            })
            .disposed(by: playerVMDisposeBag)
        
        viewModel.didPlayToEnd
            .drive(onNext: { [unowned self] _ in
                NSLog("item did play to end")
//                self.backToStart()
                self.playNextVideo()
            })
            .disposed(by: playerVMDisposeBag)
        
        
        
        
    }
    func setVolumne(offset:CGFloat){
        self.indicatorImage.image  = UIImage.init(named: "volume")
        self.indicatorLb.text = "\(offset)%"
        var newVolume : CGFloat = self.initialVolume + ( (offset * -1) / (self.view.frame.size.height / 2.5))
        
        newVolume = max(newVolume, 0)
        newVolume = min(newVolume, 1)
        
        let intValue = Int((newVolume * 100))
        
        
        
        MPVolumeView.setVolume(Float(newVolume))
        self.indicatorLb.text = "\(intValue)%"
    }
    func setBrightness(offset:CGFloat){
        self.indicatorImage.image  = UIImage.init(named: "brightness")
        var newBrightness : CGFloat = self.initialBrightness + ( (offset * -1) / (self.view.frame.size.height / 2.5))
        newBrightness = max(newBrightness, 0)
        newBrightness = min(newBrightness, 1)
        let intValue = Int((newBrightness * 100))
        print("new b \(newBrightness)")
        
        UIScreen.main.brightness = newBrightness
        self.indicatorLb.text = "\(intValue)%"
    }
    func handleIndicatorPan(panIndicator:PanIndicator, panGesture:UIPanGestureRecognizer) {
        switch panGesture.state {
        case .began :
            
            switch panIndicator {
            case .volumne:
                self.changingVolumne = true
            case .brightness:
                self.changingBrightness = true
            }
            self.initialVolume = CGFloat(AVAudioSession.sharedInstance().outputVolume)
            self.initialBrightness = UIScreen.main.brightness
            UIView.animate(withDuration: 0.3) {
                self.brightnessVolumneIndicator.alpha = 1
            }
            
        case .ended :
            changingVolumne = false
            changingBrightness = false
            UIView.animate(withDuration: 0.3) {
                self.brightnessVolumneIndicator.alpha = 0
            }
        default :
            print("default case")
        }
        
        
    }
    
    func initGestures() {
        // volumne
        self.volumneBtn.rx
            .panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] panGesture in
                
                if (!(self?.changingBrightness ?? false)) {
                
                    let piece       = panGesture.view!
                    let translation = panGesture.translation(in: piece.superview)
                    let heightDiff  = translation.y
                    
                    print("vol pan",heightDiff)
                    self?.handleIndicatorPan(panIndicator:.volumne, panGesture: panGesture)
                    self?.setVolumne(offset: heightDiff)
                }
            })
            .disposed(by: disposeBag)
        
        self.brightnessBtn.rx
            .panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] panGesture in
                if (!(self?.changingVolumne ?? false)) {
                    let piece       = panGesture.view!
                    let translation = panGesture.translation(in: piece.superview)
                    let heightDiff  = translation.y
                    
                    self?.handleIndicatorPan(panIndicator:.brightness ,panGesture: panGesture)
                    print("brightness pan",heightDiff)
                    self?.setBrightness(offset: heightDiff)
                }
            })
            .disposed(by: disposeBag)
        
        
        //right menu
        self.background.rx
            .swipeGesture(.left)
            .when(.recognized)
            .subscribe(onNext: {  [weak self] swipeGesture in
                //dismiss presented photo
                print("swipe left")
                if (!self!.essentialUiOnly) {
                if(self?.menuRightEnable ?? false){
                    self?.getRightMenu()
                }
                }
            })
            .disposed(by: disposeBag)
        
        
        //left menu
        self.background.rx
            .swipeGesture(.right)
            .when(.recognized)
            .subscribe(onNext: {  [weak self] _ in
                //dismiss presented photo
                print("swipe right")
                if(self?.menuLeftEnable  ?? false){
                    self?.handleSwipeGestureLeftMenu()
                }
            })
            .disposed(by: disposeBag)
        
        
        //show menu in background view when swipe up
        self.background.rx
            .swipeGesture(.up)
            .when(.recognized)
            .subscribe(onNext: { [weak self]  _ in
                self?.showSwipeUpMenu()
                
                print("swipe up")
            })
            .disposed(by: disposeBag)
        
        
        //show menu in background view when double tap
        self.background.rx.tapGesture{  [weak self] gesture, _ in
            gesture.numberOfTapsRequired = 2
            }
            .when(.recognized)
            .subscribe(onNext: { _ in
                
                if(self.menuUpEnable){
                    
                    if (self.tcoinBarTop.constant == 0){
                        //check if the scroll direction is enable
                        
                        self.initView()
//                        self.showMenu(menuType: .up, subMenu: .none ,youtubeMode:(self.currentPlayer == .youtube) )
                        self.showSwipeUpMenu()
                        self.resetPanDirection()
                        
                    }
                }
                print("double tap")
            })
            .disposed(by: disposeBag)
        
        //show menu in contorl view when swipe up
        self.controlViewDismissButton.rx
            .swipeGesture(.up)
            .when(.recognized)
            .subscribe(onNext: { [weak self]  _ in
                
                if(self?.menuUpEnable  ?? false){
//                    self?.setEngagementAdInvisible()
                    
                    if (self?.tcoinBarTop.constant == 0){
                        //check if the scroll direction is enable
                        if let _youtubeCenter = self?.youtubeViewCenter{
                            
                            NSLayoutConstraint.deactivate([_youtubeCenter])
                        }
                        self?.view.layoutIfNeeded()
                        self?.initView()
                        self?.pauseVideo()
                        self?.showMenu(menuType: .up, subMenu: MenuHelper.shared.getCurrentSubMenu() ,youtubeMode:(self?.currentPlayer == .youtube) )
                        self?.resetPanDirection()
                        
                    }
                }
                print("controlView swipe up")
            })
            .disposed(by: disposeBag)
        
        
        //show menu in contorl view when double tap
        self.controlViewDismissButton.rx.tapGesture{  [weak self] gesture, _ in
            gesture.numberOfTapsRequired = 2
            }
            .when(.recognized)
            .subscribe(onNext: { _ in
                
                if(self.menuUpEnable){
                    
                    if (self.tcoinBarTop.constant == 0){
                        //check if the scroll direction is enable
                        NSLayoutConstraint.deactivate([self.youtubeViewCenter])
                        self.view.layoutIfNeeded()
                        self.initView()
                        self.showMenu(menuType: .up, subMenu: .none ,youtubeMode:(self.currentPlayer == .youtube) )
                        self.resetPanDirection()
                        
                    }
                }
                print("double tap")
            })
            .disposed(by: disposeBag)
        
        
        self.controlViewDismissButton.rx.swipeGesture(.right)
            .when(.recognized)
            .subscribe(onNext: {  [weak self] _ in
                //dismiss presented photo
                print("swipe right")
                self?.hideControlView()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
                    if(self?.menuLeftEnable  ?? false){
                        
                        self?.showLeftMenu(leftMenuType: .full)
                    }
                })
                
            })
            .disposed(by: disposeBag)
        
        self.engagementAdButton.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] gestureRecognizer in
                
                if(self?.menuRightEnable  ?? false){
                    self?.panGestureForEngagement(gestureRecognizer: gestureRecognizer)
                }
            })
            .disposed(by: disposeBag)
        self.gameAdButton.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] gestureRecognizer in
                
//                if(self?.menuRightEnable  ?? false){
                    self?.panGestureForGameAd(gestureRecognizer: gestureRecognizer)
//                }
            })
            .disposed(by: disposeBag)
        
        // gameAdButtonLeading
        self.engagementView.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] gestureRecognizer in
                
                
                if(self?.menuRightEnable  ?? false){
                    self?.panGestureForEngagement(gestureRecognizer: gestureRecognizer)
                }
            })
            .disposed(by: disposeBag)
        
        
        self.engagementView.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] panGesture in
                print("engagementAdButton pan ing")
                //                panGesture.delegate = self
                if(self?.menuUpEnable  ?? false){
                    self?.panGestureForTcoinTab(panGesture: panGesture)
                    /// TODO:
                    //                    let state = panGesture.state
                    //                    if state == .ended ||
                    //                        state == .failed ||
                    //                        state == .cancelled {
                    //                        self.resetPanDirection()
                    //                    }
                    
                }
            })
            .disposed(by: disposeBag)
        
        //for left menu drawer
        self.background.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] gestureRecognizer in
                //                gestureRecognizer.delegate = self
                if(self?.menuLeftEnable  ?? false){
                    self?.handlePanGestureLeftMenu(gestureRecognizer: gestureRecognizer)
                    
                }
            })
            .disposed(by: disposeBag)
        
        //for horitzontal drawer
        
        self.background.rx.panGesture().when(.began,.changed,.ended, .cancelled, .failed)
            .subscribe(onNext: {  [weak self] panGesture in
                if(self?.menuDownEnable  ?? false){
                    //                    panGesture.delegate = self
                    self?.initView()
                    
                    self?.panGestureForTcoinTab(panGesture: panGesture)
                    /// TODO:
                    //                    let state = panGesture.state
                    //                    if state == .ended ||
                    //                        state == .failed ||
                    //                        state == .cancelled {
                    //                        self.resetPanDirection()
                    //                    }
                    
                }
                
            })
            .disposed(by: disposeBag)
        
        self.tcoinsDetailView.rx.panGesture()
            .when(.began, .changed, .ended, .cancelled, .failed)
            .subscribe(onNext: {  [weak self] panGesture in
                //                panGesture.delegate = self
                self?.panGestureForTcoinTab(panGesture: panGesture)
                
            }).disposed(by: disposeBag)
        
        self.controlViewDismissButton.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] panGesture in
                //                panGesture.delegate = self
                if(self?.menuUpEnable  ?? false){
                    self?.panGestureForTcoinTab(panGesture: panGesture)
                    /// TODO:
                    //                    let state = panGesture.state
                    //                    if state == .ended ||
                    //                        state == .failed ||
                    //                        state == .cancelled {
                    //                        self.resetPanDirection()
                    //                    }
                }
            })
            .disposed(by: disposeBag)
        
        self.lowerView.rx.panGesture().when(.began,.changed,.ended)
            .subscribe(onNext: {  [weak self] panGesture in
                //                panGesture.delegate = self
                if(self?.menuUpEnable ?? false){
                    self?.panGestureForTcoinWallet(gestureRecognizer: panGesture)
                }
            })
            .disposed(by: disposeBag)
    }
    
    
    
}
extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
  
}
