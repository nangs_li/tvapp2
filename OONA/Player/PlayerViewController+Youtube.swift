//
//  PlayerViewController+Youtube.swift
//  OONA
//
//  Created by Jack on 16/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import YoutubePlayer_in_WKWebView

extension PlayerViewController {
   
    func initYoutubeView() {
        self.youtubeView.isHidden = true
        
        let playerVars = defaultPlayerVars
        
//        youtubeView.load(withVideoId: "20k4Pa8TzdY",playerVars:playerVars)
        
        youtubePlayerView.delegate = self
       
    }
    
    func playerViewPreferredWebViewBackgroundColor(_ playerView: WKYTPlayerView) -> UIColor {
        return UIColor.clear
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        print("become rdy")
        if (currentPlayer == .youtube){
           
         youtubePlayerView.playVideo()
            
        }
//        if isFirstVideo {
//            self.pauseVideo()
//            isFirstVideo = false
//        }
    }
    
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print("YouTube player state did updated")
        self.youtubePlayerChangeToState(state:state)
    }
    
    func youtubePlayerChangeToState(state:WKYTPlayerState) {
        
        DispatchQueue.main.async {
        
        self.currentYTState = state
//        print("YT change to state",state.rawValue)
        if (self.currentPlayer == .youtube ) {
            switch state{
            case .paused:
                if self.progressBarBeingSliding  {
                    self.playButton.imageView?.image = UIImage.init(named: "pause")
                }else {
                    self.playButton.imageView?.image = UIImage.init(named: "play")
                }
            case .ended:
    //            self.youtubePlayerView.seek(toSeconds: 0, allowSeekAhead: true)
                if (self.currentPlayer == .youtube) {
                    self.playNextVideo()
                }
            case .playing:
                self.playButton.imageView?.image = UIImage.init(named: "pause")
                self.videoPlaying()
                
            case .buffering:
//                if !self.isFirstVideo {
//                    self.view.startLoading(above: self.youtubeView)
//                }
                break
            default:
                print("YT default case")
            }
            
            if (state == .playing) {
                self.youtubeLoadingBg.alpha = 0
                self.youtubeLoadingGif.alpha = 0
                self.youtubeLoadingGif.stopAnimatingGif()
                self.view.cancelLoading()
            }else{
                self.youtubeLoadingBg.alpha = 1
                self.youtubeLoadingGif.alpha = 1
                self.youtubeLoadingGif.startAnimatingGif()
            }
        }
            
            
        }
    }
    
    func playerView(_ playerView: WKYTPlayerView, didPlayTime playTime: Float) {
//        print("YT didPlayTime",playTime)
//        print("YT current PlayTime",playerView.currentTime())
        self.youtubePlayerDuration = playTime
        PlayerHelper.shared.currentDuration = Int(playTime)
        playerView.getDuration { (duration, error) in
            
            self.progressSlider.value = Float(Double(playTime) / Double(duration))
            self.youtubePlayerView.getPlayerState({ (state, error) in
                self.youtubePlayerChangeToState(state:state)
            })
            
            if (self.currentPlayer == .bco) {
                self.youtubePlayerView.pauseVideo()
            }
        }
       
    }
    
//    func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
//        self.view.cancelLoading()
//        print("YouTube Error: \(error.rawValue)")
//    }
    
    func playVideo(youtubeId:String) {
        print(#function)
        self.resetYoutubeViewConstraints()
        let playerVars = defaultPlayerVars
//        print("youtubeId",youtubeId)
        youtubePlayerView.stopVideo()
        if !self.isFirstVideo {
            self.view.startLoading(above: self.youtubeView)
        }
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.5) {
            
            self.youtubePlayerView.load(withVideoId: youtubeId,playerVars:playerVars)
            
            self.youtubePlayerView.delegate = self
//            if self.isFirstVideo {
//                print("yt firstvideo pause")
//                self.pauseVideo()
//                self.isFirstVideo = false
//            }
        }
       
    }
}
