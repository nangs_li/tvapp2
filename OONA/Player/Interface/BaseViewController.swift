//
//  BaseViewController.swift
//  OONA
//
//  Created by Jack on 16/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVKit
import YLProgressBar
import SwiftyGif
import RxLocalizer

enum Tutorial: String {
    case upMenu = "explainer-upmenu"
    case downMenu = "explainer-downmenu"
    case leftMenu = "explainer-rightmenu"
    case rightMenu = "explainer-leftmenu"
    case tapMenu = "explainer-tapmenu"
    case tcoin = "explainer-tcoin"
    case earnTcoin = "explainer-tcoins-earn"
    case tcoinBid = "explainer-tcoins-bid"
    case gamePlayAward50 = "explainer-game-congrats-50"
    case gamePlayAward100 = "explainer-game-congrats-100"
    case gamePlayAward500 = "explainer-game-congrats-500"
    case gamePlayAward1000 = "explainer-game-congrats-1000"
    case sponsorExplainer = "OONA-Sponsor"
    case spendingTcoinTutorial = "spending_tcoins_tutorial"
    
    case firstTimeBumper = "firsttime-bumper"//            let path = Bundle.main.path(forResource: "firsttime-bumper", ofType:"mp4")
}
enum RewardVideo : String {
    case Award50 = "explainer-game-congrats-50"
    case Award100 = "explainer-game-congrats-100"
    case Award250 = "explainer-game-congrats-250"
    case Award500 = "explainer-game-congrats-500"
    case Award1000 = "explainer-game-congrats-1000"
    case Award10000 = "explainer-game-congrats-10000"
}


class BaseViewController : UIViewController {
    
    public lazy var viewName : String = ""
    public lazy var menuVC : UIViewController = UIViewController()
    public var chVC : MenuViewController?
    public var circularVC : CircluarMenuViewController?
    public var circularCollectionViewWidth : CGFloat?
    lazy var tcoinsDetailVC: TCoinsDetailViewController = CreateTcoinsDetailPage()
    lazy var xploreViewController: XploreViewController = CreateXplorePage()
    lazy var newRadioViewController: NewRadioViewController = CreateNewRadioPage()
    lazy var dailyBidVC: DailyBidViewController = CreateDailyBidPage()
    lazy var tcoinsStoreVC: TCoinsStoreViewController = CreateTCoinsStorePage()
    lazy var leftMenuTcoinsDetailVC: TCoinsDetailViewController = CreateTcoinsDetailPage()
    lazy var gameViewController: GameViewController = CreateGamePage()
    lazy var searchViewController: SearchViewController = CreateSearchPage()
    lazy var chatBotViewController: ChatBotViewController = CreateChatBotPage()
    let disposeBag = DisposeBag()
    public var menuIsShowing : Bool = false
    
    var progressBar : YLProgressBar?
    var rewardLabel : UILabel?
    var currentTutorialVideo : Tutorial?
    
    private var tutorialVideoView : UIView?
    private var tutorialPlayer: AVPlayer?
    private var tutorialPlayerLayer: AVPlayerLayer?
    private var tutorialBag: DisposeBag = DisposeBag()
    
    @IBOutlet weak var rightConstraintForInnerView: NSLayoutConstraint!
    @IBOutlet weak var leftConstraintForInnerView: NSLayoutConstraint! //extra padding for all cv view
    @IBOutlet weak var leftViewPadding: NSLayoutConstraint! //extra padding for iphone 6-8 only for left view only
    
    func getLeftViewConstraint() -> CGFloat{
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                return APPSetting.leftViewConstraintSafearea
            case 1334:
                //print("iPhone 6/6S/7/8")
                return APPSetting.leftViewConstraintSafearea
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return APPSetting.leftViewConstraintSafearea
            case 2436: break
            //print("iPhone X, XS")
            case 2688: break
            //print("iPhone XS Max")
            case 1792: break
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        
        return APPSetting.leftViewConstraint
    }
    
    func getLeftConstraint() -> CGFloat{
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                return APPSetting.leftConstraintForInnerPageSafearea
            case 1334:
                //print("iPhone 6/6S/7/8")
                return APPSetting.leftConstraintForInnerPageSafearea
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return APPSetting.leftConstraintForInnerPageSafearea
            case 2436: break
            //print("iPhone X, XS")
            case 2688: break
            //print("iPhone XS Max")
            case 1792: break
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        
        return APPSetting.leftConstraintForInnerPage
        
    }
    
    func showToturial()->Bool{
        let storyboard = UIStoryboard(name: "Intro", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! IntroViewController
        self.present(vc, animated: false, completion: nil)
        //UserDefaults.standard.set(true, forKey: "tutorial123")
        //UserDefaults.standard.synchronize()
        return true
    }
    var systemDialog : UIView = UIView()
    override func viewDidLoad() {
        /**show tutorial**/
        super.viewDidLoad()
        print(String(describing: type(of: self)) + " " + #function)
        //showToturial()
        Localizer.shared.changeLanguage.subscribe(onNext: { [weak self] (languageCode) in
            self?.appLanguageDidUpdate()
        }).disposed(by: self.disposeBag)
        self.appLanguageDidUpdate()
        
        UserHelper.shared.userProfile
            .subscribe(onNext: { [weak self] (userProfile) in
                self?.updateUserViews()
            }).disposed(by: disposeBag)
        self.refreshUserDatas()
//        self.initSystemDialogContainer()
    }
    
    func initSystemDialogContainer (text:String) {
        systemDialog = UIView()
          systemDialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
//        systemDialog.backgroundColor = .red
        let tcoinsResultViewController : SystemMessageViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "SystemMessageViewController") as! SystemMessageViewController
//        tcoinsResultViewController.setMessage(text:"123456789")
        tcoinsResultViewController.message = text
        tcoinsResultViewController.completionBlock = {(result) -> () in
            //show coin wallet
            
        }
        self.addChild(tcoinsResultViewController)
        self.systemDialog.addSubview(tcoinsResultViewController.view)
        tcoinsResultViewController.view.frame = systemDialog.bounds
//        self.systemDialog.layoutIfNeeded()
//        self.systemDialog.layoutSubviews()
//
        
        self.view.addSubview(systemDialog)
        
     
    }
    
    func showSystemDialog(text:String, dismissDelay:Int) {
        
//        systemDialog.removeFromSuperview()
        let dialog = UIView()
        dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
        //        systemDialog.backgroundColor = .red
        let tcoinsResultViewController : SystemMessageViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "SystemMessageViewController") as! SystemMessageViewController
        //        tcoinsResultViewController.setMessage(text:"123456789")
        tcoinsResultViewController.setMessage(text: text)
        tcoinsResultViewController.completionBlock = {(result) -> () in
            //show coin wallet
            
        }
        self.addChild(tcoinsResultViewController)
        dialog.addSubview(tcoinsResultViewController.view)
        tcoinsResultViewController.view.frame = dialog.bounds
        //        self.systemDialog.layoutIfNeeded()
        //        self.systemDialog.layoutSubviews()
        //
        
        self.view.addSubview(dialog)
        
        UIView.animate(withDuration: 0.3) {
            
          dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height-60, width: self.view.frame.size.width, height: 60)
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(dismissDelay)) {
            UIView.animate(withDuration: 0.3) {
                
                dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
                
            }
            
        }
    }
    func showSystemDialog(html:String, dismissDelay:Int) {
        
        //        systemDialog.removeFromSuperview()
        let dialog = UIView()
        dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
        //        systemDialog.backgroundColor = .red
        let tcoinsResultViewController : SystemMessageViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "SystemMessageViewController") as! SystemMessageViewController
        //        tcoinsResultViewController.setMessage(text:"123456789")
//        tcoinsResultViewController.message = text
        tcoinsResultViewController.htmlString(html: html)
        tcoinsResultViewController.completionBlock = {(result) -> () in
            //show coin wallet
            
        }
        self.addChild(tcoinsResultViewController)
        dialog.addSubview(tcoinsResultViewController.view)
        tcoinsResultViewController.view.frame = dialog.bounds
        //        self.systemDialog.layoutIfNeeded()
        //        self.systemDialog.layoutSubviews()
        //
        
        self.view.addSubview(dialog)
        
        UIView.animate(withDuration: 0.3) {
            
            dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height-60, width: self.view.frame.size.width, height: 60)
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(dismissDelay)) {
            UIView.animate(withDuration: 0.3) {
                
                dialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
                
            }
            
        }
    }
    func showSystemDialog(text:String) {
        
        //        systemDialog.removeFromSuperview()
        self.initSystemDialogContainer(text: text)
        UIView.animate(withDuration: 0.3) {
            
            self.systemDialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height-60, width: self.view.frame.size.width, height: 60)
            
        }
    }
    func hideSystemDialog() {
        UIView.animate(withDuration: 0.3) {
            
            self.systemDialog.frame = CGRect.init(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 60)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if DEBUG
        print(String(describing: type(of: self)) + " " + #function)
        #endif
        /**Setup constraints for menu view*/
        if(leftConstraintForInnerView != nil){
            leftConstraintForInnerView.constant = getLeftConstraint()
        }
        if(leftViewPadding != nil){
            leftViewPadding.constant = getLeftViewConstraint()
        }
        if(rightConstraintForInnerView != nil){
            rightConstraintForInnerView.constant = APPSetting.rightConstraintForInnerPage
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    //print("iPhone 5 or 5S or 5C")
                    rightConstraintForInnerView.constant = APPSetting.rightConstraintForInnerPageSafearea
                case 1334:
                    //print("iPhone 6/6S/7/8")
                    rightConstraintForInnerView.constant = APPSetting.rightConstraintForInnerPageSafearea
                case 1920, 2208:
                    //print("iPhone 6+/6S+/7+/8+")
                    rightConstraintForInnerView.constant = APPSetting.rightConstraintForInnerPageSafearea
                case 2436: break
                //print("iPhone X, XS")
                case 2688: break
                //print("iPhone XS Max")
                case 1792: break
                //print("iPhone XR")
                default: break
                    //print("Unknown")
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        #if DEBUG
        print(String(describing: type(of: self)) + " " + #function)
        #endif
        viewEnterFirebase()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        #if DEBUG
        print(String(describing: type(of: self)) + " " + #function)
        #endif
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        #if DEBUG
        print(String(describing: type(of: self)) + " " + #function)
        #endif
        viewLeaveFirebase()
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    func appLanguageDidUpdate() {
        
    }
    
    func firebaseViewIndicator() -> (String){
        if (viewName.hasPrefix("ui_")){
            return viewName
        }
        return ("ui_" + self.viewName)
    }
    
    private func viewEnterFirebase() {
        if self.viewName != "" {
        AnalyticHelper.shared.viewEnter(viewName: firebaseViewIndicator())
        }
//
//        APIAnalyticHelper.shared.logEvent(name:
//        )
    }
    
    private func viewLeaveFirebase() {
        if self.viewName != "" {
        AnalyticHelper.shared.viewLeave(viewName:firebaseViewIndicator())
        }
    }
    
    func hasSubMenu()->Bool{
        return (MenuHelper.shared.getCurrentSubMenu() != .none)
    }
    
    func showSettings(completion: (() -> Void)? = nil) {
        guard let settingsMain: SettingsViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
            print("Unable to show settings"); return
        }
        let settings: SettingsNavigationViewController = SettingsNavigationViewController(rootViewController: settingsMain)
//        guard let settings: SettingsNavigationViewController =
//            UIStoryboard(name: "Settings", bundle: nil).instantiateInitialViewController() as? SettingsNavigationViewController else { print("Unable to show settings"); return }
        settings.modalTransitionStyle = .crossDissolve
        settings.navigationBar.isHidden = true
        settingsMain.completionBlock = completion
        self.present(settings, animated: true) {
            
        }
    }
    
    func playTutorial(rewardUI:Bool, tutorial: Tutorial,
                      completion: ((_ tutorial: Tutorial?) -> Void)? = nil) {
        // pause video first
        currentTutorialVideo = tutorial
        guard let path = Bundle.main.path(forResource: tutorial.rawValue, ofType:"mp4")
            else {
                print("Tutorial Path \(tutorial.rawValue) Not Found")
                completion?(nil)
                return
        }
        guard let delegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate,
            let window: UIWindow = delegate.window else {
            completion?(nil)
            return
        }
        self.tutorialBag = DisposeBag()
        tutorialVideoView?.removeFromSuperview()
        tutorialVideoView = UIView(frame: window.bounds)
        
        tutorialVideoView?.isHidden = false
        tutorialVideoView?.backgroundColor = UIColor.black
        self.tutorialVideoView?.isHidden = false
        
        tutorialPlayer = AVPlayer(url: URL(fileURLWithPath: path))
        tutorialPlayerLayer = AVPlayerLayer(player: tutorialPlayer)
        tutorialPlayerLayer?.videoGravity = .resizeAspect
        tutorialPlayerLayer?.frame = self.tutorialVideoView!.bounds
        tutorialPlayerLayer?.zPosition = -1
        
        self.tutorialVideoView?.layer.addSublayer(tutorialPlayerLayer!)
        
//        self.view.addSubview(self.tutorialVideoView!)
        window.addSubview(self.tutorialVideoView!)
        
//
//        NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification).subscribe(onNext: { [weak self] (_) in
//            self?.tutorialPlayer?.pause()
//        }).disposed(by: tutorialBag)
        
        NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification).subscribe(onNext: { [weak self] (_) in
            self?.tutorialPlayer?.play()
        }).disposed(by: tutorialBag)
        
        let vm = PlayerViewModel(player: tutorialPlayer!)
        
  
        
        if rewardUI {
            self.setRewardUI()
        }
        vm.didPlayToEnd
            .drive(onNext: { [weak self] _ in
                NSLog("tcoin did play to end")
                self?.tutorialVideoView?.removeFromSuperview()
                self?.tutorialPlayer = nil
                completion?(tutorial)
                self?.tutorialBag = DisposeBag()
            })
            .disposed(by: tutorialBag)
        
        vm.current.drive(onNext: { (progress) in
            // Check video duration here.r
            self.tutorialPlaying(progress: progress)
        }).disposed(by: tutorialBag)
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.tutorialPlayer?.play()
        })
    }
    
    func tutorialPlaying(progress:Float) {
        
        let prog : CGFloat = CGFloat((CGFloat(progress)/CGFloat((tutorialPlayer?.currentItem?.duration.seconds)!)))
        if prog.isFinite {
            var rewardAmount : CGFloat = 100.0
            if (currentTutorialVideo == .sponsorExplainer) {
                rewardAmount = 500.0
            }
            if (currentTutorialVideo == .firstTimeBumper) {
                rewardAmount = 250.0
            }
            if (currentTutorialVideo == .spendingTcoinTutorial) {
                rewardAmount = 250.0
            }
            progressBar?.progress = CGFloat(prog)
            rewardLabel?.text = "\(Int(min(prog,1) * rewardAmount))"
        }
    }
    
    func setRewardUI() {
        progressBar?.removeFromSuperview()
        progressBar = YLProgressBar(frame: CGRect(x: 0, y: view.frame.size.height - 3, width: view.frame.size.width, height: 3))
        progressBar?.progressTintColor = Color.redOONAColor()!
        progressBar?.progress = 0
        progressBar?.progressStretch = false
        progressBar?.uniformTintColor = true
        progressBar?.type = .flat
        progressBar?.hideStripes = true
        progressBar?.trackTintColor = UIColor.gray
        
        progressBar?.layer.masksToBounds = true
        
        self.tutorialVideoView!.addSubview(progressBar!)
        
        let rewardGif = UIImageView(frame: CGRect(x: self.view.safeAreaInsets.left + 5, y:  self.view.safeAreaInsets.top + 5, width: 100, height: 100*5/6))
        
        do {
            let gif = try UIImage(gifName: "tcoins_pile2", levelOfIntegrity:1.0)
            rewardGif.setGifImage(gif)
            
        } catch  {
            print("set gif error")
            print(error)
        }
        self.tutorialVideoView!.addSubview(rewardGif)
        rewardLabel = UILabel(frame: CGRect(x: rewardGif.frame.maxX-10, y: rewardGif.frame.minY+5, width: 80, height: 60))
        rewardLabel?.font = UIFont(name: "Montserrat-Regular", size: 30.0)!
        rewardLabel?.textColor = UIColor.white
        rewardLabel?.text = ""
        self.tutorialVideoView!.addSubview(rewardLabel!)
        
        
        
    }
    
    func checkParentalControl(completion: ((_ success: Bool, _ parentalControl: ParentalControl?) -> Void)?) {
        
        ParentalControlHelper.shared.getParentalControlPreference(success: { pc in
            //has parental control , ask for code
            completion?(true, pc)
        }, failure: { [weak self] code , msg in
            if let _msg = msg {
                self?.showAlert(title: "", message: _msg,
                                completion: { (alertAction) in
                    completion?(false, nil)
                })
            }
            
        })
    }
    
    func showLogin(completion: (() -> Void)? = nil){
        /*
        let loginEntryViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginEntryViewController") as! LoginEntryViewController
        loginEntryViewController.view.backgroundColor = .clear
        loginEntryViewController.modalPresentationStyle = .overCurrentContext
        loginEntryViewController.modalTransitionStyle = .crossDissolve
        self.present(loginEntryViewController , animated: true, completion: nil)
        */
        
        let loginEntryViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginEntryViewController") as! LoginEntryViewController
        let _navigationViewController: UINavigationController = UINavigationController(rootViewController: loginEntryViewController)
        _navigationViewController.navigationBar.isHidden = true
        loginEntryViewController.completionBlock = completion
        self.present(_navigationViewController, animated: true) {
        }
        
    }
    
    func showInviteCodeEnterVC(){
        let yourInfovc : ReferralEnterViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralEnterViewController") as! ReferralEnterViewController
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.inviteCode()
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        self.present(yourInfovc , animated: true, completion: nil)
    }
    
    func showReferralCodeEnterVC() {
        let yourInfovc : ReferralEnterViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralEnterViewController") as! ReferralEnterViewController
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.referCode()
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        self.present(yourInfovc , animated: true, completion: nil)
    }
    
    func showTransferCodeEnterVC(){
        let yourInfovc : ReferralEnterViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralEnterViewController") as! ReferralEnterViewController
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.transferCode()
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        self.present(yourInfovc , animated: true, completion: nil)
    }
    
    func showUserProfile(completion:(() -> Void)? = nil){
        let yourInfoVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        //yourInfovc.view.backgroundColor = .clear
        yourInfoVC.modalPresentationStyle = .overCurrentContext
        yourInfoVC.modalTransitionStyle = .crossDissolve
        yourInfoVC.completionBlock = completion
        self.present(yourInfoVC, animated: true, completion: nil)
    }
    
    func showEditProfile(){
        let yourInfovc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController")
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        self.present(yourInfovc , animated: true, completion: nil)
    }
    
    func showChangePassword(){
        let yourInfovc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController")
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        self.present(yourInfovc , animated: true, completion: nil)
    }
    
    
    
    func channelMenuOnShow() {
        
    }
    
    func channelMenuOnHide() {
        
    }
    
    func refreshUserDatas() {
        UserHelper.shared.getUserProfile(success: { (userProfile) in
            
            UserHelper.shared.userProfile.accept(userProfile)
            self.updateUserViews()
        }) { (errorCode, errorMessage) in
            print("error code :r",errorCode,"\n error message: ",errorMessage)
        }
    }
    
    func updateUserViews() {
        
    }
    
    func hideMenu(){
            //menuVC.removeFromParent()
//        menuIsShowing = false
        menuVC.dismiss(animated: true, completion: nil)
        channelMenuOnHide()
    }
    
    @objc func bannerClicked(_ btn: UIButton?) {
        //action(by: URL(string: btn?.layer.value(forKey: "action") as? String ?? ""))
    }
    
    
    @objc func showTCoinDetailPage(_ btn: UIButton?) {
        let tcoinsDetailVC = CreateTcoinsDetailPage()
        let _navigationViewController: UINavigationController = UINavigationController(rootViewController: tcoinsDetailVC)
        _navigationViewController.navigationBar.isHidden = true
        
        self.present(_navigationViewController, animated: true) {
        }
    }
    
    func loginSuccess(){
        self.refreshUserDatas()
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }
    
    //used in loginviewcontroller
    
    @IBAction func back(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: false)
    }
    
    var congratView : UIView?
    
    var introPlayer = AVPlayer()
    var introPlayerItem : AVPlayerItem = .init(url: URL.init(string: "b")!)
    var introPlayerLayer = AVPlayerLayer()
    var bag = DisposeBag()
    
    func tcoinRewardVideo(rewardAmount:RewardVideo) {
        
        bag = DisposeBag()
        
        self.congratView?.removeFromSuperview()
        congratView = UIView.init(frame: UIScreen.main.bounds)
        congratView?.isHidden = false
        congratView?.backgroundColor = UIColor.black
        introPlayerLayer.removeFromSuperlayer()
        self.congratView?.isHidden = false
        let path = Bundle.main.path(forResource: rewardAmount.rawValue, ofType:"mp4")
        introPlayer = AVPlayer.init(url: URL.init(fileURLWithPath: path!))
        introPlayerLayer = AVPlayerLayer (player: introPlayer)
        introPlayerLayer.videoGravity = .resizeAspect
        introPlayerLayer.frame = self.congratView!.bounds
        introPlayerLayer.zPosition = -1
        
        self.congratView?.layer.addSublayer(introPlayerLayer)
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.congratView!) // Add your view here
        
        introPlayer.play()
        
        
        let vm = PlayerViewModel.init(player: introPlayer)
        vm.didPlayToEnd
            .drive(onNext: { [unowned self] _ in
                NSLog("tcoinRewardVideo did play to end")
                self.congratView?.isHidden = true
                self.congratView?.removeFromSuperview()
                self.bag = DisposeBag()
//                self.nextStep()
                self.tcoinRewardVideoFinished(rewardAmount:rewardAmount)
                //                self.playtCoin()
            })
            .disposed(by: bag)
    }
    func tcoinRewardVideoFinished(rewardAmount:RewardVideo){
        // call back chatbot the reward video is done
    }
    deinit {
        print("Deallocating \(self)")
//        print("deallocating" + String(describing: type(of: self)))
    }
}


extension BaseViewController {
    internal func CreateTcoinsDetailPage() -> TCoinsDetailViewController {
        let _tcoinsDetailVC: TCoinsDetailViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "TCoinsDetailViewController") as! TCoinsDetailViewController
        return _tcoinsDetailVC
    }
    
    internal func CreateDailyBidPage() -> DailyBidViewController {
        let _dailyBidVC: DailyBidViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "DailyBidViewController") as! DailyBidViewController
        return _dailyBidVC
    }
    
    internal func CreateTCoinsStorePage() -> TCoinsStoreViewController {
        let _tcoinsStoreVC: TCoinsStoreViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "TCoinsStoreViewController") as! TCoinsStoreViewController
        return _tcoinsStoreVC
    }
    
    internal func CreateXplorePage() -> XploreViewController {
        let _tcoinsDetailVC: XploreViewController = UIStoryboard(name: "Xplore", bundle: nil).instantiateViewController(withIdentifier: "XploreViewController") as! XploreViewController
        return _tcoinsDetailVC
    }
    
    internal func CreateNewRadioPage() -> NewRadioViewController {
           let _tcoinsDetailVC: NewRadioViewController = UIStoryboard(name: "NewRadio", bundle: nil).instantiateViewController(withIdentifier: "NewRadioViewController") as! NewRadioViewController
           return _tcoinsDetailVC
       }
    
    internal func CreateGamePage() -> GameViewController {
        let vc: GameViewController = UIStoryboard(name: "GameViewController", bundle: nil).instantiateInitialViewController() as! GameViewController
        return vc
    }
    
    internal func CreateSearchPage() -> SearchViewController {
        let vc: SearchViewController = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController() as! SearchViewController
        return vc
    }
    
    internal func CreateChatBotPage() -> ChatBotViewController {
        let vc: ChatBotViewController = UIStoryboard(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "ChatBotViewController") as! ChatBotViewController
        return vc

    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    
    
}


extension UIViewController {
    
    func showAlert(title: String, message: String) {
        self.showAlert(title: title, message: message, completion: nil)
    }
    
    func showAlert(title: String, message: String, completion: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        
        let okAction = UIAlertAction(
            title: "OK",
            style: .default,
            handler: completion)
        alertController.addAction(okAction)
        
        self.present(
            alertController,
            animated: true,
            completion: nil)
    }
    
    func showDialog(title: String, msg: String ,okText: String, cancelText: String){
        
    }
}
extension UILabel {
    func set(html: String) {
        if let htmlData = html.data(using: .unicode) {
            do {
                
                let ptext  = try NSMutableAttributedString(data: htmlData,
                                                             options: [.documentType: NSAttributedString.DocumentType.html],
                                                             documentAttributes: nil)
                
                ptext.replaceFonts(with: UIFont.init(name: "Montserrat-Regular", size: 13)!)
                self.attributedText = ptext
            } catch let e as NSError {
                print("Couldn't parse \(html): \(e.localizedDescription)")
            }
        }
    }
}

extension NSMutableAttributedString {
    
    /// Replace any font with the specified font (including its pointSize) while still keeping
    /// all other attributes like bold, italics, spacing, etc.
    /// See https://stackoverflow.com/questions/19921972/parsing-html-into-nsattributedtext-how-to-set-font
    func replaceFonts(with font: UIFont) {
        let baseFontDescriptor = font.fontDescriptor
        var changes = [NSRange: UIFont]()
        enumerateAttribute(.font, in: NSMakeRange(0, length), options: []) { foundFont, range, _ in
            if let htmlTraits = (foundFont as? UIFont)?.fontDescriptor.symbolicTraits,
                let adjustedDescriptor = baseFontDescriptor.withSymbolicTraits(htmlTraits) {
                let newFont = UIFont(descriptor: adjustedDescriptor, size: font.pointSize)
                changes[range] = newFont
            }
        }
        changes.forEach { range, newFont in
            removeAttribute(.font, range: range)
            addAttribute(.font, value: newFont, range: range)
        }
    }
}
