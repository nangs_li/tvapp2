//
//  AVPlayer+Rx.swift
//  OONA
//
//  Created by Jack on 17/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import AVFoundation
import RxSwift
import RxCocoa

extension Reactive where Base: AVPlayer {
    var rate: Observable<Float> {
        return observe(Float.self, #keyPath(AVPlayer.rate))
            .map { $0 ?? 0 }
    }
    @available(iOS 10.0, tvOS 10.0, *)
    public var timeControlStatus: Observable<AVPlayer.TimeControlStatus> {
        return self.observe(AVPlayer.TimeControlStatus.self, #keyPath(AVPlayer.timeControlStatus))
            .map { $0 ?? .waitingToPlayAtSpecifiedRate }
    }
    
    
    var playing: Observable<Bool> {
        return rate.map { $0 != 0 }
    }
    
    
    var currentItem: Observable<AVPlayerItem?> {
        return observe(AVPlayerItem.self, #keyPath(AVPlayer.currentItem))
    }
    
    var status: Observable<AVPlayer.Status> {
        return observe(AVPlayer.Status.self, #keyPath(AVPlayer.status))
            .map { $0 ?? .unknown }
    }
    
    var error: Observable<NSError?> {
        return observe(NSError.self, #keyPath(AVPlayer.error))
    }
    
    func periodicTimeObserver(interval: CMTime) -> Observable<CMTime> {
        return Observable.create { observer in
            let time = self.base.addPeriodicTimeObserver(forInterval: interval, queue: nil) { time in
                observer.onNext(time)
            }
            return Disposables.create { self.base.removeTimeObserver(time) }
        }
    }
}
