
//
//  PlayerViewController.swift
//  OONA
//
//  Created by Jack on 16/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import CoreMotion
import Foundation
import AVKit
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture
import BrightcovePlayerSDK
import Gifu
import SDWebImage
import Network
import YoutubePlayer_in_WKWebView
import EMAlertController
import ABLoaderView
import SwiftyGif
import Alamofire
import SwiftyJSON
enum PlayerError: Error {
    case load
}
enum PlayerType  {
    case bco
    case avplayer
    case youtube
}

enum VideoControl {
    case Default
    case PlayPauseDim
    case Previous
    case PreviousDim
    case Next
    case NextDim
}

enum MenuTab {
    case none
    case search
    case tcoin
    case tv
    case xplore
    case music
    case game
    case chatBot
    case home
    case radio
}

class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 5.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
     func generateHandleImage(with color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: self.bounds.size.height + 20, height: self.bounds.size.height + 20)
        
        return UIGraphicsImageRenderer(size: rect.size).image { (imageContext) in
            imageContext.cgContext.setFillColor(color.cgColor)
            imageContext.cgContext.fill(rect.insetBy(dx: 10, dy: 10))
        }
    }
  
}

struct NativeControlsConstants {
    static let PlaybackServicePolicyKey = "BCpkADawqM0BEo9o-jVzOLaFZImLcsQ8wfK1Wrn7oZnKQRSI3MTodHRBW1_bMtyNQGcF528Is4tuTAI7-gKZVidRRsFnkMLCMo0qRKxxOqgK5S46rLstodHeaPZixcaChp43LXqHYXwgyofy"
    static let AccountID = "5664852414001"
    static let VideoID = "3666678807001"
}

class PlayerViewController :BaseViewController,BCOVPlaybackControllerDelegate,WKYTPlayerViewDelegate,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var subPageView: UIView!
    @IBOutlet weak var subPageViewWidthConstraint: NSLayoutConstraint!
    
    let manager: BCOVPlayerSDKManager = BCOVPlayerSDKManager.shared()
    var playbackService: BCOVPlaybackService?
    var playbackController: BCOVPlaybackController?
    var fairplayProxy : BCOVFPSBrightcoveAuthProxy?
    
    /*drawer*/
    @IBOutlet weak var leftDrawer: UIView!
    
    // TODO:
    var panDirection: PanDirection = .none
    
    @IBOutlet weak var leftViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var controlView: UIView!
    var player = AVPlayer()
    var playerItem : AVPlayerItem = .init(url: URL.init(string: "a")!)
    var playerLayer = AVPlayerLayer()
    var adHelp = VideoAdHelper()
    var displayAdHelper = DisplayAdHelper()
    var currentVideo : Video?
    var CurrentMenuType : MenuTab = .tv
    var playerVMDisposeBag = DisposeBag()
    var adHelperDisposeBag = DisposeBag()
    var viewModel: PlayerViewModel!
    var isPlayingAd : Bool = false
    var engagementStarted : Bool = false
    var progressBarBeingSliding : Bool = false
    var shouldResumePlayer: Bool = false
    let ads = stride(from: 5, through: 50, by: 10).map(MidRollAd.init)
    
    
    var activeChildVC: UIViewController?
    
    
    @IBOutlet weak var controlDarkView: UIView!
    @IBOutlet weak var controlViewDismissButton: UIButton!
    
    @IBOutlet weak var controlGestureView: UIView!
    @IBOutlet weak var brightnessBtn: UIButton!
    @IBOutlet weak var volumneBtn: UIButton!
    
    @IBOutlet weak var brightnessVolumneIndicator: UIView!
    @IBOutlet weak var indicatorImage: UIImageView!
    @IBOutlet weak var indicatorLb: UILabel!
    
    @IBOutlet weak var referralButton: UIButton!
    var changingVolumne : Bool = false
    var changingBrightness : Bool = false
    
    var initialBrightness : CGFloat = 0
    var initialVolume : CGFloat = 0
    // Timers properties
    var midrollAdIntervalTimer = OONATimer()
    var midrollAdTriggerTimer = OONATimer()
    
    var engagementAdFirstIntervalTimer = OONATimer()
    var engagementAdAfterwardIntervalTimer = OONATimer()
    var engagementAdDismissTimer = OONATimer()
    
    var essentialUiOnly : Bool = false
    var menuUpEnable = true
    var menuLeftEnable = true
    var menuRightEnable = true
    var menuDownEnable = true
    var currentPlayer : PlayerType = .bco
    var currentYTState : WKYTPlayerState = .unknown
    @IBOutlet weak var youtubeView: UIView!
    @IBOutlet var youtubeViewCenter: NSLayoutConstraint!
    
    @IBOutlet weak var youtubePlayerView: WKYTPlayerView!
    @IBOutlet weak var youtubeViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var youtubeViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var youtubeLoadingGif: UIImageView!
    @IBOutlet weak var youtubeLoadingBg: UIImageView!
    
    @IBOutlet weak var youtubeViewBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var youtubeViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var playButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextButtonLeadingConstraint: NSLayoutConstraint!
    
    var hasApplyUserProfile = false
    let defaultPlayerVars : [String : Any] = [
        "controls" : 0,
        "playsinline" : 1,
        "autohide" : 1,
        "showinfo" : 0,
        "modestbranding" : 1,
        "origin" : "http://www.youtube.com"
        ]
    
    // left menu
    
    @IBOutlet weak var userProfileLeading: NSLayoutConstraint!
    @IBOutlet weak var buttonBackView: UIView!
    @IBOutlet weak var buttonBackLine: UIImageView!
    
    @IBOutlet weak var buttonBackViewTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var tcoinButton: UIButton!
    @IBOutlet weak var xploreButton: UIButton!
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var appsButton: UIButton!
    @IBOutlet weak var botButton: UIButton!
    @IBOutlet weak var tvButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var tcoinBottomButton: UIButton!
    @IBOutlet weak var gameBottomButton: UIButton!
    @IBOutlet weak var radioButton: UIButton!
    
    
    @IBOutlet weak var leftMenuScrollView: UIScrollView!
    /*user profile*/
    @IBOutlet weak var userProfileImage: UIImageView!{
        didSet {
            userProfileImage.layer.cornerRadius = userProfileImage.frame.size.height / 2
        }
    }
    @IBOutlet weak var userProfileTopLine: UILabel!
    @IBOutlet weak var userProfileBottomLine: UILabel!
    @IBOutlet weak var userProfileRightAchor: UIImageView!
    
    
    
    
    @IBOutlet weak var gameBottomButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tcoinBottomButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var searchButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var homeButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tcoinButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var xploreButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var musicButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var appsButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var botButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tvButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var settingsButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var referralButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var homeExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var tcoinExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var tvExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var xploreExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var musicExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var appsExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var botExtendWidth: NSLayoutConstraint!
    @IBOutlet weak var settingsExtendWidth: NSLayoutConstraint!
    
    @IBOutlet weak var searchExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var homeExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var tcoinExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var tvExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var xploreExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var musicExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var appsExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var botExtendHeight: NSLayoutConstraint!
    @IBOutlet weak var settingsExtendHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchExtend: UIButton!
    @IBOutlet weak var homeExtend: UIButton!
    @IBOutlet weak var tcoinExtend: UIButton!
    @IBOutlet weak var tvExtend: UIButton!
    @IBOutlet weak var xploreExtend: UIButton!
    @IBOutlet weak var musicExtend: UIButton!
    @IBOutlet weak var appsExtend: UIButton!
    @IBOutlet weak var botExtends: UIButton!
    @IBOutlet weak var settingsExtend: UIButton!
    
    
    
    @IBOutlet weak var leftMenuWidth: NSLayoutConstraint!
    

    let leftMenuWidthConstant : CGFloat =  200
    let leftMenuHalfWidth : CGFloat = 65
    
    // TCoins Tab Views
    @IBOutlet weak var tcoinTabBarView: TCoinBar!
    @IBOutlet weak var tcoinTabBlackCover: UIView!
    
    @IBOutlet weak var tcoinsDetailView: UIView! {
        didSet {
            // Add Child View Controller
            self.addChild(self.tcoinsDetailVC)
            self.tcoinsDetailVC.willMove(toParent: self)
            // Add child VC's view to parent
            self.tcoinsDetailVC.view.frame = self.tcoinsDetailView.bounds
            tcoinsDetailView.addSubview(self.tcoinsDetailVC.view)
            // Register child VC
            self.tcoinsDetailVC.didMove(toParent: self)
        }
    }
    
    @IBOutlet weak var leftMenuDetail: UIView!
    
    @IBOutlet weak var leftMenuDetailLeading: NSLayoutConstraint!
//    let leftMenuDetailDefaultLeadingValue = (65 + self.view.safeAreaInsets.left) + (self.view.frame.size.width - (65 + self.view.safeAreaInsets.left))
    @IBOutlet weak var leftMenuDetailWidth: NSLayoutConstraint!
    @IBOutlet weak var tcoinsDetailTopToSafeArea: NSLayoutConstraint!
    @IBOutlet weak var tcoinBarTop: NSLayoutConstraint!
    
    @IBOutlet weak var engagementAdButton: UIButton! {
        didSet {
//            engagementAdButton.imageView?
        }
    }
    
    @IBOutlet weak var engagementAdsMiningLabel: UILabel!
    
    @IBOutlet weak var adLoadingBg: UIImageView!{
        didSet {
            
        }
    }
    @IBOutlet weak var engagementImage: UIImageView!{
        didSet {
//            engagementImage.image = UIImage.init(named: "watchadtcoins4K.gif")
           
        }
    }
    @IBOutlet weak var engagementSpinGif: UIImageView!
        
    
    @IBOutlet weak var adLoadingGif: UIImageView!{
        didSet {
            
        }
    }
    
    @IBOutlet weak var adButtonTrailing: NSLayoutConstraint!
    @IBOutlet weak var engagementView: UIView!
    
    @IBOutlet weak var gameAdButtonLeading: NSLayoutConstraint!
    
    @IBOutlet weak var gameAdView: UIView!
    @IBOutlet weak var gameAdGifImage: UIImageView!
    
    @IBOutlet weak var gameAdsMiningLabel: UILabel!
    
    @IBOutlet weak var gameAdButton: UIButton!
    
    @IBOutlet weak var gameAdContainer: UIView!
    
    
    @IBOutlet weak var videoNameView: UITextView!
    @IBOutlet weak var videoImage: UIImageView!
    
    @IBOutlet var lowerView: UIView!
    @IBOutlet weak var lowerViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var videoImageCenterYConstraint: NSLayoutConstraint!
    var selectButton:UIButton!
    @IBAction func gameAdAction(_ sender: Any) {
        slideGameAdButton()
    }
    
    @IBAction func onClickUserProfile(_ sender: UIButton) {
        self.shouldResumePlayer = self.videoIsPlaying() ?? false
        self.pauseVideo()
        AnalyticHelper.shared.logEventToAllPlatform(name: "left_menu_item_click", parameters: ["item_name":"user_profile"])
        
        if(UserHelper.shared.isLogin()){
            self.showUserProfile { [weak helper = PlayerHelper.shared] in
//                self.leftMenuDetailLeading.constant == 0
//                if !self.menuDetailShown {
                    helper?.resumePlayer()
//                }
            }
        }else{
            self.showLogin { [weak helper = PlayerHelper.shared] in
//                self.leftMenuDetailLeading.constant == 0
//                if !self.menuDetailShown {
                helper?.resumePlayer()
//                }
            }
        }
    }
    
    private func animateAlpha(_ view: UIView?, for animationKey: String) {
        if let _view = view, _view.layer.animation(forKey: animationKey) == nil {
            let duration: Double = 0.65
            let alphaAnimation: CABasicAnimation = CABasicAnimation(keyPath: "opacity")
            alphaAnimation.fromValue = 1
            alphaAnimation.toValue = 0.25
            alphaAnimation.duration = duration
            alphaAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            alphaAnimation.repeatCount = Float.infinity
            alphaAnimation.isRemovedOnCompletion = false
            alphaAnimation.autoreverses = true
            
            _view.layer.add(alphaAnimation, forKey: animationKey)
        }
    }
    var currentSelectingFrame : CGRect = CGRect.init(x: 0, y: 0, width: 0, height: 0)
    
    func setupEssentialUIs() {
        appsButtonHeight.constant = 0
        appsButton.isHidden = true
        
        gameBottomButtonHeight.constant = 50
        gameBottomButton.isHidden = false
        
        referralButtonHeight.constant = 0
        referralButton.isHidden = true
        
        tcoinBottomButton.isHidden = false
        tcoinBottomButtonHeight.constant = 50
        
        tcoinButton.isHidden = true
        tcoinButtonHeight.constant = 0
    }
    func sendLeftMenuClickAnalytics(tag : Int) {
        var name : String?
         switch tag {
                case self.homeButton.tag:
                    name = "home"
                case self.searchButton.tag:
                   name = "search"
                case self.botButton.tag:
                    print("gg")
                    
                case self.appsButton.tag :
                   name = "games"
                    
                case self.tcoinButton.tag:
                    name = "tcoins"
                    
                case self.xploreButton.tag:
                    name = "xplore"
                    
                case self.musicButton.tag :
                    name = "music"
                case self.tvButton.tag :
                    name = "tv"
                    
                case self.settingsButton.tag:
                    name = "setting"
                case self.referralButton.tag:
                   name = "sponsor"
                case self.radioButton.tag:
                              name = "radio"
                default:
                    
                    print ("default case")
                }
        
        if (name != nil) {
           AnalyticHelper.shared.logEventToAllPlatform(name: "left_menu_item_click", parameters: ["item_name":name])
        }
       }
    @IBAction func onClickMenu(_ sender: UIButton) {
        print ("button onclick",sender.frame.origin.y)
        let player: FRadioPlayer = FRadioPlayer.shared
        if(CurrentMenuType == .radio && player.isPlaying && sender.tag != self.radioButton.tag){
            self.selectButton = sender
            showCloseRadioPopup()
            return
        }
        
        self.sendLeftMenuClickAnalytics(tag: sender.tag)
        currentSelectingFrame = sender.frame
        self.hideGameAd()
        
        let bgTop = sender.frame.origin.y ?? -55
        var geoBlockToShow : Bool = false
        if (DeviceHelper.shared.geoBlock)  {
            if (sender.tag == tvButton.tag) || (sender.tag == searchButton.tag) || (sender.tag == xploreButton.tag)  || (sender.tag == homeButton.tag) {
                geoBlockToShow = true
            }
        }
        
        if (!geoBlockToShow ) {
        switch sender.tag {
            
//        case self.botButton.tag:
//            print("gg")
        case self.settingsButton.tag:
            print("gg")
        case self.referralButton.tag:
//            self.referralButton.startShimmering()
//            self.animateAlpha(self.referralButton, for: "referralButtonAlpha")
            self.referralButton.imageView?.layer.removeAllAnimations()
            print("gg")
            
        default:
            
            self.hideLeftMenuDetail(animated: false)
            if let _avc = self.activeChildVC {
                self.detachVCFromLeftMenu(vc: _avc)
            }
            
            
            self.resetLeftMenuButtons()
            UIView.animate(withDuration: 0.3, animations: {
                self.buttonBackViewTopContraint.constant = bgTop
                self.view.layoutIfNeeded()
            }, completion: nil)
            
            sender.isSelected = true
        }
       
        self.actionWithButtonTag(tag:sender.tag)
        
        }else{
            self.showMessage(message: "This feature is not available in your country.")
        }
//        resetDrawers()
//                self.view.layoutIfNeeded()
//        let senderButton : UIButton = sender as! UIButton
//
//        if let submenuType : SubMenu = SubMenu(rawValue: senderButton.tag){
//            //self.showMenu(menuType: .left, subMenu: submenuType)
//            //show music
//
//            self.showLeftMenu(leftMenuType: .half)
//
//            UIView.animate(withDuration:0.3) {
//                self.subPageViewWidthConstraint.constant = 810
//                self.view.layoutIfNeeded()
//            }
//
//        }else{
//            print("fail to show menu!")
//        }
 
        //self.showMenu(menuType: .up, subMenu: .music,youtubeMode:(currentPlayer == .youtube) )
        
    }
    func showFullTcoinTab() {
        self.tcoinsDetailTopToSafeArea.constant = 0
        self.tcoinsDetailView.setNeedsLayout()
        UIView.animate(withDuration: 0.4,
                       animations: {
                        let bottomInset: CGFloat    = (UIDevice.current.hasNotch ? 44 : 26)
                        let halfHeight : CGFloat    = 100.0
                        let maximumHeight : CGFloat = self.view.frame.size.height - bottomInset
                        
                        // full
                        let calculatedHeight = maximumHeight
                        let nextTabBarState : TCoinTabBarDisplayState = .displayForTabDetail
                        self.tcoinsDetailVC.showTCoinDetailView()
                        self.tcoinTabBlackCover.alpha = 1
                        
                        
                        self.tcoinBarTop.constant = calculatedHeight
                        self.tcoinTabBarView.showBarForState(barState: nextTabBarState,
                                                             animated: true)
        })
    }
    
    func showHome() {
        self.showMenu(menuType: .up, subMenu: .home,youtubeMode:(currentPlayer == .youtube) )
    }
    func actionWithButtonTag(tag:Int) {
       
        GameHelper.shared.dismissGameArea.accept(true)
        switch tag {
        case self.homeButton.tag:
            CurrentMenuType = .home
            self.showHome()
        case self.searchButton.tag:
            self.showSearch()
            CurrentMenuType = .search
            MenuHelper.shared.currentSubMenu = .search
        case self.botButton.tag:
            self.showOONABot()
            
        case self.appsButton.tag :
            self.showGames()
//            CurrentMenuType = .home
//            GameHelper.shared.dismissGameArea.accept(true)
            
        case self.tcoinButton.tag:
//            self.showLeftMenu(leftMenuType: .full)
            self.showLeftMenuTcoin()
            
//            self.showDailyBid()
            
        case self.xploreButton.tag:
            self.showXplore()
            CurrentMenuType = .xplore
            MenuHelper.shared.currentSubMenu = .xplore
            
        case self.musicButton.tag :
            //self.showSwipeUpMenu()
            // self.leftMenuTab.accept(.tv)
            CurrentMenuType = .music
            self.showMenu(menuType: .up, subMenu: .music,youtubeMode:(currentPlayer == .youtube) )
        case self.tvButton.tag :
            //self.showSwipeUpMenu()
            //self.leftMenuTab.accept(.tv)
            CurrentMenuType = .tv
            self.showMenu(menuType: .up, subMenu: .tv,youtubeMode:(currentPlayer == .youtube) )
            
        case self.settingsButton.tag:
            self.pauseVideo()
            self.showSettings {
                self.resumeVideo()
            }
        case self.referralButton.tag:
//            if (UserHelper.shared.isLogin()){
                let vc : ReferralViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateInitialViewController() as! ReferralViewController
//                self.exit()
//                vc.referral()
                
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                
                PlayerHelper.shared.presentVC(vc: vc)
        case self.radioButton.tag:
                self.showRadio()
                CurrentMenuType = .radio
                MenuHelper.shared.currentSubMenu = .radio
                    
//                }
//            }else{
//                //TODO:
//                self.pauseVideo()
//                self.showLogin { [weak helper = PlayerHelper.shared] in
//                    helper?.resumePlayer()
//                }
//            }
        default:
            self.hideLeftMenu()
            self.hideLeftMenuDetail(animated: true)
            print ("default case")
        }
    }
    
    func showSearch() {
        // TODO: updateActiveChild
        self.updateActiveChild(activeChild: self.searchViewController)
        self.leftMenuDetail.bringSubviewToFront(self.searchViewController.view)
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
        print("Search clicked")
    }
    
    func showXplore() {
        // TODO: updateActiveChild
        self.updateActiveChild(activeChild: self.xploreViewController)
        self.leftMenuDetail.bringSubviewToFront(self.xploreViewController.view)
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
        
        // Sometimes API returns error, it has to be perform request again, otherwise it would be empty in category
        if self.xploreViewController.categories.count > 0 {
            DispatchQueue.once(token: "first-appear") { [weak self] in
                self?.xploreViewController.prepareForFirstAppear()
            }
        } else {
            self.xploreViewController.loadCategoryList()
        }
//        print("xploreVCView",xploreVCView.frame)
//        print("Player view", self.view.frame)
//        print("xploreVCView",self.leftDrawer.frame)
//        print("Xplore frame",self.leftMenuDetail.frame)
        print("Xplore clicked")
    }
    func showRadio() {
            // TODO: updateActiveChild
            self.updateActiveChild(activeChild: self.newRadioViewController)
        self.leftMenuDetail.bringSubviewToFront(self.newRadioViewController.view)
            self.displayLeftMenuDetail(animated: true)
            self.showLeftMenu(leftMenuType: .half)
            
            // Sometimes API returns error, it has to be perform request again, otherwise it would be empty in category
//            if self.newRadioViewController.categories.count > 0 {
//                DispatchQueue.once(token: "first-appear") { [weak self] in
//                    self?.newRadioViewController.prepareForFirstAppear()
//                }
//            } else {
                self.newRadioViewController.loadCategoryList()
//            }
    //        print("xploreVCView",xploreVCView.frame)
    //        print("Player view", self.view.frame)
    //        print("xploreVCView",self.leftDrawer.frame)
    //        print("Xplore frame",self.leftMenuDetail.frame)
            print("Radio clicked")
        }
    func showGames() {
        // TODO: updateActiveChild
        self.updateActiveChild(activeChild: self.gameViewController)
        self.leftMenuDetail.bringSubviewToFront(self.gameViewController.view)
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
                
//        PlayerHelper.shared.startGameAdTimer.accept(true)
        //        print("xploreVCView",xploreVCView.frame)
//        print("xploreVCView",self.view.frame)
//        print("xploreVCView",self.leftDrawer.frame)
//        print("game",self.leftMenuDetail.frame)
        print("Game clicked")
    }
    func showLeftMenuTcoin() {
        // TODO: updateActiveChild
        self.updateActiveChild(activeChild: self.leftMenuTcoinsDetailVC)
        self.leftMenuDetail.bringSubviewToFront(self.leftMenuTcoinsDetailVC.view)
        self.leftMenuTcoinsDetailVC.showTCoinDetailView()
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
        //        print("xploreVCView",xploreVCView.frame)
        
    }
    func showDailyBid() {
        // TODO: updateActiveChild
        self.updateActiveChild(activeChild: self.dailyBidVC)
        self.leftMenuDetail.bringSubviewToFront(self.dailyBidVC.view)
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
        //        print("xploreVCView",xploreVCView.frame)
        
    }
//    var menuDetailShown : Bool = false
    
    func showTCoinsStore(storeVC: UIViewController) {
        self.updateActiveChild(activeChild: storeVC)
        self.leftMenuDetail.bringSubviewToFront(storeVC.view)
        self.displayLeftMenuDetail(animated: true)
        self.showLeftMenu(leftMenuType: .half)
    }
    
    
    
    func displayLeftMenuDetail(animated: Bool) {
        self.pauseVideo()
        self.leftMenuDetail.alpha = 1
        if animated {
                print("Will set menuDetailShown to true")
                UIView.animate(withDuration: 0.4, animations: {
//                    self.leftMenuDetail.alpha = 1
                    self.leftMenuDetailLeading.constant = 0
                    self.view.layoutIfNeeded()
                    print("Setting menuDetailShown to true")
                }) { [weak self] (complete) in
                    
                    print("menuDetailShown = true")
//                    self?.menuDetailShown = true
                    
                }
            
        }else {
//            self.leftMenuDetail.alpha = 1
            //            menuDetailShown = true
        }
    }
    
    func hideLeftMenuDetail(animated: Bool) {
        
        if animated {
            print("Will set menuDetailShown to false")
                UIView.animate(withDuration: 0.4,
                               animations: {
                                self.leftMenuDetailLeading.constant = self.leftMenuWidthConstant+self.view.frame.size.width
                                self.view.layoutIfNeeded()
                                
                                print("Setting menuDetailShown to false")
                }) { [weak self] (complete) in
//                    self?.menuDetailShown = false
                    self?.leftMenuDetail.alpha = 0
                // TODO:
//                if let checkedSelf = self {
//                    if checkedSelf.leftMenuTab.value == .none {
//                        checkedSelf.updateActiveChild(activeChild: nil)
//                    }
//                }
                }
        }
        else {
            self.leftMenuDetailLeading.constant = self.leftMenuWidthConstant+self.view.frame.size.width
                                           self.view.layoutIfNeeded()
            self.leftMenuDetail.alpha = 0
            self.resumeVideo()
        }
        self.leftMenuDetail.endEditing(true)
    }
    
    func menuDetailShown() -> Bool {
        return self.leftMenuDetailLeading.constant == 0
    }
    
    func showOONABot() {
//        self.playTutorial(tutorial: .earnTcoin)
//        self.showChatbotView()
    }
    
    func hideTCoinView() {
        self.tcoinTabBarView.hideTabBar()
        self.tcoinsDetailVC.resetTCoinView()
        UIView.animate(withDuration: 0.3) {
            self.tcoinBarTop.constant = 0
            self.tcoinTabBlackCover.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func controlViewTapAction(_ sender: Any) {
        if (currentPlayer == .youtube){
        if currentYTState == .paused { self.youtubePlayerView?.playVideo() }
        }
        hideControlView()
    }
    
    @IBOutlet weak var adLoadingBgLeading: NSLayoutConstraint!{
        didSet {
//            adLoadingBgLeading.constant = self.view.safeAreaInsets.left
        }
    }
    
    func hideControlView(){
//        self.hideSystemDialog()
        DispatchQueue.main.async {
          
        UIView.animate(withDuration: 0.5) {
//            self.leftViewWidth.constant = 0
            self.controlView.alpha = 0
            self.hideTCoinView()
            self.view.layoutIfNeeded()
        }
            self.hideLeftMenu()
        }
        self.resetYoutubeViewConstraints()
    }
    
    func resetDrawers(){
        HideAllExtendButtons()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() +) {
            print("currentSelectingFrame",self.currentSelectingFrame)
            self.leftMenuScrollView.scrollRectToVisible(self.currentSelectingFrame, animated: true)
//        }
        
        
        UIView.animate(withDuration: 0.2) {
            self.leftViewWidth.constant = 0
            
            self.hideProfileView()
            self.hideTCoinView()
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
    }
    
    func showControlView(){
        
//        self.showSystemDialog()
        DispatchQueue.main.async {
          
            UIView.animate(withDuration: 0.4) {
                self.youtubeViewTopConstraint.constant = 30
                self.youtubeViewBottonConstraint.constant = self.controlDarkView.frame.size.height - self.view.safeAreaInsets.bottom
                self.view.layoutIfNeeded()
            }
            UIView.animate(withDuration: 0.4) {
                self.controlView.alpha = 1
                self.tcoinTabBarView.showBarForState(barState: .displayForControlView,
                                                     animated: true)
            }
            
        }
    }
    
    @IBAction func playerViewTapAction(_ sender: Any) {
    
        if (self.leftMenuDetail.alpha > 0) {
            print("self.leftMenuDetail.alpha > 0")
            return
        }
        
        if (self.leftViewWidth.constant != 0) {
            print("width not reset")
            
            resetDrawers()
            resetPanDirection()
            return
        }
        if (self.tcoinBarTop.constant != 0 ) {
            print("menu is showing")
            resetDrawers()
            resetPanDirection()
            
            return
        }
        if (self.menuIsShowing ) {
            print("menu is showing")
            resetDrawers()
            resetPanDirection()
            return
        }
        
        initView()
        showControlView()
    }
    

    @IBOutlet weak var background: UIButton!{
        didSet {
//            let swipeGesture = UISwipeGestureRecognizer.init
        }
    }
    
    @IBOutlet weak var videoTypeTextView: UITextView!{
        didSet {
            self.videoTypeTextView.textContainerInset = UIEdgeInsets.init(top: 2, left: 0, bottom: 2, right: 0)
            self.videoTypeTextView.layer.cornerRadius = 3.0
        }
    }
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func playBtnTap(_ sender: Any) {
        print(#function)
        if currentPlayer == .bco {
            DispatchQueue.main.async {
                switch self.player.timeControlStatus {
                case .playing:
                    self.player.pause()
                    
                case .paused:
                    self.player.play()
                
                default :
                    self.player.play()
    //                print("do nothing when player is not in correct state")
                }
               
            }
        }
        if currentPlayer == .youtube {
            switch self.currentYTState {
            case .playing:
                self.youtubePlayerView.pauseVideo()
            default:
                self.youtubePlayerView.playVideo()
        
            }
        }
    }
    
    @IBAction func previousBtnTap(_ sender: Any) {
        print(#function)
        self.playPreviousVideo()
    }
    
    @IBAction func nextBtnTap(_ sender: Any) {
        print(#function)
        self.playNextVideo()
    }
    
//    @IBOutlet weak var lb: UILabel!
    var initialY : CGFloat = 0.0  // The initial center point of the view.
    var initialX : CGFloat = 0  // The initial center point of the view.
    @IBOutlet weak var progressSlider: UISlider!{
        didSet {
       
            progressSlider.setThumbImage(UIImage(named:"sliderThumb"), for: .normal)
            progressSlider.setThumbImage(UIImage(named:"sliderThumb"), for: .highlighted)
//
//            progressSlider.transform = CGAffineTransform(scaleX: 1, y: 0.33)
            // Add a gesture recognizer to the slider
//            let tapGestureRecognizer = UITapGestureRecognizer(target: progressSlider, action: #selector(sliderTapped(gestureRecognizer:)))
//            self.addGestureRecognizer(tapGestureRecognizer)
            
        }
    }
 


    func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        print(#function)
        //  print("A")
        
//        let pointTapped: CGPoint = gestureRecognizer.location(in: self.progressSlider)
//
//        let positionOfSlider: CGPoint = self.progressSlider.frame.origin
//        let widthOfSlider: CGFloat = self.progressSlider.frame.size.width
//        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.progressSlider.maximumValue) / widthOfSlider)
//
//        self.progressSlider.setValue(Float(newValue), animated: true)
//        if (self.currentPlayer == .youtube){
//
//            let targetSecond = self.youtubePlayerView.duration() * Double(newValue)
//            self.youtubePlayerView.seek(toSeconds: Float(targetSecond), allowSeekAhead: true)
//
//            self.youtubePlayerView.playVideo()
//        }else{
//
//            let targetSecond = self.player.currentItem!.duration.seconds * Double(newValue)
//
//            self.player.seek(to:CMTime.init(seconds: targetSecond, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero, completionHandler: { (completed) in
//                self.player.play()
//            })
//
//        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //if(showToturial()){
        //}
        
        if(!hasApplyUserProfile){
            if UserHelper.shared.currentUser.isLogin{
                let _user = UserHelper.shared.currentUser
                self.handleLogin(user: _user)
            }
            
            hasApplyUserProfile = true
        }
        
        if !UIDevice.current.hasNotch {
            // This part handle devices does not contain safearea
            self.updateSafeAreaConstraints()
        }
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        // This part handle devices containt safearea
       self.updateSafeAreaConstraints()
    }
    
    func updateSafeAreaConstraints() {
        DispatchQueue.once(token: "viewInit") {
            self.view.setNeedsLayout()
            self.buttonBackViewTopContraint.constant = self.tvButton.frame.origin.y
            self.leftMenuDetailLeading.constant = self.leftMenuWidthConstant+self.view.frame.size.width
            self.leftMenuDetailWidth.constant = self.view.frame.size.width - self.leftMenuHalfWidth - self.view.safeAreaInsets.left
            
            
            self.view.layoutIfNeeded()
            self.xploreViewController.view.frame = self.leftMenuDetail.bounds
            self.gameViewController.view.frame = self.leftMenuDetail.bounds
            self.searchViewController.view.frame = self.leftMenuDetail.bounds
            self.dailyBidVC.view.frame = self.leftMenuDetail.bounds
            self.leftMenuTcoinsDetailVC.view.frame = self.leftMenuDetail.bounds
            self.hideControlView()
            self.initView()
            
            self.refreshUserDatas()
        }
    }
    var isFirstVideo = true
    func updategif(){
    
    }
    func autoStartPlayVideo() {
        
        //first time to load the app.. play stored episode /channel
        
        var hasPlayVideo = false
        
        if let _channel = ChannelHelper.shared.getCurrentChannelFromStorage(){
            if let _episode = EpisodeHelper.shared.getCurrentEpisodeFromStorage(){
                
                EpisodeHelper.shared.getEpisodeList(open: true, channelId: _channel.id.description, selectedEpisodeId: _episode.id.description, offset: 0, success: { response in
                    let episodes = response
                    //self.episodeList = episodes
                    
                    episodes.forEach({ (episode) in
                        if(episode.id  == _episode.id){
                            EpisodeHelper.shared.setCurrentEpisode(episode: episode)
                            if let _video = episode.video{
                                EpisodeHelper.shared.updateVideoByStreamingInfo(video:_video, channel: _channel, episode: episode, completion: { (success, __video, error) in
                                    guard let _ = __video else {
                                        print("updateVideoByStreamingInfo unsuccess")
                                        return }
                                    print("getVideoByChannel")
                                    PlayerHelper.shared.currentVideo = __video
                                    self.playVideo(video: __video!)
                                    
//                                    self.afterAutoStartVideo()
                                    
                                })
                                hasPlayVideo = true
                                
                            }
                        }
                    })
                    
                    
                    if(!hasPlayVideo){
                        self.playVideoForFirstLoad()
                    }
                    
                    
                }, failure: { error in // failure(error)
                    
                    self.playVideoForFirstLoad()
                })
                
                
            }else{
                
                let categoryId : Int? =  CategoryHelper.shared.getCurrentCategoryIdFromStorage()
                
                var categoryIdstr : String? = nil
                var channelType = ChannelSetting.channelListFilterType.none
                if let _categoryId = categoryId?.description{
                    categoryIdstr = _categoryId
                    channelType = ChannelSetting.channelListFilterType.category
                }
                
                ChannelHelper.shared.getChannelList(open: false, categoryId : categoryIdstr, currentChannelId: _channel.id.description, filterType: channelType, offset: 0, sortBy: nil, success: { channels in
                    
                    channels.forEach({ (channel) in
                        if(channel.id  == _channel.id){
                            
                            ChannelHelper.shared.setCurrentChannel(channel: channel)
                            
                            if let _video = channel.video{
                                ChannelHelper.shared.updateVideoByStreamingInfo(video:_video, channel: channel, completion: { (success, __video, error) in
                                    print("getVideoByChannel")
                                    PlayerHelper.shared.currentVideo = __video
//                                    var controls: [VideoControl] = [.Default]
//                                    if episodes.count > 0 {
//                                        controls.append(contentsOf: [.Previous, .Next])
//                                        if index == 0 {
//                                            controls.append(.PreviousDim)
//                                        } else if index == episodes.count - 1 {
//                                            controls.append(.NextDim)
//                                        }
//                                    }
//                                    PlayerHelper.shared.videoControls.accept(controls)
                                    
                                    self.playVideo(video: __video!)
                                })
                                hasPlayVideo = true
                            }
                            
                        }
                    })
                    
                    if(!hasPlayVideo){
                        print("no video found , channel id : "+_channel.id.description)
                        self.playVideoForFirstLoad()
                    }
                    
                }, failure: { _,_ in
                    self.playVideoForFirstLoad()
                    
                })
            }
        }else{
            
            self.playVideoForFirstLoad()
        }
        
        
        
        
            //if(!hasPlayVideo){
            //    self.playVideoForFirstLoad()
            //}
            
        //}else{
        //    self.playVideoForFirstLoad()
        //}
        
    }
    func snapshot(_ view: UIView?) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view?.bounds.size ?? CGSize.zero, _: true, _: 0)
        view?.drawHierarchy(in: view?.bounds ?? CGRect.zero, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
//        return UIImage.init(named: "tcoins_store_bg")
        return image
    }
    func snapFromYoutube()  {
        if #available(iOS 11.0, *) {
            self.youtubePlayerView.webView!.takeSnapshot(with: nil) { (image, error) in
                //Do your stuff with image
                PlayerHelper.shared.receivedSnap.accept(image)
            }
        }
//        self.youtubePlayerView.webView!.swContentCapture { (capturedImage) -> Void in
//            // capturedImage is a UIImage.
//
//        }
//
        
    }

    func playVideoForFirstLoad(){
        
        print("playVideoForFirstLoad")
        //first time to load the app..
        ChannelHelper.shared.getFirstChannelByCategory(categoryId: nil, filterType: .none, success: {
            channel in
            if let video = channel.video{
                
                /*set back the parameters to helpers*/
                ChannelHelper.shared.setCurrentChannel(channel: channel)
                
                ChannelHelper.shared.updateVideoByStreamingInfo(video:video, channel: channel, completion: { (success, _video, error) in
                    print("getVideoByChannel")
                    PlayerHelper.shared.currentVideo = _video
                    self.playVideo(video: _video!)
                })
                //                self.playVideo(video: _video)
                //get first channel
            }else{
                
                EpisodeHelper.shared.getFirstEpisodeListByChannel(channel:channel, success: { episode in
                    if let video = episode.video{
                        
                        /*set back the parameters to helpers*/
                        //ChannelHelper.shared.currentChannel = channel
                        //EpisodeHelper.shared.currentEpisode = episode
                        EpisodeHelper.shared.setCurrentEpisode(episode: episode)
                        ChannelHelper.shared.setCurrentChannel(channel: channel)
                        
                        //                        PlayerHelper.shared.currentVideo = _video
                        EpisodeHelper.shared.updateVideoByStreamingInfo(video:video, channel: channel, episode: episode, completion: { (success, _video, error) in
                            print("getVideoByEpisode")
                            PlayerHelper.shared.currentVideo = _video
                            self.playVideo(video: _video!)
                        })
                        //if(self.playPreviewWithVideo(video: _preview)){
                        
                        //get first episode
                    }
                }, failure: { error in
                    //fail handling
                    print("no episode!")
                })
                
            }
        }, failure:  { error in
            //fail handling
            print("no channel!")
        })
    }
    
    func playPreviousVideo() {
        
        if let _currentChannel = ChannelHelper.shared.currentChannel {
            self.previousButton.isEnabled = false
            if (ChannelHelper.shared.currentChannelList == nil){
                //yea .. need to fetch to list if not yet loaded into the menu
                ChannelHelper.shared.getChannelList(open: true, categoryId: nil, currentChannelId: _currentChannel.id.description, filterType: ChannelSetting.channelListFilterType.none, offset: 0, sortBy: nil, success: { channels in }, failure: { code,error  in })
            }
            
            if (EpisodeHelper.shared.currentEpisodeList == nil){
                //yea .. need to fetch to list if not yet loaded into the menu
                
                EpisodeHelper.shared.getEpisodeList(open: true, channelId: _currentChannel.id.description, selectedEpisodeId: nil, offset: 0, success: { response in }, failure: {error in })
                
            }
            
            
            EpisodeHelper.shared.getPreviousEpisodeListByChannel(channel:_currentChannel, success: { episode, index, episodes in
                if let _video = episode.video{
                    
                    /*set back the parameters to helpers*/
                    //ChannelHelper.shared.currentChannel = _currentChannel
                    // EpisodeHelper.shared.currentEpisode = episode
                    
                    EpisodeHelper.shared.setCurrentEpisode(episode: episode)
                    ChannelHelper.shared.setCurrentChannel(channel: _currentChannel)
                    
                    PlayerHelper.shared.currentVideo = _video
                    var controls: [VideoControl] = [.Default]
                    if episodes.count > 0 {
                        controls.append(contentsOf: [.Previous, .Next])
                        if index == 0 {
                            controls.append(.PreviousDim)
                        } else if index == episodes.count - 1 {
                            controls.append(.NextDim)
                        }
                    }
                    PlayerHelper.shared.videoControls.accept(controls)
                    
                    //if(self.playPreviewWithVideo(video: _preview)){
                    self.playVideo(video: _video)
                    //get first episode
                }
            }, failure: { [weak self] error in
                //fail handling maybe it is a LIVE
                
                PlayerHelper.shared.videoControls.accept([.Default])
//                ChannelHelper.shared.getNextChannelByCategory(categoryId: nil, filterType: .none, success: { channel in
//
//                    EpisodeHelper.shared.getFirstEpisodeListByChannel(channel:channel, success: { episode in
//                        if let _video = episode.video{
//
//                            /*set back the parameters to helpers*/
//                            //ChannelHelper.shared.currentChannel = channel
//                            //EpisodeHelper.shared.currentEpisode = episode
//
//                            EpisodeHelper.shared.setCurrentEpisode(episode: episode)
//                            ChannelHelper.shared.setCurrentChannel(channel: channel)
//
//                            PlayerHelper.shared.currentVideo = _video
//
//                            //if(self.playPreviewWithVideo(video: _preview)){
//                            self.playVideo(video: _video)
//                            //get first episode
//                        }
//                    }, failure: { error in
//                        //fail handling
//                        print("no episode!")
//                    })
//                }, failure: { error in })
//
            })
        }else{
            print("no current channel!")
        }
    }
    
    func playNextVideo() {
        //self.playVideo(url: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8")
        
        //should not go inside the vc, change later.. TODO_OONA
        if let _currentChannel = ChannelHelper.shared.currentChannel {
            self.nextButton.isEnabled = false
            if (ChannelHelper.shared.currentChannelList == nil){
                //yea .. need to fetch to list if not yet loaded into the menu
                ChannelHelper.shared.getChannelList(open: true, categoryId: nil, currentChannelId: _currentChannel.id.description, filterType: ChannelSetting.channelListFilterType.none, offset: 0, sortBy: nil, success: { channels in }, failure: { code,error  in })
            }
            
            if (EpisodeHelper.shared.currentEpisodeList == nil){
                //yea .. need to fetch to list if not yet loaded into the menu
                
                EpisodeHelper.shared.getEpisodeList(open: true, channelId: _currentChannel.id.description, selectedEpisodeId: nil, offset: 0, success: { response in }, failure: {error in })
                
            }
            
            
            EpisodeHelper.shared.getNextEpisodeListByChannel(channel:_currentChannel, success: { episode, index, episodes in
                if let _video = episode.video{
                    
                    /*set back the parameters to helpers*/
                    //ChannelHelper.shared.currentChannel = _currentChannel
                   // EpisodeHelper.shared.currentEpisode = episode
                    _video.videoName = episode.name
                    EpisodeHelper.shared.setCurrentEpisode(episode: episode)
                    ChannelHelper.shared.setCurrentChannel(channel: _currentChannel)
                    
                    PlayerHelper.shared.currentVideo = _video
                    var controls: [VideoControl] = [.Default]
                    if episodes.count > 0 {
                        controls.append(contentsOf: [.Previous, .Next])
                        if index == 0 {
                            controls.append(.PreviousDim)
                        } else if index == episodes.count - 1 {
                            controls.append(.NextDim)
                        }
                    }
                    PlayerHelper.shared.videoControls.accept(controls)
                    //if(self.playPreviewWithVideo(video: _preview)){
                    self.playVideo(video: _video)
                    //get first episode
                }
            }, failure: { error in
                //fail handling maybe it is a LIVE
                
                PlayerHelper.shared.videoControls.accept([.Default])
//                ChannelHelper.shared.getNextChannelByCategory(categoryId: nil, filterType: .none, success: { channel in
//
//                    EpisodeHelper.shared.getFirstEpisodeListByChannel(channel:channel, success: { episode in
//                        if let _video = episode.video{
//
//                            /*set back the parameters to helpers*/
//                            //ChannelHelper.shared.currentChannel = channel
//                            //EpisodeHelper.shared.currentEpisode = episode
//
//                            EpisodeHelper.shared.setCurrentEpisode(episode: episode)
//                            ChannelHelper.shared.setCurrentChannel(channel: channel)
//
//                            PlayerHelper.shared.currentVideo = _video
//
//                            //if(self.playPreviewWithVideo(video: _preview)){
//                            self.playVideo(video: _video)
//                            //get first episode
//                        }
//                    }, failure: { error in
//                        //fail handling
//                        print("no episode!")
//                    })
//                }, failure: { error in })
                
            })
            }else{
                print("no current channel!")
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationName = Notification.Name(rawValue: "RadioMoveToOtherPage")
        var _ = NotificationCenter.default.rx
            .notification(notificationName)
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: { notification in
                
            self.onClickMenu(self.selectButton)
            self.selectButton = nil
            })
        self.loadViewIfNeeded()
        
        self.setEngagementAdVisible()
        /*UserHelper.shared.getSavedUserInfo(success: { userProfile in
            print("willApplyUserProfile")
            self.willApplyUserProfile = true
            
        }, failure: {
            error,message  in
            
        })
        */
        self.initYoutubeView()
        self.initPlayerView()
        self.initBcoPlayerView()
        self.initLeftMenuStuff()
//        self.autoStartPlayVideo()
        bindSlider()
        //        self.engagementAdButton.isHidden = true
        //        self.engagementImage.isHidden = true
        self.engagementView.isHidden = true
        self.appsButton.isSelected = true
        self.previousButton.isEnabled = false
        self.nextButton.isEnabled = false
        self.controlView.alpha = 0
        self.leftMenuDetail.alpha = 0
        self.hideTCoinView()
        self.view.layoutIfNeeded()
        //        self.tcoinsDetailTopToSafeArea.constant = -5
        
        //        tcoinGif.animate(withGIFNamed: "tcoins-spinonly-1time")
        if !UIDevice.current.hasNotch {
            self.videoImageCenterYConstraint.constant = 0
        }
        
        DispatchQueue.once(token: "gifset") {
            do {
                let gif = try UIImage(gifName: "tcoins-spin", levelOfIntegrity:0.7)
                engagementSpinGif.setGifImage(gif)
                
            } catch  {
                print(error)
            }
            do {
                let gif = try UIImage(gifName: "tcoins-spin", levelOfIntegrity:0.7)
                gameAdGifImage.setGifImage(gif)
                
            } catch  {
                print(error)
            }
            do {
                let gif = try UIImage(gifName: "minetcoins1", levelOfIntegrity:0.7)
                adLoadingGif.setGifImage(gif)
            } catch  {
                print(error)
            }
            //            do {
            //                let gif = try UIImage(gifName: "LoadingCircle2x", levelOfIntegrity:1.0)
            //                youtubeLoadingGif.setGifImage(gif)
            //            } catch  {
            //                print(error)
            //            }
            //            youtubeLoadingGif.animate(withGIFNamed: "LoadingCircle2x") {
            //                print("It's animating!")
            //            }
//            DispatchQueue.global(qos: .userInteractive).async(execute: { [weak self] in
//                var images: [UIImage] = [UIImage]()
//                for i in 1...8 {
//                    if let image = UIImage(named: "youtube_loading_\(i)") { images.append(image) }
//                } // use of #imageLiterals here
//                let animation = UIImage.animatedImage(with: images, duration: 3)
//                DispatchQueue.main.async(execute: {
//                    self?.youtubeLoadingGif.image = animation
//                })
//            })
            youtubeLoadingGif.backgroundColor = .black
        }
        
        
        
        // TODO:
        //        self.leftMenuTab.subscribe { [weak self] (menuTab) in
        //            self?.actionsForLeftMenuTab(menuTab: menuTab)
        //        }.disposed(by: self.disposeBag)
        
        NSLog("\(DeviceHelper.shared.getCurrentTimeStamp())")
        //        self.playVideo(url: "https://i.imgur.com/9rGrj10.mp4")
        
        // Language.
    
        
        
        NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification).skip(1).subscribe(onNext: { [weak self] (_) in
            self?.animateAlpha(self?.referralButton.imageView,
                               for: "referralButtonAlpha")
        }).disposed(by: self.disposeBag)
        //switch channel
        PlayerHelper.shared.playVideo.subscribe(onNext: { [weak self] (video) in
            print("menu vc play new vdo helper sub ")
            //            self.playerStartPlaying()
            self?.updateActiveChild(activeChild: nil)
            guard let _video = video else { return }
            print("video name:",_video.videoName)
            
            print("ch name:",_video.channelName)
            self?.playVideo(video: _video)
        }).disposed(by: self.disposeBag)
        
        PlayerHelper.shared.videoControls.subscribe(onNext: { [weak self] (videoControl) in
            self?.updatePlayerControlButtons(for: videoControl)
        }).disposed(by: self.disposeBag)
        
        PlayerHelper.shared.showChatbot.subscribe(onNext: { [weak self] (question) in
            self?.showChatbotView(question: question)
            
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showXplore.subscribe(onNext: { [weak self] (todo) in
            self?.CurrentMenuType = .xplore
            MenuHelper.shared.currentSubMenu = .xplore
                      
            self?.showXplore()
            
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.exitTVMenu.subscribe(onNext: { [weak self] (todo) in
            
                      
            self?.hideLeftMenuDetail(animated: true)
            self?.hideLeftMenu()
            
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showGames.subscribe(onNext: { [weak self] (todo) in
                          
                                    
                          self?.showGames()
                          
                      }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showTcoinHome.subscribe(onNext: { [weak self] (todo) in
                   
                             
                   self?.showLeftMenuTcoin()
                   
               }).disposed(by: self.disposeBag)
        //showLeftMenuTcoines
        //showSystemMessage
         //showBidPage
         PlayerHelper.shared.showBidPage.subscribe(onNext: { [weak self] (message) in

            self?.showDailyBid()
            }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showSystemMessage.subscribe(onNext: { [weak self] (message) in
//            self?.tcoinRewardVideo(rewardAmount: rewardVideo)
            self?.showSystemDialog(text: message, dismissDelay: 5)
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showHTMLSystemMessage.subscribe(onNext: { [weak self] (message) in
            //            self?.tcoinRewardVideo(rewardAmount: rewardVideo)
            self?.showSystemDialog(html: message, dismissDelay: 5)
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showRewardVideo.subscribe(onNext: { [weak self] (rewardVideo) in
            self?.tcoinRewardVideo(rewardAmount: rewardVideo)
            
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showTutorialVideo.subscribe(onNext: { [weak self] (tutorialVideo) in
            self?.playTutorial(rewardUI: false, tutorial: tutorialVideo, completion: nil)
            
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.playTutorial.subscribe(onNext: { [weak self] (tutorialId) in
//            self?.showChatbotView()
//            self?.playTutorial(tutorial: tutorial)
            print("playTutorial sub",tutorialId)
            var tutToPlay : Tutorial = .firstTimeBumper
            var tutResponse = "yes"
            if ("1" == tutorialId) {
                tutToPlay = .spendingTcoinTutorial
            }
            if ("2" == tutorialId) {
                tutToPlay = .firstTimeBumper
            }
            if ("3" == tutorialId) {
                tutToPlay = .sponsorExplainer
                tutResponse = "no"
            }
            if ("4" == tutorialId) {
                tutToPlay = .sponsorExplainer
                tutResponse = "yes"
            }
            self?.playTutorial(rewardUI:true, tutorial: tutToPlay, completion: { (tutorial) in
                //
                
                
                    let name = "tutorial-watched"
//                    let tutRes : String =  "yes"
                
                    let param = ["tutorialId":tutorialId,
                                  "tutorialResponse":tutResponse]
                    //        self.totalTcoinEarned = 0
                let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)
                
                if (tutorial != nil) {
                OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam!)
                        .responseJSON { (response) in
                            switch response.result {
                                
                            case .success(let value):
                                let json = JSON(value)
                                if let code = json["code"].int,
                                    let status = json["status"].string {
                                    APIResponseCode.checkResponse(withCode: code.description,
                                                                  withStatus: status,
                                                                  completion: { (message, isSuccess) in
                                                                    if(isSuccess){
                                                                        if ("2" == tutorialId) {
                                                                            
                                                                            UserDefaults.standard.set(true, forKey: "no_need_show_adIgnore_chatbot")
                                                                            self?.tcoinRewardVideo(rewardAmount: .Award250)
                                                                        }
                                                                        if ("1" == tutorialId) {
                                                                            
                                                                            UserDefaults.standard.set(2, forKey: "game_tutorial_stage")
                                                                            self?.tcoinRewardVideo(rewardAmount: .Award250)
                                                                            
                                                                        }
                                                                        if ("3" == tutorialId) {
                                                                            
//                                                                            UserDefaults.standard.set(2, forKey: "game_tutorial_stage")
                                                                            self?.tcoinRewardVideo(rewardAmount: .Award500)
                                                                        }
                                                                        if ("4" == tutorialId) {
                                                                            
//                                                                            UserDefaults.standard.set(2, forKey: "game_tutorial_stage")
                                                                            self?.tcoinRewardVideo(rewardAmount: .Award500)
                                                                        }
                                                                    } else {
                                                                        
                                                                    }
                                                                    
                                    })
                                }
                            case .failure(let error):
                                print(error)
                                
                                
                                
                                
                            }
                    }
                
                
                }
                
                
            })
            
        }).disposed(by: self.disposeBag)
        
        
        PlayerHelper.shared.presentVC.subscribe(onNext: { [weak self] (vc) in
            print("play vdo helper present vc sub")
            self?.shouldResumePlayer = self?.videoIsPlaying() ?? false
            self?.pauseVideo()
            if (vc.isKind(of: TCoinsDetailViewController.self)){
                self?.showLeftMenuTcoin()
                //                self?.hideTCoinView()
            }else if (vc.isKind(of: DailyBidViewController.self)){
                self?.hideTCoinView()
                self?.showDailyBid()
            }else if (vc.isKind(of: TCoinsStoreViewController.self)) {
                self?.hideTCoinView()
                self?.showTCoinsStore(storeVC: vc)
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.modalTransitionStyle = .crossDissolve
                    // Nicholas 5-Sep-2019: unknown reason cause same address of vc presented more than one time and leads to crash
                    if vc !== self?.presentedViewController {
                        self?.present(vc, animated: true, completion: nil)
                    }
                })
            }
            
        }).disposed(by: self.disposeBag)
        
        XploreHelper.shared.selectedVideo.subscribe(onNext: { [weak self] (selectedVideo) in
            if let video = selectedVideo {
                PlayerHelper.shared.playVideo(video: video)
                PlayerHelper.shared.videoControls.accept([.Default])
            }
            self?.hideLeftMenuDetail(animated: true)
            self?.hideLeftMenu()
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.selectedVideo.subscribe(onNext: { [weak self] (selectedVideo) in
            if let video = selectedVideo {
                PlayerHelper.shared.videoControls.accept([.Default])
                PlayerHelper.shared.playVideo(video: video)
            }
            self?.hideLeftMenuDetail(animated: true)
            self?.hideLeftMenu()
        }).disposed(by: self.disposeBag)
        PlayerHelper.shared.showGameAd.subscribe(onNext: { [weak self] (todo) in
           self?.showGameAd()
        }).disposed(by: disposeBag)
        PlayerHelper.shared.hideGameAd.subscribe(onNext: { [weak self] (todo) in
            self?.hideGameAd()
        }).disposed(by: disposeBag)
        
        PlayerHelper.shared.resetYoutubeView.subscribe(onNext: { [weak self] (todo) in
            print("resetYoutubeView sub")
            //            self.present(vc, animated: true, completion: nil)
            self?.setEngagementAdVisible()
            self?.resetYoutubeViewConstraints()
            if (self?.currentPlayer == .youtube){
                self?.youtubePlayerView.playVideo()
            }
            if (self?.currentPlayer == .bco) {
                self?.player.play()
            }
            self?.engagementAdFirstIntervalTimer.resumeTimer()
            self?.engagementAdAfterwardIntervalTimer.resumeTimer()
            self?.engagementAdFirstIntervalTimer.resumeTimer()
        }).disposed(by: disposeBag)
        PlayerHelper.shared.resumeVideo.subscribe(onNext: { [weak self] (resume) in
            if resume {
                self?.resumeVideo()
            } else {
                self?.pauseVideo()
            }
        }).disposed(by: disposeBag)
        
        PlayerHelper.shared.requestSnap.subscribe(onNext: { [weak self] (todo) in
            self?.snapFromYoutube()
            print("playervc request snap")
        }).disposed(by: disposeBag)
        
        UserHelper.shared.userLogin.subscribe(onNext: { [weak self] (user) in
            print("handle user login ")
            if let _user = user{
                self?.handleLogin(user: _user)
            }
        }).disposed(by: disposeBag)
        UserHelper.shared.userLogout.subscribe(onNext: { [weak self] (user) in
            print("handle user logout ")
            self?.handleLogout()
        }).disposed(by: disposeBag)
        GameHelper.shared.rewardReceived.subscribe(onNext: { [weak self] (reward) in
            
            //            self.playerStartPlaying()
            print("show tcoin viewx")
            guard let _reward = reward else { return }
            self?.gettcoinView(addedTcoin: _reward.points, totalTcoin: _reward.balance)
        }).disposed(by: self.disposeBag)
        essentialUiOnly = DeviceHelper.shared.isEssentialUiOnly
        if (essentialUiOnly) {
            setupEssentialUIs()
            DispatchQueue.main.asyncAfter(deadline: .now()+1 ) {
//                           self.()
                self.CurrentMenuType = .tv
                self.showMenu(menuType: .up, subMenu: .tv,youtubeMode:(self.currentPlayer == .youtube) )
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now()+1 ) {
                self.showGames()
            }
        }
        
        self.checkNeedShowNotice()
        
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 2) {
            self.initGestures()
        }
        
    }
    func checkNeedShowNotice() {
        
        
        if UserDefaults.standard.bool(forKey: "notice_need_show")  {
            print("need show banner")
            UserDefaults.standard.set(false, forKey: "notice_need_show")
              let vc: NoticeViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "NoticeViewController") as! NoticeViewController
            let noticeJSON = JSON(UserDefaults.standard.object(forKey: "notice_obj"))
            if let _notice = noticeJSON.array?[0] {
                vc.showNotice(notice: _notice)

                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                    
                    PlayerHelper.shared.presentVC(vc: vc)
                    
                }
            }
        } else {
            print("no need show banner")
        }

        
        
      
    }
    override func viewWillAppear(_ animated: Bool) {
//        NetworkModule.instance()?.registerDevice(success: { (task, error) in
//            print(task?.response)
//        }, failure: { (task, error) in
//            print("reg error")
//            print(task?.response)
//        })r
        
        self.animateAlpha(self.referralButton.imageView, for: "referralButtonAlpha")
    }
    
    override func updateUserViews() {
        tcoinTabBarView.updateTcoinAmount(amount:  UserHelper.shared.currentUser.profile?.myPointBalance ?? 0)
        self.setupUserView(user: UserHelper.shared.currentUser)
        
    }
    
    func showChatbotView(question:Question) {
        
        let view :ChatbotView = Bundle.main.loadNibNamed("ChatbotView", owner:self, options:nil)![0] as! ChatbotView
        view.currentQuestion = question
        view.startChatbot()
        view.frame = CGRect(x: self.view.frame.size.width/2 * -1, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(view)
        UIView.animate(withDuration: 0.3, animations: {
            view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.youtubeViewLeftConstraint.constant = self.view.frame.size.width/2 - self.view.safeAreaInsets.left
        }) { (done) in
            
        }
//        self.view.addSubview(view)
    }
  
    func showSwipeUpMenu() {
//        if (currentPlayer == .bco) {
        
//        }
        // .tv
//        self.setEngagementAdInvisible()
        if(self.menuUpEnable){
            
            if (self.tcoinBarTop.constant == 0){
                //check if the scroll direction is enable
                NSLayoutConstraint.deactivate([self.youtubeViewCenter])
                self.view.layoutIfNeeded()
                self.initView()
                self.pauseVideo()
                let subMenu : SubMenu = MenuHelper.shared.getCurrentSubMenu()
                
                self.showMenu(menuType: .up, subMenu: subMenu ,youtubeMode:(self.currentPlayer == .youtube) )
                self.resetPanDirection()
                
            }
        }
    }
    
    func resetYoutubeViewConstraints() {
        UIView.animate(withDuration: 0.4) {
            self.youtubeViewRightConstraint.constant = 0
            self.youtubeViewTopConstraint.constant = 0
            self.youtubeViewLeftConstraint.constant = 0
            self.youtubeViewBottonConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
       
    }
    func getRightMenu(){
        if !RightMenuTutorialFinished {
            RightMenuTutorialFinished = true
//            self.playTutorial(tutorial: .rightMenu)
            self.playTutorial(rewardUI:false,tutorial: .rightMenu,
                              completion: { [weak self] (tutorial) in
                                if tutorial == .rightMenu {
                                    self?.getRightMenu()
                                }
                                self?.resumeVideo()
            })
            return
        }
        if (self.leftViewWidth.constant == self.initialX){
            self.initView()
            NSLayoutConstraint.deactivate([self.youtubeViewCenter])
            self.showMenu(menuType: .right, subMenu: .music,youtubeMode:(currentPlayer == .youtube) )
            self.resetPanDirection()
            self.hideLeftMenu()
            UIView.animate(withDuration: 0.3) {
                self.youtubeViewRightConstraint.constant = self.circularCollectionViewWidth ?? 0
                self.youtubeViewBottonConstraint.constant = 100
                self.view.layoutIfNeeded()
            }
        }
    }
    func handleSwipeGestureLeftMenu(){
        //self.initView()
        //self.showMenu(menuType: .right, subMenu: .music,youtubeMode:(currentPlayer == .youtube) )
        
        //use pan gesture..
    }
    
    func handlePanGestureLeftMenu(gestureRecognizer: UIPanGestureRecognizer){
        
//        print("handlePanGestureLeftMenu")
        if gestureRecognizer.state == .began {
            // Save the view's original position.
            self.initialX = self.leftViewWidth.constant
            if panDirection.orientation == .none {
                panDirection = gestureRecognizer.panDirectionForTouchBegan()
            }
        }
        let piece = gestureRecognizer.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = gestureRecognizer.translation(in: piece.superview)
        let widthDiff = translation.x
        let newWidth = self.initialX + widthDiff
        
        if self.panDirection.orientation == .horizontal {
            UIView.animate(withDuration: 0.2, animations: {
                //DispatchQueue.main.async {
                    self.hideProfileViewRightPart()
                 //   print("route2")
               // }
            })
            //TODO_OONA: make it smoothly close like other part 
            if gestureRecognizer.state != .cancelled {
                
                
                // Add the X and Y translation to the view's original position.
                NSLayoutConstraint.deactivate([self.youtubeViewCenter])
                self.view.layoutIfNeeded()
                
                if newWidth >= 0 && newWidth <= (self.leftMenuWidthConstant + self.view.safeAreaInsets.left){
                    
                    self.leftViewWidth.constant = newWidth
                    
                    self.view.layoutIfNeeded()
                }
                
            }
            else {
                // On cancellation, return the piece to its original location.
                
                self.leftViewWidth.constant = 0-self.initialX
            }
            if gestureRecognizer.state == .ended {
                // Save the view's original position.
                //                    self.tcoinViewHeight.isActive = false
             //   self.view.setNeedsLayout()
                
                //                    self.tcoinViewHeight.isActive = true
                
                
                if self.leftViewWidth.constant > (self.leftMenuHalfWidth + self.view.safeAreaInsets.left) {
                    // show
                    self.showLeftMenu(leftMenuType: .full)
                    
                }else if self.leftViewWidth.constant > 30 {
                    self.showLeftMenu(leftMenuType: .half)
                    // show
                }else{
                    // hide
                    self.hideLeftMenu()
                    
                }
            }
        }
    }
    
    func showLeftMenu(leftMenuType : LeftMenuType){
        
        if !leftMenuTutorialFinished {
            leftMenuTutorialFinished = true
//            self.playTutorial(tutorial: .leftMenu)
            self.playTutorial(rewardUI:false,tutorial: .leftMenu,
                              completion: { [weak self] (tutorial) in
                if tutorial == .rightMenu {
                    self?.getRightMenu()
                }
                self?.resumeVideo()
            })
        }
        
        NSLayoutConstraint.deactivate([self.youtubeViewCenter])
        if(leftMenuType == .half){
            UIView.animate(withDuration: 0.2, animations: {
                self.leftViewWidth.constant = (self.leftMenuHalfWidth + self.view.safeAreaInsets.left)
                self.view.layoutIfNeeded()
                self.buttonBackView.alpha = 0
                self.buttonBackLine.alpha = 0
                
                self.userProfileImage.alpha = 1
                self.userProfileTopLine.alpha = 0
                self.userProfileBottomLine.alpha = 0
                //self.userProfileRightAchor.alpha = 0
            })
        }else{
            
            UIView.animate(withDuration: 0.2, animations: {
                self.leftViewWidth.constant = (self.leftMenuWidthConstant + self.view.safeAreaInsets.left)
                self.view.layoutIfNeeded()
                 self.buttonBackView.alpha = 1
                self.buttonBackLine.alpha = 1
                
                self.userProfileImage.alpha = 1
                self.userProfileTopLine.alpha = 1
                self.userProfileBottomLine.alpha = 1
                //self.userProfileRightAchor.alpha = 1
                
            })
        }
    }
    
    func hideProfileViewRightPart(){
        self.userProfileTopLine.alpha = 0
        self.userProfileBottomLine.alpha = 0
        //self.userProfileRightAchor.alpha = 0
        print("close user profile right part ")
    }
    
    func hideProfileView(){
        
        print("close user profile")
        self.userProfileImage.alpha = 0 //left part
        DispatchQueue.main.async {
            self.hideProfileViewRightPart()
            print("route3")
        }
        self.view.layoutIfNeeded()
        
    }
    func hideLeftMenu(){
        print("hideLeftMenu")
        
//        NSLayoutConstraint.activate([self.youtubeViewCenter])
        
        self.hideProfileView()
        
        print("currentSelectingFrame",currentSelectingFrame)
        self.leftMenuScrollView.scrollRectToVisible(currentSelectingFrame, animated: true)
        UIView.animate(withDuration: 0.2, animations: {
            self.leftViewWidth.constant = 0
            self.buttonBackView.alpha = 0
            self.buttonBackLine.alpha = 0
            self.resetPanDirection()
            self.view.layoutIfNeeded()
            
            
        })
    }
    func resetLeftMenuButtons() {
        
        self.searchButton.isSelected = false
        self.gameBottomButton.isSelected = false
        self.homeButton.isSelected = false
        self.botButton.isSelected = false
        self.musicButton.isSelected = false
        self.tcoinButton.isSelected = false
        self.xploreButton.isSelected = false
        self.appsButton.isSelected = false
        self.tvButton.isSelected = false
        self.settingsButton.isSelected = false
        self.tcoinBottomButton.isSelected = false
        self.radioButton.isSelected = false
        
    }
    func initView(){
        self.hideLowerView()
        
        self.userProfileLeading.constant = self.view.safeAreaInsets.left + 5
        
        self.searchButton.imageView?.contentMode = .scaleAspectFit
        self.homeButton.imageView?.contentMode = .scaleAspectFit
        self.botButton.imageView?.contentMode = .scaleAspectFit
        self.musicButton.imageView?.contentMode = .scaleAspectFit
        self.tcoinButton.imageView?.contentMode = .scaleAspectFit
        self.tcoinBottomButton.imageView?.contentMode = .scaleAspectFit
        self.gameBottomButton.imageView?.contentMode = .scaleAspectFit
        self.xploreButton.imageView?.contentMode = .scaleAspectFit
        self.appsButton.imageView?.contentMode = .scaleAspectFit
        self.tvButton.imageView?.contentMode = .scaleAspectFit
        self.settingsButton.imageView?.contentMode = .scaleAspectFit
        self.referralButton.imageView?.contentMode = .scaleAspectFit
        self.radioButton.imageView?.contentMode = .scaleAspectFit
        
//        self.referralButton.startShimmering()
//        print ("top bt",self.searchButton.frame.origin.y)
        let leftInset : CGFloat = self.view.safeAreaInsets.left + 3
        let topInset : CGFloat = 5.0
        let bottomInset : CGFloat = 5.0
        self.searchButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.tvButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.homeButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.botButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.tcoinButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.gameBottomButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.tcoinBottomButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.xploreButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.musicButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.appsButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        self.settingsButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
          self.referralButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
         self.radioButton.contentEdgeInsets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: 0)
        
    }
    func hideLowerView(){
        UIView.animate(withDuration: 0.3) {
            self.lowerViewConstraint.constant = 0-APPSetting.lowerViewHeight
            self.view.layoutIfNeeded()
        }
        
    }
    func showLowerView(){
        
        UIView.animate(withDuration: 0.3) {
            self.lowerViewConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.hideLowerView()
        }
    }
    
    func gettcoinView(addedTcoin: Int, totalTcoin : Int){
        
       
        self.refreshUserDatas()
        
        while let subview = lowerView.subviews.last { //clear the view first
            subview.removeFromSuperview()
        }
        
        let tcoinsResultViewController : tcoinsResultViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "tcoinsResultViewController") as! tcoinsResultViewController
        tcoinsResultViewController.addedTcoin = addedTcoin
        tcoinsResultViewController.totalTcoin = totalTcoin
        tcoinsResultViewController.completionBlock = {(result) -> () in
            //show coin wallet
            
        }
        self.addChild(tcoinsResultViewController)
        self.lowerView.addSubview(tcoinsResultViewController.view)
        self.lowerView.layoutIfNeeded()
        self.lowerView.layoutSubviews()
        showLowerView()
//        self.view.setNeed
//        self.view.layoutOfNeeded()
    }
    override func channelMenuOnShow() {
        NSLayoutConstraint.deactivate([self.youtubeViewCenter])
        UIView.animate(withDuration: 0.3) {
            self.youtubeViewTopConstraint.constant = 40
            self.youtubeViewLeftConstraint.constant = 250
            self.youtubeViewBottonConstraint.constant = 100
            self.view.layoutIfNeeded()
        }
    }
    override func channelMenuOnHide() {
        self.resetYoutubeViewConstraints()
        if (currentPlayer == .youtube){
            self.youtubePlayerView.playVideo()
        }
    }
    func closeHoverView() {
        self.hideMenu()
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("should receive",touch.view)
//        if let touchedView = touch.view, touchedView.isKind(of: CustomSlider.self) {
//            return false
//        }
//        if let touchedView = touch.view, touchedView.isKind(of: CustomSlider.self) {
//            return false
//        }
        return true
    }
 
    @IBAction func getMenu() {
        print("get menu btn")
        self.hideControlView()
        //self.showMenu()
    }
    
    public func initPlayerView() {
//
//
////        initBcoPlayerView()
//        player = AVPlayer.init(playerItem: playerItem)
 
        
      
    }
   
   
    func initLeftMenuStuff() {
//        self.leftDrawer.rx.anyGesture(pan(),)
//        self.leftDrawer.rx.panGesture().when(.began,.changed,.ended)
//            .subscribe(onNext: { gestureRecognizer in
//                print("leftD pges")
//
//                self.gestureForLeftDrawer(gestureRecognizer: gestureRecognizer)
//
//            })
//            .disposed(by: disposeBag)
       
        self.searchButton.rx.controlEvent(.touchDown)
            .subscribe(onNext: { (event) in
                /*
                let searchView: UIViewController = UIStoryboard(name: "Search", bundle: nil).instantiateInitialViewController()!
                searchView.modalTransitionStyle = .crossDissolve
                self.present(searchView, animated: true, completion: nil)
                 */
                print("touchDragEnter")
                }).disposed(by: disposeBag)
//        self.searchButton.rx.controlEvent(.touchDragOutside)
//            .subscribe(onNext: { (event) in
//                print("touchDragOutside")
//            }).disposed(by: disposeBag)
    }
    func gestureForLeftDrawer(gestureRecognizer:UIGestureRecognizer) {
        let originalHeight : CGFloat = 30.0
        let enlargeHeight : CGFloat = 50.0
        
        let currentPanPosition = gestureRecognizer.location(in: self.leftDrawer)
        self.view.setNeedsLayout()
        if self.searchButton.checkPointInSameVerticalRange(point: currentPanPosition) {
            
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.searchButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.searchExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.searchExtendHeight)
            self.searchButton.isSelected = true
        }else{
            self.searchButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.searchButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.searchExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.searchExtendHeight)
        }
        if self.homeButton.checkPointInSameVerticalRange(point: currentPanPosition){
            
            
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.homeButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.homeExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.homeExtendHeight)
            self.homeButton.isSelected = true
        }else{
            self.homeButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.homeButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.homeExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.homeExtendHeight)
        }
        if self.tcoinButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.tcoinButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.tcoinExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.tcoinExtendHeight)
            self.tcoinButton.isSelected = true
        }else{
            self.tcoinButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.tcoinButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.tcoinExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.tcoinExtendHeight)
        }
        
        if self.xploreButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.xploreButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.xploreExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.xploreExtendHeight)
            self.xploreButton.isSelected = true
        }else{
            self.xploreButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.xploreButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.xploreExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.xploreExtendHeight)
        }
        
        if self.musicButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.musicButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.musicExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.musicExtendHeight)
            self.musicButton.isSelected = true
        }else{
            self.musicButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.musicButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.musicExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.musicExtendHeight)
        }
        
        if self.appsButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.appsButtonHeight)
            //
                                self.animateConstraint(constantValue: 100, constraint: self.appsExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.appsExtendHeight)
            self.appsButton.isSelected = true
        }else{
            self.appsButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.appsButtonHeight)
            //
                                self.animateConstraint(constantValue: 0, constraint: self.appsExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.appsExtendHeight)
        }
        
        if self.botButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.botButtonHeight)
            
                                self.animateConstraint(constantValue: 130, constraint: self.botExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.botExtendHeight)
            self.botButton.isSelected = true
        }else{
            self.botButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.botButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.botExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.botExtendHeight)
        }
        
        if self.tvButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.tvButtonHeight)
            //
                                self.animateConstraint(constantValue: 70, constraint: self.tvExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.tvExtendHeight)
            self.tvButton.isSelected = true
        }else{
            self.tvButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.tvButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.tvExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.tvExtendHeight)
        }
        
        if self.settingsButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.settingsButtonHeight)
            //
            self.animateConstraint(constantValue: 70, constraint: self.settingsExtendWidth)
            self.animateConstraint(constantValue: 30, constraint: self.settingsExtendHeight)
            self.settingsButton.isSelected = true
        }else{
            self.settingsButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.settingsButtonHeight)
            
            self.animateConstraint(constantValue: 0, constraint: self.settingsExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.settingsExtendHeight)
        }
        if self.radioButton.checkPointInSameVerticalRange(point: currentPanPosition){
            self.animateConstraint(constantValue: enlargeHeight, constraint: self.xploreButtonHeight)
            
                                self.animateConstraint(constantValue: 100, constraint: self.xploreExtendWidth)
                                self.animateConstraint(constantValue: 30, constraint: self.xploreExtendHeight)
            self.radioButton.isSelected = true
        }else{
            self.radioButton.isSelected = false
            self.animateConstraint(constantValue: originalHeight, constraint: self.xploreButtonHeight)
            
                                self.animateConstraint(constantValue: 0, constraint: self.xploreExtendWidth)
                                self.animateConstraint(constantValue: 0, constraint: self.xploreExtendHeight)
        }
        
    }
    func animateConstraint(constantValue:CGFloat, constraint:NSLayoutConstraint) {
        UIView.animate(withDuration: 0.15) {
            constraint.constant = constantValue
            self.view.layoutIfNeeded()
        }
    }
    func HideAllExtendButtons() {
       
            self.animateConstraint(constantValue: 0, constraint: self.searchExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.searchExtendHeight)
        
            self.animateConstraint(constantValue: 0, constraint: self.homeExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.homeExtendHeight)
      
            self.animateConstraint(constantValue: 0, constraint: self.tcoinExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.tcoinExtendHeight)
       
            self.animateConstraint(constantValue: 0, constraint: self.xploreExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.xploreExtendHeight)
       
            self.animateConstraint(constantValue: 0, constraint: self.musicExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.musicExtendHeight)
       
            self.animateConstraint(constantValue: 0, constraint: self.appsExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.appsExtendHeight)
       
            self.animateConstraint(constantValue: 0, constraint: self.botExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.botExtendHeight)
        
            self.animateConstraint(constantValue: 0, constraint: self.tvExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.tvExtendHeight)
        
            self.animateConstraint(constantValue: 0, constraint: self.settingsExtendWidth)
            self.animateConstraint(constantValue: 0, constraint: self.settingsExtendHeight)
        
        
        
    }
    func panGestureForGameAd(gestureRecognizer:UIPanGestureRecognizer) {
        let triggerValue = self.view.frame.size.width / 4
        
        let piece = gestureRecognizer.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = gestureRecognizer.translation(in: piece.superview)
        
        let widthDiff = translation.x
        
        if gestureRecognizer.state != .cancelled {
            // Add the X and Y translation to the view's original position.
            let newWidth = widthDiff
            //                        if newWidth >= 0 && newWidth <= maximumWidth{
            if (!self.engagementStarted) {
                self.gameAdButtonLeading.constant = newWidth
            }
            //                        }
//            if (abs(newWidth) >= 30) {
//                self.gameAdContainer.isHidden = false
//            }else{
//                self.gameAdContainer.isHidden = true
//            }
            if (abs(newWidth) >= triggerValue) {
                
                self.slideGameAdButton()
            }
            
            
        }
        else {
            // On cancellation, return the piece to its original location.
            
            
            UIView.animate(withDuration: 0.2, animations: {
                self.gameAdButtonLeading.constant = 0
                self.resetPanDirection()
                self.view.layoutIfNeeded()
                
            })
        }
        if gestureRecognizer.state == .ended {
            // Save the view's original position.
            //                    self.tcoinViewHeight.isActive = false
            
            
            
            if (abs(self.gameAdButtonLeading.constant) < triggerValue){
                self.view.setNeedsLayout()
                self.gameAdContainer.isHidden = true
                // hide
                UIView.animate(withDuration: 0.2, animations: {
                    self.gameAdButtonLeading.constant = 0
                    self.resetPanDirection()
                    self.view.layoutIfNeeded()
                    
                })
            }else{
                
                // already triggered, do nothing.
            }
            //            print ("pan end, hide")
            
            
        }
    }
    func panGestureForEngagement(gestureRecognizer:UIPanGestureRecognizer) {
        let triggerValue = self.view.frame.size.width / 4

        let piece = gestureRecognizer.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = gestureRecognizer.translation(in: piece.superview)
        
        let widthDiff = translation.x

        if gestureRecognizer.state != .cancelled {
            // Add the X and Y translation to the view's original position.
            let newWidth = widthDiff
            //                        if newWidth >= 0 && newWidth <= maximumWidth{
             if (!self.engagementStarted) {
            self.adButtonTrailing.constant = newWidth
            }
            //                        }
            if (abs(newWidth) >= 30) {
               self.engagementView.isHidden = false
            }else{
                self.engagementView.isHidden = true
            }
            if (abs(newWidth) >= triggerValue) {
                
               self.slideEngagementButton()
            }
            
            
        }
        else {
            // On cancellation, return the piece to its original location.
            
            
            UIView.animate(withDuration: 0.2, animations: {
                self.adButtonTrailing.constant = 0
                self.resetPanDirection()
                self.view.layoutIfNeeded()
                
            })
        }
        if gestureRecognizer.state == .ended {
            // Save the view's original position.
            //                    self.tcoinViewHeight.isActive = false
            
            
           
            if (abs(self.adButtonTrailing.constant) < triggerValue){
                self.view.setNeedsLayout()
                self.engagementView.isHidden = true
                // hide
                UIView.animate(withDuration: 0.2, animations: {
                    self.adButtonTrailing.constant = 0
                    self.resetPanDirection()
                    self.view.layoutIfNeeded()
                    
                })
            }else{
                
                // already triggered, do nothing.
            }
//            print ("pan end, hide")
            
            
        }
    }
   
    @IBAction func engagementAdButtonTapped(_ sender: Any) {
        self.slideEngagementButton()
    }
    
    func panGestureForTcoinWallet(gestureRecognizer:UIPanGestureRecognizer) {
        
        let halfHeight : CGFloat = self.lowerView.frame.size.height/2
        let maximumHeight : CGFloat = self.lowerView.frame.size.height
        let initialHeight = self.lowerViewConstraint.constant
//        if gestureRecognizer.state == .began {
            // Save the view's original position.
            if panDirection.orientation == .none {
                panDirection = gestureRecognizer.panDirectionForTouchBegan()
            }
//        }
        let piece = gestureRecognizer.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = gestureRecognizer.translation(in: piece.superview)
        let heightDiff = translation.y * -1

            if gestureRecognizer.state != .cancelled {
                // Add the X and Y translation to the view's original position.
                let newHeight = heightDiff
                if newHeight < 0 {
                    self.lowerViewConstraint.constant = newHeight
                }
                
            }
            else {
                // On cancellation, return the piece to its original location.
                
                self.lowerViewConstraint.constant = initialHeight
            }
            if gestureRecognizer.state == .ended {
                // Save the view's original position.
                //                    self.tcoinViewHeight.isActive = false
                self.view.setNeedsLayout()
                
                //                    self.tcoinViewHeight.isActive = true
                if heightDiff < 0 {

                    UIView.animate(withDuration: 0.2, animations: {
                        self.lowerViewConstraint.constant = 0-APPSetting.lowerViewHeight
                        self.tcoinTabBarView.hideTabBar()
                        self.view.layoutIfNeeded()
                        
                        
                    })
                }
            
        }
    }
    
    func panGestureForTcoinTab(panGesture: UIPanGestureRecognizer) {
        let bottomInset: CGFloat    = (UIDevice.current.hasNotch ? 44 : 26)
        let halfHeight : CGFloat    = 100.0
        let maximumHeight : CGFloat = self.view.frame.size.height - bottomInset
        
        let piece       = panGesture.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = panGesture.translation(in: piece.superview)
        let heightDiff  = translation.y
        let tcoinState  = self.tcoinsDetailVC.currentTCoinState
        let state = panGesture.state
        if state == .began {
            // Save the view's original position.
            self.initialY = self.tcoinBarTop.constant
            if panDirection.orientation == .none {
                panDirection = panGesture.panDirectionForTouchBegan()
            }
            if panDirection.orientation == .vertical {
                tcoinsDetailTopToSafeArea.constant = 0
                self.tcoinsDetailView.layoutIfNeeded()
            }
        }
        
        if panDirection.orientation == .vertical {
            
            if state != .cancelled {
                // Add the X and Y translation to the view's original position.
                let newHeight = self.initialY + heightDiff
                if newHeight >= 0 && newHeight <= maximumHeight {
                    
                    if self.tcoinTabBarView.currentBarState == .none {
                        self.tcoinTabBarView.showBarForState(barState: .prepareTDisplayTabBar)
                    }
                    
                    var tCoinsTabBarAlpha: CGFloat = 0
                    var tCoinsTabDetailAlpha: CGFloat = 0
                    self.tcoinBarTop.constant = newHeight
                    self.tcoinTabBlackCover.alpha = 0
                    // Transits from tab bar to detail view
                    switch tcoinState {
                    case .tcoinsHidden:
                        tCoinsTabBarAlpha = min(1, (newHeight / 130))
                    case .displayingTabBar:
                        tCoinsTabBarAlpha = max(0, (1 - (newHeight / 260)) / 1)
                        tCoinsTabDetailAlpha = min(1, (newHeight / maximumHeight))
                    case .displayingDetail:
                        tCoinsTabDetailAlpha = min(1, (newHeight / maximumHeight))
                    }
                    self.tcoinsDetailView.layoutIfNeeded()
                    self.tcoinsDetailVC.transitTCoinTabBar(toAlpha: tCoinsTabBarAlpha)
                    self.tcoinsDetailVC.transitTCoinDetailView(toAlpha: tCoinsTabDetailAlpha)
                }
                
            }
            else {
                // On cancellation, return the piece to its original location.
                self.tcoinsDetailVC.resetTCoinView()
                self.tcoinBarTop.constant = self.initialY
            }
            if state == .ended {
                
                self.view.setNeedsLayout()
                if !tcoinFullTutorialFinished {
                    tcoinFullTutorialFinished = true
                    self.playTutorial(rewardUI:false,tutorial: .tcoin,
                                      completion: { [weak self] (tutorial) in
                                        if tutorial == .rightMenu {
                                            self?.getRightMenu()
                                        }
                                        self?.resumeVideo()
                    })
                }
                if heightDiff > 30 {
                    // show
                    var calculatedHeight: CGFloat = 0
                    var nextTabBarState: TCoinTabBarDisplayState = .none
                    UIView.animate(withDuration: 0.4,
                                   animations: {
                                    
                                    if heightDiff+self.initialY > maximumHeight/2 {
                                        // full
                                        calculatedHeight = maximumHeight
                                        nextTabBarState = .displayForTabDetail
                                        self.tcoinsDetailVC.showTCoinDetailView()
                                        self.tcoinTabBlackCover.alpha = 1
                                        // For tutorial
                                        self.showtcoinsBarStep2()
                                        
                                    } else {
                                        calculatedHeight = halfHeight
                                        nextTabBarState = .displayForTabBar
                                        self.tcoinsDetailVC.showTCoinTabBar()
                                        self.tcoinTabBlackCover.alpha = 0
                                        // For tutorial
                                        self.showtcoinsBarStep1()
                                    }
                                    
                                    self.tcoinBarTop.constant = calculatedHeight
                                    self.view.layoutIfNeeded()
                                    self.tcoinsDetailView.layoutIfNeeded()
                                    self.tcoinTabBarView.showBarForState(barState: nextTabBarState,
                                                                         animated: true)
                    })
                } else {
                    // hide
                    print(heightDiff)
                    UIView.animate(withDuration: 0.2,
                                   animations: {
                                    self.tcoinBarTop.constant = 0
                                    self.tcoinTabBlackCover.alpha = 0
                                    if self.controlView.alpha == 1 {
                                        self.tcoinTabBarView.showBarForState(barState: .displayForControlView,
                                                                             animated: true)
                                    } else {
                                        self.tcoinTabBarView.hideTabBar()
                                    }
                                    self.tcoinsDetailVC.resetTCoinView()
                                    
                                    self.view.layoutIfNeeded()
                    })
                    
                    
                }
                
            }
            if state == .ended ||
                state == .cancelled ||
                state == .failed {
                self.resetPanDirection()
            }
        } else if panDirection.orientation == .horizontal {
            if state == .ended ||
                state == .cancelled ||
                state == .failed {
                self.resetPanDirection()
            }
        }
    }
   
    func showtcoinsBarStep1(){
        //TODO_OONA : refator
        
    }
    func showtcoinsBarStep2(){
        
    }
    
    func panGestureForLowerViewTab(gestureRecognizer:UIPanGestureRecognizer) {
        
        let initialY : CGFloat = -106.0
        let maximumHeight : CGFloat = 0
        
        if gestureRecognizer.state == .began {
            // Save the view's original position.
            //self.initialY = self.lowerViewConstraint.constant
            if(panDirection == .none){
                panDirection = gestureRecognizer.panDirectionForTouchBegan()
            }
        }
        let piece = gestureRecognizer.view!
        // Update the position for the .began, .changed, and .ended states
        let translation = gestureRecognizer.translation(in: piece.superview)
        let heightDiff = translation.y
        print("pan heightDiff ", heightDiff)
        
        if panDirection.orientation == .vertical {
            if gestureRecognizer.state == .ended {
                // Save the view's original position.
                //                    self.tcoinViewHeight.isActive = false
                self.view.setNeedsLayout()
                
                //                    self.tcoinViewHeight.isActive = true
                if heightDiff > 30 {
                    // show
                    
                    print ("pan end, show")
                    UIView.animate(withDuration: 0.2, animations: {
                        //lowerViewConstraint = initialY
                    })
                }else{
                    // hide
                    UIView.animate(withDuration: 0.2, animations: {
                        //lowerViewConstraint = 0
                        
                    })
                    print ("pan end, hide")
                    
                }
            }
        }
    }
    var youtubePlayerDuration : Float = 0
    func getCurrentDuration() -> Int {
        if currentPlayer == .youtube {
//            self.youtubePlayerView.getCurrentTime { (time, error) in
            
                return Int(youtubePlayerDuration * 1000)
                
//            }
        }else{
            let seconds = self.player.currentTime().seconds
            guard !(seconds.isNaN || seconds.isInfinite) else {
                return 0 // or do some error handling
            }
            return Int(seconds * 1000)
        }
        
        
    }
    func handleLogin(user:User){
        self.setupUserView(user:user)
        //tcoin
        
    }
    func handleLogout() {
        userProfileTopLine.text = LanguageHelper.localizedString("log_in_text")
        userProfileBottomLine.text = LanguageHelper.localizedString("to_earn_more_points")
        self.userProfileImage.image = UIImage.init(named: "user-profile")
    }
    
    func setupUserView(user:User){
//        print("setupUserView",user.profile)
        if let userProfile = user.profile, let _firstName = userProfile.firstName , let _lastName = userProfile.lastName {
            
            userProfileTopLine.text = _firstName + " " + _lastName
            userProfileBottomLine.text = LanguageHelper.localizedString("edit_profile_text")
        }else{
            
            print("fail to get first name last name")
        }
        
        
        if let imageUrl =  user.profile?.imageUrl{
            if let url = URL.init(string: imageUrl ?? ""){
                //let image = UIImage(named: "channel-placeholder-image")
                self.userProfileImage?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"))
            }
        }
        if !UserHelper.shared.isLogin() {
            userProfileTopLine.text = LanguageHelper.localizedString("log_in_text")
                userProfileBottomLine.text = LanguageHelper.localizedString("to_earn_more_points")
                self.userProfileImage.image = UIImage.init(named: "user-profile")
            
        }
        
    }
    var playVideoTimeStamp : Int = 0
    func handleVideoAnalytics (lastVideo : Video?, nextVideo : Video) {
        if (lastVideo != nil) {
            let newTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
            AnalyticHelper.shared.videoStop(video: lastVideo!, watchedDuration: (newTimeStamp - playVideoTimeStamp) )
        }
        playVideoTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        AnalyticHelper.shared.videoStart(video: nextVideo)
    }
    
    public func playVideo(video:Video){
    
        handleVideoAnalytics(lastVideo: currentVideo, nextVideo: video)
        
        currentVideo = video
        self.playerStartPlaying()
        if (video.type == .live){
            
            progressSlider.isUserInteractionEnabled = false
            progressSlider.setValue(1.0, animated: true)
            self.videoTypeTextView.text = LanguageHelper.localizedString("LIVE")
            self.videoTypeTextView.backgroundColor = Color.redOONAColor()
            progressSlider.minimumTrackTintColor = Color.redOONAColor()
            progressSlider.maximumTrackTintColor = UIColor.init(rgb: 0xa6a6a6)
            
            progressSlider.setThumbImage(UIImage(), for: .normal)
            progressSlider.setThumbImage(UIImage(), for: .highlighted)
            PlayerHelper.shared.videoControls.accept([.Default])
//            progressSlider.
        }else{
//        if (video.type == .vod){
            progressSlider.isUserInteractionEnabled = true
            self.videoTypeTextView.text = LanguageHelper.localizedString("VOD")
            self.videoTypeTextView.backgroundColor = Color.vodTagColor()
            progressSlider.minimumTrackTintColor = Color.vodTagColor() // #179183
            progressSlider.maximumTrackTintColor = UIColor.init(rgb: 0xa6a6a6)
            
            progressSlider.setThumbImage(UIImage(named:"sliderThumb"), for: .normal)
            progressSlider.setThumbImage(UIImage(named:"sliderThumb"), for: .highlighted)
        }
        self.updatePlayerControlButtons(for: PlayerHelper.shared.videoControls.value)
        self.player.pause()
        self.youtubePlayerView.pauseVideo()
        self.playerLayer.isHidden = false
        self.youtubeView.isHidden = true
        currentPlayer = .bco
        if (video.brightcoveId != nil) {
            self.playVideo(brightcoveId: video.brightcoveId!)
        } else if (video.streamingUrl != nil) {
            self.playVideo(url: video.streamingUrl!)
        } else if (video.youtubeId != nil) {
            currentPlayer = .youtube
            self.playerLayer.isHidden = true
            self.youtubeView.isHidden = false
            self.playVideo(youtubeId: video.youtubeId!)
        }
        self.videoNameView.text = video.channelName
        self.videoImage.sd_setImage(with: URL.init(string: video.imageUrl ?? " "), completed: nil)
    }
    public func playVideo(url:String) {
        print(#function)
        self.currentPlayer = .bco
        self.playbackController?.setVideos([BCOVVideo.init(hlsSourceURL: URL.init(string: url)!)] as NSFastEnumeration)
        self.playbackController?.play()
        self.playbackController?.delegate = self
        if !self.isFirstVideo {
            self.view.startLoading(above: self.youtubeView)
        }
//        if isFirstVideo {
//            print("url firstvideo pause")
//            self.pauseVideo()
//            isFirstVideo = false
//        }
    }
    
    func updatePlayerControlButtons(for videoControl: [VideoControl]) {
        var playPauseEnabled = true
        var previousEnabled = false
        var nextEnabled = false
        var previousButtonConstraintOffsetX = -(self.nextButton.frame.size.width > 0 ? self.nextButton.frame.size.width : 40)
        var nextButtonConstraintOffsetX = previousButtonConstraintOffsetX
        
        if videoControl.contains(.Default) {
//            let dimPlay: VideoControl = [.PlayPauseDim]
            
            if videoControl.contains(.PlayPauseDim) {
                playPauseEnabled = false
            }
            
            if videoControl.contains(.Previous) || videoControl.contains(.PreviousDim) {
                previousButtonConstraintOffsetX = 0
                previousEnabled = !(videoControl.contains(.PreviousDim))
            }
            
            if videoControl.contains(.Next) || videoControl.contains(.NextDim) {
                nextButtonConstraintOffsetX = 0
                nextEnabled = !(videoControl.contains(.NextDim))
            }
        }
        
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.playButton.isEnabled = playPauseEnabled
                        self.previousButton.isEnabled = previousEnabled
                        self.nextButton.isEnabled = nextEnabled
                        self.playButtonLeadingConstraint.constant = previousButtonConstraintOffsetX
                        self.nextButtonLeadingConstraint.constant = nextButtonConstraintOffsetX
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
    func showMessage(message:String) {
    
        let popup = CustomPopup(message: message)
        //    CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:message AndMessage:message];
        //    popup.button2.hidden = YES;
        popup?.button1.setTitle("OK", for: .normal)
        //    CGRect buttonFrame = popup.button1.frame;
        //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
        //    popup.button1.frame = buttonFrame;
        let popupBlock = popup
        //    popup.backBtn. hidden  = YES;
        popup?.setupOneButton()
        popup?.completionButton1 = {
            
                popupBlock?.dismiss()
                
            
        }
        popup?.alpha = 0
        let window = UIApplication.shared.keyWindow!
        window.addSubview(popup!) // Add your view here
        
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            popup?.alpha = 1
        })
        
        
    }
    
     func playerStartPlaying() {
//        self.setupMidrollAds
        
        print("playerStartPlaying",currentVideo?.displayAdInterval.firstInterval)
        print("playerStartPlaying",currentVideo?.displayAdInterval.podDuration)
        print("playerStartPlaying",currentVideo?.displayAdInterval.podInterval)
        // disable mid roll ad first
        let duration = currentVideo?.displayAdInterval.podDuration ?? 10
        let firstInterval = currentVideo?.displayAdInterval.firstInterval ?? 30
        let AfterInterval = currentVideo?.displayAdInterval.podInterval ?? 120
//        midrollAdIntervalTimer.suspendf()
//        midrollAdIntervalTimer = Timer()
//        midrollAdIntervalTimer.startTimer(seconds: interval) {
//            self.startNewMidRollInterval(duration: duration, interval: interval)
//        }
//
        if (!DeviceHelper.shared.isAdFree) {
            self.setupPrerollAds()
        }
        if (!DeviceHelper.shared.isAdFree) {
        startNewEngagementInterval(adDuration:duration, firstInterval:firstInterval, AfterwardInterval:AfterInterval)
        }
    }
    
    
//     func startEngagementTimer(duration:Int) {
//        //        adHelp.startTimer(seconds: 4, view: self.view)
//        engagementAdDismissTimer.suspend()
//
//        engagementAdDismissTimer = Timer()
//
//        engagementAdDismissTimer.startTimer(seconds: duration) {
//
//            self.setupEngagementAds()
//        }
//
//
//    }
    

     func progress(current time: Float, duration: Float) -> Float {
        if (currentVideo!.type == .live) {
            return 1
        }else{
            return min((time / duration), 1)
        }
    }
     func backToStart() {

//        self.playVideo(url: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8")
    }
    @IBAction func leftMenuTcoinButton(_ sender: Any) {
//
  
    }
    
    var leftMenuTutorialFinished : Bool = true
    var tcoinHalfTutorialFinished : Bool = true
    var tcoinFullTutorialFinished : Bool = true
    var RightMenuTutorialFinished : Bool = true
    var ChannelListTutorialFinished : Bool = true
    
    func bcoAdvanceTo() {
//        playerLayer = AVPlayerLayer (player: player)
//        playerLayer.videoGravity = .resizeAspect
//        //        playerLayer.backgroundColor = UIColor.gray.cgColor
//        playerLayer.frame = self.view.bounds
//        playerLayer.zPosition = -1
//
//
//        self.view.layer.addSublayer(playerLayer)
    }
    func appsButtonAction() {
        
//        let vc = PopupWebViewController(url: "http://entanglement.gopherwoodstudios.com/en-US-index.html")
//        vc?.title = "Ninja Run"
        //    [self.navigationController pushViewController:webViepwController animated:YES];
        
//        let vc = UIStoryboard(name: "GameViewController", bundle: nil).instantiateInitialViewController()
//
        /// TODO:
        //        self.present(navigationViewController,
        //                     animated: true,
        //                     completion: nil)
        
//        showGames()
//        self.present(vc!, animated: true, completion: nil)

    }
    
    func resumeVideo() {
        print(#function + "check")
        if menuDetailShown() == false && self.shouldResumePlayer {
            if currentPlayer == .bco { self.player.play() }
            if currentPlayer == .youtube { self.youtubePlayerView.playVideo() }
            
            
            print("Did " + #function)
        }else{
            print("resumeVideo else case")
        }
        self.engagementAdDismissTimer.resumeTimer()
        self.engagementAdFirstIntervalTimer.resumeTimer()
        self.engagementAdAfterwardIntervalTimer.resumeTimer()
    }
    
    func pauseVideo() {
        self.player.pause()
        self.youtubePlayerView.pauseVideo()
        self.player.cancelPendingPrerolls()
        self.engagementAdDismissTimer.pauseTimer()
        self.engagementAdFirstIntervalTimer.pauseTimer()
        self.engagementAdAfterwardIntervalTimer.pauseTimer()
        print("Did " + #function)
    }
    
   var tempWifiRestrict = OONASettings.manager.settingsObject.streamingOnlyWifi
    
   func showMenu(menuType: MenuType, subMenu: SubMenu, youtubeMode: Bool){
    if menuType != .right {
         self.pauseVideo()
    }
        //cuz the left menu module exist in player layer not base, so override this
        var hasShowView = false
        if(menuType == .up){
            if(subMenu == .xplore){
                self.showXplore()
                hasShowView = true
            }
            if(CurrentMenuType == .search){
                self.showSearch()
                hasShowView = true
            }
        }
        
        if(!hasShowView){
            
//            hideLeftMenuDetail(animated: true)
            self.showTVMenu(menuType: menuType, subMenu: subMenu, youtubeMode: youtubeMode)
        }
    }
    func showTVMenu(menuType: MenuType, subMenu: SubMenu, youtubeMode: Bool){
        if(menuType == .left){
            
            let _menuVC  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            _menuVC.view.backgroundColor = .clear
            _menuVC.modalPresentationStyle = .overCurrentContext
            _menuVC.modalTransitionStyle = .flipHorizontal
            chVC = _menuVC
            menuVC = _menuVC
            
            self.updateActiveChild(activeChild: chVC)
            self.leftMenuDetail.bringSubviewToFront(chVC!.view)
            self.displayLeftMenuDetail(animated: true)
            self.showLeftMenu(leftMenuType: .half)
            
            
//            self.present(chVC!, animated: true, completion: nil)
            
        }else if(menuType == .up){
            print("show menu in base")
            var resetMenu = false
            if(MenuHelper.shared.getCurrentSubMenu() != subMenu || chVC == nil){ // if has sub menu , reset the view e.g. music
                //reset
                resetMenu = true
            }
            MenuHelper.shared.currentSubMenu = subMenu
            
            if (resetMenu) {
                let _menuVC  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                _menuVC.view.backgroundColor = .clear
                _menuVC.modalPresentationStyle = .overCurrentContext
                _menuVC.modalTransitionStyle = .crossDissolve
                chVC = _menuVC
                menuVC = _menuVC
                
            }
            
            self.channelMenuOnShow()
//            self.present(chVC!, animated: true, completion: nil)
            
            
            self.updateActiveChild(activeChild: chVC)
            self.leftMenuDetail.bringSubviewToFront(chVC!.view)
            self.displayLeftMenuDetail(animated: true)
            self.showLeftMenu(leftMenuType: .half)
            
            
        }else if(menuType == .right){
            if (circularVC == nil) {
                let circularMenuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CircluarMenuViewController") as! CircluarMenuViewController
                circularMenuVC.view.backgroundColor = .clear
                circularMenuVC.modalPresentationStyle = .overCurrentContext
                circularMenuVC.modalTransitionStyle = .crossDissolve
                circularCollectionViewWidth = circularMenuVC.collectionViewWidth
                menuVC = circularMenuVC
                circularVC = circularMenuVC
            }
            circularVC?.setBlurHidden(hidden:youtubeMode)
            
            self.present(circularVC!, animated: true, completion: nil)
        }
    }
    
    override func appLanguageDidUpdate() {
        self.userProfileTopLine.text = LanguageHelper.localizedString("log_in_text")
        
        self.userProfileBottomLine.text = LanguageHelper.localizedString("to_earn_more_points")
        if let userProfile = UserHelper.shared.currentUser.profile,
            let _ = userProfile.firstName ,
            let _ = userProfile.lastName {
            self.userProfileBottomLine.text = LanguageHelper.localizedString("edit_profile_text")
        }
        
        self.appsButton.setTitle(LanguageHelper.localizedString("menu_games"), for: .normal)
        self.tcoinButton.setTitle(LanguageHelper.localizedString("menu_tcoins"), for: .normal)
        self.tvButton.setTitle(LanguageHelper.localizedString("menu_tv"), for: .normal)
        self.xploreButton.setTitle(LanguageHelper.localizedString("menu_explorer"), for: .normal)
        self.musicButton.setTitle(LanguageHelper.localizedString("menu_music"), for: .normal)
        self.appsButton.setTitle(LanguageHelper.localizedString("menu_games"), for: .normal)
        self.referralButton.setTitle(LanguageHelper.localizedString("menu_refer"), for: .normal)
        self.homeButton.setTitle(LanguageHelper.localizedString("menu_home"), for: .normal)
        self.searchButton.setTitle(LanguageHelper.localizedString("menu_search"), for: .normal)
        self.settingsButton.setTitle(LanguageHelper.localizedString("menu_setting"), for: .normal)
        self.engagementAdsMiningLabel.text = LanguageHelper.localizedString("Mining_Ads")
        self.gameAdsMiningLabel.text = LanguageHelper.localizedString("Mining_Ads")
    }
}

extension PlayerViewController {
    /// Functional methods
    
    func resetPanDirection() {
//        print("reset pan direction")
        self.panDirection = .none
    }
    // TODO:
    
    func updateActiveChild(activeChild: UIViewController?) {
        // Detach last active vc
        if let _activeVC = self.activeChildVC {
            self.detachVCFromLeftMenu(vc: _activeVC)
        }
        
        // Attach new active vc
        if let vc = activeChild {
            self.attachVCToLeftMenu(vc: vc)
        }
    }
    
    func attachVCToLeftMenu(vc: UIViewController) {
        if !self.children.contains(vc) {
            self.addChild(vc)
        }
        vc.willMove(toParent: self)
        
        // TODO:
        let subview: UIView = vc.view
        if !self.leftMenuDetail.subviews.contains(subview) {
            self.leftMenuDetail.addSubview(subview)
            subview.frame = self.leftMenuDetail.bounds
            subview.snp.makeConstraints { (make) in
                make.top.trailing.bottom.leading.equalToSuperview()
            }
        }
        vc.didMove(toParent: self)
        self.activeChildVC = vc
    }
    
    func detachVCFromLeftMenu(vc: UIViewController) {
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
        self.activeChildVC = nil
    }
    
    func videoIsPlaying() -> Bool {
        var shouldResume = false
        if self.currentPlayer == .youtube {
            shouldResume = (self.currentYTState != .unstarted || self.currentYTState != .paused || self.currentYTState != .unknown)
        } else {
            shouldResume = self.player.timeControlStatus == .playing
        }
        return shouldResume
    }
    
    func videoPlaying() {
        if isFirstVideo {
            print("did adv to firstvideo pause")
            self.pauseVideo()
            self.player.pause()
            isFirstVideo = false
        }else{
            self.checkWifi()
        }
    }
    
    func checkWifi() {

            if NetworkReachabilityManager()!.isReachableOnWWAN {
                // .ReachableViaWWAN
                if (OONASettings.manager.settingsObject.streamingOnlyWifi) {
                    self.pauseVideo()
                    let alert = EMAlertController(title: "", message: LanguageHelper.localizedString("you_re_not_connected_to_the_wifi"))
                    alert.backgroundColor = .black
                    alert.messageColor = .white
                    alert.buttonSpacing = 0
                    let cancel = EMAlertAction(title: LanguageHelper.localizedString("play_games"), style: .normal) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            self.showGames()
                        })
                    }
                    cancel.titleColor = UIColor.white
                    cancel.actionBackgroundColor = UIColor.black
                    let confirm = EMAlertAction(title: LanguageHelper.localizedString("go_to_settings"), style: .normal) {
                        // Perform Action
//                        self.tempWifiRestrict = false
//                        self.resumeVideo()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            self.pauseVideo()
                            self.showSettings(completion: {
                                self.resumeVideo()
                            })
                        })
                        
                    }
                    confirm.titleColor = UIColor.white
                    confirm.actionBackgroundColor = UIColor.black
//                    alert.addAction(cancel)
                    alert.addAction(confirm)
                    self.present(alert, animated: true, completion: nil)
                }

            }
        }
    
    func showCloseRadioPopup(){
        let customPopupViewContoller = UIStoryboard(name: "NewRadio", bundle: nil).instantiateViewController(withIdentifier: "CustomPopupViewContoller") as! CustomPopupViewContoller
        self.present(customPopupViewContoller, animated: true, completion: nil)
    }
    
}


