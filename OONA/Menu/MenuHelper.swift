//
//  MenuHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation

class MenuHelper: NSObject {
    static let shared = MenuHelper()
    var selectedMenuId = Int()
    var currentSubMenu = SubMenu.tv

    func getCurrentSubMenu() -> SubMenu{
        return currentSubMenu
    }
    
    func getCategoryList(type: String? ,success :@escaping ([Menu]) -> (), failure : @escaping ((Any) -> ())){
        OONAApi.shared.postRequest(url: APPURL.Category, parameter: ["type":type]).responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let JSON = response.result.value as! NSDictionary? {
                        let code : Int? = JSON["code"] as? Int
                        let status : String? = JSON["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    if let menus :  [NSDictionary] = JSON["categories"] as? [NSDictionary]{
                                        var result = [Menu]()
                                        for _menu in menus {
                                            if(_menu.count > 0){
                                                var menu = Menu.init(withDict: _menu)
                                                if(menu.id == self.selectedMenuId){
                                                    //menu.selected = true
                                                }
                                                result.append(menu)
                                            }
                                        }
                                        success(result)
                                    }
                                }
                            })
                        }
                    }
                }else{
                    //failure()
                }
        }
    }
}

