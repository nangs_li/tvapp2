//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import youtube_ios_player_helper
import SwiftyGif
class MenuViewController: BaseViewController, UIScrollViewDelegate,BCOVPlaybackControllerDelegate, YTPlayerViewDelegate{
    
    let defaultPlayerVars : [String : Any] = [
        "controls" : 0,
        "playsinline" : 1,
        "autohide" : 1,
        "showinfo" : 0,
        "modestbranding" : 1,
        "origin" : "http://www.youtube.com"
    ]
    var currentYoutubeId: String?
    
    var subMenu: SubMenu?
    
    let manager: BCOVPlayerSDKManager = BCOVPlayerSDKManager.shared()
    var playbackService: BCOVPlaybackService?
    var playbackController: BCOVPlaybackController?
    var fairplayProxy : BCOVFPSBrightcoveAuthProxy?
    
    var playerVMDisposeBag = DisposeBag()
    var previewPlayer : AVPlayer?
    var previewLayer : AVPlayerLayer?
    var categoryViewController : CategoryViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seperator: UIView!
    
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var cvView: UIView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewVideoView: UIView!
    @IBOutlet weak var previewYoutubeView: YTPlayerView!
    
    @IBOutlet weak var seperatorLeftPadding: NSLayoutConstraint!
    
    /**description section**/
    @IBOutlet weak var descriptionWrapper: UIView!
    @IBOutlet weak var descriptionTxtView: UITextView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionSubTitle: UILabel!
    
    @IBOutlet weak var playerErrorView: UIView!
    @IBOutlet weak var playerErrorImageView: UIImageView!
    
    /**youtube section**/
    @IBOutlet weak var youtubeView: UIView!
    
    /**selected**/
    var category : Category?
    var episode : Episode?
    var channel : Channel?
    var video : Video?
    var currentPlayer : PlayerType = .bco
    var firstTime = false
    var isViewAppearFirstTime = false
    
    var backButtonTextObservation: NSKeyValueObservation?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //hidePreview()
        stillInTop = true
        
        if let _channel = channel{
            
            showPreview(channel: _channel , episode : episode ?? nil)
            
            print("route x 4")
        }
        
        descriptionTxtView.centerVertically()
        
        
        initActiveYoutubeView() //check if current video is youtube then show the view
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initAllView()
        self.initYoutubeView()

        self.cvView.isHidden = false
        self.categoryView.isHidden = false
        
        let subMenu : SubMenu = MenuHelper.shared.getCurrentSubMenu()
        titleLabel.isHidden = true
        if(subMenu == .home){
            self.category = Category.init(id: -96 ,name: "New", _type: .category)
            //setCategoryWithCategoryId(_index: _subMenuType.rawValue)
            //self.category = Category.init(id: subMenu.rawValue ,name: subMenu.description)
            loadCategory(category: self.category)
        }else if(subMenu == .music){
            //setCategoryWithCategoryId(_index: _subMenuType.rawValue)
            
            self.category = Category.init(id: 15 ,name: "Music", _type: .category)
            loadCategory(category: self.category)
            
            titleLabel.isHidden = false
         
            
            //if let _categoryName = self.category?.name{
            //    self.categoryViewController?.setBackButtonText(text: " < Back to " + _categoryName)
            //}else{
                //print("no cat name 1")
            //}
            self.categoryViewController?.disableCategory()
            //self.categoryViewController?.enableBackView()
            self.backButtonTextObservation?.invalidate()
            self.backButtonTextObservation = self.categoryViewController?.backButtonWrapper.observe(\.isHidden, options: [.old, .new], changeHandler: { [weak self] (backView, change) in
                UIView.animate(withDuration: 0.3,
                               animations: {
                                self?.titleLabel.isHidden = !backView.isHidden
                })
            })
        }else{
            
            loadCategory(category: self.category)
        }
        loadDescription()
        loadChannel(category: self.category)
        
        //categoryViewController?.setSelectedIndex(catid: 6)
        
        self.initPlayer()
        self.initBco()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        seperatorLeftPadding.constant = self.getLeftConstraint() + APPSetting.episodeChannelObjectWidth + getSeperatorExtraConstraint()
        
            /*
            if let _subMenuType = self.subMenu{
                if(_subMenuType == .music){
                    setCategoryWithCategory(_index: )
                    self.category = Category.init(id: _subMenuType.rawValue ,name: _subMenuType.description)
                }
            }*/
        //setCategoryWithCategory(cat: (self.category ?? nil)!)

        let subMenu : SubMenu = MenuHelper.shared.getCurrentSubMenu()
        
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        self.deactiveYoutube()
//        self.deactiveAvplayer()
//    }
    
    //func setCategoryWithCategory(cat: Category){
        //categoryViewController?.setCategory(cat: cat)
    //}
    /*
    func getPreviewRightConstraint() -> CGFloat{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                return 372
            case 1334:
                //print("iPhone 6/6S/7/8")
                return 372
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return 372
            case 2436: break
            //print("iPhone X, XS")
            case 2688: break
            //print("iPhone XS Max")
            case 1792: break
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        return 372
    }*/
    
    func getSeperatorExtraConstraint() -> CGFloat{
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                return 23
            case 1334:
                //print("iPhone 6/6S/7/8")
                return 23
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return 23
            case 2436: return 6
            //print("iPhone X, XS")
            case 2688: return 6
            //print("iPhone XS Max")
            case 1792: return 6
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        
        return 11
        
    }
    var stillInTop : Bool = false
    func disablePreview(){
//        self.previewPlayer.pause()
        self.previewView.cancelLoading()
        self.previewPlayer?.replaceCurrentItem(with: nil)
        self.previewPlayer = nil
        self.previewYoutubeView.stopVideo()
        self.previewYoutubeView.load(withVideoId: "", playerVars:defaultPlayerVars)
        self.previewLayer?.removeFromSuperlayer()
        
    }
    func stopPreview() {
        
        self.previewPlayer?.replaceCurrentItem(with: nil)
        self.previewPlayer = nil
        self.previewYoutubeView.stopVideo()
        self.previewYoutubeView.load(withVideoId: "", playerVars:defaultPlayerVars)
    }
    func hidePreview(){
        self.previewView.isHidden = true
    }
    
    func hideYoutubeView(){
        self.youtubeView.isHidden = false
    }
    
    func showPreview(channel: Channel , episode : Episode?){
        hideDescription()
        if !isViewAppearFirstTime && MenuHelper.shared.getCurrentSubMenu() != .music {
            isViewAppearFirstTime = true
        } else {
        if (episode != nil) {
            if let _episode = episode{
                EpisodeHelper.shared.getVideoByEpisode(channel: channel , episode: _episode, completion: { (success, video :Video?, error: Error?) in
                    
                    if success {
                
                        if let _video = _episode.video{
                            print("_episode.video name",_episode.video?.videoName)
                            self.channel = channel
                            self.episode = episode
                            if(self.playPreviewWithVideo(video: _video)){
                                //success
                            }
                        }else{
                            print("no video")
                        }
                    } else {
                        // download fail
                    }
                    
                })
            }
        }else{
            ChannelHelper.shared.getVideoByChannel(channel: channel, completion: { (success, video :Video?, error: Error?) in
                if success {
                    if let _video = channel.video{
                        self.channel = channel
                        self.episode = episode
                        if(self.playPreviewWithVideo(video: _video)){
                            //success
                            
                        }else{
                            //fail
                            
                            //get first episode for channel can there's no preview in channel
                            EpisodeHelper.shared.getFirstEpisodeListByChannel(channel:channel, success: { episode in
                                if let _preview = episode.video{
                                    self.channel = channel
                                    self.episode = episode
                                    if(self.playPreviewWithVideo(video: _preview)){
                                        
                                    }
                                }
                            }, failure: { error in
                                //fail handling
                                self.errorOnPreview()
                                print("no preview!")
                            })
                        }
                    }else{
                        self.errorOnPreview()
                        print("no video")
                    }
                //} else {
                    // download fail
                }
                
            })
        }
        }
    }
    func errorOnPreview() {
        print("errorOnPreview")
    }
    func deactiveAvplayer(){
        
        print("deactiveBC")
        
        if let pp = previewPlayer{
            print("mute and pause the av player")
//            pp.isMuted = true
            pp.pause()
        }else{
            print("fail to pause the av player")
        }
        previewPlayer?.replaceCurrentItem(with: nil)
//        previewPlayer = nil
//        previewLayer = AVPlayerLayer(player: previewPlayer)
        
    }
    
    func deactiveYoutube(){
        //youtube
        print("deactiveYoutube")
        
        currentYoutubeId = nil
        
        if previewYoutubeView != nil{
            previewYoutubeView.pauseVideo()
        }

    }
    
    func playPreviewWithVideo(video: Video)->Bool{
       //set video to local used later when playing
        self.video = video
        self.playerErrorView.isHidden = true
        print("current preview video name",video.videoName)
        
        if let _url = video.streamingUrl{
            
            
            print("play preview = route1")
            //live
            deactiveYoutube()
                
            if let __url = URL(string: _url){
                
                self.playPreviewWithURL(url: __url)
                self.showPreviewLayout(mode: .streamingLink)
                currentPlayer = .avplayer
                return true
            }else{
                print("url invalid")
                return false
            }
        }else if let _brightcoveId = video.brightcoveId{
            
            
            print("play preview = route2")
        
            deactiveYoutube()
            
            print("no url")
            
            self.playPreview(brightcoveId: _brightcoveId)
            self.showPreviewLayout(mode: .brightCove)
            currentPlayer = .bco
            return true
        }else if let _youtubeId = video.youtubeId{
            print("play preview = route3")
            
            deactiveAvplayer()
            
            
                self.playPreviewWithYoutubeId(youtubeId: _youtubeId)
                self.showPreviewLayout(mode: .youtube)
//                print("yt id",_youtubeId)
                currentPlayer = .youtube
                return true
            
        }else{
            self.playerErrorView.isHidden = false
            return false
        }
        return false
    }
    func playPreviewWithYoutubeId(youtubeId: String?){
        enableBackgorundPlayingLayout()
        
        if let _episode = self.episode{
            EpisodeHelper.shared.setCurrentEpisode(episode: _episode)
        }else{
            
            EpisodeHelper.shared.clearEpisodeFromStorage()
        }
        if let _channel = self.channel{
            ChannelHelper.shared.setCurrentChannel(channel: _channel)
        }else{
            
            ChannelHelper.shared.clearChannelFromStorage()
        }
        if let _category = self.category{
            CategoryHelper.shared.setCurrentCategory(_categoryId: _category.id)
        }else{
            
            CategoryHelper.shared.clearCategoryFromStorage()
        }
        
        if let _youtubeId = youtubeId {
            print("play preview = route3 - inner")
            self.playVideo(youtubeId:_youtubeId)
            self.previewView.startLoading()
        }
    }
    
    func enableBackgorundPlayingLayout(){
        //previewRightConstraint.constant = 0
        //youtubeView.isHidden = true
        hidePreview()
        self.view.layoutIfNeeded()
    }
    func hasYoutubeView()->Bool{
        var isHidden = !youtubeView.isHidden
        return isHidden
    }
    func enableNormalLayout(){
        //youtubeView.isHidden = false
        /*
        if(hasYoutubeView()){
            previewRightConstraint.constant = self.getPreviewRightConstraint()
        }else{
            previewRightConstraint.constant = 0
        }*/
        self.view.layoutIfNeeded()
    }
    
    func playPreviewWithURL(url : URL){
        enableNormalLayout()
        self.initPlayer() //TODO_OONA : no need to init everytime
        
        print("deactiveYoutube - route2")
//        self.previewYoutubeView.pauseVideo()
//        previewPlayer?.replaceCurrentItem(with: AVPlayerItem(url: url))
        self.playbackController?.setVideos([BCOVVideo.init(hlsSourceURL: url)] as NSFastEnumeration)
        self.playbackController?.play()
        self.playbackController?.delegate = self
        self.previewView.startLoading()
        
//        previewPlayer?.play()
    }
    func initPlayer(){
        //previewView.layer.cornerRadius = APPSetting.defaultCornerRadius
        if let _previewVideoView = previewVideoView {
            //_previewVideoView.layer.cornerRadius = APPSetting.defaultCornerRadius
            previewPlayer?.replaceCurrentItem(with: nil)
            previewPlayer = nil
            previewPlayer = AVPlayer()
//            previewPlayer!.isMuted = true
            previewLayer?.removeFromSuperlayer()
            previewLayer = nil
            previewLayer = AVPlayerLayer(player: previewPlayer)
            
            
           
            if let _previewPlayer = previewPlayer {
                _previewPlayer.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)
                if #available(iOS 10.0, *) {
                    _previewPlayer.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
                } else {
                    _previewPlayer.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
                }
            }
            previewLayer!.videoGravity = .resizeAspectFill
            previewLayer!.frame = self.previewVideoView.bounds
//            previewPlayer?.isMuted = true
            self.previewVideoView.layer.addSublayer(previewLayer!)
            //self.previewView.isHidden = false
            
        }

        
    }
    func initBco() {
        fairplayProxy = BCOVFPSBrightcoveAuthProxy.init(publisherId: nil, applicationId: nil)
        let basicSessionProvider :BCOVBasicSessionProvider = manager.createBasicSessionProvider(with: nil) as! BCOVBasicSessionProvider
        
        let fairplaySessionProvider = manager.createFairPlaySessionProvider(withApplicationCertificate: nil, authorizationProxy: fairplayProxy!, upstreamSessionProvider: basicSessionProvider)
        playbackController = manager.createPlaybackController(with: fairplaySessionProvider, viewStrategy: nil)
        playbackController?.delegate = self
        playbackController?.isAutoAdvance = true
        playbackController?.isAutoPlay = true
        
        // Prevents the Brightcove SDK from making an unnecessary AVPlayerLayer
        // since the AVPlayerViewController already makes one
        playbackController?.options = [ kBCOVAVPlayerViewControllerCompatibilityKey : true ]
        
        playbackService = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        
    }
    func playPreview(brightcoveId : String){
        
        print("playPreview(brightcoveId : String){")
        enableNormalLayout()
        previewPlayer?.replaceCurrentItem(with: nil)
        previewPlayer = nil
        previewPlayer = AVPlayer()
        
        
        
        if let _previewPlayer = previewPlayer{
            _previewPlayer.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)
            if #available(iOS 10.0, *) {
                _previewPlayer.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
            } else {
                _previewPlayer.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
            }
        }

        previewLayer?.removeFromSuperlayer()
//        previewPlayer?.isMuted = true
        previewLayer = AVPlayerLayer(player: previewPlayer)
        previewLayer!.videoGravity = .resizeAspectFill
        previewLayer!.frame = self.previewVideoView.bounds
        self.previewVideoView.layer.addSublayer(previewLayer!)
        previewPlayer!.play()
        self.previewView.isHidden = false
        

        
        
//
//        playbackController?.play()
//        playbackController?.delegate = self
        let service = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        service?.findVideo(withVideoID: brightcoveId, parameters: [:], completion: { (video, jsonResponse, error) in
            print("find video")
            if let _video = video {
                self.playbackController?.setVideos([_video] as NSFastEnumeration)
                self.playbackController?.delegate = self
                self.playbackController?.play()
                self.previewView.startLoading()
            }
        })
        
    }
    
    func playbackController(_ controller: BCOVPlaybackController!, didAdvanceTo session: BCOVPlaybackSession!) {
        print("MenuViewController Debug - Advanced to new session.")
        playerVMDisposeBag = DisposeBag()
        let viewModel = PlayerViewModel(player: session.player)
        session.player.currentItem?.preferredForwardBufferDuration = 1;
        self.previewView.startLoading()
        viewModel.current.drive(onNext: { (progress) in
            // Check video duration here.
//            print("drive menu")
            
            if !self.stillInTop {
                print("stillInTop menu")
                session.player.pause()
            }
            //            self.videoPlaying()
        }).disposed(by: playerVMDisposeBag)
        
        viewModel.error.drive(onNext: { (error) in
            print(error)
            self.previewView.cancelLoading()
//            self.showAlert(title: "Oops", message: "Unable to load video")
            self.playerErrorView.isHidden = false
        }).disposed(by: playerVMDisposeBag)
        
        if (stillInTop) {
            print("Advanced to new session. on top")
////            session.player.isMuted = true
//            self.previewLayer!.player?.replaceCurrentItem(with: nil)
            self.previewLayer!.player  = session.player
            if(previewView.isHidden){
                previewView.isHidden = false
            }
        }
    }
    
    func playbackController(_ controller: BCOVPlaybackController!, playbackSession session: BCOVPlaybackSession!, didProgressTo progress: TimeInterval) {
//        print(#function + "Progress \(progress)")
        if let currentItem = session.player?.currentItem,
            currentItem.isPlaybackLikelyToKeepUp {
            self.previewView.cancelLoading()
        }
    }
    func showPlayerErrorView() {
        self.playerErrorView.isHidden = false
        
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as AnyObject? === previewPlayer {
            if keyPath == "status" {
                if let _previewPlayer = previewPlayer{
                    if _previewPlayer.status == .readyToPlay {
                        _previewPlayer.play()
                        showPreviewView()
                        self.previewView.cancelLoading()
                    }
                }
            } else if keyPath == "timeControlStatus" {
                if #available(iOS 10.0, *) {
                    if let _previewPlayer = previewPlayer{
                        if _previewPlayer.timeControlStatus == .playing {
                            //videoCell?.muteButton.isHidden = false
                            showPreviewView()
                            self.previewView.cancelLoading()
                        } else {
                            // videoCell?.muteButton.isHidden = true
                        }
                    }
                }
            } else if keyPath == "rate" {
                if let _previewPlayer = previewPlayer{
                    if _previewPlayer.rate > 0 {
                        //videoCell?.muteButton.isHidden = false
                    } else {
                        //videoCell?.muteButton.isHidden = true
                    }
                }
            }
        }
    }
    
    func showPreviewView(){
        print("showPreview")
        previewVideoView.isHidden = false
        previewView.isHidden = false
    }
    
    func loadDescription(){
        /*
        if(self.descriptionView.isViewEmpty){
            var descriptionvc : DescriptionViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DescriptionViewController") as! DescriptionViewController
            //descriptionvc.completionBlock = {(dataReturned) -> () in
            //   descriptionvc.view.removeFromSuperview()
            //}
            self.addChild(descriptionvc)
            self.descriptionView.addSubview(descriptionvc.view)
        }*/
        if let channel = ChannelHelper.shared.getChannel(){
            if let episode = EpisodeHelper.shared.getEpisode(){
                descriptionTxtView.text = episode.description
                descriptionTitle.text = episode.name
            }else if let channel = ChannelHelper.shared.getChannel(){
                descriptionTxtView.text = channel.description
                descriptionTitle.text = channel.name
            }
            
            if(channel.seasons.count > 0){
                descriptionSubTitle.text = channel.seasons.count.description + "|" //TODO_OONA : year , age restricition
            }else{
                descriptionSubTitle.text = ""

            }
            
        }
        
    }
    func showDescription(){
        self.descriptionWrapper.isHidden = false
    }
    func hideDescription(){
        self.descriptionWrapper.isHidden = true
    }
    
    func loadChannel(category: Category?){
            let channelViewController : ChannelViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChannelViewController") as! ChannelViewController
            channelViewController.category = category
        channelViewController.completionBlock = {(action: CBAction, channel, episode) -> () in
                self.stopPreview()
                self.playerErrorView.isHidden = true
                self.playerErrorImageView.sd_setImage(with: URL.init(string: channel.imageUrl), completed: nil)
                if(action == .nowPlaying){
                    // hide description.
//                    self.hidePreview()
//                    self.loadDescription()
//                    self.showDescription()
                    
                }else if(action == CBAction.load){
                    if(channel.type==ChannelSetting.channelType.linear.rawValue){
                        //get the first episode from channel and show preview..
                        
                        //set selected channel
                        self.channel = channel
                        self.showPreview(channel: channel, episode: nil)
                        
                    }else{
                        self.showPreview(channel: channel, episode: nil)
                    }
                }else if(action == CBAction.select){
                    
                    //set selected channel
                    self.channel = channel
                    self.showPreview(channel: channel, episode: nil)

                }
            }
            self.addChild(channelViewController)
            self.cvView.addSubview(channelViewController.view)
    }
    
    func loadEpisode(channel: Channel){
        //actually is load eipsode or mix
            //show back button and seperator if load episode
            if let _categoryName = self.category?.name{
                self.categoryViewController?.setBackButtonText(text: " < \(LanguageHelper.localizedString("back_to").escapeHTMLEntities()) " + _categoryName)
            }else{
                //print("no cat name 3")
            }
            self.categoryViewController?.enableBackView()
            self.showSeperator()
            
            let episodeViewController: EpisodeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EpisodeViewController") as! EpisodeViewController
        episodeViewController.completionBlock = {(action :CBAction, episode) -> () in
                    self.disablePreview()
            
                    if(action == .nowPlaying){
                        //self.hidePreview()
                        //self.loadDescription()
                        //self.showDescription()
                    }else{
                        if(action == .load){
                            if let _episode = episode {
                                self.channel = channel
                                self.episode = _episode
                                self.showPreview(channel: channel, episode:_episode)
                            }
                        }else{
                            //back case
                            print("deactiveYoutube - route7")
                            self.deactiveYoutube()
                            self.deactiveAvplayer()
                            self.channel = channel
                            //if nil , back case
                            self.loadChannel(category: self.category)
                        }
                    }
                }
        
            episodeViewController.channel = channel
            self.addChild(episodeViewController)
            self.cvView.addSubview(episodeViewController.view)
        
    }
    func loadCategory(category : Category?){
        
        let subMenu : SubMenu = MenuHelper.shared.getCurrentSubMenu()
        
        CategoryHelper.shared.currentCategoryType = subMenu.categoryId
        
        if(CategoryHelper.shared.currentCategoryType == "" || CategoryHelper.shared.currentCategoryType == nil) {
            //set default value: tv
            CategoryHelper.shared.currentCategoryType =
                SubMenu.tv.categoryId
        }
        
        categoryViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoryViewController") as? CategoryViewController
        
        if let _category = category {
            categoryViewController?.setCategory(cat : _category)
        }
        
        if let _catcv = self.categoryViewController{
            _catcv.completionBlock = {(_category) -> () in
                
                
                //on click category
                self.disablePreview()
                self.hidePreview()
                
                self.initCVView()
                if let category = _category{
                    //not a back case
                    self.category = category
                }
                //CategoryHelper.shared.current = category.id
                
                self.enableNormalLayout()
                
                self.episode = nil
                self.channel = nil
                
                print("deactiveYoutube - route8")
                self.deactiveYoutube()
                self.deactiveAvplayer()
                self.loadChannel(category: self.category)
            }
            self.addChild(_catcv)
            
            self.categoryView.addSubview(_catcv.view)
        }
    }
    
    func showSeperator(){
        self.seperator.isHidden = false
    }
    func hideSeperator(){
        self.seperator.isHidden = true
    }
    func clearCvView(){
        hideSeperator()
        self.cvView.subviews.forEach { $0.removeFromSuperview() }
    }
    func clearCategoryView(){
        self.categoryView.subviews.forEach { $0.removeFromSuperview() }
    }
    
    func initCVView(){
        
        clearCvView()
    }
    
    @IBOutlet weak var goBackLabel: UILabel!
    @IBOutlet weak var goBackImage: UIImageView!
    func initAllView(){
        
        clearCvView()
        clearCategoryView()
        
        do {
            let gif = try UIImage(gifName: "gesture_tap")
            goBackImage.setGifImage(gif)
//            goBackImage.loopCount = 99999
//            goBackImage.delegate = self
            
        } catch  {
            print(error)
        }
        self.goBackLabel.text = LanguageHelper.localizedString("Tap_to_return_to_now_playing")
    }
    
    func initActiveYoutubeView(){
        if let video = PlayerHelper.shared.currentVideo{
            if let youtubeId = video.youtubeId {
                //check if is youtube, if yes enable the cover black view
                self.youtubeView.isHidden = false
            }else{
                self.youtubeView.isHidden = false
            }
        }else{
            self.youtubeView.isHidden = false
        }
    }
    
    @IBAction func clickIcon(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        self.initAllView()
        //self.loadTV()
        exitMenu()
    }
    
    @IBAction func clickPreview(_ sender: UIButton) {
        
        if let _video = self.video{
            self.exitMenu()
            //EpisodeHelper.shared.currentEpisode = self.episode
            //ChannelHelper.shared.currentChannel = self.channel
            if let episode = self.episode{
                EpisodeHelper.shared.setCurrentEpisode(episode: episode)
            }else{
                EpisodeHelper.shared.clearEpisodeFromStorage()
            }
            
            if let channel = self.channel{
                ChannelHelper.shared.setCurrentChannel(channel: channel)
            }else{
                ChannelHelper.shared.clearChannelFromStorage()
            }
            
            if let _category = self.category{
                CategoryHelper.shared.setCurrentCategory(_categoryId: _category.id)
            }else{
                
                CategoryHelper.shared.clearCategoryFromStorage()
            }
            
     
            if let episode = self.episode {
            
                EpisodeHelper.shared.updateVideoByStreamingInfo(video:_video, channel: self.channel!, episode: self.episode!, completion: { (success, __video, error) in
                guard let _ = __video else {
                    print("updateVideoByStreamingInfo unsuccess")
                    return }
                print("getVideoByChannel")
                PlayerHelper.shared.currentVideo = __video
                    PlayerHelper.shared.videoControls.accept([.Default, .Previous, .Next])
                    PlayerHelper.shared.playVideo(video: __video!)
                //                                    self.afterAutoStartVideo()
                
            })
            }else {

                ChannelHelper.shared.updateVideoByStreamingInfo(video:_video, channel: self.channel!, completion: { (success, __video, error) in
                print("getVideoByChannel")
                PlayerHelper.shared.currentVideo = __video
                    PlayerHelper.shared.videoControls.accept([.Default, .Previous, .Next])
                PlayerHelper.shared.playVideo(video: __video!)
            })
            
            }
            
            
//            PlayerHelper.shared.playVideo(video: _video)
        }
        
        if let _episode = self.episode{
         
        }else if let _channel = self.channel{
            if(_channel.type==ChannelSetting.channelType.live.rawValue){
                
               
                
            }else{
                
                self.initCVView()
                self.loadEpisode(channel: _channel)
                
             
                
//                self.clickPreview(UIButton())
            }
        }
    }
    
    @IBAction func clickBackgroundButton(_ sender: UIButton) {
        PlayerHelper.shared.resetYTView()
        self.disablePreview()
        exitMenu()
        
        if (PlayerHelper.shared.currentVideo == nil) {
            self.clickPreview(UIButton())
        }
    }
    
    func exitMenu() {
        stillInTop = false
        if (currentPlayer == .youtube) {
            print("deactiveYoutube - route3")
            previewYoutubeView.pauseVideo()
            previewYoutubeView.stopVideo()
            
            //previewYoutubeView = nil
        }
        
        self.deactiveAvplayer()
        self.previewLayer?.removeFromSuperlayer()
        self.previewPlayer?.replaceCurrentItem(with: AVPlayerItem.init(url: URL.init(string: "gg")!))
        
        //don't know what for
        currentYoutubeId = nil
        
        self.hidePreview()
        PlayerHelper.shared.exitTVMenu.accept(true)
//        self.dismiss(animated: true)
        //            hideLeftMenuDetail(animated: true)
    }
    
    func showPreviewLayout(mode : VideoMode){
        self.previewView.isHidden = false
        if(mode == .youtube){
            self.previewYoutubeView.isHidden = false
            self.previewVideoView.isHidden = true
        }else{
            self.previewYoutubeView.isHidden = true
            self.previewVideoView.isHidden = false
        }
    }
    
    override func appLanguageDidUpdate() {
        self.goBackLabel.text = LanguageHelper.localizedString("Tap_to_return_to_now_playing")
        if MenuHelper.shared.getCurrentSubMenu() == .music {
            self.titleLabel.text = LanguageHelper.localizedString("MUSIC")
        } else {
            self.titleLabel.text = ""
        }
        
        if let controller = self.categoryViewController,
            controller.backButtonEnabled,
            let _categoryName = self.category?.name {
            self.categoryViewController?.setBackButtonText(text: " < \(LanguageHelper.localizedString("back_to").escapeHTMLEntities()) " + _categoryName)
        }
    }
    
    deinit {
        self.backButtonTextObservation?.invalidate()
        self.backButtonTextObservation = nil
    }
}

extension UIView {
    var isViewEmpty : Bool {
        return  self.subviews.count == 0 ;
    }
}

extension UITextView {
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale)
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
    
}

