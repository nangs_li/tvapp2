//
//  PlayerViewController+Youtube.swift
//  OONA
//
//  Created by Jack on 16/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import youtube_ios_player_helper

extension MenuViewController {
    
    func initYoutubeView() {
        self.youtubeView.isHidden = false
        let playerVars = defaultPlayerVars
        
        //        youtubeView.load(withVideoId: "20k4Pa8TzdY",playerVars:playerVars)
        
        previewYoutubeView.delegate = self
        
    }
    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
        return UIColor.clear
    }
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        print("become rdy")
        if (currentPlayer == .youtube){
            print("playVideo route1")
            previewYoutubeView.playVideo()
        }
    }
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
        self.youtubePlayerChangeToState(state:state)
    }
    func youtubePlayerChangeToState(state:YTPlayerState) {
        print("YTPlayerView State: \(state.rawValue)")
        if state == .playing {
            self.previewView.cancelLoading()
        }
        
        
    }
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        self.showPlayerErrorView()
    }
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        //        print("YT didPlayTime",playTime)
        //        print("YT current PlayTime",playerView.currentTime())
        if (currentPlayer == .bco || currentPlayer == .avplayer ) {
            print("deactiveYoutube - route4")
            previewYoutubeView.pauseVideo()
        }
    }
    func playVideo(youtubeId:String) {
        
        let playerVars = defaultPlayerVars
        print("youtubeId",youtubeId)
        
        if(currentYoutubeId != youtubeId){
            currentYoutubeId = youtubeId
            print("deactiveBC - route2")
            //previewPlayer?.pause()
            //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(!firstTime){
                
                //first load only
                //previewYoutubeView.stopVideo()
                
                print("LoadYoutube - route5")
                self.previewYoutubeView.stopVideo()
                self.previewYoutubeView.load(withVideoId: youtubeId,playerVars:playerVars)
                self.previewYoutubeView.delegate = self
                
                self.previewYoutubeView.playVideo()
                
                firstTime = true
                
            }else{
                print("LoadYoutube - route6")
                if(previewYoutubeView == nil){
                    
                }
                self.previewYoutubeView.stopVideo()
                self.previewYoutubeView.load(withVideoId: youtubeId,playerVars:playerVars)
//                self.previewYoutubeView.cueVideo(byId: youtubeId, startSeconds: 0, suggestedQuality: YTPlaybackQuality.medium)
                
                //print("playVideo route3")
                self.previewYoutubeView.delegate = self
                self.previewYoutubeView.playVideo()
                //isViewAppearFirstTime = false
                
                /*
                if(isViewAppearFirstTime){
                    
                    print("playVideo route5")
                    self.previewYoutubeView.playVideo()
                    isViewAppearFirstTime = false
                }else{
                    print("StopYoutube - route6")
                    
                    //non- first load only
                    //previewYoutubeView.stopVideo()
                    
                    self.previewYoutubeView.cueVideo(byId: youtubeId, startSeconds: 0, suggestedQuality: YTPlaybackQuality.small)
                    print("playVideo route3")
                    self.previewYoutubeView.playVideo()
                    isViewAppearFirstTime = false
                }*/
            }
            //}
            showPreviewLayout(mode: .youtube)
            previewView.isHidden = false
        }else{
            self.previewYoutubeView.playVideo()
            
        }
    }
}
