//
//  Category.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation

struct Menu {
    var id : Int = 0
    var name : String = ""
    
    init(withDict obj:NSDictionary){
        if let _id : Int = obj["id"] as? Int{
            self.id = _id
        }
        if let _name : String  = obj["name"] as? String{
            self.name = _name
        }
    }
    init(id: Int ,name: String){
        self.id = id
        self.name = name
    }
}
