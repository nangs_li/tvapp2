//
//  Timer.swift
//  OONA
//
//  Created by Jack on 5/6/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
typealias CompletionHandler = () -> Void
class OONATimer {
    private var timerPause : Bool = false
    private var isSuspended : Bool = false
    private var countDown : Int = 0
    private var completeAction : CompletionHandler = {}
    public var identifier : String = ""
    public func startTimer(seconds : Int,
                           action : @escaping CompletionHandler ) {
        countDown = seconds
        timerPause = false
        completeAction = action
        timerLoop(action: action)
    }
    public func pauseTimer() {
        timerPause = true
        
    }
    public func resumeTimer() {
        if (timerPause) {
            timerPause = false
            startTimer(seconds: countDown, action: completeAction)
        }
    }
    public func suspend() {
        isSuspended = true
    }
    private func timerLoop(action : @escaping CompletionHandler ) {
        // this function will auto retrigger at every second with Highest thread priority.
        if isSuspended {
            return ;
        }
        if !timerPause{
            countDown -= 1
            print(identifier," timer : ", countDown)
            if (countDown == 0){
                timerPause = true
                action()
                isSuspended = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.timerLoop(action:action)
            }
        }
        
    }
}
