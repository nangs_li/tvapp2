//
//  DisplayAdHelper.swift
//  OONA
//
//  Created by Jack on 5/6/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON
/// This class is for showing display-type ADs
class DisplayAdHelper {
    
    private var adContainer : UIView?
    private var adButton : UIButton?
    
    public func requestDisplayAd(video : Video) {
        OONAApi.shared.postRequest(url: APPURL.displayAds(video:video),
                                   parameter: ["intervalFormat":"regular",
                                               "latitude":LocationHelper.shared.getLat(),
                                               "longitude":LocationHelper.shared.getLong(),
                                               "supportSpotX":"1"
            ]).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                case .failure(let error as NSError):
                    print(error)
                }
        }
    }
    
    
    private func fetchDisplayAd(currentVideo:Video) {
       
    }
}
