//
//  AdHelper.swift
//  OONA
//
//  Created by Jack on 27/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SwiftyJSON
import YLProgressBar
import GoogleInteractiveMediaAds
import InMobiSDK
import iSoma
import SpotX
import SwiftyGif
import os.log

/// This class is for playing video-type ADs
class Reward {
    var points : Int = 0
    var balance : Int = 0
    
    init(pt:Int, bal:Int) {
        self.points = pt
        self.balance = bal
    }
}
class VideoAdHelper : UIViewController,
IMAAdsLoaderDelegate,IMAAdsManagerDelegate,IMInterstitialDelegate,SOMAAdViewDelegate,SpotXAdPlayerDelegate{
    
    var rewardReceived: PublishRelay<Reward?> = PublishRelay<Reward?>()
    
    var isGameAd : Bool = false
    private var videoView : UIView = UIView()
    public var playerView : UIView = UIView()
    private var timerPause : Bool = false
    private var countDown : Int = 0
    private var currentVideo : Video?
    private var currentGame : Game?
    private var currentAds : VideoAds?
    private var currentAdItem : OONAAd?
    private var trialCounter : Int = 0
    private let _adDidClose = PublishSubject<Void>()
    private var currentAdType : AdType = .preroll
    private var adDuration : Int = 0
    var adClose : Observable<Void> { return _adDidClose.asObservable() }
    
    private let _adDidStart = PublishSubject<Void>()
    var adStart : Observable<Void> { return _adDidStart.asObservable() }
    private let disposeBag = DisposeBag()
    
    var progressBar : YLProgressBar?
    var rewardLabel : UILabel?
    // InMobi SDK start
    var interstitial: IMInterstitial?
    
 
    /**
     * Notifies the delegate that the ad server has returned an ad. Assets are not yet available.
     * Please use interstitialDidFinishLoading: to receive a callback when assets are also available.
     */
    public func interstitialDidReceiveAd(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialDidReceiveAd")
    }
    /**
     * Notifies the delegate that the interstitial has finished loading and can be shown instantly.
     */
    public func interstitialDidFinishLoading(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialDidFinishLoading")
        self.adDidShown()
        self.interstitial?.show(from: self.parent)
        self.interstitial?.show(from: self.parent, with: .flipHorizontal)
        
        
    }
    /**
     * Notifies the delegate that the interstitial has failed to load with some error.
     */
    public func interstitial(_ interstitial: IMInterstitial!, didFailToLoadWithError error: IMRequestStatus!) {
        NSLog("[VideoAdHelper] didFailToLoadWithError")
        NSLog("Interstitial ad failed to load with error %@", error)
        adFailToPlay()
    }
    /**
     * Notifies the delegate that the interstitial would be presented.
     */
    public func interstitialWillPresent(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialWillPresent")
    }
    /**
     * Notifies the delegate that the interstitial has been presented.
     */
    public func interstitialDidPresent(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialDidPresent")
    }
    /**
     * Notifies the delegate that the interstitial has failed to present with some error.
     */
    public func interstitial(_ interstitial: IMInterstitial!, didFailToPresentWithError error: IMRequestStatus!) {
        adFailToPlay()
        NSLog("[VideoAdHelper] didFailToPresentWithError")
    }
    /**
     * Notifies the delegate that the interstitial will be dismissed.
     */
    public func interstitialWillDismiss(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialWillDismiss")
    }
    /**
     * Notifies the delegate that the interstitial has been dismissed.
     */
    public func interstitialDidDismiss(_ interstitial: IMInterstitial!) {
        NSLog("[VideoAdHelper] interstitialDidDismiss")
        self.finish()
    }
    /**
     * Notifies the delegate that the interstitial has been interacted with.
     */
    public func interstitial(_ interstitial: IMInterstitial!, didInteractWithParams params: [AnyHashable : Any]!) {
        NSLog("[VideoAdHelper] didInteractWithParams")
    }
    /**
     * Notifies the delegate that the user will leave application context.
     */
    public func userWillLeaveApplication(from interstitial: IMInterstitial!){
        NSLog("[VideoAdHelper] userWillLeaveApplication")
    }
    
    // InMobi end
    
    // Smaato SDK start
    private func playSmaato() {
        let adView = SOMAInterstitialAdView()
        adView.frame = self.view.bounds
        self.view .addSubview(adView)
        adView.delegate = self
        
        let publisherId = Int.init(currentAdItem!.apiKey ?? "" ) ?? 0
        let adSpaceId = Int.init(currentAdItem!.channelId ?? "" ) ?? 0
        print("Smaato AD",publisherId,"   ",adSpaceId)
        adView.adSettings.publisherId = publisherId
        adView.adSettings.adSpaceId = adSpaceId
        
        adView.load()
        
    }
    
    func somaAdViewDidLoadAd(_ adview: SOMAAdView!) {
        adview.show()
        self.adDidShown()
    }
    func somaAdViewDidHide(_ adview: SOMAAdView!) {
        
    }
    func somaAdViewDidExitFullscreen(_ adview: SOMAAdView!) {
        self.finish()
    }
    
    
    // Smaato SDK end.
    
    // Google IMA start
    
    var contentPlayhead: IMAAVPlayerContentPlayhead?

    var adsLoader: IMAAdsLoader?
    var adsManager: IMAAdsManager?
    var player : AVPlayer?
    var vastUrl : String = ""
    let kTestAppAdTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&" +
        "iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&" +
        "gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&" +
    "cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";

    
    func setUpAdsLoader() {
        print("setUpAdsLoader")
        NotificationCenter.default.rx
            .notification(UIApplication.didEnterBackgroundNotification)
            .subscribe(onNext: { _ in
                if self.adsManager != nil {
                    self.adsManager?.pause()
                }
            })
            .disposed(by: disposeBag)
        
        NotificationCenter.default.rx
            .notification(UIApplication.didBecomeActiveNotification)
            .subscribe(onNext: { _ in
                if self.adsManager != nil {
                    self.adsManager?.resume()
                }
            })
            .disposed(by: disposeBag)
        
        
        self.player = AVPlayer()
        self.videoView = UIView()
        self.videoView.frame = self.view.bounds
        self.view.addSubview(self.videoView)
        
        

        
        self.player?.play()

        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoView.layer.bounds
        self.videoView.layer.addSublayer(playerLayer)
        
        contentPlayhead = IMAAVPlayerContentPlayhead(avPlayer: player)
        
        adsLoader = IMAAdsLoader(settings: nil)
        // NOTE: This line will cause an error until the next step, "Get the Ads Manager".
        adsLoader!.delegate = self
    }
    
    func setRewardUI() {
        progressBar?.removeFromSuperview()
        progressBar = YLProgressBar(frame: CGRect(x: 0, y: view.frame.size.height - 3, width: view.frame.size.width, height: 3))
        progressBar?.progressTintColor = Color.redOONAColor()!
        progressBar?.progress = 0
        progressBar?.progressStretch = false
        progressBar?.uniformTintColor = true
        progressBar?.type = .flat
        progressBar?.hideStripes = true
        progressBar?.trackTintColor = UIColor.gray
        
        progressBar?.layer.masksToBounds = true
        
        self.view.addSubview(progressBar!)
        
        let rewardGif = UIImageView(frame: CGRect(x: playerView.safeAreaInsets.left + 5, y:  playerView.safeAreaInsets.top + 5, width: 50, height: 50*5/6))
        
        do {
            let gif = try UIImage(gifName: "tcoins_pile2", levelOfIntegrity:1.0)
            rewardGif.setGifImage(gif)
            
        } catch  {
            print("set gif error")
            print(error)
        }
        
        rewardLabel = UILabel(frame: CGRect(x: rewardGif.frame.maxX-10, y: rewardGif.frame.minY+5, width: 40, height: 30))
        rewardLabel?.font = UIFont(name: "Montserrat-Regular", size: 15.0)!
        rewardLabel?.textColor = UIColor.white
        rewardLabel?.text = ""
        
        
        if !DeviceHelper.shared.isEssentialUiOnly {
            self.view.addSubview(rewardLabel!)
            self.view.addSubview(rewardGif)
        }
        
    }

    func adsLoader(_ loader: IMAAdsLoader!, adsLoadedWith adsLoadedData: IMAAdsLoadedData!) {
        // Grab the instance of the IMAAdsManager and set ourselves as the delegate
        NSLog("[VideoAdHelper] adsLoadedWith ")
        adsManager = adsLoadedData.adsManager
        adsManager!.delegate = self
        
        // Create ads rendering settings and tell the SDK to use the in-app browser.
        var adsRenderingSettings = IMAAdsRenderingSettings()
        adsRenderingSettings.webOpenerPresentingController = self.parent
        
        // Initialize the ads manager.
        adsManager!.initialize(with: adsRenderingSettings)
    }
    func adsManager(_ adsManager: IMAAdsManager!, adDidProgressToTime mediaTime: TimeInterval, totalTime: TimeInterval) {
//        print("ad",mediaTime,totalTime,adDuration)
        self.updateProgress(mediaTime: CGFloat(mediaTime), totalTime: CGFloat(totalTime))
      
        
    }
    func updateProgress(mediaTime:CGFloat,totalTime:CGFloat) {
          var maxTcoin : CGFloat = 100
                var maxTime : CGFloat = 0
                let totalTimeFloat = CGFloat(totalTime)
                if pointsRewards.count > 0 {
                    for pointsReward in pointsRewards {
        //                print("",pointsReward)
                        var timeToTest : CGFloat = CGFloat(pointsReward["duration"].floatValue)
                        if totalTimeFloat > timeToTest {
                            if timeToTest > maxTime {
                                maxTime = timeToTest
                                maxTcoin = CGFloat(pointsReward["points"].floatValue)
                            }
                        }
                    }
                }
                adDuration = Int(totalTime)
                let progress : CGFloat = CGFloat((mediaTime/totalTime))
                progressBar?.progress = progress
                rewardLabel?.text = "\(Int(min(progress,1) * maxTcoin))"
                
    }

    func adsLoader(_ loader: IMAAdsLoader!, failedWith adErrorData: IMAAdLoadingErrorData!) {
        print("Error loading ads: \(adErrorData.adError.message)")
        adFailToPlay()
//        contentPlayer?.play()
    }
    
    // MARK: - IMAAdsManagerDelegate
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
        
        
        switch event.type {
        case .LOADED :
             adsManager.start()
            self.setRewardUI()
        case .STARTED :
            self.adDidShown()
        
        default:
            print("\(event.type.rawValue)");
            
        }
       
    }
    
    func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
        // Something went wrong with the ads manager after ads were loaded. Log the error and play the
        // content.
        print("AdsManager error: \(error.message)")
//        self.adDidShown()
//        contentPlayer?.play()
        adFailToPlay()
    }
    
    func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
        // The SDK is going to play ads, so pause the content.
//        self.adDidShown()
//        contentPlayer?.pause()
    }
    
    func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
        // The SDK is done playing ads (at least for now), so resume the content.
//        contentPlayer?.play()
        finish()
    }
 
    
    // Google IMA end

    
    
    // SpotX start
     
    var spotxPlayer:  SpotXInlineAdPlayer?
    
    func request(for player: SpotXAdPlayer) -> SpotXAdRequest? {
        // if using an apikey
        
        
        let apiKey = String.init(currentAdItem?.apiKey ?? "e66637eea864ee137c69e824e61c4232" )
        let request: SpotXAdRequest = SpotXAdRequest(apiKey: apiKey)!
    
        var channelId = currentAdItem?.channelId ?? "85394"
//        channelId = "85394"
        request.setChannel(channelId)
         DispatchQueue.main.async {
                    self.playerView.addSubview(self.view)
               
        }
        
        return request
    }
    
    // Play the ad(s) if they were returned
    public func spotx(_ player: SpotXAdPlayer, didLoadAds group: SpotXAdGroup?, error: Error?) {
        if (group != nil && group!.ads.count > 0) {
            player.start()
            
        }
        else {
            adFailToPlay()
        }
    }
    
    // Called when the ad has started playing
    public func spotx(_ player: SpotXAdPlayer, adStart ad: SpotXAd) {
        print("SDK:Interstitial:adStart")
//        playerView.addSubview(self.view)
         DispatchQueue.main.async {
//            self.playerView.addSubview(self.view)
            self.setRewardUI()
        self.adDidShown()
        }
    }
    
    // Called when the group of ad(s) has started playing
    public func spotx(_ player: SpotXAdPlayer, adGroupStart group: SpotXAdGroup) {
        print("SDK:Interstitial:adGroupStart")
    }
    
    // Called every second during ad playback
    public func spotx(_ player: SpotXAdPlayer, adTimeUpdate ad: SpotXAd, timeElapsed seconds: Double) {
        print("SDK:Interstitial:adTimeUpdate")
        self.updateProgress(mediaTime: CGFloat(seconds), totalTime: CGFloat(ad.duration.floatValue))
    }
    
    // Called when the user has clicked the ad and has navigated to the click-through destination
    public func spotx(_ player: SpotXAdPlayer, adClicked ad: SpotXAd) {
        print("SDK:Interstitial:adClicked")
    }
    
    // Called when the ad has finished playing
    public func spotx(_ player: SpotXAdPlayer, adComplete ad: SpotXAd) {
        print("SDK:Interstitial:adComplete")
    }
    
    // Called when the user has skipped the ad
    public func spotx(_ player: SpotXAdPlayer, adSkipped ad: SpotXAd) {
        print("SDK:Interstitial:adSkipped")
    }
    
    // Called when an error happens during playback
    public func spotx(_ player: SpotXAdPlayer, adError ad: SpotXAd?, error: Error?) {
        print("SDK:Interstitial:adError ",error?.localizedDescription)
        adFailToPlay()
    }
    
    // Called when the ad(s) have completed playback and have been closed
    public func spotx(_ player: SpotXAdPlayer, adGroupComplete group: SpotXAdGroup) {
        print("SDK:Interstitial:adGroupComplete")
        self.finish()
    }

    // SpotX end
    
    
    
    
    private func playInmobi() {
        
        
        let apiKey = Int.init(currentAdItem!.apiKey ?? "" ) ?? 0
        let placementId = Int64.init(currentAdItem!.channelId ?? "" ) ?? 0
        print("inmobi AD",currentAdItem!.apiKey ,"   ",currentAdItem!.channelId )
        
        DispatchQueue.once(token: "InMobi.init") {
            print("init the imsdk once.")
            IMSdk.initWithAccountID(currentAdItem!.apiKey)
            IMSdk.setLogLevel(.debug)
            
        }

        
        interstitial = IMInterstitial.init(placementId: placementId)
        
        interstitial?.delegate = self
        interstitial?.load()
        
        
    }
    func playIMA() {
        setUpAdsLoader()
        // Create an ad display container for ad rendering.""
        NSLog("[VideoAdHelper] request ads")
        
        playerView.addSubview(self.view)
        
        let adDisplayContainer = IMAAdDisplayContainer(adContainer: self.videoView, companionSlots: nil)
        
        // Create an ad request with our ad tag, display container, and optional user context.
        let request = IMAAdsRequest(
            adTagUrl: vastUrl,
            adDisplayContainer: adDisplayContainer,
            contentPlayhead: contentPlayhead,
            userContext: nil)
        
        adsLoader!.requestAds(with: request)
    }
    private func playSpotX() {
        DispatchQueue.main.async {
           
        self.spotxPlayer = SpotXInlineAdPlayer.init(in: self.view)
        
        self.spotxPlayer?.delegate = self
        self.spotxPlayer?.load()
            
        }
    }

    public func triggerAd(video:Video, adType:AdType) {
        isGameAd = false
        print("trigger video ads json",video.prerollAds.ads)
        self.currentVideo = video
        if (adType == .engagement) {
            self.fetchEngagementAd(currentVideo:video)
        }
        if (adType == .preroll) {
//            self.fetchEngagementAd(currentVideo:video)
            self.currentAds = video.prerollAds
            if currentAds != nil {
                self.playVideoAds(videoAds: self.currentAds!)
            }
        }
        
//
//        switch adType {
//            case .preroll:
//                currentAds = video.prerollAds
//            case .midroll:
//                currentAds = video.midrollAds
//            default:
//                return
//        }
//        playVideoAds(videoAds: currentAds!)
//        fetchVideoAd(currentVideo: video)

            
        
    }
    public func triggerGameAd(game:Game) {
        isGameAd = true
//        print("trigger video ads js/on",video.prerollAds.ads)
        self.fetchGameAd(game: game)
        self.currentGame = game
        //
        //        switch adType {
        //            case .preroll:
        //                currentAds = video.prerollAds
        //            case .midroll:
        //                currentAds = video.midrollAds
        //            default:
        //                return
        //        }
        //        playVideoAds(videoAds: currentAds!)
        //        fetchVideoAd(currentVideo: video)
        
        
        
    }
    public func triggerDemoAd() {
//        vastUrl = adItem!.vastUrl ?? ""
        // demo only
//        vastUrl = "https://ads-console.oonatv.com:8000/vast/53"
//        playIMA()
        playSpotX()
    }
    var pointsRewards : [JSON] = []
    
    private func fetchEngagementAd(currentVideo:Video) {
        OONAApi.shared.postRequest(url: APPURL.displayAds(video:currentVideo),
                                   parameter: ["intervalFormat":"regular",
                                               "latitude":LocationHelper.shared.getLat(),
                                               "longitude":LocationHelper.shared.getLong(),
                                               "supportSpotX":"1"
            ])
            .responseJSON { (response) in

                if response.result.isSuccess {
                    if let json = (try? JSON(data: response.data!)) as? JSON {
                        print("engagement ads : ",json)
                        self.currentAds = VideoAds.init(with: json["displayAdInfo"], adType: .engagement)
                        
                        self.playVideoAds(videoAds: self.currentAds!)
                    }
                }
        }
    }
    private func fetchGameAd(game:Game) {
        OONAApi.shared.postRequest(url: APPURL.gameAds(game:game),
                                   parameter: ["intervalFormat":"regular",
                                               "latitude":LocationHelper.shared.getLat(),
                                               "longitude":LocationHelper.shared.getLong(),
                                               "supportSpotX":"1"
            ])
            
            .responseJSON { (response) in
                
                if response.result.isSuccess {
                    if let json = (try? JSON(data: response.data!)) as? JSON {
                        print("game ads : ",json)
                        self.currentAds = VideoAds.init(with: json["displayAdInfo"], adType: .engagement)
                        print("self.currentAds",self.currentAds?.numOfAds)
                        self.playVideoAds(videoAds: self.currentAds!)
                    }
                }
        }
    }
    private func videoAdViewAnalytic(){
//        OONAApi.shared.postRequest(url: APPURL.analyticEvent(event: "ff"), parameter:
//            [String : Any])
//            .responseJSON
    }
    var lastDisplayAdShowTimeStamp = String()
    
    public func displayAdView(video:Video, ad:OONAAd, videoTime:(Int)) {
        lastDisplayAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().stringValue
        let name = "display_ad_view"
        let parameters = ["channel_id":String(video.channelId),
                     "episode_id":String(video.episodeId),
                     //                                                 "display_ad_id":ad.id,
            "video_time":String.init(videoTime),
            "event_code":lastDisplayAdShowTimeStamp]
                                        
        _ = OONAApi.shared.postRequest(url: APPURL.analyticEvent(event: name), parameter: parameters)
        
    }
    
    public func displayAdClick(video:Video, ad:OONAAd, videoTime:(Int)) {
        let name = "display_ad_click"
                                    let parameters = ["channel_id":String(video.channelId),
                                                 "episode_id":String(video.episodeId),
                                                 //                                                 "display_ad_id":ad.id,
                                        "video_time":String.init(videoTime),
                                        "platform":ad.platform,
                                        "event_code":lastDisplayAdShowTimeStamp]
        _ = OONAApi.shared.postRequest(url: APPURL.analyticEvent(event: name), parameter: parameters)
        
    }
    public func displayAdEngagementView(video:Video, ad:OONAAd, videoTime:(Int), adWatchedDuration:(Int)) {
         AnalyticHelper.shared.displayAdVideoComplete(video: currentVideo!, ad: currentAdItem!, videoTime: PlayerHelper.shared.currentDuration ?? 0, adWatchedDuration: adDuration)
        let name = "display-ad-engagement-complete-view"
                                    let parameters = ["channelId":String(video.channelId),
                                                 "episodeId":String(video.episodeId),
                                                 //                                                 "display_ad_id":ad.id,
//                                        "video_time":String.init(videoTime),
                                        //                                                 "engagement_od":ad.engagementId,
                                        "adWatchedDuration":adDuration.description,
                                        "adDuration":adDuration.description,
                                        "platform":ad.platform,
                                        "eventCode":lastDisplayAdShowTimeStamp]
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: parameters)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                if(isSuccess){
                                    if let _balance = json["points"]["balance"].int{
                                        let points : Int? = json["reward"]["points"].int
                                        
                                        //                                    let tcoinBalance = UserHelper.shared.currentUser?.profile?.points
                                        if !DeviceHelper.shared.isEssentialUiOnly {
                                            if let pt = points {
                                                if pt == 50 {
                                                    PlayerHelper.shared.showRewardVideo.accept(.Award50)
                                                }
                                                if pt == 100 {
                                                    PlayerHelper.shared.showRewardVideo.accept(.Award100)
                                                }
                                                if pt == 250 {
                                                    PlayerHelper.shared.showRewardVideo.accept(.Award250)
                                                }
                                                if pt == 500 {
                                                    PlayerHelper.shared.showRewardVideo.accept(.Award500)
                                                }
                                                if pt == 1000 {
                                                    PlayerHelper.shared.showRewardVideo.accept(.Award1000)
                                                }
                                            }
                                        }
                                        self.rewardReceived.accept(Reward.init(pt: points ?? 100 , bal: _balance))
                                    }
                                } else {
                                    
                                }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                }
        }
        
    }
    public func gameAdEngagementView(game:Game, ad:OONAAd) {
        
        AnalyticHelper.shared.gameAdVideoComplete(game: game, ad: ad, adDuration: adDuration)
        let name = "display-ad-engagement-complete-view"
        
        var parameters = ["gameId":String(game.id),
                          
                          //                                                 "display_ad_id":ad.id,
            //                                        "video_time":String.init(videoTime),
            //                                                 "engagement_od":ad.engagementId,
            "adWatchedDuration":adDuration.description,
            "adDuration":adDuration.description,
            "platform":ad.platform,
            "eventCode":lastDisplayAdShowTimeStamp]
        
        if  (ad.channelId != "" ){
            parameters = ["gameId":String(game.id),
                          
                          //                                                 "display_ad_id":ad.id,
                //                                        "video_time":String.init(videoTime),
                "platformChannelId":ad.channelId,
                "adWatchedDuration":adDuration.description,
                "adDuration":adDuration.description,
                "platform":ad.platform,
                "eventCode":lastDisplayAdShowTimeStamp]
        }
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: parameters)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            if let _balance = json["points"]["balance"].int{
                                                                let points : Int? = json["reward"]["points"].int
                                                                
                                                                //                                    let tcoinBalance = UserHelper.shared.currentUser?.profile?.points
                                                                self.rewardReceived.accept(Reward.init(pt: points ?? 100 , bal: _balance))
                                                            }
                                                        } else {
                                                            
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                }
        }
        
    }
    
//    public func displayAdEngagementMonetiseView(video:Video, ad:OONAAd, videoTime:(Int), adWatchedDuration:(Int)) {
//        self .logEventToAllPlatform(name: "display_ad_engagement_monetise_view",
//                                    parameters: ["channel_id":String(video.channelId),
//                                                 "episode_id":String(video.episodeId),
//                                                 //                                                 "display_ad_id":ad.id,
//                                        "video_time":String.init(videoTime),
//                                        //                                                 "engagement_od":ad.engagementId,
//                                        "platform":ad.platform,
//                                        "ad_duration":ad.duration?.stringValue ?? "",
//                                        "ad_watched_duration":String.init(adWatchedDuration),
//                                        "event_code":lastDisplayAdShowTimeStamp
//
//            ])
//    }
    public func displayAdIgnore(video:Video, ad:OONAAd, videoTime:(Int)) {
        lastDisplayAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().stringValue
        let name = "display_ad_view"
                                    let parameters = ["channel_id":String(video.channelId),
                                                 "episode_id":String(video.episodeId),
                                                 //                                                 "display_ad_id":ad.id,
                                        "video_time":String.init(videoTime),
                                        "event_code":lastDisplayAdShowTimeStamp]
                                        
        _ = OONAApi.shared.postRequest(url: APPURL.analyticEvent(event: name), parameter: parameters)
    }
    private func playVideoAds(videoAds:VideoAds) {
        trialCounter = 0
        isAdSuccess = false
       tryNextAdItem()

        
    }
    private func playAdItem(adItem:OONAAd?) {
       currentAdItem = adItem!
        
        self.pointsRewards = adItem?.pointsRewards ?? []
        
        switch adItem!.platformType {
        case .vast:
            vastUrl = adItem!.vastUrl ?? ""

            playIMA()
//            tryNextAdItem()
        default:

            
            switch adItem?.platform.lowercased() {
            case "spotx":
                playSpotX()
            case "inmobi":
                playInmobi()
            case "smaato":
                playSmaato()
            default:
                tryNextAdItem()

            }
        }
    }
    private func tryNextAdItem () {
        print("tryNextAdItem",trialCounter,"    ",currentAds?.ads.count)
        if trialCounter < currentAds?.ads.count ?? 0 {
            let adItem = currentAds!.ads[trialCounter]
            trialCounter += 1
            playAdItem(adItem: adItem)
            
        }else {
            print("No more ad to play")
            dismiss()
        }
        
    }
    private func receivedAdsJsonData(data : Data){
        do {
            let jsonDecoder = JSONDecoder()
//            let responseModel = try jsonDecoder.decode(Json4Swift_Base.self, from: data)
        } catch {
            print(error)
            // or display a dialog
        }
    }
    var isAdSuccess : Bool = false
    private func adDidShown() {
//        playerView .addSubview(self.view)
        lastDisplayAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().stringValue
        
        if isGameAd  {
            AnalyticHelper.shared.gameAdVideoStart(game: currentGame!, ad: currentAdItem!, adDuration: adDuration)
            
        }else{
            if (currentVideo != nil) {
                AnalyticHelper.shared.displayAdVideoStart(video: currentVideo!, ad: currentAdItem!, videoTime: PlayerHelper.shared.currentDuration ?? 0,adDuration:adDuration)
            }
        }
        isAdSuccess = true
        _adDidStart.onCompleted()
    }
    private func finish() {
        if (isAdSuccess) {
        if (currentAdItem != nil) {
            UserDefaults.standard.set(true, forKey: "no_need_show_adIgnore_chatbot")
            if isGameAd {
                self.gameAdEngagementView(game: currentGame!, ad: currentAdItem!)
//                    AnalyticHelper.shared.gameAdVideoComplete(game: currentGame!, ad: currentAdItem!,adDuration: adDuration)
                
            }else {
                if (currentVideo != nil) {
                    self.displayAdEngagementView(video: currentVideo!, ad: currentAdItem!, videoTime: PlayerHelper.shared.currentDuration ?? 0, adWatchedDuration: adDuration)
                    
                }
            }
        }
        }
        dismiss()
    }
    private func dismiss() {
        self.view.removeFromSuperview()
        self .removeFromParent()
        _adDidClose.onCompleted()
    }
    private func adFailToPlay() {
//        if currentAds == nil {
//            dismiss()
//        }
//
        self.view.removeFromSuperview()
        tryNextAdItem()
    }
    
}
