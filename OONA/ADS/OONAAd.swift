//
//  OONAAd.swift
//  OONA
//
//  Created by Jack on 22/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

enum AdType {
    case preroll
    case midroll
    case engagement
}
enum platformType {
    case sdk
    case vast
}
class DisplayAdInfo : NSObject {
    var firstInterval : Int = 0
    var podInterval : Int = 0
    var podDuration : Int = 0
    var position : String = ""
    
    
    
    init(with json:JSON?) {
        if (json == nil) {
//            firstInterval = 0
            
        }else{
            print("init ads interval \(json)")
            firstInterval = json!["firstInterval"].intValue
            podDuration = json!["podDuration"].intValue
            podInterval = json!["podInterval"].intValue
            position = json!["position"].stringValue
        }
    }
    
}
class VideoAds : NSObject {
    var numOfAds : Int = 0
    var ads : [OONAAd] = []
    
    init(with json:JSON?, adType:AdType) {
        if (json == nil) {
            numOfAds = 0
            ads = []
        }else{
            print("init Video ads \(json)")
            numOfAds = json!["numOfAds"].int ?? 1
            for ad in json!["ads"].arrayValue{
                print("init Video ads for \(ad)")
                ads.append(OONAAd.init(with: ad, adType: adType))
            }
        }
    }
    
}

class DisplayAds : NSObject {
    var numOfAds : Int = 0
    var ads : [EngagementAd] = []
    var adImage : displayAdImage?
    var placeholderImage : displayAdImage?
    var AdIgnoreImage : displayAdImage?
    var onClickImage : displayAdImage?
    
    
    init(with json:JSON, adType:AdType) {
       
        numOfAds = json["numOfAds"].int ?? 1
        
        self.adImage = displayAdImage.init(with: json["adImage"])
        
        if json["placeholderImage"].exists() {
            self.placeholderImage = displayAdImage.init(with: json["placeholderImage"])
        }
        if json["ignoreImage"].exists() {
            self.AdIgnoreImage = displayAdImage.init(with: json["ignoreImage"])
        }
        if json["onClickImage"].exists() {
            self.onClickImage = displayAdImage.init(with: json["onClickImage"])
        }
        for ad in json["ads"].arrayValue{
            ads.append(EngagementAd.init(with: ad, adType: adType))
        }
        
    }
    
}
class EngagementAd : OONAAd {
    let monetiseViewCost : Int?
    var monetiseDuration : Int = 5
    var powintReward : Int = 0
    var showNoPointsReminder : Bool = false
    override init(with json: JSON, adType: AdType) {
        self.monetiseDuration = json["monetiseDuration"].intValue
        self.monetiseViewCost = json["monetiseViewCost"].intValue
        self.powintReward = json["powintReward"].intValue
        self.showNoPointsReminder  = json["showNoPointsReminder"].boolValue
        
        super.init(with: json, adType: .engagement)
    }
}
class OONAAd : NSObject {
    let adType : AdType
    let platform: String
    let channelId: String
    let apiKey: String?
    let platformType : platformType
    let vastUrl: String?
    let adImage : [String:Any]?
    let brand : [String:Any]?
    let duration : NSNumber?
    let customParameter : [String:Any]?
    let pointsRewards : [JSON]
    
    init(with json:JSON, adType:AdType) {
        self.adType = adType
        self.platform = json["platform"].description
        
        self.vastUrl = json["vastUrl"].description
        
        self.channelId = json["channelId"].description
        
        self.apiKey = json["apiKey"].description
        
        if (json["vastUrl"].string != nil){
            self.platformType = .vast
//            self.vastUrl = json["vastUrl"].string!
        }else {
            self.platformType = .sdk
//            self.channelId = json["channelId"].string!
        }
        self.pointsRewards = json["pointsRewards"].arrayValue
//        self.vastUrl = vUrl
        self.adImage = json["adImage"].dictionary
        self.customParameter = json["customParameters"].dictionary
        self.brand = json["brand"].dictionary
        self.duration = json["adDuration"].numberValue
    }

    
}
class displayAdImage : NSObject {
    
    let imageUrl : String
    let width : Int?
    let height : Int?
    let heightRatio : Float?
    var bgColor : UIColor?
    var GifLoopCount : Int?
    var dismissOnLoopEnd : Bool?
    
    init(with json:JSON) {
        
        self.imageUrl = json["imageUrl"].stringValue
        self.width = json["width"].intValue
        self.height = json["height"].intValue
        self.heightRatio = json["heightRatio"].floatValue
        self.bgColor = UIColor.clear
        
        self.GifLoopCount = 0
        self.dismissOnLoopEnd = false
        
        if json["bgColor"].exists() {
            self.bgColor = UIColor.init(hexString: ("0x" + json["bgColor"].stringValue) )
        }
        if json["loopCount"].exists() {
            self.GifLoopCount = json["loopCount"].intValue
        }
        if json["dismissOnLoopEnd"].exists() {
            self.dismissOnLoopEnd = json["dismissOnLoopEnd"].boolValue
        }
        
        
        
        
    }
    

}
class AdSession : NSObject {
    
    
    let adType : AdType
    let video : Video

    
    
    
    init(aType:AdType, vid:Video) {

        self.adType = aType
        self.video = vid

    }
    
    
}
