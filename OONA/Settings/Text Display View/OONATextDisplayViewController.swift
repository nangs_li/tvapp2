//
//  OONATextDisplayViewController.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa

class OONATextDisplayViewController: BaseViewController {
    
    var textColor: UIColor? { didSet { self.updateTextViewUI() } }
    var selectable: Bool = false { didSet { self.updateTextViewUI() } }
    var textContainerInset: UIEdgeInsets? { didSet { self.updateTextViewUI() } }
    var textBackgroundColor: UIColor? { didSet { self.updateTextViewUI() } }
    var textFont: UIFont? { didSet { self.updateTextViewUI() } }
    
    var textToDisplay: BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    var textStyle: NSMutableParagraphStyle? { didSet { self.applyTextStyle() } }
    @IBOutlet private weak var oonaTextView: UITextView!
    
    override func loadView() {
        super.loadView()
        print(#function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = self.title
        self.textToDisplay.skip(1).subscribe(onNext: { [weak self] (text) in
            self?.oonaTextView?.text = text
            self?.updateTextViewUI()
        }).disposed(by: disposeBag)
        
        self.updateTextViewUI()
        self.applyTextStyle()
    }
    
    private func updateTextViewUI() {
        self.oonaTextView?.isSelectable = self.selectable
        
        if let _textColor = self.textColor {
            self.oonaTextView?.textColor = _textColor
        }
        
        if let _font = self.textFont {
            self.oonaTextView.font = _font
        }
        
        if let _textBackgroundColor = self.textBackgroundColor {
            self.oonaTextView?.backgroundColor = _textBackgroundColor
        }
        
        if let _textContainerInset = self.textContainerInset {
            self.oonaTextView?.textContainerInset = _textContainerInset
        }
    }
    
    private func applyTextStyle() {
        guard let _ = self.oonaTextView,
            let _paragraphStyle = self.textStyle,
            let _text = self.textToDisplay.value
            else { return }
        let attributes = self.oonaTextView.attributedText.attributes(at: 0, effectiveRange: nil)
        let mutableAttributedString: NSMutableAttributedString = NSMutableAttributedString(string: _text)
        
        mutableAttributedString.addAttributes(attributes, range: NSRange(location: 0, length: _text.count))
        mutableAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                             value: _paragraphStyle,
                                             range: NSRange(location: 0, length: self.oonaTextView.attributedText.string.count))
        self.oonaTextView.attributedText = mutableAttributedString
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}
