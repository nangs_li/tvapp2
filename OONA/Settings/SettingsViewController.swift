//
//  SettingsViewController.swift
//  OONA
//
//  Created by nicholas on 8/15/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit


class SettingsViewController: SettingsBaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet { self.configureTableView() }
    }
    var settingsModules: [OONASettingsAction : Any] = [OONASettingsAction : Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = LanguageHelper.localizedString("menu_setting")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
//        self.showTopBar = true
        
        self.configureBackButton()
//        self.configNavigationItems()
        OONASettings.manager.settingNeedsUpdate.subscribe(onNext: { [weak self] (_) in
            self?.tableView.reloadData()
        }).disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTopBar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showTopBar = false
    }
    
    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
            self.view.layoutIfNeeded()
        }
    }
    
    private func configureBackButton() {
        let backImage: UIImage? = UIImage(named: "back-arrow")
        let backImageView: UIImageView = UIImageView(image: backImage)
        let backButton: UIButton = UIButton(type: .system)
        backButton.tintColor = .white
        backButton.contentMode = .scaleAspectFit
        backButton.addTarget(self, action: #selector(backButtonAction(sender:)), for: .touchUpInside)
        let customView: UIView = UIView()
        customView.addSubview(backButton)
        backButton.snp.makeConstraints { (make) in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        
        customView.insertSubview(backImageView, belowSubview: backButton)
        backImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.centerX.equalToSuperview()
            make.centerX.equalToSuperview().offset(-4.5)
            make.centerY.equalToSuperview().offset(-4)
        }
        let leftBarItem: UIBarButtonItem = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftBarItem
    }
    
    private func configureTableView() {
        self.tableView.backgroundColor = .clear
        self.tableView.register(UINib(nibName: "SettingsArrowCell", bundle: nil), forCellReuseIdentifier: "SettingsArrowCell")
        self.tableView.register(UINib(nibName: "SettingsSwitchCell", bundle: nil), forCellReuseIdentifier: "SettingsSwitchCell")
        self.tableView.register(UINib(nibName: "SettingsPickerCell", bundle: nil), forCellReuseIdentifier: "SettingsPickerCell")
        self.tableView.estimatedRowHeight = 50
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func showPage(for settingsAction: OONASettingsAction) {
        print(#function)
        switch settingsAction {
        case .SettingsParentalControl:
            print(settingsAction)
            let parentControlPage: ParentalControlViewController = UIStoryboard(name: "ParentalControl", bundle: nil).instantiateInitialViewController() as! ParentalControlViewController
            self.navigationController?.pushViewController(parentControlPage, animated: true)
            
        case .SettingsStreamingQuality:
            print(settingsAction)
            return //TODO: Not finish yet (not configured with video play), use this code to disable as android is not done
            let qualityPickerPage: SettingsQualityViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsQualityViewController") as! SettingsQualityViewController
            qualityPickerPage.modalTransitionStyle = .crossDissolve
            qualityPickerPage.modalPresentationStyle = .overCurrentContext
            
            qualityPickerPage.selectedQuality.skip(1).subscribe(onNext: { (quality) in
                let displayableSetting: SettingsPicker = OONASettings.manager.getDisplayaSetting(with: .SettingsStreamingQuality)
                displayableSetting.accessoryAction.accept(quality.rawValue)
            }).disposed(by: qualityPickerPage.disposeBag)
            self.navigationController?.present(qualityPickerPage, animated: true, completion: nil)
//            self.navigationController?.pushViewController(qualityPickerPage, animated: true)
            
        case .SettingsLanguage:
            print(settingsAction)
//            return //TODO: Not finish yet (not configured with video play), use this code to disable as android is not done
            let languagePickerPage: SettingsLanguageViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsLanguageViewController") as! SettingsLanguageViewController
            languagePickerPage.modalTransitionStyle = .crossDissolve
            languagePickerPage.modalPresentationStyle = .overCurrentContext
            
            languagePickerPage.selectedLanguage.skip(1).subscribe(onNext: { (language) in
                let displayableSetting: SettingsPicker = OONASettings.manager.getDisplayaSetting(with: .SettingsLanguage)
                displayableSetting.accessoryDescriptionText = language.detailString
                displayableSetting.accessoryAction.accept(language.rawValue)
            }).disposed(by: languagePickerPage.disposeBag)
            self.navigationController?.present(languagePickerPage, animated: true, completion: nil)
            
        case .SettingsTutorial:
            print(settingsAction)
            let tutorialsPage: SettingsTutorialsViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsTutorialsViewController") as! SettingsTutorialsViewController
            self.navigationController?.pushViewController(tutorialsPage, animated: true)
            
        case .SettingsAppVersions:
            let appVersionsPage: AppVersionsViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "AppVersionsViewController") as! AppVersionsViewController
            self.navigationController?.pushViewController(appVersionsPage, animated: true)
            print(settingsAction)
            
        case .SettingsPrivacyPolicy:
            print(settingsAction)
            let privacyPolicyWebPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            privacyPolicyWebPage.privacyURL = URL(string: APPURL.privacyPolicy())
            privacyPolicyWebPage.title = LanguageHelper.localizedString("privacy_policy")
            self.navigationController?.pushViewController(privacyPolicyWebPage, animated: true)
            
        case .SettingsGeneralTermsAndConditions:
            print(settingsAction)
            let privacyPolicyWebPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            privacyPolicyWebPage.privacyURL = URL(string: APPURL.settingsGeneralTermsAndConditions())
            privacyPolicyWebPage.title = LanguageHelper.localizedString("general_terms_and_conditions")
            self.navigationController?.pushViewController(privacyPolicyWebPage, animated: true)
            
        case .SettingsTCoinsTermsAndConditions:
            print(settingsAction)
            let privacyPolicyWebPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            privacyPolicyWebPage.privacyURL = URL(string: APPURL.settingsTCoinsTermsAndConditions())
            privacyPolicyWebPage.title = LanguageHelper.localizedString("tcoins_terms_and_conditions")
            self.navigationController?.pushViewController(privacyPolicyWebPage, animated: true)
            
        case .SettingsDisclaimer:
            print(settingsAction)
            let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
            disclaimerPage.title = LanguageHelper.localizedString("disclaimer")
            self.navigationController?.pushViewController(disclaimerPage, animated: true)
            
        default: break
        }
    }
    
    override func appLanguageDidUpdate() {
        self.title = LanguageHelper.localizedString("menu_setting")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
        OONASettings.manager.displayableSettings.removeAll()
        self.tableView.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.leaveAction()
    }
    
    @objc func backButtonAction(sender: UIButton) {
        print(#function)
        self.leaveAction()
    }
}
