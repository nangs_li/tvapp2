//
//  SettingsArrowCell.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsArrowCell: SettingsBaseCell {
    let bigFontName = "Montserrat-SemiBold"
    let normalFontName = "Montserrat-Medium"
    
    @IBOutlet private weak var containerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var containerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var labelVerticalConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var separator: UIView!
    
    var settingsObject: SettingsSelection?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UIDevice.current.hasNotch {
            self.containerLeadingConstraint.constant = 0
            self.containerTrailingConstraint.constant = 0
        }
    }
    
    func configure(with setting: SettingsSelection) {
        self.titleLabel.text = setting.title
        if let _titleColor = setting.titleColor {
            self.titleLabel.textColor = _titleColor
        }
        self.subTitleLabel.text = setting.subTitle
        var verticalSpacing: CGFloat = 0
        if let _subTitle = setting.subTitle, _subTitle.count > 0 {
            verticalSpacing = 5
        }
        labelVerticalConstraint.constant = verticalSpacing
        self.separator.isHidden = !setting.lastItemInSection
        
        if setting.settingsIdentifier == .SettingsParentalControl {
            self.titleLabel.font = UIFont(name: bigFontName, size: 15)
        } else {
            self.titleLabel.font = UIFont(name: normalFontName, size: 13)
        }
        self.settingsObject = setting
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
