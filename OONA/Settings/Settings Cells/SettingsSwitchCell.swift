//
//  SettingsSwitchCell.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsSwitchCell: SettingsBaseCell {
    
    @IBOutlet private weak var containerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var containerTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    @IBOutlet private weak var switchButton: UISwitch!
    
    @IBOutlet weak var separator: UIView!
    
    var settingsObject: SettingsSwitch?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.switchButton.addTarget(self, action: #selector(switchChanged(sender:)), for: .valueChanged)
        
        if UIDevice.current.hasNotch {
            self.containerLeadingConstraint.constant = 0
            self.containerTrailingConstraint.constant = 0
        }
    }
    
    func configure(with setting: SettingsSwitch) {
        self.titleLabel.text = setting.title
        self.subTitleLabel.text = setting.subTitle
        self.switchButton.isOn = setting.accessoryAction.value
        self.separator.isHidden = !setting.lastItemInSection
        self.settingsObject = setting
        self.switchButton.thumbTintColor = setting.accessoryAction.value ? UIColor.init(rgb: 0xff973e) : .white
    }
    
    @objc func switchChanged(sender: UISwitch) {
        print("Settings Switch state changed: \(sender.isOn ? "On" : "Off")")
        if let _setting = self.settingsObject {
            _setting.accessoryAction.accept(sender.isOn)
            self.switchButton.thumbTintColor = _setting.accessoryAction.value ? UIColor.init(rgb: 0xff973e) : .white
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
