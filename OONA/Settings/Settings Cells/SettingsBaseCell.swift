//
//  SettingsBaseCell.swift
//  OONA
//
//  Created by nicholas on 8/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsBaseCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
