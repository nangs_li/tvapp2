//
//  SettingsPickerCell.swift
//  OONA
//
//  Created by nicholas on 8/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsPickerCell: SettingsBaseCell {
    @IBOutlet private weak var containerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var containerTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    
    @IBOutlet private weak var accessoryBackground: UIImageView!
    @IBOutlet private weak var accessoryDescriptionLabel: UILabel!
    @IBOutlet private weak var accessoryIconImageView: UIImageView!
    @IBOutlet weak var separator: UIView!
    
    var settingsObject: SettingsPicker?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UIDevice.current.hasNotch {
            self.containerLeadingConstraint.constant = 0
            self.containerTrailingConstraint.constant = 0
        }
    }
    
    func configure(with setting: SettingsPicker) {
        self.titleLabel.text = setting.title
        self.subTitleLabel.text = setting.subTitle
        self.accessoryDescriptionLabel.text = setting.accessoryDescriptionText ?? setting.accessoryAction.value
        self.separator.isHidden = !setting.lastItemInSection
        self.settingsObject = setting
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
