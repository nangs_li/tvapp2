//
//  SettingsCommonCell.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsCommonCell: UITableViewCell, OONASettingsCellProtocol {
    var settingsCellType: OONASettingsCellType = .cellDefault
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textLabel?.text = ""
        self.detailTextLabel?.text = ""
        self.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 12)
        self.detailTextLabel?.font = UIFont(name: "Montserrat-Regular", size: 9)
        self.textLabel?.textColor = .white
        self.detailTextLabel?.textColor = .init(white: 1, alpha: 0.6)
    }
    
    func configureCell(with cellType: OONASettingsCellType) {
        
        switch cellType {
        case .cellArrow:
            let arrowImageView = UIImageView()
            arrowImageView.image = UIImage(named: "back-arrow")
            self.accessoryView = arrowImageView
        case .cellSwitch:
            let cellSwitch = UISwitch()
            cellSwitch.addTarget(self, action: #selector(switchChanged(sender:)), for: .valueChanged)
            cellSwitch.isOn = false
            self.accessoryView = cellSwitch
        default:
            break
        }
        
        self.settingsCellType = cellType
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func switchChanged(sender: UISwitch) {
        print("Settings Switch state changed: \(sender.isOn ? "On" : "Off")")
    }
}
