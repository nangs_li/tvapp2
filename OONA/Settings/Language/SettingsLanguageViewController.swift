//
//  SettingsLanguageViewController.swift
//  OONA
//
//  Created by nicholas on 9/23/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa

class SettingsLanguageViewController: SettingsBaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView! {
        didSet { self.configureTableView() }
    }
    
    let selectedLanguage: BehaviorRelay<Language> = BehaviorRelay<Language>(value: .english)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.languageOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //TODO:
        let cell: QualityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "QualityTableViewCell", for: indexPath) as! QualityTableViewCell
        cell.qualityDescriptionLabel.text = self.languageOptions[indexPath.row].detailString
        let shouldSelect: Bool = self.languageOptions[indexPath.row] == LanguageHelper.shared.getAppLanguage()
        if shouldSelect {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.languageOptions[indexPath.row]
        guard selectedItem != LanguageHelper.shared.getAppLanguage() else { return }
        self.retry = 5
        self.selectedLanguage.accept(selectedItem)
        self.reRegisterDevice { [weak self] () -> (Void) in
            self?.leaveAction()
        }
    }

    var retry: Int = 5
    func reRegisterDevice(completion: (() ->(Void))? = nil) {
        self.view.startLoading()
        OONAFirebase.shared.fetchConfig(complete: { [weak self] (result, error) in
            //after calling firebasehelper , call register device
            if let _result = result{
                DeviceHelper.shared.registerDevice(regSecret: _result, success: { response in
                    self?.view.cancelLoading()
                    completion?()
                }, failure: { error in
                    if let checkedSelf = self {
                        checkedSelf.view.cancelLoading()
                        if checkedSelf.retry > 0 {
                            checkedSelf.reRegisterDevice()
                            checkedSelf.retry -= 1
                        } else {
                            self?.tableView.reloadData()
                        }
                    }
                })
            }
            else if error != nil{
                self?.view.cancelLoading()
                //TODO_OONA : error handling for cannot remote config from firebase.
            }
        })
    }
    
    private func configureTableView() {
        //TODO:
        self.tableView.register(UINib(nibName: "QualityTableViewCell", bundle: nil), forCellReuseIdentifier: "QualityTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private var languageOptions: [Language] =
        [.english,
         .indonesian]
    
    @IBAction func closePage(_ sender: Any) {
        //        self.dismiss(animated: true, completion: nil)
        self.leaveAction()
    }
}
