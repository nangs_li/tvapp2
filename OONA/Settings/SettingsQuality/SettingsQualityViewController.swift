//
//  SettingsQualityViewController.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa

class SettingsQualityViewController: SettingsBaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView! {
        didSet { self.configureTableView() }
    }
    
    let selectedQuality: BehaviorRelay<SettingsQuality> = BehaviorRelay<SettingsQuality>(value: .QualityAlwaysAsk)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.qualityList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: QualityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "QualityTableViewCell", for: indexPath) as! QualityTableViewCell
        cell.qualityDescriptionLabel.text = self.qualityList[indexPath.row].rawValue
        let shouldSelect: Bool = self.qualityList[indexPath.row].rawValue == OONASettings.manager.settingsObject.streamingQuality.rawValue
        if shouldSelect {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.qualityList[indexPath.row]
        self.selectedQuality.accept(selectedItem)
    }
    
    private func configureTableView() {
        self.tableView.register(UINib(nibName: "QualityTableViewCell", bundle: nil), forCellReuseIdentifier: "QualityTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private var qualityList: [SettingsQuality] =
        [SettingsQuality.QualityAuto,
         SettingsQuality.QualityAlwaysAsk,
         SettingsQuality.Quality1080p,
         SettingsQuality.Quality720p,
         SettingsQuality.Quality480p,
         SettingsQuality.Quality360p,
         SettingsQuality.Quality180p]
    
    @IBAction func closePage(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.leaveAction()
    }
}
