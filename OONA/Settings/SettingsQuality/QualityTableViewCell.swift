//
//  QualityTableViewCell.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class QualityTableViewCell: UITableViewCell {
    @IBOutlet weak var selectionIndicator: UIImageView!
    @IBOutlet weak var qualityDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.tintColor = .white
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
//        self.selectionIndicator.isHidden = !selected
        self.accessoryType = selected ? UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
        
        print(#function + "\(selected ? "selected" : "deselected")")
    }
    
}
