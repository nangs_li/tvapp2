//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK

class ParentalControlEmailViewController: SettingsBaseViewController {
    var parentalControlMode : ParentalControlMode = .none
    var parentalControl: ParentalControl?
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var submit: UIButton!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topSubLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Email"
        self.navigationItem.title = self.title
        
        
        let bottomLine = CALayer()
        email.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        email.layer.addSublayer(bottomLine)
        
        email.layer.masksToBounds = true // the most important line of code
        
        submit.layer.cornerRadius = 5
        submit.layer.borderWidth = 1
        submit.layer.borderColor = UIColor.white.cgColor
        if let userEmail = UserHelper.shared.currentUserProfile()?.email,
            parentalControlMode == .create {
            self.email.text = userEmail
        } else {
            email.becomeFirstResponder()
        }
        
        self.backButton.isHidden = !(self.parentalControlMode == .edit || self.parentalControlMode == .create)
    }
    
    
    @IBAction func confirm(_ sender: UIButton) {
        if var pc = self.parentalControl {
            self.email.resignFirstResponder()
            if let email = email.text{
                if email != ""{
                    
                    pc.contactEmail = email
                    pc.enabled = true
                    ParentalControlHelper.shared.saveParentalControl( pc: pc,
                                                                      success: { [weak self, unowned helper = ParentalControlHelper.shared] in
                                                                        //back to orignal..
                                                                        helper.getParentalControlPreference(success: { (pc) in
                                                                            self?.dismiss(animated: true,
                                                                                          completion: {
                                                                                            helper.allowToAccessParentalCControl.accept(true)
                                                                            })
                                                                        },
                                                                                                            failure: nil)
                    }, failure: { [weak self] code, msg in
                        var errorMessage: String = ""
                        if let _msg: String = msg { errorMessage = _msg }
                        if let _code: Int = code { errorMessage = errorMessage + "(" + "\(String(_code))" + ")" }
                        self?.showAlert(title: "Oops! Something went wrong",
                                        message: errorMessage,
                                        completion: { [unowned helper = ParentalControlHelper.shared] (alertAction) in
                            helper.allowToAccessParentalCControl.accept(false)
                        })
                    })
                }else{
                    self.showAlert(title: "", message : "Please input email")
                }
            }else{
                self.showAlert(title: "", message : "Please input email")
            }
        }
    }
    
    @IBAction override func close(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.dismiss(animated: true) {
            if self.parentalControlMode == .create || self.parentalControlMode == .edit {
                ParentalControlHelper.shared.allowToAccessParentalCControl.accept(false)
            }
        }
    }
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        if self.parentalControlMode == .edit {
            self.leaveAction()
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func appLanguageDidUpdate() {
        self.topSubLabel.text = LanguageHelper.localizedString("enter_your_email_in_case_you_forget_your_passcode")
        self.topLabel.text = LanguageHelper.localizedString("enter_email")
        self.submit.setTitle(LanguageHelper.localizedString("submit_text"), for: .normal)
    }
    
    deinit {
//        print("deallocating " + String(describing: type(of: self)))
    }
    
}
