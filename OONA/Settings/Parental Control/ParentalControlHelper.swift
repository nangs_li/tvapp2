//
//  EpisodeHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON
import RxCocoa
import SVProgressHUD

enum AllowedChannelAction: String {
    case Add = "add"
    case Remove = "remove"
}

class ParentalControlHelper: NSObject {
    static let shared: ParentalControlHelper = {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        return ParentalControlHelper() }()
    
    var currentParentalControl: BehaviorRelay<ParentalControl?> = BehaviorRelay<ParentalControl?>(value: nil)
    let allowToAccessParentalCControl: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    var allowedChannels : [Channel] = [Channel]()
    
    func getParentalControlPreference(success: ((ParentalControl) -> ())?,
                                      failure: ((Int?, String?) -> ())?) {
        self.showHud(with: LanguageHelper.localizedString("Loading"))
        OONAApi.shared.getRequest(url: APPURL.ParentalControl(), parameter:["":""])
                .responseJSON{ [weak self] (response) in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        let code = json["code"].int
                        let status = json["status"].string
                        APIResponseCode.checkResponse(withCode:code?.description,
                                                      withStatus: status,
                                                      completion: {
                                                        [weak self] message , _success in
                                                        self?.dismissHud()
                                                        if _success, let pc : ParentalControl = ParentalControl(json:json) {
                                                            self?.currentParentalControl.accept(pc)
                                                            success?(pc)
                                                        } else {
                                                            guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                                                            failure?(code, message)
                                                        }
                        })
                        
                    case .failure(let error as NSError):
                        self?.dismissHud()
                        print("Debug print: \(error as Any)")
                        print("Debug print: \(error.localizedDescription as Any)")
                        print("Debug print: \(error.domain): \(error.code)")
                        guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                            return }
                        failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                    }
            }
    }
    
    func getAllowedPlaylist(success: (([Channel]) -> ())?,
                            failure: ((Int?, String?) -> ())?) { //used in parental control..
        
        //let param : [String: String] =  ["filter": ChannelSetting.channelListFilterType.allowed.description]
        self.showHud(with: LanguageHelper.localizedString("Loading"))
        OONAApi.shared.postRequest(url: APPURL.ParentalControlAllowed(), parameter: [:]).responseJSON{ [weak self] (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let code = json["code"].int
                let status = json["status"].string
                APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                    message , _success in
                    self?.dismissHud()
                    if _success {
                        let result: [Channel] = JSON(json["channels"]).oonaModelArray()
                        success?(result)
                    } else {
                        guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                        failure?(code, message)
                    }
                })
            case .failure(let error as NSError):
                self?.dismissHud()
                print("Debug print: \(error as Any)")
                print("Debug print: \(error.localizedDescription as Any)")
                print("Debug print: \(error.domain): \(error.code)")
                guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                    return }
                failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
            }
        }
    }
    
    func getSuitablePlaylist(success: (([Channel]) -> ())?,
                             failure: ((Int?, String?) -> ())?) { //used in parental control..
        
//        let param : [String: String] =  ["filter": ChannelSetting.channelListFilterType.suitable.description]
        self.showHud(with: "Loading")
        OONAApi.shared.postRequest(url: APPURL.SuitablePlaylist, parameter: [:]).responseJSON{ [weak self] (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let code = json["code"].int
                let status = json["status"].string
                
                self?.showHud(with: LanguageHelper.localizedString("Loading"))
                APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                    message , _success in
                    
                    self?.dismissHud()
                    if _success {
                        let result: [Channel] = JSON(json["channels"]).oonaModelArray()
                        success?(result)
                    } else {
                        guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                        failure?(code, message)
                    }
                })
            case .failure(let error as NSError):
                self?.dismissHud()
                print("Debug print: \(error as Any)")
                print("Debug print: \(error.localizedDescription as Any)")
                print("Debug print: \(error.domain): \(error.code)")
                guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                    return }
                failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
            }
        }
    }
    
    func checkPasscode(passcode: String,
                       success: (() -> ())?,
                       failure: ((Int?, String?) -> ())?) {
        let param : [String : String] = ["passcode": passcode.description]
        self.showHud(with: LanguageHelper.localizedString("Loading"))
        OONAApi.shared.postRequest(url: APPURL.ParentalControlCheckPasscode(), parameter:param)
            .responseJSON{ [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let code = json["code"].int
                    let status = json["status"].string
                    self?.dismissHud()
                    APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                        message , _success in
                        if _success {
                            success?()
                        } else {
                            guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                            failure?(code, message)
                        }
                    })
                case .failure(let error as NSError):
                    self?.dismissHud()
                    print("Debug print: \(error as Any)")
                    print("Debug print: \(error.localizedDescription as Any)")
                    print("Debug print: \(error.domain): \(error.code)")
                    guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                        return }
                    failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                }
        }
    }
    
    func saveParentalControl(pc: ParentalControl,
                             success: (() -> ())?,
                             failure: ((Int?, String?) -> ())?) {
        
        let enabledText = (pc.enabled ? "1" : "0")
        let param : [String : String] = ["enabled": enabledText,
                                         "passcode": pc.passcode?.description ?? "",
                                         "rate": pc.rate?.description ?? "0",
                                         "contactEmail": pc.contactEmail ?? ""]
        self.showHud(with: "Updating")
        OONAApi.shared.postRequest(url: APPURL.ParentalControl(), parameter:param)
            .responseJSON{ [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let code = json["code"].int
                    let status = json["status"].string
                    APIResponseCode.checkResponse(withCode:code?.description ,
                                                  withStatus: status,
                                                  completion: {
                                                    message , _success in
                                                    self?.dismissHud()
                                                    if _success {
                                                        success?()
                                                    } else {
                                                        guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                                                        failure?(code, message)
                                                    }
                    })
                case .failure(let error as NSError):
                    self?.dismissHud()
                    print("Debug print: \(error as Any)")
                    print("Debug print: \(error.localizedDescription as Any)")
                    print("Debug print: \(error.domain): \(error.code)")
                    guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                        return }
                    failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                }
        }
    }
    
    func forgetPasscode(success: (() -> ())?,
                        failure: ((Int?, String?) -> ())?) {
        self.showHud(with: LanguageHelper.localizedString("Loading"))
        OONAApi.shared.postRequest(url: APPURL.ParentalControlForgotPasscode(), parameter:["":""])
            .responseJSON{ [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let code = json["code"].int
                    let status = json["status"].string
                    APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                        message , _success in
                        self?.dismissHud()
                        if _success {
                            success?()
                        } else {
                            guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                            failure?(code, message)
                        }
                    })
                case .failure(let error as NSError):
                    self?.dismissHud()
                    print("Debug print: \(error as Any)")
                    print("Debug print: \(error.localizedDescription as Any)")
                    print("Debug print: \(error.domain): \(error.code)")
                    guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                        return }
                    failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                }
        }
    }
    
    func updateAllowedChannel(action: AllowedChannelAction,
                              channelID: String,
                              success: (() -> ())?,
                              failure: ((Int?, String?) -> ())?) {
        switch action {
        case .Add:
            print("Channel Add")
            self.addAllowedChannel(channelId: channelID, success: success, failure: failure)
        case .Remove:
            print("Channel Remove")
            self.removeAllowedChannel(channelId: channelID, success: success, failure: failure)
        }
    }
    
    func removeAllowedChannel(channelId: String,
                              success: (() -> ())?,
                              failure: ((Int?, String?) -> ())?) {
        self.showHud(with: "Updating")
        OONAApi.shared.deleteRequest(url: APPURL.ParentalControlAllowedChannel(channelId: channelId), parameter:[:])
            .responseJSON{ [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let code = json["code"].int
                    let status = json["status"].string
                    APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                        message , _success in
                        self?.dismissHud()
                        if _success {
                            success?()
                        } else {
                            guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                            failure?(code, message)
                        }
                    })
                case .failure(let error as NSError):
                    self?.dismissHud()
                    print("Debug print: \(error as Any)")
                    print("Debug print: \(error.localizedDescription as Any)")
                    print("Debug print: \(error.domain): \(error.code)")
                    guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                        return }
                    failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                }
        }
    }
    
    func addAllowedChannel(channelId: String,
                           success: (() -> ())?,
                           failure: ((Int?, String?) -> ())?) {
        self.showHud(with: "Updating")
        OONAApi.shared.putRequest(url: APPURL.ParentalControlAllowedChannel(channelId: channelId), parameter:["channelId":channelId])
            .responseJSON{ [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let code = json["code"].int
                    let status = json["status"].string
                    APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                        message , _success in
                        self?.dismissHud()
                        if _success {
                            success?()
                        } else {
                            guard let _ = failure else { self?.handleFailure(code: code, message: message); return }
                            failure?(code, message)
                        }
                    })
                case .failure(let error as NSError):
                    self?.dismissHud()
                    print("Debug print: \(error as Any)")
                    print("Debug print: \(error.localizedDescription as Any)")
                    print("Debug print: \(error.domain): \(error.code)")
                    guard let _ = failure else { self?.handleFailure(code: error.code, message: "\(error.domain): " + "\(error.localizedDescription)")
                        return }
                    failure?(error.code, "\(error.domain): " + "\(error.localizedDescription)")
                }
        }
    }
    
    func handleFailure(code: Int? = nil, message: String? = nil) {
        var errorMessage: String = message ?? ""
        if let _code: Int = code { errorMessage = errorMessage + " (" + "\(String(_code))" + ")" }
        guard let _topMostVC = UIApplication.shared.keyWindow?.rootViewController?.topMostViewController() else { print("KeyWindow or rootViewController not available"); return }
        _topMostVC.showAlert(title: "Oops! Something went wrong", message: errorMessage)
    }
    
    func showHud(with status: String? = nil ) {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.show(withStatus: status)
        })
    }
    
    func dismissHud() {
        DispatchQueue.main.async(execute: {
            SVProgressHUD.dismiss()
        })
    }
}

