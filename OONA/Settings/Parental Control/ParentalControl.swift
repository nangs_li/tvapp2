//
//  ParentalControl.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

struct ParentalControl: OONAModel, Equatable {
    var enabled : Bool = false
    var passcode : String?
    var passcodeEnabled : Bool = false
    var maxActiveHoursEnabled : Int?
    var maxActiveHoursPerDay : Int?
    var rate : Int?
    var contactEmail : String?
    
    init(){
        
    }
    
    init?(json: JSON) {

        enabled = (json["enabled"].int == 1)
        passcodeEnabled = (json["passcodeEnabled"].int == 1)
        
        //passcode = json["passcode"].bool
        maxActiveHoursEnabled = json["maxActiveHoursEnabled"].int
        maxActiveHoursPerDay = json["maxActiveHoursPerDay"].int
        rate = json["rate"].int
        contactEmail = json["contactEmail"].string
    }
}
