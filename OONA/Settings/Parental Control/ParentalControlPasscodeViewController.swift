//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import RxLocalizer

class ParentalControlPasscodeViewController: SettingsBaseViewController, UITextFieldDelegate{
    
    var userId: Int!
    var email: String!
    var confirmButton: UIButton!
    var previousEnteredPasscode: Bool = false
    var isConfirm: Bool = false
    var parentalControlMode : ParentalControlMode = .none
    var parentalControlAction : ParentalControlAction = .none
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var digitTextField1: UITextField!
    @IBOutlet weak var digitTextField2: UITextField!
    @IBOutlet weak var digitTextField3: UITextField!
    @IBOutlet weak var digitTextField4: UITextField!
    @IBOutlet weak var digitTextField5: UITextField!
    @IBOutlet weak var digitTextField6: UITextField!
    @IBOutlet weak var digitView1: UIView!
    @IBOutlet weak var digitView2: UIView!
    @IBOutlet weak var digitView3: UIView!
    @IBOutlet weak var digitView4: UIView!
    @IBOutlet weak var digitView5: UIView!
    @IBOutlet weak var digitView6: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topSubLabel: UILabel!
    @IBOutlet weak var topLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    
    var currentPass : String?
    var newPass  : String?
    var currentPasswordValid : Bool = false
    var newPassValid : Bool  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Change Passcode"
        self.navigationItem.title = self.title
        
        var bottomBorder = CALayer()
        let bottomLine = CALayer()
        
        forgetPasswordButton.isHidden = true
        
        if(self.parentalControlMode == .edit){
            print("Passcode page: edit")
        }else if (self.parentalControlMode == .access) {
            print("Passcode page: access")
            topLabelConstraint.constant = 10
            topSubLabel.isHidden = true
            forgetPasswordButton.isHidden = false
            if(parentalControlAction == .changePasscode){
                topLabel.text = "Enter passcode to change passcode"
            }else{
                topLabel.text = "Enter passcode to Access Parental Control"
            }
        } else if self.parentalControlMode == .create {
            print("Passcode page: create")
        }
        
        if self.isConfirm {
            topLabelConstraint.constant = 10
            topSubLabel.isHidden = true
            topLabel.text = "Confirm Passcode"
        }
        
        digitTextField1.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField1.layer.addSublayer(bottomLine)
        
        digitTextField1.layer.masksToBounds = true // the most important line of code
        
        digitTextField2.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField2.layer.addSublayer(bottomLine)
        
        digitTextField2.layer.masksToBounds = true // the most important line of code
        
        digitTextField3.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField3.layer.addSublayer(bottomLine)
        
        digitTextField3.layer.masksToBounds = true // the most important line of code
        
        digitTextField4.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField4.layer.addSublayer(bottomLine)
        
        digitTextField4.layer.masksToBounds = true // the most important line of code
        
        digitTextField5.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField5.layer.addSublayer(bottomLine)
        
        digitTextField5.layer.masksToBounds = true // the most important line of code
        
        digitTextField6.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 1.0)
        
        digitTextField6.layer.addSublayer(bottomLine)
        
        digitTextField6.layer.masksToBounds = true // the most important line of code
        
        digitTextField1.delegate = self;
        digitTextField2.delegate = self;
        digitTextField3.delegate = self;
        digitTextField4.delegate = self;
        digitTextField5.delegate = self;
        digitTextField6.delegate = self;
        digitTextField1.tag = 1;
        digitTextField2.tag = 2;
        digitTextField3.tag = 3;
        digitTextField4.tag = 4;
        digitTextField5.tag = 5;
        digitTextField6.tag = 6;
        
        digitTextField1.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.backButton.isHidden = !((self.parentalControlMode == .create && self.isConfirm) || self.parentalControlMode == .edit)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        self.confirm()
        return true
    }
    
    @IBAction func next(_ sender: UIButton) {
        self.confirm()
    }
    
    @IBAction func forgetPasscode(_ sender: UIButton) {
        //TODO_OONA : forgt passcode
        //self.showDialog(title: "", msg: "Are you sure you want to reset your passcode?", okText: "Yes", cancelText: "No")
        
        let controller = UIAlertController(title: "", message: "Are you sure you want to reset your passcode?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { [unowned helper = ParentalControlHelper.shared] (_) in
            helper.forgetPasscode(
                success: { [weak self] in
                    self?.showAlert(title: "Reset Passcode", message : "Please check your email to get new passcode.")
            }, failure: nil)
        }
        controller.addAction(okAction)
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
    
    func keyboardInputShouldDelete(_ textField: UITextField?) -> Bool {
        // on deleteing value from Textfield
        
        let prevTag = (textField?.tag ?? 0) - 1
        // Try to find prev responder
        let prevResponder = textField?.superview?.viewWithTag(prevTag)
        if prevResponder == nil {
            return true
        }
        if prevResponder != nil {
            // Found next responder, so set it.
            prevResponder?.becomeFirstResponder()
        }
        textField?.text = ""
        //errorLabel.hidden = true TODO_OONA : error
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // NSLog(@"%@ was pressed",string);
        var textCount = 0
        if let _textCount = textField.text?.count{
            textCount = _textCount
        }
        
        if ((textCount == 0) && (string.count > 0))
        {
            textField.text = string;
            let nextTag = textField.tag;
            enterField(tag: nextTag, string: string)
            return false
        }else if ((textCount >= 1) && (string.count > 0)) {
            //FOR MAXIMUM 1 TEXT
            let nextTag = textField.tag + 1;
            enterField(tag: nextTag, string: string)
            
            return false
        }else if ((textCount >= 2) && (string.count == 0)) {
            // on deleteing value from Textfield
            
            let prevTag = textField.tag - 1;
            // Try to find prev responder
            if let txt = self.view.viewWithTag(prevTag) as? UITextField {
                //Your code
                txt.becomeFirstResponder()
                textField.text = string;
            }
            return false
        }
        return true
    }
    
    func enterField(tag: Int, string: String) {
        switch(tag) {
        case 1:
            digitTextField1.becomeFirstResponder()
            digitTextField1.text = string
        case 2:
            digitTextField2.becomeFirstResponder()
            digitTextField2.text = string
        case 3:
            digitTextField3.becomeFirstResponder()
            digitTextField3.text = string
        case 4:
            digitTextField4.becomeFirstResponder()
             digitTextField4.text = string
        case 5:
            digitTextField5.becomeFirstResponder()
            digitTextField5.text = string
        case 6:
            digitTextField6.becomeFirstResponder()
            digitTextField6.text = string
            confirm()
        default:
            break;
        }
    }
    
    
    func reset() -> Void{
        digitTextField1.text = "";
        digitTextField1.becomeFirstResponder()
        digitTextField2.text = "";
        digitTextField3.text = "";
        digitTextField4.text = "";
        digitTextField5.text = "";
        digitTextField6.text = "";
        
    }
    
    func getPinCode() -> String {
        let txt1 : String = digitTextField1.text ?? ""
        let txt2 : String  = digitTextField2.text ?? ""
        let txt3 : String  = digitTextField3.text ?? ""
        let txt4 : String  = digitTextField4.text ?? ""
        let txt5 : String  = digitTextField5.text ?? ""
        let txt6 : String  = digitTextField6.text ?? ""
        let pincode = txt1 + txt2 + txt3 + txt4 + txt5 + txt6
        
        return pincode
    }
    
    func confirm() {
        self.view.endEditing(true)
        if (self.parentalControlMode == .create || self.parentalControlMode == .edit) {
            if !self.isConfirm {
                let parentalControlPasscodeViewController : ParentalControlPasscodeViewController = UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlPasscodeViewController") as! ParentalControlPasscodeViewController
                parentalControlPasscodeViewController.isConfirm = true
                parentalControlPasscodeViewController.currentPass = getPinCode()
                parentalControlPasscodeViewController.parentalControlMode = self.parentalControlMode
                
                self.navigationController?.pushViewController(parentalControlPasscodeViewController, animated: true)
            } else {
                    if (getPinCode() == currentPass) {
                        
                        if (self.parentalControlMode == .create) {
                            let parentalControlEmailViewController : ParentalControlEmailViewController  =   UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlEmailViewController") as! ParentalControlEmailViewController
                            parentalControlEmailViewController.parentalControlMode = self.parentalControlMode
                            //set passcode
                            var pc = ParentalControl()
                            pc.passcode = currentPass
                            pc.enabled = true
                            parentalControlEmailViewController.parentalControl = pc
                            
                            self.navigationController?.pushViewController(parentalControlEmailViewController, animated: true)
                            
                        } else {
                            
                            if var _pc = ParentalControlHelper.shared.currentParentalControl.value {
                                
                                _pc.passcode = getPinCode()
                                
                                ParentalControlHelper.shared.saveParentalControl(pc: _pc, success: { [weak self, unowned helper = ParentalControlHelper.shared] in
                                    helper.getParentalControlPreference(success: nil,
                                                                        failure: nil)
                                    //back to orignal..
                                    self?.dismiss(animated: true, completion: nil)
                                    
                                }, failure: nil)
                            }
                            
                        }
                    } else {
                        self.showAlert(title: "", message : "You seem to have typed a wrong passcode.")
                    }
            }
        }else if (self.parentalControlMode == .access) {
            //after checking
            
            if(parentalControlAction == .changePasscode){
                
                let parentalControlPasscodeViewController : ParentalControlPasscodeViewController  =   UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlPasscodeViewController") as! ParentalControlPasscodeViewController
                parentalControlPasscodeViewController.isConfirm = false
                parentalControlPasscodeViewController.parentalControlMode = .edit
                parentalControlPasscodeViewController.parentalControlAction = self.parentalControlAction
                 self.navigationController?.pushViewController(parentalControlPasscodeViewController, animated: true)
                
            } else {
                
                ParentalControlHelper.shared.checkPasscode(passcode:
                    getPinCode(), success: { [weak self, unowned helper = ParentalControlHelper.shared] in
                        self?.dismiss(animated: true, completion: {
                            helper.allowToAccessParentalCControl.accept(true)
                        })
                }, failure: { [weak self, unowned helper = ParentalControlHelper.shared] code, msg in
                    var errorMessage: String = ""
                    if let _msg: String = msg { errorMessage = _msg }
                    if let _code: Int = code { errorMessage = errorMessage + "(" + "\(String(_code))" + ")" }
                    self?.showAlert(title: "Oops! Something went wrong", message: errorMessage)
                    helper.allowToAccessParentalCControl.accept(false)
                })
            }
            
        }
    }
    @IBAction func backToPrevious(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.navigationController?.popViewController(animated: true)
        self.leaveAction()
    }
    
    @IBAction override func close(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
//        parentalControlPasscodeViewController.parentalControlMode = .access
//        parentalControlPasscodeViewController.parentalControlAction = .changePasscode
        
        if ((self.parentalControlMode == .access && self.parentalControlAction == .none) || self.parentalControlMode == .create) {
            ParentalControlHelper.shared.allowToAccessParentalCControl.accept(false)
        }
        self.dismiss(animated: true)
    }
    
    override func appLanguageDidUpdate() {
        self.topSubLabel.text = LanguageHelper.localizedString("you_need_a_passcode_to_turn_parental_control_on")
        self.topLabel.text = LanguageHelper.localizedString("create_6_digit_passcode")
        self.forgetPasswordButton.setTitle(LanguageHelper.localizedString("forgot_password_text"), for: .normal)
    }
    
    deinit {
//        print("deallocating" + String(describing: type(of: self)))
    }
}
