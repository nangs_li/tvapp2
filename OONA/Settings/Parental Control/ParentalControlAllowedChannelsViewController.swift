
//  ParentalControlViewController
//  OONA
//
//  Created by Vick on 9/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift
import RxCocoa

class ParentalControlAllowedChannelsViewController: SettingsBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {
    var upperViewHeight = 58
    var channelList: BehaviorRelay<[[Channel]]> = BehaviorRelay<[[Channel]]>(value: [[Channel]]())
    var allowedChannels: BehaviorRelay<[Channel]> = BehaviorRelay<[Channel]>(value: [Channel]())
    var toBeUploadedChannels: [String] = [String]()
    var selectedAlphabet: Int = 0
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var cvViewHeight: NSLayoutConstraint!
    var alphabets = [String]()
    
    override func viewDidLoad () {
        super.viewDidLoad()
        self.title = LanguageHelper.localizedString("edit_channels_text")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
        
        setupCollectionView()
        setupTableView()
        
        getSuitableChannel()
        getAllowedChannel()
        
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        
        self.tv.backgroundColor = UIColor.black
//
//        let str: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
//
//        for i in str {
//            alphabets.append(String(i))
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func setupTableView() {
        
        self.tv.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        self.tv.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tv.delegate = self
        self.tv.dataSource = self
        
    }
    
    func setupCollectionView() {
        self.cv.register(UINib(nibName: "ParentalControlViewControllerCell", bundle: nil), forCellWithReuseIdentifier: "ParentalControlViewControllerCell")

        self.cv.register(
            UICollectionReusableView.self,
            forSupplementaryViewOfKind:
            UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: "ParentalControlViewControllerHeader")
        
        self.cv.delegate = self
        self.cv.dataSource = self
        self.channelList.subscribe(onNext: { [weak self] ( _ ) in
            self?.cv.reloadData()
        }).disposed(by: self.disposeBag)
        
        self.allowedChannels.subscribe(onNext: { [weak self] ( _ ) in
            self?.cv.reloadData()
        }).disposed(by: self.disposeBag)
        
    }
    
    func SplitChannelByFirstCharater(channels : [Channel]) -> [[Channel]]{
        
        let _channels = channels.sorted(by: { $0.name.prefix(1) > $1.name.prefix(1) })
        
        self.alphabets = Array(Set(_channels.map({ String($0.name.prefix(1).capitalized) }))).sorted()
        self.tv.reloadData()
        //var currentIndex = 0
        var result = [[Channel]]() //two - dimentional array
        
        for alphabet in alphabets {
            var currentChannels = [Channel]()
            for channel in _channels {
                if (alphabet == channel.name.prefix(1).description) {
                    currentChannels.append(channel)
                }
            }
            
            let _currentChannels = currentChannels.sorted(by: { $0.name.prefix(2) > $1.name.prefix(2) }) //sort by alphateical order..
            
            result.append(_currentChannels)
            
            //currentIndex += 1
        }
        
        return result
    }
    
    func updateCVViewHeight() {
        print("updateCVViewHeight")
        let numberOfLines = channelList.value.count / 5
        self.cvViewHeight.constant = CGFloat(numberOfLines) * (APPSetting.episodeChannelObjectHeight + 10)
        print("updateCVViewHeight" + self.cvViewHeight.constant.description)
    }
    
    override func appLanguageDidUpdate() {
        self.title = LanguageHelper.localizedString("edit_channels_text")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
        
        self.button.setTitle(LanguageHelper.localizedString("done"), for: .normal)
    }
    
    // MARK: - IBActions
    @IBAction func submit(_ sender: UIButton) {
        
//        let updateChannelGroup = DispatchGroup()
//        for channelID in self.toBeUploadedChannels {
//
//            ParentalControlHelper.shared.removeAllowedChannel(channelId: String(channelID),
//                                                              success: ({ [weak self] in
//                //                    //retrieve the list..
//                print("remove succesfully pc : " + String(channelID))
//            }), failure: {code, msg in
//                if let _msg = msg as? String{
//                    self.showAlert(title: "", message : _msg)
//                }
//            })
//        }
//
//        updateChannelGroup.notify(queue: DispatchQueue.main) {
//            self.navigationController?.popViewController(animated: true)
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction override func close(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.dismiss(animated: true)
        
    }
    
    // MARK: - Fetch Data
    func getAllowedChannel() {
        ParentalControlHelper.shared.getAllowedPlaylist(
            success: { [weak self] (channels) in
                print("Allowed Count: \(channels.count)")
                self?.allowedChannels.accept(channels)
            },
            failure: nil)
    }
    
    func getSuitableChannel() {
        ParentalControlHelper.shared.getSuitablePlaylist(
            success: { [weak self] (channels) in
                if let _listChannels = self?.SplitChannelByFirstCharater(channels: channels) {
                    print("Suitable Count: \(channels.count)")
                    self?.channelList.accept(_listChannels)
                }else{
                    print("fail to split channel list!")
                }
                //print("\(String(describing: type(of: self)))channel list: ",channels)
            },
            failure: nil)
    }
    
    func addAllowedChannel(channel: Channel) {
        var _allowedChannels = self.allowedChannels.value
        if !self.allowedChannels.value.contains(where: { $0 == channel }) {
            _allowedChannels.append(channel)
            if !self.toBeUploadedChannels.contains(String(channel.id)) {
                self.toBeUploadedChannels.append(String(channel.id))
            }
        }
        
        self.allowedChannels.accept(_allowedChannels)
    }
    
    func removeAllowedChannel(channel: Channel) {
        var _allowedChannels = self.allowedChannels.value
        if self.allowedChannels.value.contains(where: { $0 == channel }) {
            _allowedChannels.removeAll(where: { $0 == channel })
            if !self.toBeUploadedChannels.contains(String(channel.id)) {
                self.toBeUploadedChannels.append(String(channel.id))
            } else {
                self.toBeUploadedChannels.removeAll(where: { String($0) == String(channel.id) })
            }
        }
        
        self.allowedChannels.accept(_allowedChannels)
    }
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return alphabets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =
            tableView.dequeueReusableCell(withIdentifier:
                "Cell", for: indexPath) as
        UITableViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.contentView.backgroundColor = UIColor.black
        
        if let myLabel = cell.textLabel {
            myLabel.text = alphabets[indexPath.row]
            if(selectedAlphabet == indexPath.row){
                //selected
                myLabel.textColor = UIColor.white
            }else{
                //un selected
                myLabel.textColor = UIColor.gray
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath){
        //table indexpath.row is alphabet, collectionview indexpath.section is alphbet
        if let attributes = self.cv.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: indexPath.row)) {
            self.cv.setContentOffset(CGPoint(x: 0, y: attributes.frame.origin.y - self.cv.contentInset.top), animated: true)
        }
        
        selectedAlphabet = indexPath.row
        self.tv.reloadData()
        
    }
    
    // MARK: - CollectionView Delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return alphabets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var result = 0
        if section < self.channelList.value.count{
            result = self.channelList.value[section].count
        }
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView{
        
        var reusableView = UICollectionReusableView()
        if(kind == "UICollectionElementKindSectionHeader"){
            
            // header
            reusableView =
                collectionView.dequeueReusableSupplementaryView(
                    ofKind: UICollectionView.elementKindSectionHeader,
                    withReuseIdentifier: "ParentalControlViewControllerHeader",
                    for: indexPath as IndexPath)
            
            if(alphabets.count > indexPath.section){
                //print("set up header" + text)
                if reusableView.subviews.count == 0 {
                    let lab = UILabel(frame:CGRect(x: 0,y: 0,width: 30,height: 30))
                    lab.font = UIFont(name: "Montserrat-SemiBold", size: 24)!
                    lab.textColor = UIColor.white
                    lab.textAlignment = .left
                    reusableView.addSubview(lab)
                }
                let lab = reusableView.subviews[0] as! UILabel
                lab.text = alphabets[indexPath.section]
                
                // reusableView.addSubview(label)
            }
        }
        
        return reusableView
    }
    
    func indexTitles(for collectionView: UICollectionView) -> [String]? {
        return self.alphabets
    }
    
    func collectionView(_ collectionView: UICollectionView, indexPathForIndexTitle title: String, at index: Int) -> IndexPath {
        
        return IndexPath(item: 0, section: index)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print("indexPath.section "+indexPath.section.description)
//        print("indexPath.item "+indexPath.item.description)
        var cell = ParentalControlViewControllerCell()
        if indexPath.section < self.channelList.value.count && indexPath.item < self.channelList.value[indexPath.section].count {
            let _channel = self.channelList.value[indexPath.section][indexPath.item]

            cell = self.cv.dequeueReusableCell(withReuseIdentifier: "ParentalControlViewControllerCell", for: indexPath) as! ParentalControlViewControllerCell
            cell.configure(with: _channel)
            cell.tag = _channel.id //use in didselect...
            //let _allowedChannels = ParentalControlHelper.shared.allowedChannels
            let active = allowedChannels.value.contains(where: { $0 == _channel })
            print("channel \(_channel.id.description) is " + (active ? "Active" : "Inactive"))
            cell.isActive = active
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell : ParentalControlViewControllerCell = collectionView.cellForItem(at: indexPath) as! ParentalControlViewControllerCell
        
        if indexPath.section < self.channelList.value.count && indexPath.item < self.channelList.value[indexPath.section].count {
            let channel = channelList.value[indexPath.section][indexPath.item]
            let updateAction: AllowedChannelAction = cell.isActive ? .Remove : .Add
            print(updateAction)
            ParentalControlHelper.shared.updateAllowedChannel(action: updateAction,
                                                              channelID: channel.id.description,
                                                              success: { [weak self, weak cell] in
                                                                if updateAction == .Add {
                                                                    // Add to channel
                                                                    self?.addAllowedChannel(channel: channel)
                                                                } else {
                                                                    // Remmove from channel
                                                                    self?.removeAllowedChannel(channel: channel)
                                                                }
                                                                
                                                                //retrieve the list..
                                                                if let _cell = cell {
                                                                    print("\(updateAction) succesfully pc: " + _cell.tag.description)
                                                                }
                                                                self?.cv.reloadData()
                }, failure: nil)
        }
    }
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
//        self.navigationController?.popViewController(animated: true)
        self.leaveAction()
    }
    
    deinit {
//        print("deallocating" + String(describing: type(of: self)))
    }
}

