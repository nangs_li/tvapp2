//
//  ParentalControlViewController
//  OONA
//
//  Created by Vick on 9/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift
import RxCocoa

class ParentalControlViewController: SettingsBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    var channelList: BehaviorRelay<[Channel]> = BehaviorRelay<[Channel]>(value: [Channel]())
    
    var disallowedList: [Channel] = [Channel]()
    
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var sv: UIScrollView!
    
    @IBOutlet weak var preSchoolView: UIView!
    @IBOutlet weak var childrenView: UIView!
    @IBOutlet weak var teenagersView: UIView!
    @IBOutlet weak var featuresLabel: UILabel!
    @IBOutlet weak var switchEnable: UISwitch!
    @IBOutlet weak var changePasscodeButton: UIButton!
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    @IBOutlet weak var parentalControlTitleLabel: UILabel!
    @IBOutlet weak var parentalControlSubTitleLabel: UILabel!
    
    
    @IBOutlet weak var teenagerLabel: UILabel!
    @IBOutlet weak var teenagerDesc: UILabel!
    
    @IBOutlet weak var preSchoolLabel: UILabel!
    @IBOutlet weak var preSchoolDesc: UILabel!
    
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var childrenDesc: UILabel!
    
    @IBOutlet weak var teenagerImageView: UIImageView!
    @IBOutlet weak var preSchooImageView: UIImageView!
    @IBOutlet weak var childrenImageView: UIImageView!
    
    @IBOutlet weak var seperator: UIView!
    
    @IBOutlet weak var lblAllowChannels: UILabel!
    @IBOutlet weak var lblAllowChannelsSubTitle: UILabel!
    @IBOutlet weak var btnAllowChannels: UIButton!
    
    @IBOutlet weak var cvViewHeight: NSLayoutConstraint!
    //@IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    var upperViewHeight : Int = 414
    
    var isTeenagerEnable: Bool = false
    var ispreSchoolEnable: Bool = false
    var isChildrenEnable: Bool = false
    
    
    override func viewDidLoad () {
        super.viewDidLoad()
        self.title = LanguageHelper.localizedString("parental_control")
        self.navigationItem.title = self.title
//        self.showTopBar = true
        
        ParentalControlHelper.shared.currentParentalControl.skip(1).subscribe(onNext: { [weak self] (pc) in
            self?.processParentalControl()
        }).disposed(by: self.disposeBag)
        
        var alphabet = [String]()
        
        let str: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        for i in str {
            alphabet.append(String(i))
        }
        
        //self.isActiveLabel.text = "Off"
        //self.turnOnOffBUtton.titleLabel?.text = "Turn On"
        
        btnAllowChannels.layer.cornerRadius = 5
        btnAllowChannels.layer.borderWidth = 1
        btnAllowChannels.layer.borderColor = UIColor.white.cgColor
        
        changeEmailButton.layer.cornerRadius = 5
        changeEmailButton.layer.borderWidth = 1
        changeEmailButton.layer.borderColor = UIColor.white.cgColor
        
        changePasscodeButton.layer.cornerRadius = 5
        changePasscodeButton.layer.borderWidth = 1
        changePasscodeButton.layer.borderColor = UIColor.white.cgColor
        
        preSchoolView.layer.cornerRadius = 5
        preSchoolView.layer.borderWidth = 1
        preSchoolView.layer.borderColor = UIColor.white.cgColor
        
        preSchoolView.layer.cornerRadius = 5
        preSchoolView.layer.borderWidth = 1
        preSchoolView.layer.borderColor = UIColor.white.cgColor
        
        childrenView.layer.cornerRadius = 5
        childrenView.layer.borderWidth = 1
        childrenView.layer.borderColor = UIColor.white.cgColor
        
        teenagersView.layer.cornerRadius = 5
        teenagersView.layer.borderWidth = 1
        teenagersView.layer.borderColor = UIColor.white.cgColor
        
        switchEnable.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        setupCollectionView()
        
        self.checkParentalControlStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTopBar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showTopBar = false
    }
    
    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - IBActions
    @IBAction func backToPrevious(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        //        self.navigationController?.popViewController(animated: true)
        ParentalControlHelper.shared.allowToAccessParentalCControl.accept(false)
        self.leaveAction()
        
    }
    
    @IBAction override func close(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.leaveAction()
        
    }
    
    @IBAction func clickEdit(_ sender: UIButton) {
        
        let parentalControlAllowedChannelsViewController : ParentalControlAllowedChannelsViewController  =   UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlAllowedChannelsViewController") as! ParentalControlAllowedChannelsViewController
        
        self.navigationController?.pushViewController(parentalControlAllowedChannelsViewController, animated: true)
        
    }
    
    @IBAction func clickPreSchoolView(_ sender: UIButton) {
        if let pc = ParentalControlHelper.shared.currentParentalControl.value {
            if pc.rate != nil && pc.rate != 1 {
                self.selectRate(rate: 1)
            }
        }
    }
    
    @IBAction func clickChildrenView(_ sender: UIButton) {
        if let pc = ParentalControlHelper.shared.currentParentalControl.value {
            if pc.rate != nil && pc.rate != 2 {
                self.selectRate(rate: 2)
            }
        }
    }
    
    @IBAction func clickTeenagersView(_ sender: UIButton) {
        if let pc = ParentalControlHelper.shared.currentParentalControl.value {
            if pc.rate != nil && pc.rate != 3 {
                self.selectRate(rate: 3)
            }
        }
    }
    
    @IBAction func clickChangePasscode(_ sender: UIButton) {
        
        let parentalControlPasscodeViewController : ParentalControlPasscodeViewController  =   UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlPasscodeViewController") as! ParentalControlPasscodeViewController
        
        parentalControlPasscodeViewController.parentalControlMode = .access
        parentalControlPasscodeViewController.parentalControlAction = .changePasscode
        
        let nvc: SettingsNavigationViewController = SettingsNavigationViewController(rootViewController: parentalControlPasscodeViewController)
        nvc.modalTransitionStyle = .crossDissolve
        nvc.navigationBar.isHidden = true
        self.present(nvc, animated: true, completion: nil)
//        self.navigationController?.pushViewController(parentalControlPasscodeViewController, animated: true)
    }
    
    @IBAction func clickChangeEmail(_ sender: UIButton) {
        
        let parentalControlEmailViewController : ParentalControlEmailViewController  =   UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlEmailViewController") as! ParentalControlEmailViewController
        parentalControlEmailViewController.parentalControl = ParentalControlHelper.shared.currentParentalControl.value
        parentalControlEmailViewController.parentalControlMode = .edit
        parentalControlEmailViewController.modalTransitionStyle = .crossDissolve
//        self.navigationController?.pushViewController(parentalControlEmailViewController, animated: true)
        self.present(parentalControlEmailViewController, animated: true, completion: nil)
    }
    
    @IBAction func switchChanged(theSwitch: UISwitch) {
        
        print("switch " + theSwitch.isOn.description)
        if var pc = ParentalControlHelper.shared.currentParentalControl.value {
            if theSwitch.isOn {
                // turn on parental control
                if pc.passcodeEnabled {
                    self.showPasscodePage(with: .access)
                } else {
                    self.showPasscodePage(with: .create)
                }
//                self.checkParentalControlStatus()
            } else {
                // turn off parental control
                pc.enabled = theSwitch.isOn
                ParentalControlHelper.shared.allowToAccessParentalCControl.accept(false)
                ParentalControlHelper.shared.saveParentalControl(pc: pc,
                                                                 success: { [unowned helper = ParentalControlHelper.shared] in
                                                                    helper.getParentalControlPreference(success: nil,
                                                                                                        failure: nil)
                }, failure: nil)
            }
        } else {
            theSwitch.setOn(false, animated: true)
        }
    }
    
    func checkParentalControlStatus() {
        if let _ = ParentalControlHelper.shared.currentParentalControl.value {
            self.processParentalControl()
        } else {
            self.checkParentalControl(completion: nil)
        }
    }
    
    private func processParentalControl() {
        
        if let _pc = ParentalControlHelper.shared.currentParentalControl.value {
            if _pc.enabled && _pc.passcodeEnabled && !ParentalControlHelper.shared.allowToAccessParentalCControl.value {
                self.showPasscodePage(with: .access)
            }
            self.updateParentalControlUI(parentalControl: _pc)
        } else {
            // Cannot get parental control settings should update UIs to hidden
            self.updateParentalControlUI(parentalControl: nil)
        }
    }
    
    private func showPasscodePage(with parentalControlMode: ParentalControlMode) {
        let passcodeViewController = UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlPasscodeViewController") as! ParentalControlPasscodeViewController
        passcodeViewController.parentalControlMode = parentalControlMode
        ParentalControlHelper.shared.allowToAccessParentalCControl.skip(1).subscribe(onNext: { [weak self, unowned helper = ParentalControlHelper.shared] (allowed) in
            if parentalControlMode == .access && !allowed {
                if let pc = helper.currentParentalControl.value, pc.enabled {
                    self?.navigationController?.popViewController(animated: true)
                }
                self?.processParentalControl()
            } else if (parentalControlMode == .create && !allowed) {
                self?.processParentalControl()
            } else {
                if var pc = helper.currentParentalControl.value {
                    pc.enabled = allowed
                    helper.saveParentalControl(pc: pc, success: {
                        helper.getParentalControlPreference(success: nil, failure: nil)
                    }, failure: nil)
                }
                
                self?.processParentalControl()
            }
        }).disposed(by: passcodeViewController.disposeBag)
        if parentalControlMode == .access {
            //Already set passcode, but not verify
            passcodeViewController.modalTransitionStyle = .crossDissolve
            self.present(passcodeViewController, animated: true, completion: nil)
        } else {
            //Passcode never been set
            let nvc: SettingsNavigationViewController = SettingsNavigationViewController(rootViewController: passcodeViewController)
            nvc.modalTransitionStyle = .crossDissolve
            nvc.navigationBar.isHidden = true
            self.present(nvc, animated: true, completion: nil)
        }
    }
    
    func updateParentalControlUI(parentalControl: ParentalControl?) {
        var enabled = false
        if let pc = parentalControl {
          enabled = pc.enabled && ParentalControlHelper.shared.allowToAccessParentalCControl.value
        }
        
        self.switchEnable.isOn = enabled
        self.preSchoolView.isHidden = !enabled
        self.childrenView.isHidden = !enabled
        self.teenagersView.isHidden = !enabled
        self.featuresLabel.isHidden = !enabled
        self.txtEmail.isHidden = !enabled
        self.lblEmail.isHidden = !enabled
        self.cv.isHidden = !enabled
        
        self.txtEmail.text = parentalControl?.contactEmail
        
        
        self.changePasscodeButton.isHidden = !enabled
        self.changeEmailButton.isHidden = !enabled
        self.seperator.isHidden = !enabled
        
        
        lblAllowChannels.isHidden = true
        lblAllowChannelsSubTitle.isHidden = true
        self.btnAllowChannels.isHidden = true
        
        if enabled {
            if let rate = parentalControl?.rate{
                self.initView()
                switch rate {
                case 1:
                    self.selectPreSchoolUpdateUI()
                case 2:
                    self.selectChildrenUpdateUI()
                case 3:
                    self.selectTeenagerUpdateUI()
                default:
                    self.initView()
                }
            }
        }
    }
    
    func selectChildrenUpdateUI() {
        isTeenagerEnable = false
        isChildrenEnable = true
        ispreSchoolEnable = false
        
        lblAllowChannels.isHidden = false
        lblAllowChannelsSubTitle.isHidden = false
        self.btnAllowChannels.isHidden = false
        
        self.childrenView.tag = 1
        self.preSchoolView.tag = 0
        self.teenagersView.tag = 0
        self.childrenView.backgroundColor = Color.BlurryWhiteBackground()
        self.showAllowChannels()
    }
    
    func selectPreSchoolUpdateUI() {
        isTeenagerEnable = false
        isChildrenEnable = false
        ispreSchoolEnable = true
        
        lblAllowChannels.isHidden = false
        lblAllowChannelsSubTitle.isHidden = false
        self.btnAllowChannels.isHidden = false
        
        self.childrenView.tag = 0
        self.preSchoolView.tag = 1
        self.teenagersView.tag = 0
        self.preSchoolView.backgroundColor = Color.BlurryWhiteBackground()
        self.showAllowChannels()
    }
    
    func selectTeenagerUpdateUI() {
        isTeenagerEnable = true
        isChildrenEnable = false
        ispreSchoolEnable = false
        
        lblAllowChannels.isHidden = false
        lblAllowChannelsSubTitle.isHidden = false
        self.btnAllowChannels.isHidden = false
        
        self.childrenView.tag = 0
        self.preSchoolView.tag = 0
        self.teenagersView.tag = 1
        self.teenagersView.backgroundColor = Color.BlurryWhiteBackground()
        self.showAllowChannels()
        
    }
    
    func selectChildren() {
        initView()
        
        if var pc = ParentalControlHelper.shared.currentParentalControl.value {
            pc.rate = 2
            ParentalControlHelper.shared.saveParentalControl(pc: pc,
                                                             success: { [unowned helper = ParentalControlHelper.shared] in
                                                                helper.getParentalControlPreference(success: nil,
                                                                                                    failure: nil)
                                                                self.selectChildrenUpdateUI()
                
            }, failure: nil)
        }
        
    }
    
    func selectPreSchool() {
        
        initView()
        if var pc = ParentalControlHelper.shared.currentParentalControl.value {
            pc.rate = 1
            ParentalControlHelper.shared.saveParentalControl(pc: pc,
                                                             success: { [unowned helper = ParentalControlHelper.shared] in
                helper.getParentalControlPreference(success: nil,
                                                                          failure: nil)
                self.selectPreSchoolUpdateUI()
                
            }, failure: nil)
        }
    }
    
    
    func selectRate(rate: Int) {
        
        initView()
        
        //if let pc = ParentalControlHelper.shared.currentParentalControl{
        //    self.saveRateAndPush(rate: rate, pc: pc)

        //}else{
        ParentalControlHelper.shared.getParentalControlPreference(success: { pc in
            self.saveRateAndPush(rate: rate, pc: pc)
        }, failure: nil)
        //}
        
    }
        
    func saveRateAndPush(rate: Int, pc: ParentalControl) {
        var _pc = pc
        _pc.rate = rate
        
        ParentalControlHelper.shared.saveParentalControl(pc: _pc,
                                                         success: {
            let parentalControlAllowedChannelsViewController : ParentalControlAllowedChannelsViewController  = UIStoryboard(name: "ParentalControl", bundle: nil).instantiateViewController(withIdentifier: "ParentalControlAllowedChannelsViewController") as! ParentalControlAllowedChannelsViewController
            self.navigationController?.pushViewController(parentalControlAllowedChannelsViewController, animated: true)
            
            if(rate==1){
                self.selectPreSchool()
            }
            else if(rate==2){
                self.selectChildrenUpdateUI()
            }
            else if(rate==3){
                
                self.selectTeenagerUpdateUI()
            }
            ParentalControlHelper.shared.getParentalControlPreference(success: nil,
                                                                      failure: nil)
        }, failure: nil)
    }
    
    func addAllowedChannel(channel: Channel) {
        if self.disallowedList.contains(where: { $0 == channel }) {
            disallowedList.removeAll(where: { $0 == channel })
            //            if !self.toBeUploadedChannels.contains(String(channel.id)) {
            //                self.toBeUploadedChannels.append(String(channel.id))
            //            } else {
            //                self.toBeUploadedChannels.removeAll(where: { String($0) == String(channel.id) })
            //            }
        }
        //        self.allowedChannels.accept(_allowedChannels)
    }
    
    func removeAllowedChannel(channel: Channel) {
        if !self.disallowedList.contains(where: { $0 == channel }) {
            disallowedList.append(channel)
            //            if !self.toBeUploadedChannels.contains(String(channel.id)) {
            //                self.toBeUploadedChannels.append(String(channel.id))
            //            }
        }
        //        self.allowedChannels.accept(_allowedChannels)
    }
    
    func showAllowChannels() {
        cv.isHidden = false
        getAllowedChannel()
    }
    
    func initView() {
        self.teenagerLabel.textColor = UIColor.white
        self.teenagerDesc.textColor = UIColor.white
        self.teenagersView.backgroundColor = UIColor.black
        self.preSchoolLabel.textColor = UIColor.white
        self.preSchoolDesc.textColor = UIColor.white
        self.preSchoolView.backgroundColor = UIColor.black
        self.childrenLabel.textColor = UIColor.white
        self.childrenDesc.textColor = UIColor.white
        self.childrenView.backgroundColor = UIColor.black
    }
    
    func setupCollectionView() {
        self.cv.register(UINib(nibName: "ParentalControlViewControllerCell", bundle: nil), forCellWithReuseIdentifier: "ParentalControlViewControllerCell")
        //self.cv.collectionViewLayout = cvLayout
        
        self.cv.delegate = self
        self.cv.dataSource = self
        
        self.channelList.subscribe(onNext: { [weak self] ( _ ) in
            self?.cv.reloadData()
            self?.updateCVViewHeight()
            self?.updateScrollViewHeight()
        }).disposed(by: self.disposeBag)
        
        
    }
    
    func getAllowedChannel() {
        ParentalControlHelper.shared.getAllowedPlaylist(
            success: {[weak self] channels in
                self?.channelList.accept(channels)
            },
            failure: nil)
    }
    
    func updateScrollViewHeight(){
        
//        var numberOfLines = channelList.value.count / 5
//        if(channelList.value.count == 0){
//            numberOfLines = 0
//        }
//        else if(channelList.value.count < 5){
//            numberOfLines = 1
//        }
//        let scrollViewHeightVar = (CGFloat(numberOfLines) * (APPSetting.episodeChannelObjectHeight + 10)) + CGFloat(upperViewHeight)
        let scrollViewHeightVar = self.cv.contentSize.height + CGFloat(upperViewHeight)
        
//        print("updateScrollViewHeight numberOfLines " + numberOfLines.description)
        print("updateScrollViewHeight scrollViewHeightVar " + scrollViewHeightVar.description)
        //self.scrollViewHeight.constant = scrollViewHeightVar
        sv.layoutIfNeeded()
        sv.isScrollEnabled = true
        self.sv.contentSize = CGSize(width: self.sv.contentSize.width, height: scrollViewHeightVar)
        
    }
    
    func updateCVViewHeight(){
        print("updateCVViewHeight")
//        var numberOfLines = channelList.value.count / 5
//        if(channelList.value.count == 0){
//            numberOfLines = 0
//        }
//        else if(channelList.value.count < 5){
//            numberOfLines = 1
//        }
//        self.cvViewHeight.constant = CGFloat(numberOfLines) * (APPSetting.episodeChannelObjectHeight + 10)
        self.cvViewHeight.constant = self.cv.contentSize.height
//        print("updateCVViewHeight numberOfLines " + numberOfLines.description)
        print("updateCVViewHeight scrollViewHeightVar " + self.cvViewHeight.constant.description)
    }
    
    override func appLanguageDidUpdate() {
        self.parentalControlTitleLabel.text = LanguageHelper.localizedString("parental_control_text")
        self.parentalControlSubTitleLabel.text = LanguageHelper.localizedString("parental_control_hint_text")
        
        self.lblEmail.text = LanguageHelper.localizedString("email_colon_text")
        
        self.changeEmailButton.setTitle(LanguageHelper.localizedString("change_email_text"), for: .normal)
        self.changePasscodeButton.setTitle(LanguageHelper.localizedString("change_passcode_text"), for: .normal)
        
        self.featuresLabel.text = LanguageHelper.localizedString("filter_content_by_age_text")
        
        self.preSchoolLabel.text = LanguageHelper.localizedString("pre_school_text")
        self.preSchoolDesc.text = LanguageHelper.localizedString("suitable_for_children_aged_06_years_old")
        
        self.childrenLabel.text = LanguageHelper.localizedString("children_text")
        self.childrenDesc.text = LanguageHelper.localizedString("suitable_for_children_aged_712_years_old")
        
        self.teenagerLabel.text = LanguageHelper.localizedString("teenagers_text")
        self.teenagerDesc.text = LanguageHelper.localizedString("suitable_for_viewers_aged_1317_years_old")
        
        self.lblAllowChannels.text = LanguageHelper.localizedString("allowed_channels_text")
        self.lblAllowChannelsSubTitle.text = LanguageHelper.localizedString("allowed_channels_hint_text")
        
        self.btnAllowChannels.setTitle(LanguageHelper.localizedString("edit_text"), for: .normal)
        
    }

    
    //MARK: - CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("channelList.value.count " + channelList.value.count.description)
        return self.channelList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let _channel: Channel = self.channelList.value[indexPath.item]
        let cell = self.cv.dequeueReusableCell(withReuseIdentifier: "ParentalControlViewControllerCell", for: indexPath) as! ParentalControlViewControllerCell
        cell.configure(with: _channel)
        let active = !disallowedList.contains(where: { $0 == _channel })
        print("channel \(_channel.id.description) is " + (active ? "Active" : "Inactive"))
        cell.isActive = active
//        print("Cell frame: \(_channel.id)")
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell : ParentalControlViewControllerCell = collectionView.cellForItem(at: indexPath) as! ParentalControlViewControllerCell
        
        if indexPath.item < self.channelList.value.count {
            let channel = self.channelList.value[indexPath.item]
            let updateAction: AllowedChannelAction = cell.isActive ? .Remove : .Add
            ParentalControlHelper.shared.updateAllowedChannel(action: updateAction,
                                                              channelID: channel.id.description,
                                                              success: { [weak self, weak cell] in
                                                                print("\(updateAction.rawValue) succesfully pc : " + channel.id.description)
                                                                if updateAction == .Add {
                                                                    // Add to channel
                                                                    self?.addAllowedChannel(channel: channel)
                                                                } else {
                                                                    // Remmove from channel
                                                                    self?.removeAllowedChannel(channel: channel)
                                                                }
                                                                if let _cell = cell {
                                                                    _cell.isActive = !_cell.isActive
                                                                }
                                                                self?.cv.reloadData()
                }, failure: nil)
        }
    }
    
    deinit {
//        print("deallocating" + String(describing: type(of: self)))
    }
}
