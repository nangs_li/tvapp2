//
//  ChannelCell.swift
//  OONA
//
//  Created by Vick on 14/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SDWebImage

class ParentalControlViewControllerHeader: UICollectionReusableView {
    override func awakeFromNib() {
        
    }
    
    @IBOutlet weak var sectionHeaderlabel: UILabel!
    
    func setText(text: String){
        self.sectionHeaderlabel.text = text
    }
    
}
