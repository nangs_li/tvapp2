//
//  OONASettings.swift
//  OONA
//
//  Created by nicholas on 8/20/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxCocoa

class OONASettings: NSObject {
    static let manager: OONASettings = {
        let manager = OONASettings()
        manager.loadSettings()
        return manager }()
    
    var displayableSettings: [OONASettingsAction : Any] = [OONASettingsAction : Any]()
    
    var settingsObject: SettingsObject = SettingsObject()
    
    let settingNeedsUpdate: PublishRelay<Bool> = PublishRelay<Bool>()
    
    var settings: [[OONASettingsAction]] {
        return [
            // App settings
            [.SettingsStreamOnlyWiFi,
             .SettingsLanguage],
            // Parental control
            [.SettingsParentalControl],
            // Quality
            [.SettingsStreamingQuality],
            // Help
            [.SettingsTutorial],
            // About
            [.SettingsAppVersions,
             .SettingsPrivacyPolicy,
             .SettingsGeneralTermsAndConditions,
             .SettingsTCoinsTermsAndConditions,
             .SettingsDisclaimer]
        ]
    }
    
    func getSettingsDisclaimerText() -> String {
        return "OONA TV is not in anyway associated with any sites. We do not host any videos on OONA TV itself. OONA TV is absolutely legal and contains only links to other third party websites like Youtube, Google, Dailymotion,… and many more which actually host videos. OONA TV is not responsible for the compliance, copyright, legality, decency, or any other aspect of the content of other linked sites. If you have any legal issues please contact the appropriate media file owners or linked hosting websites. OONA TV only and only provides links to third party video hosting sites which videos are uploaded by third party users. OONA TV in no way affiliated with them nor intend to do that."
    }
    
    // MARK: - UserDefaults actions
    func loadSettings() {
        /// Retrieve data from disk
        if let storedSettings = UserDefaults.standard.object(forKey: "userSetting.json") as? Data {
            let decoder = JSONDecoder()
            if let loadedSettings = try? decoder.decode(SettingsObject.self, from: storedSettings) {
                print("Settings retrieved")
                print(loadedSettings)
                self.settingsObject = loadedSettings
                
            } else {
                self.syncSettings()
                print("Creating new Settings")
            }
        } else {
            self.syncSettings()
            print("Creating new Settings")
        }
    }
    
    func syncSettings() {
        /// Save data to disk
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self.settingsObject) {
            UserDefaults.standard.set(encoded, forKey: "userSetting.json")
            print("Settings stored")
        }
    }
    
    // MARK: - Displayable settings
    func getDisplayaSetting<T>(with settingsType: OONASettingsAction) -> T {
        guard let _setting = self.displayableSettings[settingsType] else {
            let setting: SettingsModule = self.createDisplayableSetting(with: settingsType)
            self.displayableSettings[settingsType] = setting
            return setting as! T
        }
        return _setting as! T
    }
    
    func createDisplayableSetting<T>(with settingsType: OONASettingsAction) -> T {
        //        var setting: SettingsModule
        
        switch settingsType {
            
        // App settings
        case .SettingsStreamOnlyWiFi:
            let setting = SettingsSwitch(with: .cellSwitch)
            setting.title = LanguageHelper.localizedString("stream_with_wifi_only")
            setting.subTitle = LanguageHelper.localizedString("warn_me_when_wifi_is_not_available")
            setting.accessoryAction.accept(self.settingsObject.streamingOnlyWifi)
            setting.settingsIdentifier = .SettingsStreamOnlyWiFi
//            setting.lastItemInSection = true
            setting.accessoryAction.skip(1).subscribe(onNext: { [weak self] (wifiOnly) in
                self?.settingsObject.streamingOnlyWifi = wifiOnly
                self?.syncSettings()
                self?.settingNeedsUpdate.accept(true)
            }).disposed(by: setting.disposeBag)
            return setting as! T
            
        case .SettingsLanguage:
            let setting = SettingsPicker(with: .cellPicker)
            setting.title = LanguageHelper.localizedString("language")
            setting.accessoryAction.accept(LanguageHelper.shared.appLanguageDetail())
            setting.settingsIdentifier = .SettingsLanguage
            setting.lastItemInSection = true
            setting.accessoryAction.skip(1).subscribe(onNext: { [weak self, unowned helper = LanguageHelper.shared] (language) in
//                self?.settingsObject.streamingOnlyWifi = wifiOnly
//                self?.syncSettings()
                helper.changeLanguage(lang: Language(rawValue: language) ?? .english)
                self?.settingNeedsUpdate.accept(true)
            }).disposed(by: setting.disposeBag)
            return setting as! T
            
        // Parental Control
        case .SettingsParentalControl:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("parental_control")
            setting.settingsIdentifier = .SettingsParentalControl
            setting.lastItemInSection = true
            return setting as! T
            
        // Quality
        case .SettingsStreamingQuality:
            let setting = SettingsPicker(with: .cellPicker)
            setting.title = LanguageHelper.localizedString("streaming_quality")
            setting.subTitle = LanguageHelper.localizedString("if_not_available_lower_quality_will_be_selected")
            setting.accessoryAction.accept(self.settingsObject.streamingQuality.rawValue)
            setting.settingsIdentifier = .SettingsStreamingQuality
            setting.accessoryAction.skip(1).subscribe(onNext: { [weak self] (quality) in
                self?.settingsObject.streamingQuality = SettingsQuality(rawValue: quality) ?? SettingsQuality.QualityAlwaysAsk
                self?.syncSettings()
                self?.settingNeedsUpdate.accept(true)
            }).disposed(by: setting.disposeBag)
            setting.lastItemInSection = true
            return setting as! T
            
        // Help
        case .SettingsTutorial:
            let setting = SettingsSelection(with: .cellArrow)
            setting.title = LanguageHelper.localizedString("tutorials_text")
            setting.titleColor = .red
            setting.subTitle = LanguageHelper.localizedString("how_to_use_the_app_faq")
            setting.settingsIdentifier = .SettingsTutorial
            setting.lastItemInSection = true
            return setting as! T
            
        // About
        case .SettingsAppVersions:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("app_versions")
            setting.settingsIdentifier = .SettingsAppVersions
            return setting as! T
            
        case .SettingsPrivacyPolicy:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("privacy_policy")
            setting.settingsIdentifier = .SettingsPrivacyPolicy
            return setting as! T
            
        case .SettingsGeneralTermsAndConditions:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("general_terms_and_conditions")
            setting.settingsIdentifier = .SettingsGeneralTermsAndConditions
            return setting as! T
            
        case .SettingsTCoinsTermsAndConditions:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("tcoins_terms_and_conditions")
            setting.settingsIdentifier = .SettingsTCoinsTermsAndConditions
            return setting as! T
            
        case .SettingsDisclaimer:
            let setting = SettingsSelection(with: .cellArrowOnlyTitle)
            setting.title = LanguageHelper.localizedString("disclaimer")
            setting.settingsIdentifier = .SettingsDisclaimer
            setting.lastItemInSection = true
            return setting as! T
            
        default:
            let setting = SettingsSelection()
            return setting as! T
        }
        
        //        return setting
    }

}
