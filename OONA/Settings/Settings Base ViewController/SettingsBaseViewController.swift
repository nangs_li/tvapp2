//
//  SettingsBaseViewController.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsBaseViewController: BaseViewController {
    private var topThinBar: UIImageView?
    
    typealias SettingsPageCompletionBlock = (() ->())
    var completionBlock: SettingsPageCompletionBlock?
    
    var showTopBar: Bool = false { didSet { self.updateTopBar() } }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.navigationController != nil {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        self.configNavigationItems()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func configNavigationItems() {
    }
    
    private func updateTopBar() {
//        let nvc = self.navigationController as? SettingsNavigationViewController
//        nvc?.showTopBar = self.showTopBar
        if let loadedView = self.view, self.topThinBar == nil {
            var topBar: UIImageView

            topBar = UIImageView(image: UIImage(named: "baronly"))
            topBar.contentMode = .scaleAspectFill

            loadedView.addSubview(topBar)
            topBar.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.leading.trailing.equalTo(self.view.safeAreaLayoutGuide)
                make.height.equalTo(3)
            }
            self.topThinBar = topBar
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
            self.topThinBar?.alpha = self.showTopBar ? 1 : 0
        }, completion: nil)
        
    }
    
    func leaveAction() {
        print(#function)
        if let _navigationViewController = self.navigationController {
            if _navigationViewController.viewControllers.count > 1 {
                self.navigationController?.popViewController(animated: true)
            } else if _navigationViewController.viewControllers.count == 1 {
                _navigationViewController.dismiss(animated: true) {
                    self.completionBlock?()
                }
            }
        } else {
            self.dismiss(animated: true) {
                self.completionBlock?()
            }
        }
    }
    
}
