//
//  SettingsNavigationViewController.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let backImage: UIImage? = UIImage(named: "back-arrow")
        self.navigationBar.backIndicatorImage = backImage
        self.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationBar.tintColor = .white
        self.navigationItem.backBarButtonItem?.title = ""
        if let font = UIFont(name: "Montserrat-SemiBold", size: 14) {
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font]
        }
        // Remove transparency and shadow at the bottom of nativationBar
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
    }
    
}
