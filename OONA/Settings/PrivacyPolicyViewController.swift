//
//  PrivacyPolicyViewController.swift
//  OONA
//
//  Created by nicholas on 8/23/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: SettingsBaseViewController {
    lazy var privacyPolicyWebPage: OONAWKWebViewController = {
        let webPage = OONAWKWebViewController(nibName: "OONAWKWebViewController", bundle: nil)
        webPage.allowToZoom = false
        webPage.allowSelection = false
        return webPage
    }()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var privacyPolicyView: UIView!
    
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    var privacyURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
//        self.showTopBar = true
        
//        self.configNavigationItems()
        self.loadChildView()
        if let _url = privacyURL {
            print("setting webview url",_url)
            self.privacyPolicyWebPage.urlRequest.accept(URLRequest(url: _url))
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTopBar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showTopBar = false
    }
    
    private func loadChildView() {
        self.privacyPolicyWebPage.webView.isOpaque = false
        self.privacyPolicyWebPage.webView.backgroundColor = .black
        self.privacyPolicyWebPage.progressBarColor = .white
        
        
        self.privacyPolicyWebPage.willMove(toParent: self)
        let privacyView: UIView = self.privacyPolicyWebPage.view
        self.privacyPolicyView.addSubview(privacyView)
        
        privacyView.snp.makeConstraints { (make) in
//            make.top.trailing.bottom.leading.equalTo(self.privacyPolicyView)
            make.top.trailing.bottom.leading.equalTo(privacyView.superview!.safeAreaLayoutGuide)
        }
        self.privacyPolicyWebPage.didMove(toParent: self)
    }

    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.leaveAction()
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}
