//
//  AppVersionsViewController.swift
//  OONA
//
//  Created by nicholas on 8/20/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class AppVersionsViewController: SettingsBaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var copyRightLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = LanguageHelper.localizedString("app_versions")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
        
//        self.configNavigationItems()
        var versionString = LanguageHelper.localizedString("version")
        if let _version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
        versionString.append(contentsOf: " \(_version)")
        }
        self.versionLabel.text = versionString
        self.copyRightLabel.text = "\(LanguageHelper.localizedString("copyright_oona_limited"))\n\(LanguageHelper.localizedString("all_rights_reserved"))"
    }
    
    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.leaveAction()
    }
}
