//
//  TutorialsTableViewCell.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class TutorialsTableViewCell: UITableViewCell {
    @IBOutlet weak var tutorialTitleLabel: UILabel!
    @IBOutlet weak var tutorialDescriptionLabel: UILabel!
    var tutorialObject: TutorialObject? { didSet {} }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.tintColor = .white
        self.selectionStyle = .none
    }
    
    func configureTutorial(tutorial: TutorialObject) {
        
        self.tutorialTitleLabel?.text = tutorial.tutorialTitle
        self.tutorialDescriptionLabel?.text = tutorial.tutorialSubtitle
        
        self.tutorialObject = tutorial
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
//        self.selectionIndicator.isHidden = !selected
//        self.accessoryType = selected ? UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
//        print(#function + "\(selected ? "selected" : "deselected")")
    }
    
}
