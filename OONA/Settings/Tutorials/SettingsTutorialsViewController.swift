//
//  SettingsTutorialsViewController.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsTutorialsViewController: SettingsBaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet { self.configureTableView() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        self.showTopBar = true
        self.title = LanguageHelper.localizedString("menu_setting")
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
    }
    
    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTopBar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showTopBar = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tutorialList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TutorialsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TutorialsTableViewCell",
                                                                         for: indexPath) as! TutorialsTableViewCell
        let tutorial: TutorialObject = self.tutorialList[indexPath.row]
        cell.configureTutorial(tutorial: tutorial)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tutorial: TutorialObject = self.tutorialList[indexPath.row]
        if let _tutorial = tutorial.tutorial {
            self.playTutorial(rewardUI:false,tutorial: _tutorial)
        }
    }
    
    private var tutorialList: [TutorialObject] = { return [TutorialObject(LanguageHelper.localizedString("control_video"),
                                                                          LanguageHelper.localizedString("fast_forward_rewind_adjust_brightness_and_volume"),
                                                                          .firstTimeBumper),
                                                           TutorialObject(LanguageHelper.localizedString("left_menu"),
                                                                          LanguageHelper.localizedString("games_search_content_tcoins_store"),
                                                                          .leftMenu),
                                                           TutorialObject(LanguageHelper.localizedString("right_menu"),
                                                                          LanguageHelper.localizedString("screen_capture_share_chatbot"),
                                                                          .rightMenu),
                                                           TutorialObject(LanguageHelper.localizedString("channel_list"),
                                                                          LanguageHelper.localizedString("how_to_choose_channel"),
                                                                          .upMenu),
                                                           TutorialObject(LanguageHelper.localizedString("earn_tcoins"),
                                                                          LanguageHelper.localizedString("how_to_get_tcoins"),
                                                                          .earnTcoin),
                                                           TutorialObject(LanguageHelper.localizedString("tcoins_menu"),
                                                                          LanguageHelper.localizedString("view_tcoins_details"),
                                                                          .downMenu),
                                                           TutorialObject(LanguageHelper.localizedString("bidding"),
                                                                          LanguageHelper.localizedString("how_to_use_tcoins_to_win_prizes"),
                                                                          .tcoinBid)
        ] }()
    
    private func configureTableView() {
        self.tableView.register(UINib(nibName: "TutorialsTableViewCell", bundle: nil), forCellReuseIdentifier: "TutorialsTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 45
    }
    
    @IBAction func closePage(_ sender: Any) {
        self.leaveAction()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.leaveAction()
    }

}

class TutorialObject: NSObject {
    
    var tutorialTitle: String?
    var tutorialSubtitle: String?
//    var tutorialPath: String?
    var tutorial: Tutorial?
    
    override init() {
        super.init()
    }
    
    convenience init(_ title: String?,
                     _ detail: String?,
                     _ tutorial: Tutorial) {
        self.init()
        self.tutorialTitle = title
        self.tutorialSubtitle = detail
        self.tutorial = tutorial
    }
}
