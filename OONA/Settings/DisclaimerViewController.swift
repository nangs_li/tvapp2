//
//  DisclaimerViewController.swift
//  OONA
//
//  Created by nicholas on 8/23/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class DisclaimerViewController: SettingsBaseViewController {
    lazy var disclaimerPage: OONATextDisplayViewController = {
        let textPage = OONATextDisplayViewController(nibName: "OONATextDisplayViewController", bundle: nil)
        textPage.selectable = false
        return textPage
    }()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var disclaimerView: UIView!
    
    
    @IBOutlet weak var navigationBarLeading: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTrailing: NSLayoutConstraint!
    
    typealias CustomBackClosure = ((UIViewController?) -> (Void))
    var customBackAction: CustomBackClosure?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = self.title
        self.titleLabel.text = self.title
//        self.showTopBar = true
        
//        self.configNavigationItems()
        self.loadChildView()
        // Before accessing any force unwrapped property, you must ensure the access actions is peform after viewDidLoad or loadView
        if self.disclaimerPage.textToDisplay.value == nil {
            self.disclaimerPage.textToDisplay.accept(OONASettings.manager.getSettingsDisclaimerText())
        }
        // Set Paragraph Style
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 15
        self.disclaimerPage.textStyle = paragraphStyle
        
        // Set Font
        self.disclaimerPage.textFont = UIFont(name: "Montserrat-Regular", size:  10.0)
        
        // Set Text View Insets
        self.disclaimerPage.textContainerInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTopBar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showTopBar = false
    }
    
    private func loadChildView() {
        self.disclaimerPage.textColor = .white
        self.disclaimerPage.textBackgroundColor = .black
        
        self.disclaimerPage.willMove(toParent: self)
        let disclaimerView: UIView = self.disclaimerPage.view
        self.disclaimerView.addSubview(disclaimerView)
        disclaimerPage.view.snp.makeConstraints { (make) in
//            make.top.trailing.bottom.leading.equalToSuperview()
            make.top.trailing.bottom.leading.equalTo(disclaimerView.superview!.safeAreaLayoutGuide)
        }
        self.disclaimerPage.didMove(toParent: self)
    }
    
    override func configNavigationItems() {
        if !UIDevice.current.hasNotch {
            self.navigationBarLeading.constant = 30
            self.navigationBarTrailing.constant = 30
        }
    }

    
    @IBAction func backButtonAction(_ sender: Any) {
        if self.customBackAction != nil {
            self.customBackAction?(self)
        } else {
            self.leaveAction()
        }
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}
