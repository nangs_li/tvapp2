//
//  SettingsObject.swift
//  OONA
//
//  Created by nicholas on 8/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

enum SettingsQuality: String, Codable {
    case QualityAuto = "Auto"
    case QualityAlwaysAsk = "Always Ask"
    case Quality1080p = "1080p"
    case Quality720p = "720p"
    case Quality480p = "480p"
    case Quality360p = "360p"
    case Quality180p = "180p"
}

struct SettingsObject: Codable {
    
    var streamingOnlyWifi: Bool
    var streamingQuality: SettingsQuality = .QualityAuto
    enum CodingKeys: String, CodingKey {
        case streamingOnlyWifi
        case streamingQuality
        case parentalControllEnabled
    }
    
    init() {
        self.streamingOnlyWifi = false
        self.streamingQuality = .QualityAuto
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let value = try decoder.container(keyedBy: CodingKeys.self)
        self.streamingOnlyWifi = try value.decode(Bool.self, forKey: .streamingOnlyWifi)
        self.streamingQuality = try value.decode(SettingsQuality.self, forKey: .streamingQuality)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.streamingOnlyWifi, forKey: .streamingOnlyWifi)
        try container.encode(self.streamingQuality, forKey: .streamingQuality)
    }
}
