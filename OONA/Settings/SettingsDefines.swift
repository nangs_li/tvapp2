//
//  SettingsCellProtocol.swift
//  OONA
//
//  Created by nicholas on 8/20/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
enum OONASettingSections: Int {
    case AppSetting = 0
    case ParentalControl
    case Quality
    //    case Notifications
    case Help
    case About
}

enum OONASettingsCellType {
    case cellDefault
    case cellSwitch
    case cellArrow
    case cellArrowOnlyTitle
    case cellPicker
}

enum OONASettingsAction: String {
    case SettingsDefault = ""
    case SettingsStreamOnlyWiFi = "SettingsStreamOnlyWiFi"
    case SettingsLanguage = "SettingsLanguage"
    case SettingsParentalControl = "SettingsParentalControl"
    case SettingsStreamingQuality = "SettingsStreamingQuality"
    case SettingsTutorial = "SettingsTutorial"
    case SettingsAppVersions = "SettingsAppVersions"
    case SettingsPrivacyPolicy = "SettingsPrivacyPolicy"
    case SettingsGeneralTermsAndConditions = "SettingsGeneralTermsAndConditions"
    case SettingsTCoinsTermsAndConditions = "SettingsTCoinsTermsAndConditions"
    case SettingsDisclaimer = "SettingsDisclaimer"
//    case Settings = ""
//    case Settings = ""
//    case Settings = ""
//    case Settings = ""
//    case Settings = ""
}

protocol OONASettingsCellProtocol {
    func configureCell(with cellType: OONASettingsCellType)
}


