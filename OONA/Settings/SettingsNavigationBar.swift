//
//  SettingsNavigationBar.swift
//  OONA
//
//  Created by nicholas on 8/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SettingsNavigationBar: UIView {
    let kCONTENT_XIB_NAME = "SettingsNavigationBar"
    
    weak var navigationController: UINavigationController?
    
    @IBOutlet var contentView: UIView!
    
    var title: String? { didSet {
        self.titleLabel.text = title
        } }
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadView()
    }
    
    private func loadView() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.top.trailing.bottom.left.equalToSuperview()
        }
    }
    
    @IBAction func leftButtonAction(_ sender: Any) {
        guard let _navigationViewController = self.navigationController else { return }
        if _navigationViewController.viewControllers.count > 1 {
            self.navigationController?.popViewController(animated: true)
        } else if _navigationViewController.viewControllers.count == 1 {
            _navigationViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
