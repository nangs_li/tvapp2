
//  ParentalControlViewController
//  OONA
//
//  Created by Vick on 9/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxSwift
import RxCocoa

class ParentalControlAllowedChannelsViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource,
UITableViewDelegate, UITableViewDataSource {
    
    var upperViewHeight = 58
    var channelList: BehaviorRelay<[[Channel]]> = BehaviorRelay<[[Channel]]>(value: [[Channel]]())
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var cvViewHeight: NSLayoutConstraint!
    
    var alphabets = [String]()
    
    override func viewDidLoad () {
        super.viewDidLoad()
        setupCollectionView()
        setupTableView()
        getSuitableChannel()
        
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        
        self.tv.backgroundColor = UIColor.black

        let str: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        for i in str {
            alphabets.append(String(i))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func submit(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction override func close(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.dismiss(animated: true)
        
    }
    
    func setupCollectionView(){
        self.cv.register(UINib(nibName: "ParentalControlViewControllerCell", bundle: nil), forCellWithReuseIdentifier: "ParentalControlViewControllerCell")
        
        self.cv.register(
            UICollectionReusableView.self,
            forSupplementaryViewOfKind:
            UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: "ParentalControlViewControllerHeader")
        
        self.cv.delegate = self
        self.cv.dataSource = self
        self.channelList.subscribe(onNext: { [weak self] ( _ ) in
            //self?.updateScrollViewHeight()
            //self?.updateCVViewHeight()
            
            self?.cv.reloadData()
        }).disposed(by: self.disposeBag)
        
        
    }
    
    func setupTableView(){
        
        self.tv.register(
            UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        self.tv.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tv.delegate = self
        self.tv.dataSource = self
        
    }
    
    func updateCVViewHeight(){
        print("updateCVViewHeight")
        let numberOfLines = channelList.value.count / 5
        self.cvViewHeight.constant = CGFloat(numberOfLines) * (APPSetting.episodeChannelObjectHeight + 10)
        print("updateCVViewHeight" + self.cvViewHeight.constant.description)
    }
    
    func SplitChannelByFirstCharater(channels : [Channel]) -> [[Channel]]{
        
        let _channels = channels.sorted(by: { $0.name.prefix(1) > $1.name.prefix(1) })
        
        //var currentIndex = 0
        var result = [[Channel]]() //two - dimentional array
        
        for alphabet in alphabets{
            var currentChannels = [Channel]()
            for channel in _channels
            {
                if(alphabet == channel.name.prefix(1).description){
                    currentChannels.append(channel)
                }
            }
            
            let _currentChannels = currentChannels.sorted(by: { $0.name.prefix(2) > $1.name.prefix(2) }) //sort by alphateical order..
            
            result.append(_currentChannels)
            
            //currentIndex += 1
        }
        
        return result
    }
    
    func getSuitableChannel(){
        
        /*
         ParentalControlHelper.shared.getPlaylist(filterType: .suitable, success: { [weak self] (channels) in
         if let _listChannels = self?.SplitChannelByFirstCharater(channels: channels){
         self?.channelList.accept(_listChannels)
         }else{
         print("fail to split channel list!")
         }
         //print("\(String(describing: type(of: self)))channel list: ",channels)
         }, failure: { code , msg in
         if let _msg = msg{
         self.showAlert(title: "", message : _msg)
         }
         
         })*/
        
        ChannelHelper.shared.getChannelList(open: false, categoryId: nil, currentChannelId: nil, filterType: .none, offset: nil, sortBy: nil, success: {[weak self] (channels) in
            if let _listChannels = self?.SplitChannelByFirstCharater(channels: channels){
                self?.channelList.accept(_listChannels)
            }else{
                print("fail to split channel list!")
            }
            //print("\(String(describing: type(of: self)))channel list: ",channels)
            
            }, failure: { code, msg in
                
                if let _msg = msg as? String{
                    self.showAlert(title: "", message : _msg)
                }
                
        })
        /*
         ChannelHelper.shared.getChannelList(open: false, categoryId: nil, currentChannelId: nil, filterType: .none, offset: nil, sortBy: nil, success: {[weak self] (channels) in
         if let _listChannels = self?.SplitChannelByFirstCharater(channels: channels){
         self?.channelList.accept(_listChannels)
         }else{
         print("fail to split channel list!")
         }
         //print("\(String(describing: type(of: self)))channel list: ",channels)
         
         }, failure: { code, msg in
         
         if let _msg = msg as? String{
         self.showAlert(title: "", message : _msg)
         }
         
         })*/
    }
    
}
