//
//  SettingsModule.swift
//  OONA
//
//  Created by nicholas on 8/20/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SettingsModule: NSObject {
    var title: String?
    var subTitle: String?
    var settingsCellType: OONASettingsCellType = .cellDefault
    var settingsIdentifier: OONASettingsAction = .SettingsDefault
    var lastItemInSection: Bool = false
    let disposeBag: DisposeBag = DisposeBag()
    
    override init() {
        super.init()
    }
    
}

class SettingsSwitch: SettingsModule {
    var accessoryAction: BehaviorRelay<Bool>
    override init() {
        self.accessoryAction = BehaviorRelay<Bool>(value: false)
        super.init()
        super.settingsCellType = .cellSwitch
    }
    convenience init(with type: OONASettingsCellType) {
        self.init()
        self.settingsCellType = type
    }
}

class SettingsSelection: SettingsModule {
    var titleColor: UIColor?
    override init() {
        super.init()
        super.settingsCellType = .cellArrow
    }
    
    convenience init(with type: OONASettingsCellType) {
        self.init()
        self.settingsCellType = type
    }
}

class SettingsPicker: SettingsModule {
    var accessoryAction: BehaviorRelay<String>
    var accessoryDescriptionText: String?
    
    override init() {
        self.accessoryAction = BehaviorRelay<String>(value: "")
        super.init()
        super.settingsCellType = .cellPicker
    }
    convenience init(with type: OONASettingsCellType) {
        self.init()
        self.settingsCellType = type
    }
}
