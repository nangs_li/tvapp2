//
//  SettingsViewController+TableView.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

extension SettingsViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return OONASettings.manager.settings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRow: Int = 0
        if section < OONASettings.manager.settings.count {
            numberOfRow = OONASettings.manager.settings[section].count
        }
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var heightForHeader: CGFloat = 45
        if section == OONASettingSections.ParentalControl.rawValue { heightForHeader = .leastNormalMagnitude }
        return heightForHeader
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 { return 40 }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var sectionTitle: String?
        
        switch section {
        case 0:// App settings
            sectionTitle = LanguageHelper.localizedString("app_settings")
        case 1: // Parental control
            sectionTitle = nil
        case 2: // Quality
            sectionTitle = LanguageHelper.localizedString("quality")
        case 3: // Help
            sectionTitle = LanguageHelper.localizedString("help")
        case 4: // About
            sectionTitle = LanguageHelper.localizedString("about")
        default: break
        }
        
        return self.createHeaderView(with: sectionTitle)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = OONASettings.manager.settings[indexPath.section][indexPath.row]
        let settingModule: SettingsModule = OONASettings.manager.getDisplayaSetting(with: type)
        
        switch settingModule.settingsCellType {
        case .cellArrow:
            let cell: SettingsArrowCell = self.dequeueReusableCell(tableView, in: indexPath, for: .cellArrow)
            let _setting = settingModule as! SettingsSelection
            cell.configure(with: _setting)
            return cell
            
        case .cellArrowOnlyTitle:
            let cell: SettingsArrowCell = self.dequeueReusableCell(tableView, in: indexPath, for: .cellArrowOnlyTitle)
            let _setting: SettingsSelection = settingModule as! SettingsSelection
            cell.configure(with: _setting)
            return cell
            
        case .cellSwitch:
            let cell: SettingsSwitchCell = self.dequeueReusableCell(tableView, in: indexPath, for: .cellSwitch)
            let _setting: SettingsSwitch = settingModule as! SettingsSwitch
            cell.configure(with: _setting)
            return cell
            
        case .cellPicker:
            let cell: SettingsPickerCell = self.dequeueReusableCell(tableView, in: indexPath, for: .cellPicker)
            let _setting: SettingsPicker = settingModule as! SettingsPicker
            cell.configure(with: _setting)
            return cell
            
        default:
            let cell: SettingsCommonCell = self.dequeueReusableCell(tableView, in: indexPath, for: .cellDefault)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isMember(of: UITableViewCell.self) {
            cell.backgroundColor = .black
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section >= 0, indexPath.row >= 0, indexPath.section < OONASettings.manager.settings.count,
            indexPath.row < OONASettings.manager.settings[indexPath.section].count else { return }
        
        let type = OONASettings.manager.settings[indexPath.section][indexPath.row]
        let settingModule: SettingsModule = OONASettings.manager.getDisplayaSetting(with: type)
        
        self.showPage(for: settingModule.settingsIdentifier)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func dequeueReusableCell<T>(_ tableView: UITableView,
                                        in indexPath: IndexPath,
                                        for type: OONASettingsCellType) -> T {
        switch type {
        case .cellArrow: fallthrough
        case .cellArrowOnlyTitle:
            return tableView.dequeueReusableCell(withIdentifier: "SettingsArrowCell", for: indexPath) as! T
            
        case .cellSwitch:
            return tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchCell", for: indexPath) as! T
            
        case .cellPicker:
            return tableView.dequeueReusableCell(withIdentifier: "SettingsPickerCell", for: indexPath) as! T
            
        default:
            return SettingsCommonCell(style: .subtitle, reuseIdentifier: "SettingsCommonCell") as! T
        }
    }
    
    private func createHeaderView(with description: String?) -> UIView? {
        guard description != nil else { return nil }
        let headerView: UIView = UIView()
        let label: UILabel = UILabel()
        label.textColor = .white
        label.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        label.text = description
        headerView.addSubview(label)
        label.snp.makeConstraints { (make) in
            if UIDevice.current.hasNotch {
                make.top.trailing.bottom.leading.equalToSuperview()
            } else {
                make.top.bottom.equalToSuperview()
                make.leading.trailing.equalToSuperview().offset(20)
            }
        }
        return headerView
    }
    
}
