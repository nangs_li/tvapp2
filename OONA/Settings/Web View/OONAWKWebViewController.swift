//
//  OONAWKWebViewController.swift
//  OONA
//
//  Created by nicholas on 8/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import WebKit
import SnapKit
import RxSwift
import RxCocoa

class OONAWKWebViewController: BaseViewController, WKScriptMessageHandler, WKNavigationDelegate {
    var urlRequest: BehaviorRelay<URLRequest?> = BehaviorRelay<URLRequest?>(value:nil)
    
    var allowToZoom: Bool = true
    var allowSelection: Bool = true
    var showLoadingProgress: Bool = false
    
    var progressBarColor: UIColor?
    var webView: WKWebView { get { return self._wkWebView } }

    private lazy var _wkWebView: WKWebView = {
        let webView = WKWebView(frame: CGRect.zero, configuration: self.webConfig)
        webView.navigationDelegate = self
        return webView }()
    
    var loadingObservation: NSKeyValueObservation?
    var loadingProgressObservation: NSKeyValueObservation?
    var pinchGestureRecognizerObservation: NSKeyValueObservation?
    
    @IBOutlet weak var loadingProgress: UIProgressView!
    private lazy var webConfig: WKWebViewConfiguration =
        {
            let config = WKWebViewConfiguration()
            let userContentController = WKUserContentController()
            config.preferences.javaScriptEnabled = true
            config.userContentController = userContentController
            return config
    }()
    
    override func loadView() {
        super.loadView()
        print(#function)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
        // Do any additional setup after loading the view.
        self.navigationItem.title = self.title
        
        self.urlRequest.skip(1).subscribe(onNext: { [weak self] (urlRequest) in
            guard let _request = urlRequest else { return }
            self?.webView.load(_request)
        }).disposed(by: self.disposeBag)
        
        self.loadingProgress.progress = 0.0
        if let _color = self.progressBarColor {
            self.loadingProgress.progressTintColor = _color
        }
        self.showLoadingProgress(self.showLoadingProgress)
        
        self.clearWebSiteDataStore()
        
        self.configWebView()
        
        self.view.insertSubview(self.webView, belowSubview: self.loadingProgress)
        
        self._wkWebView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
//            make.top.equalTo(navigationBarView.snp.bottom)
        }
        self.addObservers()
//        self._wkWebView.isUserInteractionEnabled = false
        if let _request: URLRequest = self.urlRequest.value {
            self.webView.load(_request)
        }
    }
    
    private func showLoadingProgress(_ showProgress: Bool) {
        print("Progress is hidden: " + (!(showProgress && self.showLoadingProgress) ? "YES" : "NO"))
        self.loadingProgress.isHidden = !(showProgress && self.showLoadingProgress)
    }
    
    private func updateProgress(progress: Float) {
        self.loadingProgress.setProgress(progress, animated: true)
    }
    
    private func configWebView() {
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.allowsLinkPreview = true
    }
    
    private func updateWebViewZoomable() {
        self.webView.scrollView.pinchGestureRecognizer?.isEnabled = self.allowToZoom
        print(self.webView.scrollView.pinchGestureRecognizer as Any)
    }
    
    private func addObservers() {
        self.removeObsercers()
        self.loadingObservation = self.webView.observe(\.isLoading, options: [.old, .new], changeHandler: { [weak self] (wkWebView, changes) in
            self?.showLoadingProgress(wkWebView.isLoading)
        })
        
        self.loadingProgressObservation = self.webView.observe(\.estimatedProgress, options: [.old, .new], changeHandler: { [weak self] (wkWebView, changes) in
            self?.updateProgress(progress: Float(wkWebView.estimatedProgress))
            if wkWebView.estimatedProgress >= 1.0 {
                self?.updateProgress(progress: 0)
            }
        })
        
        self.pinchGestureRecognizerObservation = self.webView.scrollView.observe(\.pinchGestureRecognizer, options: [.initial], changeHandler: { [weak self] (wkWebView, changes) in
            self?.updateWebViewZoomable()
        })
    }
    
    private func removeObsercers() {
        self.loadingObservation?.invalidate()
        self.loadingProgressObservation?.invalidate()
        self.pinchGestureRecognizerObservation?.invalidate()
    }
    
    private func clearWebSiteDataStore() {
        let webSiteDataTypes: Set = Set([WKWebsiteDataTypeDiskCache,
              //WKWebsiteDataTypeOfflineWebApplicationCache,
            WKWebsiteDataTypeMemoryCache,
            //WKWebsiteDataTypeLocalStorage,
            WKWebsiteDataTypeCookies,
            //WKWebsiteDataTypeSessionStorage,
            WKWebsiteDataTypeDiskCache,
            //WKWebsiteDataTypeIndexedDBDatabases,
            //WKWebsiteDataTypeWebSQLDatabases
            ])
        
        let dateSince: Date = Date(timeIntervalSince1970: 0)
        WKWebsiteDataStore.default().removeData(ofTypes: webSiteDataTypes,
                                                modifiedSince: dateSince) {
            print("WKWebsiteDataStore cleaned up")
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print(#function)
    }
    deinit {
//        print("Deallocating \(self)")
        self.removeObsercers()
    }
}

// MARK: - WKNavigationDelegates
extension OONAWKWebViewController {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
        if !self.allowSelection {
            let javaScriptStyle = "var css = '*{-webkit-touch-callout:none;-webkit-user-select:none}'; var head = document.head || document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css'; style.appendChild(document.createTextNode(css)); head.appendChild(style);"
            webView.evaluateJavaScript(javaScriptStyle, completionHandler: nil)
        }
        
        if !self.allowToZoom {
            let javaScriptStyle =
            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'user-scalable=no, initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
//            let javascriptStyle =
//            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'user-scalable=no, initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, width=device-width, height=device-height, target-densitydpi=device-dpi'); document.getElementsByTagName('head')[0].appendChild(meta);"
            webView.evaluateJavaScript(javaScriptStyle, completionHandler: nil)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
    }

}
