//
//  GameCell.swift
//  OONA
//
//  Created by Jack on 28/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//


import Foundation
import SDWebImage
import Kingfisher

class GameCell: UICollectionViewCell {
    override func awakeFromNib() {
        
    }
    
    
    @IBOutlet weak var overlayImg: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var underline: UIView!
    
    @IBAction func click() {
        // print("clicked")
    }
    
    func configure(with game: Game) {
        img?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
//        if let url = URL.init(string: game.imageUrl) {
        let encodedURLString: String = game.imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? game.imageUrl
        let url = URL(string: encodedURLString)
//        self.img?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"))
        print("set game image for: \(game.name)")
        self.img.kf.indicatorType = .activity
        self.img.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder-games"),
            options: [
                //                    .processor(RoundCornerImageProcessor(cornerRadius: 10)),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage])
        { (result) in
            switch result {
            case .success(let success):
                print("download game image for: \(game.name), success: \(success)")
                print(success)
            case .failure(let error):
                print("download game image for: \(game.name), failure: \(error)")
                print(error)
            }
        }
//        }
        text.text = game.name
        
    }
    func setInActive(){
        self.underline.isHidden = true
    }
    func setActive(){
        self.underline.isHidden = false
    }
}
