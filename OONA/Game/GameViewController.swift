//
//  GameViewController.swift
//  OONA
//
//  Created by Jack on 28/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import WebKit
import RxSwift
import RxCocoa
import SwiftyJSON
import SwiftyGif
class GameViewController : BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,WKScriptMessageHandler {
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 7
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 3
    
    @IBOutlet weak var gameCollectionView: UICollectionView!
    @IBOutlet weak var gameWebView: WKWebView!
    @IBOutlet weak var playArea: UIView!
    
    @IBOutlet weak var tcoinLabel: UILabel!
    @IBOutlet weak var bannerGifImageView: UIImageView!
    
    var gameAdFirstIntervalTimer = OONATimer()
    var gameAdAfterwardIntervalTimer = OONATimer()
    var gameAdDismissTimer = OONATimer()
    
    
    @IBOutlet weak var gamePageTitleLabel: UILabel!
    @IBOutlet weak var gamePageDescriptionLabel: UILabel!
    
    var gameList: BehaviorRelay<[Game]> = BehaviorRelay<[Game]>(value: [Game]())
    var unlockedGameIdList: [Int] = ( UserDefaults.standard.array(forKey: "unlockedGameIdList") ?? [] )as! [Int]
    var offset : Int = 0
    var webView: WKWebView?
    var hasLoad = false
    var currentGame : Game?
    var tutorialPosterView: UIView?
//    var gameAdTutorialView: UIView?
    @IBOutlet weak var gameAdContainerView: UIView?
    @IBOutlet weak var gameAdContainerConstraintLeading: NSLayoutConstraint?
    var numOfGamePlayed : Int = UserDefaults.standard.integer(forKey: "number_of_game_played")
    
    var gameAdDisposeBag: DisposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("number_of_game_played",UserDefaults.standard.integer(forKey: "number_of_game_played"))
        
        PlayerHelper.shared.startGameAdTimer.subscribe(onNext: { [weak self] (todo) in
//            self?.showGameAd()
            
        }).disposed(by: disposeBag)
        GameHelper.shared.gameAdStart.subscribe(onNext: { [weak self] (todo) in
                self?.gameAdStart()
//            self?.startNewAdInterval(adDuration: 10, firstInterval: 15, AfterwardInterval: 180)
        }).disposed(by: disposeBag)
        
        if GameHelper.shared.userFirstPlay {
            GameHelper.shared.showGameAdTutorial.subscribe(onNext: { [weak self] (show) in
                if show {
                    self?.pauseGame()
                    self?.pauseGameAdTimer()
                    self?.showGameAdTutorial()
                } else {
                    self?.dismissGameAdTutorial()
                }
            }).disposed(by: disposeBag)
        } else {
            self.gameAdContainerView = nil
//            self.gameAdTutorialView = nil
        }
        
        do {
            let gif = try UIImage(gifName: "game-banner-text")
            bannerGifImageView.setGifImage(gif)
//            bannerGifImageView.loopCount = x
//            bannerGifImageView.delegate = self
            
        } catch  {
            print(error)
        }
        PaymentHelper.shared.requestProductInfo()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadViewIfNeeded()
        if(!hasLoad){
            self.gameWebView.scrollView.isScrollEnabled = false
            hasLoad = true
            GameHelper.shared.dismissGameArea.subscribe(onNext: { [weak self] (todo) in
                UIView.animate(withDuration: 0.3,
                               animations: {
                                if (self?.currentGame != nil) {
                                    self?.sendGameEndAnalytics(game: (self?.currentGame!)!)
                                    self?.currentGame = nil
                                }
                                
                                self?.playArea.alpha = 0
                                self?.webView?.removeFromSuperview()
                                self?.requestReward()
                                self?.pauseGameAdTimer()
                                self?.hideTutorialPoster()
                                
//                                let url = URL.init(string: "https://static-ind.oonatv.com/games/NinjaRun/index.html")
//                                self?.play(url: url!)

                })
            }).disposed(by: self.disposeBag)
            
            self.gameList.subscribe(onNext: { [weak self] (_) in
                self?.gameCollectionView.reloadData()
            }).disposed(by: self.disposeBag)
            
            self.configCollectionView()
            self.getGameList()

            //        self.play(game: T##Game)
//            self.loadViewIfNeeded()
//
        
            
            if (!DeviceHelper.shared.isAdFree) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                var launchCount : Int = UserDefaults.standard.integer(forKey: "launch_count")
                print ("launch count",UserDefaults.standard.integer(forKey: "launch_count"))
                if launchCount > 3 {
                    print("launch count > 3")
                    
                    if !UserDefaults.standard.bool(forKey: "no_need_show_adIgnore_chatbot") {
                        PlayerHelper.shared.showChatbot.accept(.ignoreAd)
                    }else{
                        print("no_need_show_adIgnore_chatbot")
                    }
                    
                }
            }
            }
        }
        if (DeviceHelper.shared.isEssentialUiOnly) {
            let url = URL.init(string: "https://static-ind.oonatv.com/games/Ninja-Run/index.html")
            
                    self.play(url: url!)
                    
        }
    }
    
//    private func formateredNumber(with number: Int?) -> String {
//        let numberFormatter = NumberFormatter()
//        numberFormatter.groupingSeparator = ","
//        numberFormatter.numberStyle = .decimal
//        return numberFormatter.string(for: number) ?? "-"
//    }
    
    override func updateUserViews() {
        self.tcoinLabel.text = UtilityHelper.shared.formateredNumber(with: UserHelper.shared.currentUser.profile?.myPointBalance)
    }
    
    func gameAdStart() {
        print ("game ad start")
        self.pauseGame()
    }
    
    func requestReward() {
        if self.totalTcoinEarned > 0 {
        let name = "game-rewards"
        let param = ["points":self.totalTcoinEarned.description,
                     "gameId":currentGame!.id.description]
        self.totalTcoinEarned = 0
            let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)!
            
            OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
        APIResponseCode.checkResponse(withCode: code.description,
                                      withStatus: status,
                                      completion: { (message, isSuccess) in
                                        if(isSuccess){
                                            let balance : Int? = json["points"]["balance"].int
                                            let points : Int? = json["reward"]["points"].int
                                            
                                            print("game sub now")
                                            var needShowTcoinTutorial : Bool = false
                                            
                                            
                                            self.numOfGamePlayed += 1
                                            UserDefaults.standard.set(self.numOfGamePlayed, forKey: "number_of_game_played")
                                            if (self.numOfGamePlayed >= 3){
                                                if (UserDefaults.standard.integer(forKey: "game_tutorial_stage") < 2){
                                                    needShowTcoinTutorial = true
                                                    
                                                }
                                                
                                            }
                                            if (!DeviceHelper.shared.isEssentialUiOnly) {
                                                if needShowTcoinTutorial {
                                                    PlayerHelper.shared.showChatbot.accept(.TcoinTutorialSpend)
                                                }else{
    //                                                PlayerHelper.shared.showChatbot.accept(.Tcoin2000Earned)
                                                    if let needShow = json["reward"]["showSponsorTutorialEarned2000"].int {
                                                        if needShow == 1 {
                                                            PlayerHelper.shared.showChatbot.accept(.Tcoin2000Earned)
                                                        }
                                                    }
                                                }
                                            }
                                        GameHelper.shared.rewardReceived.accept(Reward.init(pt: points ?? 100 , bal: balance!))
                                        } else {
                                            
                                        }
                                        
        })
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    
                }
        }
        }
    }
    
    func getGameList() {
        self.view.startLoading()
        print("Fetching game list")
        GameHelper.shared.getGameList(type: "mobile",
                                      offset:self.offset,
                                      success: { [weak self] (games) in
            
                                        self?.gameList.accept(games)
                                        print("\(String(describing: type(of: self)))game list: ",games)
                                        self?.view.cancelLoading()
                                        print("Fetch game list success")
        }) { (error) in
            self.view.cancelLoading()
            print("Fetch game list failed")
        }
    }
    
    func configCollectionView() {
        self.gameCollectionView.contentInset = UIEdgeInsets(top: lineSpacing,
                                                            left: interitemSpacing,
                                                            bottom: lineSpacing,
                                                            right: interitemSpacing)
        
        self.gameCollectionView.register(UINib(nibName: "GameCell", bundle: nil), forCellWithReuseIdentifier: "GameCell")
        self.gameCollectionView.delegate = self
        self.gameCollectionView.dataSource = self
        
        self.gameList.subscribe(onNext: { [weak self] (_) in
            self?.gameCollectionView.reloadData()
        }).disposed(by: self.disposeBag)
        
    }
    
    func pauseGame() {
        self.webView?.evaluateJavaScript("c2_callFunction('execPause',[1]);", completionHandler: nil)
    }
    
    func pauseGameAdTimer() {
        self.gameAdDismissTimer.suspend()
        self.gameAdAfterwardIntervalTimer.suspend()
        self.gameAdFirstIntervalTimer.suspend()
        
    }
    func startNewAdInterval(adDuration:Int, firstInterval:Int, AfterwardInterval:Int) {
        print("start new game ad interval")
        self.gameAdDismissTimer.suspend()
        self.gameAdAfterwardIntervalTimer.suspend()
        self.gameAdFirstIntervalTimer.suspend()
        
        
        
        self.gameAdFirstIntervalTimer = OONATimer()
        self.gameAdFirstIntervalTimer.identifier = "gameAdFirstIntervalTimer"
        self.gameAdFirstIntervalTimer.startTimer(seconds:firstInterval) {
//            self.showEngagementAd()
            // main show game ad button
            PlayerHelper.shared.showGameAd.accept(true)
            self.gameAdDismissTimer.suspend()
            self.gameAdDismissTimer = OONATimer()
            self.gameAdDismissTimer.identifier = "engagementAdDismissTimer"
            self.gameAdDismissTimer.startTimer(seconds: adDuration){
//                self.hideEngagementAd()
                // main hide game ad button
                PlayerHelper.shared.hideGameAd.accept(true)
                self.startAfterwardAdInterval(adDuration:adDuration,interval:AfterwardInterval)
                
            }
            
        }
        
    }
    func startAfterwardAdInterval(adDuration:Int, interval:Int) {
        self.gameAdAfterwardIntervalTimer.suspend()
        self.gameAdAfterwardIntervalTimer = OONATimer()
        self.gameAdAfterwardIntervalTimer.identifier = "gameAdAfterwardIntervalTimer"
        self.gameAdAfterwardIntervalTimer.startTimer(seconds: interval) {
//            self.showEngagementAd()
            // main show ad button
            PlayerHelper.shared.showGameAd.accept(true)
            self.gameAdDismissTimer.suspend()
            self.gameAdDismissTimer = OONATimer()
            self.gameAdDismissTimer.identifier = "gameAdDismissTimer after "
            self.gameAdDismissTimer.startTimer(seconds: adDuration) {
                
                // main hide game ad button
//                self.hideEngagementAd()
                PlayerHelper.shared.hideGameAd.accept(true)
                self.startAfterwardAdInterval(adDuration:adDuration,interval:interval)
            }
        }
    }
//    print("resetYoutubeView sub")
//    //            self.present(vc, animated: true, completion: nil)
//    self?.resetYoutubeViewConstraints()
//    if (self?.currentPlayer == .youtube){
//    self?.youtubePlayerView.playVideo()
//    }
//    }).disposed(by: disposeBag)
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {// edit: changed fun to func
        if (message.name == "tcoinRewards"){
            print("WebKit tcoinRewards callback \(message.body)")
            var tcoinEarned : Int = 0
            let json = JSON(message.body)
            if let tcoin = json["tcoins"].int {
                print("str",tcoin)
                tcoinEarned = tcoin
            }
            
            totalTcoinEarned = totalTcoinEarned + tcoinEarned
            
            self.requestReward()
            print ("tcoin earned: ",tcoinEarned)
            print ("Total earned: ",totalTcoinEarned)
        }
    }
    func sendGameStartAnalytics(game:Game) {
        AnalyticHelper.shared.logEventToAllPlatform(name: "game_open", parameters: ["game_id":game.id])
    }
    func sendGameEndAnalytics(game:Game) {
          AnalyticHelper.shared.logEventToAllPlatform(name: "game_close", parameters: ["game_id":game.id])
      }
    var totalTcoinEarned : Int = 0
    func play(game: Game) {
        if (self.currentGame != nil) {
            self.sendGameEndAnalytics(game: (self.currentGame!))
            self.currentGame = nil
        }
        
        
        currentGame = game
        GameHelper.shared.currentGame = game
        sendGameStartAnalytics(game: game)
        
        print("play game",game.url)
        let contentController = WKUserContentController()
        contentController.add(self, name: "tcoinRewards")
//
//        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
//        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)

//        contentController.addUserScript(userScript)
        
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        
        self.webView?.removeFromSuperview()
        
        self.webView = WKWebView.init(frame: self.gameWebView.bounds, configuration: config)
        self.webView?.scrollView.isScrollEnabled = false
        self.gameWebView.addSubview(self.webView!)
        if let url = (URL.init(string: game.url )) {
//        let url = URL.init(string: "http://192.168.50.209:5500/HTML5/test.html")
        self.webView!.load(URLRequest.init(url: url))
        GameHelper.shared.currentGame = game 
        UIView.animate(withDuration: 0.3, animations: {
            self.playArea.alpha = 1
            if (!DeviceHelper.shared.isAdFree) {
            self.startNewAdInterval(adDuration: 10, firstInterval: 15, AfterwardInterval: 180)
            }
        })
        }else{
            self.hideSystemDialog()
            self.showSystemDialog(text: (game.name + " is coming soon, stay tuned!"))
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hideSystemDialog()
              
                
            }
        }
    }
    
    func play(url: URL) {
        if (self.currentGame != nil) {
            self.sendGameEndAnalytics(game: (self.currentGame!))
            self.currentGame = nil
        }
        
        var game = Game.init()
        game.id = 1
        game.name = "Ninja Run"
        currentGame = game
        GameHelper.shared.currentGame = game
        
        print("play url",url.absoluteString)
        let contentController = WKUserContentController()
        contentController.add(self, name: "tcoinRewards")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        
        self.webView?.removeFromSuperview()
        
        self.webView = WKWebView.init(frame: self.gameWebView.bounds, configuration: config)
        self.webView?.scrollView.isScrollEnabled = false
        self.gameWebView.addSubview(self.webView!)
//        let url = (URL.init(string: url))!
        //        let url = URL.init(string: "http://192.168.50.209:5500/HTML5/test.html")
        self.webView!.load(URLRequest.init(url: url))
        UIView.animate(withDuration: 0.3, animations: {
            self.playArea.alpha = 1
        })
    }
    func tryUnlock(gameId:Int) {
        let message = "Are you sure to use 100 tcoins to unlock this game?\n Current Balance: \(UserHelper.shared.currentUserProfile()?.myPointBalance ?? 0)"
        let popup = CustomPopup(message: message)

          popup?.button1.setTitle("Yes", for: .normal)
          popup?.button2.setTitle("No", for: .normal)
            //    CGRect buttonFrame = popup.button1.frame;
            //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
            //    popup.button1.frame = buttonFrame;
            let popupBlock = popup
            //    popup.backBtn. hidden  = YES;
            popup?.setupTwoButton()
            popup?.completionButton1 = {
                self.unlockedGameIdList.append(gameId)
                UserDefaults.standard.set(self.unlockedGameIdList, forKey: "unlockedGameIdList")
                self.gameCollectionView.reloadData()
                popupBlock?.dismiss()
                
//              PlayerHelper.shared.showXplore.accept(true)
                
            }
              popup?.completionButton2 = {
                  
                    popupBlock?.dismiss()
                      
                  
              }
            popup?.alpha = 0
            let window = UIApplication.shared.keyWindow!
            window.addSubview(popup!) // Add your view here
            
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                popup?.alpha = 1
            })
    }
    @IBAction func showPosterImage(_ sender: Any) {
        self.showTutorialPoster()
    }
    
    override func appLanguageDidUpdate() {
        self.gamePageTitleLabel.text = LanguageHelper.localizedString("GAMES")
        self.gamePageDescriptionLabel.text = LanguageHelper.localizedString("Play_games_to_earn_tcoins_and_WIN_real_prizes")
    }
}

// MARK: - CollectionView Delegate, Datasource and Layout
extension GameViewController {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.gameList.value.count > 0 && indexPath.item < self.gameList.value.count {
            var locked = false
//            if (DeviceHelper.shared.isEssentialUiOnly) {
//                if (gameList.value[indexPath.row].id == 10) || (gameList.value[indexPath.row].id == 13) {
//                    if !unlockedGameIdList.contains(gameList.value[indexPath.row].id) {
//                        locked = true
//                    }
//                }
//
//            }
            if locked{
                self.tryUnlock(gameId: gameList.value[indexPath.row].id)
            } else{
                self.play(game: self.gameList.value[indexPath.row])
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gameList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let _game: Game = self.gameList.value[indexPath.item]
        let cell = self.gameCollectionView.dequeueReusableCell(withReuseIdentifier: "GameCell",
                                                               for: indexPath) as! GameCell
        cell.configure(with: _game)
        cell.overlayImg.isHidden = true
//        if (DeviceHelper.shared.isEssentialUiOnly) {
//            if (_game.id == 10) || (_game.id == 13) {
//                if !unlockedGameIdList.contains(_game.id) {
//                    cell.overlayImg.isHidden = false
//                }
//            }
//
//        }
        
        print("Cell frame: \(cell.frame)")
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
//        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        let height = ((viewHeight - lineSpacing ) ) / 2 - lineSpacing * 2//17 is label height
        return CGSize(width: width, height: height)
    }
}

// Show Tutorials
extension GameViewController {
    func showGameAdTutorial() {
//        guard self.gameAdTutorialView == nil else { print("Game Ad is displaying"); return }
        guard let container = self.gameAdContainerView else { return }
        print("Displaying game ad tutorial")
        container.isHidden = false
        container.backgroundColor = .black
        container.alpha = 0

        let closeButton: UIButton = UIButton(type: .system)
        closeButton.setImage(UIImage(named: "close-off"), for: .normal)
        closeButton.setImage(UIImage(named: "close-off"), for: .highlighted)
        closeButton.addTarget(self, action: #selector(dismissGameAdTutorial), for: .touchUpInside)
        
        container.addSubview(closeButton)
        
        closeButton.snp.makeConstraints { (make) in
            make.trailing.top.equalTo(self.view.safeAreaLayoutGuide)
            make.width.height.equalTo(50)
        }
        self.gameAdContainerConstraintLeading?.constant = 0
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
                        container.alpha = 1
        },
                       completion: nil)
    }
    
    @objc func dismissGameAdTutorial() {
        guard let container = self.gameAdContainerView else { print("Game ad Poster not exist"); return }
        
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
                        container.alpha = 0
        },
                       completion: { (_) in
                        container.removeFromSuperview()
                        self.gameAdContainerView = nil
//                        self.gameAdTutorialView = nil
                        self.startNewAdInterval(adDuration: 10, firstInterval: 15, AfterwardInterval: 180)
        })
        if GameHelper.shared.userFirstPlay {
            GameHelper.shared.updateUserFirstPlayed()
        }
    }
    
    func showTutorialPoster() {
        let container: UIView = UIView()
        container.backgroundColor = .black
        container.alpha = 0
        
        let posterView: UIImageView = UIImageView()
        posterView.image = UIImage(named: "tutorialposter")
        posterView.contentMode = .scaleAspectFit
        
        let closeButton: UIButton = UIButton(type: .system)
        closeButton.setImage(UIImage(named: "close-off"), for: .normal)
        closeButton.setImage(UIImage(named: "close-off"), for: .highlighted)
        closeButton.tintColor = .white
        closeButton.addTarget(self, action: #selector(hideTutorialPoster), for: .touchUpInside)
        
        container.addSubview(posterView)
        container.addSubview(closeButton)
        self.view.addSubview(container)
        
        container.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        
        posterView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        
        closeButton.snp.makeConstraints { (make) in
            make.trailing.top.equalTo(self.view.safeAreaLayoutGuide)
            make.width.height.equalTo(50)
        }
        
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
                        container.alpha = 1
        },
                       completion: nil)
        
        self.tutorialPosterView = container
    }
    
    @objc func hideTutorialPoster() {
        guard let _posterView = self.tutorialPosterView else { print("Tutorial Poster not exist"); return }
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
                        _posterView.alpha = 0
        },
                       completion: { (_) in
                        _posterView.removeFromSuperview()
                        self.tutorialPosterView = nil
        })
        if !GameHelper.shared.userFirstPlay {
            self.gameAdDisposeBag = DisposeBag()
        }
    }
}
