//
//  GameHelper.swift
//  OONA
//
//  Created by Jack on 28/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
import RxSwift
import RxCocoa
//import PromiseKit

class GameHelper: NSObject {
    static let shared: GameHelper = {
      let helper = GameHelper()
//        #if DEBUG
//        helper.resetFirstPlay()
//        #endif
        helper.userFirstPlay = helper.userIsFirstPlay()
        return helper
    }()
    var selectedCategoryID = Int()
    var dismissGameArea: PublishRelay<Bool?> = PublishRelay<Bool?>()
    var showGameAdTutorial: PublishRelay<Bool> = PublishRelay<Bool>()
    var gameAdStart: PublishRelay<Bool?> = PublishRelay<Bool?>()
    var rewardReceived: PublishRelay<Reward?> = PublishRelay<Reward?>()
    //func getSelectedChannelId(){
    //return self.selectedChannelId
    //}
    public var currentGame : Game?
    
    var userFirstPlay: Bool = false
    
    func updateUserFirstPlayed() {
        UserDefaults.standard.set(Date(), forKey:"OONAGameUserFirstPlay")
        userFirstPlay = self.userIsFirstPlay()
    }
    
    func resetFirstPlay() {
        UserDefaults.standard.removeObject(forKey: "OONAGameUserFirstPlay")
    }
    
    func userIsFirstPlay() -> Bool {
        let playedDate: Date? = UserDefaults.standard.object(forKey: "OONAGameUserFirstPlay") as? Date
        self.userFirstPlay = playedDate == nil
        return playedDate == nil
    }
    
    func getGameList(type: String?, offset: Int, success: @escaping ([Game]) -> (), failure: @escaping ((Any?) -> ())){
        let _type = type ?? ""
        OONAApi.shared.postRequest(url: APPURL.GameList(), parameter: ["type": _type , "offset":offset]).responseJSON{ [weak self] (response) in
            
            switch response.result {
                
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string {
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { (message, isSuccess) in
                                                    if(isSuccess){
                                                        let games: [Game] = JSON(json["games"])
                                                            .oonaModelArray(action: { (games: Game) in
                                                                
                                                                return games
                                                            })
                                                        success(games)
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                        failure(nil)
                                                        }
                                                    
                        })
                    }
                
                case .failure(let error):
                    print(error)
                    failure(error)
                }
            }
    }
}
