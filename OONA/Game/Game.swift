//
//  Ga,e.swift
//  OONA
//
//  Created by Jack on 28/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

struct Game: OONAModel {
    var id : Int = 0
    var name : String = ""
    
    var imageUrl : String = ""
    var url : String = ""
    
    init(){
        
    }
    
    init?(json: JSON) {
        if let _id: Int = json["id"].int {
            self.id = _id
        }
        if let _name: String = json["name"].string {
            self.name = _name
        }
      
        if let _imageUrl: String = json["image"].string {
            self.imageUrl = _imageUrl
        }
        if let _url: String = json["url"].string {
            self.url = _url
        }
       
    }
    
   
}
