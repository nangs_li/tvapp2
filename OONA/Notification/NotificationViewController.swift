//
//  NotificationViewController.swift
//  OONA
//
//  Created by Vick on 16/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import WebKit
import SDWebImage

class NotificationViewController: HoverViewController{

    var dt : NotificationDisplayType = NotificationDisplayType.Image
    var url:URL?
    var action:URL?
    @IBOutlet weak var webView:WKWebView!
    @IBOutlet weak var btn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _url = url{
            if(self.dt == NotificationDisplayType.Image){
                webView.isHidden = true
                //use image
                btn.layer.cornerRadius = 16 //custom setting
                btn.layer.masksToBounds = true
                btn?.sd_setBackgroundImage(with: _url, for: .normal, completed: nil)
            }else{
                btn.isHidden = true
                let request = URLRequest(url: _url)
                webView.load(request)
                webView.layer.cornerRadius = 16 //custom setting
                webView.layer.masksToBounds = true
            }
        }
    }
    
    @IBAction func btnResult(_ sender: UIButton) {
        if let _action = action {
            UIApplication.shared.open(_action, options: [:]) { (success) in
                print("Open \(_action): \(success)")
            }
        }
    }
}
