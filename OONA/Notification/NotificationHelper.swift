//
//  NotificationHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import RxCocoa
import UserNotifications
import SwiftyJSON

class NotificationHelper: NSObject, UNUserNotificationCenterDelegate {
//    let notification: PublishRelay<UNNotificationContent> = PublishRelay<UNNotificationContent>()
    static let shared: NotificationHelper = { return NotificationHelper() } ()
    
    func configure() { self.checkPermissionForPushNotifications() }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("\(String(describing: type(of: self))) -> \(#function) -> \(center)")
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("\(String(describing: type(of: self))) -> \(#function) -> \(center) -> Notification content\(response.notification.request.content.userInfo as Dictionary)")
        
        print("didReceive noti 1",response.notification.request.content.userInfo)
        completionHandler()
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        var applicationState: String = ""
        switch UIApplication.shared.applicationState {
        case .active:
            applicationState = "Active"
        case .inactive:
            applicationState = "Inactive"
        case .background:
            applicationState = "Background"
            
        @unknown default:
            fatalError("Unknown default, probably has unhandled new cases.")
        }
        
        print("[NotificationHelper] \(String(describing: type(of: self))) -> \(#function) -> \(center) -> App satus: \(applicationState)")
        if UIApplication.shared.applicationState == .active {
            //TODO: incomplete arguments and logics
            print("[NotificationHelper] received notification when app in ACTIVE\nMessage body:",notification.request.content.userInfo)
            
            if let userInfo = notification.request.content.userInfo as [AnyHashable : Any]?
            {
                print("reward userInfo",userInfo[AnyHashable("reward")])
                
                let rewardObject = JSON.init(parseJSON: userInfo[AnyHashable("reward")] as! String)
                if let message = rewardObject["message"].string {
                     PlayerHelper.shared.showHTMLSystemMessage.accept(message)
                }
                
 
              
            }
            
            
            let json = JSON(notification.request.content.userInfo)
            if let reward = json["reward"].dictionaryObject {
                
                if  let message = reward["message"]{
                    print("_message",message)
                }
                
            }
            if let _message = json["reward"]["message"].string {
                
            }
            let urlString: String = "https://www.oonatv.com/assets/img/freechannels.png"
            var url: URL? = URL(string: urlString)
            if let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                url = URL(string: encodedURL)
                print(encodedURL)
                print(url as Any)
            }
            
            let actionString: String = "https://www.oonatv.com"
            var action: URL? = URL(string: actionString)
            if let encodedActionURL = actionString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                action = URL(string: encodedActionURL)
                print(encodedActionURL)
                print(action as Any)
            }
            
            let type = NotificationDisplayType.Image
            self.showNotification(url: url, action: action, type: type)
        }
        //completionHandler([.badge, .alert, .sound])
    }
    
    private func checkPermissionForPushNotifications() {
        
        let authorizationOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center // 1
            .requestAuthorization(options: authorizationOptions) { // 2
                [weak self] (granted, error) in
                print("Permission granted: \(granted)") // 3
                if let _error = error {
                    print("\(_error)")
                }
                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    
    private func getNotificationSettings() {
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
        /* // may use in the future
         UNUserNotificationCenter.current().getNotificationSettings { settings in
         print("Notification settings: \(settings)")
         guard settings.authorizationStatus == .authorized else { return }
         DispatchQueue.main.async {
         UIApplication.shared.registerForRemoteNotifications()
         }
         }
         */
    }
    
    func showNotification(url: URL? , action:URL? , type:NotificationDisplayType){
        if(!UserData.safeMode()){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateInitialViewController() as! NotificationViewController
            vc.url = url
            vc.action = action
            vc.dt = type
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .popover
            
            guard let _topMostVC = UIApplication.shared.keyWindow?.rootViewController?.topMostViewController() else { print("KeyWindow or rootViewController not available"); return }
            //TODO: incomplete page design
//            _topMostVC.present(vc, animated: true, completion: nil)
        }
    }

}
