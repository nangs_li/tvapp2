//
//  BaseViewController.swift
//  OONA
//
//  Created by Jack on 16/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import UIKit

//for level 3 view controller

class HoverViewController : BaseViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func dismissbuttonClicked(_ sender: AnyObject?) {
        dismiss(animated: false, completion: nil)
    }
}
