//
//  DomainConstants.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

struct APPURL {
    
    public struct Domains {
        //static let dev = "https://api-us.oonatv.com"
        
        static let dev = "http://apidev.boosst.info"
        
        // https://api-us.oonatv.com
//        static let uat = "https://apiuat4.watchoona.com"
        static let uat = "https://api-ph-uat.watchoona.com"
        static let prod = "https://api.watchoona.com"
        
    }
    public func setDomain(url:String, region:String) {
        OONAApi.shared.setDomain(url: url, region: region)
    }
    private  struct Routes {
        static let Api = "/api/v1"
    }
    public static let GlobalDomain = "https://api-global.oonatv.com"
//    public  static var Domain = "https://" + OONAApi.shared.currentDomain()
    private static let Route = Routes.Api
    private static func BaseURL() -> String {
        
        var prefix = "https://"
        
        if OONAApi.shared.currentDomain().hasPrefix("http"){
            prefix = ""
        }
        return prefix + OONAApi.shared.currentDomain() + Route
    }
    private  static let GlobalURL = GlobalDomain + Route
    
    
    static var UserRegion: String {
        return GlobalURL + "/user-region"
    }
    static var inviteCodeEnter : String {
        return GlobalURL + "/verify-invite-code"
    }
    static var RegisterDevice: String {
        return BaseURL() + "/register-device"
    }
    static var ForgetPassword : String {
        return BaseURL() + "/forgot-password"
    }
    static var Category: String {
        return BaseURL() + "/categories"
    }
    static var RadioCategory: String {
        return BaseURL() + "/radio/categories"
    }
    static var Channel: String {
        return BaseURL() + "/channel"
    }
    static var SuitablePlaylist: String {
        return BaseURL() + "/playlist/" + ChannelSetting.channelListFilterType.suitable.description
    }
    static func Series(channelId: String) -> String {
        return BaseURL() + "/channel/" + channelId + "/series"
    }
    static func Episode(channelId: String) -> String {
        return BaseURL() + "/channel/" + channelId + "/episode"
    }
    static var ReferCodeEnter : String {
        return BaseURL() + "/verify-referral-code"
    }
    static var TransferCodeEnter : String {
        return BaseURL() + "/verify-transfer-code"
    }
    static var Login: String {
        return BaseURL()  + "/login/email"
    }
    static var Register: String {
        return BaseURL()  + "/register"
    }
    static var Passcode: String {
        return BaseURL()  + "/verify-user-registration-code"
    }
    static var UserProfile: String {
        return BaseURL()  + "/user-profile"
    }
    static var UpdateUserProfile: String {
        return BaseURL()  + "/user-profile/update"
    }
    static var ChangePassword: String {
        return BaseURL()  + "/change-password"
    }
    static var BidItemList: String {
        return BaseURL()  + "/points/bid/items"
    }
    static func BidItemDetail(itemId:String) -> String{
        return BaseURL()  + "/points/bid/item/" + itemId
    }
    static var PlaceBid: String {
        return BaseURL() + "/points/bid/place"
    }
//    static func LiveDisplayAD(channelId: String) -> String {
//        return BaseURL + "/channel/" + channelId + "/display-ads"
//    }
    static func displayAds(video:Video) -> String {
        if (video.type == .vod) {
            // live channel
            return BaseURL() + "/channel/" + String(video.channelId) + "/episode/" + String(video.episodeId) + "/display-ads"
        }
        return BaseURL() + "/channel/" + String(video.channelId) +  "/display-ads"
    }
    
    static func videoAds(video:Video) -> String {
        if (video.type == .vod) {
            // live channel
            return BaseURL() + "/channel/" + String(video.channelId) + "/episode/" + String(video.episodeId) + "/video-ads"
        }
        return BaseURL() + "/channel/" + String(video.channelId) +  "/video-ads"
        
    }
    static func gameAds(game:Game) -> String {
        
        var gameid = String(game.id)
        gameid = "1"
        return BaseURL() + "/game/" + gameid + "/display-ads"
//        }
        
    }
    static var MultiAnalyticsEvent: String {
        return BaseURL() + "/analytics/events"
    }
    
    static func StreamingInfo(channelId: String) -> String {
        return BaseURL() + "/channel/" + channelId + "/streaming-info"
    }
    
    static func StreamingInfo(channelId: String, episodeId: String) -> String {
        return BaseURL() + "/channel/" + channelId + "/episode/" + episodeId + "/streaming-info"
    }
    
    static func getStreamingUrl(brightcoveId: String) -> String {
        return BaseURL() + "/brightcove/" + brightcoveId + "/streaming-url"
    }
    static func analyticEvent(event:String) -> String {
        return BaseURL() + "/analytics/event/" + event
    }
    static func requestReward(event:String) -> String {
        return BaseURL() + "/points/request-reward/" + event
    }
    
    //static var FacebookLogin: String {
    //    return BaseURL + "/auth/facebook"
    
    static var facebookLogin: String {
        return BaseURL()  + "/login/facebook"
    }
    
    
    static var googleLogin: String {
        return BaseURL()  + "/login/gmail"
    }
    
    static var instagramLogin: String {
        return BaseURL()  + "/login/instagram"
    }
    static func SearchInfo() -> String {
        return BaseURL() + "/search/info"
    }
    static func KeyWordSuggestions() -> String {
        return BaseURL() + "/search/suggestion"
    }
    
    static func SearchResult() -> String {
        return BaseURL() + "/search"
    }
    
    static func SearchYouTubeResult() -> String {
        return BaseURL() + "youtube/search"
    }
    
    static func ExploreEpisode() -> String {
        return BaseURL() + "/explore"
    }
    
    static func RadioChannelList() -> String {
        return BaseURL() + "/radio"
    }
    
    static func RadioEpisodeList(channelId: String) -> String {
        return BaseURL() +  "/radio/" + channelId + "/episode"
    }
    
    static func AddFavouriteRadio(channelId: String) -> String {
        return BaseURL() +  "/favourite/radio/" +  channelId
    }
    static func AddFavouriteRadio(channelId: String,episodeId: String) -> String {
            return BaseURL() +  "/favourite/radio/" +  channelId + "/podcast/" + episodeId
    }
    
    static func RemoveFavouriteRadio(channelId: String) -> String {
              return BaseURL() +  "/favourite/radio/" +  channelId
    }
    static func RemoveFavouriteRadio(channelId: String,episodeId: String) -> String {
          return BaseURL() +  "/favourite/radio/" +  channelId + "/podcast/" + episodeId
      }
    
    static func GameList() -> String {
        return BaseURL() + "/game"
    }
    
    static func ParentalControl() -> String {
        return BaseURL() + "/parental-control/preference"
    }
    
    static func ParentalControlCheckPasscode() -> String {
        return BaseURL() + "/parental-control/passcode"
    }
    
    static func ParentalControlForgotPasscode() -> String {
        return BaseURL() + "/parental-control/forgot-passcode"
    }
    
    static func ParentalControlAllowedChannel(channelId:String) -> String {
        return BaseURL() + "/parental-control/allowed-channel/" + channelId
    }
    
    static func ParentalControlAllowed() -> String {
        return BaseURL() + "/playlist/" + ChannelSetting.channelListFilterType.allowed.description
    }
    /**external api***/
    
    static func getInstagramApi(access_token: String) -> String {
        return "https://api.instagram.com/v1/users/self/?access_token=" + access_token
    }
    
    static func privacyPolicy() -> String {
        
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/privacy-policy.html"
//        return "http://dev.boosst.info:803/privacy-policy.html"
//        return "http://apidev.boosst.info/public/privacy-policy.html"
    }
    static func settingsGeneralTermsAndConditions() -> String {
        var suffix = ""
        if (DeviceHelper.shared.isEssentialUiOnly) {
            suffix = "?code=199999"
        }
        
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/terms.html" + suffix
    }
    
    static func settingsTCoinsTermsAndConditions() -> String {
        var suffix = ""
        if (DeviceHelper.shared.isEssentialUiOnly) {
            suffix = "?code=199999"
        }
        
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/tcoins-terms.html" + suffix
    }
    
    static func biddingTermsAndConditions() -> String {
        var suffix = ""
        if (DeviceHelper.shared.isEssentialUiOnly) {
            suffix = "?code=199999"
        }
        
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/bidding-terms.html" + suffix
    }
    ///bid-winners
    static func biddingWinner() -> String {
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/bid-winners"
    }
    static func howDoesItWork() -> String {
        return BaseURL().replacingOccurrences(of: Route, with: "") + "/howtouse.html"
    }
    
    static func tcoinsStoreCategoryList() -> String {
        return BaseURL() + "/points/gifts/categories"
        
        //MARK: will remove when feature complete
        /*
         Rqeuest:
         Type: GET
         
         
         Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "categories" : [
            {
            "id" : 1,
            "name" : "XXXXX"
            }, ...
            ]
        }
        
        Fail:
        {
            "status" : "error",
            "code" : XX
        }
         */
    }
    static func unlockItem() -> String {
           return BaseURL() + "/presents"
    }
    static func tcoinsStoreItemList() -> String {
        return BaseURL() + "/points/gifts"
        
        //MARK: will remove when feature complete
        /*
        Request:
        Type: POST
        Parameters:
        offset (Optional) Number of offset items, for pagination
        categoryId (Optional)
        
        Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "pointsBalance" : 30000,
            "disableBonusCodeInput" : 1 // By default (v 1.7.11 or above) the Bonus Code input bar appears in Gift list
            "gifts" : [
            {
            "id" : 1,
            "name" : "XXXXX",
            "points" : 10000,
            "imageUrl" : "http://XXXXX.jpg",
            "overlayUrl" : "http://XXXXX.png",
            "stock" : 6500,
            "type" : "XXXXX",
            "validUntil" : "2018-01-01 10:00:00",
            "expiresIn" : 86400, (Currently 24 hours)
            "description" : "XXXX",
            "terms" : "XXXX",
            "howToUse" : "XXXX",
            "validForDays" : 7
            }, {
            "type" : "bidBanner",
            "imageUrl" : "http://XXXXX.jpg",
            }, ...
            ]
        }
        
        Fail:
        {
            "status" : "error",
            "code" : XX
        }
         */
    }
    
    static func tcoinStoreItemDetail(_ giftID: Int) -> String {
        return BaseURL() + "/points/gift/\(giftID)"
        //MARK: will remove when feature complete
         /*
         Request:
         Type: GET
        
         Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "type" : "XXXXX",
            "name" : "XXXXX",
            "points" : 10000,
            "imageUrl" : "http://XXXXX.jpg",
            "validUntil" : "2018-01-01 10:00:00",
            "expiresIn" : 86400, (Currently 24 hours)
            "description" : "XXXX",
            "terms" : "XXXX",
            "howToUse" : "XXXX",
            "validForDays" : 7
        }
         
        Fail:
        {
            "status" : "error",
            "code" : XX
        }
         */
    }
    
    static func tcoinStorePurchasedVoucherList() -> String {
        return BaseURL() + "/points/vouchers"
        //MARK: will remove when feature complete
        /*
        Request:
        Type: POST
        Parameters:
        offset (Optional) Number of offset items, for pagination
            
        Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "pointsBalance" : 30000,
            "vouchers" : [
            {
            "id" : XX,
            "name" : "XXXXX",
            "imageUrl" : "http://XXXXX.jpg",
            "expiresIn" : 36000 (seconds)
            "validUntil" : "2018-01-01 10:00:00",
            "description" : "XXXX",
            "terms" : "XXXX",
            "howToUse" : "XXXX",
            "voucherCode" : "qbcJAHBHa1",
            }, ...
            ]
        }
        
        Fail:
        {
            "status" : "error",
            "code" : XX
        }
         */
    }
    
    static func tcoinStorePurchasedVoucherDetail(_ voucherID: Int) -> String {
        return BaseURL() + "/points/voucher/\(voucherID)"
        //MARK: will remove when feature complete
        /*
        Request:
        Type: GET
         
        Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "name" : "XXXXX",
            "imageUrl" : "http://XXXXX.jpg",
            "validUntil" : "2018-01-01 10:00:00",
            "description" : "XXXX",
            "terms" : "XXXX",
            "howToUse" : "XXXX",
            "voucherCode" : "qbcJAHBHa1",
            "expiresIn" : 36000
        }
        
        Fail:
        {
            "status" : "error",
            "code" : XX
        }
         */
    }
    
    static func tcoinsStoreRedeemItem() -> String {
        return BaseURL() + "/points/gift/redeem"
        //MARK: will remove when feature complete
        /*
        Request:
        Type: POST
        Parameters:
        giftId
         
        Response:
        Success:
        {
            "status" : "success",
            "code" : XX,
            "voucherId" : XX,
            "pointsBalance" : 0
        }
        
        Fail:
        {
            "status" : "error",
            "code" : XX
        }

        */
    }
}

