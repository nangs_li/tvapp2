//
//  DomainConstants.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

import UIKit
import Gifu

extension UIImageView: GIFAnimatable {
    private struct AssociatedKeys {
        static var AnimatorKey = "gifu.animator.key"
    }
    
    override open func display(_ layer: CALayer) {
        updateImageIfNeeded()
    }
    
    public var animator: Animator? {
        get {
            guard let animator = objc_getAssociatedObject(self, &AssociatedKeys.AnimatorKey) as? Animator else {
                let animator = Animator(withDelegate: self)
                self.animator = animator
                return animator
            }
            
            return animator
        }
        
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.AnimatorKey, newValue as Animator?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

struct APPSetting {
    
    static let googleClientId: String = "857701237614-b6b0u75nkt69pno298s638r9lp1r0n1s.apps.googleusercontent.com"
    static let regSecret: String = "wEa+sc8BaEzrS?-K"
    static let sizeToSendAnalyticQueue = 10 ;
    struct firebase {
        static let fetchConfigexpirationDuration : TimeInterval = 3600.0
        static let signatureKey = "chimmeng"
    }
    
    static let bugsnagKey = "847129d04343e624b7bfddcd6753b962"
    
    struct font {
        
        static let montserratXS : UIFont = UIFont(name: "Montserrat", size: 12)!
        static let montserratS : UIFont = UIFont(name: "Montserrat", size: 16)!
        static let montserrat : UIFont = UIFont(name: "Montserrat", size: 18)!
        static let montserratX : UIFont =  UIFont(name: "Montserrat", size: 22)!
        static let montserratXXL: UIFont =  UIFont(name: "Montserrat", size: 40)!
        
    }
    
    //used to esitmate the width of the category text, have to be sync with category cell
    static let defaultCategoryCellHeight : CGFloat = 30.0
    static let defaultCategoryCellReservedSpace : CGFloat = 40.0
    
    //
    static let leftViewConstraint : CGFloat = 0
    //iphone 678
    static let leftViewConstraintSafearea : CGFloat = 10
    //
    static let leftConstraintForInnerPage : CGFloat = 24
    //iphone 678
    static let leftConstraintForInnerPageSafearea : CGFloat = 0
    
    
    static let rightConstraintForInnerPage : CGFloat = 70
    //iphone 678
    static let rightConstraintForInnerPageSafearea : CGFloat = 0
    
    //
    static let episodeChannelObjectWidth : CGFloat = 111.0
    static let episodeChannelObjectHeight : CGFloat = 87.0
    static let defaultCornerRadius : CGFloat = 2.5
    static let lowerViewHeight : CGFloat = 60.0
    
    static let defaultCvViewLeftPadding : CGFloat = 132.0
    
    
}


struct InstagramIds {
    
    /**instagram**/
    static let InstgramAuthUrl = "https://api.instagram.com/oauth/authorize/"
    static let InstgramAPIURl = "https://api.instagram.com/v1/users/"
    static let InstgramClientId = "a8ce4e9896444614831598cad1af3e16"
    static let InstgramClientSecret = "eb4183dfd89b47108fe575fe58c500ad"
    static let InstgramRedirectUrl = "http://oona-ios.com"
    static let InstgramAccessToken = "access_token"
    static let InstgramScope = "likes+comments+relationships"
    
}

enum VideoMode: Int {
    case none
    case youtube
    case brightCove //brightcove
    case streamingLink //streamingLink
}

enum MenuType: Int {
    case none
    case left
    case right
    case up
    case bottom
}

enum ParentalControlMode: Int {
    case none
    case edit
    case create
    case access
}

enum ParentalControlAction: Int {
    case none
    case changePasscode
    case changeEmail
}

enum UserActionType: Int {
    case none
    case register
    case login
}

enum SubMenu : Int {
    case none = 0
    case search = 1
    case home = 2
    case tcoins = 3
    case tv = 4
    case xplore = 5
    case music = 15 //15 is the category id of music in db
    case apps = 6
    case bot = 7
    case radio = 11
    var description : String {
        switch self {
            case .none: return ""
            case .search: return "Search"
            case .home: return "Home"
            case .tcoins: return "tcoins"
            case .tv: return "TV"
            case .xplore: return "Xplore"
            case .music: return "Music"
            case .apps: return "Apps"
            case .bot: return "OONA Bot"
            case .radio:return "Radio"
        }
    }
    
    var categoryId : String { //used to post to category
        switch self {
        case .none: return ""
        case .home: return "home"
        case .tv: return "tv"
        case .music: return "music"
        case .search:
            return ""
        case .tcoins:
            return ""
        case .xplore:
            return ""
        case .apps:
            return ""
        case .bot:
            return ""
        case . radio:
            return ""
        }
    }
    
}

enum LoginType: Int {
    case oona
    case facebook
    case instagram
    case google
}

enum LeftMenuType: Int {
    case none
    case half
    case full
}


enum MenuAction: Int { //completion block action
    case none
    case preview
    case load
}

enum ErrorAction: Int{
    case none
    case signupConfirmation
    case showError
}

enum CBAction: Int { //completion block action
    case none
    case load
    case back
    case select
    case nowPlaying
}

enum PanOrientation: Int {
    case none
    case horizontal
    case vertical
}

enum PanDirection: Int {
    case none
    case panUp
    case panDown
    case panLeft
    case panRight

    var orientation: PanOrientation {
        switch self {
        case .panLeft, .panRight: return .horizontal
        case .panUp, .panDown: return .vertical
        default: return .none
        }
    }
}

//extension UIPanGestureRecognizer {
//
//    var direction: Orientation? {
//        let panVelocity = velocity(in: view)
//        let vertical = abs(panVelocity.y) > abs(panVelocity.x)
//        switch (vertical, panVelocity.x, panVelocity.y) {
//        case (true, _, let y) where y < 0: return .up
//        case (true, _, let y) where y > 0: return .down
//        case (false, let x, _) where x > 0: return .right
//        case (false, let x, _) where x < 0: return .left
//        default: return nil
//        }
//    }
//}

enum NotificationDisplayType: String {
    case Image = "Image"
    case Webview = "Webview"
}

enum Language : String {
    case english = "en"
    case indonesian = "id"
    case arabic = "ar"
    
    var description : String {
        switch self {
        case .english: return "en"
        case .indonesian: return "id"
        case .arabic: return "ar"
        }
    }
    
    var detailString: String {
        switch self {
        case .english: return "English"
        case .indonesian: return "Indonesian"
        case .arabic: return "Arabic"
        }
    }
}

enum CategoryType : Int{ //actually should seperate category with filter .. TODO_OONA
    case category
    case filter
}

struct ChannelSetting{
    enum channelType : String {
        case none
        case live
        case vod
        case mix
        case linear
        var description : String {
            switch self {
                case .none: return ""
                case .live: return "live"
                case .vod: return "vod"
                case .mix: return "mix"
                case .linear: return "linear"
            }
        }
    }
    
    enum channelListFilterType : Int  {
        case none
        case mood
        case category
        case shuffle
        case search
        case normal
        case favourites = -99
        case live = -98
        case recentlyWatched = -97
        case new  = -96
        case popular = -95
        case searchResultChannelSingle
        case searchResultEpisodeSingle
        case searchResultChannelList
        case searchResultEpisodeList
        case suitable
        case allowed
        var description : String {
            switch self {
                case .none: return ""
                case .mood: return "mood"
                case .category: return "category"
                case .shuffle: return "shuffle"
                case .search: return "search"
                case .normal: return "normal"
                case .favourites: return "favourites"
                case .live: return "live"
                case .recentlyWatched: return "recently-watched"
                case .new: return "new"
                case .popular: return "popular"
                case .searchResultChannelSingle: return "searchResultChannelSingle"
                case .searchResultEpisodeSingle: return "searchResultEpisodeSingle"
                case .searchResultChannelList: return "searchResultChannelList"
                case .searchResultEpisodeList: return "searchResultEpisodeList"
                case .suitable: return "suitable"
                case .allowed: return "allowed"
            }
        }
    }
    
}

@objc public class Color : NSObject {
    @objc class func blueOONAColor() -> UIColor? {
        return UIColor(red: 0.0 / 255.0, green: 120.0 / 255.0, blue: 254.0 / 255.0, alpha: 1)
    }
    
    @objc public class func darkGrayOONAColor() -> UIColor? {
        return UIColor.init(hexString: "0x1c232b")
    }
    
    @objc public class func lighterDarkGrayOONAColor() -> UIColor? {
        return UIColor.init(hexString: "0x28333e")
    }
    
    @objc public class func darkGrayOONAColor(_ alpha: CGFloat) -> UIColor? {
        return UIColor.init(hexString: "0x1c232b")
    }
    
    @objc public class func darkRedColor() -> UIColor {
        return UIColor.init(hexString: "0xe94339")
    }
    
    @objc public class func darkRedColorWithHex() -> UIColor {
        return UIColor.init(hexString: "#e94339")
    }
    
    @objc public class func searchTagColor() -> UIColor? {
        return UIColor.init(hexString: "0x959595")
    }
    
    @objc public class func redOONAColor() -> UIColor? {
        return UIColor(red: 214.0 / 255.0, green: 23.0 / 255.0, blue: 0.0 / 255.0, alpha: 1)
        //
    }
    @objc public class func vodTagColor() -> UIColor? {
        return UIColor(rgb: 0x179183)
        //
    }
    @objc public class func orangeOONAColor() -> UIColor? {
        return UIColor(red: 238, green: 124, blue: 17)
    }
    @objc public class func orangeBidColor() -> UIColor? {
        return UIColor(red: 242, green: 154, blue: 57)
    }
    
    @objc public class func bgOONAColor() -> UIColor? {
        return UIColor(red: 19.0 / 255.0, green: 25.0 / 255.0, blue: 31.0 / 255.0, alpha: 1)
    }
    
    @objc public class func bgHeaderOONAColor() -> UIColor? {
        return UIColor(red: 47.0 / 255.0, green: 54.0 / 255.0, blue: 62.0 / 255.0, alpha: 1)
    }
    
    @objc public class func bgTransparantOONAColor() -> UIColor? {
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.15)
    }
    
    /**for gradient Button
    @objc public class func OONAButtonLeft()-> UIColor {
        return UIColor.init(hexString: "0xd61800")
    }
    
    @objc public class func OONAButtonMid() -> UIColor {
        return UIColor.init(hexString: "0xef6817")
    }
    
    @objc public class func OONAButtonRight() -> UIColor {
        return UIColor.init(hexString: "0xff973e")
    }*/
    

    /**for gradient Button*/
    
    
    @objc public class func BlurryWhiteBackground()-> UIColor {
        return hexStringToUIColor(hex: "#5D5F61")
    }
    
    @objc public class func OONAButtonLeft()-> UIColor {
        return hexStringToUIColor(hex: "#ff973e")
    }
    
    @objc public class func OONAButtonMid() -> UIColor {
        return hexStringToUIColor(hex: "#ef6817")
    }
    
    @objc public class func OONAButtonRight() -> UIColor {
        return hexStringToUIColor(hex: "#d61800")
    }
    
    public class  func getButtonGradient(colors: [CGColor] , view: UIView) -> CAGradientLayer{
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        gradient.colors = colors
        return gradient
    }
    
    @objc public class func gradientColor(on view: UIView?, withColors colors: [Any]?) {
        
        let gradient = CAGradientLayer()
        
        gradient.frame = view?.bounds ?? CGRect.zero
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        view?.layer.insertSublayer(gradient, at: 0)
    }
    
}
extension UIView {
    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        
        var position = layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        layer.position = position
        layer.anchorPoint = point
    }
}

public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:() -> ()) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    
    
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


