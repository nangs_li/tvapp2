//
//  IncomingMessageTextCell.swift
//  OONA
//
//  Created by nicholas on 8/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class IncomingMessageTextCell: MessageBaseCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
