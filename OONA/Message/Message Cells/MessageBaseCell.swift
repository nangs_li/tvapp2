//
//  MessageBaseCell.swift
//  OONA
//
//  Created by nicholas on 9/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class MessageBaseCell: UICollectionViewCell {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.prepareCellUI()
    }
    
    func prepareCellUI() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }
}
