//
//  OutgoingMessageTextCell.swift
//  OONA
//
//  Created by nicholas on 9/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class OutgoingMessageTextCell: MessageBaseCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
