//
//  MessageBaseViewModel.swift
//  OONA
//
//  Created by nicholas on 9/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

enum OONAMessageViewModelType {
    case DefaultMessage
    case IncomingMessageHead
    case IncomingMessageAppend
    case OutgoingMessageHead
    case OutgoingMessageAppend
}

class OONAMessageBaseViewModel: NSObject {
    var messageViewModelType: OONAMessageViewModelType = .DefaultMessage
    var messageText: String?
    
}


