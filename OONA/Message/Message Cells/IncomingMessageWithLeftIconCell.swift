//
//  IncomingMessageWithLeftIconCell.swift
//  OONA
//
//  Created by nicholas on 8/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class IncomingMessageWithLeftIconCell: MessageBaseCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
