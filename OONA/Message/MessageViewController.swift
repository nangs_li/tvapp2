//
//  MessageBaseViewController.swift
//  OONA
//
//  Created by nicholas on 8/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
enum MessageControllerType {
    case Default
    case ChatBot
    case Instant
}


class MessageBaseViewController: BaseViewController {
    lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: 50)
        flowLayout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height), collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .black
        return collectionView
    }()
    
    var controlType: MessageControllerType = .Default { didSet {
        switch controlType {
        case .ChatBot:
            self.prepareForChatBot()
        case .Instant:
            self.prepareForInstantMessage()
        default:break
        }
        }
    }
    
    override func loadView() {
        super.loadView()
        self.setupMessageView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("Super background color \(String(describing: self.view.backgroundColor))")
    }
    
    private func setupMessageView() {
        self.view.addSubview(self.collectionView)
        self.collectionView.snp.makeConstraints { (make) in
            make.top.trailing.leading.bottom.equalToSuperview()
        }
        
    }
    
    func prepareForChatBot() {
        print("Super -> \(#function)")
    }
    
    func prepareForInstantMessage() {
        print("Super -> \(#function)")
    }
}
