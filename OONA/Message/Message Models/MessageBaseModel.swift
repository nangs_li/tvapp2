//
//  MessageBaseModel.swift
//  OONA
//
//  Created by nicholas on 9/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

class MessageBaseModel: NSObject, OONAModel {
    var messageID: Int
    
    
    required init?(json: JSON) {
        if let _id = json["id"].int {
            self.messageID = _id
        } else { return nil }
        super.init()
    }
}
