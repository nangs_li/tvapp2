//
//  ChatBotViewController.swift
//  OONA
//
//  Created by nicholas on 9/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class ChatBotViewController: MessageBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var chatBotMessages: [Any] = []
    
    @IBOutlet weak var chatBotInputView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.controlType = .ChatBot
        self.configureUIs()
        self.prepareForChatBot()
        print("Child background color \(String(describing: self.view.backgroundColor))")
    }
    
    private func configureUIs() {
        let dismissButton = UIButton(type: .system)
        dismissButton.addTarget(self, action: #selector(dismissAction), for: .touchUpInside)
        self.view.insertSubview(dismissButton, belowSubview: self.collectionView)
        dismissButton.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        self.view.bringSubviewToFront(self.chatBotInputView)
    }
    
    override func prepareForChatBot() {
        print("Child -> \(#function)")
        self.collectionView.snp.updateConstraints { (update) in
            update.trailing.equalToSuperview().inset(self.view.frame.width / 2)
        }
        
        self.collectionView.register(UINib(nibName: "IncomingMessageWithLeftIconCell", bundle: nil), forCellWithReuseIdentifier: "IncomingMessageWithLeftIconCell")
        self.collectionView.register(UINib(nibName: "IncomingMessageTextCell", bundle: nil), forCellWithReuseIdentifier: "IncomingMessageTextCell")
        self.collectionView.register(UINib(nibName: "OutgoingMessageTextCell", bundle: nil), forCellWithReuseIdentifier: "OutgoingMessageTextCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.collectionView.snp.updateConstraints { (update) in
//            update.trailing.equalToSuperview().inset(self.view.frame.width / 2)
//            print("Updating Chat Bot CollectionView width")
//            print("Chat bot view width: \(self.view.frame.width)")
//            print("Chat bot view half width: \(self.view.frame.width / 2)")
//            print("Chat bot Collection view width: \(self.collectionView.frame.width)")
//            print("Chat bot Collection view trailing: \(update.trailing)")
//        }
    }
    
    @objc private func dismissAction() {
//    self.dismiss(animated: true, completion: nil)
        if let _ = self.view.superview {
            self.view.removeFromSuperview()
        }
    }
}

extension ChatBotViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatBotMessages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "<#T##String#>", for: indexPath)
        
        return MessageBaseCell()
    }

}
