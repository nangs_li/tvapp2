//
//  SharingViewController.swift
//  OONA
//
//  Created by Jack on 31/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import FacebookShare
import FBSDKShareKit
import TwitterKit
import RxSwift
import RxCocoa

class SharingViewController : UIViewController,FBSDKSharingDelegate{
    
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        self.doneShare()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("sharer didFailWithError maybe not installed FB")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    
    var FBShareActivityIndicator : UIActivityIndicatorView?
    
    @IBOutlet weak var shareMainView: UIView!{
        didSet {
            self.shareMainView.layer.cornerRadius = 15.0
            self.shareMainView.layer.borderColor = UIColor.init(rgb: 0x33383C).cgColor
            self.shareMainView.layer.borderWidth = 1.0
            
        }
    }
    @IBOutlet weak var mainImageView: UIImageView!{
        didSet {
            self.mainImageView.layer.cornerRadius = 15.0
            if (isReferal) {
                /* //Nicholas 5-Sep-2019: Do not use froce unwrap property for optional type as current approach at app launch doesn't guarantee currentUser.profile is not nil
                let url : URL = URL.init(string: UserHelper.shared.currentUser.profile!.inviteImage ?? "")!
                self.mainImageView.sd_setImage(with: url, completed: nil)
                 */
                if let profile = UserHelper.shared.currentUser.profile,
                    let imagePath = profile.inviteImage,
                    let url = URL(string: imagePath) {
                    self.mainImageView.sd_setImage(with: url, completed: nil)
                }
            }else{

                self.mainImageView.sd_setImage(with: URL(string: PlayerHelper.shared.currentVideo?.imageUrl ?? ""), completed: nil)

            }
      
            
        }
    }
    
    @IBOutlet weak var videoNameLabel: UILabel!{
        didSet {
            if (isReferal) {
                self.videoNameLabel.text = "Share to your friends!"
                
            }else{
                self.videoNameLabel.text = PlayerHelper.shared.currentVideo?.channelName
                
            }
        }
    }
    @IBOutlet weak var videoInfoLabel: UILabel!{
        didSet {
            self.videoInfoLabel.text = ""
            
        }
    }
    
    @IBAction func shareIgAction(_ sender: Any) {
        shareByInstagram()
    }
    
    @IBAction func shareFbAction(_ sender: Any) {
        self.shareByFacebook()
        
    }
    @IBAction func shareTwitterAction(_ sender: Any) {
        self.shareByTwitter()
    }
    @IBAction func shareWhatsappAction(_ sender: Any) {
        self.shareByWhatsapp()
    }
    @IBAction func shareOtherAction(_ sender: Any) {
        self.shareToOthers()
    }
    
    override func awakeFromNib() {
         super.awakeFromNib()
        
    }
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        
        ShareManager.sharedManager.dicShared.subscribe(onNext: { [weak self] (applicationName) in
            print("dicShared share mgr sub ",applicationName)
            //            self.playerStartPlaying()
//            guard let _video = video else { return }
//            self?.playVideo(video: _video)
            self?.doneShare()
        }).disposed(by: self.disposeBag)
        
//        PlayerHelper.shared.receivedSnap.subscribe(onNext: { [weak self] (image) in
//            self?.mainImageView.image = image
//            print("share vc receivedsnap")
//        }).disposed(by: self.disposeBag)
//
//        PlayerHelper.shared.requestSnap.accept(true)
    }
    
    
    var isReferal : Bool = false
    func normalShare() {
        isReferal = false
       
        
    }
    func referral() {
        isReferal = true
     
    }
    func shareToActivityVC(image: UIImage, caption: String) {
        
        // Prepare image to share
        let imageShare: Data
        imageShare = image.jpegData(compressionQuality: 1.0)!
        
        
        let activityVC = UIActivityViewController(activityItems: [imageShare,caption], applicationActivities: nil)
        activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
        activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            // 如果錯誤存在，跳出錯誤視窗並顯示給使用者。
            if error != nil {
                self.showAlert(title: "Sorry", message: "Error:\(error!.localizedDescription)")
                return
            }
            
            // 如果發送成功，跳出提示視窗顯示成功。
            if completed {
//                self.showAlert(title: "Success", message: "Share \(self.userName.text!)'s information.")
            }
            
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    func shareToOthers() {
//        self.doneShare()
//        ShareManager.sharedManager.postImageToAllWithCaption(image: self.mainImageView.image!, caption: "OONA!", view: self.view)
        self.shareToActivityVC(image: self.mainImageView.image!, caption: "testing")
    }
    func shareByWhatsapp() {
//        self.doneShare()
        ShareManager.sharedManager.postImageToWhatsappWithCaption(image: self.mainImageView.image!, caption: "OONA!", view: self.view)
    }
    func shareByFacebook() {
        
        let photo = FBSDKSharePhoto()
        photo.image = self.mainImageView.image
        photo.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]

        let dialog = FBSDKShareDialog()
        dialog.shareContent = content
        dialog.fromViewController = self
        dialog.delegate = self
        dialog.mode = .shareSheet
        dialog.show()



        
    }
    func shareByMessenger() {
        
        let photo = FBSDKSharePhoto()
        photo.image = self.mainImageView.image
        photo.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        let dialog = FBSDKShareDialog()
        dialog.shareContent = content
        dialog.fromViewController = self
        dialog.delegate = self
        dialog.mode = .shareSheet
        dialog.show()
        
////        FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//        let urlButton = FBSDKShareMessengerActionButton()
//        urlButton.title = "Visit Facebook"
//        urlButton.url = "https://www.facebook.com"
//
//        let element = FBSDKShareMessengerGenericTemplateElement()
//        element.title = "This is a Cat Picture";
//        element.subtitle = "Look at this cat, wow! Amazing.";
//
//        element.imageURL = URL.init(string: "https://static.pexels.com/photos/126407/pexels-photo-126407.jpeg")
//        element.button = urlButton;
//
//        let content = FBSDKShareMessengerGenericTemplateContent()
//        content.pageID = // Your page ID, required for attribution
//        content.element = element;
//
//        let messageDialog = FBSDKMessageDialog()
//        messageDialog.shareContent = content;
//
//        if messageDialog.canShow {
////            [messageDialog show];
//            messageDialog.show()
//        }
        
        
        
    }
    func shareByTwitter() {
        tweetMain()
    }
    func tweetMain() {
//        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        activityIndicatorView.frame = view.frame
//        activityIndicatorView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//        activityIndicatorView.hidesWhenStopped = true
//        view.addSubview(activityIndicatorView)
//        view.bringSubviewToFront(activityIndicatorView)
//        activityIndicatorView.startAnimating()
        
        // Twitter 認証トークンのチェック
        if TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers() {
            // 認証トークンあり
            tweet()
//            activityIndicatorView.stopAnimating()
        } else {
            // 認証トークンなし
            // Twitterログイン
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                
            
//                activityIndicatorView.stopAnimating()
                if session != nil {
                    // ログイン OK
                    
                    self.tweet()
                } else {
                    // ログイン キャンセル
                    
                    let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in to make Tweet.", preferredStyle: .alert)
                    let yesButton = UIAlertAction(title: "OK", style: .default, handler: { action in
                    })
                    alert.addAction(yesButton)
                    self.present(alert, animated: true)
//                    self.hideShareMenu()
                }
            }
        }
    }
    
    func tweet() {
//        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        activityIndicatorView.frame = view.frame
//        activityIndicatorView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//        activityIndicatorView.hidesWhenStopped = true
//        view.addSubview(activityIndicatorView)
//        view.bringSubviewToFront(activityIndicatorView)
//        activityIndicatorView.startAnimating()
        
        // Twitter投稿データ編集
        let composer = TWTRComposer()
        
        composer.setText("OONA")
        composer.setImage(self.mainImageView.image)
       
        // Twitter投稿画面表示
        composer.show(from: self) { result in
//            shareMenu.hidden = false
            if result == .cancelled {
                // キャンセル
            } else {
                self.doneShare()
//                self.postShareResult("twitter")
            }
//            activityIndicatorView.stopAnimating()
        }
    }
    func shareByInstagram() {
        
        ShareManager.sharedManager.postImageToInstagramWithCaption(imageInstagram: self.mainImageView.image!, instagramCaption: "OONA!", view: self.view)

        
        
        
//        let image = self.mainImageView.image!
//        let instagramURL : URL = URL.init(string: "instagram://app")!
//        if UIApplication.shared.canOpenURL(instagramURL) {
//            let saveImagePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("instagram.png")
//
//            let urlToStore : URL = saveImagePath!
//            let imageData = image.pngData()
//            do {
//                try imageData!.write(to: urlToStore)
//            } catch {
//                print("Instagram sharing error")
//            }
//
//            let documentInteractionController = UIDocumentInteractionController.init()
//            documentInteractionController.url = urlToStore
//            documentInteractionController.annotation = ["InstagramCaption" : "Testing"]
////            documentInteractionController.uti = "com.instagram.exclusivegram"
//
//
//            if !documentInteractionController.presentOpenInMenu(from: CGRect.init(x: 1, y: 1, width: 1, height: 1), in: self.view, animated: true) {
//                print("Instagram not found")
//            }
//        }
//        else {
//            print("Instagram not found")
//        }
    }
    func doneShare() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.exit()
        }
    }
    func exit() {
        self.dismiss(animated: true, completion: nil)
        PlayerHelper.shared.resumePlayer()
    }
    @IBAction func dismissButton(_ sender: Any) {
        self.exit()
    }
    
}
