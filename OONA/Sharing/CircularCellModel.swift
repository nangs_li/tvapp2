//
//  CircularCellModel.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation

struct CircularCellModel {
    var id : String = ""
    var name : String = ""
    var redInColor : Bool = false
    init(){
    }
    
    init(id: String, name: String){
        self.id = id
        self.name = name
        self.redInColor = false
    }
    init(id: String, name: String, redInColor:Bool){
        self.id = id
        self.name = name
        self.redInColor = true
    }
}
