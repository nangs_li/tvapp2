//
//  IGMgr.swift
//  OONA
//
//  Created by Jack on 1/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
//let documentInteractionController = UIDocumentInteractionController()
class ShareManager: NSObject, UIDocumentInteractionControllerDelegate {
    
    var dicShared: PublishRelay<String?> = PublishRelay<String?>()
    
    private let documentInteractionController = UIDocumentInteractionController()
    private let kInstagramURL = "instagram://"
    private let kIgUTI = "com.instagram.exclusivegram"
    private let kIgfileNameExtension = "instagram.igo"
    private let kAlertViewTitle = "Sorry"
    private let kIgAlertViewMessage = "Please install or login the Instagram application"
    
    private let kWhatsappURL = "instagram://"
    private let kWhatsappUTI = "public.image"
    private let kWhatsappfileNameExtension = "image.wai"
    private let kWhatsappAlertViewMessage = "Please install or login the Whatsapp application"
    
    private let kUniversalUTI = "public.data, public.content"
    private let kUniversalfileNameExtension = "image.jpg"
    // singleton manager
    class var sharedManager: ShareManager {
        struct Singleton {
            static let instance = ShareManager()
        }
        return Singleton.instance
    }
   
    func postImageToWhatsappWithCaption(image: UIImage, caption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        let instagramURL = NSURL(string: kWhatsappURL)
        if UIApplication.shared.canOpenURL(instagramURL! as URL) {
            let jpgPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(kWhatsappfileNameExtension)
            
            do {
                try image.jpegData(compressionQuality: 1)?.write(to: URL(fileURLWithPath: jpgPath), options: .atomic)
            } catch {
                print(error)
            }
            
            let rect = CGRect.zero
            let fileURL = NSURL.fileURL(withPath: jpgPath)
            
            
            documentInteractionController.url = fileURL
            documentInteractionController.delegate = self
            documentInteractionController.uti = kWhatsappUTI
            
            // adding caption for the image
//            documentInteractionController.annotation = ["InstagramCaption": caption]
            documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
        }
        else {
            
            // alert displayed when the instagram application is not available in the device
            UIAlertView(title: kAlertViewTitle, message: kWhatsappAlertViewMessage, delegate:nil, cancelButtonTitle:"OK").show()
        }
    }
    func postImageToInstagramWithCaption(imageInstagram: UIImage, instagramCaption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        let url = NSURL(string: kInstagramURL)
        if UIApplication.shared.canOpenURL(url! as URL) {
            let jpgPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(kIgfileNameExtension)
            
            do {
                try imageInstagram.jpegData(compressionQuality: 1)?.write(to: URL(fileURLWithPath: jpgPath), options: .atomic)
            } catch {
                print(error)
            }
            
            let rect = CGRect.zero
            let fileURL = NSURL.fileURL(withPath: jpgPath)
            
            
            documentInteractionController.url = fileURL
            documentInteractionController.delegate = self
            documentInteractionController.uti = kIgUTI
            
            // adding caption for the image
            documentInteractionController.annotation = ["InstagramCaption": instagramCaption]
            documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
        }
        else {
            
            // alert displayed when the instagram application is not available in the device
            UIAlertView(title: kAlertViewTitle, message: kIgAlertViewMessage, delegate:nil, cancelButtonTitle:"OK").show()
        }
    }
    func postImageToAllWithCaption(image: UIImage, caption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
       
            let jpgPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(kUniversalfileNameExtension)
            
            do {
                try image.jpegData(compressionQuality: 1)?.write(to: URL(fileURLWithPath: jpgPath), options: .atomic)
            } catch {
                print(error)
            }
            
            let rect = CGRect.zero
            let fileURL = NSURL.fileURL(withPath: jpgPath)
            
            
            documentInteractionController.url = fileURL
            documentInteractionController.delegate = self
            documentInteractionController.uti = kUniversalUTI
            
            // adding caption for the image
            //            documentInteractionController.annotation = ["InstagramCaption": caption]
            documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
       
    }
    func documentInteractionControllerDidDismissOptionsMenu(_ controller: UIDocumentInteractionController) {
        print("finish share")
    }
    func documentInteractionController(_ controller: UIDocumentInteractionController, didEndSendingToApplication application: String?) {
        print("application",application)
        ShareManager.sharedManager.dicShared.accept(application)
    }
}
