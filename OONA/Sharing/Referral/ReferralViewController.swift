//
//  ReferralViewController.swift
//  OONA
//
//  Created by nicholas on 9/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxLocalizer


class ReferralViewController: BaseViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var shareToFriendImageView: UIImageView!
    @IBOutlet weak var inputCodeImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        containerView.layer.cornerRadius = 13
        containerView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.view.alpha = 1
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.view.alpha = 0
        })
    }
    
    @IBAction func shareCodeTapAction(_ sender: Any) {
        let vc : ReferralShareViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralShareViewController") as! ReferralShareViewController
        vc.referral()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        
        PlayerHelper.shared.resumePlayer()
        self.dismiss(animated: true) {
            PlayerHelper.shared.presentVC(vc: vc)
        }
    }
    
    @IBAction func enterCodeTapAction(_ sender: Any) {
        
//        if let parentInviteCode = UserHelper.shared.currentUserProfile()?.parentInviteCode {
//            self.showSystemDialog(text: ("You had already entered the SPONSOR CODE! Your Sponsor: " + parentInviteCode), dismissDelay: 5)
//        }else{
            goToEnterCode()
//        }
        
    }
    func goToEnterCode() {
        
        let yourInfovc : ReferralEnterViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralEnterViewController") as! ReferralEnterViewController
        //yourInfovc.view.backgroundColor = .clear
        yourInfovc.referCode()
        yourInfovc.modalPresentationStyle = .overCurrentContext
        yourInfovc.modalTransitionStyle = .crossDissolve
        
        PlayerHelper.shared.resumePlayer()
        self.dismiss(animated: true) {
            PlayerHelper.shared.presentVC(vc: yourInfovc)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissArea(_ sender: Any) {
//        PlayerHelper.shared.resumePlayer()
        self.exit()
    }
    
    override func appLanguageDidUpdate() {
        if let _localizedImage = localizedImage("sponsor-page") {
            self.backdropImageView.image = _localizedImage
        }
        
        if let _localizedImage = localizedImage("share-my-code") {
            self.shareToFriendImageView.image = _localizedImage
        }
        
        if let _localizedImage = localizedImage("enter-code") {
            self.inputCodeImageView.image = _localizedImage
        }
        
//        self.backdropImageView.image = localizedImage("sponsor-page")
//        self.shareToFriendImageView.image = localizedImage("share-my-code")
//        self.inputCodeImageView.image = localizedImage("enter-code")
    }
    
    func localizedImage(_ imageName: String) -> UIImage? {
        var fullImageName: String = imageName
        switch LanguageHelper.shared.getAppLanguage() {
        case .english:
            fullImageName = "\(fullImageName)-\(Language.english.rawValue)"
        case .indonesian:
            fullImageName = "\(fullImageName)-\(Language.indonesian.rawValue)"
        default: break
        }
        return UIImage(named: fullImageName)
    }
    
    func exit() {
        self.dismiss(animated: true) {
            // do something on completion
        }
    }
}
