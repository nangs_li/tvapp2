//
//  ReferralShareViewController.swift
//  OONA
//
//  Created by Jack on 31/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import FacebookShare
import FBSDKShareKit
import TwitterKit
import RxSwift
import RxCocoa
import JGProgressHUD
extension String {
    func makeHTMLfriendly() -> String {
        var finalString = ""
        for char in self {
            for scalar in String(char).unicodeScalars {
                finalString.append("&#\(scalar.value)")
            }
        }
        return finalString
    }
}


class ReferralShareViewController : BaseViewController,FBSDKSharingDelegate{
    
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        self.doneShare()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("sharer didFailWithError maybe not installed FB")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    
    var FBShareActivityIndicator : UIActivityIndicatorView?
    var imageToShare : UIImage = UIImage()
    @IBAction func shareIgAction(_ sender: Any) {
        shareByInstagram()
    }
    
    @IBAction func shareFbAction(_ sender: Any) {
//        self.shareByFacebook()
        UIPasteboard.general.string = UserHelper.shared.currentUserProfile()?.inviteMessage?.makeHTMLfriendly()
        
        self.showSystemDialog(text: "Sponsor code copied! Please paste your code in your caption.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.hideSystemDialog()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.shareByFacebook()
            })
            
        }
    }
    @IBAction func shareTwitterAction(_ sender: Any) {
        self.shareByTwitter()
    }
    @IBAction func shareWhatsappAction(_ sender: Any) {
//        self.shareByWhatsapp()
        UIPasteboard.general.string = UserHelper.shared.currentUserProfile()?.inviteMessage
//        self.showSystemDialog(text: "Referral code copied! Please paste your code in your caption.")
        let hud = JGProgressHUD(style: .dark)
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.textLabel.text = "Sponsor code copied! Please paste your code in your caption."
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 3.0)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.hideSystemDialog()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.shareToOthers()
            })
            
        }
        
    }
    @IBAction func shareOtherAction(_ sender: Any) {
        self.shareToOthers()
    }
    
    override func awakeFromNib() {
         super.awakeFromNib()
        
    }
    
    @IBOutlet weak var referMainView: UIView!
    
//    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        self.referMainView.layer.cornerRadius = 15.0
//        self.referMainView.layer.borderColor = UIColor.init(rgb: 0x33383C).cgColor
//        self.referMainView.layer.borderWidth = 1.0
        if let profile = UserHelper.shared.currentUser.profile,
            let imagePath = profile.inviteImage,
            let url = URL(string: imagePath) {
            
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            
                            self?.imageToShare = image
                        }
                    }else{
                        // server fail to get image.
                        
                    }
                }
            }
            
        }
        self.referMainView.layer.masksToBounds = true
        ShareManager.sharedManager.dicShared.subscribe(onNext: { [weak self] (applicationName) in
            print("dicShared share mgr sub ",applicationName)
            //            self.playerStartPlaying()
//            guard let _video = video else { return }
//            self?.playVideo(video: _video)
            
            self?.doneShare()
        }).disposed(by: self.disposeBag)
        
//        PlayerHelper.shared.receivedSnap.subscribe(onNext: { [weak self] (image) in
//            self?.mainImageView.image = image
//            print("share vc receivedsnap")
//        }).disposed(by: self.disposeBag)
//
//        PlayerHelper.shared.requestSnap.accept(true)
    }
//    var noNeedResume : Bool = false
    
    var isReferal : Bool = false
    func normalShare() {
        isReferal = false
       
        
    }
    func referral() {
        isReferal = true
     
    }
    func shareToActivityVC(image: UIImage, caption: String) {
        
        // Prepare image to share
        let imageShare: Data
        imageShare = image.jpegData(compressionQuality: 1.0)!
        
        
        let activityVC = UIActivityViewController(activityItems: [imageShare], applicationActivities: nil)
        activityVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
        activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            // 如果錯誤存在，跳出錯誤視窗並顯示給使用者。
            if error != nil {
                self.showAlert(title: "Sorry", message: "Error:\(error!.localizedDescription)")
                return
            }
            
            // 如果發送成功，跳出提示視窗顯示成功。
            if completed {
                self.doneShare()
//                self.showAlert(title: "Success", message: "Share \(self.userName.text!)'s information.")
            }
            
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    func shareToOthers() {
//        self.doneShare()
//        ShareManager.sharedManager.postImageToAllWithCaption(image: self.mainImageView.image!, caption: "OONA!", view: self.view)
        self.shareToActivityVC(image: self.imageToShare, caption: "OONA")
    }
    func shareByWhatsapp() {
//        self.doneShare()
        ShareManager.sharedManager.postImageToWhatsappWithCaption(image: self.imageToShare, caption: "OONA!", view: self.view)
    }
    func shareByFacebook() {
        
        let photo = FBSDKSharePhoto()
        photo.image = self.imageToShare
        photo.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]

        let dialog = FBSDKShareDialog()
        dialog.shareContent = content
        dialog.fromViewController = self
        dialog.delegate = self
        dialog.mode = .shareSheet
        dialog.show()



        
    }
    func shareByMessenger() {
        
        let photo = FBSDKSharePhoto()
        photo.image = self.imageToShare
        photo.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        let dialog = FBSDKShareDialog()
        dialog.shareContent = content
        dialog.fromViewController = self
        dialog.delegate = self
        dialog.mode = .shareSheet
        dialog.show()
        
////        FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//        let urlButton = FBSDKShareMessengerActionButton()
//        urlButton.title = "Visit Facebook"
//        urlButton.url = "https://www.facebook.com"
//
//        let element = FBSDKShareMessengerGenericTemplateElement()
//        element.title = "This is a Cat Picture";
//        element.subtitle = "Look at this cat, wow! Amazing.";
//
//        element.imageURL = URL.init(string: "https://static.pexels.com/photos/126407/pexels-photo-126407.jpeg")
//        element.button = urlButton;
//
//        let content = FBSDKShareMessengerGenericTemplateContent()
//        content.pageID = // Your page ID, required for attribution
//        content.element = element;
//
//        let messageDialog = FBSDKMessageDialog()
//        messageDialog.shareContent = content;
//
//        if messageDialog.canShow {
////            [messageDialog show];
//            messageDialog.show()
//        }
        
        
        
    }
    func shareByTwitter() {
        tweetMain()
    }
    func tweetMain() {
//        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        activityIndicatorView.frame = view.frame
//        activityIndicatorView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//        activityIndicatorView.hidesWhenStopped = true
//        view.addSubview(activityIndicatorView)
//        view.bringSubviewToFront(activityIndicatorView)
//        activityIndicatorView.startAnimating()
        
        // Twitter 認証トークンのチェック
        if TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers() {
            // 認証トークンあり
            tweet()
//            activityIndicatorView.stopAnimating()
        } else {
            // 認証トークンなし
            // Twitterログイン
            TWTRTwitter.sharedInstance().logIn { (session, error) in
                
            
//                activityIndicatorView.stopAnimating()
                if session != nil {
                    // ログイン OK
                    
                    self.tweet()
                } else {
                    // ログイン キャンセル
                    
                    let alert = UIAlertController(title: "No Twitter Accounts Available", message: "You must log in to make Tweet.", preferredStyle: .alert)
                    let yesButton = UIAlertAction(title: "OK", style: .default, handler: { action in
                    })
                    alert.addAction(yesButton)
                    self.present(alert, animated: true)
//                    self.hideShareMenu()
                }
            }
        }
    }
    
    func tweet() {
//        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
//        activityIndicatorView.frame = view.frame
//        activityIndicatorView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//        activityIndicatorView.hidesWhenStopped = true
//        view.addSubview(activityIndicatorView)
//        view.bringSubviewToFront(activityIndicatorView)
//        activityIndicatorView.startAnimating()
        
        // Twitter投稿データ編集
        let composer = TWTRComposer()
        
        composer.setText("OONA")
        composer.setImage(self.imageToShare)
       
        // Twitter投稿画面表示
        composer.show(from: self) { result in
//            shareMenu.hidden = false
            if result == .cancelled {
                // キャンセル
            } else {
                self.doneShare()
//                self.postShareResult("twitter")
            }
//            activityIndicatorView.stopAnimating()
        }
    }
    func shareByInstagram() {
        
        ShareManager.sharedManager.postImageToInstagramWithCaption(imageInstagram: self.imageToShare, instagramCaption: "OONA!", view: self.view)

        
        
        
//        let image = self.mainImageView.image!
//        let instagramURL : URL = URL.init(string: "instagram://app")!
//        if UIApplication.shared.canOpenURL(instagramURL) {
//            let saveImagePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("instagram.png")
//
//            let urlToStore : URL = saveImagePath!
//            let imageData = image.pngData()
//            do {
//                try imageData!.write(to: urlToStore)
//            } catch {
//                print("Instagram sharing error")
//            }
//
//            let documentInteractionController = UIDocumentInteractionController.init()
//            documentInteractionController.url = urlToStore
//            documentInteractionController.annotation = ["InstagramCaption" : "Testing"]
////            documentInteractionController.uti = "com.instagram.exclusivegram"
//
//
//            if !documentInteractionController.presentOpenInMenu(from: CGRect.init(x: 1, y: 1, width: 1, height: 1), in: self.view, animated: true) {
//                print("Instagram not found")
//            }
//        }
//        else {
//            print("Instagram not found")
//        }
    }
    func doneShare() {
        let hud = JGProgressHUD(style: .dark)
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.textLabel.text = "Completed!"
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 3.0)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            self.exit()
        }
    }
    func exit() {
        self.dismiss(animated: true, completion: nil)
//        if noNeedResume {
//            PlayerHelper.shared.resumePlayer()
//        }
        
    }
    
    @IBAction func dismissButton(_ sender: Any) {
        PlayerHelper.shared.resumePlayer()
        self.exit()
    }
}
