//
//  NoticeViewController.swift
//  OONA
//
//  Created by nicholas on 9/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxLocalizer
import SwiftyJSON

class NoticeViewController: BaseViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backdropImageView: UIImageView!
    var actionString : String = ""
    var imgURL : URL?
    
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBAction func clickAction(_ sender: Any) {
        self.exit()
        DeviceHelper.shared.action(by: URL.init(string: actionString))
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
//        containerView.layer.cornerRadius = 13
//        containerView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    func showNotice(notice:JSON) {
        
//        notice" : [
//        {
//        "id" : XX,
//        "imageUrl": "https://xxx.xxx.com/xxx.jpg",
//        "action" : "oona://bids"
//        }
//        ],

        self.actionString = notice["action"].stringValue
        self.imgURL = URL.init(string: notice["imageUrl"].stringValue)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backdropImageView.sd_setImage(with: self.imgURL, completed: nil)
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.view.alpha = 1
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIView.animate(withDuration: 0.3,
                       animations: {
            self.view.alpha = 0
        })
    }
    
    @IBAction func dismissArea(_ sender: Any) {
        print("dismiss Area")
//        PlayerHelper.shared.resumePlayer()
        self.exit()
    }
    
    func exit() {
        self.dismiss(animated: true) {
            // do something on completion
        }
    }
}
