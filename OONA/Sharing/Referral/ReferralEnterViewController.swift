//
//  ReferralEnterViewController.swift
//  OONA
//
//  Created by Jack on 2/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import  UIKit
import SwiftyJSON
class ReferralEnterViewController : BaseViewController  {
    
    @IBOutlet weak var inputField: UITextField!
    
    var isReferCode = false
    var isTransferCode = false
    var isInviteCode = false
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var submitButton: GradientButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func viewDidLoad () {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        self.backButton.isHidden = false
        
        if isReferCode {
            questionLabel.text = LanguageHelper.localizedString("input_oona_code_msg") // "Cool,\nEnter the OONA code in the box below."
            self.backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(), for: .normal)
        }
        if isTransferCode {
            questionLabel.text = LanguageHelper.localizedString("input_transfer_code_msg")
            self.backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(), for: .normal)
            //"Cool,\nEnter your transfer code in the box below."
        }
        if isInviteCode {
            questionLabel.text = LanguageHelper.localizedString("hi_oona_tv_is_not_avilable_yet_in_your_country_text")// "Hi,\nOONA TV is not available yet in your country.\nIf you have a referral code, input to unlock."
            self.backButton.isHidden = true
        }
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            //            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 12)!
        ]
        
        inputField.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("input_code_text"), attributes:attributes)
        
        
        //        self.updateUserViews()
        //        self.refreshUserDatas()
    }
    
    @IBAction func backToMenuAction(_ sender: Any) {
        PlayerHelper.shared.resumePlayer()
        self.exitPage()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if inputField.text?.count ?? 0 > 0{
            if (isReferCode) {
                self.submitReferCode()
            }
            if (isTransferCode) {
                self.submitTransferCode()
            }
            if (isInviteCode) {
                self.submitInviteCode()
            }
            self.view.endEditing(true)
        }
    }
    
    func transferCode() { isTransferCode = true }
    
    func referCode() { isReferCode = true }
    
    func inviteCode() { isInviteCode = true }
    
    func submitTransferCode() {
        let param : [String : String] = ["code" : inputField.text ?? ""]
        //        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)
        OONAApi.shared.postRequest(url: APPURL.TransferCodeEnter, parameter: param)
            .responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let jsonDict : NSDictionary = response.result.value as? NSDictionary {
                        let code : Int? = jsonDict["code"] as? Int
                        let status : String? = jsonDict["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                
                                if(_success){
                                 
                                     let json = JSON(response.result.value)
                                    self.exitPage()
                                    
                                    UserHelper.shared.getUserProfile(success: { (profile) in
                                        
                                    }, failure: { (error, string) in
                                        
                                    })
                                    if let points = json["reward"]["points"].int {
                                        if points == 1000 {
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                PlayerHelper.shared.showRewardVideo.accept(.Award1000)
                                            })
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 5.5, execute: {
                                                PlayerHelper.shared.showHTMLSystemMessage.accept(json["reward"]["message"].stringValue)
//                                                self.showSystemDialog(text: json["reward"]["message"].stringValue, dismissDelay: 5)
                                            })
                                        }
                                        if points == 10000 {
                                                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                                                        PlayerHelper.shared.showRewardVideo.accept(.Award10000)
                                                                                    })
                                                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.5, execute: {
                                                                                        PlayerHelper.shared.showHTMLSystemMessage.accept(json["reward"]["message"].stringValue)
                                        //                                                self.showSystemDialog(text: json["reward"]["message"].stringValue, dismissDelay: 5)
                                                                                    })
                                                                                }
                                    }
                                    
                                }else{
                                    if let _message = message{
                                        //                                            self.showAlert(title: "",message: _message)
                                        self.showErrorMessage(message: _message)
                                    }
                                }
                            })
                        }
                    }
                }
        }
    }
    
    func submitInviteCode() {
        
        let param : [String : String] = ["inviteCode" : inputField.text ?? ""]
        //        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)
        OONAApi.shared.postRequest(url: APPURL.inviteCodeEnter, parameter: param)
            .responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let jsonDict : NSDictionary = response.result.value as? NSDictionary {
                        let code : Int? = jsonDict["code"] as? Int
                        let status : String? = jsonDict["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    
                                    self.exitPage()
                                    let json = JSON(response.result.value)
                                    let userRegion = json["userRegion"].stringValue
                                    let apiDomain = json["apiDomain"].stringValue
                                    print ("check region",apiDomain,userRegion)
                                    
                                    
                                    OONAApi.shared.setDomain(url: apiDomain, region: userRegion)
                                    UserDefaults.standard.set(userRegion, forKey: "user-region")
                                    UserDefaults.standard.set(apiDomain, forKey: "user-apiUrl")
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                        DeviceHelper.shared.inviteCodeEntered.accept(true)
                                    })
                                    
                                    //                                    self.showAlert(title: "",message: "Your password has been changed.")
                                    
                                }else{
                                    if (code == 2022){
                                        //[self showErrorWithMessage:str(@"resend_verify_code")];
                                        //                                        self.showAlert(title: "",message:  Localizer.shared.localized("resend_verify_code"))
                                        
                                    }else{
                                        if let _message = message{
                                            //                                            self.showAlert(title: "",message: _message)
                                             self.showErrorMessage(message: _message)
                                        }
                                    }
                                }
                            })
                        }
                    }
                }else{
//                    print("")
                }
        }
    }
    
    func submitReferCode() {
        
        let param : [String : String] = ["referralCode" : inputField.text ?? ""]
        //        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)
        OONAApi.shared.postRequest(url: APPURL.ReferCodeEnter, parameter: param)
            .responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let jsonDict : NSDictionary = response.result.value as? NSDictionary {
                        let code : Int? = jsonDict["code"] as? Int
                        let status : String? = jsonDict["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    
                                    
                                    
                                    let json = JSON(response.result.value)
                                        if let userRegion = json["userRegion"].string,
                                            let apiDomain = json["apiDomain"].string {
                                        print ("check region",apiDomain,userRegion)
                                        
                                        OONAApi.shared.setDomain(url: apiDomain, region: userRegion)
                                        UserDefaults.standard.set(userRegion, forKey: "user-region")
                                        UserDefaults.standard.set(apiDomain, forKey: "user-apiUrl")
                                        
                                        let popup = CustomPopup(message: "You had entered an Invite code!")
                                        //    CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:message AndMessage:message];
                                        //    popup.button2.hidden = YES;
                                        popup?.button1.setTitle("Restart Now", for: .normal)
                                        //    CGRect buttonFrame = popup.button1.frame;
                                        //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
                                        //    popup.button1.frame = buttonFrame;
                                        let popupBlock = popup
                                        //    popup.backBtn. hidden  = YES;
                                        popup?.setupOneButton()
                                        popup?.completionButton1 = {
                                            UserHelper.shared.cleanUserData()
                                            exit(0)
                                        }
                                        popup?.alpha = 0
                                        let window = UIApplication.shared.keyWindow!
                                        window.addSubview(popup!) // Add your view here
                                        
                                        
                                        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                                            popup?.alpha = 1
                                        })
                                            
                                        }else{
//                                            self.tcoinRewardVideo(rewardAmount: .Award1000)
                                            self.exitPage()
                                            UserHelper.shared.getUserProfile(success: { (profile) in
                                                
                                            }, failure: { (error, string) in
                                                
                                            })
                                            if let points = json["reward"]["points"].int {
                                                if points == 1000 {
                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                        PlayerHelper.shared.showRewardVideo.accept(.Award1000)
                                                    })
                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.5, execute: {
                                                        self.showSystemDialog(text: json["reward"]["message"].stringValue, dismissDelay: 5)
                                                    })
                                                }
                                                if points == 10000 {
                                                                                                                                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                                                                                                       PlayerHelper.shared.showRewardVideo.accept(.Award10000)
                                                                                                                                   })
                                                                                                                                   DispatchQueue.main.asyncAfter(deadline: .now() + 5.5, execute: {
                                                                                                                                       PlayerHelper.shared.showHTMLSystemMessage.accept(json["reward"]["message"].stringValue)
                                                                                       //                                                self.showSystemDialog(text: json["reward"]["message"].stringValue, dismissDelay: 5)
                                                                                                                                   })
                                                                                                                               }
                                            }
                                           
                                    }
                                }else{
                                    if (code == 2022){
                                        //[self showErrorWithMessage:str(@"resend_verify_code")];
                                        //                                        self.showAlert(title: "",message:  Localizer.shared.localized("resend_verify_code"))
                                        
                                    }else{
                                        if let _message = message{
                                            //                                            self.showAlert(title: "",message: _message)
                                            self.showErrorMessage(message: _message)
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
        }
    }
    func showErrorMessage(message:String) {
        let popup = CustomPopup(message: message)
        //    CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:message AndMessage:message];
        //    popup.button2.hidden = YES;
        popup?.button1.setTitle(LanguageHelper.localizedString("ok"), for: .normal)
        //    CGRect buttonFrame = popup.button1.frame;
        //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
        //    popup.button1.frame = buttonFrame;
        let popupBlock = popup
        //    popup.backBtn. hidden  = YES;
        popup?.setupOneButton()
        popup?.completionButton1 = {
            popupBlock?.removeFromSuperview()
        }
        popup?.alpha = 0
        let window = UIApplication.shared.keyWindow!
        window.addSubview(popup!) // Add your view here
        
    }
    
    override func appLanguageDidUpdate() {
        if isReferCode {
            questionLabel.text = LanguageHelper.localizedString("input_oona_code_msg") // "Cool,\nEnter the OONA code in the box below."
            self.backButton.setTitle(LanguageHelper.localizedString("back_text"), for: .normal)
        }
        if isTransferCode {
            questionLabel.text = LanguageHelper.localizedString("input_transfer_code_msg") //"Cool,\nEnter your transfer code in the box below."
        }
        if isInviteCode {
            questionLabel.text = LanguageHelper.localizedString("hi_oona_tv_is_not_avilable_yet_in_your_country_text")// "Hi,\nOONA TV is not available yet in your country.\nIf you have a referral code, input to unlock."
        }
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray
        ]
        self.inputField.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("input_code_text"), attributes:attributes)
        
        self.submitButton.setTitle(LanguageHelper.localizedString("submit_text"), for: .normal)
    }
    
    func exitPage() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
