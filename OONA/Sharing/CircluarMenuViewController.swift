//
//  CircluarMenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import InfiniteLayout
import GhostTypewriter

class CircluarMenuViewController: BaseViewController{
    typealias circularMenuSelectBlock = (_ result :Any?) ->()
    var completionBlock:circularMenuSelectBlock?
    
    @IBOutlet weak var infiniteCollectionView: InfiniteCollectionView!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    /*tutorial*/
    @IBOutlet weak var swipePic: UIImageView!
    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var tutorialText: TypewriterLabel!
    
    @IBOutlet weak var channelInfoView: UIView!
    @IBOutlet weak var bgBlurView: UIView!
    
    @IBOutlet weak var circularChannelName: UILabel!
    @IBOutlet weak var circularChannelInfo: UILabel!
    
    public let collectionViewWidth : CGFloat = 275
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infiniteCollectionView.register(UINib(nibName: "CircularCell", bundle: nil), forCellWithReuseIdentifier: "CircularCell")
    }
    public func setBlurHidden(hidden:Bool) {
        self.bgBlurView.isHidden = hidden
    }
    
//    override func showTutorial(){
        //tutorialView.isHidden = false
        //swipePic.animate(withGIFNamed: "gesture_swipe-up")
        /*tutorialText.startTypewritingAnimation(completion:{
            guard let cb = self.completionBlock else {return}
            cb(nil)
        })*/
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadViewIfNeeded()
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [], animations: {
            self.channelInfoView.alpha = 1
            self.rightConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
        self.circularChannelName.text = PlayerHelper.shared.currentVideo?.videoName ?? ""
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func clickBackgroundButton(_ sender: UIButton) {
        self.exit()
    }
    func itemSelected(tag:Int) {
        print("roll selected",(tag % 3))
        let selectedTag = (tag % 3)
        switch selectedTag {
//        case 0 :
//            // chatbot
//            self.chatbot()
//            print("selectedTag",selectedTag)
        case 0 :
            print("selectedTag",selectedTag)
            self.refer()
        case 1 :
            print("selectedTag",selectedTag)
            // Share
            self.share()
            
        case 2 :
            // tcoin
            self.tcoin()
            print("selectedTag",selectedTag)
        default :
            print("selectedTag",selectedTag)
        }
    }
    func chatbot() {
        self.exit()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            
            PlayerHelper.shared.showChatbotView()
        }
    }
    func tcoin() {
       self.exit()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            let vc : TCoinsDetailViewController = self.CreateTcoinsDetailPage()
            PlayerHelper.shared.presentVC(vc: vc)
        }
        
    }
    func refer() {
//        if (UserHelper.shared.isLogin()){
        let vc : ReferralViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateInitialViewController() as! ReferralViewController
            self.exit()
//            vc.referral()
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                
                PlayerHelper.shared.presentVC(vc: vc)
                
            }
//        }else{
//            PlayerHelper.shared.pausePlayer()
//            self.showLogin {[weak helper = PlayerHelper.shared] in
//                helper?.resumePlayer()
//            }
//        }
    }
    
    func share() {
        let vc : SharingViewController = UIStoryboard(name: "Sharing", bundle: nil).instantiateInitialViewController() as! SharingViewController
        self.exit()
        vc.normalShare()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            
            PlayerHelper.shared.presentVC(vc: vc)
//            PlayerHelper.shared.requestSnap.accept(true)
        }
    }
    
    func exit() {
        PlayerHelper.shared.resetYTView()
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
            self.channelInfoView.alpha = 0
            self.rightConstraint.constant = 0 - self.collectionViewWidth
            self.view.layoutIfNeeded()
            
        }, completion: { (finished: Bool) in
            self.dismiss(animated: true)
        })
    }
    override func appLanguageDidUpdate() {
        self.infiniteCollectionView.reloadData()
    }
    
    func createCellModel() -> Array<CircularCellModel> {
        return [CircularCellModel.init(id: "referral", name: LanguageHelper.localizedString("right_menu_refer"), redInColor:true),
                CircularCellModel.init(id: "share", name: LanguageHelper.localizedString("right_menu_share")),
                CircularCellModel.init(id: "tcoin", name: LanguageHelper.localizedString("right_menu_tcoins"))]
    }
}


extension CircluarMenuViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 18
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CircularCell", for: indexPath) as! CircularCell
        cell.circularModel = self.createCellModel()
        cell.setup(index: self.infiniteCollectionView!.indexPath(from: indexPath).row)
        
        return cell
    }
    
}

extension CircluarMenuViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CircularCell", for: indexPath) as! CircularCell
        self.itemSelected(tag: indexPath.row)
//         circularModel : [CircularCellModel] = [CircularCellModel.init(id: "chat", name: "Chatbot"),CircularCellModel.init(id: "referral", name: "Refer OONA"),CircularCellModel.init(id: "share", name: "Share"),CircularCellModel.init(id: "screencap", name: "Snap"),CircularCellModel.init(id: "tcoin", name: "tcoins")]
    }
}

extension CircluarMenuViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 157, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView.frame.height
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
}
