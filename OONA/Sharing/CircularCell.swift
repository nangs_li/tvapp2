//
//  CircularCell.swift
//
//  Created by Vick hungon 5/7/2019.
//  Copyright © 2019 Vick Hung. All rights reserved.
//

import UIKit

class CircularCell: UICollectionViewCell {
    
    public var circularModel : [CircularCellModel] = [CircularCellModel.init(id: "referral", name: LanguageHelper.localizedString("right_menu_refer"), redInColor:true),
                                                      CircularCellModel.init(id: "share", name: LanguageHelper.localizedString("right_menu_share")),
                                                      CircularCellModel.init(id: "tcoin", name: LanguageHelper.localizedString("right_menu_tcoins"))]
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func setup(index: Int) {
        let ctag = (index % circularModel.count)
        let _circularModel = self.circularModel[index % circularModel.count]
        self.titleLabel.text = _circularModel.name
        self.tag = ctag
        self.imageView.image = UIImage(named: _circularModel.id)
        
        if (_circularModel.redInColor) {
            self.titleLabel.textColor = Color.redOONAColor()
        }else{
           self.titleLabel.textColor = .white
        }
    }
}
/*
enum CellStyle {
    case circular
    case `default`
    
    static let all = [circular, `default`]
}

@IBDesignable class CellView: UIView {
    
    let sharingChannel : [SharingChannel] = [SharingChannel.init(id: "chatbot", name: "Chatbot"),SharingChannel.init(id: "refer", name: "Refer OONA"),SharingChannel.init(id: "share", name: "Share"),SharingChannel.init(id: "Snap", name: "Screenshot"),SharingChannel.init(id: "tcoins", name: "tcoins")]
    
    let titleLabel = UILabel()
    let imageView =  UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    
    //let colors = [#colorLiteral(red: 0.5254901961, green: 0.6901960784, blue: 0.9137254902, alpha: 1), #colorLiteral(red: 0.5254901961, green: 0.6196078431, blue: 0.9137254902, alpha: 1), #colorLiteral(red: 0.6078431373, green: 0.5254901961, blue: 0.9137254902, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.5254901961, blue: 0.8392156863, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.5254901961, blue: 0.6, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.6784313725, blue: 0.5254901961, alpha: 1), #colorLiteral(red: 0.9137254902, green: 0.9058823529, blue: 0.5254901961, alpha: 1), #colorLiteral(red: 0.5254901961, green: 0.9137254902, blue: 0.5921568627, alpha: 1), #colorLiteral(red: 0.5254901961, green: 0.8, blue: 0.9137254902, alpha: 1)]
    
    @IBInspectable var styleIndex: Int {
        get { return CellStyle.all.firstIndex(of: style)! }
        set { style = CellStyle.all[newValue % CellStyle.all.count] }
    }
    
    var style: CellStyle = .default {
        didSet {
            updateStyle()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func sharedInit() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .white
        titleLabel.font = APPSetting.font.montserratXS
        self.addSubview(titleLabel)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    func update(index: Int) {
        //self.titleLabel.text = String(index + 1)
        let _sharingChannel = sharingChannel[index % sharingChannel.count]
        self.imageView.image = UIImage(named: _sharingChannel.id)
        
        self.titleLabel.text = _sharingChannel.name
        //self.backgroundColor = colors[index % colors.count]
    }
    
    func updateStyle() {
        switch style {
        case .default:
            self.layer.cornerRadius = 8
        case .circular:
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateStyle()
    }

    override func prepareForInterfaceBuilder() {
        self.update(index: 0)
    }
}
*/
