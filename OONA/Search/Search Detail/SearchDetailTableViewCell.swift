//
//  SearchDetailTableViewCell.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum SearchDetailRowType: String {
    case None = ""
    case Channel = "Channels"
    case Episode = "Episodes"
    case Merchant = "Merchants"
    
}

final class SearchDetailTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var disposeBag = DisposeBag()

    @IBOutlet private weak var searchDetailResultTitleLabel: UILabel!
//    @IBOutlet private weak var searchDetailResultDescriptionLabel: UILabel!
    @IBOutlet private weak var searchDetailResultCollectionView: UICollectionView!
    @IBOutlet weak var seeAllButton: UIButton!
    
    var previewItem: BehaviorRelay<PreviewModel?> = BehaviorRelay<PreviewModel?>(value: nil)
    
    var previousSelectedChannelCell: SearchDetailChannelCell?
    
    var previousSelectedEpisodeCell: SearchDetailEpisodeCell?
    
    var searchDetailType: SearchDetailRowType = .None
    
//    var channels: BehaviorRelay<[Channel]?> = BehaviorRelay<[Channel]?>(value: nil)
//    var episodes: BehaviorRelay<[Episode]?> = BehaviorRelay<[Episode]?>(value: nil)
    var channels: [Channel]? = [] { didSet { self.updateCollectionView() } }
    var episodes: [Episode]? = [] { didSet { self.updateCollectionView() } }
    
    var searchDetailResultTitle: String? {
        get {
            return searchDetailResultTitleLabel.text
        }
        set {
            searchDetailResultTitleLabel.text = newValue
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        searchDetailResultCollectionView.dataSource = self
        searchDetailResultCollectionView.delegate = self
        searchDetailResultCollectionView.register(UINib(nibName: "SearchDetailChannelCell", bundle: nil), forCellWithReuseIdentifier: "SearchDetailChannelCell")
        searchDetailResultCollectionView.register(UINib(nibName: "SearchDetailEpisodeCell", bundle: nil), forCellWithReuseIdentifier: "SearchDetailEpisodeCell")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.searchDetailType = .None
        self.searchDetailResultTitle = ""
        self.channels = nil
        self.episodes = nil
        self.seeAllButton.isHidden = true
        self.previewItem.accept(nil)
        previousSelectedChannelCell?.updateCellSelection(selection: false)
        previousSelectedEpisodeCell?.updateCellSelection(selection: false)
        previousSelectedChannelCell = nil
        previousSelectedEpisodeCell = nil
        self.disposeBag = DisposeBag()
        print("\(#function)")
    }
    
    func updateCollectionView() {
        print("\(#function)")
        (self.searchDetailResultCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).invalidateLayout()
        self.searchDetailResultCollectionView.reloadData()
    }
    
    @IBAction func seeAllButtonAction(_ sender: Any) {
        SearchHelper.shared.selectedField.accept(searchDetailType.rawValue)
    }
    
    // MARK: Collectionview

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch searchDetailType {
        case .Channel:
            guard let _channel = self.channels else {
                fallthrough
            }
            return _channel.count
        case .Episode:
            guard let _episodes = self.episodes else {
                fallthrough
            }
            return _episodes.count
//        case .Merchant:

        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch searchDetailType {
        case .Channel:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchDetailChannelCell", for: indexPath) as! SearchDetailChannelCell
            if let channel = self.channels?[indexPath.item] {
                cell.configure(with: channel)
            }
            return cell
            
        case .Episode:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchDetailEpisodeCell", for: indexPath) as! SearchDetailEpisodeCell
            if let episode = self.episodes?[indexPath.item] {
                cell.configure(with: episode)
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\(#function)")
        switch searchDetailType {
        case .Channel:
            if let channel = self.channels?[indexPath.item] {
                let cell = collectionView.cellForItem(at: indexPath) as! SearchDetailChannelCell
                if cell === previousSelectedChannelCell {
                    self.previewItem.accept(nil)
                    cell.updateCellSelection(selection: false)
                    previousSelectedChannelCell = nil
                    return
                }
                var subtitle = ""
                if channel.seasons.count > 0 {
                    subtitle = channel.seasons.count.description + "|"
                }
                let previewItem = PreviewModel(title: channel.name,
                                               subtitle: subtitle,
                                               description: channel.description,
                                               owner: .channel,
                                               video: channel.video)
                self.previewItem.accept(previewItem)
                previousSelectedChannelCell?.updateCellSelection(selection: false)
                cell.updateCellSelection(selection: cell.isSameChannel(channel: channel))
                previousSelectedChannelCell = cell
                
            }
            
        case .Episode:
            if let episode = self.episodes?[indexPath.item] {
                let cell = collectionView.cellForItem(at: indexPath) as! SearchDetailEpisodeCell
                if cell === previousSelectedEpisodeCell {
                    self.previewItem.accept(nil)
                    cell.updateCellSelection(selection: false)
                    previousSelectedEpisodeCell = nil
                    return
                }
                
                let previewItem = PreviewModel(title: episode.name,
                                               subtitle: nil,
                                               description: episode.description,
                                               owner: .episode,
                                               video: episode.video)
                self.previewItem.accept(previewItem)
                previousSelectedEpisodeCell?.updateCellSelection(selection: false)
                cell.updateCellSelection(selection: cell.isSameEpisode(episode: episode))
                previousSelectedEpisodeCell = cell
            }
            
        default:
            break
        }
    }
    
    // CollectionView flow layout
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 0
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 4
    private let cellLength: CGFloat = 80
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        var updatedInteritemSpacing = interitemSpacing
        switch searchDetailType {
//        case .Merchant:
        case .Episode:
            updatedInteritemSpacing = updatedInteritemSpacing / 2
        case .Channel: fallthrough
        default:
            break
        }
        return updatedInteritemSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var updatedInteritemSpacing = interitemSpacing
        let viewWidth: CGFloat = collectionView.frame.width
//        let viewHeight: CGFloat = collectionView.bounds.height
        var width = (viewWidth - (updatedInteritemSpacing * (numberOfColumns + 1 + 1))) / (numberOfColumns + 1)
        //        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        var height = lineSpacing//label height
        
        switch searchDetailType {
        case .Channel:
            width = (viewWidth - (updatedInteritemSpacing * (numberOfColumns + 1 + 1))) / (numberOfColumns + 1)
            height = (width / (16/9)) + 17 + 3
        case .Episode:
            updatedInteritemSpacing = interitemSpacing / 2
            width = (viewWidth - (updatedInteritemSpacing * (numberOfColumns + 1))) / numberOfColumns
            height = (width / (16/9)) + 3//label height
//        case .Merchant:
        default:
            break
        }
        
        return CGSize(width: width, height: height)
    }

}
