//
//  SearchDetailViewController.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class SearchDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var isRespondingSearch: Bool { return SearchHelper.shared.currentPage.value == self.searchPage }
    let searchPage: SearchPage = .Detail
    
    let searchManager: SearchDetailManager = SearchDetailManager()
    var currentKeyword: String?
//    var selectedKeyword: BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    
    var basicSearchResults: [OONAModel] = [] { didSet { self.tableView.reloadData() } }
    var previewController: PreviewViewController = { return PreviewViewController(nibName: "PreviewViewController", bundle: nil) }()
    
    let previewItem: PublishRelay<PreviewModel?> = PublishRelay<PreviewModel?>()
    
    @IBOutlet weak var previewView: UIView!
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    let previewHeight: CGFloat = 170
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SearchHelper.shared.currentPage.accept(self.searchPage)
        tableView.register(UINib(nibName: "SearchDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchDetailTableViewCell")
        self.getSearchResults(with: currentKeyword)
        self.addRXObservers()
        self.loadPreviewView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _currentKeyword = self.currentKeyword {
            SearchHelper.shared.searchKeyword.accept(currentKeyword)
            self.getSearchResults(with: _currentKeyword)
        }
        SearchHelper.shared.currentPage.accept(self.searchPage)
        SearchHelper.shared.selectedField.accept(SearchDetailRowType.None.rawValue)
        self.view.alpha = 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.alpha = 0
        self.previewController.stopPreview()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.identifier as Any)
        if let vc = segue.destination as? SearchChannelPreviewViewController {
            //TODO: assign something to destination viewcontroller if needed
            vc.currentKeyword = self.currentKeyword
            vc.searchPreviewType = .Channel
        } else if let vc = segue.destination as? SearchEpisodePreviewViewController {
            vc.currentKeyword = self.currentKeyword
            vc.searchPreviewType = .Episode
        }
    }
    
    override func appLanguageDidUpdate() {
        self.tableView.reloadData()
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}

// MARK: - Functional methods
extension SearchDetailViewController {
    private func addRXObservers() {
        SearchHelper.shared.selectedField.skip(1).filter({ $0.trimmingCharacters(in: .whitespaces).count > 0 }).subscribe(onNext: { [weak self] (selectedField) in
            print("Go to preview with field selected: \(selectedField)")
            self?.showPreviewDetail(selectedField: selectedField)
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.searchKeyword.skip(1).subscribe(onNext: { [weak self] (searchKeyword) in
            self?.updateCurrentSearchText(searchKeyword)
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.canSearchInNewPage.subscribe(onNext: { [weak self] (searchInNewPage) in
            self?.getSearchResults(with: self?.currentKeyword)
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.basicSearchResults.subscribe(onNext: { [weak self] (searchResults) in
            self?.showSearchResult(searchResults: searchResults)
        }).disposed(by: self.disposeBag)
        
        self.previewItem.subscribe(onNext: { [weak self] (previewItem) in
            self?.updatePreviewShowHide(with: previewItem)
        }).disposed(by: self.disposeBag)
        
        previewController.selectedItem.subscribe(onNext: { [weak self] (selectedItem) in
//            self?.shouldPlayVideo(video: selectedItem?.video)
            self?.prepareToPlay(previewItem: selectedItem)
        }).disposed(by: self.disposeBag)
    }
    
    private func updateCurrentSearchText(_ searchKeyword: String?) {
//        if isRespondingSearch { self.currentKeyword = searchKeyword }
        self.currentKeyword = searchKeyword
    }
    
    func updatePreviewShowHide(with previewItem: PreviewModel?) {
        
        UIView.transition(with: self.view,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.tableViewTopConstraint.constant = (previewItem != nil) ? self.previewHeight : 0
                            if previewItem != nil {
                                self.view.layoutIfNeeded()
                                self.previewController.previewItem.accept(previewItem)
                            } else {
                                self.previewController.previewItem.accept(previewItem)
                                self.view.layoutIfNeeded()
                            }
        })
        
    }
    
    func prepareToPlay(previewItem: PreviewModel?) {
        if let item = previewItem,
            let video = item.video {
            SearchHelper.shared.updateStreamingInfo(with: video,
                                                    videoOwner: item.videoOwner) { (success, video, error) in
//                                                        if success {
                                                            self.shouldPlayVideo(video: video)
//                                                        }
            }
        }
    }
    
    func shouldPlayVideo(video: Video?) {
        SearchHelper.shared.selectedVideo.accept(video)
    }
    
    private func loadPreviewView() {
        // Add Child View Controller
        self.addChild(self.previewController)
        self.previewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let previewVCView: UIView = self.previewController.view
        self.previewView.addSubview(previewVCView)
        // Configure Child View
        previewVCView.frame = self.previewView.bounds
        previewVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.previewController.didMove(toParent: self)
    }
    
//    private func performSearchAction(with keyword: String?, searchInNewPage: Bool) {
//        if isRespondingSearch {
//            if searchInNewPage {
//                self.currentKeyword = keyword
//                self.showPreviewDetail()
//            } else {
//                self.getSearchResults(with: keyword)
//            }
//        }
//    }
    
    private func getSearchResults(with keyword: String?) {
        if self.isRespondingSearch {
            self.view.startLoading()
            self.searchManager.requestSearchDetail(keyword: keyword)
        }
        if self.previewController.previewItem.value != nil {
            self.previewController.previewItem.accept(nil)
        }
    }
    
    private func showSearchResult(searchResults: [OONAModel]) {
        self.view.cancelLoading()
        self.basicSearchResults = searchResults
    }
    
    private func showPreviewDetail(selectedField: String) {
        
        if selectedField == SearchDetailRowType.Channel.rawValue {
            if self.navigationController?.viewControllers.filter({ $0 is SearchChannelPreviewViewController }).count == 0 {
                performSegue(withIdentifier: "showChannelPreviewPage", sender: nil)
            }
        } else if selectedField == SearchDetailRowType.Episode.rawValue {
            if self.navigationController?.viewControllers.filter({ $0 is SearchEpisodePreviewViewController }).count == 0 {
                performSegue(withIdentifier: "showEpisodePreviewPage", sender: nil)
            }
        }
        self.tableView.reloadData() // reset selected item at collectionView
        self.updatePreviewShowHide(with: nil) // hide preview 
    }
}


