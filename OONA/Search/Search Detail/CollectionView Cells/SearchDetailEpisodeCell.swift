//
//  SearchDetailEpisodeCell.swift
//  OONA
//
//  Created by nicholas on 8/3/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SearchDetailEpisodeCell: UICollectionViewCell {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var underline: UIView!
    var episode: Episode?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        descriptionLabel.backgroundColor = .init(red: 0, green: 0, blue: 0, alpha: 0.4)
    }
    
    @IBAction func click() {
        // print("clicked")
    }
    
    override func prepareForReuse() {
        self.previewImageView.image = nil
        self.descriptionLabel.text = nil
        self.underline.isHidden = true
        self.episode = nil
        super.prepareForReuse()
    }
    
    func configure(with episode: Episode) {
        self.episode = episode
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: episode.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        descriptionLabel.text = episode.name
        
    }
    
    func isSameEpisode(episode: Episode?) -> Bool {
        if let _reps = episode, let _leps = self.episode {
            return _reps == _leps
        }
        return false
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
    /*
     func setInActive(){
     self.underline.isHidden = true
     }
     
     func setActive(){
     self.underline.isHidden = false
     }*/

}
