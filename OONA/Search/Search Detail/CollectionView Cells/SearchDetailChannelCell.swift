//
//  SearchDetailChannelCell.swift
//  OONA
//
//  Created by nicholas on 8/3/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SearchDetailChannelCell: UICollectionViewCell {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    var channel: Channel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func click() {
        // print("clicked")
    }
    
    override func prepareForReuse() {
        self.previewImageView.image = nil
        self.descriptionLabel.text = nil
        self.underline.isHidden = true
        self.channel = nil
        super.prepareForReuse()
    }
    
    func configure(with channel: Channel) {
        self.channel = channel
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: channel.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        descriptionLabel.text = channel.name
    }
    
    func isSameChannel(channel: Channel?) -> Bool {
        if let _rchl = channel, let _lchl = self.channel {
            return _rchl == _lchl
        }
        return false
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
    /*
     func setInActive(){
     self.underline.isHidden = true
     }
     
     func setActive(){
     self.underline.isHidden = false
     }*/

}
