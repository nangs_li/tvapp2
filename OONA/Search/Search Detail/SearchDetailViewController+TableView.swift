//
//  SearchDetailViewController+TableView.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation


extension SearchDetailViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.basicSearchResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellHeight: CGFloat = 150
        switch self.basicSearchResults[indexPath.row] {
        case _ as SearchChannelObject:
            cellHeight = tableView.frame.height * (1/2)
        case _ as SearchEpisodeObject:
            cellHeight = tableView.frame.height * (3/5)
        default:
            break
        }
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SearchDetailTableViewCell", for: indexPath) as! SearchDetailTableViewCell
        if indexPath.row < self.basicSearchResults.count {
            switch self.basicSearchResults[indexPath.row] {
            case let searchChannel as SearchChannelObject:
                cell.searchDetailType = .Channel
                cell.searchDetailResultTitle = LanguageHelper.localizedString("channels_text") // cell.searchDetailType.rawValue+":"
                cell.channels = searchChannel.results
                cell.seeAllButton.isHidden = !(searchChannel.hasMore)
                cell.seeAllButton.setTitle(LanguageHelper.localizedString("search_see_all_text"), for: .normal)
                cell.previewItem.skip(1).subscribe(onNext: { (previewItem) in
                    self.previewItem.accept(previewItem)
                }).disposed(by: cell.disposeBag)
            case let searchEpisode as SearchEpisodeObject:
                cell.searchDetailType = .Episode
                cell.searchDetailResultTitle = LanguageHelper.localizedString("episodes_text") // cell.searchDetailType.rawValue+":"
                cell.episodes = searchEpisode.results
                cell.seeAllButton.isHidden = !(searchEpisode.hasMore)
                cell.seeAllButton.setTitle(LanguageHelper.localizedString("search_see_all_text"), for: .normal)
                cell.previewItem.skip(1).subscribe(onNext: { (previewItem) in
                    self.previewItem.accept(previewItem)
                }).disposed(by: cell.disposeBag)
            default:
                break
            }
        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let selectedField = (tableView.cellForRow(at: indexPath) as! SearchDetailTableViewCell).searchDetailType
//        SearchHelper.shared.selectedField.accept(selectedField.rawValue)
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation: CGPoint = scrollView.panGestureRecognizer.translation(in: scrollView.panGestureRecognizer.view)
        if abs(translation.y) > 5 {
            if let _ = self.previewController.previewItem.value {
                self.previewItem.accept(nil)
                self.tableView.reloadData()
            }
        }
    }
}
