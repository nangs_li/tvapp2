//
//  SearchDetailManager.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
class SearchDetailManager: NSObject {
    
    override init() {
        super.init()
    }
    
    func requestSearchDetail(keyword: String?) {
        if let _keyword = keyword {
            if _keyword.trimmingCharacters(in: .whitespaces).count >= 0 {
                /// Publish rx signal
//                SearchHelper.shared.basicSearchResults.accept([])
                
                self.getSearchDetailList(keyword: _keyword) { [unowned basicSearchResults = SearchHelper.shared.basicSearchResults] (success, searchResult) in
                    var _searchResult: [OONAModel] = []
                    if success, let checkSearchResult = searchResult {
                        _searchResult = checkSearchResult
                    }
                    //TODO: this part cause of object not being released
                    basicSearchResults.accept(_searchResult)
//                    basicSearchResults.accept([])
                }
                
            } else {
                // TODO:
//                SearchHelper.shared.basicSearchResults.accept([])
            }
        }
        SearchHelper.shared.basicSearchResults.accept([])
    }

    
    private func getSearchDetailList(keyword: String,
                                     completion: ((_ success: Bool, _ searchResults: [OONAModel]?) -> Void)?) {
        
        SearchHelper.shared.makeStandardSearch(with: keyword,
                                               success: { (keyword, searchResult) in
                                                completion?(true, searchResult)
        },
                                               failure: { (error) in
                                                completion?(false, [])
        })
    }
    
    deinit {
        print("Deallocating \(self)")
    }
}
