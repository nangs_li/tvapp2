//
//  SearchViewController.swift
//  OONA
//
//  Created by nicholas on 7/16/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import IQKeyboardManagerSwift

class SearchViewController: BaseViewController, UISearchBarDelegate {
    @IBOutlet weak var containerView: UIView!
    
    
    @IBOutlet weak var selectedChannelOrEpisodeButton: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var backButtonWidth: NSLayoutConstraint!
    
    var hasLoad = false
    
    @IBOutlet weak var selectedFieldButtonLeading: NSLayoutConstraint!
    let searchNavigationViewController: UINavigationController = { return UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "SearchNavigationViewController") as! UINavigationController }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupViews()
        
        if(!hasLoad){
            
            // Do any additional setup after loading the view.
            self.attachChildViewController(vc: self.searchNavigationViewController)
            IQKeyboardManager.shared.enable = true
            IQKeyboardManager.shared.shouldResignOnTouchOutside = true
            self.updateBackButton()
            self.addRXObservers()
            hasLoad = true
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    @objc func cancelSearch() {
        if self.isEditing || self.isFirstResponder {
            self.resignFirstResponder()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if self.searchNavigationViewController.viewControllers.count > 1 {
            self.searchNavigationViewController.popViewController(animated: true)
        }
        self.updateBackButton()
    }
    
    override func appLanguageDidUpdate() {
        self.searchBar.placeholder = LanguageHelper.localizedString("search_searchfield_placeholder")
        
    }
}

// MARK: - Functional methods
extension SearchViewController {
    private func addRXObservers() {
        SearchHelper.shared.searchKeyword.subscribe(onNext: { [weak self] (keyword) in
            self?.searchBar.text = keyword
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.currentPage.subscribe(onNext: { [weak self] (_) in
            self?.updateBackButton()
        }).disposed(by: self.disposeBag)
        
        // Update field button at left
        SearchHelper.shared.selectedField.subscribe(onNext: { [weak self] (selectedField) in
            self?.updateChannelOrEpisodeButton(selectedField: selectedField)
            self?.shouldEndEditing()
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.selectedVideo.subscribe(onNext: { [weak self] (_) in
//            self?.resetSearchPage()
        }).disposed(by: self.disposeBag)
    }
    
    func updateBackButton() {
        //TODO: Bug around here
        var width: CGFloat = 0
        if self.searchNavigationViewController.viewControllers.count > 1 {
//        if SearchHelper.shared.currentPage.value != .Main {
            width = 40
        }
        UIView.animate(withDuration: 0.4) {
            self.backButtonWidth.constant = width
            self.view.layoutIfNeeded()
        }
    }
    
    func updateChannelOrEpisodeButton(selectedField: String?) {
        var buttonLeading: CGFloat = -30
        var localizedFieldString: String = ""
        if let _selectedField = selectedField, _selectedField.count > 0 {
            buttonLeading = 0
            localizedFieldString = _selectedField
            
            switch SearchDetailRowType(rawValue: _selectedField) {
            case .Channel?:
                localizedFieldString = LanguageHelper.localizedString("channels_text")
            case .Episode?:
                localizedFieldString = LanguageHelper.localizedString("episodes_text")
            default: break
            }
        }
        //Bug: button text / button width missing
        self.selectedChannelOrEpisodeButton.setTitle(localizedFieldString, for: .normal)
        self.selectedChannelOrEpisodeButton.isHidden = (buttonLeading < 0)
        selectedFieldButtonLeading.constant = buttonLeading
    }
    
    func resetSearchPage() {
        self.searchNavigationViewController.popToRootViewController(animated: false)
    }
    
    func shouldEndEditing() {
        if searchBar.isFirstResponder {
            searchBar.resignFirstResponder()
        }
    }
    
    private func attachChildViewController(vc: UIViewController) {
        vc.willMove(toParent: self)
        self.addChild(vc)
        let childView = vc.view as UIView
        self.containerView.addSubview(childView)
        childView.frame = self.containerView.bounds
        childView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        vc.didMove(toParent: self)
    }
}

// MARK: - Search Bar
extension SearchViewController {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count < 3, self.searchNavigationViewController.viewControllers.count > 1 {
            self.searchNavigationViewController.popToRootViewController(animated: true)
        }
        SearchHelper.shared.searchKeyword.accept(searchBar.text)
        SearchHelper.shared.canSearchInNewPage.accept(false)
        print("\(#function)")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SearchHelper.shared.searchKeyword.accept(searchBar.text)
        SearchHelper.shared.canSearchInNewPage.accept(true)
        self.updateBackButton()
        self.shouldEndEditing()
//        self.view.endEditing(true)
    }
}
