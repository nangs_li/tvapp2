//
//  SearchResult.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

class SearchResultViewModel: OONABaseModel {
    let suggestions: [String]
    required init?(json: JSON) {
        if
            let suggestions = json["suggestions"].arrayObject as? [String] {
            self.suggestions = suggestions
        } else { return nil }
        super.init(json: json)
    }
}
