//
//  SearchHelper.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import Alamofire
import SwiftyJSON


enum SearchPage {
    case Main
    case Detail
    case Preview
}

class SearchHelper: NSObject {
    static let shared: SearchHelper = { return SearchHelper() }()
    
    /// Indicating which page being displayed
    var currentPage: BehaviorRelay<SearchPage> = BehaviorRelay<SearchPage>(value: .Main)
    
    /// To determine can perform a search in new page
    let canSearchInNewPage: PublishRelay<Bool> = PublishRelay()
    
    /// Update search bar text
    var searchKeyword: BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    
    /// Search info list
    var searchInfo: BehaviorRelay<SearchInfo?> = BehaviorRelay<SearchInfo?>(value: nil)
    
    /// Update suggestion list
    var suggestions: BehaviorRelay<[String]> = BehaviorRelay<[String]>(value: [])
    
    /// Selected trending / suggestion
    var selectedField: BehaviorRelay<String> = BehaviorRelay<String>(value:"")
    
    ///
    var basicSearchResults: BehaviorRelay<[OONAModel]> = BehaviorRelay<[OONAModel]>(value: [])
    
    var selectedVideo: PublishRelay<Video?> = PublishRelay<Video?>()
    
    /// Get search info
    ///
    /// - Parameter completionHandler: return an searchInfo object if found
    func getSearchInfo(completionHandler: ((SearchInfo?) -> Void)?) -> Void {
        let url: String = APPURL.SearchInfo()
        OONAApi.shared.getRequest(url: url, parameter: [:]).responseJSON { [weak self] (response) in
            var searchInfo: SearchInfo?
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                searchInfo = SearchInfo(json: json)
            case .failure(let error):
                print(error)
            }
            completionHandler?(searchInfo)
            self?.searchInfo.accept(searchInfo)
        }
    }
    
    /// Get suggestions by providing user input keywords
    ///
    /// - Parameters:
    ///   - keyword: from user input
    ///   - completionHandler: alway return an array
    func getSuggestions(with keyword: String?,
                        completionHandler: (([String]) -> Void)?) -> Void {
        guard let checkedKeyword = keyword,
            checkedKeyword.trimmingCharacters(in: .whitespaces).count >= 3
            else {
                completionHandler?([])
                self.suggestions.accept([])
            return }
        
        let url: String = APPURL.KeyWordSuggestions()
        OONAApi.shared.postRequest(url: url,
                                   parameter: ["keyword" : checkedKeyword]).responseJSON { [weak self] (response) in
                                    var suggestionsResult: [String] = []
                                    switch response.result {
                                    case .success(let value):
                                        let json = JSON(value)
                                        if let _suggestions = json["suggestions"].arrayObject as? [String] {
                                            suggestionsResult = _suggestions
                                        }
                                        
                                    case .failure(let error):
                                        print(error)
                                        
                                    }
                                    completionHandler?(suggestionsResult)
                                    self?.suggestions.accept(suggestionsResult)
        }
    }
    
    /// Inject ad into selected video
    ///
    /// - Parameters:
    ///   - video: video to inject
    ///   - videoOwner: identical video is belong to channel or episode
    ///   - completion: will call this block no matter success or failure
    func updateStreamingInfo(with video: Video,
                             videoOwner: VideoOwner,
                             completion: @escaping (_ success: Bool, _ result: Video?, _ error: Error?) -> Void) {
        var urlString: String?
        
        switch videoOwner {
        case .channel:
            urlString = APPURL.StreamingInfo(channelId: String(video.channelId))
        case .episode:
            urlString = APPURL.StreamingInfo(channelId: String(video.channelId), episodeId: String(video.episodeId))
        default: break
        }
        
        guard let _ = urlString else { return }
        
        OONAApi.shared.postRequest(url: urlString!,
                                   parameter: ["openedFrom": "search"]).responseJSON{ (response) in
                                    print("streamingInfo ",response)
                                    switch response.result {
                                    case .success(let value):
                                        let json = JSON(value)
                                        let code  = json["code"].int
                                        let status = json["status"].string
                                        APIResponseCode.checkResponse(withCode:code?.description ,
                                                                      withStatus: status,
                                                                      completion: {
                                                                        message , _success in
                                                                        print("streamingInfo ", message as Any)
                                                                        if(_success){
                                                                            video.update(json:json)
                                                                        }
                                                                        completion(_success, video, nil)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        completion(false, video, error)
                                    }
        }
    }
    
    /// Get basic search by providing user input keywords
    ///
    /// - Parameters:
    ///   - keyword: from user input
    ///   - success: Call back will fire when request is success but could still be contain some other error even request were made successfully.
    ///   - failure: Call back fill fire when request encountered error or failed to made.
    func makeStandardSearch(with keyword: String,
                            success: @escaping ((String, [OONAModel]) -> Void),
                            failure: ((Error?) -> Void)?) -> Void {
        let url: String = APPURL.SearchResult()
        OONAApi.shared.postRequest(url: url, parameter: ["keyword": keyword]).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string {
                    
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { message , _success in
                                                    if(_success){
                                                        var searchResults: [OONAModel] = []
                                                        let resultJSON = JSON(json["results"]).arrayValue
                                                        for _json in resultJSON {
                                                            if let _type = _json["type"].string {
                                                                switch _type {
                                                                case "channels":
                                                                    if let channelResult = SearchChannelObject(json: _json), channelResult.results.count > 0 {
                                                                        searchResults.append(channelResult)
                                                                    }
                                                                case "episodes":
                                                                    if let episodelResult = SearchEpisodeObject(json: _json), episodelResult.results.count > 0 {
                                                                        searchResults.append(episodelResult)
                                                                    }
                                                                default:
                                                                    if let searchResult = SearchBaseObject<Any>(json: _json), searchResult.results.count > 0 {
                                                                        searchResults.append(searchResult)
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        
                                                        success(keyword, searchResults)
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                        failure?(nil)
                                                    }
                    })
                }
            case .failure(let error):
                print(error)
                failure?(error)
                
            }
        }
    }
    
    /// Get search result by providing searching criterias
    ///
    /// - Parameters:
    ///   - searchObject: searching criterias (* mandatory fields) keyword*, chunkSize, offset, filter, sortBy
    ///   - success: Call back will fire when request is success but could still be contain some other error even request were made successfully.
    ///   - failure: Call back fill fire when request encountered error or failed to made.
    func makeAdvancedSearch(with searchObject: SearchCriteriasObject<OONAModel>,
                            success: @escaping ((String, String?, [OONAModel]) -> Void),
                            failure: ((Error?) -> Void)?) -> Void {
        
        let keyword = searchObject.keyword
        guard keyword.trimmingCharacters(in: .whitespaces).count >= 0 else { return }
        
        let url: String = APPURL.SearchResult()
        let parameter: [String: Any] =
            ["keyword": keyword,
             "chunkSize": searchObject.nextRequestRange.length,
             "offset": searchObject.nextRequestRange.location,
             "filter": searchObject.filter.rawValue,
             "sortBy": searchObject.sortBy.rawValue]
        OONAApi.shared.postRequest(url: url, parameter: parameter).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string {
                    
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { message , _success in
                                                    if(_success){
                                                        let jsonArray = json["results"].arrayValue
                                                        var itemList: [OONAModel] = []
                                                        if jsonArray.count > 0 {
                                                            if let  type = jsonArray[0]["type"].string, type == searchObject.filter.rawValue {
                                                                switch searchObject.filter {
                                                                case .searchChannels:
                                                                    itemList = jsonArray[0]["results"].oonaModelArray() as [Channel]
                                                                case .searchEpisodes:
                                                                    itemList = jsonArray[0]["results"].oonaModelArray() as [Episode]
                                                                default: break
                                                                }
                                                            }
                                                        }
                                                        success(keyword, searchObject.filter.rawValue, itemList)
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                        failure?(nil)
                                                    }
                    })
                }
            case .failure(let error):
                print(error)
                failure?(error)
                
            }
        }
    }
    
//    func makeYouTubeSearch(with searchObject: SearchObject<Any>,
//                           success: @escaping ((String, String?, [OONAChannel]) -> Void),
//                           failure: ((Error?) -> Void)?) -> Void {
//        let url: String = APPURL.SearchYouTubeResult()
////        let parameter: [String: Any] =
////            ["categoryId": xploreEpisode.categoryID,
////             "chunkSize": xploreEpisode.nextRequestRange.length,
////             "offset": xploreEpisode.nextRequestRange.location]
//    }
}
