//
//  SearchViewController+ConfigSubViews.swift
//  OONA
//
//  Created by nicholas on 7/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

extension SearchViewController {
    
    func setupViews() {
        // Button
        self.selectedChannelOrEpisodeButton.layer.cornerRadius = self.selectedChannelOrEpisodeButton.frame.height / 2
        self.selectedChannelOrEpisodeButton.backgroundColor = UIColor(rgb: 0x424648)
        self.selectedChannelOrEpisodeButton.layer.masksToBounds = true
        self.selectedChannelOrEpisodeButton.layer.shouldRasterize = true
        self.selectedChannelOrEpisodeButton.layer.rasterizationScale = UIScreen.main.scale
        self.selectedChannelOrEpisodeButton.layer.layoutIfNeeded()
        
        // SearchBar
//        self.searchBar.setImage(nil, for: .search, state: .focused)
    }
}
