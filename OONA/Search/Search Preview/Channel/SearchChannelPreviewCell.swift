//
//  SearchChannelPreviewCell.swift
//  OONA
//
//  Created by nicholas on 8/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class SearchChannelPreviewCell: UICollectionViewCell {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func click() {
        // print("clicked")
    }
    
    func configure(with channel: Channel) {
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: channel.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        descriptionLabel.text = channel.name
        
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
    /*
     func setInActive(){
     self.underline.isHidden = true
     }
     
     func setActive(){
     self.underline.isHidden = false
     }*/

}
