//
//  SearchPreviewDatasource.swift
//  OONA
//
//  Created by nicholas on 8/2/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class SearchChannelPreviewDatasource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static let cellIdentifier: String = "SearchChannelPreviewCell"
    
    var channelList: BehaviorRelay<[Channel]>
    var previewChannel: BehaviorRelay<Channel?>
    var previousSelectedCell: SearchChannelPreviewCell?
    let disposeBag = DisposeBag()
    weak var searchChannelManager: SearchChannelPreviewManager?
    
    override init() {
        self.channelList = BehaviorRelay<[Channel]>(value: [Channel]())
        self.previewChannel = BehaviorRelay<Channel?>(value: nil)
        super.init()
    }
    
    deinit { print("Deallocating \(self)") }
    
    func bindDataSource(to collectionView: UICollectionView) {
        // Register Cell Nib for collectionView
        collectionView.register(UINib(nibName: "SearchChannelPreviewCell", bundle: nil),
                                forCellWithReuseIdentifier: SearchChannelPreviewDatasource.cellIdentifier)
        // Set collectionView content inset
        collectionView.contentInset = UIEdgeInsets(top: lineSpacing,
                                                   left: interitemSpacing,
                                                   bottom: 0,
                                                   right: interitemSpacing)
        // Assign collectionView delegate and datasource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Subscribe rx update from episodeList to perform reloadData
        self.channelList.subscribe(onNext: { [weak collectionView] (episodeList) in
            collectionView?.reloadData()
        }).disposed(by: self.disposeBag)
        
        print("did add RXObserver to collectionView")
    }
    
    func selectedSameChannel(channelID: Int) -> Bool {
        if let _previewChannel = self.previewChannel.value {
            return _previewChannel.id == channelID
        }
        return false
    }
    
    // MARK: - CollectionView Delegate, Datasource and Layout
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channelList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SearchChannelPreviewCell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchChannelPreviewDatasource.cellIdentifier,
                                                                         for: indexPath) as! SearchChannelPreviewCell
        let channel: Channel = self.channelList.value[indexPath.item]
        
        cell.configure(with: channel)
        //        print("\(String(describing: type(of: self)))Cell frame: \(cell.frame)")
        //        cell.setInActive()
        
        cell.updateCellSelection(selection: selectedSameChannel(channelID: channel.id))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.channelList.value.count > 0 && indexPath.item < self.channelList.value.count {
            collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
            let channel = self.channelList.value[indexPath.item]
            let cell = collectionView.cellForItem(at: indexPath) as! SearchChannelPreviewCell
            if let _channel = self.previewChannel.value {
                if _channel == channel {
                    self.previewChannel.accept(nil)
                    cell.updateCellSelection(selection: false)
                    previousSelectedCell = nil
                    return
                }
            }
            self.previewChannel.accept(channel)
            previousSelectedCell?.updateCellSelection(selection: false)
            previousSelectedCell = cell
            cell.updateCellSelection(selection: selectedSameChannel(channelID: channel.id))
        }
        
    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 10
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 5
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
        
        //        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        let height = (width / (16/9)) + 17 + 3//label height
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation: CGPoint = scrollView.panGestureRecognizer.translation(in: scrollView.panGestureRecognizer.view)
        if abs(translation.y) > 5 {
            if let _ = self.previewChannel.value {
                self.previewChannel.accept(nil)
                previousSelectedCell?.updateCellSelection(selection: false)
                previousSelectedCell = nil
            }
            
        }
    }
}
