//
//  SearchChannelPreviewViewController.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class SearchChannelPreviewViewController: BaseViewController {
    
    let previewHeight: CGFloat = 170
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
        collectionView.backgroundColor = nil
        collectionView.keyboardDismissMode = .onDrag
        } }
    var previewController: PreviewViewController = { return PreviewViewController(nibName: "PreviewViewController", bundle: nil) }()
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var previewView: UIView!
    let searchChannelPreviewDatasource: SearchChannelPreviewDatasource = SearchChannelPreviewDatasource()
    let searchChannelPreviewManager: SearchChannelPreviewManager = SearchChannelPreviewManager()
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    
//    var searchCriteriasObject: SearchCriteriasObject<OONAModel>?
    
    
    var isRespondingSearch: Bool { return SearchHelper.shared.currentPage.value == self.searchPage }
    let searchPage: SearchPage = .Preview
    
    var searchPreviewType: SearchDetailRowType = .None // TODO: need to update
    
    var currentKeyword: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SearchHelper.shared.currentPage.accept(self.searchPage)
        self.addRXObservers()
        self.loadPreviewView()
        self.searchChannelPreviewManager._searchCriteriasObject.keyword = currentKeyword ?? ""
        self.searchChannelPreviewDatasource.searchChannelManager = self.searchChannelPreviewManager
        self.getSearchResults(with: currentKeyword)
        self.setupCollectionViewPullUpLoadMoreFooter()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SearchHelper.shared.currentPage.accept(self.searchPage)
        if let _ = self.currentKeyword {
            SearchHelper.shared.searchKeyword.accept(currentKeyword)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //TODO:
        self.previewController.stopPreview()
//        SearchHelper.shared.selectedField.accept(searchPreviewType.rawValue)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    override func appLanguageDidUpdate() {
        self.headerLabel.text = LanguageHelper.localizedString("channels_text")
        SearchHelper.shared.selectedField.accept(SearchDetailRowType.Channel.rawValue)
    }
    
    deinit {
//        print("Deallocating \(self)")
        SearchHelper.shared.selectedField.accept(SearchDetailRowType.None.rawValue)
    }
}

// MARK: - Config Views
extension SearchChannelPreviewViewController {
    private func setupCollectionViewPullUpLoadMoreFooter() {
        let loadMoreFooter = MJRefreshAutoNormalFooter()
        loadMoreFooter.refreshingBlock = { [weak self] in
            self?.loadMoreData()
        }
        loadMoreFooter.isHidden = true
        self.collectionView.mj_footer = loadMoreFooter
    }
}

// MARK: - Functional methods
extension SearchChannelPreviewViewController {
    
    private func addRXObservers() {
        SearchHelper.shared.searchKeyword.skip(1).subscribe(onNext: { [weak self] (searchKeyword) in
            self?.updateCurrentSearchText(searchKeyword)
        }).disposed(by: self.disposeBag)
        
        SearchHelper.shared.canSearchInNewPage.subscribe(onNext: { [weak self] (searchInNewPage) in
            self?.collectionView.mj_footer?.isHidden = true
            self?.searchChannelPreviewManager._searchCriteriasObject.itemList.removeAll()
            self?.getSearchResults(with: self?.currentKeyword)
        }).disposed(by: self.disposeBag)
        
        // Bind dataSource to collectionView
        searchChannelPreviewDatasource.bindDataSource(to: collectionView)
        
        // Subscribe data change from Episode manager on data has been updated
        searchChannelPreviewManager.searchCriteriasObject.subscribe(onNext: { [weak self] (searchCriterias) in
            var channelList: [Channel] = []
            if let _channelList: [Channel] = searchCriterias.itemList as? [Channel] {
                channelList = _channelList
            }
            self?.searchChannelPreviewDatasource.channelList.accept(channelList)
            self?.view.cancelLoading()
            self?.endLoadMoreData(with: searchCriterias)
        }).disposed(by: self.disposeBag)
        
        searchChannelPreviewDatasource.previewChannel.subscribe(onNext: { [weak self] (channel) in
            
            self?.updatePreviewShowHide(with: channel)
            
        }).disposed(by: self.disposeBag)
        
        previewController.selectedItem.subscribe(onNext: { [weak self] (selectedItem) in
//            self?.shouldPlayVideo(video: selectedItem?.video)
            self?.prepareToPlay(previewItem: selectedItem)
        }).disposed(by: self.disposeBag)
        
    }
    
    private func updateCurrentSearchText(_ searchKeyword: String?) {
//        if isRespondingSearch { self.currentKeyword = searchKeyword }
        self.currentKeyword = searchKeyword
    }
    
    private func getSearchResults(with keyword: String?) {
        if self.isRespondingSearch {
            self.view.startLoading()
            self.searchChannelPreviewManager.requestItemList(keyword: keyword)
        }
        
        if self.previewController.previewItem.value != nil {
            self.previewController.previewItem.accept(nil)
        }
    }
    
    func loadMoreData() {
        self.getSearchResults(with: self.currentKeyword)
    }
    
    func endLoadMoreData(with searchCriterias: SearchCriteriasObject<OONAModel>) {
        //        self.collectionView.mj_footer.endRefreshing()
        self.collectionView.mj_footer?.isHidden = false
        if searchCriterias.hasNextPage {
            self.collectionView.mj_footer?.endRefreshing()
        } else {
            self.collectionView.mj_footer?.endRefreshingWithNoMoreData()
        }
    }
    
    func prepareToPlay(previewItem: PreviewModel?) {
        if let item = previewItem,
            let video = item.video {
            SearchHelper.shared.updateStreamingInfo(with: video,
                                                    videoOwner: item.videoOwner) { (success, video, error) in
//                                                        if success {
                                                            self.shouldPlayVideo(video: video)
//                                                        }
            }
        }
    }
    
    func shouldPlayVideo(video: Video?) {
        SearchHelper.shared.selectedVideo.accept(video)
    }
}


extension SearchChannelPreviewViewController {
    
    func updatePreviewShowHide(with channel: Channel?) {
        
        var subtitle = ""
        if let _channel = channel, _channel.seasons.count > 0 {
            subtitle = _channel.seasons.count.description + "|"
        }
        let previewItem = PreviewModel(title: channel?.name,
                                       subtitle: subtitle,
                                       description: channel?.description,
                                       owner: .channel,
                                       video: channel?.video)
//        let indexPath = self.collectionView.visible
        UIView.transition(with: self.view,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.collectionViewTopConstraint.constant = (channel != nil) ? self.previewHeight : 0
                            if channel != nil {
                                self.view.layoutIfNeeded()
                                self.previewController.previewItem.accept(previewItem)
                            } else {
                                self.previewController.previewItem.accept(previewItem)
                                self.view.layoutIfNeeded()
                            }
        })
    }
    
    func updateCellVisibilityInCollectionView(with item: Int) {
        self.collectionView.scrollToItem(at: IndexPath(item: item,
                                                       section: 0),
                                         at: .top, animated: true)
    }
    
    private func loadPreviewView() {
        // Add Child View Controller
        self.addChild(self.previewController)
        self.previewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let previewVCView: UIView = self.previewController.view
        self.previewView.addSubview(previewVCView)
        // Configure Child View
        previewVCView.frame = self.previewView.bounds
        previewVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.previewController.didMove(toParent: self)
    }
}

