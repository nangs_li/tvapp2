//
//  SearchEpisodePreviewManager.swift
//  OONA
//
//  Created by nicholas on 8/3/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RxSwift
import RxCocoa

class SearchEpisodePreviewManager: NSObject {
    var searchCriteriasObject : PublishRelay<SearchCriteriasObject> = PublishRelay<SearchCriteriasObject<OONAModel>>()
    
    var _searchCriteriasObject: SearchCriteriasObject<OONAModel>
    
    let disposeBag = DisposeBag()
    
    override init() {
        self._searchCriteriasObject = SearchCriteriasObject(keyword: "")
        if SearchHelper.shared.selectedField.value == SearchDetailRowType.Episode.rawValue {
            self._searchCriteriasObject.filter = .searchEpisodes
        } else if SearchHelper.shared.selectedField.value == SearchDetailRowType.Episode.rawValue {
            self._searchCriteriasObject.filter = .searchEpisodes
        }
        super.init()
    }
    
    func isSameSearch(_ keyword: String) -> Bool {
        return keyword == _searchCriteriasObject.keyword
    }
    
    func requestItemList(keyword: String?) {
        guard let _keyword = keyword else { return }
        if _keyword != "" {
            if !isSameSearch(_keyword) {
                print("Performing new episode search request")
                self._searchCriteriasObject.keyword = _keyword
                self._searchCriteriasObject.itemList.removeAll()
            } else {
                print("Performing next episode search request")
            }
        }
        else {
            print("Performing new episode search request")
            self._searchCriteriasObject.keyword = _keyword
        }
        //        /// Publish rx signal
        self.searchCriteriasObject.accept(self._searchCriteriasObject)
        self.getSearchPreviewList(searchCriterias: _searchCriteriasObject,
                                  completion: { [weak self] (success, keyword, searchType, previreList) in
                                    if success {
                                        self?.updatePreviewList(previewList: previreList!, keyword: keyword)
                                    }
        })
    }
    
    private func updatePreviewList(previewList: [OONAModel],
                                   keyword: String) {
        
        /// Check xploreEpisode is exist
        if self._searchCriteriasObject.keyword != "" {
            print("Updating PreviewList for keyword: \(keyword)")
            if keyword == self._searchCriteriasObject.keyword {
                print("Appending PreviewList for keyword: \(keyword)")
                self._searchCriteriasObject.itemList.append(contentsOf: previewList)
            } else {
                print("Replacing PreviewList for keyword from: \(_searchCriteriasObject.keyword) to: \(keyword)")
                self._searchCriteriasObject.keyword = keyword
                self._searchCriteriasObject.itemList = previewList
            }
            
            self._searchCriteriasObject.hasNextPage = (previewList.count == self._searchCriteriasObject.nextRequestRange.length)
            
            /// Publish rx signal
            self.searchCriteriasObject.accept(self._searchCriteriasObject)
        }
    }
    
    
    func getSearchPreviewList(searchCriterias: SearchCriteriasObject<OONAModel>,
                              completion: ((_ success: Bool, _ keyword: String, _ searchType: String?, _ previewList: [OONAModel]?) -> Void)?) {
        //        if let _criterias = self.searchCriteriasObject.value {
        
        SearchHelper.shared.makeAdvancedSearch(with: searchCriterias,
                                               success: { (keyword, searchType, previewList) in
                                                completion?(true, keyword, searchType, previewList)
        },
                                               failure: { (error) in
                                                
                                                completion?(false, searchCriterias.keyword, searchCriterias.filter.rawValue, nil)
                                                print(error as Any)
        })
        //        }
    }
    
    deinit { print("Deallocating \(self)") }
    

}
