//
//  SearchEpisodePreviewDatasource.swift
//  OONA
//
//  Created by nicholas on 8/3/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class SearchEpisodePreviewDatasource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static let cellIdentifier: String = "SearchEpisodePreviewCell"
    
    var episodeList: BehaviorRelay<[Episode]>
    var previewEpisode: BehaviorRelay<Episode?>
    var previousSelectedCell: SearchEpisodePreviewCell?
    let disposeBag = DisposeBag()
    weak var searchEpisodeManager: SearchEpisodePreviewManager?
    
    override init() {
        self.episodeList = BehaviorRelay<[Episode]>(value: [Episode]())
        self.previewEpisode = BehaviorRelay<Episode?>(value: nil)
        super.init()
    }
    
    deinit { print("Deallocating \(self)") }
    
    func bindDataSource(to collectionView: UICollectionView) {
        // Register Cell Nib for collectionView
        collectionView.register(UINib(nibName: "SearchEpisodePreviewCell", bundle: nil),
                                forCellWithReuseIdentifier: SearchEpisodePreviewDatasource.cellIdentifier)
        // Set collectionView content inset
        collectionView.contentInset = UIEdgeInsets(top: lineSpacing,
                                                   left: interitemSpacing,
                                                   bottom: 0,
                                                   right: interitemSpacing)
        // Assign collectionView delegate and datasource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Subscribe rx update from episodeList to perform reloadData
        self.episodeList.subscribe(onNext: { [weak collectionView] (episodeList) in
            collectionView?.reloadData()
        }).disposed(by: self.disposeBag)
        
        print("did add RXObserver to collectionView")
    }
    
    func selectedSameEpisode(episodeID: Int) -> Bool {
        if let _previewEpisode = self.previewEpisode.value {
            return _previewEpisode.id == episodeID
        }
        return false
    }
    
    // MARK: - CollectionView Delegate, Datasource and Layout
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return episodeList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SearchEpisodePreviewCell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchEpisodePreviewDatasource.cellIdentifier,
                                                                                for: indexPath) as! SearchEpisodePreviewCell
        let episode: Episode = self.episodeList.value[indexPath.item]
        
        cell.configure(with: episode)
        //        print("\(String(describing: type(of: self)))Cell frame: \(cell.frame)")
        //        cell.setInActive()
        
        cell.updateCellSelection(selection: selectedSameEpisode(episodeID: episode.id))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.episodeList.value.count > 0 && indexPath.item < self.episodeList.value.count {
            collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
            let episode = self.episodeList.value[indexPath.item]
            let cell = collectionView.cellForItem(at: indexPath) as! SearchEpisodePreviewCell
            if let _episode = self.previewEpisode.value {
                if _episode == episode {
                    self.previewEpisode.accept(nil)
                    cell.updateCellSelection(selection: false)
                    previousSelectedCell = nil
                    return
                }
            }
            self.previewEpisode.accept(episode)
            previousSelectedCell?.updateCellSelection(selection: false)
            previousSelectedCell = cell
            cell.updateCellSelection(selection: selectedSameEpisode(episodeID: episode.id))
        }
        
    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 10
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
//        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
        //        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        let height = (width / (16/9))//label height
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation: CGPoint = scrollView.panGestureRecognizer.translation(in: scrollView.panGestureRecognizer.view)
        if abs(translation.y) > 5 {
            if let _ = self.previewEpisode.value {
                self.previewEpisode.accept(nil)
                previousSelectedCell?.updateCellSelection(selection: false)
                previousSelectedCell = nil
            }
        }
    }
}
