//
//  TrendingCollectionView.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TrendingCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let disposeBag: DisposeBag = DisposeBag()
    
    var trendingSearches: [String] = [] { didSet { self.reloadData() } }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: "TrendingCell", bundle: nil), forCellWithReuseIdentifier: "TrendingCell")
        self.delegate = self
        self.dataSource = self
        self.contentInset = UIEdgeInsets(top: lineSpacing/2, left: interitemSpacing, bottom: 0, right: 0)
        self.reloadData()
    }
    
    override func reloadData() {
        (self.collectionViewLayout as! UICollectionViewFlowLayout).invalidateLayout()
        super.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return Int(max(0, min(numberOfColumns, CGFloat(trendingSearches.count))))
        return trendingSearches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCell", for: indexPath) as! TrendingCell
        if trendingSearches.count > 0 && indexPath.item < trendingSearches.count {
            let search = trendingSearches[indexPath.item]
            cell.keyword = search
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var item: String = ""
        if self.trendingSearches.count > 0,
            self.trendingSearches.count > indexPath.item {
            let selectedResult = self.trendingSearches[indexPath.item]
            SearchHelper.shared.searchKeyword.accept(selectedResult)
            SearchHelper.shared.canSearchInNewPage.accept(true)
            item = selectedResult
        }
        print("did selected item: \(item)")
    }
    
    // CollectionView flow layout
    private let interitemSpacing: CGFloat = 5
    private let lineSpacing: CGFloat = 8
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 5
    private let cellLength: CGFloat = 80
    var itemWidth: CGFloat = 0
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize = CGSize()
        if self.trendingSearches.count > 0,
            self.trendingSearches.count > indexPath.item {
//            let updatedNoOfCol: CGFloat = max(0, min(numberOfColumns, CGFloat(trendingSearches.count)))
            let viewWidth: CGFloat = collectionView.frame.width
            var width: CGFloat = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
            let height: CGFloat = collectionView.frame.height - lineSpacing * 2
            
            // dynamic: self sizing by content's width
             let selectedResult = self.trendingSearches[indexPath.item]
//            width = min(width, self.width(forHeight: height, body: selectedResult))
            width = self.width(forHeight: height, body: selectedResult)
            size = CGSize(width: max(15, width) + 20, height: height)
            }
        return size
    }
    
    // calculate label width
    public func width(forHeight height: CGFloat, body: String?) -> CGFloat {
        let sizingLabel             = UILabel()
        sizingLabel.numberOfLines   = 1
        sizingLabel.font            = UIFont(name: "Montserrat-Regular", size: 15)
        sizingLabel.lineBreakMode   = .byTruncatingTail
        sizingLabel.text            = body
        
        let maxSize = CGSize(width: .greatestFiniteMagnitude, height: height)
        let size = sizingLabel.sizeThatFits(maxSize)
        
        return size.width
    }
}
