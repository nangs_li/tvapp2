//
//  TrendingCell.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class TrendingCell: UICollectionViewCell {
    var keyword: String? {
        get { return keywordLabel.text }
        set {
            keywordLabel.text = newValue
//            self.keywordLabel.preferredMaxLayoutWidth = 200
//            self.layoutIfNeeded()
        }
    }
    @IBOutlet private weak var keywordLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.init(rgb: 0x424648)
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
        
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = UIScreen.main.scale
    }
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        
        // Specify you want _full width_
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        
        // Calculate the size (height) using Auto Layout
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        
        // Assign the new size to the layout attributes
        autoLayoutAttributes.frame = autoLayoutFrame
//        setNeedsLayout()
//        layoutIfNeeded()
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//        layoutAttributes.frame = frame
        return autoLayoutAttributes
    }
}
