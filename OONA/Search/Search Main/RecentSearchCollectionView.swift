//
//  RecentSearchCollectionView.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RecentSearchCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let disposeBag: DisposeBag = DisposeBag()
    
    var recentSearches: [OONAModel] = [] { didSet { self.reloadData() } }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(UINib(nibName: "EpisodeCell", bundle: nil), forCellWithReuseIdentifier: "EpisodeCell")
        self.register(UINib(nibName: "SearchChannelPreviewCell", bundle: nil), forCellWithReuseIdentifier: "SearchChannelPreviewCell")
        self.delegate = self
        self.dataSource = self
        (self.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing = interitemSpacing
        (self.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = lineSpacing
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recentSearches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item < self.recentSearches.count {
            switch self.recentSearches[indexPath.item] {
            case let channel as Channel:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchChannelPreviewCell", for: indexPath) as! SearchChannelPreviewCell
                cell.configure(with: channel)
                return cell
            case let episode as Episode:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell", for: indexPath) as! EpisodeCell
                cell.configure(with: episode)
                return cell
            default: return UICollectionViewCell()
            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var video: Video?
        if indexPath.item < self.recentSearches.count {
            switch self.recentSearches[indexPath.item] {
            case let channel as Channel:
                video = channel.video
            case let episode as Episode:
                video = episode.video
            default: break
            }
        }
        SearchHelper.shared.selectedVideo.accept(video)
    }
    
    
    // CollectionView flow layout
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 0
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 4
    private let cellLength: CGFloat = 80
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        //        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1 + 1))) / (numberOfColumns + 1)
        let height = (width / (16/9)) + 17+3//label height
//        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        
        return CGSize(width: width, height: height)
    }
}
