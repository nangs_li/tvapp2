//
//  SearchMainViewController.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchMainViewController: BaseViewController {
    
    @IBOutlet weak var trendingLabel: UILabel!
    @IBOutlet weak var trendingCollectionView: TrendingCollectionView!
    
    @IBOutlet weak var recentSearchLabel: UILabel!
    @IBOutlet weak var recentSearchCollectionView: RecentSearchCollectionView!
    
    var isRespondingSearch: Bool { return SearchHelper.shared.currentPage.value == self.searchPage }
    let searchPage: SearchPage = .Main
    
    var currentKeyword: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchHelper.shared.currentPage.accept(self.searchPage)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.addRXObservers()
        self.getSearchInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _currentKeyword = self.currentKeyword, _currentKeyword.count >= 3 {
            SearchHelper.shared.searchKeyword.accept(currentKeyword)
            self.getSearchSuggestions(with: _currentKeyword)
        } else {
            // Call and check if any update
            self.getSearchInfo()
        }
        SearchHelper.shared.currentPage.accept(self.searchPage)
        SearchHelper.shared.selectedField.accept(SearchDetailRowType.None.rawValue)
        
        self.view.alpha = 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.alpha = 0
        self.view.endEditing(true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.identifier as Any)
        if let vc = segue.destination as? SearchDetailViewController {
            //TODO: assign something to destination viewcontroller if needed
//            print("Address of vc: \(Unmanaged.passUnretained(vc).toOpaque())")
            vc.currentKeyword = self.currentKeyword
        }
    }
    
    override func appLanguageDidUpdate() {
        self.trendingLabel.text = LanguageHelper.localizedString("search_trending_text")
        self.recentSearchLabel.text = LanguageHelper.localizedString("search_recent_searches_text")
        
    }
}

// MARK: - Functional methods
extension SearchMainViewController {
    private func addRXObservers() {
        
        // Observe search bar text did changed
        SearchHelper.shared.searchKeyword.skip(1).subscribe(onNext: { [weak self] (searchKeyword) in
            self?.updateCurrentSearchText(searchKeyword)
        }).disposed(by: self.disposeBag)
        
        // Observe trending / recent search list
        SearchHelper.shared.searchInfo.subscribe(onNext: { [weak self] (searchInfo) in
            self?.showSearchInfo()
        }).disposed(by: self.disposeBag)
        
        // Observe search suggestion results
        SearchHelper.shared.suggestions.subscribe(onNext: { [weak self] (suggestions) in
            self?.showSuggestions(suggestions: suggestions)
        }).disposed(by: self.disposeBag)
        
        // Received authorize can perform search update from search bar
        SearchHelper.shared.canSearchInNewPage.subscribe(onNext: { [weak self] (searchInNewPage) in
            self?.performSearchAction(with: self?.currentKeyword, searchInNewPage: searchInNewPage)
        }).disposed(by: self.disposeBag)
        
        // Reset search page after video did selected
        SearchHelper.shared.selectedVideo.subscribe(onNext: { [weak self] (_) in
            self?.performSearchAction(with: "", searchInNewPage: false)
        }).disposed(by: self.disposeBag)
        
    }
    
    private func updateCurrentSearchText(_ searchKeyword: String?) {
//        if isRespondingSearch { self.currentKeyword = searchKeyword }
        self.currentKeyword = searchKeyword
    }
    
    private func performSearchAction(with keyword: String?, searchInNewPage: Bool) {
        if isRespondingSearch {
            if searchInNewPage {
                self.currentKeyword = keyword
                self.showSearchDetail()
            } else {
                if let _keyword = keyword, _keyword.trimmingCharacters(in: .whitespaces).count >= 3 {
                    self.getSearchSuggestions(with: keyword)
                } else {
                    // Call and check if any update
                    self.getSearchInfo()
                }
            }
        }
    }
    
    // Get default list when search bar characters less than 3
    private func getSearchInfo() {
        // Show search info first
        self.showSearchInfo()
        
        SearchHelper.shared.getSearchInfo(completionHandler: nil)
    }
    
    // Get search results when search bar characters equal or greater than 3
    private func getSearchSuggestions(with keyword: String?) {
        
        self.showSuggestions(suggestions: SearchHelper.shared.suggestions.value)
        SearchHelper.shared.getSuggestions(with: keyword, completionHandler: nil)
    }
    
    // Did received search info update
    private func showSearchInfo() {
        var trending: [String] = []
        var recentSearchs: [OONAModel] = []
        if let searchInfo = SearchHelper.shared.searchInfo.value {
            if let _trending = searchInfo.trending {
                trending = _trending
            }
            
            if let _recentSearchs = searchInfo.recentSearches {
                recentSearchs = _recentSearchs
            }
        }
        self.trendingCollectionView.trendingSearches = trending
        self.recentSearchCollectionView.recentSearches = recentSearchs
        
        UIView.transition(with: self.view,
                          duration: 0.4,
                          options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {
                            self.recentSearchLabel.isHidden = false
                            self.recentSearchCollectionView.isHidden = false
        })

    }
    
    // Did received suggesstion update
    private func showSuggestions(suggestions: [String]) {
        
        UIView.transition(with: self.view,
                          duration: 0.4,
                          options: UIView.AnimationOptions.transitionCrossDissolve,
                          animations: {
                            self.recentSearchLabel.isHidden = true
                            self.recentSearchCollectionView.isHidden = true
        })
        
        self.trendingCollectionView.trendingSearches = suggestions
    }
    
    private func showSearchDetail() {
        if self.navigationController?.viewControllers.filter({ $0 is SearchDetailViewController }).count == 0 {
            performSegue(withIdentifier: "showDetailPage", sender: nil)
        }
    }
    
}
