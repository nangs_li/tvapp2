//
//  SearchInfo.swift
//  OONA
//
//  Created by nicholas on 8/1/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

class SearchInfo: OONAModel {
    var trending: [String]?
    var recentSearches: [OONAModel]?
    required init?(json: JSON) {
        
        self.trending = json["trending"].arrayValue.map( { $0.stringValue } )
        
        self.recentSearches = json["recentSearches"].arrayValue.compactMap({ (searchJSON) -> OONAModel? in
            let _searchJSON = JSON(searchJSON)
            if let type = _searchJSON["type"].string {
                switch type {
                case "live":
                    return Channel(json: _searchJSON)
                case "episode":
                    return Episode(json: _searchJSON)
                default:
                    return nil
                }
            }
            return nil
        })
    }
}
