//
//  SearchBaseObject.swift
//  OONA
//
//  Created by nicholas on 8/1/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

class SearchBaseObject<T>: OONAModel {
    var type: String?
    var hasMore: Bool = false
    var results: [T] = []
    
    required init?(json: JSON) {
        if let _type = json["type"].string {
            self.type = _type
        }
        
        if let _hasMore = json["hasMore"].number {
            hasMore = _hasMore.boolValue
        }
    }
}

class SearchChannelObject: SearchBaseObject<Channel> {
    required init?(json: JSON) {
        super.init(json: json)
        self.results = JSON(json["results"]).oonaModelArray() as [Channel]
    }
}

class SearchEpisodeObject: SearchBaseObject<Episode> {
    required init?(json: JSON) {
        super.init(json: json)
        self.results = JSON(json["results"]).oonaModelArray() as [Episode]
    }
}
