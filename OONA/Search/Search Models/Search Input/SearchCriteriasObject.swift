//
//  SearchObject.swift
//  OONA
//
//  Created by nicholas on 7/31/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SwiftyJSON

enum SearchType: String {
    case searchDefault = ""
    case searchChannels = "channels"
    case searchEpisodes = "episodes"
}

enum SearchSortOption: String {
    case searchSortDefault = ""
    case searchSortRelevant = "relevant"
    case searchSortRecent = "recent"
    case searchSortRopular = "popular"
}

class SearchCriteriasObject<T>: Pagination<T> {
    /// For identify which
    
    var keyword: String
    
    var filter: SearchType = .searchDefault
    
    var sortBy: SearchSortOption = .searchSortDefault
    
    // Input model
    init(keyword: String) {
        self.keyword = keyword
    }
    
}

