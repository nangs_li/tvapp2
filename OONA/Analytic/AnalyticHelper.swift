//
//  AnalyticHelper.swift
//  OONA
//
//
// Please put All parameter name of events need to this class.
//
//
//
//

//  Created by Jack on 21/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

class AnalyticHelper : NSObject {
    static let shared = AnalyticHelper()
    var lastVideoStartTimeStamp : Int = 0
    var lastChatroomEnterTimeStamp : Int = 0
    var lastDisplayAdShowTimeStamp : Int = 0
    var lastGameAdShowTimeStamp : Int = 0
    var lastVideoAdViewTimeStamp : Int = 0
    public func viewEnter(viewName:String) {
        self.logEventToAllPlatform(name: viewName,
                      parameters:[
                        "action":"enter",
                        "event_code":DeviceHelper.shared.getCurrentTimeStamp().intValue
                        ])
    }
 
    public func viewLeave(viewName:String) {
        self.logEventToAllPlatform(name: viewName,
                                   parameters: [
                                    "action":"leave",
                                   "event_code":DeviceHelper.shared.getCurrentTimeStamp().intValue
                                    
            ])
        
    }
    public func firstLaunch() {
        self.logEventToAllPlatform(name: "first_launch",
                                   parameters: [:])
        
    }
    public func appOpen() {
        self.logEventToAllPlatform(name: "app_open",
                                   parameters:[:])
    }
///     Video
    
    public func videoStart(video:Video) {
        lastVideoStartTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self.logEventToAllPlatform(name: "video_start",
                                   parameters:[
                                    "channel_id":(video.channelId),
                                    "episode_id":(video.episodeId),
                                    "offline_mode":"0",
                                    "event_code":lastVideoStartTimeStamp
            ])
    }
    public func videoStop(video:Video, watchedDuration:Int) {
        self.logEventToAllPlatform(name: "video_stop",
                                   parameters: [
                                    "channel_id":(video.channelId),
                                    "episode_id":(video.episodeId),
                                    "watched_duration":watchedDuration,
                                    "offline_mode":"0",
                                    "event_code":lastVideoStartTimeStamp
            ])
    }
    
    
    
///     Chatroom
    
    public func chatroomEnter(video:Video) {
        lastChatroomEnterTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self .logEventToAllPlatform(name: "chatroom_enter",
                                    parameters: ["channel_id":video.channelId,
                                                 "event_code":lastChatroomEnterTimeStamp
                                                 
            ])
    }
    public func chatroomQuit(video:Video) {
        
        self .logEventToAllPlatform(name: "chatroom_quit",
                                    parameters: ["channel_id":(video.channelId),
                                                 "event_code":lastChatroomEnterTimeStamp
                                        
            ])
    }
    public func chatroomGetmore(video:Video) {
        self .logEventToAllPlatform(name: "chatroom_getmore",
                                    parameters: ["channel_id":video.channelId
            ])
    }
    public func chatroomSentMsg(video:Video) {
        self .logEventToAllPlatform(name: "chatroom_send_message",
                                    parameters: ["channel_id":video.channelId
            ])
    }
    
///     Social Media
    public func socialMediaShare(video:Video, platform:SocialMediaType, videoTime:(Int)) {
        self .logEventToAllPlatform(name: "social_media_share",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
                                                 "video_time":videoTime,
                                                 "platform":platform.rawValue
                                        
            ])
    }
    
///     Display Ad Engagement
    public func displayAdView(video:Video, videoTime:(Int)) {
        
        lastDisplayAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self .logEventToAllPlatform(name: "display_ad_view",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
                                                 "video_time":videoTime,
                                                 "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    
    public func displayAdClick(video:Video, videoTime:(Int)) {
        self .logEventToAllPlatform(name: "display_ad_click",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
                                                 "video_time":videoTime,
                                                 "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    public func displayAdVideoStart(video:Video, ad:OONAAd, videoTime:(Int),adDuration:Int) {
        self .logEventToAllPlatform(name: "display_ad_video_start",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
                                                 //                                                 "display_ad_id":ad.id,
                                        "video_time":videoTime,
                                        //                                                 "engagement_od":ad.engagementId,
                                        "ad_duration":ad.duration ?? 0 ,
                                        "platform":ad.platform,
                                        "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    public func displayAdVideoComplete(video:Video, ad:OONAAd, videoTime:(Int), adWatchedDuration:(Int)) {
        self .logEventToAllPlatform(name: "display_ad_video_complete_view",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
                                                 //                                                 "display_ad_id":ad.id,
                                        "video_time":videoTime,
                                        //                                                 "engagement_od":ad.engagementId,
                                        "ad_duration":adWatchedDuration ,
                                        "platform":ad.platform,
                                        "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    public func displayAdEngagementView(video:Video, ad:OONAAd, videoTime:(Int), adWatchedDuration:(Int)) {
        self .logEventToAllPlatform(name: "display_ad_engagement_view",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
//                                                 "display_ad_id":ad.id,
                                                 "video_time":videoTime,
//                                                 "engagement_od":ad.engagementId,
                                        "ad_duration":ad.duration ?? 0 ,
                                                 "platform":ad.platform,
                                                 "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    
//    public func displayAdEngagementMonetiseView(video:Video, ad:OONAAd, videoTime:(Int), adWatchedDuration:(Int)) {
//        self .logEventToAllPlatform(name: "display_ad_engagement_monetise_view",
//                                    parameters: ["channel_id":String(video.channelId),
//                                                 "episode_id":String(video.episodeId),
////                                                 "display_ad_id":ad.id,
//                                                 "video_time":String.init(videoTime),
////                                                 "engagement_od":ad.engagementId,
//                                                 "platform":ad.platform,
//                                                 "ad_duration":ad.duration?.stringValue ?? "",
//                                                 "ad_watched_duration":String.init(adWatchedDuration),
//                                                 "event_code":lastDisplayAdShowTimeStamp
//
//            ])
//    }
    public func displayAdIgnore(video:Video, ad:OONAAd, videoTime:(Int)) {
        lastDisplayAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self .logEventToAllPlatform(name: "display_ad_view",
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
//                                                 "display_ad_id":ad.id,
                                                 "video_time":String.init(videoTime),
                                                 "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    public func gameAdView(game:Game) {
        
        lastGameAdShowTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self .logEventToAllPlatform(name: "game_ad_view",
                                    parameters: ["game_id":game.id,
                                                 "event_code":lastGameAdShowTimeStamp
                                        
            ])
    }
    
    public func gameAdClick(game:Game) {
        self .logEventToAllPlatform(name: "game_ad_click",
                                    parameters: ["game_id":game.id,
                                                 "event_code":lastGameAdShowTimeStamp
            ])
    }
    public func gameAdVideoStart(game:Game, ad:OONAAd,adDuration:Int) {
        self .logEventToAllPlatform(name: "game_ad_start",
                                    parameters: ["game_id":game.id,
                                            "ad_duration":ad.duration ?? 0 ,
                                            "platform":ad.platform,
                                            "event_code":lastGameAdShowTimeStamp
                                        
            ])
    }
    public func gameAdVideoComplete(game:Game, ad:OONAAd,adDuration:Int) {
        self .logEventToAllPlatform(name: "game_ad_complete_view",
                                    parameters: ["game_id":game.id,
                                        "ad_duration":adDuration ,
                                        "platform":ad.platform,
                                        "event_code":lastGameAdShowTimeStamp
                                        
            ])
    }
    public func videoAdView(video:Video, ad:OONAAd, videoTime:(Int)) {
        lastVideoAdViewTimeStamp = DeviceHelper.shared.getCurrentTimeStamp().intValue
        self .logEventToAllPlatform(name: getvideoAdViewFirebaseEventName(ad: ad),
                                    parameters: ["channel_id":video.channelId,
                                                 "episode_id":video.episodeId,
//                                                 "display_ad_id":ad.id,
                                                 "video_time":videoTime,
                                                 "event_code":lastDisplayAdShowTimeStamp
                                        
            ])
    }
    
    public func getvideoAdViewFirebaseEventName(ad:OONAAd) -> String {
        var platformPrefix = ad.platform.lowercased() + "_"
        var adTypeString : String = ""
        var eventBaseName : String = ""
        var eventName : String = ""
        
        switch (ad.adType){
            case .preroll :
                adTypeString = "pre_roll_"
                eventBaseName = "video_ad_view"
            case .midroll :
                adTypeString = "mid_roll_"
                eventBaseName = "video_ad_view"
            case .engagement:
                adTypeString = ""
                eventBaseName = "display_ad_engagement_view"
        }
        
        if ad.platform.lowercased() == "spotx" {
            
            if ad.adType == .preroll {
                
                platformPrefix = ""
            }
            if ad.adType == .midroll {
                adTypeString = ""
                platformPrefix = ""
            }
            
        }
        eventName = platformPrefix + adTypeString + eventBaseName
        return eventName
    
    }
            
        
        
    
    
    public func logEventToAllPlatform(name:String,parameters:[String: Any]) {
        var param = parameters
        param["latitude"] = LocationHelper.shared.getLat()
        param["longitude"] = LocationHelper.shared.getLong()
        param["env"] = "Prod"
        OONAFirebase.shared.logEvent(name:name,parameters:param)
//        APIAnalytic.shared.logEvent(name:name,parameters:parameters)
    }
    
    
    
}
