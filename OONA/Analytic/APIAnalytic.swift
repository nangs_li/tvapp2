//
//  APIAnalytic.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import os.log
import Foundation


class APIAnalytic : NSObject {
    static let shared = APIAnalytic()
    var sendingEvent : Bool = false
    var events : Array<[String:Any]> = []
    public func logEvent(name:String,parameters:[String: String]) {
        NSLog("[OONA] [Analytics] API log event named: %@ \n parameters : \n %@ \n", name, parameters)
        self.addEventsToQueue(name: name, parameters: parameters)
//        Analytics.logEvent(name, parameters: parameters)
    }

    
    private func addEventsToQueue(name:String,parameters:[String: String])  {
        
        events.append(["timestamp":DeviceHelper.shared.getCurrentTimeStamp().stringValue,
                                "key":name,
                                "parameters":parameters])
        storeEventsQueueToSystem()
        
        if events.count  > APPSetting.sizeToSendAnalyticQueue {
            sentEventQueue()
        }
        
    }
    private func sentEventQueue() {
        if sendingEvent || events.count <= 0 {
            return
        }
        sendingEvent = true
        let eventToSentCount = events.count
        
        let parameter = ["events":events,
                         "device":[
                            "deviceId":"1",
                            "deviceOS":"iOS",
                            "deviceModel":"1"
                            ]
//                         "user":[
//                            "userId":String(UserData.userID()),
//                            "age":String(UserData.userAge() ),
//                            "gender":String(UserData.gender() )
//                            ]
            ] as [String : Any]

        OONAApi.shared.postRequest(url: APPURL.MultiAnalyticsEvent, parameter: parameter).responseJSON { [weak self] (response) in
           
            if response.result.isSuccess {
                os_log("multi analytic event success")
                self?.events.removeSubrange(0...eventToSentCount-1)
                self?.storeEventsQueueToSystem()
            }else{
                os_log("multi analytic event failed : %@",response.result.error?.localizedDescription ?? "")
                os_log("multi analytic event failed : %@",response.response ?? "")
            }
            self?.sendingEvent = false
        }
    }
    private func storeEventsQueueToSystem() {
        UserDefaults.standard.set(events, forKey: "oonaAnalyticEventsQueue")
    }
    private func restoreEventQueueFromSystem() {
        events = UserDefaults.standard.array(forKey: "oonaAnalyticEventsQueue") as! Array<[String:Any]>
    }
    
}
