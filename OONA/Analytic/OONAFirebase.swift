//
//  FirebaseHelper.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseRemoteConfig

class OONAFirebase : NSObject {
    static let shared = OONAFirebase()
    public func logEvent(name:String,parameters:[String: Any]) {
        NSLog("[OONA] [Analytics] Firebase log event named: %@ \n parameters : \n %@ \n", name, parameters)
        let userId = UserDefaults.standard.integer(forKey: "user-userId")
        
        Analytics.setUserProperty(OONAApi.shared.currentRegion(), forName: "user_region")
        Analytics.setUserProperty(userId.description, forName: "device_id")
        Analytics.logEvent(name, parameters: parameters)
    }
    
    func fetchConfig(complete: @escaping (_ result: String?, _ error: Error?) -> Void){
        let remoteConfig = RemoteConfig.remoteConfig();
        remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: true)

        remoteConfig.fetch(withExpirationDuration: APPSetting.firebase.fetchConfigexpirationDuration, completionHandler: {status, error in
            if (status == RemoteConfigFetchStatus.success) {
                //remoteConfig.value(forKey: APPSetting.firebase.signatureKey)
                remoteConfig.activateFetched()
                if let regSecret = remoteConfig[APPSetting.firebase.signatureKey].stringValue {
                    complete(regSecret, nil)
                    //print("remote config: \(String(describing: regSecret))")
                }else{
                    //error
                    //TODO_OONA : error handling
                }
                
            }else{
                complete(nil, error)
            }
        })
    }
}
