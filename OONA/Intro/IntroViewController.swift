//
//  PlayerViewController.swift
//  OONA
//
//  Created by Jack on 16/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import AVKit
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture
import GhostTypewriter
import SwiftyGif


class IntroViewController :PlayerViewController, SwiftyGifDelegate{
    
    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var step1Button: UIButton!
    
    @IBOutlet weak var step1Wrapper: UIView!
    
    @IBOutlet weak var step1text: TypewriterLabel!
    @IBOutlet weak var step1Gif: UIImageView!
    @IBOutlet weak var step1TapButton: UIButton!
    
    @IBOutlet weak var step2Wrapper: UIView!
    @IBOutlet weak var step2text: TypewriterLabel!
    @IBOutlet weak var step2Gif: UIImageView!
    
    @IBOutlet weak var step3Wrapper: UIView!
    @IBOutlet weak var step3text: TypewriterLabel!
    @IBOutlet weak var step3Gif: UIImageView!
    
    @IBOutlet weak var step4Wrapper: UIView!
    @IBOutlet weak var step4text: TypewriterLabel!
    @IBOutlet weak var step4Gif: UIImageView!
    
    @IBOutlet weak var step5Wrapper: UIView!
    @IBOutlet weak var step5text: TypewriterLabel!
    @IBOutlet weak var step5Gif: UIImageView!
    
    @IBOutlet weak var step6Wrapper: UIView!
    @IBOutlet weak var step6text: TypewriterLabel!
    @IBOutlet weak var step6Gif: UIImageView!
    
    @IBOutlet weak var step7Wrapper: UIView!
    @IBOutlet weak var step7text: TypewriterLabel!
    @IBOutlet weak var step7Gif: UIImageView!
    
    @IBOutlet weak var step8Wrapper: UIView!
    @IBOutlet weak var step8text: TypewriterLabel!
    @IBOutlet weak var step8Gif: UIImageView!
    
    @IBOutlet weak var step9Wrapper: UIView!
    @IBOutlet weak var step9text: TypewriterLabel!
    @IBOutlet weak var step9Gif: UIImageView!
    
    @IBOutlet weak var step10Wrapper: UIView!
    @IBOutlet weak var step10text: TypewriterLabel!
    @IBOutlet weak var step10Gif: UIImageView!
    @IBOutlet var tcoinSuccessView : UIView!
    
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var brightnessButton: UIButton!
    @IBOutlet weak var volumneButton: UIButton!
    

    

    
    var currentStep = 2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        /*
        step1Gif.animate(withGIFNamed: "gesture_tap")
        step2Gif.animate(withGIFNamed: "gesture_tap")
        step3Gif.animate(withGIFNamed: "gesture_tap")
        step4Gif.animate(withGIFNamed: "gesture_swipe-right")
        step5Gif.animate(withGIFNamed: "gesture_swipe-right")
        step6Gif.animate(withGIFNamed: "gesture_swipe-left")
        step7Gif.animate(withGIFNamed: "gesture_swipe-up")
        step8Gif.animate(withGIFNamed: "gesture_swipe-down")
        step9Gif.animate(withGIFNamed: "gesture_swipe-down")
 */
//        step4Wrapper.isHidden = false

//        step1TapButton.isHidden = true

        //self.slideForMoreTab.alpha = 1
        //self.slideForMoreText.alpha = 1
        self.initIntroPlayerView()
//        self.tutorialView.backgroundColor = Color.bgTransparantOONAColor()
        self.registerTutorialControls()
        self.engagementView.isHidden = true
//        animateControlButtons()
        
     
        do {
            let gif = try UIImage(gifName: "gesture_tap")
            step1Gif.setGifImage(gif)
            step1Gif.delegate = self
            
        } catch  {
            print(error)
        }
    }
    
    override func backToStart() {
        
        self.playVideo(url: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8")
    }
    
    @IBAction override func playerViewTapAction(_ sender: Any) {
        super.playerViewTapAction(sender)
        
        if(currentStep==3){
          //  nextStep()
        }
    }
    
//    @IBAction func step1start(_ sender: Any) {
//        nextStep()
//        step1TapButton.isHidden = true
//    }
    
    
    func hasTurtorial()-> Bool{
        if let tutorial : Bool = UserDefaults.standard.bool(forKey: "tutorial") {
            if(!tutorial){
                return true
            }
        }else{
            return true
        }
        return false
    }
    func animateControlButtons() {
        enlargeButtons()
    }
    func animateButtons(size:CGFloat) {
        let mag = size
        UIView.animate(withDuration: 0.3, animations: {
            self.brightnessButton.transform =  self.brightnessButton.transform.scaledBy(x: mag, y: mag)
              self.volumneButton.transform =  self.volumneButton.transform.scaledBy(x: mag, y: mag)
              self.bookmarkButton.transform =  self.bookmarkButton.transform.scaledBy(x: mag, y: mag)
        }) { (finished) in
            self.resetButtons()
        }
    }
    func enlargeButtons() {
        self.animateButtons(size: 1.5)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.resetButtons()
        }
    }
    func resetButtons() {
        self.animateButtons(size: 1.0)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.enlargeButtons()
        }
    }
    func registerTutorialControls() {
        
    }
    
    @IBAction func step1start(_ sender: Any) {
        nextStep()
        step1TapButton.isHidden = true
    }
    func nextStep(){
        currentStep = currentStep + 1
        
        if (currentStep == 3){
            print("step3")
            // brightness video
         
            
            initTutorialView()
            self.playBrightnessExplainerVideo()
//            step1Wrapper.isHidden = false
//            self.showControlView()
//            self.showTcoinSuccessView()
            
//            step1Wrapper.isHidden = false
            
        }else if (currentStep == 4){
            print("step4")
            // left menu
            do {
                let gif = try UIImage(gifName: "gesture_swipe-right")
                step4Gif.setGifImage(gif)
                step4Gif.delegate = self
                
            } catch  {
                print(error)
            }
            
            hideControlView()
            initTutorialView()
            self.menuUpEnable = false
            self.menuLeftEnable = true
            self.menuRightEnable = false
            self.menuDownEnable = false
            step4Wrapper.isHidden = false
            step4text.startTypewritingAnimation()
        }
        else if (currentStep == 5){
            print("step5")
            //left full menu..
            
            do {
                let gif = try UIImage(gifName: "gesture_swipe-right")
                step5Gif.setGifImage(gif)
                step5Gif.delegate = self
                
            } catch  {
                print(error)
            }
            initTutorialView()
            step5Wrapper.isHidden = false
            step5text.startTypewritingAnimation()
            
            self.menuUpEnable = false
            self.menuLeftEnable = true
            self.menuRightEnable = false
            self.menuDownEnable = false
            
            
        }
        else if (currentStep == 6){
            print("step6")
            // right share menu
            do {
                let gif = try UIImage(gifName: "gesture_swipe-left")
                step6Gif.setGifImage(gif)
                step6Gif.delegate = self
                
            } catch  {
                print(error)
            }
            
            self.initTutorialView()
            step6text.startTypewritingAnimation()
//            playtCoin()
            
            self.menuUpEnable = false
            self.menuLeftEnable = false
            self.menuRightEnable = true
            self.menuDownEnable = false
            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                self.hideLeftMenu()
//            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.step6Wrapper.isHidden = false
                
//                self.nextStep()
//            }
            
            
        }
        else if (currentStep == 7){
            print("step7")
            
            initTutorialView()
            
            //var circularVC : CircluarMenuViewController = menuVC as! CircluarMenuViewController
            //circularVC.showTutorial()
            
            //circularVC.completionBlock = {(dataReturned) -> () in
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    //wait for two seconds
//                    self.dismiss(animated: true)
                    self.initTutorialView()
                    self.hideMenu()
                    
                    
//                    self.playtCoin()
                    
                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.playtCoin()
//                    }
                    
                    //self.showTcoinSuccessView()
                })
            //}
        }
        else if (currentStep == 8){
            
            self.tcoinTabBarView.showBarForState(barState: .displayForTabBar,
                                                 animated: true)
            
            print("step8")
            initTutorialView()
            
            do {
                let gif = try UIImage(gifName: "gesture_swipe-down")
                step8Gif.setGifImage(gif)
                step8Gif.delegate = self
                
            } catch  {
                print(error)
            }
            
            step8Wrapper.isHidden = false
            step8text.startTypewritingAnimation()
            
            
            //self.showControlView()
            
            self.menuUpEnable = false
            self.menuLeftEnable = false
            self.menuRightEnable = false
            self.menuDownEnable = true
            
        }
        else if (currentStep == 9){
            
            print("step9")
            initTutorialView()
            
            do {
                let gif = try UIImage(gifName: "gesture_swipe-down")
                step9Gif.setGifImage(gif)
                step9Gif.delegate = self
                
            } catch  {
                print(error)
            }
            
            step9Wrapper.isHidden = false
            step9text.text = "Slide down one more time \nfor full access"
            step9text.startTypewritingAnimation()
            //self.showControlView()
            
            self.menuUpEnable = false
            self.menuLeftEnable = false
            self.menuRightEnable = false
            self.menuDownEnable = true
            
            
            //ghost type writer
            
            
        }
        else if (currentStep == 10){
            
            print("step10")
            initTutorialView()
            
            do {
                let gif = try UIImage(gifName: "gesture_swipe-up")
                step10Gif.setGifImage(gif)
                step10Gif.delegate = self
                
            } catch  {
                print(error)
            }
            step10Wrapper.isHidden = false
            step10text.startTypewritingAnimation()
            
            self.menuUpEnable = true
            self.menuLeftEnable = false
            self.menuRightEnable = false
            self.menuDownEnable = false
            
            /*
             step8Wrapper.isHidden = false
             
             self.menuUpEnable = false
             self.menuLeftEnable = false
             self.menuRightEnable = false
             self.menuDownEnable = true*/
            
        }
        else if (currentStep == 11){
            print("step11")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hideMenu()
                self.initTutorialView()
                self.playtCoin()
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                    self.nextStep()
//                }
            }
            self.menuUpEnable = false
            self.menuLeftEnable = false
            self.menuRightEnable = false
            self.menuDownEnable = false
            
            //end tutorial
        }
        else{
            /*
            let vc = UIStoryboard.init(name: "PlayerViewController", bundle: nil).instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            */
            let storyBoard: UIStoryboard = UIStoryboard(name: "PlayerViewController", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            self.present(newViewController, animated: false, completion: nil)
            
            //self.dismiss(animated: true)
        }
    }
    
    
    override func showMenu(menuType: MenuType, subMenu: SubMenu, youtubeMode:Bool){
        
        super.showMenu(menuType: menuType, subMenu: subMenu, youtubeMode: youtubeMode)
        
        if(menuType == .up){
            nextStep()
        }
        
    }
    override func handlePanGestureLeftMenu(gestureRecognizer: UIPanGestureRecognizer){

    }
    override func handleSwipeGestureLeftMenu(){
        if(currentStep==4){
            showLeftMenu(leftMenuType: .half)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.nextStep()
            }
        }else if(currentStep==5){
            showLeftMenu(leftMenuType: .full)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.playtCoin()
            }
//            nextStep()
        }
    }
    
    
    
    func initTutorialView(){
//        step1Wrapper.isHidden = true
        //step2Wrapper.isHidden = true
        //step3Wrapper.isHidden = true
        step4Wrapper.isHidden = true
        step5Wrapper.isHidden = true
        step6Wrapper.isHidden = true
        step7Wrapper.isHidden = true
        step8Wrapper.isHidden = true
        step9Wrapper.isHidden = true
        step1Gif.isHidden = true
        
        //tcoinSuccessView.isHidden = true
    }
    
    
    override func getRightMenu(){
        //if (self.leftViewWidth.constant == self.initialX){
        //step7
            self.initView()
        
        self.showMenu(menuType: .right, subMenu: .none,youtubeMode:false)
            nextStep()
        //}
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.nextStep()
//        }
//
    }
    
    func initIntroPlayerView() {
        
        //self.tcoinSuccessView.layer.addSublayer(playerLayer)
//
//        introPlayer = AVPlayer.init(playerItem: playerItem)
//        introPlayerLayer = AVPlayerLayer (player: player)
//        introPlayerLayer.videoGravity = .resizeAspect
//        introPlayerLayer.frame = self.view.bounds
//        introPlayerLayer.zPosition = -1
        
        //self.view.layer.addSublayer(introPlayerLayer)
       // self.tcoinSuccessView.layer.addSublayer(introPlayerLayer)
        
//        self.tcoinSuccessView.layer.addSublayer(playerLayer)
//
//        introPlayer = AVPlayer.init(playerItem: introPlayerItem)
//        introPlayerLayer = AVPlayerLayer (player: introPlayer)
//        introPlayerLayer.videoGravity = .resizeAspect
//        introPlayerLayer.frame = self.view.bounds
//        introPlayerLayer.zPosition = -1
//
//
//        self.view.layer.addSublayer(introPlayerLayer)
    }
    
    func showTcoinSuccessView(){
        //tcoinSuccessView.isHidden = false
        
        playtCoin()
    }
    override func showControlView() {
        if (currentStep == 1){
           self.nextStep()
        }
//        if (currentStep != 0){
//         super.showControlView()
//        }
    }
    
    func playtCoin() {
        
        tcoinSuccessView.isHidden = false
        tcoinSuccessView.backgroundColor = UIColor.black
        introPlayerLayer.removeFromSuperlayer()
        self.tcoinSuccessView.isHidden = false
        let path = Bundle.main.path(forResource: "explainer-game-congrats-100", ofType:"mp4")
        introPlayer = AVPlayer.init(url: URL.init(fileURLWithPath: path!))
        introPlayerLayer = AVPlayerLayer (player: introPlayer)
        introPlayerLayer.videoGravity = .resizeAspect
        introPlayerLayer.frame = self.tcoinSuccessView.bounds
        introPlayerLayer.zPosition = -1
        
        self.tcoinSuccessView.layer.addSublayer(introPlayerLayer)
        introPlayer.play()
        
        
        let vm = PlayerViewModel.init(player: introPlayer)
        vm.didPlayToEnd
            .drive(onNext: { [unowned self] _ in
                NSLog("tcoin did play to end")
                self.tcoinSuccessView.isHidden = true
                self.nextStep()
//                self.playtCoin()
            })
            .disposed(by: bag)
    }
    
    func playBrightnessExplainerVideo() {
        
        introPlayerLayer.removeFromSuperlayer()
        self.tcoinSuccessView.isHidden = false
        let path = Bundle.main.path(forResource: "explainer-brightness", ofType:"mp4")
        introPlayer = AVPlayer.init(url: URL.init(fileURLWithPath: path!))
        introPlayerLayer = AVPlayerLayer (player: introPlayer)
        introPlayerLayer.videoGravity = .resizeAspect
        introPlayerLayer.frame = self.tcoinSuccessView.bounds
        introPlayerLayer.zPosition = -1
        
        let vm = PlayerViewModel.init(player: introPlayer)
        vm.didPlayToEnd
            .drive(onNext: { [unowned self] _ in
                NSLog("brightness did play to end")
                self.tcoinSuccessView.isHidden = true
                self.playtCoin()
            })
            .disposed(by: bag)
        
        self.tcoinSuccessView.layer.addSublayer(introPlayerLayer)
        introPlayer.play()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            //            self.introPlayerLayer.removeFromSuperlayer()
//            self.tcoinSuccessView.isHidden = true
//        }
    }

    override func initPlayerView() {
        super.initPlayerView()
        
        
    }
    override func bcoAdvanceTo () {
        super.bcoAdvanceTo()
        player.isMuted = true
        
    }
    override func showtcoinsBarStep1(){
        //TODO_OONA : refator
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.nextStep()
        }
//        nextStep()
    }
    override func showtcoinsBarStep2(){
        //TODO_OONA : refator
        self.initTutorialView()
        //done step
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
//            self.tcoinView.alpha = 0
//            self.tcoinsDetailView.alpha = 0
//            self.tcoinBarTop.constant = 0
//
            
            
            self.playtCoin()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.hideControlView()
            })
            
        }
        
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            //wait for two seconds
        })*/
    }

}





