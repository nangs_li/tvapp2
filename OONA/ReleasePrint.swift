//
//  ReleasePrint.swift
//  OONA
//
//  Created by nicholas on 8/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

#if /*!arch(x86_64) && !arch(i386) || */RELEASE
public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {}
public func debugPrint(_ items: Any..., separator: String = " ", terminator: String = "\n") {}
public func debugPrint<Target>(_ items: Any..., separator: String = " ", terminator: String = "\n", to output: inout Target) where Target : TextOutputStream {}
public func print<Target>(_ items: Any..., separator: String = " ", terminator: String = "\n", to output: inout Target) where Target : TextOutputStream {}
#endif
