//
//  NetworkModule.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import CoreLocation
import RxLocalizer
import RxSwift



class LanguageHelper: NSObject {
    
    static let shared = LanguageHelper()
    
    
    func getCurrentLanguageCode() -> String {
        return getSystemLocaleCode()
    }
    
    func getSystemLocaleCode() -> String {
        let language = NSLocale.preferredLanguages[0]
        var languageDic = NSLocale.components(fromLocaleIdentifier: language)
        let languageCode: String  = languageDic["kCFLocaleLanguageCodeKey"] ?? ""
        return languageCode
    }
    
    func appLanguageDetail() -> String {
        if UserDefaults.standard.value(forKey: "selected_language") != nil {
            guard let localizedCode: String = UserDefaults.standard.string(forKey: "selected_language") else { return Language.english.detailString }
            return Language(rawValue: localizedCode)?.detailString ?? Language.english.detailString
        }else{
            //default: english
            self.changeLanguage(lang: .english)
            return Language.english.detailString
        }
        
    }
    
    func getAppLanguage() -> Language {
        if let languageCode = UserDefaults.standard.string(forKey: "selected_language"),
            let language = Language(rawValue: languageCode) {
            return language
        }else{
            //default: english
            self.changeLanguage(lang: .english)
            return Language.english
        }
    }
    
    func changeLanguage(lang: Language) {
        UserDefaults.standard.set(lang.description, forKey: "selected_language")
        Localizer.shared.changeLanguage.accept(lang.description)
    }
    
    static func localizedString(_ localizeKey: String) -> String {
        return Localizer.shared.localized(localizeKey)
    }
        
        //stringArray=[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:langToLoad ofType:@"strings"]];
        

/*
    + (void)subscribeTopic:(Language)lang {
    if ([APP_ENV isEqualToString:@"dev"]){
    NSString *  DEBUG_HK_KEY = @"debug-HongKong";
    [[FIRMessaging messaging] subscribeToTopic:DEBUG_HK_KEY
    completion:^(NSError * _Nullable error) {
    NSLog(@"Subscribed to topic:%@",DEBUG_HK_KEY);
    }];
    }
    
    
    NSString *topicToSub = @"";
    NSString *topicToUnSub = @"";
    NSString *generalTopicToSub = @"";
    NSString *generalTopicToUnSub = @"";
    if (lang == LanguageEnglish){
    topicToSub = enTopic;
    topicToUnSub = idTopic;
    generalTopicToSub = enGeneralTopic;
    generalTopicToUnSub = idGeneralTopic;
    }else{
    topicToSub = idTopic;
    topicToUnSub = enTopic;
    generalTopicToUnSub = enGeneralTopic;
    generalTopicToSub = idGeneralTopic;
    }
    
    //    [[FIRMessaging messaging] uns
    [[FIRMessaging messaging] subscribeToTopic:topicToSub
    completion:^(NSError * _Nullable error) {
    NSLog(@"Subscribed to topic:%@",topicToSub);
    }];
    
    [[FIRMessaging messaging] unsubscribeFromTopic:topicToUnSub
    completion:^(NSError * _Nullable error) {
    NSLog(@"Unsubscribed to topic:%@",topicToUnSub);
    }];
    
    [[FIRMessaging messaging] subscribeToTopic:generalTopicToSub
    completion:^(NSError * _Nullable error) {
    NSLog(@"Subscribed to topic:%@",generalTopicToSub);
    }];
    
    [[FIRMessaging messaging] unsubscribeFromTopic:generalTopicToUnSub
    completion:^(NSError * _Nullable error) {
    NSLog(@"Unsubscribed to topic:%@",generalTopicToUnSub);
    }];
    
    
    }
    */
    
}

extension String {
    //TODO: prase or remove html
    
//    init?(htmlEncodedString: String) {
//
//        guard let data = htmlEncodedString.data(using: .utf8) else {
//            return nil
//        }
//
//        let options: [String: Any] = [
//            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
//            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
//        ]
//
//        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
//            return nil
//        }
//
//        self.init(attributedString.string)
//    }
    func escapeHTMLEntities() -> String {
        guard self.count > 0 else { return self }
        let noTagString = self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        let noEntitiesString = noTagString.replacingOccurrences(of: "&[^;]+;", with: "", options: String.CompareOptions.regularExpression, range: nil)
        return noEntitiesString
        //    let noTagString = self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        //    let b = a.replacingOccurrences(of: "&[^;]+;", with: "", options: String.CompareOptions.regularExpression, range: nil)
    }
    
}
