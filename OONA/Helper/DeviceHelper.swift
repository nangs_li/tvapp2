//
//  NetworkModule.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import AdSupport
import CoreTelephony
import FirebaseMessaging
import SDWebImage
import SwiftyJSON
import RxSwift
import RxCocoa
import JGProgressHUD
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

class DeviceHelper: NSObject {
    static let shared = DeviceHelper()
    
    var inviteCodeEntered : PublishRelay<Bool> = PublishRelay<Bool>()
    let firebaseConfigFetched = false;
    let UUID = UIDevice.current.identifierForVendor?.uuidString
//    var accessToken = UIDevice.current.identifierForVendor?.uuidString
    let advertisingIdentifier = ASIdentifierManager.shared().advertisingIdentifier.uuidString
    let timeZoneName = TimeZone.current.identifier
    var isFailsafeAPIReset = false
    var isFailsafeENVReset = false
    var isEssentialUiOnly = false
    var isAdFree = false
    var bidImageUrl : String = ""
    var storeImageUrl : String = ""
    var bidThumbnailImageUrl : String = ""
    var geoBlock : Bool = false
    var watchButtonJson : JSON = JSON.init(arrayLiteral: [])
    var allowInputTransferCode : Bool = false
    var serverTime : Date = Date()
    
    func registerDevice(regSecret: String , success : @escaping (NSDictionary) -> (), failure : ((NSError) -> ())? = nil)  {
        let param = getDeviceParam(regSecret: regSecret)
        OONAApi.shared.postRequest(url: APPURL.RegisterDevice, parameter: param).responseJSON { (response) in
            
            if(response.result.isSuccess){
                
                if let JSONDict = response.result.value as! NSDictionary? {
                    print("[RegDevice]",JSONDict)
                    let code : Int? = JSONDict["code"] as? Int
                    let status : String? = JSONDict["status"] as? String
                    
                    if code == 199999 {
                        self.isEssentialUiOnly = true
                        self.isAdFree = true
                    }else{
                        self.isEssentialUiOnly = false
                        self.isAdFree = false
                    }
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            print("alert dict",JSONDict["alert"])
                            if let alert = JSONDict["alert"] as? [String:Any] {
                                let al = JSON(alert)
                                self.showAlert(al)
                            }
                            if(_success){
                                let responseObject = JSONDict
                                var showNotice : Bool = false
                                
                                if responseObject["notice"] != nil {
                                    let notice = JSON(responseObject["notice"])
                                    
                                    let noticeID = notice[0]["id"].stringValue
                                        if UserDefaults.standard.object(forKey: "notice_id") == nil {
                                            print("no noticeid, show ")
                                            UserDefaults.standard.set(noticeID, forKey: "notice_id")
                                            showNotice = true
                                        } else {
                                            if !(noticeID == UserDefaults.standard.string(forKey: "notice_id")) {
                                                print("have noticeid but not same, need show and update id")
                                                showNotice = true
                                                UserDefaults.standard.set(noticeID, forKey: "notice_id")
                                            } else {
                                                if noticeID != "" {
                                                    showNotice = false
                                                    print("already show this notice \(noticeID)")
                                                } else {
                                                    showNotice = true
                                                    print("notice id nil, just show and do nothing")
                                                }
                                            }
                                        }
                                    
                                }

                                if (showNotice){
                                       
                                      
                                       UserDefaults.standard.set(NSNumber(value: true), forKey: "notice_need_show")
                                       UserDefaults.standard.set(responseObject["notice"], forKey: "notice_obj")
//                                       NotificationCenter.default.post(name: NSNotification.Name("haveNotice"), object: responseObject["notice"])

                                   }else{
                                       
                                   }
                                
                                
                                if let jsonObj = (try? JSON(data: response.data!)) {
                                    let brightcove = jsonObj["brightcove"]
                                    
                                    if let accountId = brightcove["accountId"].string, let policyKey = brightcove["policyKey"].string {
                                        print ("new bco",accountId,policyKey)
                                         PlayerHelper.shared.setBrightcove(accountId: accountId, policyKey: policyKey)
                                    }
                                    if let _gBlock = jsonObj["geoBlock"].bool  {
                                        
                                        DeviceHelper.shared.geoBlock = _gBlock
                                    }
                                    DeviceHelper.shared.allowInputTransferCode = false
                                    if let _alloctransfercode = jsonObj["allowInputTransferCode"].int  {
                                        if (_alloctransfercode == 1) {
                                            DeviceHelper.shared.allowInputTransferCode = true
                                        }
                                    }
                                    
                                        
                                    
                                    let bid = jsonObj["bid"]
                                    if let bidImageUrl = bid["imageUrl"].string  {
                                        print ("bid image",bidImageUrl)
                                        DeviceHelper.shared.bidImageUrl = bidImageUrl
                                    }
                                    let store = jsonObj["store"]
                                    if let storeImageUrl = store["imageUrl"].string  {
                                                                           print ("store image",storeImageUrl)
                                                                           DeviceHelper.shared.storeImageUrl = storeImageUrl
                                                                       }
                                    if let _bidThumbnailImageUrl = bid["thumbnailUrl"].string  {
                                        print ("bid thumbnailUrl",_bidThumbnailImageUrl)
                                        DeviceHelper.shared.bidThumbnailImageUrl = _bidThumbnailImageUrl
                                    }
                                    self.watchButtonJson = bid["watchButton"]
                                    
                                    // thumbnailUrl
                                    
                                    
                                    
//                                    bid =     {
//                                        imageUrl = "https://static-ind.oonatv.com/bids/bid-image-2019-08.jpg";
//                                    };
                                    
                                }
//                                if let apiUrl: String = JSON["apiUrl"] as? String{r
//                                    //fail safe handling
//                                    if(!self.isFailsafeENVReset){
//                                        APPURL.Domain = apiUrl
//                                        self.isFailsafeENVReset = true
//                                        //redo the whole thing
//                                        self.registerDevice(regSecret: regSecret, success: success, failure: failure)
//                                    }
//                                }
                                if let deviceId: Int = JSONDict["deviceId"] as? Int{
                                    UserHelper.shared.setDeviceID(deviceId: deviceId);
                                }
                                
                                if let prefetchImages : NSArray = JSONDict["prefetchImages"] as? NSArray{
                                    var arrImageUrl :[URL] = [URL]();
                                    for imageUrlString in prefetchImages {
                                        if let _imageUrlString : String = imageUrlString as? String{
                                            if let url = URL(string:_imageUrlString){
                                                arrImageUrl.append(url);
                                            }
                                        }
                                    }
                                    SDWebImagePrefetcher.shared.prefetchURLs(arrImageUrl, progress: nil, completed: { finishedCount, skippedCount in
                                    })

                                }
                                if let enteredReferralCode : Int = JSONDict["enteredReferralCode"] as? Int{
                                    let result = (enteredReferralCode == 1) ? true : false
                                    UserDefaults.standard.set(result, forKey: "reg_device_referal_code_entered")
                                }
                                if let totalDailyNews : Int = JSONDict["totalDailyNews"] as? Int { UserDefaults.standard.set(totalDailyNews , forKey: "total_daily_news_count")
                                }
                                if let unreadNotification : Int = JSONDict["unreadNotification"] as? Int { UserDefaults.standard.set(unreadNotification , forKey: "unreadNotification")
                                }
                                
                                //notifications
                                if let noticeArr : [NSDictionary] = JSONDict["notice"] as? [NSDictionary] {
                                    for noticelist : NSDictionary in noticeArr{
                                        //if let id : Int = noticelist["id"] as? Int{
                                        //TODO_OONA : show notice
                                        //}
                                    }
                                }
                                
                                if let currentTime : String = JSONDict["currentTime"] as? String {
                                    //store
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//                                    dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                                    let date = dateFormatter.date(from:currentTime)!
                                        
                                    
                                    self.serverTime = date
                                    UserDefaults.standard.set(currentTime, forKey: "server_date_string")
                                    
                                    if let serverDate: String = UserDefaults.standard.object(forKey: "server_date_string") as? String {
                                        //have server date
                                        if(!UtilityHelper.checkIfSameDate(stringDate1: currentTime, stringDate2: serverDate)){
                                            //TODO_OONA : reload
                                            //[[NSNotificationCenter defaultCenter]
                                            //   postNotificationName:@"reloadMenu"
                                            //    object:nil];
                                            
                                        }
                                    }
                                }
                                
                                if let bid : NSDictionary = JSONDict["bid"] as? NSDictionary {
                                    if bid["result"] != nil{
                                        UserDefaults.standard.set(true, forKey: "bid_result_need_show")
                                        UserDefaults.standard.set(bid["result"] , forKey: "bid_result_obj")
                                        
                                        //[[NSNotificationCenter defaultCenter] postNotificationName:@"haveBidSuccess" object:responseObject[@"bid"][@"result"]];
                                        //TODO_OONA: RX - haveBidSuccess
                                    }else{
                                        UserDefaults.standard.set(false, forKey: "bid_result_need_show")
                                    }
                                }else{
                                    UserDefaults.standard.set(false, forKey: "bid_result_need_show")
                                }
                                
                                //TODO_OONA : reload menu
                                //
                                //[[NSUserDefaults standardUserDefaults] synchronize];
                                //[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMenu" object:nil];
                            }else{
                                print("Register device error response code",code)
                                //TODO_OONA : Error handling : wrong response code.
                            }
                            
                            success(JSONDict)
                            
                        });
                        
                    }else{
                        print("Register device error",response.result.error?.localizedDescription)
                        //TODO_OONA : Error handling
                    }
                }
            }
        }
    }
        
    
    func showNotice(noticeObj: NSDictionary){
        //store
        UserDefaults.standard.set(true , forKey: "notice_need_show")
        UserDefaults.standard.set(noticeObj , forKey: "notice_obj")
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"haveNotice" object:responseObject[@"notice"]];
        //TODO : notice inm switch to RX
    }
    
    func showAlert(_ alert: JSON) {
        print("alert: ",alert)
        let popup = CustomPopup(message: alert["content"].stringValue)

        popup!.info.isScrollEnabled = false
        popup!.backBtn.isHidden = true
        let buttonArray = alert["buttons"].arrayValue

    
     
        if (buttonArray.count == 0) || (buttonArray == nil) {
            popup?.button1.isHidden = true
            popup?.button2.isHidden = true
            popup?.setupZeroButton()
        }
        if (buttonArray.count ) >= 1 {
            //        print()

            popup!.button1.setTitle(buttonArray[0]["label"].stringValue, for: .normal)
            popup!.completionButton1 = {
                if let needDismiss = buttonArray[0]["clickToDismiss"].int {
                    if needDismiss == 1 {
                        popup?.dismiss()
                    }
                }
                

                let HUD = JGProgressHUD(style: .dark)
                HUD.textLabel.text = "Copied to clipboard"
                HUD.indicatorView = JGProgressHUDSuccessIndicatorView()
                let mainWindow = UIApplication.shared.keyWindow
                HUD.show(in: mainWindow!)
                HUD.dismiss(afterDelay: 2.0)

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.action(by: URL(string: alert["buttons"][0]["action"].stringValue ?? ""))
                })
            }
            popup!.setupOneButton()
            //    popup.button1.hidden = NO ;
        } else {

        }
        if (buttonArray.count) >= 2 {

            popup!.button2.setTitle(buttonArray[1]["label"].stringValue, for: .normal)
            popup!.completionButton2 = {
                 if let needDismiss = buttonArray[1]["clickToDismiss"].int {
                                   if needDismiss == 1 {
                                       popup?.dismiss()
                                   }
                               }
                               
                self.action(by: URL(string: alert["buttons"][1]["action"].stringValue ?? ""))
                
                
                popup!.setupTwoButton()
                //    popup.button1.hidden = NO ;


            }
               
        }
        let mainWindow = UIApplication.shared.keyWindow
        mainWindow?.addSubview(popup!)
    }
  func action(by actionUrl: URL?) {
      var actionUrl = actionUrl

      print("action url: \(actionUrl?.absoluteString ?? "")")

      if let path = actionUrl?.pathComponents {
          print("\(path)")
      }

      if (actionUrl?.host == "webview") {
        if let link = (actionUrl?.path as NSString?)?.substring(from: 1) {
            webview(url:link)
        }
      }
    if (actionUrl?.host == "openWebUrl") {
      if let link = (actionUrl?.path as NSString?)?.substring(from: 1) {
          openWebUrl(url:link)
      }
    }
    if (actionUrl?.host == "bids") {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            PlayerHelper.shared.showBidPage.accept(true)
        }
        
    }
  }
    func openWebUrl(url:String) {
           UIApplication.shared.openURL(URL.init(string: url)!)
//        UIApplication.shared.open(URL.init(string: url)!, options: nil, completionHandler: nil)
       }
    func webview(url:String) {
//        UIApplication.shared.open(URL.init(string: url)!, options: nil, completionHandler: nil)
//        UIApplication.shared.openURL(URL.init(string: url)!)
        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        vc.privacyURL = URL(string: url)
        vc.modalTransitionStyle = .crossDissolve
        vc.title = ""
        PlayerHelper.shared.presentVC(vc: vc)

    }

    public func getDeviceParam(regSecret : String) -> [String:String] {
        
        let isAdvertisingTrackingEnabled = ASIdentifierManager.shared().isAdvertisingTrackingEnabled
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        var  param : [String : String] = [
                "deviceOS": "iOS",
                "deviceModel": getHardwareString(),
                "osVersion": UIDevice.current.systemVersion,
                "appLanguage": LanguageHelper.shared.getAppLanguage().rawValue,
                "osLanguage": LanguageHelper.shared.getSystemLocaleCode(),
                "latitude": LocationHelper.shared.getLat(),
                "longitude": LocationHelper.shared.getLong(),
                "screenWidth": screenWidth.description,
                "screenHeight": screenHeight.description,
                "carrier": getCarrierName(),
                "carrierId": getCarrierID(),
                "appVersion": getVersionNumber(),
                "timeZone": getTimeZoneID(),
                "timeZoneName": timeZoneName,
                "advertisingIdentifier": advertisingIdentifier,
                "isAdvertisingTrackingEnabled": isAdvertisingTrackingEnabled ? "1" : "0" //convert to integer
                ]
        //check if jail broken
        if(self.getIsJailBroken()){
            param["isRooted"] = "1"
        }
        //set firebase token
        if Messaging.messaging().fcmToken != nil {
            param["pushNotificationToken"] = Messaging.messaging().fcmToken
        }
        //get app secret
        param["chimmeng"] = regSecret
//        return param
        //if encrption is success , return encrypted data , if not , return original dictionary.
        print("register device param",param)
        return getEncryptedData(param: param) ?? param
    
    }
    func getEncryptedData(param : [String : String]) -> [String : String]?{
    
        var paramData: Data? = nil
        do {
            paramData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        } catch {
            //@data error
        }
        
        let aesKey : String  = self.random256BitKey()
        
        let public_key_path = Bundle.main.path(forResource: "public_key.der", ofType: nil)
        
        let encryptStr :String = RSAEncryptor.encryptString(aesKey, publicKeyWithContentsOfFile: public_key_path)
        
        let encryptedData :String =  AESCrypt.encryptData(paramData, password: aesKey)
        
        var encryptedDict : [String : String]? = nil
        
        if(encryptedData != nil && encryptStr != nil && encryptedData != "" && encryptStr != ""){
            encryptedDict = [
                "encryptedData": encryptedData ?? "",
                "key": encryptStr ?? ""
            ]
        }
        
        return encryptedDict
        
    }
    func random256BitKey() -> String {
        var key = ""
        var randomBytes = [UInt8](repeating: 0, count: 16)
        let result = SecRandomCopyBytes(kSecRandomDefault, 16, &randomBytes)
        if result == 0 {
            key = String(repeating: "\0", count: 16 * 2)
            for index in 0..<16 {
                key += String(format: "%02x", randomBytes[index])
            }
        } else {
            print("SecRandomCopyBytes failed for some reason")
        }
        
        print("generated string : \(key)")

        return key;
    }
    func getIsJailBroken() ->Bool{
        if TARGET_IPHONE_SIMULATOR != 1
        {
            // Check 1 : existence of files that are common for jailbroken devices
            
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                
                || FileManager.default.fileExists(atPath: "/bin/bash")
                
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                
                || FileManager.default.fileExists(atPath: "/etc/apt")
                
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                //disable to make the whole class run on background thread
                //|| UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!)
                
            {
                return true
            }
            
            // Check 2 : Reading and writing in system directories (sandbox violation)
            
            let stringToWrite = "Jailbreak Test"
            
            do
            {
                try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                //Device is jailbroken
                return true
            }catch
            {
                return false
            }
        }else
        {
            return false
        }
        
    }
    
    func getCurrentTimeStamp() -> (NSNumber) {
        return NSNumber.init(value:Date().toMillis())
    }
//    - (NSNumber *)getTimeStampNow {
//    NSTimeInterval time = ([[NSDate date] timeIntervalSince1970]); // returned as a double
//    NSLog(@"Time: %f",time);
//    if ( [[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] != nil){
//    NSTimeInterval timeDiff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] floatValue];
//    time += timeDiff;
//    NSLog(@"Time Diff: %f",timeDiff);
//
//    }
//
//
//    NSLog(@"Time: %f",time);
//    //    NSDate *serverTime = [[NSDate date]dateByAddingTimeInterval:timeDiff];
//    //    NSLog(@"server time: %@ , local time: %@",serverTime,[NSDate date]);
//
//    unsigned long long digits = (unsigned long long)time; // this is the first 10 digits
//    unsigned long long decimalDigits = (unsigned long long)(fmod(time, 1) * 1000); // this will get the 3 missing digits
//    unsigned long long timestamp = (digits * 1000) + decimalDigits;
//
//    //    NSString *timestampString = [NSString stringWithFormat:@"%ld%d",digits ,decimalDigits];
//
//    return [NSNumber numberWithUnsignedLongLong:timestamp] ;
//    }
    func getTimeZoneID() -> String{
        var result = ""
        if let tz = TimeZone.current.abbreviation(){
            //input format : GMT+8  output format : +8
            if (tz.contains("GMT")){
                result = tz.replacingOccurrences(of: "GMT", with: "")
            }
        }
        return result
    }

    func getVersionNumber() -> String{
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "";
    }

    func getHardwareString() -> String {
        return "testing-device"
//        return NetworkModule.hardwareString()
//
//        var size = 0
//        sysctlbyname("hw.machine", nil, &size, nil, 0)
//        var machine = [CChar](repeating: 0,  count: size)
//        sysctlbyname("hw.machine", &machine, &size, nil, 0)
//        return String(cString: machine)
    }


    func getCarrier() -> CTCarrier?{
        let networkInfo = CTTelephonyNetworkInfo()
        return networkInfo.subscriberCellularProvider ?? nil
    }
    
    func getCarrierName() -> String{
        if let carrier = getCarrier(){
            return carrier.carrierName ?? ""
        }else{
            return ""
        }
        
    }
    func getCarrierID() -> String{
        if let carrier = getCarrier(){
            let mcc = carrier.mobileCountryCode
            let mnc = carrier.mobileNetworkCode
            let iOSCarrierID = "\(mcc ?? "")\(mnc ?? "")"
            return iOSCarrierID
        }else{
            return ""
        }
    }
    
}

