//
//  NetworkModule.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import CoreLocation

class LocationHelper: NSObject {
    static let shared = LocationHelper()
    
    let firebaseConfigFetched = false
    var cllLocationManager = CLLocationManager()
    var lat = ""
    var long = ""
    
    public override init() {
        super.init()
        self.cllLocationManager.distanceFilter = kCLDistanceFilterNone
        self.cllLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters // 100 m
        if Float(UIDevice.current.systemVersion) ?? 0.0 >= 8.0 && CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
        } else {
            self.cllLocationManager.startUpdatingLocation() //Will update location immediately
        }
        
        _ = self.getLat();
        _ = self.getLong();
        
    }
    
    func getLat() -> String {
        self.cllLocationManager.distanceFilter = kCLDistanceFilterNone
        self.cllLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters // 100 m
        
        if Float(UIDevice.current.systemVersion) ?? 0.0 >= 8.0 && CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            print("lat not in use permi")
        } else {
            print("lat start update")
            self.cllLocationManager.startUpdatingLocation() //Will update location immediately
        }
        
        if CLLocationManager.authorizationStatus() == .denied {
            print("User denied GPS long")
        }
        
        let latitude = String(self.cllLocationManager.location?.coordinate.latitude ?? 0)
        
        return latitude;
    }
    
    func getLong()  -> String{
        self.cllLocationManager.distanceFilter = kCLDistanceFilterNone
        self.cllLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters // 100 m
        if Float(UIDevice.current.systemVersion) ?? 0.0 >= 8.0 && CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            print("long not in use permi")
        } else {
            self.cllLocationManager.startUpdatingLocation()
            print("long start update")
        }
        if CLLocationManager.authorizationStatus() == .denied {
            print("User denied GPS long")
        }
        
        let longitude = String(cllLocationManager.location?.coordinate.longitude ?? 0)
        
        self.long = longitude;
        return longitude;
    }

    


}
