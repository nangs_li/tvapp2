//
//  UtilityHelper
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
class UtilityHelper: NSObject {
    static let shared = UtilityHelper()
    
    class func checkIfSameDate(stringDate1: String, stringDate2: String) -> Bool {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var date1result: Date
        var date2result: Date
        if let date1: Date = dateFormat.date(from: stringDate1){
            date1result = date1
        }else{
            return false
        }
        if let date2: Date = dateFormat.date(from: stringDate2){
            date2result = date2
        }else{
            return false
        }
        
        return Calendar.current.isDate(date1result, inSameDayAs: date2result)
        
    }
    
    func processURL(actionUrl: URL) {
        if actionUrl.host == "website"{
            //OONA_TODO: EPISODE
            ///let link: String = actionUrl.path.substring(from: 1){
                //webview(link)
            //}
            
            let link = actionUrl.path
            
            //webview(link)
        }
        if (actionUrl.host == "channel") {
            let urlComponents = actionUrl.pathComponents
            if urlComponents.count == 2 {
                // open channel only
                //OONA_TODO_CHANNEL
                //openChannelNoMoodBar("\(urlComponents?[1])")
            }
            if urlComponents.count == 4 {
                // open ep
                
                //OONA_TODO: EPISODE
                //openEpisode(withChannelID: "\(urlComponents?[1])", episodeID: "\(urlComponents?[3])")
            }
        }
        if (actionUrl.host == "bids") {
            //goToBid()
            //OONA_TODO: BID
        }
        if (actionUrl.host == "gifts") {
            //showTcoins()
            //OONA_TODO: BID
        }
    }
    
    func formateredNumber(with number: Int?) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(for: number) ?? "-"
    }
}


extension String {
    func esimatedheight(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func esimatedWidth(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension UIButton {
    func checkPointInSameVerticalRange(point:CGPoint) -> Bool {
        
        
        let currentY = point.y
        let topY = self.frame.minY
        let botY = self.frame.maxY
        
        return (currentY > topY && currentY < botY)
    }
}

extension UIDevice {
    var hasNotch: Bool {
//        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
//        return bottom > 0
        return (self.screenType == .iPhone_XR || self.screenType == .iPhones_X_XS || self.screenType == .iPhone_XSMax)
    }
    
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
}
