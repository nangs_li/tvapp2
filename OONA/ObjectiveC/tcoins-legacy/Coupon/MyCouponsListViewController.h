//
//  MyCouponsListViewController.h
//  OONA
//
//  Created by OONA iOS on 1/8/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *catalogListCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIView *informationView;
@property (weak, nonatomic) IBOutlet UILabel *screenTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tcoinsLabel;
@property (weak, nonatomic) IBOutlet UIButton *backToNotPlayBtn;

- (IBAction)backButtonAction:(id)sender;

- (IBAction)informationButtonAction:(id)sender;
@end
