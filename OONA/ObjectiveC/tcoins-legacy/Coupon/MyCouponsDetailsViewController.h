//
//  MyCouponsDetailsViewController.h
//  OONA
//
//  Created by OONA iOS on 1/15/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponsDetailsViewController : UIViewController
@property (nonatomic) NSDictionary *couponDetails;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet UILabel *screenTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *detailsImage;
@property (weak, nonatomic) IBOutlet UILabel *detailsTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *couponDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *couponDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *couponHowToUseButton;
@property (weak, nonatomic) IBOutlet UIButton *couponTNCButton;
@property (weak, nonatomic) IBOutlet UIImageView *scratchOverlayImageView;
@property (weak, nonatomic) IBOutlet UILabel *expireInLabel;
@property (weak, nonatomic) IBOutlet UILabel *voucherCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *voucherCodeTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backToNotPlayBtn;
@property (weak, nonatomic) IBOutlet UIButton *VoucherCodeButton;

- (IBAction)couponDetailsButtonAction:(id)sender;
- (IBAction)couponHowToUseButtonAction:(id)sender;
- (IBAction)couponTNCButtonAction:(id)sender;


- (IBAction)backButtonAction:(id)sender;
@end
