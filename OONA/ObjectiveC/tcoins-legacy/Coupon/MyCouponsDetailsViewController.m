//
//  MyCouponsDetailsViewController.m
//  OONA
//
//  Created by OONA iOS on 1/15/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "MyCouponsDetailsViewController.h"
#import "CouponDetails.h"
#import "CouponHowToUse.h"
#import "CouponTermsConditions.h"
#import "OONA-Swift.h"
//#import "MDScratchImageView.h"
#import "CommonHeader.h"
#import <SDWebImage/UIImageView+WebCache.h>
@import JGProgressHUD;
@interface MyCouponsDetailsViewController () <UIScrollViewDelegate>
{
    NSTimer *timer;
}
@property (nonatomic) CouponDetails *couponDetailsSegmentedView;
@property (nonatomic) CouponHowToUse *couponHowToUse;
@property (nonatomic) CouponTermsConditions *couponTermsConditions;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic) NSArray *buttonArray;
@property (nonatomic) UIView *selectedIndicator;
@property (nonatomic) int remainingTime;
@end

@implementation MyCouponsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self setupDisplay];
    [self showData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillLayoutSubviews {
}
- (void)backToHome {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)setupDisplay{
    _detailsView.layer.cornerRadius = 14;
    _detailsView.layer.masksToBounds = YES;
    
    [_couponDetailsButton setTitle:str(@"details") forState:UIControlStateNormal];
    [_couponHowToUseButton setTitle:str(@"how_to_use") forState:UIControlStateNormal];
    [_couponTNCButton setTitle:str(@"terms_and_condition") forState:UIControlStateNormal];
    
    _couponDetailsButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponDetailsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    _couponHowToUseButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponHowToUseButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    _couponTNCButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponTNCButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [_backToNotPlayBtn setTitle:str(@"back_to_now_playing") forState:UIControlStateNormal];
    _backToNotPlayBtn.layer.cornerRadius = _backToNotPlayBtn.frame.size.height/2;
    _backToNotPlayBtn.layer.masksToBounds = YES;
    _backToNotPlayBtn.layer.borderWidth = 1;
    _backToNotPlayBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [_backToNotPlayBtn addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    _buttonArray = @[_couponDetailsButton,_couponHowToUseButton,_couponTNCButton];
    
    float viewWidth = _couponDetailsView.frame.size.width;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40, viewWidth, _couponDetailsView.frame.size.height-40)];
    self.scrollView.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(viewWidth * 3, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, viewWidth, self.scrollView.frame.size.height) animated:NO];
    [_couponDetailsView addSubview:self.scrollView];
    
    self.couponDetailsSegmentedView.frame =CGRectMake(0, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponDetailsSegmentedView];
    
    self.couponHowToUse.frame =CGRectMake(viewWidth, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponHowToUse];
    
    self.couponTermsConditions.frame =CGRectMake(viewWidth*2, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponTermsConditions];
    
    _selectedIndicator = [[UIView alloc] init];
    _selectedIndicator.backgroundColor = [Color redOONAColor];
    [_couponDetailsView addSubview:_selectedIndicator];
    
    [self.view layoutIfNeeded];
    _VoucherCodeButton.layer.cornerRadius = _VoucherCodeButton.frame.size.height/ 2;
    _VoucherCodeButton.layer.masksToBounds = YES ;
    _VoucherCodeButton.userInteractionEnabled = YES ;
    [_VoucherCodeButton addTarget:self action:@selector(copyToClipboard) forControlEvents:UIControlEventTouchUpInside];
    [self setSelectedButtonAtIndex:0];
    
//    MDScratchImageView *scratchImageView = [[MDScratchImageView alloc] initWithFrame:_scratchOverlayImageView.frame];
//    scratchImageView.image = [UIImage imageNamed:@"scratch-overlay"];
//    scratchImageView.delegate = self;
//    [_detailsView addSubview:scratchImageView];
    
    _screenTitleLabel.text = str(@"coupon_details");
    _voucherCodeTitleLabel.text = str(@"voucher_code");
}
- (void)copyToClipboard {
    
    
    NSString *copyText =  [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"voucherCode"]];
    JGProgressHUD *HUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
    HUD.textLabel.text = @"Copied to clipboard";
    HUD.indicatorView = [[JGProgressHUDSuccessIndicatorView alloc] init];
    [HUD showInView:self.view];
    [HUD dismissAfterDelay:2.0];
    
    [UIPasteboard generalPasteboard].string = copyText;
}

- (void)showData {
    NSLog(@"_couponDetails = %@",_couponDetails);
    [_detailsImage sd_setImageWithURL:[_couponDetails objectForKey:@"imageUrl"] placeholderImage:[UIImage imageNamed:@"coupon-icon-placeholder"]];
    _detailsTitleLabel.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"name"]];
    
    [_VoucherCodeButton setTitle: [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"voucherCode"]] forState:UIControlStateNormal];
    self.couponDetailsSegmentedView.couponDetailsTextVIew.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"description"]];
    
    self.couponHowToUse.couponHowToUseTextView.text = [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"howToUse"]];
    
    self.couponTermsConditions.couponTermsConditionsTextView.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"terms"]];
    
    _remainingTime = [[_couponDetails objectForKey:@"expiresIn"] intValue];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startCounter) userInfo:nil repeats:YES];
    [timer fire];
}

-(void) startCounter{
    if (_remainingTime >= 0) {
        int minutes, seconds;
        int hours, days;
        
        _remainingTime--;
        
        days = _remainingTime / (3600*24);
        hours = (_remainingTime % (3600*24)) /3600;
        minutes = (_remainingTime % 3600) / 60;
        seconds = (_remainingTime %3600) % 60;
        NSString *time = str(@"expires_in");
        
        if (days > 0 ) {
            time = [NSString stringWithFormat:@"%@ %d day",time, days];
        }
        if (days > 1) {
            time = [NSString stringWithFormat:@"%@s",time];
        }
        
        if (hours > 0 ) {
            time = [NSString stringWithFormat:@"%@ %d hour",time, hours];
        }
        if (hours > 1) {
            time = [NSString stringWithFormat:@"%@s",time];
        }
        
        if (minutes > 0 ) {
            time = [NSString stringWithFormat:@"%@ %d minute",time, minutes];
        }
        if (minutes > 1) {
            time = [NSString stringWithFormat:@"%@s",time];
        }
        
        if (seconds > 0 ) {
            time = [NSString stringWithFormat:@"%@ %d second",time, seconds];
        }
        if (seconds > 1) {
            time = [NSString stringWithFormat:@"%@s",time];
        }

        _expireInLabel.text = [NSString stringWithFormat:@"%@",time];
        _VoucherCodeButton.layer.cornerRadius = _VoucherCodeButton.frame.size.height/ 2;
    }
    if (_remainingTime <= 0) {
        if ([timer isValid]) {
            [timer invalidate];
            timer = nil;
            _expireInLabel.text = str(@"expired");
            _VoucherCodeButton.alpha = 0.5;
        }
    }
    
}

- (CouponDetails *)couponDetailsSegmentedView{
    if (!_couponDetailsSegmentedView) {
        _couponDetailsSegmentedView = [[NSBundle mainBundle] loadNibNamed:@"CouponDetails" owner:self options:nil].firstObject;
    }
    return _couponDetailsSegmentedView;
}

- (CouponHowToUse *)couponHowToUse{
    if (!_couponHowToUse) {
        _couponHowToUse = [[NSBundle mainBundle] loadNibNamed:@"CouponHowToUse" owner:self options:nil].firstObject;
    }
    return _couponHowToUse;
}

- (CouponTermsConditions *)couponTermsConditions{
    if (!_couponTermsConditions) {
        _couponTermsConditions = [[NSBundle mainBundle] loadNibNamed:@"CouponTermsConditions" owner:self options:nil].firstObject;
    }
    return _couponTermsConditions;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self setSelectedButtonAtIndex:page];
}

- (void)setSelectedButtonAtIndex:(NSInteger)index{
    UIButton *selectedButton = [_buttonArray objectAtIndex:index];
    [selectedButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    float indicatorHeight = 3.0;
    CGRect selectedButtonFrame = selectedButton.frame;
    [UIView animateWithDuration:0.2 animations:^{
        _selectedIndicator.frame = CGRectMake(CGRectGetMinX(selectedButtonFrame), CGRectGetMaxY(selectedButtonFrame)-indicatorHeight, selectedButtonFrame.size.width, indicatorHeight);
    }];
    for (UIButton *button in _buttonArray) {
        if (button != selectedButton) {
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)couponDetailsButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:0];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)couponHowToUseButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:1];
    [self.scrollView setContentOffset:CGPointMake(_couponDetailsView.frame.size.width, 0) animated:YES];
}

- (IBAction)couponTNCButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:2];
    [self.scrollView setContentOffset:CGPointMake(_couponDetailsView.frame.size.width*2, 0) animated:YES];
}

//- (void)mdScratchImageView:(MDScratchImageView *)scratchImageView didChangeMaskingProgress:(CGFloat)maskingProgress {
//}

- (IBAction)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
