//
//  CouponHowToUse.h
//  OONA
//
//  Created by OONA iOS on 1/16/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponHowToUse : UIView
@property (weak, nonatomic) IBOutlet UITextView *couponHowToUseTextView;

@end
