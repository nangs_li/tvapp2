//
//  MyCouponsCollectionViewCell.m
//  OONA
//
//  Created by OONA iOS on 1/15/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "MyCouponsCollectionViewCell.h"

@implementation MyCouponsCollectionViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 16;
    self.layer.masksToBounds = YES;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startCounter) userInfo:nil repeats:YES];
}

- (void)startCountDown{
    [timer fire];
}

-(void) startCounter{
    if (_remainingTime >= 0) {
        int minutes, seconds;
        int hours, days;
        
        _remainingTime--;
        
        days = _remainingTime / (3600*24);
        hours = (_remainingTime % (3600*24)) /3600;
        minutes = (_remainingTime % 3600) / 60;
        seconds = (_remainingTime %3600) % 60;
        NSString *time;
        if (days > 0) {
            if (hours == 0) {
                time = [NSString stringWithFormat:@"%d days", days];
            }
            else{
                time = [NSString stringWithFormat:@"%d days %d hours", days, hours];
            }
        }
        else if (hours > 0){
            if (minutes == 0){
                time = [NSString stringWithFormat:@"%d hours", hours];
            }
            else{
                time = [NSString stringWithFormat:@"%d hours %d minutes", hours, minutes];
            }
        }
        else if (minutes > 0){
            time = [NSString stringWithFormat:@"%d minutes %d seconds", minutes, seconds];
        }
        else{
            time = [NSString stringWithFormat:@"%d seconds", seconds];
        }
        
        self.validDateLabel.text = time;
    }
    if (_remainingTime <= 0) {
        if ([timer isValid]) {
            [timer invalidate];
            timer = nil;
            self.alpha = 0.5;
            self.validDateLabel.text = @"";
        }
    }
}
@end
