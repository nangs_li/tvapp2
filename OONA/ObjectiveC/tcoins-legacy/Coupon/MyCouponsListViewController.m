//
//  MyCouponsListViewController.m
//  OONA
//
//  Created by OONA iOS on 1/8/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "MyCouponsListViewController.h"
#import "MyCouponsCollectionViewCell.h"
#import "MyCouponsDetailsViewController.h"
//#import "CommonHeader.h"
#import "OONAString.h"
#import "NetworkModule.h"
#import "UserData.h"
#import "CustomPopup.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyCouponsListViewController ()
{
    UITapGestureRecognizer *removeInformationGesture;
    UIView *noGiftView;
    BOOL loading;
}
@property (nonatomic) NSArray *dummyCatalogList;
@property (nonatomic) NSMutableArray *voucherList;
@end

@implementation MyCouponsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _voucherList = [[NSMutableArray alloc] init];
    
    [self setupDisplay];
    [self setupData];
    [self loadMyVoucher];
}

- (void)setupDisplay{
    _informationLabel.alpha = 0;
    _informationView.alpha = 0;
    _informationLabel.hidden = YES;
    _informationView.hidden = YES;
    _informationLabel.text = str(@"keep_interacting_with_the_ad_to_earn_more_points");
    
    [_backToNotPlayBtn setTitle:str(@"back_to_now_playing") forState:UIControlStateNormal];
    _backToNotPlayBtn.layer.cornerRadius = _backToNotPlayBtn.frame.size.height/2;
    _backToNotPlayBtn.layer.masksToBounds = YES;
    _backToNotPlayBtn.layer.borderWidth = 1;
    _backToNotPlayBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [_backToNotPlayBtn addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@""];
    NSTextAttachment *attachmentvideo = [[NSTextAttachment alloc] init];
    attachmentvideo.image = [UIImage imageNamed:@"arrow-down"];
    attachmentvideo.bounds = CGRectMake(0, 0, 12, 10);
    NSAttributedString *attachmentStringVideo = [NSAttributedString attributedStringWithAttachment:attachmentvideo];
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:@"Categories  " attributes:attrs];
    
    [myString appendAttributedString:attrStr];
    [myString appendAttributedString:attachmentStringVideo];
    
    _catalogListCollectionView.contentInset = UIEdgeInsetsMake(-10, 0, 0, 0);
    
    [self.view layoutIfNeeded];
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, _informationView.frame.size.width, _informationView.frame.size.height) byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){_informationView.frame.size.height, _informationView.frame.size.height}].CGPath;
    
    _informationView.layer.mask = maskLayer;
    _informationView.layer.masksToBounds = YES;
    
    _screenTitleLabel.text = str(@"my_coupon");
    [self initNoGiftView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupData)
                                                 name:@"login_success"
                                               object:nil];
}
- (void)backToHome {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)initNoGiftView {
    noGiftView = [[UIView alloc]initWithFrame:_catalogListCollectionView.frame];
    [self.view addSubview:noGiftView];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(noGiftView.frame.size.width/2-50, 0, 100, 100)];
    [img setImage:[UIImage imageNamed:@"no-notification-image"]];
//    [noGiftView addSubview:img];
    
    UILabel *lb = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, noGiftView.frame.size.width, 30)];
    [lb setText:@"You do not have any vouchers yet"];
    [lb setTextColor:UIColor.whiteColor];
    lb.textAlignment = NSTextAlignmentCenter;
    [noGiftView addSubview:lb];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(noGiftView.frame.size.width/2-75, 90, 150, 40)];
    [btn setTitle:@"Redeem now" forState:UIControlStateNormal];
    btn.layer.cornerRadius = btn.frame.size.height/2;
    [btn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:UIColor.clearColor];
    btn.layer.borderWidth = 1.f ;
    btn.layer.borderColor = UIColor.whiteColor.CGColor;
    
    [noGiftView addSubview:btn];
    noGiftView.hidden = YES;
    
}
- (void)setupData{
    _tcoinsLabel.text = [NSString stringWithFormat:@"%@",[UserData userPointsString]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:      (NSInteger)section
{
    return _voucherList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MyCouponsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCouponsCollectionViewCell" forIndexPath:indexPath];
    cell.validStatusLabel.text = str(@"expires_in");
    
    if (!cell.remainingTime) {
        cell.remainingTime = [[[_voucherList objectAtIndex:indexPath.row] objectForKey:@"expiresIn"] intValue];
        [cell startCountDown];
    }
    
    if (cell.remainingTime <= 0){
//        cell.alpha = 0.5;
        cell.voucherExpiredOverlay.hidden = NO;
        cell.validStatusLabel.text = str(@"expired");
        cell.expired = YES;
    }
    else{
//        cell.alpha = 1;
        cell.expired = YES;
        cell.voucherExpiredOverlay.hidden = YES;
    }
    
    [cell.catalogImageView sd_setImageWithURL:[[_voucherList objectAtIndex:indexPath.row] objectForKey:@"imageUrl"] placeholderImage:[UIImage imageNamed:@"coupon-icon-placeholder"]];
    cell.catalogTitleLabel.text = [NSString stringWithFormat:@"%@",[[_voucherList objectAtIndex:indexPath.row] valueForKey:@"name"]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float collectionHeight = collectionView.frame.size.height-10;
    return CGSizeMake(collectionHeight*.7315, collectionHeight);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    int remainingTime = [[[_voucherList objectAtIndex:indexPath.row] objectForKey:@"expiresIn"] intValue];
    if (remainingTime > 0)
    [self fetchVoucherDetail:[[_voucherList objectAtIndex:indexPath.row] valueForKey:@"id"]];
}
   
- (IBAction)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)informationButtonAction:(id)sender {
    if (_informationView.hidden == NO) {
        [self dismissInformation];
        return;
    }
    
    _informationLabel.hidden = NO;
    _informationView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _informationLabel.alpha = 1;
        _informationView.alpha = 1;
    }];
    
    removeInformationGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissInformation)];
    [self.view addGestureRecognizer:removeInformationGesture];
}

- (void)dismissInformation{
    [UIView animateWithDuration:0.2 animations:^{
        _informationLabel.alpha = 0;
        _informationView.alpha = 0;
    } completion:^(BOOL finished) {
        _informationLabel.hidden = YES;
        _informationView.hidden = YES;
    }];
    
    [self.view removeGestureRecognizer:removeInformationGesture];
}

- (void)loadMyVoucher{
    if (loading){
        return;
    }
    
    loading = YES;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"%@points/vouchers",API_URL];
    NSArray *headers = @[@{@"key":@"Authorization",@"value":[NSString stringWithFormat:@"Bearer %@",[UserData userToken]]}];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if ([[_voucherList lastObject] valueForKey:@"id"]) {
        [param setObject:[NSString stringWithFormat:@"%lu",(unsigned long)_voucherList.count] forKey:@"offset"];
    }
//    [[NetworkModule instance] POST:url parameters:param headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"my wallet : %@",responseObject);
//        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
//        NSString *status = [responseObject valueForKey:@"status"];
//        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//            if (isSuccess) {
//                if ([responseObject valueForKey:@"vouchers"]) {
//                    if ([[responseObject valueForKey:@"vouchers"] count] > 0) {
//                        [_voucherList addObjectsFromArray:[responseObject valueForKey:@"vouchers"]];
//                    }
//                }
//                if (_voucherList.count == 0) {
//                    noGiftView.hidden = NO;
//                }else{
//                    noGiftView.hidden = YES;
//                }
//                [_catalogListCollectionView reloadData];
//            }
//            else{
//                [self showErrorWithMessage:message];
//            }
//            loading = NO;
//            [activityIndicator stopAnimating];
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [self showErrorWithMessage:[error localizedDescription]];
//        loading = NO;
//        [activityIndicator stopAnimating];
//    }];
}

- (void)showErrorWithMessage :(NSString *)message{
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:message];
    
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    CGRect buttonFrame = popup.button1.frame;
    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
    popup.button1.frame = buttonFrame;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    
    popup.button1.hidden = NO ;
    
    popup.button2.hidden = YES ;
    [self.view addSubview:popup];
    __block CustomPopup *popupBlock = popup;
    
    popup.completionButton1  = ^{
        [popupBlock dismiss];
        
    };
    
}

- (void)fetchVoucherDetail:(NSString *)voucherID {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
//    [[NetworkModule instance]getVoucherDetailWithID:voucherID success:^(NSURLSessionDataTask *task, id responseObject) {
//        [activityIndicator stopAnimating];
//        MyCouponsDetailsViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyCouponsDetailsViewController"];
//        detailVC.couponDetails = responseObject;
//        [self.navigationController pushViewController:detailVC animated:YES];
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [activityIndicator stopAnimating];
//    }];
}
@end
