//
//  MyCouponsCollectionViewCell.h
//  OONA
//
//  Created by OONA iOS on 1/15/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponsCollectionViewCell : UICollectionViewCell
{
    NSTimer *timer;
}
@property (weak, nonatomic) IBOutlet UIImageView *catalogImageView;
@property (weak, nonatomic) IBOutlet UILabel *catalogTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *validDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *validStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *voucherExpiredOverlay;

@property (nonatomic) int remainingTime;
@property (nonatomic) BOOL expired;

- (void)startCountDown;
@end
