//
//  CatalogListDetailsViewController.m
//  OONA
//
//  Created by OONA iOS on 1/7/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "OONAFirbase.h"
#import "CatalogListDetailsViewController.h"
#import "OONA-Swift.h"

@interface CatalogListDetailsViewController () <UIScrollViewDelegate>
@property (nonatomic) CouponDetails *couponDetailsSegmentedView;
@property (nonatomic) CouponHowToUse *couponHowToUse;
@property (nonatomic) CouponTermsConditions *couponTermsConditions;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic) NSArray *buttonArray;
@property (nonatomic) UIView *selectedIndicator;

@property (nonatomic) CGPoint panCoord;
@property (nonatomic) CGRect defaultDragbleViewFrame;
@end

@implementation CatalogListDetailsViewController
BOOL SliderTriggered;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self setupDisplay];
    [self showData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [_detailTextView setContentOffset:CGPointZero animated:NO];
    [self.couponDetailsSegmentedView.couponDetailsTextVIew setContentOffset:CGPointZero];
    [self.couponHowToUse.couponHowToUseTextView setContentOffset:CGPointZero];
    [self.couponTermsConditions.couponTermsConditionsTextView setContentOffset:CGPointZero];
}

- (void)setupDisplay {
    _detailsView.layer.cornerRadius = 14;
    _detailsView.layer.masksToBounds = YES;
    
    _detailsRedeemButton.layer.cornerRadius = _detailsRedeemButton.frame.size.height/2;
    _detailsRedeemButton.layer.masksToBounds = YES;
    _detailsRedeemButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    _detailsRedeemButton.layer.borderColor = UIColor.lightGrayColor.CGColor;
    _detailsRedeemButton.layer.borderWidth = 1.f;
    [_detailsRedeemButton setBackgroundColor:HexColor(0x4D6070)];
    _dragableView.layer.cornerRadius = _dragableView.frame.size.height/2 ;
    _dragableView.layer.masksToBounds = YES ;
    [_dragableView setImage:[UIImage imageNamed:@"slider-button"]];
    _dragableView.contentMode = UIViewContentModeScaleAspectFit;
    SliderTriggered = NO ;
    [_detailTextView setTextContainerInset:UIEdgeInsetsMake(14, 10, 14, 14)];
    
    [_backToNowPlayBtn setTitle:str(@"back_to_now_playing") forState:UIControlStateNormal];
    _backToNowPlayBtn.layer.cornerRadius = _backToNowPlayBtn.frame.size.height/2;
    _backToNowPlayBtn.layer.masksToBounds = YES;
    _backToNowPlayBtn.layer.borderWidth = 1;
    _backToNowPlayBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [_backToNowPlayBtn addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    
    _screenTitleLabel.text = str(@"coupon_details");
    
    [_couponDetailsButton setTitle:str(@"details") forState:UIControlStateNormal];
    [_couponHowToUseButton setTitle:str(@"how_to_use") forState:UIControlStateNormal];
    [_couponTNCButton setTitle:str(@"terms_and_condition") forState:UIControlStateNormal];
    
    _couponDetailsButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponDetailsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    _couponHowToUseButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponHowToUseButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    _couponTNCButton.titleLabel.minimumScaleFactor = 0.5f;
    _couponTNCButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    _buttonArray = @[_couponDetailsButton,_couponHowToUseButton,_couponTNCButton];
    
    float viewWidth = _couponDetailsView.frame.size.width;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40, viewWidth, _couponDetailsView.frame.size.height-40)];
    self.scrollView.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(viewWidth * 3, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, viewWidth, self.scrollView.frame.size.height) animated:NO];
    [_couponDetailsView addSubview:self.scrollView];
    
    self.couponDetailsSegmentedView.frame =CGRectMake(0, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponDetailsSegmentedView];
    
    self.couponHowToUse.frame =CGRectMake(viewWidth, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponHowToUse];
    
    self.couponTermsConditions.frame =CGRectMake(viewWidth*2, 0, viewWidth, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.couponTermsConditions];
    
    _selectedIndicator = [[UIView alloc] init];
    _selectedIndicator.backgroundColor = [Color redOONAColor];
    [_couponDetailsView addSubview:_selectedIndicator];
    
    [self setSelectedButtonAtIndex:0];
    
    [_detailsRedeemButton setTitle:str(@"slide_to_redeem") forState:UIControlStateNormal];
}
- (void)backToHome {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)showData {
    [_detailsImage sd_setImageWithURL:[_couponDetails objectForKey:@"imageUrl"] placeholderImage:[UIImage imageNamed:@"coupon-icon-placeholder"]];
    _detailsTitleLabel.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"name"]];
    _detailsValidDateLabel.text = [NSString stringWithFormat:@"%@ %@",str(@"valid_until"),[_couponDetails objectForKey:@"validUntil"]];
    if ([_couponDetails objectForKey:@"validUntil"] == nil){
        _detailsValidDateLabel.hidden = YES;
    }
    NSString *prices =  [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"points"]];
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * priceString =  [formatter stringFromNumber:[NSNumber numberWithInteger:[prices integerValue]]];
    priceString = [priceString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    _detailPriceLabel.text = priceString;
    _detailTextView.text = [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"description"]];
    
    self.couponDetailsSegmentedView.couponDetailsTextVIew.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"description"]];
    
    self.couponHowToUse.couponHowToUseTextView.text = [NSString stringWithFormat:@"%@",[_couponDetails valueForKey:@"howToUse"]];
    
    self.couponTermsConditions.couponTermsConditionsTextView.text = [NSString stringWithFormat:@"%@",[_couponDetails objectForKey:@"terms"]];
    
    UIPanGestureRecognizer *swipeLeft = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
    [self.dragableView addGestureRecognizer:swipeLeft];

    _defaultDragbleViewFrame = self.dragableView.frame;
    NSLog(@"coupon detail :%@",_couponDetails);
}

-(void)dragging:(UIPanGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        self.panCoord = [gesture locationInView:gesture.view];
    }
    else if(gesture.state == UIGestureRecognizerStateEnded)
    {
        if (SliderTriggered)
            return;
        
        [self resetSlideBtn];
       
//        SliderTriggered = NO ;
        return;
    }
    CGPoint newCoord = [gesture locationInView:gesture.view];
    float dX = newCoord.x-_panCoord.x;
    
    if (gesture.view.frame.origin.x+dX < _defaultDragbleViewFrame.origin.x) {
        return;
    }
    if (gesture.view.frame.origin.x+dX+gesture.view.frame.size.width > CGRectGetMaxX(_detailsRedeemButton.frame)) {
        if (!SliderTriggered){
            [_detailsRedeemButton setBackgroundColor:[Color redOONAColor]];
            _dragableView.alpha = 0;
            [self SlideRedeem];
            SliderTriggered = YES ;
        }
        return;
    }
    
    gesture.view.frame = CGRectMake(gesture.view.frame.origin.x+dX, gesture.view.frame.origin.y, gesture.view.frame.size.width, gesture.view.frame.size.height);
}
- (void)resetSlideBtn {
    [self.view setNeedsLayout];
    [_detailsRedeemButton setBackgroundColor:HexColor(0x4D6070)];
    [UIView animateWithDuration:.3 animations:^{
        _dragableView.frame = _defaultDragbleViewFrame;
        _dragableView.alpha = 1;
        
        SliderTriggered = NO ;
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)SlideRedeem {
//    if (![CheckSession checkLoginStatus]){
//        [self requireSignIn];
//
//        return ;
//    }
    [self checkCurrentCoinWithCompletion:^(BOOL isSuccess) {
        if (isSuccess) {
            CustomPopup *popup ;
            __block CustomPopup *popupBlock = popup;
             NSString *secondText = [NSString stringWithFormat:str(@"are_you_sure_you_want_to_redeem_for_tcoins"),[_couponDetails objectForKey:@"name"],[_couponDetails valueForKey:@"points"]];
            
            if ([[self.couponDetails objectForKey:@"type"] isEqualToString:@"phoneBalance"]){
                popup = [[CustomPopup alloc]initPhoneDialogWithMessage:secondText];
                popupBlock = popup;
                popup.editAction = ^{
                    [popupBlock dismiss];
                    [self resetSlideBtn];
                    [self popupEditProfile];
                };
                
            }else{
                popup = [[CustomPopup alloc] initWithMessage:@""];
                popupBlock = popup;
                NSString *expireWarning = [NSString stringWithFormat:str(@"redeem_expire_warning"),@"7"];
                
                
               
    //            CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:secondText AndMessage:@"By redeeming, you agree with our Terms and Conditions"];
                popup.backBtn.hidden = YES;
                NSLog(@"terms : %@",str(@"by_buying_you_are_agree_with_terms_Conditions"));
                popup.info.text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@",secondText,expireWarning,str(@"by_buying_you_are_agree_with_terms_Conditions")];
                
                NSRange rangeBold  = [popup.info.text rangeOfString:secondText];
                NSRange rangeSmall = [popup.info.text rangeOfString:str(@"by_buying_you_are_agree_with_terms_Conditions")];
                NSRange rangeMiddle = [popup.info.text rangeOfString:expireWarning];
                
                UIFont *fontText = [UIFont fontWithName:@"SourceSansPro-Bold" size:16];
                NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
                NSDictionary *colorAttribute = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
                NSDictionary *grayColorAttribute = @{ NSForegroundColorAttributeName : [UIColor grayColor] };
                NSDictionary *dictMediumText = @{ NSFontAttributeName : [UIFont fontWithName:@"SourceSansPro-Regular" size:13]};
                NSDictionary *dictSmallText = @{ NSFontAttributeName : [UIFont fontWithName:@"SourceSansPro-Regular" size:13]};
                
                NSMutableAttributedString *mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:popup.info.text];
                [mutAttrTextViewString addAttributes:dictBoldText range:rangeBold];
                [mutAttrTextViewString addAttributes:colorAttribute range:rangeBold];
                [mutAttrTextViewString addAttributes:dictSmallText range:rangeSmall];
                [mutAttrTextViewString addAttributes:grayColorAttribute range:rangeSmall];
                [mutAttrTextViewString addAttributes:dictMediumText range:rangeMiddle];
                [mutAttrTextViewString addAttributes:colorAttribute range:rangeMiddle];
                
                [popup.info setAttributedText:mutAttrTextViewString];
                
                
            }
            popup.backBtn.hidden = YES ;
            [popup.button1 setTitle:str(@"Cancel") forState:UIControlStateNormal];
            [popup.button2 setTitle:str(@"redeem") forState:UIControlStateNormal];
            popup.button2.backgroundColor = [Color redOONAColor];
            popup.button2.layer.borderColor = [Color redOONAColor].CGColor;
//            __block CustomPopup *popupBlock = popup;
            popup.completionButton1 = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [popupBlock dismiss];
                    [self resetSlideBtn];
                });
            };
            popup.completionButton2 = ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [popupBlock dismiss];
                    [self resetSlideBtn];
                    [self redeem];
                });
            };
            popup.alpha = 0;
            [self.view addSubview:popup];
            [UIView animateWithDuration:0.4
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 popup.alpha = 1;
                             }
                             completion:nil];
        }
        else{
            [self showErrorWithMessage:[NSString stringWithFormat:@"%@\n%@",str(@"sorry_you_dont_have_enough_coins_to_redeem"),str(@"keep_interacting_with_the_ad_to_earn_more_points")]];
        }
    }];
}

- (void)checkCurrentCoinWithCompletion:(void (^)(BOOL isSuccess))completion{
    int voucherPrice = [[_couponDetails valueForKey:@"points"] intValue];
    int userCoin = [[UserData userPoints] intValue];
    NSLog(@" %d vs %d ",[[_couponDetails valueForKey:@"points"] intValue],userCoin);
    if (userCoin >= voucherPrice) {
        NSLog(@" %d <= %d ",[[_couponDetails valueForKey:@"points"] intValue],userCoin);
        completion(YES);
        return;
    }
    NSLog(@" %d >= %d ",[[_couponDetails valueForKey:@"points"] intValue],userCoin);
    completion(NO);
}
- (void)popupEditProfile {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)redeem {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
//    [[NetworkModule instance]redeemVoucherWithID:_couponID success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
//        NSString *status = [responseObject valueForKey:@"status"];
//        [OONAFirBase logEventWithName:@"points_redeem" parameters:@{@"points" : @([[_couponDetails valueForKey:@"points"] intValue]),
//                                                                    @"action" : @"redeem_gift",
//                                                                    @"item_id" : @(_couponID.intValue)
//                                                                    }];
//        [activityIndicator stopAnimating];
//        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//           if (isSuccess) {
//                if ([self.delegate respondsToSelector:@selector(didFinishRedeemVoucherWithResponseObject: isTopup:)]) {
//                    BOOL isTopup = [[self.couponDetails objectForKey:@"type"] isEqualToString:@"phoneBalance"];
//
//                    [self.delegate didFinishRedeemVoucherWithResponseObject:responseObject isTopup:isTopup];
//                    [self dismissViewControllerAnimated:YES completion:nil];
//                }
//            }else{
//                [self showErrorWithMessage:message];
//
//            }
//
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [activityIndicator stopAnimating];
//        [self showErrorWithMessage:str(@"generic_failure_text")];
////        if ([self.delegate respondsToSelector:@selector(didFinishRedeemVoucherWithResponseObject:)]) {
//////            [self.delegate didFinishRedeemVoucher:_couponID];
////            [self dismissViewControllerAnimated:YES completion:nil];
////        }
//    }];
}
- (IBAction)redeemButtonAction:(id)sender {
    
}


- (CouponDetails *)couponDetailsSegmentedView {
    if (!_couponDetailsSegmentedView) {
        _couponDetailsSegmentedView = [[NSBundle mainBundle] loadNibNamed:@"CouponDetails" owner:self options:nil].firstObject;
    }
    return _couponDetailsSegmentedView;
}

- (CouponHowToUse *)couponHowToUse {
    if (!_couponHowToUse) {
        _couponHowToUse = [[NSBundle mainBundle] loadNibNamed:@"CouponHowToUse" owner:self options:nil].firstObject;
    }
    return _couponHowToUse;
}

- (CouponTermsConditions *)couponTermsConditions {
    if (!_couponTermsConditions) {
        _couponTermsConditions = [[NSBundle mainBundle] loadNibNamed:@"CouponTermsConditions" owner:self options:nil].firstObject;
    }
    return _couponTermsConditions;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self setSelectedButtonAtIndex:page];
}

- (void)setSelectedButtonAtIndex:(NSInteger)index{
    UIButton *selectedButton = [_buttonArray objectAtIndex:index];
    [selectedButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    float indicatorHeight = 3.0;
    CGRect selectedButtonFrame = selectedButton.frame;
    [UIView animateWithDuration:0.2 animations:^{
        _selectedIndicator.frame = CGRectMake(CGRectGetMinX(selectedButtonFrame), CGRectGetMaxY(selectedButtonFrame)-indicatorHeight, selectedButtonFrame.size.width, indicatorHeight);
    }];
    for (UIButton *button in _buttonArray) {
        if (button != selectedButton) {
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)couponDetailsButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:0];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)couponHowToUseButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:1];
    [self.scrollView setContentOffset:CGPointMake(_couponDetailsView.frame.size.width, 0) animated:YES];
}

- (IBAction)couponTNCButtonAction:(id)sender {
    [self setSelectedButtonAtIndex:2];
    [self.scrollView setContentOffset:CGPointMake(_couponDetailsView.frame.size.width*2, 0) animated:YES];
}
- (void)requireSignIn {
    self.signInAlertView = [[SignInAlertView alloc]init];
    [SignInAlertView add:self.signInAlertView withController:self withDismiss:^{
        //        [self play];
        [self resetSlideBtn];
    } withAction:^{
        [self resetSlideBtn];
        [self popupSignIn];
    }];
}
- (void)popupSignIn {
    
    //    LandingPageViewController *viewController = [[LandingPageViewController alloc]initWithPopUpMode];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LandingPageViewController" bundle:nil];
//    LandingPageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LandingPageViewController"];
//    [viewController setIsPopupMode:YES];
//    LandscapeViewController *navVC = [[LandscapeViewController alloc]initWithRootViewController:viewController];
//    [self presentViewController:navVC animated:YES completion:^{
//        [viewController setIsPopupMode:YES];
//    }];
//
}
- (void)showErrorWithMessage:(NSString *)message{
    CustomPopup *popup = [[CustomPopup alloc] initWithMessage:message];
//    CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:message AndMessage:message];
//    popup.button2.hidden = YES;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
//    CGRect buttonFrame = popup.button1.frame;
//    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
//    popup.button1.frame = buttonFrame;
    __block CustomPopup *popupBlock = popup;
//    popup.backBtn. hidden  = YES;
    [popup setupOneButton];
    popup.completionButton1 = ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [popupBlock dismiss];
            [self resetSlideBtn];
        });
    };
    popup.alpha = 0;
    [self.view addSubview:popup];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popup.alpha = 1;
                     }
                     completion:nil];
}
@end
