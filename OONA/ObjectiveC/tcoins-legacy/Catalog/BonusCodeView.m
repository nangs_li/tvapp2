//
//  BonusCodeView.m
//  OONA
//
//  Created by jack on 20/12/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "BonusCodeView.h"
#import "CommonHeader.h"
#import "NetworkModule.h"
#import "UserData.h"
#import "OONAString.h"
#import "OONA-Swift.h"

@implementation BonusCodeView{
    RTSpinKitView *spinner ;
    NSString *placeHolderStr ;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    placeHolderStr = str(@"Input_code_here") ;
    [self setupDisplay];
}
- (void)showPlaceHolder {
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : HexColor(0xa0a2a5) };
    _inputField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolderStr
                                                                        attributes:attrs];
}
- (void)hidePlaceHolder {
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : UIColor.clearColor};
    _inputField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolderStr
                                                                        attributes:attrs];
}
- (void)setupDisplay {
    self.layer.cornerRadius = 17.5f;
    self.layer.borderColor  = UIColor.whiteColor.CGColor ;
    self.layer.borderWidth = 1.f ;
    
    self.layer.masksToBounds = YES ;
    [self setBackgroundColor:UIColor.clearColor];

    [_inputField setFont:[UIFont fontWithName:OONAFontRegular size:14.0]];

    [_inputField setTextColor:UIColor.whiteColor];
    [_inputField setBackgroundColor:UIColor.clearColor];
    [_inputField addTarget:self action:@selector(bonusFieldValueChanged:) forControlEvents:UIControlEventEditingChanged ];
//    _inputField.placeholder = @"Input code here..." ;

    _inputField.delegate = self ;

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, _inputField.frame.size.height)];
    _inputField.leftView = paddingView;
    _inputField.leftViewMode = UITextFieldViewModeAlways;

    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleArc];
    [spinner setColor:UIColor.whiteColor];
    [spinner setFrame:_spinnerBackView.bounds];
    spinner.spinnerSize = 20;
    [_spinnerBackView addSubview:spinner];

    [_submitButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];

    [_submitButton.titleLabel setFont:[UIFont fontWithName:OONAFontRegular size:14.0]];
    [_submitButton setTitle:str(@"redeem") forState:UIControlStateNormal];
    [_submitButton addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];

    [self resetBonusCodeView];
}
- (void)cleanInputField {
    [_inputField setText:@""];
}
- (void)submit {
    if (_inputField.text.length == 0){
        return ;
    }
    if ([self.delegate respondsToSelector:@selector(dismissToast)]){
        [self.delegate dismissToast];
    }
    [self bonusCodeLoading];
    
//    return ;
//    [[NetworkModule instance] verifyBonusCode:_inputField.text success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
//        NSString *status = [responseObject valueForKey:@"status"];
//        NSLog(@"verify bonus code %@",responseObject);
//        if ([responseObject valueForKey:@"reward"] != nil){
//            [UserData setUserPoints:[NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"reward"] valueForKey:@"pointsBalance"]]];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPointLabel" object:nil];
//        }
//          [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//              if (isSuccess){
//                  [_successBlockerView setHidden:NO];
//              }else{
//                  [_failureBlockerView setHidden:NO];
////                  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////                      [_inputField becomeFirstResponder];
////                  });
//              }
//              [_inputField setUserInteractionEnabled:YES];
//              if ([self.delegate respondsToSelector:@selector(didFinishRedeemBonusCode:Success:WithResponseObject:)]){
//                  [self.delegate didFinishRedeemBonusCode:_inputField.text Success:isSuccess WithResponseObject:responseObject];
//              }
//          }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        [self resetBonusCodeView];
//    }];
}
//textFieldv
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.text.length == 0)
        [self resetBonusCodeView];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self engageBonusCodeView];
    
    return YES;
}
- (void)bonusFieldValueChanged :(UITextField *)textfield{
    NSLog(@"value changed");
    if (textfield.text.length >= 2){
        [UIView animateWithDuration:.2  animations:^{
            [_submitButton setUserInteractionEnabled:YES];
//            [_submitButton setBackgroundColor:Color.redOONAColor];
        }];
    }else{
//         [_submitButton setBackgroundColor:HexColor(0xababac)];
        [_submitButton setUserInteractionEnabled:NO];
    }
}
- (void)resetBonusCodeView {
    [UIView animateWithDuration:.2 animations:^{
//        [_inputField setBackgroundColor:HexColor(0x4d535a)];
        [self showPlaceHolder];
        [_submitButton setBackgroundColor:HexColor(0xababac)];
        [_submitButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [_spinnerBlockerView setHidden:YES];
        [_successBlockerView setHidden:YES];
        [_failureBlockerView setHidden:YES];
    }];
}
- (void)dismissErrorViews {
    [UIView animateWithDuration:.2 animations:^{
        [self hidePlaceHolder];
        [_submitButton setBackgroundColor:UIColor.whiteColor];
        [_submitButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_spinnerBlockerView setHidden:YES];
        [_successBlockerView setHidden:YES];
        [_failureBlockerView setHidden:YES];
    }];
}
- (void)engageBonusCodeView {
    [UIView animateWithDuration:.2  animations:^{
//        [_inputField setBackgroundColor:HexColor(0xFFFFFF)];
        [self hidePlaceHolder];
        [_submitButton setBackgroundColor:UIColor.whiteColor];
        [_submitButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_spinnerBlockerView setHidden:YES];
        [_successBlockerView setHidden:YES];
        [_failureBlockerView setHidden:YES];
        
        
    }];
    
}
- (void)bonusCodeLoading {
    [self endEditing:YES];
    [_inputField setUserInteractionEnabled:NO ];
    [_spinnerBlockerView setHidden:NO];

//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [_inputField setUserInteractionEnabled:YES ];
//    });
}

@end
