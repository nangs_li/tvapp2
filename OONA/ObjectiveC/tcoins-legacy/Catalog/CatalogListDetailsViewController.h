//
//  CatalogListDetailsViewController.h
//  OONA
//
//  Created by OONA iOS on 1/7/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPopup.h"
#import "CouponDetails.h"
#import "CouponHowToUse.h"
#import "CouponTermsConditions.h"
//#import "OONA-Swift.h"
#import "CommonHeader.h"
//#import "CommonHeader.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NetworkModule.h"
//#import "CheckSession.h"
//#import "LandscapeViewController.h"
//#import "LandingPageViewController.h"
#import "SignInAlertView.h"
#import "UserData.h"
@protocol CatalogListDetailDelegate <NSObject>
@optional
- (void)didFinishRedeemVoucherWithResponseObject:(NSDictionary *)responseObject isTopup:(BOOL)isTopup;

@end

@interface CatalogListDetailsViewController : UIViewController
@property (nonatomic) NSDictionary *couponDetails;
@property (nonatomic) NSString *couponID;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (nonatomic, strong) SignInAlertView *signInAlertView;
@property (weak, nonatomic) IBOutlet UIImageView *detailsImage;
@property (weak, nonatomic) IBOutlet UILabel *detailsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsValidDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsRedeemButton;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *screenTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *couponDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *backToNowPlayBtn;

@property (weak, nonatomic) IBOutlet UIButton *couponDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *couponHowToUseButton;
@property (weak, nonatomic) IBOutlet UIButton *couponTNCButton;
@property (weak, nonatomic) IBOutlet UIImageView *dragableView;

@property (nonatomic, weak) id<CatalogListDetailDelegate> delegate;

- (IBAction)couponDetailsButtonAction:(id)sender;
- (IBAction)couponHowToUseButtonAction:(id)sender;
- (IBAction)couponTNCButtonAction:(id)sender;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)redeemButtonAction:(id)sender;
@end
