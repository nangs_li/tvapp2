//
//  CatalogListCollectionViewCell.h
//  OONA
//
//  Created by OONA iOS on 1/5/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *overlayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *catalogImageView;
@property (weak, nonatomic) IBOutlet UILabel *catalogTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *catalogPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *catalogRedeemButton;
@property (weak, nonatomic) IBOutlet UIImageView *outOfStockMask;

@end
