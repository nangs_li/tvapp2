//
//  CatalogListViewController.m
//  OONA
//
//  Created by OONA iOS on 1/5/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "SignInAlertView.h"
#import "CatalogListViewController.h"
#import "CatalogListCollectionViewCell.h"
#import "CommonHeader.h"
#import "CustomPopup.h"
#import "CatalogListDetailsViewController.h"
#import "CommonHeader.h"
#import "NetworkModule.h"
#import "UserData.h"
#import "CustomPopup.h"
//#import "CheckSession.h"
//#import "LandscapeViewController.h"
//#import "LandingPageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BrowseLiveBidCollectionViewCell.h"
#import "LiveBidViewController.h"

@interface CatalogListViewController () <UITableViewDelegate,UITextFieldDelegate,UITableViewDataSource,CatalogListDetailDelegate>
{
    UITapGestureRecognizer *removeInformationGesture;
    UITapGestureRecognizer *removeCategoryGesture;
    UITapGestureRecognizer *dismissKB;
    UITapGestureRecognizer *gesture;
    BonusCodeView * bonusCode;
    BOOL loading;
    UIView *noGiftView;
}
@property (nonatomic) NSMutableArray *catalogList;
@property (nonatomic) NSMutableArray *categoryList;
@property (nonatomic) UIButton *dismissCategoryButton;
@property (nonatomic) NSString *selectedCategory;
@end

@implementation CatalogListViewController

- (void)viewDidLoad {
    [OONAString initLanguage:LanguageEnglish];
    self.viewName = @"ui_points_gift_list";
    [super viewDidLoad];
    [self hideBonusCode];
    [self.catalogListCollectionView registerNib:[UINib nibWithNibName:@"BrowseLiveBidCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BrowseLiveBidCollectionViewCell"];
    _catalogList = [[NSMutableArray alloc] init];
    [self setupDisplay];
    [self setupData];
    [self loadCatalog];
    _hideCongratulationConstraints.constant = self.view.frame.size.height * -2;
    loading = NO;
    _myCouponsButton.hidden = NO;
    _categoriesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupData)
                                                 name:@"refreshPointLabel"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"pushToListBid"
                                               object:nil];
}

- (void)receiveTestNotification:(NSNotification *) notification
{
    LiveBidViewController *vc = [[LiveBidViewController alloc]init];
    [self presentViewController:vc animated:YES completion:nil];
//    [self.navigationController pushViewController:live animated:YES];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    dismissKB = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(dismissKeyboard)];
    // warning! this line must be appear. it will affect the mainbot.
    dismissKB.cancelsTouchesInView = YES;
    // end
    [self.view addGestureRecognizer:dismissKB];
   
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardDidHideNotification object:nil];
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.view removeGestureRecognizer:dismissKB];
}
- (void)setupDisplay {
    [self registerForKeyboardNotifications];
    [_myCouponsButton setTitle:str(@"my_coupon") forState:UIControlStateNormal];
    
    CGRect frame = _bonusCodeView.bounds ;
    
    [_successViewTitle setText:str(@"Congratulations")];
    bonusCode  = [[[NSBundle mainBundle] loadNibNamed:@"BonusCodeView" owner:self options:nil] objectAtIndex:0];
    bonusCode.delegate = self ;

    [bonusCode setFrame:frame];
    [_bonusCodeView addSubview:bonusCode];
    _toastBar.layer.cornerRadius = 20.f;
    _toastImg.layer.cornerRadius = 10.f;
    
    
    _categoriesButton.layer.cornerRadius = _categoriesButton.frame.size.height/2;
    _categoriesButton.layer.masksToBounds = YES;
    _categoriesButton.layer.borderWidth = 1;
    _categoriesButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _myCouponsButton.layer.cornerRadius = _myCouponsButton.frame.size.height/2;
    _myCouponsButton.layer.masksToBounds = YES;
    _myCouponsButton.layer.borderWidth = 1;
    _myCouponsButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [_categoriesButton setTitle:str(@"All") forState:UIControlStateNormal];
    _catalogListCollectionView.contentInset = UIEdgeInsetsMake(-10, 15, 0, 0);
    
    [self.view layoutIfNeeded];
    
    
    
    _infoView.alpha = 0;
    //    _informationLabel.alpha = 0;
    _infoView.hidden = YES;
    //    _informationView.hidden = YES;
    _infoView.textContainerInset = UIEdgeInsetsMake(7, 7, 7, 7);
    _infoView.text = str(@"keep_interacting_with_the_ad_to_earn_more_points");
    
    _infoView.layer.cornerRadius = _infoView.frame.size.height/2;
    _infoView.layer.masksToBounds = YES;
    [_infoView layoutIfNeeded];
    if (@available(iOS 11.0, *)) {
        
        _infoView.layer.maskedCorners = kCALayerMaxXMaxYCorner  |kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner;
        
    } else { // Fallback on earlier versions }
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, _infoView.frame.size.width, _infoView.frame.size.height) byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){_infoView.frame.size.height, _infoView.frame.size.height}].CGPath;
        
        _infoView.layer.mask = maskLayer;
        _infoView.layer.masksToBounds = YES;
    }
    
    
    [_categoriesButton layoutIfNeeded];
    
    _dismissCategoryButton = [[UIButton alloc] init];
    _dismissCategoryButton.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [_dismissCategoryButton addTarget:self action:@selector(dismissCategory) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_dismissCategoryButton];
    _dismissCategoryButton.hidden = YES;
    
    CGFloat height = MIN(40*_categoryList.count, 200)  ;
    _adviceView = [[UITableView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_categoriesButton.frame)-(_categoriesButton.frame.size.width/2), CGRectGetMaxY(_categoriesButton.frame), _categoriesButton.frame.size.width*1.5, height)];
    [_adviceView setBackgroundColor:darkThemeColor];
    [_adviceView setDelegate:self];
    [_adviceView setDataSource:self];
    [_adviceView setSeparatorColor:UIColor.viewFlipsideBackgroundColor];
    _adviceView.layer.cornerRadius = 5.f ;
    [self.view addSubview:_adviceView];
    
    _adviceView.alpha = 0;
    _adviceView.hidden = YES;
    
    NSMutableAttributedString *secondString = [[NSMutableAttributedString alloc] initWithString:str(@" catalog")];
    NSRange boldedRange = [@" tcoins" rangeOfString:@"t"];
    
    NSDictionary *attrstringColor = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@" tcoins" attributes:attrstringColor];
    
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"SourceSansPro-Bold" size:20]
                       range:boldedRange];
    [secondString appendAttributedString:attrString];
    
    
    _screenTitleLabel.attributedText = secondString;
    
    [self autoSizeOfTable];
    [self initNoGiftView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupData)
                                                 name:@"login_success"
                                               object:nil];
    
    
}
- (void)initNoGiftView {
    noGiftView = [[UIView alloc]initWithFrame:_catalogListCollectionView.frame];
    [self.view insertSubview:noGiftView belowSubview:_adviceView];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(noGiftView.frame.size.width/2-50, 0, 100, 100)];
    [img setImage:[UIImage imageNamed:@"no-notification-image"]];
//    [noGiftView addSubview:img];
    
    UILabel *lb = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, noGiftView.frame.size.width, 30)];
    [lb setText:str(@"sorry_we_do_not_have_gift_available_yet")];
    [lb setTextColor:UIColor.whiteColor];
    lb.textAlignment = NSTextAlignmentCenter;
    [noGiftView addSubview:lb];
    
    CGFloat btnWidth = 220 ;
    CGFloat btnHeight = 40 ;
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(noGiftView.frame.size.width/2-btnWidth/2, 90, btnWidth, btnHeight)];
    [btn setTitle:str(@"continue_watching") forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.cornerRadius = btn.frame.size.height/2;
    [btn setBackgroundColor:UIColor.clearColor];
    btn.layer.borderWidth = 1.f ;
    btn.layer.borderColor = UIColor.whiteColor.CGColor;
    [noGiftView addSubview:btn];
    noGiftView.hidden = YES;
    
}
- (void)didFinishRedeemBonusCode:(NSString *)code Success:(BOOL)success WithResponseObject:(NSDictionary *)responseObject {
    // Success String .
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];

    // OONAGRAB500 successfully entered! You got bonus 500 tcoins!
    // %@ successfully entered! You got bonus %@ tcoins!
    NSString *successStrTemplate = str(@"successfully_entered_You_got_bonus_tcoins");
    NSString *rewardStr = [NSString stringWithFormat:@"%@",responseObject[@"reward"][@"points"]];
    NSString *successStr =  [NSString stringWithFormat:successStrTemplate,code,rewardStr];
    
    NSRange codeRange  = [successStr rangeOfString:code];
    NSRange rewardRange  = [successStr rangeOfString:rewardStr];
//    NSRange rangeNormal = [successStr rangeOfString:str(@"by_buying_you_are_agree_with_terms_Conditions")];
    
    
    UIFont *fontText = [UIFont fontWithName:@"SourceSansPro-Bold" size:14];
    NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
 
    
    NSMutableAttributedString *mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:successStr];
    
    [mutAttrTextViewString addAttributes:dictBoldText range:codeRange];
    [mutAttrTextViewString addAttributes:dictBoldText range:rewardRange];

    
    [_successViewText setAttributedText:mutAttrTextViewString];
    
    
    // fail string
    
    // OONAGRAB500 successfully entered! You got bonus 500 tcoins!
    // %@ successfully entered! You got bonus %@ tcoins!
    NSString *failStrTemplate = str(@"is_not_valid_The_promotion_may_have_ended_or_check_if_you_have_a_typo");
//    NSString *rewardStr = [NSString stringWithFormat:@"%@",responseObject[@"reward"][@"points"]];
    NSString *failStr =  [NSString stringWithFormat:failStrTemplate,code];
    
    codeRange  = [failStr rangeOfString:code];
//    NSRange rewardRange  = [successStr rangeOfString:rewardStr];
    //    NSRange rangeNormal = [successStr rangeOfString:str(@"by_buying_you_are_agree_with_terms_Conditions")];
    
    
//    UIFont *fontText = [UIFont fontWithName:@"SourceSansPro-Bold" size:18];
    dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    
    
    mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:failStr];
    
    [mutAttrTextViewString addAttributes:dictBoldText range:codeRange];
//    [mutAttrTextViewString addAttributes:dictBoldText range:rewardRange];
    
    
    [_failureToastText setAttributedText:mutAttrTextViewString];

    NSLog(@"catalog bonus code response %@",responseObject);
    if (success) {
        gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissCongra)];
        [self.view addGestureRecognizer:gesture];
        [UIView animateWithDuration:.3 animations:^{
    //        [_hideCongratulationConstraints setActive:NO];
            _hideCongratulationConstraints.constant = self.view.frame.size.height*-1;
            [self.view layoutIfNeeded];
        }];
    }else {
         [UIView animateWithDuration:.3 animations:^{
             _toastBar.alpha = 1 ;
         } completion:^(BOOL finished) {
             [self performSelector:@selector(dismissToast) withObject:self afterDelay:10];
         }];
    }
    
   
}
- (void)dismissToast {
    [bonusCode dismissErrorViews];
    [UIView animateWithDuration:.3 animations:^{
        _toastBar.alpha = 0 ;
    }];
}
- (IBAction)dismissToastButtonAction:(id)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self dismissToast];
}
- (void)dismissCongra {
    [self.view removeGestureRecognizer:gesture];
    [bonusCode cleanInputField];
    [UIView animateWithDuration:.3 animations:^{
//        [_hideCongratulationConstraints setActive:YES];
//        _hideCongratulationConstraints.multiplier = .5f;
        _hideCongratulationConstraints.constant = self.view.frame.size.height * -2;
        [bonusCode resetBonusCodeView];
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)bonusCodeSendAction:(id)sender {
//    NSLog(@"%@",_bonusCodeField.text);
    [[self view]endEditing:YES];
    [[NetworkModule instance]verifyBonusCode:_bonusCodeField.text success:^(NSURLSessionDataTask *task, id responseObject){
        NSLog(@"%@",responseObject);
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            NSLog(@"%@",responseObject);
            if ([[responseObject valueForKey:@"reward"] valueForKey:@"pointsBalance"]) {
                [UserData setUserPoints:[NSString stringWithFormat:@"%@",[[responseObject valueForKey:@"reward"] valueForKey:@"pointsBalance"]]];
                
            }
//            [self setupData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPointLabel" object:nil];
            
            if (isSuccess) {
                [_bonusCodeField setText:@""];
                [self showErrorWithMessage:message];
            }
            else{
                [self showErrorWithMessage:message];
            }
            
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

- (void)autoSizeOfTable {
    
    [_categoriesButton layoutIfNeeded];
    CGFloat height = MIN(40*_categoryList.count, 200)  ;
    [UIView animateWithDuration:.3f animations:^{
        CGRect frame = _adviceView.frame;
        frame.size.height = height;
        [_adviceView setFrame:frame];
        
    }];
}

- (void)setupData{
    _tcoinsLabel.text = [NSString stringWithFormat:@"%@",[UserData userPointsString]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Data Source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.catalogList.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   if ( [@"bidBanner" isEqualToString:[NSString stringWithFormat:@"%@",[self.catalogList[indexPath.row]objectForKey:@"type"] ]]) {
        NSDictionary *model = [self.catalogList objectAtIndex:0];
        BrowseLiveBidCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BrowseLiveBidCollectionViewCell" forIndexPath:indexPath];
        [cell.imageView sd_setImageWithURL:[model objectForKey:@"imageUrl"]];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        cell.layer.cornerRadius = 16;
        cell.layer.masksToBounds = YES;
        return cell;
    } else {
        CatalogListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CatalogListCollectionViewCell" forIndexPath:indexPath];
        [cell.catalogRedeemButton setTitle:str(@"redeem") forState:UIControlStateNormal];
        [cell.catalogImageView sd_setImageWithURL:[[_catalogList objectAtIndex:indexPath.row] objectForKey:@"imageUrl"] placeholderImage:[UIImage imageNamed:@"coupon-icon-placeholder"]];
        [cell.overlayImageView sd_setImageWithURL:[[_catalogList objectAtIndex:indexPath.row] objectForKey:@"overlayUrl"]];
        cell.catalogTitleLabel.text = [NSString stringWithFormat:@"%@",[[_catalogList objectAtIndex:indexPath.row] valueForKey:@"name"]];
        NSString *prices =  [NSString stringWithFormat:@"%@",[[_catalogList objectAtIndex:indexPath.row] valueForKey:@"points"]];
        NSNumberFormatter * formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString * priceString =  [formatter stringFromNumber:[NSNumber numberWithInteger:[prices integerValue]]];
//        priceString = [priceString stringByReplacingOccurrencesOfString:@"," withString:@"."];
        cell.catalogPriceLabel.text = priceString;
        cell.catalogRedeemButton.tag = indexPath.row+1;
        [cell.catalogRedeemButton addTarget:self action:@selector(redeemAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.catalogRedeemButton.userInteractionEnabled = NO;
        if ([[[_catalogList objectAtIndex:indexPath.row] objectForKey:@"stock"] intValue] > 0 ){
            cell.outOfStockMask.hidden = YES;
        } else {
            cell.outOfStockMask.hidden = NO;
        }
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float collectionHeight = MAX(collectionView.frame.size.height-10,0);
    return CGSizeMake(collectionHeight*.7315, collectionHeight);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    if ( [@"bidBanner" isEqualToString:[NSString stringWithFormat:@"%@",[self.catalogList[indexPath.row]objectForKey:@"type"] ]]) {
        LiveBidViewController *vc = [[LiveBidViewController alloc]init];
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        if ([self.catalogList[indexPath.row][@"stock"] intValue] > 0 )
            [self fetchGiftDetail:[[self.catalogList objectAtIndex:indexPath.row]valueForKey:@"id"]];
    }
}

- (void)fetchGiftDetail:(NSString *)giftID {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    [[NetworkModule instance]getGiftDetailWithID:giftID success:^(NSURLSessionDataTask *task, id responseObject) {
        [activityIndicator stopAnimating];
        CatalogListDetailsViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CatalogListDetailsViewController"];
        detailVC.couponID = giftID ;
        detailVC.couponDetails = responseObject;
        detailVC.delegate = self ;
//        [self.navigationController pushViewController:detailVC animated:YES];
        [self presentViewController:detailVC animated:YES completion:nil];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [activityIndicator stopAnimating];
    }];
}

- (void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    if (scrollView == _catalogListCollectionView) {
        float scrollViewwidth = scrollView.frame.size.width;
        float scrollContentSizewidth = scrollView.contentSize.width;
        float scrollOffset = scrollView.contentOffset.x;
        
        if (scrollOffset + scrollViewwidth >= scrollContentSizewidth)
        {
            [self loadCatalog];
        }
    }
}

- (IBAction)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)myCouponsButtonAction:(id)sender {
//    if (![CheckSession checkLoginStatus]){
//        [self requireSignIn];
//        return ;
//    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Coupon" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MyCouponsListViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)informationButtonAction:(id)sender {
    if (_infoView.hidden == NO) {
        [self dismissInformation];
        return;
    }
    [self dismissCategory];
    
    _infoView.hidden = NO;
    _infoView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _infoView.alpha = 1;
        _infoView.alpha = 1;
    }];
    
    removeInformationGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissInformation)];
    [self.view addGestureRecognizer:removeInformationGesture];
}

- (void)requireSignIn {
    self.signInAlertView = [[SignInAlertView alloc]init];
    [SignInAlertView add:self.signInAlertView withController:self withDismiss:^{
//        [self play];
    } withAction:^{
        [self popupSignIn];
    }];
}
- (void)popupSignIn {
    
    //    LandingPageViewController *viewController = [[LandingPageViewController alloc]initWithPopUpMode];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LandingPageViewController" bundle:nil];
//    LandingPageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LandingPageViewController"];
//    [viewController setIsPopupMode:YES];
//    LandscapeViewController *navVC = [[LandscapeViewController alloc]initWithRootViewController:viewController];
//    [self presentViewController:navVC animated:YES completion:^{
//        [viewController setIsPopupMode:YES];
//    }];
    
}
- (void)dismissInformation{
    [UIView animateWithDuration:0.2 animations:^{
        _infoView.alpha = 0;
        _infoView.alpha = 0;
    } completion:^(BOOL finished) {
        _infoView.hidden = YES;
        _infoView.hidden = YES;
    }];
    
    [self.view removeGestureRecognizer:removeInformationGesture];
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_categoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //    if (cell == nil)
    //    {
    cell = [[UITableViewCell alloc]
            initWithStyle:UITableViewCellStyleDefault
            reuseIdentifier:CellIdentifier];
    [cell.textLabel setText:[NSString stringWithFormat:@"%@",_categoryList[indexPath.row][@"name"]]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont systemFontOfSize:14]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell setBackgroundColor:darkThemeColor];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = HexColor(0x3C4248);
    [cell setSelectedBackgroundView:bgColorView];
    cell.separatorInset = UIEdgeInsetsZero;
    //    }
    
    return cell;
}
#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        _selectedCategory = nil;
    }
    else{
        _selectedCategory = [[_categoryList objectAtIndex:indexPath.row] valueForKey:@"id"];
    }
    _catalogList = [[NSMutableArray alloc] init];
    [_catalogListCollectionView reloadData];
    [self loadCatalog];
    [self dismissCategory];
    
    [_categoriesButton setTitle:[NSString stringWithFormat:@"%@",[[_categoryList objectAtIndex:indexPath.row] valueForKey:@"name"]] forState:UIControlStateNormal];
    _categoriesButton.titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (IBAction)categoryButtonAction:(id)sender {
    [self loadCategory];
}

- (void)showCategory{
    if (_adviceView.hidden == NO) {
        [self dismissCategory];
        return;
    }
    [self dismissInformation];
    
    _adviceView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _adviceView.alpha = 1;
    }];
    
    _dismissCategoryButton.hidden = NO;
    
//    removeCategoryGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissCategory)];
//    [self.view addGestureRecognizer:removeCategoryGesture];
}

- (void)dismissCategory{
    [UIView animateWithDuration:0.2 animations:^{
        _adviceView.alpha = 0;
    } completion:^(BOOL finished) {
        _adviceView.hidden = YES;
    }];
    
    _dismissCategoryButton.hidden = YES;
    
//    [self.view removeGestureRecognizer:removeCategoryGesture];
}

-(IBAction)redeemAction:(id)sender{
    UIButton *pressedButton = (UIButton *)sender;
    CustomPopup *popup = [[CustomPopup alloc] initWithMessage:@""];
    NSString *areYouSureStr = [NSString stringWithFormat:@"Are you sure want to buy for %@ coins?",[[_catalogList objectAtIndex:pressedButton.tag-1] valueForKey:@"points"]];
    NSString *expireWarning = [NSString stringWithFormat:str(@"redeem_expire_warning"),@"7"];
    popup.backBtn.hidden = YES;
    
    popup.info.text =  [NSString stringWithFormat:@"%@\n%@\n%@",areYouSureStr
                        ,expireWarning
                        ,str(@"by_buying_you_are_agree_with_terms_Conditions")];
    
    NSRange rangeBold  = [popup.info.text rangeOfString:areYouSureStr];
    NSRange rangeSmall = [popup.info.text rangeOfString:str(@"by_buying_you_are_agree_with_terms_Conditions")];
    NSRange rangeMiddle = [popup.info.text rangeOfString:expireWarning];
    
    UIFont *fontText = [UIFont fontWithName:@"SourceSansPro-Bold" size:18];
    NSDictionary *dictBoldText = [NSDictionary dictionaryWithObjectsAndKeys:fontText, NSFontAttributeName, nil];
    NSDictionary *colorAttribute = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    NSDictionary *dictMediumText = @{ NSFontAttributeName : [UIFont fontWithName:@"SourceSansPro-Regular" size:16]};
    NSDictionary *dictSmallText = @{ NSFontAttributeName : [UIFont fontWithName:@"SourceSansPro-Regular" size:12]};
    
    NSMutableAttributedString *mutAttrTextViewString = [[NSMutableAttributedString alloc] initWithString:popup.info.text];
    [mutAttrTextViewString addAttributes:dictBoldText range:rangeBold];
    [mutAttrTextViewString addAttributes:colorAttribute range:rangeBold];
    [mutAttrTextViewString addAttributes:dictSmallText range:rangeSmall];
    [mutAttrTextViewString addAttributes:colorAttribute range:rangeSmall];
    [mutAttrTextViewString addAttributes:dictMediumText range:rangeMiddle];
    [mutAttrTextViewString addAttributes:colorAttribute range:rangeMiddle];
    
    [popup.info setAttributedText:mutAttrTextViewString];
    
    [popup.button1 setTitle:@"Cancel" forState:UIControlStateNormal];
    [popup.button2 setTitle:@"Redeem" forState:UIControlStateNormal];
    __block CustomPopup *popupBlock = popup;
    popup.completionButton1 = ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [popupBlock dismiss];
        });
    };
    popup.completionButton2 = ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [popupBlock dismiss];
        });
    };
    popup.alpha = 0;
    [self.view addSubview:popup];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popup.alpha = 1;
                     }
                     completion:nil];
}
- (void)hideBonusCode {
    [_bonusCodeView setHidden:YES];
}
- (void)showBonusCode {
    [_bonusCodeView setHidden:NO];
}
- (void)loadCatalog {
    if (loading) {
        return;
    }
    
    loading = YES;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"%@points/gifts",API_URL];
    NSArray *headers = @[@{@"key":@"Authorization",@"value":[NSString stringWithFormat:@"Bearer %@",[UserData userToken]]}];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (_catalogList.count > 0) {
        // have item, so it is fetch more
        [param setObject:[NSString stringWithFormat:@"%lu",(unsigned long)_catalogList.count] forKey:@"offset"];
    }else{
        [param setObject:@(0)forKey:@"offset"];
    }
    if (_selectedCategory) {
        [param setObject:_selectedCategory forKey:@"categoryId"];
    }
    NSLog(@"tcoin load param = \n %@",param);
    [[NetworkModule instance] POST:url parameters:param headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"tcoin load = \n %@",responseObject);
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        if ([@"1" isEqualToString:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"disableBonusCodeInput"]]]){
            [self hideBonusCode];
        }else {
            [self showBonusCode];
        }
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess) {
                if ([responseObject valueForKey:@"pointsBalance"]) {
                    [UserData setUserPoints:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"pointsBalance"]]];
                    
//                    [self setupData];
                }
                if ([responseObject valueForKey:@"gifts"]) {
                    if ([[responseObject valueForKey:@"gifts"] count] > 0) {
                        [_catalogList addObjectsFromArray:[responseObject valueForKey:@"gifts"]];
                    }
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPointLabel" object:nil];
                [self reloadCatalogCollection];
            } else {
                [self showErrorWithMessage:message];
            }
            loading = NO;
            [activityIndicator stopAnimating];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        loading = NO;
        [self reloadCatalogCollection];
        [activityIndicator stopAnimating];
    }];
}
- (void)reloadCatalogCollection {
    if (_catalogList.count == 0) {
        noGiftView.hidden = NO;
    }else{
        noGiftView.hidden = YES;
    }
    [_catalogListCollectionView reloadData];
}
- (void)loadCategory {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];

    [[NetworkModule instance] getGiftCategoriesWithSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"tcoin cat load%@",responseObject);
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess) {
    
                _categoryList = [[NSMutableArray alloc] initWithArray:[responseObject objectForKey:@"categories"]];
                [_categoryList insertObject:@{@"name":str(@"All"),@"id":@"0"} atIndex:0];
                [_adviceView reloadData];
                [self autoSizeOfTable];
                [self showCategory];
                
            }
            else{
                [self showErrorWithMessage:message];
            }
            [activityIndicator stopAnimating];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self showErrorWithMessage:[error localizedDescription]];
        [activityIndicator stopAnimating];
    }];
}

- (void)showErrorWithMessage :(NSString *)message{
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:message];
    
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    
    [popup setupOneButton];
    [self.view addSubview:popup];
    __block CustomPopup *popupBlock = popup;
    
    popup.completionButton1  = ^{
        [popupBlock dismiss];
        
    };
    
}

#pragma mark - Catalog List Details Delegate

- (void)didFinishRedeemVoucherWithResponseObject:(NSDictionary *)responseObject isTopup:(BOOL)isTopup {
    if (responseObject) {
        if ([responseObject valueForKey:@"pointsBalance"]) {
            [UserData setUserPoints:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"pointsBalance"]]];
        }
        
    
//    [self setupData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshPointLabel" object:nil];
        int responseCode = [[responseObject valueForKey:@"code"] intValue];
        if (responseCode == 11057) {
            [self showErrorWithMessage:str(@"pending_text_topup")];
        }else{
//            [self showDialogRedeemSuccessWithObject:responseObject];
            if (isTopup){
                [self showErrorWithMessage:str(@"topup_redeem_success")];
            }else{
                [self myCouponsButtonAction:nil];
            }
        }
    }
    
}

- (void)showDialogRedeemSuccessWithObject:(NSDictionary *)object{
    CustomPopup *popup = [[CustomPopup alloc] initWithMessage:[NSString stringWithFormat:@"Congratulations !,\nYour voucher successfully redeemed."]];
    popup.button2.hidden = YES;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    CGRect buttonFrame = popup.button1.frame;
    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
    popup.button1.frame = buttonFrame;
    __block CustomPopup *popupBlock = popup;
    popup.completionButton1 = ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [popupBlock dismiss];
            [self myCouponsButtonAction:nil];
        });
    };
    popup.alpha = 0;
    [self.view addSubview:popup];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popup.alpha = 1;
                     }
                     completion:nil];
}
@end
