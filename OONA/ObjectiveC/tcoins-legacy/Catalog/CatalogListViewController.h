//
//  CatalogListViewController.h
//  OONA
//
//  Created by OONA iOS on 1/5/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OONABaseViewController.h"
#import "SignInAlertView.h"
#import "BonusCodeView.h"
@interface CatalogListViewController : OONABaseViewController <BonusCodeViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *catalogListCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *categoriesButton;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIView *informationView;
@property (nonatomic, strong) UITableView *adviceView;
@property (weak, nonatomic) IBOutlet UILabel *screenTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *myCouponsButton;
@property (weak, nonatomic) IBOutlet UILabel *tcoinsLabel;
@property (nonatomic, strong) SignInAlertView *signInAlertView;
@property (weak, nonatomic) IBOutlet UITextField *bonusCodeField;
@property (weak, nonatomic) IBOutlet UITextView *infoView;
@property (weak, nonatomic) IBOutlet BonusCodeView *bonusCodeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hideCongratulationConstraints;
@property (weak, nonatomic) IBOutlet UIView *toastBar;
@property (weak, nonatomic) IBOutlet UIImageView *toastImg;
@property (weak, nonatomic) IBOutlet UILabel *successViewText;
@property (weak, nonatomic) IBOutlet UILabel *successViewTitle;
@property (weak, nonatomic) IBOutlet UILabel *failureToastText;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)myCouponsButtonAction:(id)sender;
- (IBAction)categoryButtonAction:(id)sender;

- (IBAction)informationButtonAction:(id)sender;
@end
