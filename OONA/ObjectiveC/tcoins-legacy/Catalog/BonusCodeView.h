//
//  BonusCodeView.h
//  OONA
//
//  Created by jack on 20/12/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "OONAButton.h"
@import UIKit ;
@import SpinKit;

@protocol BonusCodeViewDelegate <NSObject>
@optional
- (void)didFinishRedeemBonusCode:(NSString *)code Success:(BOOL)success WithResponseObject:(NSDictionary *)responseObject;
- (void)dismissToast;

@end


@interface BonusCodeView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet OONAButton *submitButton;
@property (weak, nonatomic) IBOutlet RTSpinKitView *spinner;
@property (weak, nonatomic) IBOutlet UIView *spinnerBackView;
@property (weak, nonatomic) IBOutlet UIView *spinnerBlockerView;
@property (weak, nonatomic) IBOutlet UIView *successBlockerView;
@property (weak, nonatomic) IBOutlet UIView *failureBlockerView;
@property (nonatomic, weak) id<BonusCodeViewDelegate> delegate;
- (void)dismissErrorViews;
- (void)resetBonusCodeView ;
- (void)cleanInputField;
@end
