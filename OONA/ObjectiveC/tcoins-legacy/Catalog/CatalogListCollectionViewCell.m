//
//  CatalogListCollectionViewCell.m
//  OONA
//
//  Created by OONA iOS on 1/5/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "CatalogListCollectionViewCell.h"

@implementation CatalogListCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 16;
    self.layer.masksToBounds = YES;
    _catalogRedeemButton.layer.cornerRadius = _catalogRedeemButton.frame.size.height/2;
    _catalogRedeemButton.layer.masksToBounds = YES;
}
@end
