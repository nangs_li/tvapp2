//
//  PlaceBidViewController.h
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "OONA-Swift.h"
#import "ConfirmBidViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomPopup.h"
//#import "CheckSession.h"
#import "SignInAlertView.h"
//#import "LandingPageViewController.h"

@interface PlaceBidViewController : UIViewController<UITextFieldDelegate> {
    int maxValue;
    int timeCount;
    NSTimer *timer;
}
@property (nonatomic) int timeLeft;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameProductBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxPointBidLabel;
@property (weak, nonatomic) IBOutlet UISlider *bidSlider;
@property (weak, nonatomic) IBOutlet UITextField *pointBidTextField;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minButton;
@property (weak, nonatomic) IBOutlet UIButton *placeBidButton;
@property (strong, nonatomic) NSNumber *valueBid;
@property (strong, nonatomic) NSDictionary *bidDetailDict;
@property (weak, nonatomic) IBOutlet UIView *blackBg;
@property (weak, nonatomic) IBOutlet UILabel *errorLB;
@property (nonatomic, strong) SignInAlertView *signInAlertView;

@end
