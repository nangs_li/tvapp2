//
//  LiveBidCollectionViewCell.m
//  OONA
//
//  Created by OONA on 13/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "LiveBidCollectionViewCell.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
@implementation LiveBidCollectionViewCell{
    CAGradientLayer *gradient ;
    CAGradientLayer *superBidGradient ;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.bidButton
    self.bidButton.layer.cornerRadius = 15 ;
    self.superBidButton.layer.cornerRadius = 15 ;
    self.superBidButton.layer.masksToBounds = YES;
    self.superBidButton.layer.borderColor = UIColor.darkGrayColor.CGColor ;
    self.superBidButton.layer.borderWidth = 2.f;
    self.bidButton.layer.borderColor = UIColor.clearColor.CGColor ;
    self.bidButton.layer.borderWidth = 2.f;
    self.bidButton.layer.masksToBounds = YES;
    
//    });
    
}
- (void)initGradient {
//    [CATransaction setDisableActions:YES];
//
//    gradient = [CAGradientLayer layer];
//    gradient.frame = self.bidButton.bounds;
//    [self.bidButton.layer insertSublayer:gradient atIndex:0];
//    gradient.startPoint = CGPointMake(0.0, 0.5);
//    gradient.endPoint = CGPointMake(1.0, 0.5);
//    gradient.cornerRadius = 15 ;
//    gradient.masksToBounds = YES;
//    gradient.colors = @[(id)HexColor(0xd86a35).CGColor,(id)HexColor(0xd86a35).CGColor];
//
//    superBidGradient = [CAGradientLayer layer];
//    superBidGradient.frame = self.superBidButton.bounds;
//    [self.superBidButton.layer insertSublayer:superBidGradient atIndex:0];
//    superBidGradient.startPoint = CGPointMake(0.0, 0.5);
//    superBidGradient.endPoint = CGPointMake(1.0, 0.5);
//    superBidGradient.cornerRadius = 15 ;
//    superBidGradient.masksToBounds = YES;
//    superBidGradient.colors = @[(id)HexColor(0xaf3c94).CGColor,(id)HexColor(0x6c2d78).CGColor];
}
- (void)superBidBtn {

    _superBidButton.hidden = NO;
    _bidButton.hidden = YES;
    
    
//    gradient.masksToBounds = YES;
    
}
- (void)normalBidBtn {
    _superBidButton.hidden = YES;
    _bidButton.hidden = NO;
}
@end
