//
//  DetailBidViewController.h
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "String.h"
#import "CouponDetails.h"
#import "CouponTermsConditions.h"


#import "NetworkModule.h"
#import "UserData.h"
#import "PlaceBidViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OONAViewGenerator.h"
#import "SignInAlertView.h"
#import "OONABaseViewController.h"
@interface DetailBidViewController : OONABaseViewController<UIScrollViewDelegate> {
    int timeCount;
    NSTimer *timer;
}
@property (strong, nonatomic) NSString *pointBid;
@property (weak, nonatomic) IBOutlet UILabel *tcoinsLabel;
@property (weak, nonatomic) IBOutlet UIView *tcoinsView;
@property (weak, nonatomic) IBOutlet UIImageView *tcoinsImageView;
@property (nonatomic) int timeLeft;
@property (nonatomic, strong) NSDictionary *itemBidDetailDict;
@property (nonatomic) NSMutableArray *buttonArray;
@property (weak, nonatomic) IBOutlet UIView *bidView;
@property (weak, nonatomic) IBOutlet UIView *separateLine;
@property (weak, nonatomic) IBOutlet UILabel *opensAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *bidTitleLabel;

@property (nonatomic, strong)UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *leftHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHeaderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *bidButton;
@property (weak, nonatomic) IBOutlet UIImageView *bidImage;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIButton *tncButton;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *bidDetailsView;
@property (strong, nonatomic) NSDictionary *bidDetailDict;
@property (nonatomic,strong)NSArray *descriptionArray;
@property (nonatomic,strong)NSArray *tncArray;
@property (nonatomic,strong)NSArray *contentArray;
@property (nonatomic, strong) SignInAlertView *signInAlertView;
@property (nonatomic) UIView *selectedIndicator;
@property (nonatomic) CouponDetails *couponDetailsSegmentedView;
@property (nonatomic) CouponTermsConditions *couponTermsConditions;

@end
