//
//  LiveBidViewController.h
//  OONA
//
//  Created by OONA on 13/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveBidCollectionViewCell.h"
#import "NetworkModule.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserData.h"
#import "CustomPopup.h"
#import "DetailBidViewController.h"
#import "PopupWebViewController.h"
#import "OONABaseViewController.h"
@interface LiveBidViewController : OONABaseViewController<UICollectionViewDelegate,UICollectionViewDataSource> {
    BOOL loading;
    BOOL isTimerOn;
    int timeCount;
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UITextView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *bidTitleLabel;
@property (strong, nonatomic) NSMutableArray *itemLiveBid;
@property (weak, nonatomic) IBOutlet UILabel *tcoinLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftHeaderLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *bidNowButton;
@property (weak, nonatomic) IBOutlet UIButton *winnerButton;
@property (weak, nonatomic) IBOutlet UIImageView *bidNowBannerImg;
- (IBAction)infoBtnPressed:(id)sender;

@end
