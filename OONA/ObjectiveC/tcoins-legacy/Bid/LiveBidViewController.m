//
//  LiveBidViewController.m
//  OONA
//
//  Created by OONA on 13/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "LiveBidViewController.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
@import SDWebImage ;
@interface LiveBidViewController (){
    UITapGestureRecognizer *removeInformationGesture;
}

@end

@implementation LiveBidViewController

- (void)viewDidLoad {
    
    self.viewName = @"ui_points_bid_item_list";
    [super viewDidLoad];
    [self initView];
    [self initButton];
    [self initCollection];
    self.itemLiveBid = [[NSMutableArray alloc]init];
    [self getItemLiveBid];
    [self timerCountdown];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshList)
                                                 name:@"finishBid"
                                               object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)refreshList {
    self.itemLiveBid = [[NSMutableArray alloc]init];
    [self getItemLiveBid];
}
- (void)initView {
    [_bidTitleLabel setText:str(@"bid_title")];
    self.tcoinLabel.text = [NSString stringWithFormat:@"%@",[UserData userPointsString]];
    
    _infoView.alpha = 0;
    //    _informationLabel.alpha = 0;
    _infoView.hidden = YES;
    //    _informationView.hidden = YES;
    _infoView.textContainerInset = UIEdgeInsetsMake(7, 7, 7, 7);
    _infoView.text = str(@"keep_interacting_with_the_ad_to_earn_more_points");
    
    _infoView.layer.cornerRadius = _infoView.frame.size.height/2;
    _infoView.layer.masksToBounds = YES;
    [_infoView layoutIfNeeded];
    if (@available(iOS 11.0, *)) {
        
        _infoView.layer.maskedCorners = kCALayerMaxXMaxYCorner  |kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner;
        
    } else { // Fallback on earlier versions }
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, _infoView.frame.size.width, _infoView.frame.size.height) byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){_infoView.frame.size.height, _infoView.frame.size.height}].CGPath;
        
        _infoView.layer.mask = maskLayer;
        _infoView.layer.masksToBounds = YES;
    }
   
    
}

- (void)initButton {
    [self.backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    [self.bidNowButton addTarget:self action:@selector(bidNow) forControlEvents:UIControlEventTouchUpInside];
    [self.winnerButton addTarget:self action:@selector(winner) forControlEvents:UIControlEventTouchUpInside];
    self.winnerButton.layer.cornerRadius = self.winnerButton.frame.size.height /2 ;
    self.winnerButton.layer.borderColor = UIColor.whiteColor.CGColor;
    self.winnerButton.layer.borderWidth = 1.f ;
    _bidNowBannerImg.hidden = YES ;
    [_bidNowBannerImg setImage:[UIImage imageNamed:str(@"bid_now_straight_banner_img")]];
}

- (void)initCollection {
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveBidCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveBidCollectionViewCell"];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    self.collectionView.delegate = self ;
}

#pragma mark button action

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)bidNow {
    [self goToDetailBid:0];
}

- (void)winner {
    PopupWebViewController *vc = [[PopupWebViewController alloc] initWithUrl:@"https://oona.tv/ngebid/pemenang.html"];
    vc.title = @"Winners";
    //loginViewController.modalPresentationStyle = .overCurrentContext
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self.navigationController pushViewController:webViewController animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)bidDetail:(UIButton*)sender {
 
    CGPoint center = sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:rootViewPoint];
    [self goToDetailBid:indexPath.row];
}

- (void)goToDetailBid:(NSUInteger)index {
    NSDictionary *model = [self.itemLiveBid objectAtIndex:index];
    DetailBidViewController *vc = [[DetailBidViewController alloc]init];
    vc.bidDetailDict = model;
    vc.timeLeft = [[model valueForKey:@"expiresIn"]intValue] - timeCount;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self.navigationController pushViewController:detailBid animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark collectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _itemLiveBid.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LiveBidCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LiveBidCollectionViewCell" forIndexPath:indexPath];
    NSDictionary *model = [_itemLiveBid objectAtIndex:indexPath.row];
    cell.nameLabel.text = [model valueForKey:@"name"];
    cell.tcoinsView.layer.cornerRadius = 16;
    cell.tcoinsView.layer.borderWidth = 1;
    cell.tcoinsView.layer.borderColor = [Color orangeBidColor].CGColor;
    NSLog(@"%@",model);
    [cell initGradient];
    if ([@"1" isEqualToString:[NSString stringWithFormat:@"%@",[[_itemLiveBid objectAtIndex:indexPath.row]valueForKey:@"isSuperbid"  ]]]){
//        NSLog(@"super bid");
        [cell superBidBtn];
    }else{
//        NSLog(@"normal bid");
        [cell normalBidBtn];
    }
    
    if ([model valueForKey:@"expiresIn"] != nil) {
        int totalTime = [[model valueForKey:@"expiresIn"]intValue] - timeCount;
        int hours = totalTime / 3600;
        int minutes = (totalTime % 3600) / 60;
        int sec = (totalTime % 3600) % 60;
        if (hours < 24) {
            
            cell.timeLabel.text = [NSString stringWithFormat:@"%@ %02d:%02d:%02d",str(@"bid_close"),hours,minutes,sec];
        } else {
            int days = hours / 24 ;
            int hoursFilter = hours - (24*days) ;
            cell.timeLabel.text = [NSString stringWithFormat:@"Closes in %d days %d hours",days,hoursFilter];
            cell.timeLabel.text = [NSString stringWithFormat:@"%@ %d %@ %d %@",str(@"bid_close"),days,str(@"bid_days"),hoursFilter,str(@"bid_hours")];
        }
        if ([model valueForKey:@"myBid"] != nil) {
            cell.tcoinsImageView.hidden = NO;
            cell.tcoinsView.hidden = NO;
            cell.tcoinsLabel.hidden = NO;
            int points = [[[model valueForKey:@"myBid"] valueForKey:@"points"]intValue];
            cell.tcoinsLabel.text = [NSString stringWithFormat:@"%d",points];
            cell.bidButton.hidden = YES;
            cell.timeLabel.hidden = NO;
            cell.timeOpensLabel.hidden = YES;
        } else {
            cell.tcoinsImageView.hidden = YES;
            cell.tcoinsView.hidden = YES;
            cell.tcoinsLabel.hidden = YES;
            cell.bidButton.hidden = NO;
            cell.timeLabel.hidden = NO;
            cell.timeOpensLabel.hidden = YES;
        }
        isTimerOn = YES;
    } else {
        cell.tcoinsImageView.hidden = YES;
        cell.tcoinsView.hidden = YES;
        cell.tcoinsLabel.hidden = YES;
        cell.bidButton.hidden = YES;
        cell.timeLabel.hidden = YES;
        cell.timeOpensLabel.hidden = NO;
        cell.tcoinsImageView.hidden = YES;
        cell.tcoinsView.hidden = YES;
        cell.tcoinsLabel.hidden = YES;
        // 2018-08-15 12:11:49
        // yyyy-MM-dd HH:mm:ss ZZZ
        NSDateFormatter* openDateFormat = [[NSDateFormatter alloc] init];
        [openDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate* date = [openDateFormat dateFromString:[model valueForKey:@"opensAt"]];
        NSDateFormatter* openDateFormatAfter = [[NSDateFormatter alloc] init];
        [openDateFormatAfter setDateFormat:@"MMM dd, hh:mm a"];
        NSString *dateString = [openDateFormatAfter stringFromDate:date];
        cell.timeOpensLabel.text = [NSString stringWithFormat:@"%@ %@",str(@"bid_open"),dateString];
    }
    [cell.imageView sd_setImageWithURL:[model objectForKey:@"imageUrl"]];
    if ([model objectForKey:@"overlayUrl"]!= nil){
        [cell.overlayImg sd_setImageWithURL:[model objectForKey:@"overlayUrl"]];
    }else{
        [cell.overlayImg sd_setImageWithURL:nil];
    }



    cell.layer.cornerRadius = 16;
    cell.layer.masksToBounds = YES;
    cell.bidButton.tag = indexPath.row;
    [cell.bidButton setTitle:str(@"bid_now") forState:UIControlStateNormal];
    [cell.superBidButton setTitle:str(@"bid_now") forState:UIControlStateNormal];
    [cell.bidButton addTarget:self action:@selector(bidDetail:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float collectionHeight = collectionView.frame.size.height;
    return CGSizeMake(collectionHeight*.7315, collectionHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"click item %@",self.itemLiveBid[indexPath.row]);
    if ([@"0" isEqualToString:[NSString stringWithFormat:@"%@",[self.itemLiveBid[indexPath.row] objectForKey:@"allowOpen"]]]){
        return ;
    }
    
    NSDictionary *model = [self.itemLiveBid objectAtIndex:indexPath.row];
//    if ([model valueForKey:@"myBid"] != nil) {
        DetailBidViewController *detailBid = [[DetailBidViewController alloc]init];
        detailBid.bidDetailDict = model;
        if ([model valueForKey:@"myBid"] != nil) {
            int points = [[[model valueForKey:@"myBid"] valueForKey:@"points"]intValue];
            detailBid.pointBid = [NSString stringWithFormat:@"%d",points];
        }
        detailBid.timeLeft = [[model valueForKey:@"expiresIn"]intValue] - timeCount;
        [self presentViewController:detailBid animated:YES completion:nil];
        
//    }
}

#pragma mark network request

- (void)getItemLiveBid {
    if (loading) {
        return;
    }
    
    loading = YES;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"%@points/bid/items",API_URL];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSArray *headers = @[@{@"key":@"uuid",@"value":uuidString}];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (self.itemLiveBid.count > 0) {
        // have item, so it is fetch more
        [param setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.itemLiveBid.count] forKey:@"offset"];
    } else {
        [param setObject:@(0)forKey:@"offset"];
    }

    [[NetworkModule instance] POST:url parameters:param headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"getItemLiveBid = \n %@",responseObject);
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
//        if (responseObject [@"verticalBannerImageUrl"] != nil) {
//            _bidNowBannerImg.layer.cornerRadius = 5.f ;
//            _bidNowBannerImg.layer.masksToBounds = YES ;
            _bidNowBannerImg.hidden = NO ;
            _bidNowBannerImg.contentMode = SDImageScaleModeAspectFit ;
//            [_bidNowBannerImg sd_setImageWithURL:responseObject[@"verticalBannerImageUrl"]];
//        }
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess) {
                if ([responseObject valueForKey:@"items"]) {
                    if ([[responseObject valueForKey:@"items"] count] > 0) {
                        [self.itemLiveBid addObjectsFromArray:[responseObject valueForKey:@"items"]];
                        timeCount = 0;
                    }
                }
                [self.collectionView reloadData];
                [self.collectionView setNeedsLayout];
                [self.collectionView layoutIfNeeded];
            } else {
                [self showErrorWithMessage:message];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                loading = NO;
            });
            
            [activityIndicator stopAnimating];
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        loading = NO;
        [self.collectionView reloadData];
        [activityIndicator stopAnimating];
    }];
}

- (void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    if (scrollView == _collectionView) {
        float scrollViewwidth = scrollView.frame.size.width;
        float scrollContentSizewidth = scrollView.contentSize.width;
        float scrollOffset = scrollView.contentOffset.x;
        
        if (scrollOffset + scrollViewwidth >= scrollContentSizewidth)
        {
            [self getItemLiveBid];
        }
    }
}
- (void)showErrorWithMessage:(NSString *)message {
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:message];
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    [popup setupOneButton];
    [self.view addSubview:popup];
    __block CustomPopup *popupBlock = popup;
    popup.completionButton1  = ^{[popupBlock dismiss];};
}

- (void)timerCountdown {
    timeCount = timeCount + 1;
    [self.collectionView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self timerCountdown];
    });
}
- (void)resetCountDown {
    timeCount = 0 ;
}

- (IBAction)infoBtnPressed:(id)sender {
    if (_infoView.hidden == NO) {
        [self dismissInformation];
        return;
    }

    
    _infoView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        _infoView.alpha = 1;
        
    }];
    
    removeInformationGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissInformation)];
    [self.view addGestureRecognizer:removeInformationGesture];
    
}
- (void)dismissInformation{
    [UIView animateWithDuration:0.2 animations:^{
        _infoView.alpha = 0;
        _infoView.alpha = 0;
    } completion:^(BOOL finished) {
        _infoView.hidden = YES;
        _infoView.hidden = YES;
    }];
    
    [self.view removeGestureRecognizer:removeInformationGesture];
}
@end
