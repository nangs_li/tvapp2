//
//  FinishBidViewController.m
//  OONA
//
//  Created by OONA on 15/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "FinishBidViewController.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
@interface FinishBidViewController ()

@end

@implementation FinishBidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    self.nameLabel.text = [NSString stringWithFormat:@"%@, %@!",str(@"bid_good_luck"),[UserData userName]];
    self.youBidLabel.text = str(@"bid_you");
    self.nameProductLabel.text = [self.bidDetailDict valueForKey:@"name"];
    self.tcoinBidLabel.text = [NSString stringWithFormat:@"%@",self.tcoinBidNumber];
    [self.imageView sd_setImageWithURL:[self.bidDetailDict objectForKey:@"imageUrl"]];
    self.finishBidButton.layer.cornerRadius = 16;
    self.finishBidButton.layer.borderWidth = 3;
    self.finishBidButton.layer.borderColor = [Color orangeBidColor].CGColor;
    [self.finishBidButton setTitle:str(@"bid_see_next_week") forState:UIControlStateNormal];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(close)
//                                                 name:@"finishBid"
//                                               object:nil];
    self.blackBG.layer.cornerRadius = 16;
    self.nameLabel.text = [NSString stringWithFormat:@"Good Luck, %@!",[UserData userName]];
    self.nameProductLabel.text = [self.bidDetailDict valueForKey:@"name"];
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * tcoinString =  [formatter stringFromNumber:self.tcoinBidNumber];
    
    
    self.tcoinBidLabel.text = [NSString stringWithFormat:@"%@",tcoinString];
    [self.imageView sd_setImageWithURL:[self.bidDetailDict objectForKey:@"imageUrl"]];
    self.imageView.layer.cornerRadius = 16;
    self.finishBidButton.layer.cornerRadius = self.finishBidButton.frame.size.height/2;
    self.finishBidButton.layer.borderWidth = 3;
    self.finishBidButton.layer.borderColor = [Color orangeBidColor].CGColor;
}

- (void)initButton {
    [self.closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.finishBidButton addTarget:self action:@selector(finishBid) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark button action

- (void)close {
//    [self goToMainPage];
    NSLog(@"finsih bid close no animation");
    [[NSNotificationCenter defaultCenter]postNotificationName:@"finishBid" object:nil userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"finishBid" object:nil userInfo:nil];
    
    
}

- (void)finishBid {
//    [self goToMainPage];
//    [self dismissViewControllerAnimated:YES completion:nil];
//     [[NSNotificationCenter defaultCenter]postNotificationName:@"finishBid" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"finishBidAndGoBidList" object:nil userInfo:nil];
    [self close];
//    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)goToMainPage {
//    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateInitialViewController];
//    LandscapeViewController *navCon = [[LandscapeViewController alloc]initWithRootViewController:viewController];
//    self.window.rootViewController = navCon;
//    [self.window makeKeyAndVisible];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushToListTcoinsItem" object:self];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushToListBid" object:self];
    });
}

@end
