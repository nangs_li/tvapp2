//
//  ConfirmBidViewController.m
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "ConfirmBidViewController.h"
//#import "CheckSession.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
@import FirebaseMessaging;
@interface ConfirmBidViewController ()

@end

@implementation ConfirmBidViewController{
    BOOL haveName ;
    BOOL havePhone ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initButton];
    [self loadProfileData];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(close)
                                                 name:@"finishBid"
                                               object:nil];
    self.view.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    self.blackBG.layer.cornerRadius = 16;
    self.imageView.layer.cornerRadius = 16;
    self.imageView.layer.masksToBounds = YES;
    self.confirmBidButton.layer.cornerRadius = self.confirmBidButton.frame.size.height/2;
    self.confirmBidButton.layer.borderWidth = 3;
    self.confirmBidButton.layer.borderColor = [Color orangeBidColor].CGColor;
//    self.editProfileView.layer.cornerRadius = self.editProfileView.frame.size.height/2;
//    self.editProfileView.layer.borderWidth = 1;
//    self.editProfileView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.checkBoxTnCView.layer.cornerRadius = 5;
    self.checkBoxTnCView.layer.masksToBounds = YES;
    self.checkBoxProfileView.layer.cornerRadius = 5;
    self.checkBoxProfileView.layer.masksToBounds = YES;
    [self.phoneLB setText:str(@"bid_confirm_contact_info")];
    isCheckListTnC = NO;
    isCheckListProfile = NO;
    
    self.nameLabel.text = [self.bidDetailDict valueForKey:@"name"];
    
    
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * priceString =  [formatter stringFromNumber:self.tcoinBidNumber];
    self.pointBidLabel.text = [NSString stringWithFormat:@"%@",priceString];
    [self.imageView sd_setImageWithURL:[self.bidDetailDict objectForKey:@"imageUrl"]];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"phonebid"];
//    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"emailbid"];
    
    self.tncLabel.text = str(@"bid_read_tnc_label");
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:str(@"bid_read_tnc_button")];
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    UIColor *color = [UIColor whiteColor];
    NSRange titleRange = NSMakeRange(0, [commentString length]);
    [commentString addAttribute:NSForegroundColorAttributeName value:color range:titleRange];
    [self.tncButton setAttributedTitle:commentString forState:UIControlStateNormal];
    self.confirmLabel.text = str(@"bid_confirm_your_bid");
    
}

- (void)initButton {
    [self.closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.tncButton addTarget:self action:@selector(tnc) forControlEvents:UIControlEventTouchUpInside];
    [self.editProfileButton addTarget:self action:@selector(editProfile) forControlEvents:UIControlEventTouchUpInside];
    [self.checkBoxTnCButton addTarget:self action:@selector(checkBoxTnC) forControlEvents:UIControlEventTouchUpInside];
    [self.checkBoxProfileButton addTarget:self action:@selector(checkBoxProfile) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBidButton addTarget:self action:@selector(confirmBid) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark button action

- (void)close {
    NSLog(@"confirm bid close ");
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)tnc {
    PopupWebViewController *webViewController = [[PopupWebViewController alloc]
                                                 initWithUrl:@"https://oona.tv/ngebid/snk.html"];
    webViewController.isPresent = YES;
    webViewController.title = @"Terms and Conditions";
    [self presentViewController:webViewController animated:YES completion:nil];
}

- (void)editProfile {
    EditProfileBidViewController *editProfileBid = [[EditProfileBidViewController alloc]init];
    if (haveName)
        editProfileBid.email = self.userEmailLabel.text;
    else
        editProfileBid.email = @"";
    
    if (havePhone)
        editProfileBid.phone = self.userPhoneLabel.text;
    else
        editProfileBid.phone = @"";
    [self presentViewController:editProfileBid animated:NO completion:nil];
}

- (void)checkBoxProfile {
    if ([self.userEmailLabel.text isEqualToString:@"-"] || [self.userPhoneLabel.text isEqualToString:@"-"]) {
//        [self showErrorWithMessage:@"Please complete your profile"];
        [self editProfile];
        return;
    }
    if ([self.userEmailLabel.text isEqualToString:@""] || [self.userPhoneLabel.text isEqualToString:@""]) {
        //        [self showErrorWithMessage:@"Please complete your profile"];
        [self editProfile];
        return;
    }
    if (isCheckListProfile == NO) {
        [self setProfileChecked];
    } else {
        [self setProfileEmpty];
    }
}

- (void)checkBoxTnC {
    if (isCheckListTnC == NO) {
        isCheckListTnC = YES;
        self.checkBoxTnCView.backgroundColor = [Color orangeBidColor];
        self.checkBoxTnCImage.image = [UIImage imageNamed:@"bid_checkbox_checked"];
    } else {
        isCheckListTnC = NO;
        self.checkBoxTnCView.backgroundColor = [UIColor whiteColor];
        self.checkBoxTnCImage.image = [UIImage imageNamed:@"bid_checkbox_empty"];
    }
}
- (void)setProfileChecked {
    isCheckListProfile = YES;
    self.checkBoxProfileView.backgroundColor = [Color orangeBidColor];
    self.checkBoxProfileImage.image = [UIImage imageNamed:@"bid_checkbox_checked"];
}
- (void)setProfileEmpty{
    isCheckListProfile = YES;
    self.checkBoxProfileView.backgroundColor = [UIColor whiteColor];
    self.checkBoxProfileImage.image = [UIImage imageNamed:@"bid_checkbox_empty"];
}
- (void)confirmBid {
 
    if (isCheckListProfile == NO) {
        [self editProfile];
//        [self showErrorWithMessage:str(@"please_make_sure_to_check_the_boxes")];
        return;
    }
    if (isCheckListTnC == NO) {
        [self showErrorWithMessage:str(@"bid_no_tnc")];
        return;
    }
    
    [self confirmBidRequest];
}

- (void)showErrorWithMessage:(NSString *)message {
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:message];
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    [popup setupOneButton];
    [self.view addSubview:popup];
    __block CustomPopup *popupBlock = popup;
    popup.completionButton1  = ^{[popupBlock dismiss];};
}

#pragma mark network request

- (void)loadProfileData {
//    if (![CheckSession checkLoginStatus]) {
        [self loadGuestData];
        return ;
//    }
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"%@user-profile",API_URL];
    NSArray *headers = @[@{@"key":@"Authorization",@"value":[NSString stringWithFormat:@"Bearer %@",[UserData userToken]]}];
    [[NetworkModule instance] GETMETHOD:url parameters:nil headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
//        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//            if (isSuccess) {
//                NSDictionary* profileData = [HandleNull nullFreeDictionaryWithDictionary:responseObject];
//
//                NSString *phoneNumberString = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"phone"]];
//                phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
//                phoneNumberString = [phoneNumberString
//                                     stringByReplacingOccurrencesOfString:phoneLocationCode withString:[NSString stringWithFormat:@"%@ ",phoneLocationCode]];
//                self.userPhoneLabel.text = phoneNumberString;
//
//
//
//                self.userEmailLabel.text = [profileData valueForKey:@"email"];
//            }
//            else{
//                [self showErrorWithMessage:message];
//            }
//            [activityIndicator stopAnimating];
//        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (error.code == NSURLErrorTimedOut) {
        }
        [activityIndicator stopAnimating];
    }];
}
- (void)loadGuestData {
    [self setProfile];
}
- (void)setProfile {
    haveName = NO ;
    havePhone = NO ;
    if ([UserData checkNameExist]){
        [_userNameLabel setText:[NSString stringWithFormat:@"%@",[UserData userName]]];
        [_userNameLabel setTextColor:UIColor.whiteColor];
        haveName = YES;
    }else{
        [_userNameLabel setText:@"-"];
        [_userNameLabel setTextColor:Color.redOONAColor];
    }
    NSString *phoneBid = [[NSUserDefaults standardUserDefaults]objectForKey:@"phonebid"];
    NSLog(@"phonebid : %@",phoneBid);
//    NSString *emailBid = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailbid"];
//    if (phoneBid == nil)
//        return ;
//    if (emailBid == nil)
//        return ;
    if( phoneBid == nil) {
        phoneBid = @"-";
        [self.userPhoneLabel setTextColor:Color.redOONAColor];
    }else{
        [self.userPhoneLabel setTextColor:UIColor.whiteColor];
        havePhone = YES;
    }
    NSString *phoneNumberString = [NSString stringWithFormat:@"%@",phoneBid];
    phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberString = [phoneNumberString
                         stringByReplacingOccurrencesOfString:phoneLocationCode withString:[NSString stringWithFormat:@"%@ ",phoneLocationCode]];
    phoneBid = phoneNumberString;
    
    if (phoneBid != nil) {
        self.userPhoneLabel.text = phoneBid;
    }
    if (havePhone && haveName){
        [self setProfileChecked];
    }
//    if (emailBid != nil) {
//        self.userEmailLabel.text = emailBid;
//    }
}

- (void)confirmBidRequest {
    if (loading) {
        return;
    }
    loading = YES;
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"%@points/bid/place",API_URL];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSArray *headers = @[@{@"key":@"uuid",@"value":uuidString}];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];

    NSString *phoneToSubmit = [NSString stringWithFormat:@"%@",self.userPhoneLabel.text];
    phoneToSubmit = [phoneToSubmit
                     stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",phoneLocationCode] withString:phoneLocationCode];
   
    [param setObject:[NSString stringWithFormat:@"%@",[self.bidDetailDict valueForKey:@"id"]] forKey:@"itemId"];
    [param setObject:self.tcoinBidNumber forKey:@"points"];
    [param setObject:[NSString stringWithFormat:@"%@",self.userNameLabel.text] forKey:@"name"];
    
    [param setObject:[NSString stringWithFormat:@"%@",phoneToSubmit] forKey:@"phone"];
    NSLog(@"params confirm bid = %@",param);
//    [[NetworkModule instance] POST:url parameters:param headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"confirmBid = \n %@",responseObject);
//        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
//        NSString *status = [responseObject valueForKey:@"status"];
//        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//            if (isSuccess) {
//                NSString *itemID = [NSString stringWithFormat:@"%@",[self.bidDetailDict valueForKey:@"id"]];
//                
//                
//                NSString *TopicToSub = [NSString stringWithFormat:@"bid-item-%@-%@",itemID,[OONAString currentLang]];
//                [[FIRMessaging messaging] subscribeToTopic:TopicToSub
//                                                completion:^(NSError * _Nullable error) {
//                                                    NSLog(@"Subscribed to topic:%@",TopicToSub);
//                                                }];
//                
//                loading = NO;
//                FinishBidViewController *finishBid = [[FinishBidViewController alloc]init];
//                finishBid.bidDetailDict = self.bidDetailDict;
//                finishBid.tcoinBidNumber = self.tcoinBidNumber;
//                [self presentViewController:finishBid animated:NO completion:nil];
//            } else {
//                [self showErrorWithMessage:message];
//            }
//            loading = NO;
//            [activityIndicator stopAnimating];
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        loading = NO;
//        [activityIndicator stopAnimating];
//    }];
}

@end
