//
//  PlaceBidViewController.m
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "PlaceBidViewController.h"
//#import "OONA-Swift.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
@interface PlaceBidViewController ()

@end

@implementation PlaceBidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)initView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(close)
                                                 name:@"finishBid"
                                               object:nil];
    NSString *userPoints =  [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
    maxValue = [userPoints intValue];
//    maxValue = 9999999999999999;
    self.blackBg.layer.cornerRadius = 16;
    self.view.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    self.imageView.layer.cornerRadius = 16;
    self.imageView.layer.masksToBounds = YES;
    self.nameProductBidLabel.text = [NSString stringWithFormat:@"%@ %@",str(@"bid_for"),[self.bidDetailDict valueForKey:@"name"]];
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * maxValueString =  [formatter stringFromNumber:[NSNumber numberWithInt:maxValue]];
    
    
    self.maxPointBidLabel.text = [NSString stringWithFormat:@"%@",maxValueString];
    [self.imageView sd_setImageWithURL:[self.bidDetailDict objectForKey:@"imageUrl"]];
    self.textFieldView.layer.cornerRadius = _textFieldView.frame.size.height/2;
    self.textFieldView.layer.borderWidth = 2;
    self.textFieldView.layer.borderColor = [Color orangeBidColor].CGColor;
    
    self.valueBid = [NSNumber numberWithInt: MIN(maxValue, 100)];
    self.pointBidTextField.text = @"0";
    [self.bidSlider setMinimumValue:0];
    [self.bidSlider setMaximumValue:maxValue];
    [self.bidSlider setThumbTintColor:[Color orangeBidColor]];
    self.bidSlider.value = [self.valueBid floatValue];
    self.placeBidButton.layer.cornerRadius = _placeBidButton.frame.size.height/2;
    self.placeBidButton.layer.borderWidth = 2;
    self.placeBidButton.layer.borderColor = [Color orangeBidColor].CGColor;
    self.pointBidTextField.delegate = self;
    [self.pointBidTextField addTarget:self action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    [self.pointBidTextField setText:[NSString stringWithFormat:@"%@",self.valueBid]];
    [_errorLB setTextColor:Color.darkRedColor];
    [_errorLB setText:@""];
    timeCount = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountdown) userInfo:nil repeats:YES];
    [self.placeBidButton setTitle:str(@"bid_place") forState:UIControlStateNormal];

}

- (void)initButton {
    [self.minButton addTarget:self action:@selector(minPointBid) forControlEvents:UIControlEventTouchUpInside];
    [self.plusButton addTarget:self action:@selector(plusPointBid) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.placeBidButton addTarget:self action:@selector(placeBid) forControlEvents:UIControlEventTouchUpInside];
    [self.bidSlider addTarget:self action:@selector(bidSliderValue:) forControlEvents:UIControlEventValueChanged];
}

- (void)timerCountdown {
    timeCount = timeCount + 1;
    int totalTime = self.timeLeft - timeCount;
    int hours = totalTime / 3600;
    int minutes = (totalTime % 3600) / 60;
    int sec = (totalTime % 3600) % 60;
    if (hours < 24) {
        self.remainingTimeLabel.text = [NSString stringWithFormat:@"%@: %02d:%02d:%02d",str(@"bid_remaining"),hours,minutes,sec];
    } else {
        int days = hours / 24 ;
        int hoursFilter = hours - (24*days) ;
        
        self.remainingTimeLabel.text = [NSString stringWithFormat:@"Remaining time: %d days %d hours",days,hoursFilter];
        self.remainingTimeLabel.text = [NSString stringWithFormat:@"%@: %d %@ %d %@",str(@"bid_remaining"),days,str(@"bid_days"),hoursFilter,str(@"bid_hours")];
    }
    
}

#pragma mark button action

- (void)minPointBid {
    if (self.valueBid == 0 || self.valueBid < 0 || [self.valueBid intValue] - 5 < 0) {
        return;
    }
    int mid = [self.valueBid intValue] - 5 ;
    self.valueBid = [NSNumber numberWithInt:mid];
    [self.bidSlider setValue:[self.valueBid floatValue] animated:YES];
    self.pointBidTextField.text =  [NSString stringWithFormat:@"%@",self.valueBid];
}

- (void)plusPointBid {
    if ([self.valueBid intValue] == maxValue || [self.valueBid intValue] > maxValue || [self.valueBid intValue] + 5 > maxValue) {
        return;
    }
    int mid = [self.valueBid intValue] + 5 ;
//    if (mid < 100){
//        mid = 100
//    }
    self.valueBid = [NSNumber numberWithInt:mid];
    [self.bidSlider setValue:[self.valueBid floatValue] animated:YES];
    self.pointBidTextField.text =  [NSString stringWithFormat:@"%@",self.valueBid];
}

- (void)close {
    NSLog(@"place bid close ");
    NSString *placeBidPopBack = [[NSUserDefaults standardUserDefaults]objectForKey:@"placebidpopback"];
    if ([placeBidPopBack isEqualToString:@"0"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"placebidpopback"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)closeNoAnimation {
    NSLog(@"place bid close no animation");
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)placeBid {
    [_errorLB setText:@""];
    if (self.valueBid == 0 || [self.pointBidTextField.text isEqualToString:@"0"] || [self.pointBidTextField.text isEqualToString:@""]) {
        [self showErrorWithMessage:str(@"bid_nominal")];
        return;
    }
    if ([self.valueBid intValue] > maxValue) {
        [self showErrorWithMessage:str(@"bid_edit_data")];
        return;
    }
    if ([self.valueBid intValue] < 100) {
        [self showErrorWithMessage:str(@"bid_under_100")];
        return;
    }
    ConfirmBidViewController *confirmBid = [[ConfirmBidViewController alloc]init];
    confirmBid.bidDetailDict = self.bidDetailDict;
    confirmBid.tcoinBidNumber = self.valueBid;
    [self presentViewController:confirmBid animated:NO completion:nil];
}

- (void)bidSliderValue:(UISlider *)sender {
    NSLog(@"slider value = %f", sender.value);
    [_errorLB setText:@""];
    int value = (int)(sender.value);
    self.valueBid = [NSNumber numberWithInt:value];
    self.pointBidTextField.text =  [NSString stringWithFormat:@"%@",self.valueBid];
}

- (void)showErrorWithMessage:(NSString *)message {
    [self.errorLB setText:message];
    [self.errorLB sizeToFit];
    self.errorLB.numberOfLines = 0;
}

- (void)popupSignIn {
    
    //    LandingPageViewController *viewController = [[LandingPageViewController alloc]initWithPopUpMode];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LandingPageViewController" bundle:nil];
//    LandingPageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LandingPageViewController"];
//    [viewController setIsPopupMode:YES];
//    LandscapeViewController *navVC = [[LandscapeViewController alloc]initWithRootViewController:viewController];
//    [self presentViewController:navVC animated:YES completion:^{
//        [viewController setIsPopupMode:YES];
//    }];
//    
}

#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField*)textField {
    [_errorLB setText:@""];
    self.valueBid = [NSNumber numberWithInt:[self.pointBidTextField.text intValue]];
}

@end
