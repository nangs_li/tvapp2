//
//  ConfirmBidViewController.h
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "OONA-Swift.h"
#import "CustomPopup.h"
#import "EditProfileBidViewController.h"
#import "FinishBidViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomPopup.h"
#import "UserData.h"
#import "NetworkModule.h"
#import "HandleNull.h"
#import "PopupWebViewController.h"

@interface ConfirmBidViewController : UIViewController {
    BOOL isCheckListProfile;
    BOOL isCheckListTnC;
    BOOL loading;
}

@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;
@property (weak, nonatomic) IBOutlet UILabel *tncLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *userEmailLabel;
@property (weak, nonatomic) IBOutlet UIView *checkBoxProfileView;
@property (weak, nonatomic) IBOutlet UIView *checkBoxTnCView;
@property (weak, nonatomic) IBOutlet UIView *editProfileView;
@property (weak, nonatomic) IBOutlet UIView *blackBG;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *checkBoxProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxTnCImage;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxTnCButton;
@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *tncButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmBidButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (strong, nonatomic) NSDictionary *bidDetailDict;
@property (strong, nonatomic) NSNumber *tcoinBidNumber;

@end
