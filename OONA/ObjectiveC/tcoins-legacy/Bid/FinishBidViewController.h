//
//  FinishBidViewController.h
//  OONA
//
//  Created by OONA on 15/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LandscapeViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserData.h"
//#import "OONA-Swift.h"
//#import "OONAString.h"


@interface FinishBidViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *youBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameProductLabel;
@property (weak, nonatomic) IBOutlet UILabel *tcoinBidLabel;

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *finishBidButton;
@property (weak, nonatomic) IBOutlet UIView *blackBG;

@property (strong, nonatomic) NSDictionary *bidDetailDict;
@property (strong, nonatomic) NSNumber *tcoinBidNumber;

@end
