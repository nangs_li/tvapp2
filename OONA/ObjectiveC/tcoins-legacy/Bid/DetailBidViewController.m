//
//  DetailBidViewController.m
//  OONA
//
//  Created by OONA on 14/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "DetailBidViewController.h"
//#import "CheckSession.h"
//#import "LandscapeViewController.h"
//#import "LandingPageViewController.h"
#import "OONA-Swift.h"
#import "CommonHeader.h"
//#import "Color.h"
//#import "OONAString.h"
@interface DetailBidViewController ()

@end

@implementation DetailBidViewController {
    PlaceBidViewController *placeBid;
}

- (void)viewDidLoad {
    self.viewName = @"points_bid_item_details";
    [super viewDidLoad];
    [self initView];
//    [self initScrollView];
    [self getDetailBid];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(close)
                                                 name:@"finishBid"
                                               object:nil];
    [_bidTitleLabel setText:str(@"bid_title")];
    NSLog(@"bid detail init view : %@",self.bidDetailDict );
    [self.tncButton addTarget:self action:@selector(tncBid) forControlEvents:UIControlEventTouchUpInside];
    [self.detailsButton addTarget:self action:@selector(detailsBid) forControlEvents:UIControlEventTouchUpInside];
    [self.bidButton addTarget:self action:@selector(bidNow) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightHeaderLabel.text = [NSString stringWithFormat:@"%@",[UserData userPointsString]];
    self.couponDetailsSegmentedView.couponDetailsTextVIew.text = [self.itemBidDetailDict valueForKey:@"description"];
    self.couponTermsConditions.couponTermsConditionsTextView.text = [self.itemBidDetailDict valueForKey:@"terms"];
    self.nameLabel.text = [self.bidDetailDict valueForKey:@"name"];
    [self.imageView sd_setImageWithURL:[self.bidDetailDict objectForKey:@"imageUrl"]];
    timeCount = 0;
    [_separateLine setBackgroundColor:HexColor(0xF7F7F7)];
//    self.bidDetailsView.layer.borderWidth = 1.f ;
//    self.bidDetailsView.layer.borderColor = UIColor.lightGrayColor.CGColor;
//    CALayer *layer = [CALayer layer];
//    layer.borderColor = UIColor.lightGrayColor.CGColor;
//    layer.borderWidth = 1;
//
//    [layer setFrame:CGRectMake(self.imageView.frame.size.width-1, 0, 1, self.imageView.frame.size.height)];
//    [self.imageView.layer addSublayer:layer];
//    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountdown) userInfo:nil repeats:YES];
    
    if (self.pointBid != nil) {
        self.tcoinsView.layer.cornerRadius = 16;
        self.tcoinsView.layer.borderWidth = 1;
        self.tcoinsView.layer.borderColor = [Color orangeBidColor].CGColor;
        self.bidImage.hidden = YES;
        self.bidButton.hidden = YES;
        self.tcoinsLabel.hidden = NO;
        self.tcoinsView.hidden = NO;
        self.tcoinsImageView.hidden = NO;
        self.tcoinsLabel.text = self.pointBid;
    }
    
    [self.bidButton setTitle:str(@"bid_now") forState:UIControlStateNormal];
    self.bidButton.layer.cornerRadius = 15.f ;
    self.bidButton.layer.borderWidth = 2.f ;
    self.bidButton.layer.masksToBounds = YES ;
    if ([@"1" isEqualToString:[NSString stringWithFormat:@"%@",[self.bidDetailDict objectForKey:@"isSuperbid"]]]) {
        [self.bidButton setBackgroundImage:[UIImage imageNamed:@"bid-button-purple.png"] forState:UIControlStateNormal];
            self.bidButton.layer.borderColor = UIColor.darkGrayColor.CGColor ;
        
    }else{
          [self.bidButton setBackgroundImage:[UIImage imageNamed:@"bid-button-orange.png"] forState:UIControlStateNormal];
        self.bidButton.layer.borderColor = UIColor.clearColor.CGColor;
    }
   
    if ([self.bidDetailDict valueForKey:@"opensAt"] != nil) {
        self.bidButton.hidden = YES;
        self.bidImage.hidden = YES;
        
        NSDateFormatter* openDateFormat = [[NSDateFormatter alloc] init];
        [openDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate* date = [openDateFormat dateFromString:[self.bidDetailDict valueForKey:@"opensAt"]];
        NSDateFormatter* openDateFormatAfter = [[NSDateFormatter alloc] init];
        [openDateFormatAfter setDateFormat:@"MMM dd, hh:mm a"];
        NSString *dateString = [openDateFormatAfter stringFromDate:date];
        self.opensAtLabel.text = [NSString stringWithFormat:@"%@ %@",str(@"bid_open"),dateString];
        [self.timeLabel setHidden:YES];
//        self.timeLabel.text = [NSString stringWithFormat:@"%@ %@",str(@"bid_open"),[self.bidDetailDict valueForKey:@"opensAt"]];
    }else{
//        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountdown) userInfo:nil repeats:YES];
        [self timerCountdown];
    }
}

- (void)initScrollView {
    NSLog(@"tabs : %@",_contentArray);
    UIButton *btn ;
    
    self.buttonArray = NSMutableArray.array;
    
    CGFloat btnWidth  = _bidDetailsView.frame.size.width / (MAX (_contentArray.count, 1));
    CGFloat btnHeight = 40 ;
   
    self.detailView.layer.cornerRadius = 14;
    self.detailView.layer.masksToBounds = YES;
    
//    [self.detailsButton setTitle:str(@"details") forState:UIControlStateNormal];
//    [self.tncButton setTitle:str(@"terms_and_condition") forState:UIControlStateNormal];
    
//    self.detailsButton.titleLabel.minimumScaleFactor = 0.5f;
//    self.detailsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
//
//    self.tncButton.titleLabel.minimumScaleFactor = 0.5f;
//    self.tncButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.tncButton.hidden = YES;
    self.detailsButton.hidden = YES;
    
    if (_contentArray.count == 1){
        btnHeight = 0;
    }
    for (int i = 0 ; i < _contentArray.count ; i++ ){
        
        btn = [[UIButton alloc] init];
        [btn setFrame:CGRectMake(btnWidth* i, 0, btnWidth, btnHeight)];
        
        [btn setTitle:[NSString stringWithFormat:@"%@",[_contentArray[i]objectForKey:@"label"] ] forState:UIControlStateNormal];

        [btn sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_contentArray[i]objectForKey:@"iconUrl"]]] forState:UIControlStateNormal completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {

            
        }];
        [btn setTitleColor:UIColor.darkGrayColor forState:UIControlStateNormal];
        
        CGFloat padding = 10 ;
        if ([_contentArray[i]objectForKey:@"label"]  == nil) {
            padding = 0;
            [btn setTitle:@"" forState:UIControlStateNormal];
        }
        btn.contentEdgeInsets = UIEdgeInsetsMake(7, 0, 7, padding);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 7);
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [btn.titleLabel setFont:[UIFont fontWithName:OONAFontRegular size:12]];
        btn.titleLabel.adjustsFontSizeToFitWidth = NO;
        btn.titleLabel.numberOfLines = 2 ;
        btn.titleLabel.minimumScaleFactor = 0.5f;
        btn.tag = i ;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        // the space between the image and text
        // the space between the image and text
       
        
        
        [btn addTarget:self action:@selector(clickTab:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonArray addObject:btn];
        
        [self.bidDetailsView addSubview:btn];
        
    }
    
    
    
    float viewWidth = self.bidDetailsView.frame.size.width;
    NSLog(@"frame viewWidth = %f",viewWidth);
    float viewHeight = self.bidDetailsView.frame.size.height-btnHeight;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, btnHeight, viewWidth,viewHeight)];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(viewWidth *_contentArray.count, self.scrollView.frame.size.height);
    self.scrollView.delegate = self;
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, viewWidth, self.scrollView.frame.size.height) animated:NO];
    [self.bidDetailsView addSubview:self.scrollView];
    
    self.selectedIndicator = [[UIView alloc] init];
    self.selectedIndicator.backgroundColor = [Color redOONAColor];
    [self.bidDetailsView addSubview:self.selectedIndicator];
//    [self setSelectedButtonAtIndex:0];
//    [self renderDescription];
//    [self renderTnC];
//    [self detailsBid];
    NSLog(@"frame scrollView = %@",NSStringFromCGSize(self.scrollView.contentSize));
    [self renderTabs];
    [self setSelectedButtonAtIndex:0];
}
- (void)clickTab:(UIButton *)sender {
    [self setSelectedButtonAtIndex:sender.tag];
}
- (void)timerCountdown {
    timeCount = timeCount + 1;
    int totalTime = self.timeLeft - timeCount;
    int hours = totalTime / 3600;
    int minutes = (totalTime % 3600) / 60;
    int sec = (totalTime % 3600) % 60;
    NSString *text = [NSString stringWithFormat:@"%@ %02d:%02d:%02d",str(@"bid_close"),hours,minutes,sec];
    if (hours > 24) {
        int days = hours / 24 ;
        int hoursFilter = hours - (24*days) ;
        
        text = [NSString stringWithFormat:@"%@ %d %@ %d %@",str(@"bid_close"),days,str(@"bid_days"),hoursFilter,str(@"bid_hours")];
    }
    
    self.timeLabel.text = text;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self timerCountdown];
    });
    
    
}
NSInteger currentPage ;
- (void)setSelectedButtonAtIndex:(NSInteger)index{
    if (currentPage == index){
        return;
    }
    currentPage = index;
    NSLog(@"scroll view content width  %f",self.scrollView.contentSize.width);
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width*index/_buttonArray.count, 0) animated:YES];
    UIButton *selectedButton = [_buttonArray objectAtIndex:index];
    [selectedButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    float indicatorHeight = 3.0;
    CGRect selectedButtonFrame = selectedButton.frame;
    [UIView animateWithDuration:0.2 animations:^{
        _selectedIndicator.frame = CGRectMake(CGRectGetMinX(selectedButtonFrame), CGRectGetMaxY(selectedButtonFrame)-indicatorHeight, selectedButtonFrame.size.width, indicatorHeight);
    }];
    for (UIButton *button in _buttonArray) {
        if (button != selectedButton) {
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
}

- (void)close {
    [placeBid dismissViewControllerAnimated:YES completion:^{
        [self back];
    }];
    
}
#pragma mark button action

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)detailsBid {
    [self setSelectedButtonAtIndex:0];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)tncBid {
    [self setSelectedButtonAtIndex:1];
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width/2, 0) animated:YES];
}

- (void)bidNow {
//    if (![CheckSession checkLoginStatus]) {
//        self.signInAlertView = [[SignInAlertView alloc]init];
//        [SignInAlertView add:self.signInAlertView withController:self
//                 withDismiss:^{
//
//                 } withAction:^{
//                     [self popupSignIn];
//                 }];
//        return;
//    }

    placeBid = [[PlaceBidViewController alloc]init];
    placeBid.bidDetailDict = self.bidDetailDict;
    placeBid.timeLeft = self.timeLeft - timeCount;
    placeBid.providesPresentationContextTransitionStyle = YES;
    placeBid.definesPresentationContext = YES;
    [placeBid setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:placeBid animated:YES completion:nil];
}

- (void)popupSignIn {
    
    //    LandingPageViewController *viewController = [[LandingPageViewController alloc]initWithPopUpMode];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LandingPageViewController" bundle:nil];
//    LandingPageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LandingPageViewController"];
//    [viewController setIsPopupMode:YES];
//    LandscapeViewController *navVC = [[LandscapeViewController alloc]initWithRootViewController:viewController];
//    [self presentViewController:navVC animated:YES completion:^{
//        [viewController setIsPopupMode:YES];
//    }];
    
}

#pragma mark network request

- (void)getDetailBid {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = self.view.frame;
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSNumber *itemId = [NSNumber numberWithInt:[[self.bidDetailDict valueForKey:@"id"] intValue]];
    [[NetworkModule instance]getDetailBid:[itemId stringValue] success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"getDetailBid %@",responseObject);
        [activityIndicator stopAnimating];
        self.descriptionArray = [responseObject objectForKey:@"description"];
        self.tncArray = [responseObject objectForKey:@"terms"];
        self.contentArray = [responseObject objectForKey:@"tabs"];
//        [self renderDescription];
//        [self renderTnC];
        [self initScrollView];

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [activityIndicator stopAnimating];
        NSLog(@"getDetailBid error /n = %@",error);
    }];
}

#pragma mark nib loaded

- (CouponDetails *)couponDetailsSegmentedView {
    if (!_couponDetailsSegmentedView) {
        _couponDetailsSegmentedView = [[NSBundle mainBundle] loadNibNamed:@"CouponDetails" owner:self options:nil].firstObject;
    }
    return _couponDetailsSegmentedView;
}

- (CouponTermsConditions *)couponTermsConditions {
    if (!_couponTermsConditions) {
        _couponTermsConditions = [[NSBundle mainBundle] loadNibNamed:@"CouponTermsConditions" owner:self options:nil].firstObject;
    }
    return _couponTermsConditions;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x / pageWidth)+0.1;
    NSLog(@"end accel %f",scrollView.contentOffset.x / pageWidth);
    [self setSelectedButtonAtIndex:page];
}
- (void)renderTabs{
    UIView *newView ;
    UIButton *tapBtn ;
    CGFloat contentHeight = 0 ;
    for (int j = 0; j < _contentArray.count ; j ++){
        NSArray * arrayToRender  = [[_contentArray objectAtIndex:j] objectForKey:@"contents"];
        contentHeight = 0;
        for (int i = 0; i < arrayToRender.count ; i ++){
            NSLog(@"ren tab arr %d %d %@",j,i,arrayToRender[i]);
            if ([[arrayToRender[i] objectForKey:@"type"]isEqualToString:@"label"]){
                newView = [[[OONAViewGenerator alloc]init] renderViewWithScheme:arrayToRender[i] onView:self.bidDetailsView];
                CGFloat newViewHeight = newView.frame.size.height ;
                
                [newView setFrame:CGRectMake(newView.frame.origin.x + (self.scrollView.contentSize.width/_contentArray.count * j), contentHeight+newView.frame.origin.y, self.scrollView.contentSize.width/_contentArray.count, newViewHeight)];
                contentHeight += CGRectGetMaxY(newView.frame) ;
                [self.scrollView addSubview:newView];
                
                if ([arrayToRender[i] objectForKey:@"url"] != nil ){
                    // has url.v
                    NSLog(@"has url");
                    
                    
                    tapBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height)];
                    [tapBtn addTarget:self action:@selector(popupWebView:) forControlEvents:UIControlEventTouchUpInside];
                    [tapBtn.layer setValue:[arrayToRender[i] objectForKey:@"url"] forKey:@"url"];
                    [tapBtn.layer setValue:[arrayToRender[i] objectForKey:@"text"] forKey:@"text"];
                    [tapBtn setTag:i];
                    //                [tapBtn setBackgroundColor:Color.redOONAColor];
                    [newView addSubview:tapBtn];
                    
                    //                [newView addGestureRecognizer:tap];
                    
                }
            }
        }
    }
    
    CGRect contentRect = CGRectZero;
    
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;//    [self detailsBid];
}

- (void)renderDescription {
    UIView *newView ;
    CGFloat contentHeight = 0 ;
    for (int i = 0; i < _descriptionArray.count ; i ++){
        NSLog(@"%@",_descriptionArray[i]);
        if ([[_descriptionArray[i] objectForKey:@"type"]isEqualToString:@"label"]){
            newView = [[[OONAViewGenerator alloc]init] renderViewWithScheme:_descriptionArray[i] onView:self.bidDetailsView];
            CGFloat newViewHeight = newView.frame.size.height ;
            [newView setFrame:CGRectMake(newView.frame.origin.x, contentHeight+newView.frame.origin.y, self.scrollView.contentSize.width/2, newViewHeight)];
            contentHeight += CGRectGetMaxY(newView.frame) ;
            [self.scrollView addSubview:newView];
            
            
        }
    }
    [self detailsBid];
}

- (void)renderTnC {
    UIView *newView ;
    CGFloat contentHeight = 0;
    
    UIButton *tapBtn ;
    for (int i = 0; i < _tncArray.count ; i ++){
        NSLog(@"tnc %@",_tncArray[i]);
        if ([[_tncArray[i] objectForKey:@"type"]isEqualToString:@"label"]){
            newView = [[[OONAViewGenerator alloc]init] renderViewWithScheme:_tncArray[i] onView:self.bidDetailsView];
            
            CGFloat newViewHeight = newView.frame.size.height ;
            [newView setFrame:CGRectMake(self.scrollView.contentSize.width/2, contentHeight+newView.frame.origin.y, self.scrollView.contentSize.width/2, newViewHeight)];
            contentHeight += CGRectGetMaxY(newView.frame) ;
            [self.scrollView addSubview:newView];
            
            
            if ([_tncArray[i] objectForKey:@"url"] != nil ){
                // has url.v
                NSLog(@"has url");
                
                
                tapBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, newView.frame.size.width, newView.frame.size.height)];
                [tapBtn addTarget:self action:@selector(popupWebView:) forControlEvents:UIControlEventTouchUpInside];
                [tapBtn setTag:i];
//                [tapBtn setBackgroundColor:Color.redOONAColor];
                [newView addSubview:tapBtn];
                
//                [newView addGestureRecognizer:tap];
                
            }
        }
        
        
    }
}
- (void)popupWebView:(UIButton *)urlBtn {
    NSLog(@"gg");
    NSString *url  = [urlBtn.layer valueForKey:@"url"];
    NSString *title  = [urlBtn.layer valueForKey:@"text"];
    PopupWebViewController *vc = [[PopupWebViewController alloc] initWithUrl:url];
    vc.title = title;
//    [self.navigationController pushViewController:webViewController animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
