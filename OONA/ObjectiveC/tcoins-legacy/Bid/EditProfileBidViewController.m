//
//  EditProfileBidViewController.m
//  OONA
//
//  Created by OONA on 15/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "CommonHeader.h"
#import "EditProfileBidViewController.h"
//#import "Validation.h"
#import "UserData.h"
#import "OONA-Swift.h"
@interface EditProfileBidViewController ()

@end

@implementation EditProfileBidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    BOOL haveName = NO;
    BOOL havePhone = NO;
    
    self.blackBG.layer.cornerRadius = 16;
    _phoneErrorLb.hidden = YES;
    _nameErrorlb.hidden = YES;
    _phoneErrorLb.text = @"";
    _nameErrorlb.text = @"";
    _nameErrorlb.textContainerInset = UIEdgeInsetsMake(7, 7, 7, 7);
    _phoneErrorLb.textContainerInset = UIEdgeInsetsMake(7, 7, 7, 7);
    _nameErrorlb.layer.cornerRadius = _nameErrorlb.frame.size.height/2;
    _phoneErrorLb.layer.cornerRadius = _phoneErrorLb.frame.size.height/2;
    if (@available(iOS 11.0, *)) {
        
        _phoneErrorLb.layer.maskedCorners = kCALayerMaxXMaxYCorner  |kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner;
        _nameErrorlb.layer.maskedCorners = kCALayerMaxXMaxYCorner  |kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner;
        
    } else { // Fallback on earlier versions }
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, _nameErrorlb.frame.size.width, _nameErrorlb.frame.size.height) byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){_nameErrorlb.frame.size.height, _nameErrorlb.frame.size.height}].CGPath;
        
        _nameErrorlb.layer.mask = maskLayer;
        _nameErrorlb.layer.masksToBounds = YES;
        
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, _phoneErrorLb.frame.size.width, _phoneErrorLb.frame.size.height) byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){_phoneErrorLb.frame.size.height, _phoneErrorLb.frame.size.height}].CGPath;
        
        _phoneErrorLb.layer.mask = maskLayer;
        _phoneErrorLb.layer.masksToBounds = YES;
    }
    
    if ([UserData checkNameExist]){
        _name = [UserData userName];
        haveName = YES;
    }else{
        _name = @"";
    }
    if (self.phone.length > 0) {
        havePhone = YES;
    }
    [_nameLabel setText:str(@"your_name")];
    
    self.emailTextField.text = _name;
    self.phoneNumberTextField.text = [NSString stringWithFormat:@"%@",self.phone];
    NSString *phoneNumberString = [NSString stringWithFormat:@"%@",self.phone];
    
    phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberString = [phoneNumberString
                         stringByReplacingOccurrencesOfString:phoneLocationCode withString:@""];
    self.phoneNumberTextField.text = phoneNumberString;

    [self.emailTextField addTarget:self action:@selector(didTextFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.phoneNumberTextField addTarget:self action:@selector(didTextFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    if( phoneNumberString == nil) {
        self.phoneNumberTextField.text = @"";
    }
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _phoneNumberTextField.frame.size.width*0.4, _phoneNumberTextField.frame.size.height)];
    
    float flagHeight = paddingView4.frame.size.height/2;
    UIImageView *flagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (paddingView4.frame.size.height/2)-(flagHeight/2), flagHeight*3/2, flagHeight)];
    flagImageView.backgroundColor = [UIColor whiteColor];
    UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, flagImageView.frame.size.width, flagHeight/2)];
    redView.backgroundColor = [UIColor redColor];
    [flagImageView addSubview:redView];
    
    UILabel *codeNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(flagImageView.frame)+5, 0, paddingView4.frame.size.width-CGRectGetMaxX(flagImageView.frame)-10, paddingView4.frame.size.height)];
    codeNumber.font = [UIFont fontWithName:@"SourceSansPro-Light" size:14];
    codeNumber.textColor = [UIColor whiteColor];
    codeNumber.text = phoneLocationCode;
    codeNumber.adjustsFontSizeToFitWidth = YES;
    codeNumber.minimumScaleFactor = 0.5;
    
    [paddingView4 addSubview:flagImageView];
    [paddingView4 addSubview:codeNumber];
    _phoneNumberTextField.leftView = paddingView4;
    _phoneNumberTextField.leftViewMode = UITextFieldViewModeAlways;
    
    _emailBG.layer.cornerRadius = _emailBG.frame.size.height/2;
//    self.emailTextField.layer.masksToBounds = YES;
    _phoneBG.layer.cornerRadius = _phoneBG.frame.size.height/2;
//    self.phoneNumberTextField.layer.masksToBounds = YES;
    [_phoneLB setText:str(@"phone_number")];
    self.saveButton.layer.cornerRadius = _saveButton.frame.size.height/2;
    self.saveButton.layer.borderWidth = 2;
    self.saveButton.layer.borderColor = [Color orangeBidColor].CGColor;
    [self.editYourDataLabel setText:str(@"bid_edit_data")];
    [self.saveButton setTitle:str(@"bid_save") forState:UIControlStateNormal];
    
    if (haveName && havePhone){
        [self.pageLabel setText:str(@"bid_edit_contact")];
//        [self.pageLabel setText:str(@"")];
    }else{
        [self.pageLabel setText:str(@"bid_complete_contact")];
    }
//    "bid_complete_contact"="Complete your contact details";
//    "bid_edit_contact"="Your contact details";
}
- (void)didTextFieldValueChanged : (UITextField *)textView{
    if (textView == self.emailTextField){
        _nameErrorlb.hidden = YES;
    }
    if (textView == self.phoneNumberTextField){
        _phoneErrorLb.hidden = YES;
    }
}
- (void)initButton {
    [self.closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.saveButton addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark button action

- (void)close {
    [self dismissViewControllerAnimated:NO completion:nil];
}
NSString *tempPhone ;
- (void)save {
    BOOL nameValid = NO;
    BOOL phoneValid = NO;
    if (self.phoneNumberTextField.text.length > 0) {
        tempPhone = self.phoneNumberTextField.text;
        if ([tempPhone hasPrefix:@"0"]){
            tempPhone = [tempPhone substringFromIndex:1];
            // trim 0.
            
        }
//        if ([Validation validateWithPhone:tempPhone]) {
//
//            phoneValid = YES ;
////            [self storeAndExit];
////            [Validation validateWithEmail:self.emailTextField.text success:^(BOOL isValid) {
////                if (isValid){
////                    // correct email.
////                    [self storeAndExit];
////                }else{
////                    [self showErrorWithMessage:str(@"whoops_your_email_seem_to_be_wrong_")];
////                }
////            }];
//
//        }

    }
    
    if (self.emailTextField.text.length > 0) {
        _name = self.emailTextField.text;
        nameValid = YES;
    }
    
    if (!phoneValid){
        [self showPhoneError:str(@"please_enter_correct_phone_number")];
    }
    if (!nameValid){
        [self showNameError:@"Please input your name."];
    }
    if (nameValid && phoneValid){
        [self finishChecking];
    }
  
}
- (void)finishChecking {
    if (![UserData checkNameExist]){
        [self submitName:_name];
    }else{
        [self storeAndExit];
    }
    
}
- (void)submitName:(NSString *)name {
    NSDictionary *param = @{@"topic":@"registration",
                            @"name" : name};
    
    
    NSLog(@"submit ans %@",param);
//    [[NetworkModule instance] POST:[NSString stringWithFormat:@"%@chatbot/user-info",API_URL] parameters:param headers:@[] success:^(NSURLSessionDataTask *task, id responseObject) {
////        [self loadingViewStop];
//        NSLog(@"main chatbot response %@",responseObject);
//        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
//        NSString *status = [responseObject valueForKey:@"status"];
//        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
//                        if (isSuccess) {
//                            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"chatbot_user_name"];
//                             [self storeAndExit];
////                            [self createGoToMainChat];
////                            [self nextProgress];
//                        } else {
//                            [self showErrorWithMessage:message];
//                        }
//        }];
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"main chatbot error %@",error.localizedDescription);
////        [self loadingViewStop];
//        [self showErrorWithMessage:[error localizedDescription]];
//    }];
}
- (void)storeAndExit {
    NSString *phone = [NSString stringWithFormat:@"%@%@",phoneLocationCode,tempPhone];
    NSString *name = [NSString stringWithFormat:@"%@",_emailTextField.text];
    [[NSUserDefaults standardUserDefaults]setObject:phone forKey:@"phonebid"];
    [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"namebid"];
//    [[NSUserDefaults standardUserDefaults]setObject:self.emailTextField.text forKey:@"emailbid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)showNameError:(NSString *)msg {
    _nameErrorlb.hidden = NO ;
    _nameErrorlb.text = msg ;
}
- (void)showPhoneError:(NSString *)msg {
    _phoneErrorLb.hidden = NO ;
    _phoneErrorLb.text = msg ;
}
- (void)showErrorWithMessage:(NSString *)message {
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:message];
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    [popup.button1 setTitle:@"OK" forState:UIControlStateNormal];
    [popup setupOneButton];
    [self.view addSubview:popup];
    __block CustomPopup *popupBlock = popup;
    popup.completionButton1  = ^{[popupBlock dismiss];};
}

@end
