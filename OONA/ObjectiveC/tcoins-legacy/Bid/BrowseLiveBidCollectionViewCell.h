//
//  BrowseLiveBidCollectionViewCell.h
//  OONA
//
//  Created by OONA on 13/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseLiveBidCollectionViewCell : UICollectionViewCell

@property (weak,nonatomic) IBOutlet UIImageView *imageView;

@end
