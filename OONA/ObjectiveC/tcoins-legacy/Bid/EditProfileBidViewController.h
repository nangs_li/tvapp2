//
//  EditProfileBidViewController.h
//  OONA
//
//  Created by OONA on 15/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "OONA-Swift.h"
#import "CustomPopup.h"
#import "NetworkModule.h"

@interface EditProfileBidViewController : UIViewController 

@property (weak, nonatomic) IBOutlet UILabel *editYourDataLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIView *phoneBG;
@property (weak, nonatomic) IBOutlet UIView *emailBG;
@property (weak, nonatomic) IBOutlet UIView *blackBG;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pageLabel;
@property (weak, nonatomic) IBOutlet UILabel *pageSubLabel;
@property (weak, nonatomic) IBOutlet UITextView *phoneErrorLb;
@property (weak, nonatomic) IBOutlet UITextView *nameErrorlb;

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *name;

@end
