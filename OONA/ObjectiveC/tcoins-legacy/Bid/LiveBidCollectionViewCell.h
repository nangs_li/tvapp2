//
//  LiveBidCollectionViewCell.h
//  OONA
//
//  Created by OONA on 13/08/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveBidCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *tcoinsLabel;
@property (weak, nonatomic) IBOutlet UIView *tcoinsView;
@property (weak, nonatomic) IBOutlet UIImageView *tcoinsImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *bidButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeOpensLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBidButtonConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *overlayImg;
@property (weak, nonatomic) IBOutlet UIButton *superBidButton;

- (void)initGradient;
- (void)superBidBtn;
- (void)normalBidBtn;
@end
