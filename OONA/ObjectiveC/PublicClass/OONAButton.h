//
//  OONAButton.h
//  OONA
//
//  Created by jack on 21/8/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//

@import UIKit;
#import <SDWebImage/UIButton+WebCache.h>
@interface OONAButton : UIButton
@property (strong, nonatomic)NSString *buttonIdentifier;
@end
