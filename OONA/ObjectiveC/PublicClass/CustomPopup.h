//
//  CustomPopup.h
//  OONA
//
//  Created by OONA iOS on 11/21/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPopup : UIView
- (id)initWithMessage:(NSString *)message;
- (id)initPhoneDialogWithMessage:(NSString *)message;
- (id)initWithTextFieldTitle:(NSString *)titleText AndMessage:(NSString *)message;
@property (nonatomic) CGRect *viewFrame;
@property (nonatomic) UIButton *button1;
@property (nonatomic) UIButton *button2;
@property (nonatomic) UIButton *button3;
@property (nonatomic) UIButton *button4;
@property (nonatomic) UIButton *backBtn;
@property (nonatomic) UIButton *editPhoneBtn;
@property (nonatomic) UIView *popupView;
@property (nonatomic) UITextView *info;
@property (nonatomic) UITextView *phoneInfo;
@property (nonatomic) UITextView *info2;
@property (nonatomic) UITextView *msg;
@property (nonatomic) UITextField *inputField;
@property (nonatomic) UIImageView *icon ;
@property (nonatomic) void (^dismissCompletion)(void);
@property (nonatomic) void (^editAction)(void);
@property (nonatomic) void (^completionButton1)(void);
@property (nonatomic) void (^completionButton2)(void);
@property (nonatomic) void (^completionButton3)(void);
@property (nonatomic) void (^completionButton4)(void);
- (void)setupOneButton;
- (void)setupTwoButton;
- (void)dismiss;
- (void)setupZeroButton;
@end
