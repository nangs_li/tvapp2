//
//  HoobexViewController.m
//  OONA
//
//  Created by jack on 13/10/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import "PopupImageViewController.h"

@implementation PopupImageViewController
- (instancetype)initWithImage:(UIImage *)image{
    self = [super init];
    
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    _image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    [_image setImage:image];
    UIImage *img = image ;
    CGFloat ratio =  img.size.height/img.size.width ;
    _image.frame = CGRectMake(_image.frame.origin.x, _image.frame.origin.y,
                                 _image.frame.size.width, _image.frame.size.width * ratio);
    [_image setContentMode:UIViewContentModeScaleAspectFill];
    
    [scroll addSubview:_image];
    [scroll setContentSize:CGSizeMake(_image.frame.size.width, _image.frame.size.width * ratio)];
    
    [self.view addSubview:scroll];
    
    [self initTabBar];
    return self;
}
- (void)initTabBar{
    UIView *tab = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-90, 22, 50, 50)];
    backBtn.clipsToBounds = YES;
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 15, 0);
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft ;
    backBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter ;
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit ;
    [backBtn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [tab addSubview:backBtn];
    [self.view addSubview:tab];
    
}
- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
