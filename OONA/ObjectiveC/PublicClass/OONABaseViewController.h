//
//  OONABaseViewController.h
//  OONA
//
//  Created by jack on 12/10/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//

@import UIKit;


@interface OONABaseViewController : UIViewController
@property (nonatomic, strong) NSString *viewName;

@end
