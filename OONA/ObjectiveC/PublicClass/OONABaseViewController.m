//
//  OONABaseViewController.m
//  OONA
//
//  Created by jack on 12/10/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "OONABaseViewController.h"
#import "OONAFirBase.h"
//#import "CommonHeader.h"
#import "NetworkModule.h"

@implementation OONABaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self viewEnterFirebase];
    NSLog(@"OONA VC named: [%@] appear",_viewName);
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    NSLog(@"OONA VC did disappear");
    NSLog(@"OONA VC named: [%@] did disappear",_viewName);
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self viewLeaveFirebase];
    NSLog(@"OONA VC named: [%@] will disappear",_viewName);
}
- (NSString *)getFirebaseViewIndicator {
    
    NSString * indicator = [NSString stringWithFormat:@"ui_%@",_viewName];
    
    if ([_viewName hasPrefix:@"ui_"]){
        return _viewName;
    }
    
    return indicator;
}
- (void)viewEnterFirebase {
//    [OONAFirBase logEventWithName:[self getFirebaseViewIndicator] parameters:@{@"action" : @"enter",
//                                                                               @"event_code" : [[NetworkModule instance]getTimeStampNow]}];
}
- (void)viewLeaveFirebase {
    
//    [OONAFirBase logEventWithName:[self getFirebaseViewIndicator] parameters:@{@"action" : @"leave",
//                                                                           @"event_code" : [[NetworkModule instance]getTimeStampNow]}];
}
@end
