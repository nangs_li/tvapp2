//
//  PopupWebViewController.m
//  OONA
//
//  Created by jack on 9/11/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "CommonHeader.h"
#import "PopupWebViewController.h"
#import "OONA-Swift.h"
@import WebKit ;
@implementation PopupWebViewController{
    NSURL *WebUrl ;
    WKWebView *webview ;
}

- (instancetype)initWithUrl:(NSString *)url {
    self = [super init];
    WebUrl = [NSURL URLWithString:url];

    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
}
- (void)createView {
    [tab setBackgroundColor:Color.darkGrayOONAColor ];
    CGFloat height = self.view.frame.size.height - tab.frame.size.height ;
    CGFloat width = height * 16 / 9 ;
    webview = [[WKWebView alloc]initWithFrame:CGRectMake((self.view.frame.size.width - width )/2, tab.frame.size.height, width, height)];
    [webview.scrollView setScrollEnabled:NO];
//    WKWebViewConfiguration *webConfig = [[WKWebViewConfiguration alloc]init];
    
    [self.view addSubview:webview];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:WebUrl];
    [webview loadRequest:urlRequest];
}
@end
