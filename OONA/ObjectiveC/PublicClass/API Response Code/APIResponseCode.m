//
//  APIResponseCode.m
//  OONA
//
//  Created by OONA iOS on 11/1/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//
@import Crashlytics;
@import CoreTelephony;
#import "APIResponseCode.h"
#include <sys/sysctl.h>
#include <sys/types.h>
@import UIKit;
@implementation APIResponseCode
+ (void)checkResponseWithCode:(NSString *)code WithStatus:(NSString *)status completion:(void (^)(NSString *message, BOOL isSuccess))completion{
    int codeNumber = [code intValue];
    NSString *textMessage = [NSString stringWithFormat:@"An error occured.(%d)",codeNumber];
    if ([status isEqualToString:@"success"]) {
        BOOL succes = YES;
        switch (codeNumber) {
                // Modul 1 --------------
            case 199999:
                textMessage = @"success";
                break;
            case 1001:
                textMessage = @"Generic success";
                break;
            case 1011:
                textMessage = @"Register device success";
                break;
            case 1018:
                textMessage = @"Register device success";
                break;
            case 1021:
                textMessage = @"invite code accepted.";
                break;
            case 1081:
                textMessage = @"invite code success";
                break;
                // Modul 2 --------------
            case 2011:
                textMessage = @"Registration success";
                break;
            case 2021:
                textMessage = @"Verification success";
                break;
            case 2022:
                textMessage = @"Email for user verification has been sent";
                break;
            case 2031:
                textMessage = @"Login with email success";
                break;
            case 2041:
                textMessage = @"Login with Facebook success";
                break;
            case 2051:
                textMessage = @"Login with Instagram success";
                break;
            case 2061:
                textMessage = @"Login with Google success";
                break;
            case 2071:
                textMessage = @"Login with Indihome success";
                break;
            case 2081:
                textMessage = @"Logout success";
                break;
            case 2091:
                textMessage = @"Email for password instruction has been sent";
                break;
            case 2101:
                textMessage = @"Password changed successfully";
                break;
            case 2111:
                textMessage = @"User profile retrieved";
                break;
            case 2121:
                textMessage = @"Profile updated successfully";
                break;
            case 2131:
                textMessage = @"Login success";
                break;
            case 2141:
                textMessage = @"Refresh token success";
                break;
                // Modul 3 --------------
            case 3011:
                textMessage = @"Channel list retrieved";
                break;
            case 3014:
                textMessage = @"channel_not_available_4";
                break;
            case 3021:
                textMessage = @"Episode list retrieved";
                break;
            case 3031:
                textMessage = @"Streaming info retrieved";
                break;
            case 3041:
                textMessage = @"Display ad info retrieved";
                break;
            case 3051:
                textMessage = @"Playlist retrieved";
                break;
            case 3061:
                textMessage = @"Mood list retrieved";
                break;
            case 3071:
                textMessage = @"Category list retrieved";
                break;
            case 3081:
                textMessage = @"Add to favourite successfully";
                break;
            case 3091:
                textMessage = @"Remove from favourite successfully";
                break;
            case 3101:
                textMessage = @"Add to shuffle successfully";
                break;
            case 3111:
                textMessage = @"Remove from shuffle successfully";
                break;
            case 3121:
                textMessage = @"Add to blocked successfully";
                break;
            case 3131:
                textMessage = @"Remove from blocked successfully";
                break;
                // Modul 4 --------------
            case 4011:
                textMessage = @"Parental control retrieved";
                break;
            case 4021:
                textMessage = @"Parental control update succeeded";
                break;
            case 4031:
                textMessage = @"Parental control passcode matched";
                break;
            case 4041:
                textMessage = @"Parental control passcode update succeeded (Forgot Passcode)";
                break;
            case 4051:
                textMessage = @"Add to allowed successfully";
                break;
            case 4061:
                textMessage = @"Remove from allowed successfully";
                break;
                // Modul 5 --------------
            case 5011:
                textMessage = @"Keyword suggestion retrieved";
                break;
            case 5021:
                textMessage = @"Searched result for channel retrieved";
                break;
                // Modul 6 --------------
            case 6011:
                textMessage = @"Message list retrieved";
                break;
            case 6021:
                textMessage = @"Send message succeeded";
                break;
            case 6031:
                textMessage = @"Session update succeeded";
                break;
                // Modul 7 --------------
            case 7011:
                textMessage = @"Notification list retrieved";
                break;
                // Modul 8 --------------
            case 8011:
                textMessage = @"Send sharing result succeeded";
                break;
                // Modul 9 --------------
            case 9011:
                textMessage = @"Send user info succeeded";
                break;
            case 9021:
                textMessage = @"Options list retrieved";
                break;
                // Modul 10 -------------
            case 10011:
                textMessage = @"Programs list retrieved";
                break;
            case 10021:
                textMessage = @"Program details retrieved";
                break;
                // Modul 11 -------------
            case 11011:
                textMessage = @"User points history retrieved";
                break;
            case 11021:
                textMessage = @"Gift list retrieved";
                break;
            case 11031:
                textMessage = @"Gift details retrieved";
                break;
            case 11041:
                textMessage = @"Gift categories retrieved";
                break;
            case 11051:
                textMessage = @"Gift voucher redeem succeeded";
                break;
            case 11061:
                textMessage = @"Gift voucher list retrieved";
                break;
            case 11071:
                textMessage = @"Gift voucher details retrieved";
                break;
            case 11081:
                textMessage = @"Bonus code verification success";
                break;
                
                // Modul 13 -------------
            case 13011:
                textMessage = @"Gift voucher details retrieved";
                break;
            case 13021:
                textMessage = @"Report succeeded";
                break;
            default:
                textMessage = @"Unknown error";
                succes = NO;
                break;
        }
        completion(textMessage,succes);
    }
    else{
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        CLS_LOG(@"UUID:%@ Error:%d",uuidString, codeNumber);
        switch (codeNumber){
                // Modul 1 --------------
            case 1000:
                textMessage = @"Server is in maintanence";
                break;
            case 1002:
                textMessage = @"generic_failure_text";
                break;
            case 1003:
                textMessage = @"Access Token Validation - Invalid User";
                break;
            case 1004:
                textMessage = @"token_expired_warning";
                [self expiredToken];
                break;
            case 1005:
                break;
            case 1006:
                textMessage = @"User not logged in";
                break;
            case 1007:
                textMessage = @"inv_account_not_exist";
                break;
            case 1008:
                textMessage = @"OONA is not yet available in your country";
                break;
            case 1012:
                textMessage = @"Invite Code Validation - Invite code missed";
                break;
            case 1013:
                textMessage = @"This code is invalid";
                break;
            case 1014:
                textMessage = @"This code is already used";
                break;
            case 1015:
                textMessage = @"OONA is not yet available in your country";
                break;
            case 1016:
                textMessage = @"Code is already been use, please use another code.";
                break;
            case 1017:
                textMessage = @"remove APPLE1 ";
                break;
          
            case 1022:
                textMessage = @"Please input your sponsor code";
                break;
            case 1023:
                textMessage = @"The code is invalid";
                break;
            case 1024:
                textMessage = @"OONA is not yet available in your country";
                break;
            case 1082:
                textMessage = @"Please input your code";
                break;
            case 1083:
                textMessage = @"The code is invalid";
                break;
                // Modul 2 --------------
            case 2012:
                textMessage = @"Invalid registration format";
                break;
            case 2013:
                textMessage = @"Registration failed";
                break;
            case 2014:
                textMessage = @"User already registered and activated";
                break;
            case 2023:
                textMessage = @"Token validation failure (input not matched 6-digit numeric)";
                break;
            case 2024:
                textMessage = @"Invalid verification token";
                break;
            case 2032:
                textMessage = @"Invalid login credentials";
                break;
            case 2033:
                textMessage = @"Inactive user (banned, disabled by admin, etc)";
                break;
            case 2034:
                textMessage = @"Authentication failure, either email or password not same with database";
                break;
            case 2035:
                textMessage = @"User not registered";
                break;
            case 2036:
//                textMessage = @"JWT validation failure";
                textMessage = @"login_failed_please_try_again_later";
                break;
            case 2082:
                textMessage = @"Failed to invalidate JWT";
                break;
            case 2092:
                textMessage = @"Failed to create new password";
                break;
            case 2102:
                textMessage = @"Password not matched the criteria";
                break;
            case 2103:
                textMessage = @"Password and Confirm Password not matched";
                break;
            case 2104:
                textMessage = @"Password update failure";
                break;
            case 2105:
                textMessage = @"Password same with old password in the database";
                break;
            case 2132:
                textMessage = @"Validation error, either email or password are invalid format";
                break;
            case 2133:
                textMessage = @"Inactive user (banned, disabled by admin, etc)";
                break;
            case 2134:
                textMessage = @"Authentication failure, either email or password not same with database";
                break;
            case 2135:
                textMessage = @"User not registered";
                break;
                // Modul 3 --------------
            case 3012:
                textMessage = @"Channel list retrieve failed";
                break;
            case 3013:
                textMessage = @"channel_not_available_3";
                break;
            case 3022:
                textMessage = @"episode_not_available_2";
                break;
            case 3023:
                textMessage = @"Inactive Channel";
                break;
            case 3032:
                textMessage = @"Streaming info retrieve failed";
                break;
            case 3052:
                textMessage = @"Playlist retrieve failed";
                break;
            case 3062:
                textMessage = @"Mood list retrieve failed";
                break;
            case 3072:
                textMessage = @"Category list retrieve failed";
                break;
            case 3082:
                textMessage = @"Add to favourite failed";
                break;
            case 3092:
                textMessage = @"Remove from favourite failed";
                break;
            case 3102:
                textMessage = @"Add to shuffle failed";
                break;
            case 3112:
                textMessage = @"Remove from shuffle failed";
                break;
            case 3122:
                textMessage = @"Add to blocked failed";
                break;
            case 3132:
                textMessage = @"Remove from blocked failed";
                break;
                // Modul 4 --------------
            case 4032:
                textMessage = @"Parental control passcode incorrect";
                break;
            case 4042:
                textMessage = @"Parental control passcode update failed (Forgot Password)";
                break;
            case 4052:
                textMessage = @"Add to allowed failed";
                break;
            case 4062:
                textMessage = @"Remove from allowed failed";
                break;
                // Modul 6 --------------
            case 6012:
                textMessage = @"Message list retrieve failed";
                break;
            case 6022:
                textMessage = @"Send message failed";
                break;
            case 6032:
                textMessage = @"Session update failed";
                break;
                // Modul 9 --------------
            case 9012:
                textMessage = @"Send user info failed";
                break;
            case 9022:
                textMessage = @"Options list retrieve failed";
                break;
                // Modul 10 -------------
            case 10012:
                textMessage = @"Programs list failed";
                break;
            case 10022:
                textMessage = @"Program details retrieve failed";
                break;
                // Modul 11 -------------
            case 11012:
                textMessage = @"User points history retrieve failed";
                break;
            case 11022:
                textMessage = @"Gift list retrieve failed";
                break;
            case 11032:
                textMessage = @"Gift details retrieve failed";
                break;
            case 11042:
                textMessage = @"Gift categories retrieve failed";
                break;
            case 11052:
                textMessage = @"Gift voucher redeem failed";
                break;
            case 11053:
                textMessage = @"out_of_stock_fail";
                break;
            case 11055:
                textMessage = @"reach_limit_failure_text";
                break;

            case 11056:
                textMessage = @"wrong_phone_failure_text";
                break;
            case 11062:
                textMessage = @"Gift voucher list retrieve failed";
                break;
            case 11072:
                textMessage = @"Gift voucher details retrieve failed";
                break;
            case 11082:
                textMessage = @"Bonus code verification failed";
                break;
            case 11083:
                textMessage = @"Invalid bonus code";
                break;
            case 11084:
                textMessage = @"Bonus code gift out of stock";
                break;
            case 11124:
                textMessage = @"bid_phone_already_bid";
                break;
                // Modul 13 -------------
            case 13012:
                textMessage = @"Report list retrieve failed";
                break;
            case 13022:
                textMessage = @"Report failed";
                break;
                // Modul 14 --------------
            case 14021 :
                textMessage = @"Invalid item";
                break;
            case 14022 :
                textMessage = @"Invalid payment method";
                break;
            case 14023 :
                textMessage = @"Payment request failed";
                break;
            case 14024 :
                textMessage = @"Failed preparing new transaction number";
                break;
            case 14025 :
                textMessage = @"No related carrier_billing_transactions record found";
                break;
            case 14026 :
                textMessage = @"Invalid TAC";
                break;
            case 14031 :
                textMessage = @"Invalid transaction ID";
                break;
            
            case 17012 :
                textMessage = @"Invalid bonus code";
                break;
            case 17013 :
                textMessage = @"Bonus code has already used";
                break;
            case 17014:
                textMessage = @"Bonus code expired";
                break;

            default:
                break;
        }
        completion(textMessage,NO);
    }
    
}
+ (void)expiredToken {
    NSLog(@"API Response Code: Your token is expired.");
    
}
@end

