//
//  APIResponseCode.h
//  OONA
//
//  Created by OONA iOS on 11/1/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIResponseCode : NSObject
+ (void)checkResponseWithCode:(NSString *)code WithStatus:(NSString *)status completion:(void (^)(NSString *message, BOOL isSuccess))completion;
@end
