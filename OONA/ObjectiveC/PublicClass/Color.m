//
//  Color.m
//  OONA
//
//  Created by OONA iOS on 11/7/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "CommonHeader.h"
// #import "Color.h"

@implementation Color
+ (UIColor *)blueOONAColor {
    return [UIColor colorWithRed:0.0/255.0 green:120.0/255.0 blue:254.0/255.0 alpha:1];
}
+ (UIColor *)darkGrayOONAColor {
    return HexColor(0x1c232b);
}
+ (UIColor *)lighterDarkGrayOONAColor {
    return HexColor(0x28333e);
}
+ (UIColor *)darkGrayOONAColor:(CGFloat )alpha {
    return HexColorA(0x1c232b,alpha);
}
+ (UIColor *)darkRedColor {
    return HexColor(0xE94339);
}
+ (UIColor *)searchTagColor {
    return HexColor(0x959595);
}
+ (UIColor *)redOONAColor{
    return [UIColor colorWithRed:214.0/255.0 green:23.0/255.0 blue:0.0/255.0 alpha:1];
}

+ (UIColor *)bgOONAColor {
    return [UIColor colorWithRed:19.0/255.0 green:25.0/255.0 blue:31.0/255.0 alpha:1];
}

+ (UIColor *)bgHeaderOONAColor {
    return [UIColor colorWithRed:47.0/255.0 green:54.0/255.0 blue:62.0/255.0 alpha:1];
}

+ (UIColor *)bgTransparantOONAColor {
    return [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.15];
}

+ (UIColor *)orangeBidColor {
    return [UIColor colorWithRed:242.0/255.0 green:154.0/255.0 blue:57.0/255.0 alpha:1.0];
}
+ (void)gradientColorOnView:(UIView *)view withColors:(NSArray *)colors{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.colors = colors;
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    [view.layer insertSublayer:gradient atIndex:0];
}
@end
