//
//  OONAString.h
//  ReacDemo
//
//  Created by admin on 29/5/2017.
//  Copyright © 2017 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#define str(x) [OONAString string:x]
@interface OONAString : NSObject
typedef enum Language {
    LanguageEnglish,
    LanguageIndonesian
}Language;
+(OONAString *)instance;
+(void)initLanguage:(Language)lang;
+(NSString *)string:(NSString *)key;
+(NSString *)currentLang;
@end
