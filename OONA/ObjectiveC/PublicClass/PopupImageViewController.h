//
//  HoobexViewController.h
//  OONA
//
//  Created by jack on 13/10/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupImageViewController : UIViewController{
    
}
@property (nonatomic,strong) UIImageView *image ;
- (instancetype)initWithImage:(UIImage *)image ;
@end
