//
//  signInView.m
//  OONA
//
//  Created by OONA on 03/05/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "UserData.h"
#import "SignInAlertView.h"
#import "OONA-Swift.h"
#import "OONAString.h"
#import "OONA-Swift.h"


typedef void(^actionBlock)(void);
typedef void(^dictionaryBlock)(NSDictionary *);
typedef void(^arrayBlock)(NSArray *);
typedef void(^callbackBlock)(id value);

@implementation SignInAlertView{
    
}
CGFloat buttonWidth = 100;
CGFloat buttonHeight = 40;

static SignInAlertView *thisView;
+(void)add:(SignInAlertView*)view
            withController:(UIViewController*)viewController
            withDismiss:(actionBlock)dismiss
            withAction:(actionBlock)action  {
    loginAction = action;
    dismissAction = dismiss;
    thisView = view ;
    view.frame = CGRectMake(0,
                            0,
                            viewController.view.frame.size.width,
                            viewController.view.frame.size.height);
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    blurEffectView.frame = view.bounds;
//    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
//    UIViewAutoresizingFlexibleHeight;
//    [view addSubview:blurEffectView];
    [view setBackgroundColor:[UIColor colorWithWhite:0 alpha:.7]];
    UIView *blueBG = [[UIView alloc]initWithFrame:CGRectMake(view.frame.size.width*.125, view.frame.size.height*.15, view.frame.size.width*.75, view.frame.size.height*.65)];
    [blueBG setBackgroundColor:[Color bgHeaderOONAColor]];
    
    blueBG.layer.cornerRadius = 40.f;
    UIImageView *logo = [[UIImageView alloc]initWithFrame:CGRectMake(25,25 ,25*130/38, 25)];
    [logo setImage:[UIImage imageNamed:@"app-left-icon"]];
    logo.alpha = .5 ;
    [blueBG addSubview:logo];
    UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(30, 80, 80, 80)];
    [icon setImage:[UIImage imageNamed:@"siska-bot-image"]];
    icon.layer.cornerRadius = 40 ;
    icon.contentMode = UIViewContentModeScaleAspectFill;
    icon.clipsToBounds = YES ;
    [blueBG addSubview:icon];
    NSString *infoText = [NSString stringWithFormat:str(@"hi_just_quick_reminder_please_sign_in"),[UserData userName]];
    UITextView *signInReminderTextView = [[UITextView alloc]initWithFrame:CGRectMake(120, 70, blueBG.frame.size.width-150, 100)];
    [signInReminderTextView setText:infoText];
    [signInReminderTextView setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:15]];
    [signInReminderTextView setBackgroundColor:[UIColor clearColor]];
    [signInReminderTextView setTextColor:[UIColor whiteColor]];
    signInReminderTextView.editable = NO ;
    signInReminderTextView.userInteractionEnabled = NO ;
    [blueBG addSubview:signInReminderTextView];
    [view addSubview:blueBG];

    
    UIButton * backBtn = [[UIButton alloc]initWithFrame:
                          CGRectMake(blueBG.frame.size.width-75,
                                     10,
                                     50,
                                     50)];
    backBtn.clipsToBounds = YES;
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 15, 0);
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft ;
    backBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter ;
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit ;
    [backBtn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self
                action:@selector(dismissButton)
      forControlEvents:UIControlEventTouchUpInside];
    [blueBG addSubview:backBtn];
//    [view setHidden:YES];
    UIButton *signIn = [[UIButton alloc]initWithFrame:
                        CGRectMake(blueBG.frame.size.width/2-buttonWidth/2,
                                   blueBG.frame.size.height-buttonHeight-15,
                                   buttonWidth,
                                   buttonHeight)];
    signIn.layer.cornerRadius = signIn.frame.size.height/2 ;
    signIn.layer.borderColor = [UIColor whiteColor].CGColor;
    signIn.layer.borderWidth = 1.f ;
    [signIn addTarget:self action:@selector(signinBtn) forControlEvents:UIControlEventTouchUpInside];
    [signIn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [signIn setTitle:str(@"log_in") forState:UIControlStateNormal];
    
    [blueBG addSubview:signIn];
    
    
    view.alpha = 0;
    [[[UIApplication sharedApplication] keyWindow] addSubview:view];
    
    [UIView animateWithDuration:.3f animations:^{
        view.alpha = 1;
    }];
    
}
+ (void)dismissButton {
    [SignInAlertView removeView:^{
        if (dismissAction)
            dismissAction();
    }];
}
+ (void)signinBtn{
    NSLog(@"click signin");
    [SignInAlertView removeView:^{
        if (loginAction){
            NSLog(@"signin action");
            loginAction();
        }
    }];
}
+(void)removeView:(actionBlock)completion {
    [UIView animateWithDuration:.3f animations:^{
        thisView.alpha = 0;
    } completion:^(BOOL finished) {
         [thisView removeFromSuperview];
        completion() ;
    }];
   
}

@end
