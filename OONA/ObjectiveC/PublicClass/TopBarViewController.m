//
//  TopBarViewController.m
//  OONA
//
//  Created by jack on 28/9/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//
//#import "CommonHeader.h"
#import <Foundation/Foundation.h>
#import "TopBarViewController.h"
#import "OONA-Swift.h"

@implementation TopBarViewController{
    CGFloat tabBarHeight ;
    UITapGestureRecognizer *dismissKB;
    
}

- (void)viewDidLoad{
    [super viewDidLoad];
//    [self.view setBackgroundColor:[UIColor whiteColor]];
    bg = [[UIImageView alloc]initWithFrame:self.view.frame];
    [bg setImage:[UIImage imageNamed:@"background-landscape"]];
    [self.view addSubview:bg];
    [self.navigationController setNavigationBarHidden:YES];
    [self registerForKeyboardNotifications];
    [self initTabBar];
}
- (void)initTabBar {
    tabBarHeight = 35 ;
    tab = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, tabBarHeight)];
    [tab setBackgroundColor:[Color bgHeaderOONAColor]];
    
    [self.view addSubview:tab];
    
    logoBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - (120 *.4), 5, 240*.4, 53*.4)];
    [logoBtn setImage:[UIImage imageNamed:@"app-left-icon"] forState:UIControlStateNormal];
    logoBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [logoBtn addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    [tab addSubview:logoBtn];
    
    icon = [[UIButton alloc]initWithFrame:CGRectMake(self.view.safeAreaInsets.left+10 , 0, 30 , 30 )];
    icon.imageEdgeInsets = UIEdgeInsetsMake(0, 2, 0, 2);
    [icon setImage:[UIImage imageNamed:@"arrow-left"] forState:UIControlStateNormal];
    [icon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    icon.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight ;
    icon.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter ;
    icon.imageView.contentMode = UIViewContentModeScaleAspectFit ;
    [tab addSubview:icon];
    
    titleLB = [[UILabel alloc]initWithFrame:CGRectMake(42+10, 0, CGRectGetMinX(logoBtn.frame)-52, tabBarHeight)];
    [titleLB setText:self.title];
    titleLB.font = [UIFont fontWithName:@"SourceSansPro-Regular" size:18.0];
    [titleLB setTextColor:[UIColor whiteColor]];
    [tab addSubview:titleLB];
    
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    dismissKB = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(dismissKeyboard)];
    // warning! this line must be appear. it will affect the mainbot.
//    dismissKB.cancelsTouchesInView = YES;
    dismissKB.delegate = self;
    // end
    [self.view addGestureRecognizer:dismissKB];
   
}
#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // 输出点击的view的类名
    
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UIButton"])
    {
        return NO;
    }
    return  YES;
}
- (void)dismissKeyboard {
    [self.view endEditing:YES];
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.view removeGestureRecognizer:dismissKB];

}
- (void)back {
    if (self.isPresent == YES) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)home {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
