//
//  signInView.h
//  OONA
//
//  Created by OONA on 03/05/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//
//#import "OONA-Swift.h"


#import <UIKit/UIKit.h>
//#import "CommonHeader.h"
typedef void(^actionBlock)(void);
typedef void(^dictionaryBlock)(NSDictionary *);
typedef void(^arrayBlock)(NSArray *);
typedef void(^callbackBlock)(id value);

static actionBlock dismissAction;
static actionBlock loginAction;

@interface SignInAlertView : UIView {
 
}
+(void)add:(SignInAlertView*)view
withController:(UIViewController*)viewController
withDismiss:(actionBlock)dismiss
withAction:(actionBlock)action ;

+(void)removeView:(actionBlock)completion;

@end
