//
//  NSDictionary+HandleNull.h
//  OONA
//
//  Created by OONA on 02/05/18.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandleNull : NSObject

+(NSDictionary *)nullFreeDictionaryWithDictionary:(NSDictionary *)dictionary;

@end
