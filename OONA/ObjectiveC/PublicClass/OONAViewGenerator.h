//
//  OONAViewGenerator.h
//  OONA
//
//  Created by jack on 26/6/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OONAViewGenerator : NSObject {

}
@property (nonatomic) float defaultTopPadding;
@property (nonatomic) float defaultLeftPadding;
@property (nonatomic) float defaultBottomPadding;
@property (nonatomic) float defaultRightPadding;
- (UIView *)renderViewWithScheme:(NSDictionary *)scheme onView:(UIView *)superView;
+ (void)addShadowToView:(UIView *)view
            withOpacity:(float)shadowOpacity
           shadowRadius:(CGFloat)shadowRadius
        andCornerRadius:(CGFloat)cornerRadius;
@end
