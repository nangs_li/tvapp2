//
//  Header.h
//  OONA
//
//  Created by jack on 27/9/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "OONAString.h"
//#import "TabButton.h"
#import "OONAButton.h"
#define VariableName(arg) (@""#arg)
#define OONAFontRegular @"SourceSansPro-Regular"
#define OONAFontSemiBold @"SourceSansPro-SemiBold"
#define OONAFontLight @"SourceSansPro-Light"
#define OONAFontBold @"SourceSansPro-Bold"
//static NSString * const kViewControllerAccountID = @"5664852414001";
static NSString * const kViewControllerVideoID = @"5668005539001";
static NSString * const kPeer5CustomerID = @"03gcg5dk1zygxhm10ykx";
static NSString * const kSpotXApiKey = @"e66637eea864ee137c69e824e61c4232"; // old a4135a54591e55795add350522122e27
#define INMOBI_ACCOUNT_ID   @"4028cb8b2c3a0b45012c406824e800ba"
#define SafeString(STRING) ([STRING length] == 0 ? @"" : STRING)
#define darkThemeColor [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:1]
#define darkMenuColor [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:.5]
//#define themeBlueColor HexColor(0x0078fe)
#define UIColorFromRGB(rgbValue) [UIColor \colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \ green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \ blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define noCodeBtnTitleColor HexColor(0x979797)
#define themeBlueColor [UIColor colorWithRed:0.0/255.0 green:120.0/255.0 blue:254.0/255.0 alpha:1]
#define themeLightBlueColor HexColor(0x50a1fa)
#define HexColor(hex) [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16)) / 255.0 \
green:((float)((hex & 0xFF00) >> 8)) / 255.0 \
blue:((float)(hex & 0xFF)) / 255.0 \
alpha:1.0]
#define HexColorA(hex,a) [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16)) / 255.0 \
green:((float)((hex & 0xFF00) >> 8)) / 255.0 \
blue:((float)(hex & 0xFF)) / 255.0 \
alpha: a ]
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height <= 320)?TRUE:FALSE

#define IS_IPHONE_X (MIN(CGRectGetWidth([[UIScreen mainScreen] bounds]), CGRectGetHeight([[UIScreen mainScreen] bounds])) == 812)
typedef NS_ENUM(NSInteger, ChannelType) {
    ChannelTypeMood,
    ChannelTypeFavourite,
    ChannelTypeCategory,
    ChannelTypeShuffle,
    ChannelTypeSearch,
    ChannelTypeNormal,
    ChannelTypeSearchResultChannelSingle,
    ChannelTypeSearchResultEpisodeSingle,
    ChannelTypeSearchResultChannelList,
    ChannelTypeSearchResultEpisodeList
};

static NSString * const kViewControllerPlaybackServicePolicyKey = @"BCpkADawqM0BEo9o-jVzOLaFZImLcsQ8wfK1Wrn7oZnKQRSI3MTodHRBW1_bMtyNQGcF528Is4tuTAI7-gKZVidRRsFnkMLCMo0qRKxxOqgK5S46rLstodHeaPZixcaChp43LXqHYXwgyofy";
static NSString * const kViewControllerAccountID = @"5664852414001";
//static NSString * const kOMSDKUrlString = @"https://s3-us-west-2.amazonaws.com/omsdk-files/compliance-js/omid-validation-verification-script-v1.js" ;

static NSString * const kOMSDKUrlString = @"https://s3-ap-southeast-1.amazonaws.com/oona-files-upload/ias/omsdk-v1.js" ;
static NSString * const kOMSDKVarificationUrlString = @"https://s3-us-west-2.amazonaws.com/omsdk-files/compliance-js/omid-validation-verification-script-v1.js";
static NSString * const kOMSDKParam = @"iabtechlab-oona" ;
static NSString * const kOMSDKVendorKey = @"iabtechlab.com-omid" ;
static NSString * const PARTNER_SDK_OR_APP_VERSION = @"1.2.11-Oonaemail";
static NSString * const phoneLocationCode = @"+62";
typedef void(^actionBlock)(void);
typedef void(^dictionaryBlock)(NSDictionary *);
typedef void(^arrayBlock)(NSArray *);
typedef void(^callbackBlock)(id value);

// testing purpose
//static NSString * const kViewControllerAccountID = @"5970343594001";
//static NSString * const kViewControllerPlaybackServicePolicyKey=@"BCpkADawqM3J0ShmV9cA5fiqwTmhkzFLAur9TVyY06FJOK3rBu8Mv1KNF3-HfYxz27Rw65_QToxXdadp95oj9qx6qOt48h53Nu20wiihfpDalygGnhW5ZGSFvtt_YvfiyPhLrmyG1UVnDgPP";


// end testing
