//
//  Color.h
//  OONA
//
//  Created by OONA iOS on 11/7/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Color : NSObject
+ (UIColor *)blueOONAColor;
+ (UIColor *)redOONAColor;
+ (UIColor *)bgOONAColor;
+ (UIColor *)bgHeaderOONAColor;
+ (UIColor *)bgTransparantOONAColor;
+ (UIColor *)darkRedColor;
+ (UIColor *)lighterDarkGrayOONAColor;
+ (UIColor *)darkGrayOONAColor;
+ (UIColor *)darkGrayOONAColor:(CGFloat)alpha;
+ (UIColor *)searchTagColor;
+ (UIColor *)orangeBidColor;
+ (void)gradientColorOnView:(UIView *)view withColors:(NSArray *)colors;
@end
