//
//  TopBarViewController.h
//  OONA
//
//  Created by jack on 28/9/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBarViewController : UIViewController<UIGestureRecognizerDelegate>{
    UILabel *titleLB ;
    UIButton *icon ;
    UIView *tab ;
    UIButton * logoBtn;
    UIImageView *bg ;
}
@property (nonatomic)BOOL isPresent;
@end

