//
//  CustomPopup.m
//  OONA
//
//  Created by OONA iOS on 11/21/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "CommonHeader.h"
#import "CustomPopup.h"
#import "OONA-Swift.h"
#import "UserData.h"
//#import ""

@implementation CustomPopup{
    UIImageView *logo;
     UITapGestureRecognizer *dismissKB;
}

- (id)initWithMessage:(NSString *)message{
    self = [super init];
    [self setupDisplayWithWidth:[UIScreen mainScreen].bounds.size.width*.65 andHeight:[UIScreen mainScreen].bounds.size.height*.75];
    _info = [[UITextView alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(logo.frame), _popupView.frame.size.width-150, _popupView.frame.size.height -  CGRectGetMaxY(logo.frame) -75)];
    
    [_info setFont:[UIFont fontWithName:OONAFontRegular size:16]];
    _info.editable = NO ;
    _info.userInteractionEnabled = NO ;
    [_info setBackgroundColor:[UIColor clearColor]];
    
    [_info setTextColor:[UIColor whiteColor]];
    [_popupView addSubview:_info];
    
    [_info setText:message];
    [self setupTwoButton];

    return self;
}
- (id)initPhoneDialogWithMessage:(NSString *)message {
    self = [super init];
    [self setupDisplayWithWidth:[UIScreen mainScreen].bounds.size.width*.7 andHeight:[UIScreen mainScreen].bounds.size.height*.7];
    
    _info = [[UITextView alloc]initWithFrame:CGRectMake(100, CGRectGetMaxY(logo.frame)+5, _popupView.frame.size.width-150, 50)];
    
    [_info setFont:[UIFont fontWithName:OONAFontRegular size:16]];
    [_info setText:message];
    _info.editable = NO ;
    _info.userInteractionEnabled = NO ;
    _info.scrollEnabled = NO ;
    [_info setBackgroundColor:[UIColor clearColor]];
    
    [_info setTextColor:[UIColor whiteColor]];
    _info.textContainerInset = UIEdgeInsetsZero;
    [_popupView addSubview:_info];
    
    _phoneInfo = [[UITextView alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(_info.frame), _popupView.frame.size.width-150-50, 30)];
    [_phoneInfo setBackgroundColor:UIColor.clearColor];
    [_phoneInfo setTextColor:UIColor.whiteColor];
    _phoneInfo.editable = NO ;
    _phoneInfo.userInteractionEnabled = NO ;
    _phoneInfo.scrollEnabled = NO ;
    [_phoneInfo setFont:[UIFont fontWithName:OONAFontRegular size:16]];
    [_phoneInfo setText:[NSString stringWithFormat:@"Phone: %@",[UserData phoneNumber]]];
    [_phoneInfo sizeToFit];
    [_popupView addSubview:_phoneInfo];
    
    _editPhoneBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_phoneInfo.frame)+10,CGRectGetMinY(_phoneInfo.frame),50,_phoneInfo.frame.size.height)];
    
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:str(@"edit")];
    [commentString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:OONAFontRegular size:16]
                       range:NSMakeRange(0, [commentString length])];
    
    [commentString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor whiteColor]
                          range:NSMakeRange(0, [commentString length])];
//    [commentString addAttribute:NSFontAttributeName
//                          value:[UIFont fontWithName:OONAFontRegular size:16]
//                          range:NSMakeRange(0, [commentString length])];
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    
    [_editPhoneBtn setAttributedTitle:commentString forState:UIControlStateNormal];
    
//    [_editPhoneBtn setTitle:@"EDIT" forState:UIControlStateNormal];
    [_editPhoneBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
//    _editPhoneBtn.layer.cornerRadius = _editPhoneBtn.frame.size.height/2 ;
//    _editPhoneBtn.layer.borderColor = UIColor.whiteColor.CGColor ;
//    _editPhoneBtn.layer.borderWidth = 1.f ;
    [_editPhoneBtn.titleLabel setFont:[UIFont fontWithName:OONAFontRegular size:14]];
    [_editPhoneBtn addTarget:self action:@selector(editAction) forControlEvents:UIControlEventTouchUpInside];
//    [_editPhoneBtn setBackgroundColor:UIColor.redColor];
    [_popupView addSubview:_editPhoneBtn];
    
    _info2 = [[UITextView alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(_phoneInfo.frame)+5, _popupView.frame.size.width-150, _popupView.frame.size.height -  CGRectGetMaxY(_phoneInfo.frame) -75)];
    
    [_info2 setFont:[UIFont fontWithName:OONAFontRegular size:13]];
    [_info2 setText:str(@"by_buying_you_are_agree_with_terms_Conditions")];
    _info2.textContainerInset = UIEdgeInsetsZero;
    _info2.editable = NO ;
    _info2.userInteractionEnabled = NO ;
    [_info2 setBackgroundColor:[UIColor clearColor]];
    
    [_info2 setTextColor:[UIColor grayColor]];
    [self setupTwoButton];
    [_popupView addSubview:_info2];
    return self;
}
- (void)setupZeroButton {
//    [self setupDisplayWithWidth:[UIScreen mainScreen].bounds.size.width*.7 andHeight:[UIScreen mainScreen].bounds.size.height*.5];
    [_popupView setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-[UIScreen mainScreen].bounds.size.width*.7)/2, ([UIScreen mainScreen].bounds.size.height-[UIScreen mainScreen].bounds.size.height*.5)/2, [UIScreen mainScreen].bounds.size.width*.7, [UIScreen mainScreen].bounds.size.height*.5)];
    
    CGRect buttonFrame = _button1.frame;
    buttonFrame.origin.x = (_popupView.frame.size.width/2)-(buttonFrame.size.width/2);
    _button1.frame = buttonFrame;
    _button1.hidden = YES;
    _button2.hidden = YES;
    _button3.hidden = YES;
    _button4.hidden = YES;
}
- (void)setupOneButton {
    CGRect buttonFrame = _button1.frame;
    buttonFrame.origin.x = (_popupView.frame.size.width/2)-(buttonFrame.size.width/2);
    _button1.frame = buttonFrame;
    _button1.hidden = NO;
    _button2.hidden = YES;
    _button3.hidden = YES;
    _button4.hidden = YES;
}
- (void)setupTwoButton {
    [_button1 setFrame:CGRectMake((_popupView.frame.size.width/2)-150-10+5, _popupView.frame.size.height-50, 150, 36)];
    [_button2 setFrame:CGRectMake((_popupView.frame.size.width/2)+10-5, _popupView.frame.size.height-50, 150, 36)];
    _button1.hidden = NO;
    _button2.hidden = NO;
    _button3.hidden = YES;
    _button4.hidden = YES;

}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    dismissKB = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(dismissKeyboard)];
    // warning! this line must be appear. it will affect the mainbot.
    dismissKB.cancelsTouchesInView = YES;
    // end
    [self addGestureRecognizer:dismissKB];
    
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardDidHideNotification object:nil];
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self removeGestureRecognizer:dismissKB];
}
- (void)dismissKeyboard {
    [self endEditing:YES];
}
- (void)setupDisplayWithWidth:(CGFloat)width andHeight:(CGFloat)height {
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:.8]];
    [self registerForKeyboardNotifications];
    CGFloat popupViewWidth = width;
    CGFloat popupViewHeight = height;
    
    _popupView = [[UIView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-popupViewWidth)/2, ([UIScreen mainScreen].bounds.size.height-popupViewHeight)/2, popupViewWidth, popupViewHeight)];
    [_popupView setBackgroundColor:[Color bgHeaderOONAColor]];
    _popupView.layer.cornerRadius = 40.f;
    
    logo = [[UIImageView alloc]initWithFrame:CGRectMake(25,15 ,25*130/38, 25)];
    [logo setImage:[UIImage imageNamed:@"app-left-icon"]];
    logo.alpha = 1.0f ;
    [_popupView addSubview:logo];
    
    _icon = [[UIImageView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(logo.frame)+10, 60, 60)];
    [_icon setImage:[UIImage imageNamed:@"OONA Bot Icon"]];
    _icon.layer.cornerRadius = _icon.frame.size.height/2 ;
    _icon.contentMode = UIViewContentModeScaleAspectFill;
    _icon.clipsToBounds = YES ;
    [_popupView addSubview:_icon];
    
 
    _button1 = [[UIButton alloc]initWithFrame:CGRectMake((_popupView.frame.size.width/2)-120-10+5, _popupView.frame.size.height-50, 150, 36)];
    _button1.layer.cornerRadius = _button1.frame.size.height/2 ;
    _button1.layer.borderColor = [UIColor whiteColor].CGColor;
    _button1.layer.borderWidth = 1.f ;
    [_button1 addTarget:self action:@selector(button1Action) forControlEvents:UIControlEventTouchUpInside];
    [_button1.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:15]];
    [_button1 setTitle:@"Yes" forState:UIControlStateNormal];
    [_popupView addSubview:_button1];
    
    _button2 = [[UIButton alloc]initWithFrame:CGRectMake((_popupView.frame.size.width/2)+10-5, _popupView.frame.size.height-50, 110, 36)];
    _button2.layer.cornerRadius = _button2.frame.size.height/2 ;
    _button2.layer.borderColor = [UIColor whiteColor].CGColor;
    _button2.layer.borderWidth = 1.f ;
    [_button2 addTarget:self action:@selector(button2Action) forControlEvents:UIControlEventTouchUpInside];
    [_button2.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:15]];
    [_button2 setTitle:@"No" forState:UIControlStateNormal];
    [_popupView addSubview:_button2];
    
    _button3 = [[UIButton alloc]initWithFrame:CGRectMake((_popupView.frame.size.width/2)-120-10+5, _popupView.frame.size.height-50-10, 110, 36)];
    _button3.layer.cornerRadius = _button1.frame.size.height/2 ;
    _button3.layer.borderColor = [UIColor whiteColor].CGColor;
    _button3.layer.borderWidth = 1.f ;
    [_button3 addTarget:self action:@selector(button3Action) forControlEvents:UIControlEventTouchUpInside];
    [_button3.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_button3 setTitle:@"Yes" forState:UIControlStateNormal];
    [_popupView addSubview:_button3];
    
    _button4 = [[UIButton alloc]initWithFrame:CGRectMake((_popupView.frame.size.width/2)+10-5, _popupView.frame.size.height-50-10, 110, 36)];
    _button4.layer.cornerRadius = _button2.frame.size.height/2 ;
    _button4.layer.borderColor = [UIColor whiteColor].CGColor;
    _button4.layer.borderWidth = 1.f ;
    [_button4 addTarget:self action:@selector(button4Action) forControlEvents:UIControlEventTouchUpInside];
    [_button4.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [_button4 setTitle:@"No" forState:UIControlStateNormal];
    [_popupView addSubview:_button4];
    
    _button3.hidden = YES;
    _button4.hidden = YES;
    
    _backBtn = [[UIButton alloc]initWithFrame:CGRectMake(_popupView.frame.size.width-65, 10, 50, 50)];
    _backBtn.clipsToBounds = YES;
    _backBtn.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 15, 0);
    _backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft ;
    _backBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter ;
    _backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit ;
    [_backBtn setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    [_backBtn addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    _backBtn.hidden = YES ;
    [_popupView addSubview:_backBtn];
    
    
    [self addSubview:_popupView];
}

- (id)initWithTextFieldTitle:(NSString *)titleText AndMessage:(NSString *)message{
    self = [super init];
    [self setupDisplayWithWidth:[UIScreen mainScreen].bounds.size.width*.7 andHeight:[UIScreen mainScreen].bounds.size.height*.8];
    
    _info = [[UITextView alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(logo.frame)+10, _popupView.frame.size.width-150, 80)];
    
    [_info setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:15]];
    _info.editable = NO ;
    _info.userInteractionEnabled = NO ;
    [_info setBackgroundColor:[UIColor clearColor]];
    
    [_info setTextColor:[UIColor whiteColor]];
    [_popupView addSubview:_info];
    
    [_info setText:titleText];
    
    _inputField = [[UITextField alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(_info.frame)+5, _popupView.frame.size.width-150, 35)];
    _inputField.layer.cornerRadius = 5.f ;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 35)];
    _inputField.leftView = paddingView;
    _inputField.leftViewMode = UITextFieldViewModeAlways;
    [_inputField setBackgroundColor:UIColor.whiteColor];
    
    [_popupView addSubview:_inputField];
    
    _msg = [[UITextView alloc]initWithFrame:CGRectMake(120, CGRectGetMaxY(_inputField.frame)+5, _popupView.frame.size.width-150, 60)];
    
    [_msg setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:13]];
    _msg.editable = NO ;
    _msg.userInteractionEnabled = NO ;
    [_msg setBackgroundColor:[UIColor clearColor]];
    
    [_msg setTextColor:[UIColor whiteColor]];
    [_msg setText:message];
    [_popupView addSubview:_msg];
    
    [self setupTwoButton];
    return self;
}
- (void)exit{
    [self dismiss];
    if (_dismissCompletion) {
        _dismissCompletion();
    }
}

- (void)dismiss{
    [UIView animateWithDuration:.2 animations:^{
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}
- (void)editAction {
    if (_editAction) {
        _editAction();
    }
}
- (void)button1Action{
    if (_completionButton1) {
        _completionButton1();
    }
}

- (void)button2Action{
    if (_completionButton2) {
        _completionButton2();
    }
}

- (void)button3Action{
    if (_completionButton3) {
        _completionButton3();
    }
}

- (void)button4Action{
    if (_completionButton4) {
        _completionButton4();
    }
}
@end
