//
//  String.m
//  ReacDemo
//
//  Created by admin on 29/5/2017.
//  Copyright © 2017 admin. All rights reserved.
//

#import "OONAString.h"
#import "NetworkModule.h"
@import FirebaseMessaging;
@implementation OONAString
static NSString *enTopic = @"DailyNews-en";
static NSString *idTopic = @"DailyNews-id";
static NSString *enGeneralTopic = @"general-en";
static NSString *idGeneralTopic = @"general-id";
static OONAString *instance=nil;
static NSDictionary *stringArray=nil;
static Language appLang;
+(OONAString *)instance{
    @synchronized(self){
        if(instance==nil){
            instance=[OONAString new];
        }
        return instance;
    }
}
+(void)initLanguage:(Language)lang{
    [self subscribeTopic:lang];
    NSString *langToLoad=[self langEnumToFilename:lang];
    appLang = lang;
    [[NSUserDefaults standardUserDefaults] setObject:langToLoad forKey:@"selected_language"];
    stringArray=[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:langToLoad ofType:@"strings"]];
}
+ (void)subscribeTopic:(Language)lang {
    if ([APP_ENV isEqualToString:@"dev"]){
        NSString *  DEBUG_HK_KEY = @"debug-HongKong";
        [[FIRMessaging messaging] subscribeToTopic:DEBUG_HK_KEY
                                        completion:^(NSError * _Nullable error) {
                                            NSLog(@"Subscribed to topic:%@",DEBUG_HK_KEY);
                                        }];
    }
        
    
    NSString *topicToSub = @"";
    NSString *topicToUnSub = @"";
    NSString *generalTopicToSub = @"";
    NSString *generalTopicToUnSub = @"";
    if (lang == LanguageEnglish){
        topicToSub = enTopic;
        topicToUnSub = idTopic;
        generalTopicToSub = enGeneralTopic;
        generalTopicToUnSub = idGeneralTopic;
    }else{
        topicToSub = idTopic;
        topicToUnSub = enTopic;
        generalTopicToUnSub = enGeneralTopic;
        generalTopicToSub = idGeneralTopic;
    }
    
//    [[FIRMessaging messaging] uns
    [[FIRMessaging messaging] subscribeToTopic:topicToSub
                                    completion:^(NSError * _Nullable error) {
                                        NSLog(@"Subscribed to topic:%@",topicToSub);
                                    }];

    [[FIRMessaging messaging] unsubscribeFromTopic:topicToUnSub
                                    completion:^(NSError * _Nullable error) {
                                        NSLog(@"Unsubscribed to topic:%@",topicToUnSub);
                                    }];
    
    [[FIRMessaging messaging] subscribeToTopic:generalTopicToSub
                                    completion:^(NSError * _Nullable error) {
                                        NSLog(@"Subscribed to topic:%@",generalTopicToSub);
                                    }];
    
    [[FIRMessaging messaging] unsubscribeFromTopic:generalTopicToUnSub
                                        completion:^(NSError * _Nullable error) {
                                            NSLog(@"Unsubscribed to topic:%@",generalTopicToUnSub);
                                        }];


}
+ (NSString *)currentLang {
    if (appLang == LanguageIndonesian){
        return @"id";
    }else{
        return @"en";
    }
    return nil;
}
+(NSString *)langEnumToFilename:(Language)lang{
    switch (lang) {
        case LanguageEnglish:
            return @"en";
            break;
        case LanguageIndonesian: 
            return @"id";
            break;
        default:
            break;
    }
    return @"en";
}
+(NSString *)string:(NSString *)key{
    if (stringArray[key] == nil)
        return @"";
    
    return stringArray[key];
}
@end
