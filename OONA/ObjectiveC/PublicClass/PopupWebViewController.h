//
//  Header.h
//  OONA
//
//  Created by jack on 9/11/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "TopBarViewController.h"

@interface PopupWebViewController : TopBarViewController
- (instancetype)initWithUrl:(NSString *)url ;
@end

