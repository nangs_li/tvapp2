//
//  OONAViewGenerator.m
//  OONA
//
//  Created by jack on 26/6/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//

#import "OONAViewGenerator.h"
#import "CommonHeader.h"
@import CoreText;
@import BrightcovePlayerSDK;
@implementation OONAViewGenerator{
    
}
- (instancetype)init {
    self = [super init];
    
    // default variable here.
    _defaultTopPadding = 5;
    _defaultLeftPadding = 5;
    _defaultBottomPadding = 5;
    _defaultRightPadding = 5;
    
    return self;
}
- (UIView *)renderViewWithScheme:(NSDictionary *)scheme onView:(UIView *)superView{
    
    CGRect superFrame = superView.frame;
    NSLog(@"render %@",scheme);
    
    if ([scheme objectForKey:@"padding"]){
        if ([[scheme objectForKey:@"padding"] objectForKey:@"top"]){
            _defaultTopPadding = [[[scheme objectForKey:@"padding"] objectForKey:@"top"] floatValue];
        }
        if ([[scheme objectForKey:@"padding"] objectForKey:@"left"]){
            _defaultLeftPadding = [[[scheme objectForKey:@"padding"] objectForKey:@"left"] floatValue];
        }
        if ([[scheme objectForKey:@"padding"] objectForKey:@"bottom"]){
            _defaultBottomPadding = [[[scheme objectForKey:@"padding"] objectForKey:@"bottom"] floatValue];
        }
        if ([[scheme objectForKey:@"padding"] objectForKey:@"right"]){
            _defaultRightPadding = [[[scheme objectForKey:@"padding"] objectForKey:@"right"] floatValue];
        }
    }
    NSLog(@"default top padding %f",_defaultTopPadding);
    NSLog(@"default left padding %f",_defaultLeftPadding);
    NSLog(@"default right padding %f",_defaultRightPadding);
    NSLog(@"default bottom padding %f",_defaultBottomPadding);
    
    UIView *view ;
    UIView *renderedView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, superFrame.size.width, 0)];
    if ([[scheme objectForKey:@"type"]isEqualToString:@"label"]){
        view = [self renderTextViewWithScheme:scheme onView:superView];
    }
    
    if ([[scheme objectForKey:@"type"]isEqualToString:@"video"]){
        view = [self renderVideoViewWithScheme:scheme onView:superView];
        return view;
    }
    
    
    [view setFrame:CGRectMake(_defaultLeftPadding, _defaultTopPadding, view.frame.size.width, view.frame.size.height)];
    //    [textView setBackgroundColor:UIColor.lightGrayColor];
    [renderedView addSubview:view];
    [renderedView setFrame:CGRectMake(0, 0, superFrame.size.width, view.frame.size.height+_defaultBottomPadding+_defaultTopPadding)];
    
    
    return renderedView;
}
- (BCOVPUIPlayerView *)renderVideoViewWithScheme:(NSDictionary *)scheme onView:(UIView *)superView{
    CGRect superFrame = superView.frame;
    BCOVPlayerSDKManager *manager = [BCOVPlayerSDKManager sharedManager];
    BCOVFPSBrightcoveAuthProxy *bcoFairplayProxy = [[BCOVFPSBrightcoveAuthProxy alloc]initWithPublisherId:nil applicationId:nil];
    
    BCOVBasicSessionProvider *basicSessionProvider = [manager createBasicSessionProviderWithOptions:nil];
    id<BCOVPlaybackSessionProvider> fairplaySessionProvider = [manager createFairPlaySessionProviderWithApplicationCertificate:nil authorizationProxy:bcoFairplayProxy upstreamSessionProvider:basicSessionProvider];
    
    id<BCOVPlaybackController> controller = [manager createPlaybackControllerWithSessionProvider:fairplaySessionProvider viewStrategy:nil];
    // player layout
    BCOVPUIPlayerView *BCOView = [[BCOVPUIPlayerView alloc] initWithPlaybackController:controller options:nil];

    BCOVPUILayoutView *playbackLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonPlayback                   width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    
    BCOVPUILayoutView *currentTimeLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagLabelCurrentTime width:50.f elasticity:0.0];
    
    BCOVPUILayoutView *durationLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagLabelDuration      width:50.f elasticity:0.0];
    
    BCOVPUILayoutView *progressLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagSliderProgress width:kBCOVPUILayoutUseDefaultValue elasticity:1.0];
    
    BCOVPUILayoutView *screenModeLayoutView = [BCOVPUIBasicControlView layoutViewWithControlFromTag:BCOVPUIViewTagButtonScreenMode width:kBCOVPUILayoutUseDefaultValue elasticity:0.0];
    
    
    //
    
    NSArray *standardLayoutLine1 = @[ playbackLayoutView, currentTimeLayoutView, durationLayoutView, progressLayoutView, screenModeLayoutView ];
    NSArray *standardLayoutLines = @[ standardLayoutLine1 ];
    
    
    NSArray *compactLayoutLine1 = @[ playbackLayoutView,currentTimeLayoutView,progressLayoutView,durationLayoutView,screenModeLayoutView ];
    NSArray *compactLayoutLines = @[ compactLayoutLine1];
    BCOVPUIControlLayout *customLayout = [[BCOVPUIControlLayout alloc] initWithStandardControls:standardLayoutLines compactControls:compactLayoutLines];

    BCOView.controlsView.layout = customLayout;
    // Make the player view frame match its parent
    CGFloat videoViewWidth =  superFrame.size.width-_defaultLeftPadding-_defaultRightPadding;
    CGFloat videoViewHeight = videoViewWidth*9/16;
    BCOView.frame = CGRectMake(0, 0, videoViewWidth, videoViewHeight);
    BCOView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    BCOView.playbackController = controller;
    
    controller.delegate = self ;
    controller.autoPlay = YES;
    controller.autoAdvance = YES;
    
    
    
    
    [BCOView setFrame:CGRectMake(_defaultLeftPadding, _defaultTopPadding, BCOView.frame.size.width, BCOView.frame.size.height)];
    
    return BCOView;
}
- (UITextView *)renderTextViewWithScheme:(NSDictionary *)scheme onView:(UIView *)superView{
    CGRect superFrame = superView.frame;
    NSString *schemeText = [NSString stringWithFormat:@"%@",[scheme objectForKey:@"text"]];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc]initWithString:schemeText];
    NSString *fontName = @"SourceSansPro-Regular";
    
    CGFloat fontSize = 16;
    UIColor *fontColor ;
    
    
    if ([scheme objectForKey:@"fontColor"]){
        NSMutableString *tempHex=[[NSMutableString alloc] init];
        
        [tempHex appendString:[NSString stringWithFormat:@"0x%@",[scheme objectForKey:@"fontColor"]]];
        
        unsigned colorInt = 0;
        
        [[NSScanner scannerWithString:tempHex] scanHexInt:&colorInt];
        
        fontColor=HexColor(colorInt);
        
    }else{
        fontColor = UIColor.blackColor;
        
    }
    
    //    lblAttString.backgroundColor=UIColorFromRGB(colorInt);
    
    if ([scheme objectForKey:@"fontStyle"]){
        NSString *fontStyle = [NSString stringWithFormat:@"%@",[scheme objectForKey:@"fontStyle"]] ;
        if ([fontStyle rangeOfString:@"u"].location != NSNotFound) {
            // need to under line.
            [attString addAttribute:NSUnderlineStyleAttributeName
                              value:@(NSUnderlineStyleSingle)
                              range:(NSRange){0,[attString length]}];
            NSLog(@"string contains bla!");
        }
        if ([fontStyle rangeOfString:@"i"].location != NSNotFound) {
            // bold and italic.
            fontName = @"SourceSansPro-Italic";
        }
        if ([fontStyle rangeOfString:@"b"].location != NSNotFound) {
            // need to bold.
            fontName = @"SourceSansPro-Bold";
            if ([fontStyle rangeOfString:@"i"].location != NSNotFound) {
                // bold and italic.
                fontName = @"SourceSansPro-BoldItalic";
            }
        }
    }
    
    if ([scheme objectForKey:@"fontSize"]){
        NSString *fontSizeStr = [NSString stringWithFormat:@"%@",[scheme objectForKey:@"fontSize"]] ;
        
        
        if ([fontSizeStr rangeOfString:@"s"].location != NSNotFound) {
            // Small font size
            fontSize = 14;
        }
        if ([fontSizeStr rangeOfString:@"m"].location != NSNotFound) {
            // Medium font size
            fontSize = 18;
        }
        if ([fontSizeStr rangeOfString:@"l"].location != NSNotFound) {
            // Large font size
            fontSize = 22;
        }
        if ([fontSizeStr rangeOfString:@"xl"].location != NSNotFound) {
            // Extra large font size
            fontSize = 26;
        }
        if ([fontSizeStr rangeOfString:@"xxl"].location != NSNotFound) {
            // Extra Extra Large font size
            fontSize = 30;
        }
        
    }
    UIFont *font = [UIFont fontWithName:fontName size:fontSize] ;
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, superFrame.size.width-_defaultLeftPadding-_defaultRightPadding, MAXFLOAT)];
    textView.textContainerInset = UIEdgeInsetsZero;
    textView.textContainer.lineFragmentPadding = 0;
    [textView setEditable:NO];
    [textView setScrollEnabled:NO];
    textView.attributedText = attString;
    [textView setFont:font];
    [textView setTextColor:fontColor];
    [textView sizeToFit];
    return textView;
}

+ (void)addShadowToView:(UIView *)view
            withOpacity:(float)shadowOpacity
           shadowRadius:(CGFloat)shadowRadius
        andCornerRadius:(CGFloat)cornerRadius
{
    //////// shadow /////////
    CALayer *shadowLayer = [CALayer layer];
    shadowLayer.frame = view.layer.frame;
    
    shadowLayer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    shadowLayer.shadowOffset = CGSizeMake(13, 13);//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    shadowLayer.shadowOpacity = shadowOpacity;//0.8;//阴影透明度，默认0
    shadowLayer.shadowRadius = shadowRadius;//8;//阴影半径，默认3
    
    //路径阴影
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float width = shadowLayer.bounds.size.width;
    float height = shadowLayer.bounds.size.height;
    float x = shadowLayer.bounds.origin.x;
    float y = shadowLayer.bounds.origin.y;
    
    CGPoint topLeft      = shadowLayer.bounds.origin;
    CGPoint topRight     = CGPointMake(x + width, y);
    CGPoint bottomRight  = CGPointMake(x + width, y + height);
    CGPoint bottomLeft   = CGPointMake(x, y + height);
    
    CGFloat offset = -1.f;
    [path moveToPoint:CGPointMake(topLeft.x - offset, topLeft.y + cornerRadius)];
    [path addArcWithCenter:CGPointMake(topLeft.x + cornerRadius, topLeft.y + cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
    [path addLineToPoint:CGPointMake(topRight.x - cornerRadius, topRight.y - offset)];
    [path addArcWithCenter:CGPointMake(topRight.x - cornerRadius, topRight.y + cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI_2 * 3 endAngle:M_PI * 2 clockwise:YES];
    [path addLineToPoint:CGPointMake(bottomRight.x + offset, bottomRight.y - cornerRadius)];
    [path addArcWithCenter:CGPointMake(bottomRight.x - cornerRadius, bottomRight.y - cornerRadius) radius:(cornerRadius + offset) startAngle:0 endAngle:M_PI_2 clockwise:YES];
    [path addLineToPoint:CGPointMake(bottomLeft.x + cornerRadius, bottomLeft.y + offset)];
    [path addArcWithCenter:CGPointMake(bottomLeft.x + cornerRadius, bottomLeft.y - cornerRadius) radius:(cornerRadius + offset) startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
    [path addLineToPoint:CGPointMake(topLeft.x - offset, topLeft.y + cornerRadius)];
    
    //设置阴影路径
    shadowLayer.shadowPath = path.CGPath;
    
    //////// cornerRadius /////////
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
    view.layer.shouldRasterize = YES;
    view.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    [view.superview.layer insertSublayer:shadowLayer below:view.layer];
}
@end
