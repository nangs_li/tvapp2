//
//  NetworkModule.m
//  OONA
//
//  Created by jack on 13/9/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import "NetworkModule.h"
#include <sys/types.h>
#include <sys/sysctl.h>


@implementation NetworkModule {
    
    
    
}


+ (NSString*)hardwareString {
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}

+(NetworkModule*)instance{
    static NetworkModule *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[NetworkModule alloc] init];
        haveNewEnv = NO ;
        haveNewApiUrl = NO ;
        needFetchRemoteConfig = YES ;
        firebaseConfigFetched = NO ;
        firbaseConfigFetchSuccess = NO ;
        regSecret = @"wEa+sc8BaEzrS?-K" ;
        [[NSNotificationCenter defaultCenter] addObserver:sharedInstance selector:@selector(refreshBgApi) name:@"refresh_bg_api" object:nil];
    });
    
    if (needFetchRemoteConfig) {
        firebaseRemoteConfig = [FIRRemoteConfig remoteConfig];
        //    firebaseRemoteConfig
        needFetchRemoteConfig = NO ;
        [firebaseRemoteConfig fetchWithExpirationDuration:3600 completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(600 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                needFetchRemoteConfig = YES ;
                firebaseConfigFetched = NO ;
            });
            firebaseConfigFetched = YES ;
            if (status == FIRRemoteConfigFetchStatusSuccess) {
                
                
                regSecret = firebaseRemoteConfig[@"chimmeng"].stringValue ;
                NSLog(@"Firbase Remote Config fetched! : %@",firebaseRemoteConfig[@"chimmeng"].stringValue);
                
                firbaseConfigFetchSuccess = YES;
                [firebaseRemoteConfig activateFetched];
            } else {
                needFetchRemoteConfig = YES ;
                NSLog(@"Network Config not fetched");
                firbaseConfigFetchSuccess = NO;
                NSLog(@"Error %@", error.localizedDescription);
            }
            //        [self logEventApiWithName:name  parameters:ApiParams];
            
        }];
    }
    return sharedInstance;
}

- (NSNumber *)getTimeStampNow {
    NSTimeInterval time = ([[NSDate date] timeIntervalSince1970]); // returned as a double
    NSLog(@"Time: %f",time);
    if ( [[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] != nil){
        NSTimeInterval timeDiff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] floatValue];
        time += timeDiff;
        NSLog(@"Time Diff: %f",timeDiff);
        
    }
    
    
    NSLog(@"Time: %f",time);
    //    NSDate *serverTime = [[NSDate date]dateByAddingTimeInterval:timeDiff];
    //    NSLog(@"server time: %@ , local time: %@",serverTime,[NSDate date]);
    
    unsigned long long digits = (unsigned long long)time; // this is the first 10 digits
    unsigned long long decimalDigits = (unsigned long long)(fmod(time, 1) * 1000); // this will get the 3 missing digits
    unsigned long long timestamp = (digits * 1000) + decimalDigits;
    
    //    NSString *timestampString = [NSString stringWithFormat:@"%ld%d",digits ,decimalDigits];
    
    return [NSNumber numberWithUnsignedLongLong:timestamp] ;
}
+ (NSString *)getRegisterSecret {
    return regSecret;
}
+ (NSString *)random256BitKey {
    NSMutableString *key = @"";
    uint8_t randomBytes[16];
    int result = SecRandomCopyBytes(kSecRandomDefault, 16, randomBytes);
    if(result == 0) {
        key = [[NSMutableString alloc] initWithCapacity:16*2];
        for(NSInteger index = 0; index < 16; index++)
        {
            [key appendFormat: @"%02x", randomBytes[index]];
        }
        
    } else {
        NSLog(@"SecRandomCopyBytes failed for some reason");
    }
    
    NSLog(@"generated string : %@",key);
    
    
    return key;
}

/*



- (NSURLSessionDataTask *)sendAnalyticsQueueWithAlternateUrl:(NSString *)altUrl param:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/events",altUrl];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}

- (NSURLSessionDataTask *)sendAnalyticsQueue:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/events",API_URL];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}

+ (NSString *)apiUrl {
    if (firbaseConfigFetchSuccess)
        if (![@"" isEqualToString:firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue]){
            NSLog(@"using remote config api url: %@",firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue);
            return [NSString stringWithFormat:@"%@",firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue];
        }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",[self domainUrl],API_SUFFIX];
    return url ;
}

+ (NSString *)domainUrl {
    if ([APP_ENV isEqualToString:@"dev"])
        return DOMAIN_URL_DEV;
    
    if ([APP_ENV isEqualToString:@"uat"])
        return DOMAIN_URL_UAT;
    
    return DOMAIN_URL_PROD;
}*/
@end
