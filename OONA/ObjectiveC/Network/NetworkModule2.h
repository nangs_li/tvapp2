
@import UIKit;
@import Firebase;
#import "Constants.h"


static NSString *newEnv ;
static BOOL haveNewApiUrl ;
static NSString *newApiUrl ;
static BOOL haveNewEnv ;
static BOOL needFetchRemoteConfig ;
static FIRRemoteConfig *firebaseRemoteConfig;
static BOOL firbaseConfigFetchSuccess;
static BOOL firebaseConfigFetched;
static NSString *regSecret ;


@interface NetworkModule : NSObject
+ (NSString *)apiUrl ;
+ (NetworkModule*)instance ;
+ (NSString *)env ;
+ (NSString *)random256BitKey;
+ (NSString *)getRegisterSecret;
+ (NSString*)hardwareString;
- (NSNumber *)getTimeStampNow;
- (NSURLSessionDataTask *)sendAnalyticsQueueWithAlternateUrl:(NSString *)altUrl param:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
- (NSURLSessionDataTask *)sendAnalyticsQueue:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
- (NSURLSessionDataTask *)GETMETHOD:(NSString *)URLString
                         parameters:(id)parameters
                            headers:(NSArray *)headers
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
@end
