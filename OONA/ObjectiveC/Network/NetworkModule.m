//
//  NetworkModule.m
//  OONA
//
//  Created by jack on 13/9/2017.
//  Copyright © 2017 BOOSST. All rights reserved.
//
#import "NetworkModule.h"
#import "UserData.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "CommonHeader.h"
#import "CustomPopup.h"
#import "OONAFirBase.h"
//#import "RSA.h"
//#import "RefreshTokenHelper.h"
#import "AESCrypt.h"

#import "RSAEncryptor.h"

//#import <YYCategories/YYCategories.h>
//#import <YYCategories/UIDevice+YYAdd.h>
//@import  YYCategories.UIDevice_YYAdd;
//@import YYCategories ;
@import CoreTelephony;
@import Firebase;
@import SDWebImage;
@import AdSupport;
//#import <CoreTelephony/CTTelephonyNetworkInfo.h>
//#import <CoreTelephony/CTTelephonyNetworkInfo.h>
static NSString *newEnv ;
static BOOL haveNewApiUrl ;
static NSString *newApiUrl ;
static BOOL haveNewEnv ;
static BOOL needFetchRemoteConfig ;
static FIRRemoteConfig *firebaseRemoteConfig;
static BOOL firbaseConfigFetchSuccess;
static BOOL firebaseConfigFetched;
//static BOOL
static NSString *regSecret ;
NSString *const kAlternativeApiUrlKey = @"alternativeApiUrl";
NSString *const kSignatureKey = @"chimmeng";
NSString *const kAlternativeApplicationEnvironmentKey = @"applicationEnvironment";
//alternativeApiUrl
@implementation NetworkModule {
    CLLocationManager *locationManager;
    NSString *currentAppEnv ;
}
+ (NSString *)env {
    if (firbaseConfigFetchSuccess)
        if (![@"" isEqualToString:firebaseRemoteConfig[kAlternativeApplicationEnvironmentKey].stringValue])
            return [NSString stringWithFormat:@"%@",firebaseRemoteConfig[kAlternativeApplicationEnvironmentKey].stringValue];
    
    return DEFAULT_ENV;
}
+ (NSString *)apiUrl {
    if (firbaseConfigFetchSuccess)
        if (![@"" isEqualToString:firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue]){
            NSLog(@"using remote config api url: %@",firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue);
            return [NSString stringWithFormat:@"%@",firebaseRemoteConfig[kAlternativeApiUrlKey].stringValue];
        }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",[self domainUrl],API_SUFFIX];
    return url ;
}
+ (NSString *)domainUrl {
    if ([APP_ENV isEqualToString:@"dev"])
        return DOMAIN_URL_DEV;
    
    if ([APP_ENV isEqualToString:@"uat"])
        return DOMAIN_URL_UAT;
    
    return DOMAIN_URL_PROD;
}
+(NetworkModule*)instance{
    
    static NetworkModule *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    if ([UserData BgQueue].count == 0){
        
    }
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[NetworkModule alloc] init];
        haveNewEnv = NO ;
        haveNewApiUrl = NO ;
        needFetchRemoteConfig = YES ;
        firebaseConfigFetched = NO ;
        firbaseConfigFetchSuccess = NO ;
        regSecret = @"wEa+sc8BaEzrS?-K" ;
        [[NSNotificationCenter defaultCenter] addObserver:sharedInstance selector:@selector(refreshBgApi) name:@"refresh_bg_api" object:nil];
    });
    
    if (needFetchRemoteConfig) {
        firebaseRemoteConfig = [FIRRemoteConfig remoteConfig];
        //    firebaseRemoteConfig
        needFetchRemoteConfig = NO ;
        [firebaseRemoteConfig fetchWithExpirationDuration:3600 completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(600 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                needFetchRemoteConfig = YES ;
                firebaseConfigFetched = NO ;
            });
            firebaseConfigFetched = YES ;
            if (status == FIRRemoteConfigFetchStatusSuccess) {
                
                
                regSecret = firebaseRemoteConfig[kSignatureKey].stringValue ;
                NSLog(@"Firbase Remote Config fetched! : %@",firebaseRemoteConfig[kSignatureKey].stringValue);
                
                firbaseConfigFetchSuccess = YES;
                [firebaseRemoteConfig activateFetched];
            } else {
                needFetchRemoteConfig = YES ;
                NSLog(@"Network Config not fetched");
                firbaseConfigFetchSuccess = NO;
                NSLog(@"Error %@", error.localizedDescription);
            }
            //        [self logEventApiWithName:name  parameters:ApiParams];
            
        }];
    }
    return sharedInstance;
}
+ (NSString *)getRegisterSecret {
    //
    //    if (!firebaseConfigFetched) {
    //        return [NetworkModule getRegisterSecret];
    //    }
    return regSecret;
}
- (NSString *)GPSLat {
    //    return @"0.0000";
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        ) {
        
        NSLog(@"lat not in use permi");
        
    } else {
        NSLog(@"lat start update");
        [locationManager startUpdatingLocation]; //Will update location immediately
    }
    if ( [CLLocationManager authorizationStatus]== kCLAuthorizationStatusDenied ){
        NSLog(@"User denied GPS");
    }
    
    NSString *latitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    
    return latitude;
}

- (BOOL)checkGpsPermission {
    //    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters; // 10 m
    if ( [CLLocationManager authorizationStatus]== kCLAuthorizationStatusDenied ){
        NSLog(@"User denied GPS");
        return NO;
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        ) {
        // Will open an confirm dialog to get user's approval
        NSLog(@"lat not in use permi");
        
        
        return NO;
        //[_locationManager requestAlwaysAuthorization];
    } else {
        NSLog(@"lat start update");
        [locationManager startUpdatingLocation]; //Will update location immediately
        return YES;
    }
}
- (void)requestGpsPermission {
    //    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters; // 10 m
    [locationManager requestWhenInUseAuthorization];
}
//- (void)s
- (NSString *)GPSLong {
    //    return @"0.0000";
    //    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        ) {
        
        
    } else {
        [locationManager startUpdatingLocation]; //Will update location immediately
    }
    //    [locationManager startUpdatingLocation];
    
    NSString *longitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];;
    return longitude;
}
- (void)updateAppLanguage {
    NSString *url = [NSString stringWithFormat:@"%@register-device",API_URL];
    NSDictionary *params = @{@"appLanguage" : [UserData appLanguage]} ;
    
    
    NSLog(@"%@",params);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:^(NSURLSessionDataTask *task, id responseObject) {
                                                               NSLog(@"Language change sent.");
                                                           } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                               
                                                           }];
    
    
}
- (NSDictionary *)regDeviceParam {
    NSString *FCM = [NSString stringWithFormat:@"%@",[FIRMessaging messaging].FCMToken];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *advertisingIdentifier = [NSString stringWithFormat:@"%@",[ASIdentifierManager sharedManager].advertisingIdentifier];
    NSString *isAdvertisingTrackingEnabled = ([ASIdentifierManager sharedManager].isAdvertisingTrackingEnabled ? @"1" : @"0" ) ;
    NSLog(@"UUID %@",uuidString);
    if (!locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        ) {
        
    } else {
        [locationManager startUpdatingLocation]; //Will update location immediately
    }
    //    [locationManager startUpdatingLocation];
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    //    NSString *countryCode = [languageDic objectForKey:@"kCFLocaleCountryCodeKey"];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    NSString *latitude = [self GPSLat];
    NSString *longitude = [self GPSLong];;
    NSString *screenWidth = [NSString stringWithFormat:@"%f",[[UIScreen mainScreen] bounds].size.width];
    NSString *screenHeight = [NSString stringWithFormat:@"%f",[[UIScreen mainScreen] bounds].size.height];
    
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = networkInfo.subscriberCellularProvider;
    NSString* iOSCarrierName = carrier.carrierName;
    NSString *mcc = [carrier mobileCountryCode];
    NSString *mnc = [carrier mobileNetworkCode];
    NSString* iOSCarrierID = [NSString stringWithFormat:@"%@%@",mcc,mnc];
    if ([[UserData appLanguage] isEqualToString:@""] || [UserData appLanguage] == nil){
        
        [UserData setAppLanguage:[OONAString currentLang]];
    }
    if (iOSCarrierName == nil){
        iOSCarrierName = @"";
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = 20;
    
    NSString *timeZone = [formatter stringFromNumber:[NSNumber numberWithFloat:([NSTimeZone systemTimeZone].secondsFromGMT / 3600)]];
    NSString *timeZoneName = [NSTimeZone systemTimeZone].name ;
    
    if (![timeZone hasPrefix:@"-"]){
        timeZone = [NSString stringWithFormat:@"+%@",timeZone] ;
    }
    
    //    NSString *inviteCode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"inviteCode"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"deviceOS": @"iOS" ,
               @"deviceModel" : [self hardwareString],
                              @"pushNotificationToken" : FCM,
               @"appLanguage" : [UserData appLanguage],
               @"osLanguage" : languageCode,
               @"osVersion" : [UIDevice currentDevice].systemVersion,
               @"latitude" : latitude,
               @"longitude" : longitude,
               @"screenWidth" : screenWidth,
               @"screenHeight" : screenHeight,
               @"carrier" : iOSCarrierName,
               @"carrierId" : iOSCarrierID,
               @"appVersion" : [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
               @"timeZone" : timeZone,
               @"timeZoneName" : timeZoneName,
               @"advertisingIdentifier" : advertisingIdentifier,
               @"isAdvertisingTrackingEnabled" : isAdvertisingTrackingEnabled
               
               }.mutableCopy;
//
//    if ([UIDevice currentDevice].isJailbroken){
//        [params setValue:@1 forKey:@"isRooted"];
//    }
    if ([FIRMessaging messaging].FCMToken != nil){
        [params setObject:[FIRMessaging messaging].FCMToken forKey:@"pushNotificationToken"];
    }
    NSDictionary *originalParam = params ;
    [params setObject:[NetworkModule getRegisterSecret] forKey:@"chimmeng"];
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
    
    
    NSString *aesKey = [self random256BitKey];
    //原始数据
    NSString *originalString = aesKey;
    
    
    NSString *public_key_path = [[NSBundle mainBundle] pathForResource:@"public_key.der" ofType:nil];
    
    NSString *encryptStr = [RSAEncryptor encryptString:originalString publicKeyWithContentsOfFile:public_key_path];
    
    NSString *encryptedData = [AESCrypt encryptData:dataFromDict password:aesKey];
    
    //    if ()
    NSDictionary *encryptedDict = @{@"encryptedData" : encryptedData,
                                    @"key" : (encryptStr ? encryptStr : @"")
                                    };
    NSLog(@"originalParam:%@",originalParam);
    if (encryptedData == nil || encryptStr == nil) {
        return originalParam;
    }
    return encryptedDict;
}

- (NSString *)random256BitKey {
    //    unsigned char salt[32];
    //    NSString *salt = @"";
    //    for (int i=0; i<32; i++) {
    //
    //        salt = [NSString stringWithFormat:@"%@%c",salt,(unsigned char)arc4random()] ;
    //    }
    //    int SecRandomCopyBytes ( SecRandomRef rnd, size_t count, uint8_t *bytes );
    //    NSLog(@"%@",SecRandomCopyBytes(<#SecRandomRef rnd#>, <#size_t count#>, <#uint8_t *bytes#>))
    NSMutableString *key = @"";
    uint8_t randomBytes[16];
    int result = SecRandomCopyBytes(kSecRandomDefault, 16, randomBytes);
    if(result == 0) {
        key = [[NSMutableString alloc] initWithCapacity:16*2];
        for(NSInteger index = 0; index < 16; index++)
        {
            [key appendFormat: @"%02x", randomBytes[index]];
        }
        
    } else {
        NSLog(@"SecRandomCopyBytes failed for some reason");
    }
    
    NSLog(@"generated string : %@",key);
    
    
    return key;
    
    //    return [NSData dataWithBytes:salt length:32];
}
//NSString *ApiURL = @"http://apiuat.oonaid.com/api/v1/" ;
// Edit header and UUID here.
///////////////////////////////////////////////////////

- (AFHTTPSessionManager *)JsonSerializer {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:10];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    manager.requestSerializer = [AFJSONRequestSerializer serializer] ;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:uuidString forHTTPHeaderField:@"uuid"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript", @"text/plain",nil];
    return manager;
}
- (AFHTTPSessionManager *)JsonSerializerWithBearer {
    AFHTTPSessionManager *manager = [self JsonSerializer] ;
    if ([[UserData userToken] isEqualToString:@""])
        return manager;
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",[UserData userToken]] forHTTPHeaderField:@"Authorization"];
    return manager;
}
- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSLog(@"[OONA][API] GET URL : %@ \n Parameter : %@",URLString,parameters);
    int RetryCount = 2 ;
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] GET:URLString
                                                               parameters:nil
                                                                 progress:nil
                                                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                                      NSLog(@"[OONA][API] GET URL : %@ \n response : %@",URLString,responseObject);
                                                                      if ([@"1009" isEqualToString:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"code"]]] ){
//                                                                          [RefreshTokenHelper apiReceivedNeedRefreshToken:@"get" :URLString :parameters success:success failure:failure];
                                                                          
                                                                      }else{
                                                                          success(task,responseObject);
                                                                      }
                                                                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                                      failure(task,error);
                                                                  }
                                                               retryCount:2 retryInterval:1 progressive:false fatalStatusCodes:@[@401, @403]];
    
    
    
    return dataTask;
    
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    if ([URLString containsString:@"/analytics/events"]){
        NSLog(@"[OONA][API] POST URL : %@ \n ",URLString);
    }else{
        NSLog(@"[OONA][API] POST URL : %@ \n Parameter : %@",URLString,parameters);
    }
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] POST:URLString
                                                                parameters:parameters
                                                                  progress:nil
                                                                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                                                       NSLog(@"parent POST url : %@ return : %@",URLString,responseObject);
                                                                       if ([@"1009" isEqualToString:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"code"]]] ){
//                                                                           [RefreshTokenHelper apiReceivedNeedRefreshToken:@"post" :URLString :parameters success:success failure:failure];
                                                                           //                                                   [self refreshToken:^(NSURLSessionDataTask *task, id responseObject) {
                                                                           ////                                                       if ()
                                                                           //                                                   } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                           //
                                                                           //                                                   }];
                                                                       }else{
                                                                           
                                                                           if ([responseObject objectForKey:@"alert"] != nil ) {
                                                                               NSLog(@"have alert! %@",[responseObject objectForKey:@"alert"]);
                                                                               //            if (userInfo[@"notificationId"] != nil){
                                                                               //                                                   [[NSNotificationCenter defaultCenter] postNotificationName: @"apiAlertDialog" object:[responseObject objectForKey:@"alert"]];
                                                                               //            }
                                                                               [self showAlert:[responseObject objectForKey:@"alert"]];
                                                                           }
                                                                           success(task,responseObject);
                                                                       }
                                                                   }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                                                       NSLog(@"parent POST failure : %@ \n %@ \n %@ ",URLString,parameters,error.localizedDescription);
                                                                       failure(task,error);
                                                                   }
                                                                retryCount:2 retryInterval:1 progressive:false fatalStatusCodes:@[@401, @403]];
    return dataTask;
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSLog(@"[OONA][API] PUT URL : %@ \n Parameter : %@",URLString,parameters);
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] PUT:URLString
                                                               parameters:parameters
                                                                  success:success
                                                                  failure:failure
                                                               retryCount:2 retryInterval:1 progressive:false fatalStatusCodes:@[@401, @403]];
    return dataTask;
}

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] DELETE:URLString
                                                                  parameters:parameters
                                                                     success:success
                                                                     failure:failure
                                                                  retryCount:2 retryInterval:1 progressive:false fatalStatusCodes:@[@401, @403]];
    
    return dataTask;
}

///////////////////////////////////////////////////////////
// Time Stamp function

- (NSNumber *)getTimeStampNow {
    NSTimeInterval time = ([[NSDate date] timeIntervalSince1970]); // returned as a double
    NSLog(@"Time: %f",time);
    if ( [[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] != nil){
        NSTimeInterval timeDiff = [[[NSUserDefaults standardUserDefaults] objectForKey:@"server_time_diff"] floatValue];
        time += timeDiff;
        NSLog(@"Time Diff: %f",timeDiff);
        
    }
    
    
    NSLog(@"Time: %f",time);
    //    NSDate *serverTime = [[NSDate date]dateByAddingTimeInterval:timeDiff];
    //    NSLog(@"server time: %@ , local time: %@",serverTime,[NSDate date]);
    
    unsigned long long digits = (unsigned long long)time; // this is the first 10 digits
    unsigned long long decimalDigits = (unsigned long long)(fmod(time, 1) * 1000); // this will get the 3 missing digits
    unsigned long long timestamp = (digits * 1000) + decimalDigits;
    
    //    NSString *timestampString = [NSString stringWithFormat:@"%ld%d",digits ,decimalDigits];
    
    return [NSNumber numberWithUnsignedLongLong:timestamp] ;
}

// Custom API call Details (url, params)
- (void)verifyInviteCode:(NSString *)inviteCode success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    [self verifyInviteCodeWithCode:inviteCode success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess){
                // code correct.
                
                if ([inviteCode isEqualToString:@"SKIP_00"]){
                    // skip code
                    
                    [OONAFirBase logEventWithName:@"referral_code_page_skip_clicked" parameters:@{@"latitude" : [[NetworkModule instance] GPSLat],
                                                                                                  @"longitude" : [[NetworkModule instance] GPSLong]}];
                }else{
                    [OONAFirBase logEventWithName:@"referral_code_page_code_submitted" parameters:@{@"latitude" : [[NetworkModule instance] GPSLat],
                                                                                                    @"longitude" : [[NetworkModule instance] GPSLong]}];
                }
            }
        }];
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(task,error);
    }];
    
}
- (void)verifyInviteCode:(NSString *)inviteCode captchaToken:(NSString *)token success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    [self verifyInviteCodeWithCode:inviteCode captchaToken:token success:^(NSURLSessionDataTask *task, id responseObject) {
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess){
                // code correct.
                
                if ([inviteCode isEqualToString:@"SKIP_00"]){
                    // skip code
                    
                    [OONAFirBase logEventWithName:@"referral_code_page_skip_clicked" parameters:@{@"latitude" : [[NetworkModule instance] GPSLat],
                                                                                                  @"longitude" : [[NetworkModule instance] GPSLong]}];
                }else{
                    [OONAFirBase logEventWithName:@"referral_code_page_code_submitted" parameters:@{@"latitude" : [[NetworkModule instance] GPSLat],
                                                                                                    @"longitude" : [[NetworkModule instance] GPSLong]}];
                }
            }
        }];
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(task,error);
    }];
    
}
- (NSURLSessionDataTask *)verifyInviteCodeWithCode:(NSString *)inviteCode captchaToken:(NSString *)token success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@verify-invite-code",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    if (token != nil){
        params = @{@"inviteCode":inviteCode,
                   @"captchaToken":token
                   };
    }else{
        params = @{@"inviteCode":inviteCode
                   };
    }
    
    if ([inviteCode isEqualToString:@"SKIP_00"]){
        params = @{@"skip":@"1"};
    }
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)verifyCaptchaToken:(NSString *)token success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@test/verify",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"response":token};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)verifyInviteCodeWithCode:(NSString *)inviteCode success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@verify-invite-code",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"inviteCode":inviteCode};
    if ([inviteCode isEqualToString:@"SKIP_00"]){
        params = @{@"skip":@"1"};
    }
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)verifyBonusCode:(NSString *)bonusCode success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@bonus-code/verify",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"bonusCode":bonusCode};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getPollQuestion:(NSString *)channelID :(NSString *)episodeID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@polling/question",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"channelId":channelID,
               @"episodeId":episodeID
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)submitPollAnswer:(id)ans :(NSString *)channelID :(NSString *)episodeID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@polling/submit-answer",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"channelId":channelID,
               @"episodeId":episodeID,
               @"answers":ans
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getNotificationWithID:(NSString *)notiID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@notification/%@",API_URL,notiID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)socialShareResult:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@social-media-sharing-result",API_URL];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)sendEventNamed:(NSString *)eventName parameter:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/event/%@",API_URL,eventName];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)sendAnalyticsNamed:(NSString *)analticsNamed parameter:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/%@",API_URL,analticsNamed];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)sendAnalyticsQueue:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/events",API_URL];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)sendAnalyticsQueueWithAlternateUrl:(NSString *)altUrl param:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/events",altUrl];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)sendAnalyticswithAlternateUrl:(NSString *)altUrl named:(NSString *)analticsNamed parameter:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@analytics/%@",altUrl,analticsNamed];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}
- (NSURLSessionDataTask *)youtubeViewEventAPI:(NSDictionary *)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@analytics/event/youtube-video-view",API_URL];
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    
    return dataTask;
}

- (NSURLSessionDataTask *)getReportCategoriesWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@report/video",API_URL];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getGiftCategoriesWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@points/gifts/categories",API_URL];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getGiftDetailWithID:(NSString *)voucherID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@points/gift/%@",API_URL,voucherID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getVoucherDetailWithID:(NSString *)voucherID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@points/voucher/%@",API_URL,voucherID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)redeemVoucherWithID:(NSString *)giftID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@points/gift/redeem",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    
    params = @{@"giftId":giftID};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (void)refreshAccessToken:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    [self refreshToken:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess) {
                [UserData setUserToken:responseObject[@"accessToken"]];
                [OONAFirBase logEventWithName:@"user_refresh_token_success" parameters:@{}];
            }else{
                [OONAFirBase logEventWithName:@"user_refresh_token_fail" parameters:@{}];
            }
        }];
        
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(task,error);
    }];
}
- (NSURLSessionDataTask *)refreshToken:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@refresh-token",API_URL];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListAndSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                           failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    if (![[UserData lastChannelID] isEqualToString:@""])
        params = @{@"currentChannelId":[UserData lastChannelID]}.mutableCopy;
    NSLog(@"get ch list :%@",params);
    
    //    [params setValue:@"1" forKey:@"zeroRatingTest"];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithType:(ChannelType)type Offset:(NSString *)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    NSString * filter = @"";
    if (type == ChannelTypeFavourite){
        filter = @"favourite";
    }
    if (type == ChannelTypeShuffle){
        filter = @"shuffle";
    }
    params = @{@"filter":filter,@"offset":offset}.mutableCopy;
    NSLog(@"get ch list with param : %@",params);
    //    [params setValue:@"1" forKey:@"zeroRatingTest"];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithType:(ChannelType)type filterID:(NSString *)filterId Offset:(NSString *)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    int ofs = [offset intValue] ;
    if (type == ChannelTypeCategory){
        
        return [[NetworkModule instance]getChannelListWithCategoryID:filterId offset:ofs success:success failure:failure];
    }
    if (type == ChannelTypeMood){
        
        return [[NetworkModule instance]getChannelListWithMoodID:filterId offset:ofs success:success failure:failure];
    }
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    NSString * filter = @"";
    if (type == ChannelTypeFavourite){
        filter = @"favourite";
    }
    if (type == ChannelTypeShuffle){
        filter = @"shuffle";
    }
    params = @{@"filter":filter,@"offset":offset}.mutableCopy;
    NSLog(@"get ch list with param : %@",params);
    //    [params setValue:@"1" forKey:@"zeroRatingTest"];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithType:(ChannelType)type Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSString * filter = @"";
    if (type == ChannelTypeFavourite){
        filter = @"favourite";
    }
    if (type == ChannelTypeShuffle){
        filter = @"shuffle";
    }
    params = @{@"filter":filter};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithCategoryID:(NSString *)catID offset:(int)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    
    
    params = @{@"filter":@"category",
               @"categoryId":catID,
               @"offset" : @(offset)
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)openChannelWithID:(NSString *)ChannelID Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    
    if (ChannelID != nil){
        params = @{@"openChannelId":ChannelID};
    }
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)openChannelWithID:(NSString *)ChannelID sortBy:(NSString *)sortBy success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    
    if (ChannelID != nil){
        [params setObject:ChannelID forKey:@"openChannelId"];
    }
    if (sortBy != nil){
        [params setObject:sortBy forKey:@"sortBy"];
    }
    
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getEpisodeListWithChannelID:(NSString *)channelID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/episode",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    if (![[UserData currentEpisodeID] isEqualToString:@""]){
        params = @{@"currentEpisodeId":[UserData currentEpisodeID]};
        //        [UserData setCurrentEpisodeID:nil];
    }
    NSLog(@"api ep list url %@ param %@",url,params);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getEpisodeListWithChannelID:(NSString *)channelID andOffset:(NSNumber *)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/episode",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"offset":offset};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)keywordSuggestion:(NSString *)keyword success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@search/suggestion",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{ @"keyword" : keyword };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)searchWithKeyword:(NSString *)keyword WithFilter:(NSString *)filter SortBy:(NSString *)sortBy AndOffset:(NSString *)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@search",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{ @"keyword" : keyword,
                @"filter" : filter,
                @"sortBy" : sortBy,
                @"offset" : offset
                };
    [OONAFirBase logEventWithName:@"search" parameters:@{@"keyword":keyword}];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getEpisodeChannelListSearchKeyword:(NSString *)keyword Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"filter":@"search",
               @"keyword":keyword,
               @"searchType":@"episode"
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getEpisodeListWithMoodID:(NSString *)moodID andOffset:(NSString *)offset Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/-997/episode",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{
               @"moodId":moodID
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getEpisodeListSearchKeyword:(NSString *)keyword Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/-998/episode",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{
               @"keyword":keyword
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListSearchKeyword:(NSString *)keyword Success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"filter":@"search",
               @"keyword":keyword
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)recommendListWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/recommended",API_URL];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:nil
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)recentlyWatchSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/recently-watched",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    //    params = @{ @"keyword" : keyword };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)recentlyWatchWithOffset:(NSNumber *)offset  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/recently-watched",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"offset":offset};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getplaylistWithCategory:(NSString *)category success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/%@",API_URL,category];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    //params = @{@"channelId":channelID};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)getplaylistWithCategory:(NSString *)category AndOffset:(NSNumber *)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"offset":offset,
               @"categoryId":category
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getAllChannelInShuffleWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"shuffleAllowed":@"1"};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getAllChannelInShuffleAndOffset:(NSNumber *)offset WithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"shuffleAllowed":@"1",
               @"offset":offset
               };
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getCategoryListWithSuccess:(NSString *)type success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@categories",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"type" : type};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getShufPlayListWithCategoryID:(NSString *)CategoryID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"categoryId" : CategoryID,@"shuffleAllowed":@"1"};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getPlayListWithCategoryID:(NSString *)CategoryID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{ @"categoryId" : CategoryID };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getPlayListWithCategoryID:(NSString *)CategoryID AndOffset:(NSNumber* )offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@playlist/category",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{ @"categoryId" : CategoryID ,
                @"offset" : offset
                };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)getChatbotOptions:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@chatbot/options",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    //params = @{@"channelId":channelID};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:params
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getMoodListWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@moods",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    //params = @{@"channelId":channelID};
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:params
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithMoodID:(NSString *)moodID offset:(int)offset success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"filter":@"mood" , @"moodId" : moodID, @"offset" : @(offset) };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChannelListWithMoodID:(NSString *)moodID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/",API_URL];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"filter":@"mood" , @"moodId" : moodID };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getStreamingInfoWithChannelID:(NSString *)channelID  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/streaming-info",API_URL,channelID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)getEpisodeInfoWithChannelID:(NSString *)channelID episodeID:(NSString *)episodeID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/episode/%@/streaming-info",API_URL,channelID,episodeID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)ChatroomSessionUpdate:(NSString *)channelID  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/chatroom/session",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"action":@"update"};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)ChatroomSessionDelete:(NSString *)channelID  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/chatroom/session",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"action":@"delete"};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getChatMsgWithChannelID:(NSString *)channelID  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/chatroom/message",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    if (channelID!=nil)
        params = @{@"channelId":channelID};
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)getChatMsgWithChannelIDandMsgID:(NSString *)channelID MsgID:(NSString *)msgID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                                  failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/chatroom/message",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"channelId":channelID , @"firstMessageId" : msgID };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)sentChatMsgWithChannelID:(NSString *)channelID Text:(NSString *)text success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/chatroom/message/send",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    params = @{@"channelId":channelID , @"text" : text };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)getDisplayADWithChannelID:(NSString *)channelID spotXMode:(BOOL)spotXmode format:(NSString *)format success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/display-ads",API_URL,channelID];
    NSString *supportSpotX = @"1" ;
    if (!spotXmode)
        supportSpotX = @"0";
    
    NSDictionary *param = @{
                            @"latitude":[self GPSLat],
                            @"longitude":[self GPSLong],
                            @"supportSpotX" : supportSpotX,
                            @"intervalFormat" : format
                            };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getVideoADWithChannelID:(NSString *)channelID episodeID:(NSString *)episodeID platform:(NSString *)platform success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@channel/%@/video-ads",API_URL,channelID];
    if (episodeID != nil) {
        url = [NSString stringWithFormat:@"%@channel/%@/episode/%@/video-ads",API_URL,channelID,episodeID];
    }
    
    NSDictionary *param = @{
                            @"latitude":[self GPSLat],
                            @"longitude":[self GPSLong],
                            @"platform":platform
                            };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:param
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSString*)hardwareString {
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}
- (NSURLSessionDataTask *)registerDeviceConfirmCoverWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@register-device",API_URL];
    NSMutableDictionary *params = [self regDeviceParam].mutableCopy;
    [params setObject:@1 forKey:@"confirmSwitch"];
    
    NSLog(@"%@",params);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)registerDeviceWithFCMTokenSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@register-device",API_URL];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:[self regDeviceParam]
                                                           success:success
                                                           failure:failure];
    
    return dataTask ;
    
}
- (void)registerDeviceWithSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    if (!firebaseConfigFetched) {
        NSLog(@"need wait fir remote confg");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self registerDeviceWithSuccess:success failure:failure];
        });
        return ;
    }
    NSLog(@"calling parent function of reg device.");
    [[NetworkModule instance]registerDeviceWithFCMTokenSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"parent reg device response:%@",responseObject);
        
        if ([responseObject objectForKey:@"deviceId"] != nil)
            [UserData setDeviceID:[responseObject objectForKey:@"deviceId"]];
        
        if ([responseObject valueForKey:@"env"] != nil){
            haveNewEnv = YES;
            newEnv = [NSString stringWithFormat:@"%@", [responseObject valueForKey:@"env"]];
        }
        if ([responseObject valueForKey:@"apiUrl"] != nil){
            haveNewApiUrl = YES;
            newApiUrl = [NSString stringWithFormat:@"%@", [responseObject valueForKey:@"apiUrl"]];
        }
        if ([responseObject objectForKey:@"prefetchImages"]){
            NSMutableArray *pref = [responseObject objectForKey:@"prefetchImages"] ;
            NSMutableArray *arr = NSMutableArray.array ;
            for (int i = 0 ; i < pref.count ; i ++ ){
                [arr addObject:[NSURL URLWithString:pref[i]]];
            }
            NSLog(@"prefetch urls %@",arr);
            [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:arr];
            
        }
        NSString *enterRefCode = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"enteredReferralCode"]];
        if ([enterRefCode isEqualToString:@"1"]){
            NSLog(@"reg_device_referal_code_entered = 1");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"reg_device_referal_code_entered"];
        }else{
            NSLog(@"reg_device_referal_code_entered = 0");
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"reg_device_referal_code_entered"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMenu" object:nil];
        if (haveNewApiUrl || haveNewEnv){
            
            [self registerDeviceWithSuccess:success failure:failure];
            
        }else{
            
            NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
            NSString *status = [responseObject valueForKey:@"status"];
            [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
                
                
                if (isSuccess) {
                    // success
                    if ([responseObject objectForKey:@"smartphoneBidItemAvailable"] != nil){
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"smartphone_bid_available"];
                    }else{
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"smartphone_bid_available"];
                    }
                    
                    
                    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
                    [dateformat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
                    NSDate *serverDate = [dateformat dateFromString:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"currentTime"]]];
                    NSTimeInterval timeDiff = [serverDate timeIntervalSinceDate:[NSDate date]];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@(timeDiff) forKey:@"server_time_diff"];
                    [[NSUserDefaults standardUserDefaults] setObject:serverDate forKey:@"server_date"];
                    
                    [self handleNotifications:responseObject];
                    
                }
                else{
                    
                }
                
            }];
        }
        //        [[NSUserDefaults standardUserDefaults] setObject:@"2018-07-18 12:31:48"forKey:@"server_date_string"];
        
        
        
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"parent function of reg device error.");
        failure(task,error);
    }];
}
- (void)handleNotifications:(NSDictionary *)responseObject {
    BOOL needReload = NO;
    BOOL showNotice = NO;
    if ([responseObject[@"unreadNotification"] intValue] > 0){
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"notificationUnseen"];
        NSLog(@"un seen noti.");
        needReload = YES;
    }
    if (responseObject[@"bid"] != nil){
        if (responseObject[@"bid"][@"result"] != nil){
            [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"bid_result_need_show"];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"bid"][@"result"] forKey:@"bid_result_obj"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"haveBidSuccess" object:responseObject[@"bid"][@"result"]];
            
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:@"bid_result_need_show"];
        }
        
    }
    if (responseObject[@"notice"] != nil){
        
        if ([responseObject[@"notice"] isKindOfClass:[NSArray class]]){
            NSString *noticeID = [NSString stringWithFormat:@"%@",responseObject[@"notice"][0][@"id"]];
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"notice_id"] == nil){
                NSLog(@"no noticeid, show ");
                [[NSUserDefaults standardUserDefaults] setObject:noticeID forKey:@"notice_id"];
                showNotice = YES;
            }else{
                if ( ![noticeID isEqualToString: [[NSUserDefaults standardUserDefaults] objectForKey:@"notice_id"]] ){
                    NSLog(@"have noticeid but not same, need show and update id");
                    showNotice = YES;
                    [[NSUserDefaults standardUserDefaults] setObject:noticeID forKey:@"notice_id"];
                }else{
                    if (responseObject[@"notice"][0][@"id"] != nil){
                        showNotice = NO;
                        NSLog(@"already show this notice %@",noticeID);
                    }else{
                        showNotice = YES;
                        NSLog(@"notice id nil, just show and do nothing");
                    }
                }
                
            }
            
        }
    }
    if (responseObject[@"totalDailyNews"] != nil )
        [[NSUserDefaults standardUserDefaults] setObject:@([responseObject[@"totalDailyNews"] intValue]) forKey:@"total_daily_news_count"];
    
    if (responseObject[@"currentTime"] != nil) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"server_date_string"] == nil){
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"currentTime"] forKey:@"server_date_string"];
        }
        if (!([[[NSUserDefaults standardUserDefaults] objectForKey:@"total_daily_news_count"] intValue ] == 0 )){
            [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"dailyNewsUnseen"];
            
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:@"dailyNewsUnseen"];
        }
        
        if([UserData checkIfIsInSameDay:[[NSUserDefaults standardUserDefaults] objectForKey:@"server_date_string"] :responseObject[@"currentTime"]]){
            NSLog(@"on same day");
            
        }else{
            NSLog(@"not on same day");
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"currentTime"] forKey:@"server_date_string"];
            
            needReload = YES;
            
        }
    }
    if (showNotice){
        
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"notice_need_show"];
        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"notice"] forKey:@"notice_obj"];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"haveNotice"
         object:responseObject[@"notice"]];
        
    }else{
        
    }
    if (needReload){
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"reloadMenu"
         object:nil];
    }
}
- (NSURLSessionDataTask *)AddFavouriteChannelWithChannelID:(NSString *)channelID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@favourite/channel/%@",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] PUT:url
                                                       parameters:params
                                                          success:success
                                                          failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)RemoveFavouriteChannelWithChannelID:(NSString *)channelID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@favourite/channel/%@",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] DELETE:url
                                                          parameters:params
                                                             success:success
                                                             failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)AddShuffleChannelWithChannelID:(NSString *)channelID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@shuffle/channel/%@",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] PUT:url
                                                       parameters:params
                                                          success:success
                                                          failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)RemoveShuffleChannelWithChannelID:(NSString *)channelID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@shuffle/channel/%@",API_URL,channelID];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] DELETE:url
                                                          parameters:params
                                                             success:success
                                                             failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getTVODList:(NSString *)filter params:(NSDictionary *)params success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@",API_URL,filter];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)getTVODDetail:(NSString *)tvodID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@",API_URL,tvodID];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:@{}
                                                          success:success
                                                          failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)getTVODCategories:(NSString *)filter success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@/categories",API_URL,filter];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:@{}
                                                          success:success
                                                          failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)tvodKeywordSuggestionFilter:(NSString *)filter keyword:(NSString *)keyword success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@/search/suggestion",API_URL,filter];
    NSDictionary *params = [[NSMutableDictionary alloc]init];
    NSLog(@"tvod sugg url %@",url);
    params = @{ @"keyword" : keyword };
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)transactionPaymentWithMethod:(NSString *)paymentMethod action:(NSString *)action params:(NSDictionary *)params success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@transactions/payment/%@/%@",API_URL,paymentMethod,action];
    NSLog(@"%@",url);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)transactionStatusWithItemID:(NSString *)itemID success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@/payment-status",API_URL,itemID];
    NSLog(@"%@",url);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:@{}
                                                          success:success
                                                          failure:failure];
    return dataTask ;
}
- (NSURLSessionDataTask *)getTVODStreamingInfo:(NSString *)itemID  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@tvod/%@/streaming-info",API_URL,itemID];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (NSURLSessionDataTask *)getDailyNewsSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@news/daily",API_URL];
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
- (void)getCurrentLocation {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}

- (NSURLSessionDataTask *)AnalyticAPI:(NSString *)analyticType params:(NSDictionary *)params success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@analytics/event/%@",API_URL,analyticType];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:params
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}
- (void)refreshBgApi {
    for (int i=0 ;i < [UserData BgQueue].count ; i++){
        __block NSString *type = [NSString stringWithFormat:@"%@",[[UserData BgQueue][i] objectForKey:@"type"]]  ;
        __block NSDictionary *param = [[UserData BgQueue][i] objectForKey:@"param"] ;
        [self AnalyticAPI:type params:param success:^(NSURLSessionDataTask *task, id responseObject) {
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [UserData addToBgQueue:@{ @"type" : type, @"param" : param }];
        }];
        
    }
    [UserData cleanBgQueue];
}

- (NSURLSessionDataTask *)fetchDevicePreferenceSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@device-preference",API_URL];
    
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] POST:url
                                                        parameters:@{}
                                                           success:success
                                                           failure:failure];
    return dataTask ;
}


//CUSTOM NETWORK FUNCTION---------------------------------------------
- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(id)parameters
                      headers:(NSArray *)headers
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    manager.requestSerializer = [AFJSONRequestSerializer serializer] ;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:uuidString forHTTPHeaderField:@"uuid"];
    if (headers.count > 0) {
        for (NSDictionary *headerObject in headers) {
            [manager.requestSerializer setValue:[headerObject valueForKey:@"value"] forHTTPHeaderField:[headerObject valueForKey:@"key"]];
        }
    }
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] PUT:URLString
                                                               parameters:parameters
                                                                  success:success
                                                                  failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
                      parameters:(id)parameters
                         headers:(NSArray *)headers
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    manager.requestSerializer = [AFJSONRequestSerializer serializer] ;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:uuidString forHTTPHeaderField:@"uuid"];
    if (headers.count > 0) {
        for (NSDictionary *headerObject in headers) {
            [manager.requestSerializer setValue:[headerObject valueForKey:@"value"] forHTTPHeaderField:[headerObject valueForKey:@"key"]];
        }
    }
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] DELETE:URLString
                                                                  parameters:parameters
                                                                     success:success
                                                                     failure:failure];
    return dataTask;
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       headers:(NSArray *)headers
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSLog(@"UUID %@",uuidString);
    manager.requestSerializer = [AFJSONRequestSerializer serializer] ;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:uuidString forHTTPHeaderField:@"uuid"];
    //    [manager.requestSerializer setTimeoutInterval:10];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript", @"text/plain",nil];
    if (headers.count > 0) {
        for (NSDictionary *headerObject in headers) {
            [manager.requestSerializer setValue:[headerObject valueForKey:@"value"] forHTTPHeaderField:[headerObject valueForKey:@"key"]];
        }
    }
    NSLog(@"[OONA][API] POST URL : %@ \n Parameter : %@",URLString,parameters);
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] POST:URLString
                                                                parameters:parameters
                                                                  progress:nil
                                                                   success:success
                                                                   failure:failure];
    return dataTask;
}

- (void)showAlert : (NSDictionary *)alert {
    CustomPopup *popup = [[CustomPopup alloc]initWithMessage:alert[@"content"]];
    
    popup.info.scrollEnabled = NO ;
    popup.backBtn.hidden = YES ;
    NSArray * buttonArray = alert[@"buttons"] ;
    [popup setupZeroButton];
    if ( (buttonArray.count == 0) || (buttonArray = nil) ){
        popup.button1.hidden = YES;
        popup.button2.hidden = YES;
        [popup setupZeroButton];
    }
    if (buttonArray.count >= 1){
        
        [popup.button1 setTitle:alert[@"buttons"][0][@"label"] forState:UIControlStateNormal];
        __weak typeof(popup) weakPopup = popup;
        popup.completionButton1 = ^{
            if ([weakPopup respondsToSelector:(@selector(dismiss))]) {
                [weakPopup dismiss];
            }
            //            [self actionByUrl:[NSURL URLWithString:alert[@"buttons"][0][@"action"]]];
        };
        [popup setupOneButton];
        //    popup.button1.hidden = NO ;
        
    }
    if (buttonArray.count >= 2){
        
        [popup.button2 setTitle:alert[@"buttons"][1][@"label"] forState:UIControlStateNormal];
        __weak typeof(popup) weakPopup = popup;
        popup.completionButton2 = ^{
            if ([weakPopup respondsToSelector:(@selector(dismiss))]) {
                [weakPopup dismiss];
            }
            //            [self actionByUrl:[NSURL URLWithString:alert[@"buttons"][1][@"action"]]];
        };
        [popup setupTwoButton];
        //    popup.button1.hidden = NO ;
        
    }
    
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    [mainWindow addSubview:popup];
    
}
- (NSURLSessionDataTask *)GETMETHOD:(NSString *)URLString
                         parameters:(id)parameters
                            headers:(NSArray *)headers
                            success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer] ;
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:uuidString forHTTPHeaderField:@"uuid"];
    //    [manager.requestSerializer setTimeoutInterval:10];
    if (headers.count > 0) {
        for (NSDictionary *headerObject in headers) {
            [manager.requestSerializer setValue:[headerObject valueForKey:@"value"] forHTTPHeaderField:[headerObject valueForKey:@"key"]];
        }
    }
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", @"text/json", @"text/javascript", @"text/plain",nil];
    NSLog(@"[OONA][API] GET URL : %@ \n Parameter : %@",URLString,parameters);
    NSURLSessionDataTask *dataTask = [[self JsonSerializerWithBearer] GET:URLString
                                                               parameters:parameters
                                                                 progress:nil
                                                                  success:success
                                                                  failure:failure];
    
    return dataTask;
    
}

- (void)MULTIPARTFORM:(NSString *)url
               method:(NSString *)method
                files:(NSArray *)files
             filename:(NSString *)name
            fileparam:(NSString *)fileparam
           parameters:(NSDictionary *)parameters
              headers:(NSArray *)headers
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSError *error))failure{
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:method URLString:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (int i = 0 ; i<files.count; i++) {
            int timestamp = [[NSDate date] timeIntervalSince1970];
            NSString *imagename = [NSString stringWithFormat:@"%@-%d.jpg",name,timestamp];
            [formData appendPartWithFileData:[files objectAtIndex:i] name:fileparam fileName:imagename mimeType:@"image/png"];
            
        }
    } error:nil];
    
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    if (headers.count > 0) {
        for (NSDictionary *headerObject in headers) {
            [request setValue:[headerObject valueForKey:@"value"] forHTTPHeaderField:[headerObject valueForKey:@"key"]];
        }
    }
    
    [request setValue:uuidString forHTTPHeaderField:@"uuid"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //                          [progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          failure(error);
                      } else {
                          success(responseObject);
                      }
                  }];
    
    [uploadTask resume];
}
- (NSURLSessionDataTask *)getDetailBid:(NSString *)itemId success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@points/bid/item/%@",API_URL,itemId];
    NSLog(@"url getDetailBid = %@",url);
    NSURLSessionDataTask *dataTask =[[NetworkModule instance] GET:url
                                                       parameters:nil
                                                          success:success
                                                          failure:failure];
    return dataTask;
}
@end
