//
//  CheckSession.m
//  OONA
//
//  Created by OONA iOS on 11/13/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import "Constants.h"
#import "CheckSession.h"
#import "UserData.h"


@implementation CheckSession
/*
+ (void)checkSessionWithCompletion:(void (^)(BOOL isSessionValid))complete{
//    if ([UserData userToken] == nil || [[UserData userToken] isEqualToString:@""]) {
//        complete(NO);
//        return ;
//    }
    NSString *url = [NSString stringWithFormat:@"%@user-profile",API_URL];
    NSArray *headers = @[@{@"key":@"Authorization",@"value":[NSString stringWithFormat:@"Bearer %@",[UserData userToken]]}];
    [[NetworkModule instance] GETMETHOD:url parameters:nil headers:headers success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"load profile data:%@",responseObject);
        NSString *responseCode = [[responseObject valueForKey:@"code"] stringValue];
        NSString *status = [responseObject valueForKey:@"status"];
        
        [APIResponseCode checkResponseWithCode:responseCode WithStatus:status completion:^(NSString *message, BOOL isSuccess) {
            if (isSuccess) {
                if ([CheckSession checkLoginStatus]){
                    [UserData setUserFirstName:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"firstName"]]];
                    [UserData setUserLastName:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"lastName"]]];
                    [UserData setUserEmail:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"email"]]];
                    [UserData setUserGender:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"gender"]]];
                    [UserData setUserPhone:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"phone"]]];
                    [UserData setUserDateOfBirth:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"birthDate"]]];
                    [UserData setuserImageURL:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"imageUrl"]]];
                    [UserData setRankName:[[responseObject valueForKey:@"rank"] valueForKey:@"name"]];
                    if ([responseObject valueForKey:@"reward"]) {
                        [UserData setUserRewards:[responseObject valueForKey:@"reward"]];
                    }
                    [UserData setReferCode:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"inviteCode"]]];
                    [UserData setUserPoints:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"points"]]];
                    if ([responseObject valueForKey:@"accessToken"] != nil)
                        [UserData setUserToken: [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"accessToken"]]];
                    
                    if ([responseObject valueForKey:@"favouriteCategories"] != nil){
                        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"chatbot_user_tvcat_finished"];
                    }
                        complete(YES);
                } else{
                    [UserData setUserPoints:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"points"]]];
                    complete(NO);
                    
                }
                
            }
            else{
                complete(NO);
            }
        }];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        complete(NO);
    }];
}
 */
+ (BOOL)checkLoginStatus {
//    NSLog(@"Check login: %@",[UserData userToken]);
    if ([UserData userToken] == nil || [[UserData userToken] isEqualToString:@""]) {
        return NO;
    }else{
        return YES;
    }
    
}
@end
