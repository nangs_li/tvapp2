//
//  CheckSession.h
//  OONA
//
//  Created by OONA iOS on 11/13/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckSession : NSObject
//+ (void)checkSessionWithCompletion:(void (^)(BOOL isSessionValid))complete;
+ (BOOL)checkLoginStatus ;
@end
