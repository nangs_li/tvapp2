//
//  UserData.m
//  OONA
//
//  Created by OONA iOS on 11/2/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//
//#import "CheckSession.h"
#import "UserData.h"
@import FirebaseAnalytics;
@import Bugsnag;
@implementation UserData
static NSString *searchKeyword;
BOOL safeMode;

+ (NSString *)searchKeyword {
    if (searchKeyword == nil)
        return @"";
    
    return searchKeyword;
}
+ (void)setSearchKeyword:(NSString *)keyword {
    searchKeyword = keyword ;
}
+ (NSString *)userToken{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_token"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_token"]];
}
+ (void)skipAllBanner {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_share"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_referral"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"screenshot_explainer"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_youtube"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_sidemenu"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_live"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_vod"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_download"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"download_with_wifi"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_epg"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"home_channel_search"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fav_tips"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shuff_tips"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ad_tips"];
    
}
+ (void)enableSafeMode{
    safeMode = YES;
    [self skipAllBanner];
}
+ (void)disableSafeMode{
    safeMode = NO;
}
+ (BOOL)safeMode{
    return safeMode;
}


+ (int)userAge {
    int age = 0;
    if ([[UserData dateOfBirth] isEqualToString:@""]){
        return 0;
    }
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *dateOfBirth=[format dateFromString:[UserData dateOfBirth]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];
    
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
        age= [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        age= [dateComponentsNow year] - [dateComponentsBirth year];
    }

    return age;
}


+ (NSString *)userName {
    NSString *name = @"";
    if ([self checkLoginStatus]){
        name = [UserData firstName];
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"chatbot_user_name"] == nil){
            return @"Guest";
        }
        name = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"chatbot_user_name"] ];
    }
    
    return name;
}
+ (NSString *)userFullName {
    return [NSString stringWithFormat:@"%@%@",[UserData firstName],[UserData lastName]];
}
+ (NSString *)deviceID{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"device_id"] == nil) {
        return @"0";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"device_id"]];
}
+ (void)setDeviceID:(NSString *)deviceID{
    NSString *code = deviceID;
    
    if (deviceID == nil) {
        code = @"0";
    }
    [[Bugsnag configuration] setUser:code withName:@"" andEmail:@""];
    [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"device_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)referCode{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"refer_code"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"refer_code"]];
}

+ (void)setReferCode:(NSString *)referCode{
    NSString *code = @"";;
    if (referCode != nil) {
        code = referCode;
    }
    [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"refer_code"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSMutableArray *)BgQueue{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"background_queue"] == nil) {
        return @[].mutableCopy;
    }
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"background_queue"];
}
+ (void)cleanBgQueue{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"background_queue"];
}
+ (void)addToBgQueue:(NSDictionary *)newItem{
    NSMutableArray *tempArr = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"background_queue"]];
    [tempArr addObject:newItem];
    [[NSUserDefaults standardUserDefaults] setObject:tempArr forKey:@"background_queue"];
}
+ (NSString *)lastChannelID{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"last_channel_id"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"last_channel_id"]];
}

+ (void)setLastChannelID:(NSString *)lastChannelID{
    NSString *chID = @"";;
    if (lastChannelID != nil) {
        chID = lastChannelID;
    }
    [[NSUserDefaults standardUserDefaults] setObject:chID forKey:@"last_channel_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)lastEpisodeID{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"last_episode_id"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"last_episode_id"]];
}

+ (void)setLastEpisodeID:(NSString *)lastEpisodeID{
    NSString *epID = @"";;
    if (lastEpisodeID != nil) {
        epID = lastEpisodeID;
    }
    [[NSUserDefaults standardUserDefaults] setObject:epID forKey:@"last_episode_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)currentEpisodeID{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"current_episode_id"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"current_episode_id"]];
}

+ (void)setCurrentEpisodeID:(NSString *)currentEpisodeID{
    NSString *epID = @"";;
    if (currentEpisodeID != nil) {
        epID = currentEpisodeID;
    }
    [[NSUserDefaults standardUserDefaults] setObject:epID forKey:@"current_episode_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)notificationID{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"notificationID"] == nil) {
        return nil;
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"notificationID"]];
}
+ (void)setNotificationID :(NSString *)notiID {
    NSString *notiString = nil;
    if (notiID != nil) {
        notiString = notiID;
    }
    [[NSUserDefaults standardUserDefaults] setObject:notiString forKey:@"notificationID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)userID{
    if (![self checkLoginStatus]){
        return @"0";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"]];
}

+ (NSString *)firstName{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_first_name"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_first_name"]];
}

+ (NSString *)lastName{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_last_name"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_last_name"]];
}
+ (NSString *)email{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_email"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_email"]];
}


+ (NSString *)phoneNumber{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_phone"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_phone"]];
}


+ (NSString *)imageURL{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_image-url"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_image-url"]];
}

+ (NSString *)dateOfBirth{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_date_of_birth"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_date_of_birth"]];
}

+ (NSString *)gender{
    if ([self checkLoginStatus]){
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_gender"] == nil) {
            return @"";
        }
        
        return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_gender"]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"chatbot_user_gender"] == nil) {
            return @"";
        }
        
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"chatbot_user_gender"]];
    
        
}
+ (NSString *)genderString {
    if ([[self gender] isEqualToString:@""]) {
        return @"";
    }
    
    if ([[self gender] isEqualToString:@"M"]){
        return [NSString stringWithFormat:@"%@",@"male"];
    }else{
        return [NSString stringWithFormat:@"%@",@"female"];
    }
    
}
+ (NSString *)rankName{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_rank_name"] == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_rank_name"]];
}

+ (NSString *)userPoints{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"] == nil) {
        return @"0";
    }
    NSString *pt = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
    if ([pt isEqualToString:@""]) {
        return @"0";
    }
    NSString *prices =  [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
    NSNumberFormatter * formatter = [NSNumberFormatter new];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * priceString =  [formatter stringFromNumber:[NSNumber numberWithInteger:[prices integerValue]]];
//    priceString = [priceString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return priceString;
//    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
}
+ (NSString *)userPointsString{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"] == nil) {
        return @"-";
    }
    NSString *pt = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
    if ([pt isEqualToString:@""]) {
        return @"-";
    }
    NSString *prices =  [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
    NSNumberFormatter * formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString * priceString =  [formatter stringFromNumber:[NSNumber numberWithInteger:[prices integerValue]]];
//        priceString = [priceString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    return priceString;
    //    return [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"user_points"]];
}
+ (NSDictionary *)userReward{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_reward"] == nil) {
        return nil;
    }
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"user_reward"];
}

+ (BOOL)isLoginByEmail{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"user_login_email"] == nil) {
        return NO;
    }
    return [[[NSUserDefaults standardUserDefaults] valueForKey:@"user_login_email"] boolValue];
    
}
+ (NSString *)appLanguage{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"selected_language"] == nil) {
        return nil;
    }
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"selected_language"];
    
}

+ (void)setAppLanguage:(NSString *)appLang{
    NSString *langStr = @"";
    if (appLang != nil) {
        langStr = appLang;
    }
    [[NSUserDefaults standardUserDefaults] setObject:langStr forKey:@"selected_language"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setUserToken:(NSString *)token{
    NSString *tokenString = @"";;
    if (token != nil) {
        tokenString = token;
    }
    [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:@"user_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setUserID:(NSString *)userID{
    NSLog(@"set userid : %@",userID);
    NSString *userIDString = @"";;
    if (userID == nil) {
        userIDString = userID;
        return;
    }
    [FIRAnalytics setUserID:userID];
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setUserFirstName:(NSString *)name{
    NSString *nameString = @"";
    if (name != nil) {
        nameString = name;
    }
    [[NSUserDefaults standardUserDefaults] setObject:nameString forKey:@"user_first_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserLastName:(NSString *)name{
    NSString *nameString = @"";;
    if (name != nil) {
        nameString = name;
    }
    [[NSUserDefaults standardUserDefaults] setObject:nameString forKey:@"user_last_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (void)setUserEmail:(NSString *)email{
    NSString *nameString = @"";;
    if (email != nil) {
        nameString = email;
    }
    [[NSUserDefaults standardUserDefaults] setObject:nameString forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (void)setUserGender:(NSString *)gender{
    
    NSString *genderString = @"";;
    if (gender != nil) {
        genderString = gender;
    }
    [[NSUserDefaults standardUserDefaults] setObject:genderString forKey:@"user_gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserPhone:(NSString *)phone{
    NSString *phoneString = @"";;
    if (phone != nil) {
        phoneString = phone;
    }
    [[NSUserDefaults standardUserDefaults] setObject:phoneString forKey:@"user_phone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserDateOfBirth:(NSString *)date{
    NSString *dateString = @"";;
    if (date != nil) {
        dateString = date;
    }
    [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:@"user_date_of_birth"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setuserImageURL:(NSString *)url{
    NSString *urlString = @"";;
    if (url != nil) {
        urlString = url;
    }
    [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:@"user_image-url"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setRankName:(NSString *)rank{
    NSString *rankString = @"";;
    if (rank != nil) {
        rankString = rank;
    }
    [[NSUserDefaults standardUserDefaults] setObject:rankString forKey:@"user_rank_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserPoints:(NSString *)points{
    NSLog(@"set tcoin%@",points);
    NSString *pointsString = @"";
    if (points != nil) {
        pointsString = points;
    }
    [[NSUserDefaults standardUserDefaults] setObject:pointsString forKey:@"user_points"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserRewards:(NSDictionary *)rewards{
    NSDictionary *rewardsObject = @{};;
    if (rewards != nil) {
        rewardsObject = rewards;
    }
    [[NSUserDefaults standardUserDefaults] setObject:rewardsObject forKey:@"user_reward"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setLoginByEmail:(BOOL)isByEmail{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isByEmail] forKey:@"user_login_email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*
+ (void)clearUserData{
//    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"Chatbot_Finished"];
    [OONAFirBase logEventWithName:@"clear_token" parameters:@{}];
    [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:@"signin_reminder_stage"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_token"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_first_name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_last_name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_gender"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_phone"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_date_of_birth"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_image-url"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_rank_name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"user_points"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_login_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_reward"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"last_channel_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"last_episode_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"current_episode_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
 */
+ (void)recordEpisodelastProgress:(float)progress chID:(NSString *)chID epID:(NSString *)epID{
//    NSLog(@"set %f ,ch %@,ep %@",progress,chID,epID);
    if (epID == nil)
        return;
    if (chID == nil)
        return;
    
    NSMutableDictionary *history = [[NSMutableDictionary alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults]  objectForKey:@"last_watch_progress"]];
    if (history == nil){
        history =  @{}.mutableCopy;
    }
    NSMutableDictionary *chHistory = [[NSMutableDictionary alloc]initWithDictionary:[history objectForKey:chID]];
    if (chHistory == nil){
        chHistory = @{}.mutableCopy;
    }
    [chHistory setObject:@(progress) forKey:epID];
    [history setObject:chHistory forKey:chID];
//    NSLog(@"record %@",history);
    [[NSUserDefaults standardUserDefaults] setObject:history forKey:@"last_watch_progress"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (float)getEpisodelastProgressByChID:(NSString *)chID epID:(NSString *)epID{
    float progress ;
    
    NSMutableDictionary *history = [[NSUserDefaults standardUserDefaults]  objectForKey:@"last_watch_progress"];
    if (history == nil){
        return 0;
    }
    NSMutableDictionary *chHistory = [history objectForKey:chID];
    
    
    progress = [[chHistory objectForKey:epID] floatValue];
    
    if (chHistory == nil)
        return 0;
    
    if ([chHistory objectForKey:epID] == nil)
        return 0;
    
    return progress;
}
+ (NSString *)getShareUrl {
    NSString *str = [NSString stringWithFormat:@"%@%@",@"Download%20OONA%20app%20now%2C%20and%20use%20my%20referral%20code%20to%20start%20earning%20tcoins%20rewards!%0AURL%20%3A%20https%3A%2F%2Foona.tv%2Fdownload-now%2F%0AMy%20Referral%20Code%3A",[UserData referCode]];
    return str ;
    
}
+ (NSString *)getShareText {
    
    NSString *str = [NSString stringWithFormat:@"%@%@",@"Download OONA app now, and use my referral code to start earning tcoins rewards!\nURL : https://oona.tv/download-now/\nMy Referral Code: ",[UserData referCode]];
    return str ;
    
}
+ (void)clearUserRewards{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_reward"];
}
+ (BOOL)checkIfIsInSameDay:(NSString *)stringDate1 :(NSString *)stringDate2 {
    
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date1 = [dateFormat dateFromString:stringDate1];
        NSDate *date2 = [dateFormat dateFromString:stringDate2];
    
        return [[NSCalendar currentCalendar] isDate:date1 inSameDayAsDate:date2];;
    
}
+(BOOL)checkNameExist {
    NSLog(@"check user name:%@",[UserData userName]);
    if ([self checkLoginStatus]){
        NSLog(@"n1");
        return YES;
    }
    
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chatbot_Finished"] boolValue]) {
    //        NSLog(@"n2");
    //        return YES;
    //    }
    
    if ([@"" isEqualToString:[UserData userName]]){
        NSLog(@"n3");
        return NO;
    }
    if ([@"Guest" isEqualToString:[UserData userName]]){
        NSLog(@"n4");
        return NO;
    }
    NSLog(@"n5");
    
    return YES;
}

+(BOOL)checkGenderExist {
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chatbot_Finished"] boolValue]) {
    //        NSLog(@"n2");
    //        return YES;
    //    }
    
    if ([self checkLoginStatus]){
        if ([@"" isEqualToString:[UserData gender]]){
            return NO;
        }
        
        //        return YES;
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"chatbot_user_gender_finished"] == YES)
        return YES;
    
    //    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Chatbot_Finished"]) {
    //        return YES;
    //    }
    return NO;
    
}

+(BOOL)checkTVCatExist {
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chatbot_Finished"] boolValue]) {
    //        NSLog(@"n2");
    //        return YES;
    //    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"chatbot_user_tvcat_finished"] boolValue] == YES){
        return YES;
    }
    return NO;
}

+(BOOL)checkbirthExist {
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Chatbot_Finished"] boolValue]) {
    //        NSLog(@"n2");
    //        return YES;
    //    }
    
    if ([self checkLoginStatus]){
        if ([@"" isEqualToString:[UserData dateOfBirth]]){
            return NO;
        }
        else
            return YES;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"chatbot_user_birth_finished"] boolValue] == YES){
        return YES;
    }
    return NO;
}
+ (int)chatbotFlowCurrentStage {
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"third_stage_chatbot_done"] == YES){
        return 3;
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"second_stage_chatbot_done"] == YES){
        return 2;
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"first_stage_chatbot_done"] == YES){
        return 1;
    }
    return 0;
}
+ (BOOL)isRegQuestionAllFinished {
    return ([self checkNameExist] && [self checkbirthExist] && [self checkTVCatExist] && [self checkGenderExist]);
}

/*mirgrate to here*/

+ (BOOL)checkLoginStatus {
    //    NSLog(@"Check login: %@",[UserData userToken]);
    if ([UserData userToken] == nil || [[UserData userToken] isEqualToString:@""]) {
        return NO;
    }else{
        return YES;
    }
    
}

@end
