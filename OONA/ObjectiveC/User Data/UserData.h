//
//  UserData.h
//  OONA
//
//  Created by OONA iOS on 11/2/17.
//  Copyright © 2017 BOOSST. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject
+ (void)enableSafeMode;
+ (void)disableSafeMode;
+ (BOOL)safeMode;
+ (NSString *)searchKeyword;
+ (void)setSearchKeyword:(NSString *)keyword;
+ (NSString *)deviceID;
+ (NSString *)email;
+ (NSString *)userToken;
+ (NSString *)userID;
+ (NSString *)firstName;
+ (NSString *)lastName;
+ (NSString *)phoneNumber;
+ (NSString *)imageURL;
+ (NSString *)dateOfBirth;
+ (NSString *)gender;
+ (NSString *)genderString ;
+ (NSString *)rankName;
+ (NSString *)userPoints;
+ (NSString *)userPointsString;
+ (NSDictionary *)userReward;
+ (NSString *)notificationID;
+ (BOOL)isLoginByEmail;
+ (NSString *)appLanguage;
+ (NSString *)lastChannelID;
+ (void)setLastChannelID:(NSString *)lastChannelID;
+ (NSString *)lastEpisodeID;
+ (void)setLastEpisodeID:(NSString *)lastEpisodeID;
+ (NSString *)currentEpisodeID;
+ (void)setReferCode:(NSString *)referCode;
+ (NSString *)referCode;
+ (void)setDeviceID:(NSString *)deviceID;
+ (void)setCurrentEpisodeID:(NSString *)currentEpisodeID;
+ (void)setNotificationID :(NSString *)notiID ;
+ (void)setAppLanguage:(NSString *)appLang;
+ (void)setUserToken:(NSString *)token;
+ (void)setUserID:(NSString *)userID;
+ (NSString *)userFullName;
+ (void)setUserFirstName:(NSString *)name;
+ (void)setUserLastName:(NSString *)name;
+ (void)setUserEmail:(NSString *)email;
+ (void)setUserGender:(NSString *)gender;
+ (void)setUserPhone:(NSString *)phone;
+ (void)setUserDateOfBirth:(NSString *)date;
+ (void)setuserImageURL:(NSString *)url;
+ (void)setRankName:(NSString *)rank;
+ (void)setUserPoints:(NSString *)points;
+ (void)setLoginByEmail:(BOOL)isByEmail;
+ (void)setUserRewards:(NSDictionary *)rewards;
+ (void)addToBgQueue:(NSDictionary *)newItem;
+ (void)cleanBgQueue;
+ (NSMutableArray *)BgQueue;
+ (void)clearUserData;
+ (void)clearUserRewards;
+ (NSString *)userName;
+ (NSString *)getShareText ;
+ (NSString *)getShareUrl ;
+ (void)recordEpisodelastProgress:(float)progress chID:(NSString *)chID epID:(NSString *)epID;
+ (float)getEpisodelastProgressByChID:(NSString *)chID epID:(NSString *)epID;
+ (int)userAge ;
+ (BOOL)checkIfIsInSameDay:(NSString *)stringDate1 :(NSString *)stringDate2;
+ (BOOL)checkNameExist;
+ (BOOL)checkGenderExist;
+ (BOOL)checkTVCatExist;
+ (BOOL)checkbirthExist;
+ (BOOL)isRegQuestionAllFinished;
+ (int)chatbotFlowCurrentStage;
@end
