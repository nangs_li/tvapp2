//
//  OONAFirBase.m
//  OONA
//
//  Created by jack on 11/4/2018.
//  Copyright © 2018 BOOSST. All rights reserved.
//
#import "OONAFirBase.h"
//#import "UserData.h"


@implementation OONAFirBase{
   
}
static NSMutableArray *_events;
static FIRRemoteConfig *_remoteConfig;
static BOOL firbaseConfigFetchSuccess;
static BOOL sending ;
NSString *const kEnableAnalyticApiKey = @"enableAnalyticsApi";
NSString *const kAlternateAnalyticApiUrlKey = @"alternativeAnalyticsApiUrl";
static unsigned int const queueSendCount = 50 ;
static dispatch_once_t onceToken;
static  long expirationDuration ;
+ (void)initService {
    _remoteConfig = [FIRRemoteConfig remoteConfig];
    
    
    [_remoteConfig setDefaults:@{kEnableAnalyticApiKey : @YES}];
    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
    _remoteConfig.configSettings = remoteConfigSettings;
    expirationDuration = 3600;
    // If your app is using developer mode, expirationDuration is set to 0, so each fetch will
    // retrieve values from the Remote Config service.
    if (_remoteConfig.configSettings.isDeveloperModeEnabled) {
        expirationDuration = 0;
    }
    //TODO: make it work again
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnalyticsQueue)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];*/
    [self restoreSystemAnalyticQueue];
}
/*
+ (void)logEventWithName:(NSString *)name parameters:(nullable NSDictionary<NSString *,id> *)parameters{
    
    dispatch_once(&onceToken, ^{
        [self initService];
    });
    
    
    NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSMutableDictionary *params = parameters.mutableCopy;
    [FIRAnalytics setUserPropertyString:[UserData deviceID] forName:@"device_id"];
    [FIRAnalytics setUserPropertyString:[UserData gender] forName:@"user_gender"];
    [FIRAnalytics setUserPropertyString:[UserData dateOfBirth] forName:@"user_birthdate"];
    [FIRAnalytics setUserPropertyString:[NSString stringWithFormat:@"%d",[UserData userAge]] forName:@"user_age"];
    [FIRAnalytics setUserPropertyString:APP_ENV forName:@"env"];
    [FIRAnalytics setUserPropertyString:uuidString forName:@"uuid"];
    
    NSLog(@"%@,%@,%@,%@,%d",[UserData userID],[UserData deviceID],[UserData gender],[UserData dateOfBirth],[UserData userAge]);
    
    
    [params setObject:[NSString stringWithFormat:@"%@",APP_ENV] forKey:@"env"];
    NSLog(@"OONA Firebase log event name: %@\n paramater: %@",name,params);
//    [self.remoteConfig setDefaultsetting];
//    self.re
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"demoApp"]){
        if ([APP_ENV isEqualToString:@"prod"]){
            NSLog(@"prod demo app, not send Firbase event.");
            return;
        }
    }
    NSMutableDictionary *ApiParams = parameters.mutableCopy;
    [ApiParams setObject:[UserData deviceID] forKey:@"device_id"];
    [ApiParams setObject:[UserData gender] forKey:@"user_gender"];
    [ApiParams setObject:[UserData dateOfBirth] forKey:@"user_birthdate"];
    [ApiParams setObject:[NSString stringWithFormat:@"%d",[UserData userAge]] forKey:@"user_age"];
    
    if (![[UserData userID] isEqualToString:@"0"] )
    if (![[UserData userID] isEqualToString:@""] )
    if ([UserData userID] != nil )
        [ApiParams setObject:[UserData userID] forKey:@"user_id"];
    
    
    
    
    
    [_remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            NSLog(@"Config fetched!");
            firbaseConfigFetchSuccess = YES;
            [_remoteConfig activateFetched];
        } else {
            NSLog(@"Config not fetched");
            firbaseConfigFetchSuccess = NO;
            NSLog(@"Error %@", error.localizedDescription);
        }
//        [self logEventApiWithName:name  parameters:ApiParams];
       
    }];
    [self addToAnalyticsQueueNamed:name param:ApiParams];
    [FIRAnalytics logEventWithName:name parameters:params];
    
}
+ (void)fetchRemoteConfig {
  
    [_remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            NSLog(@"Config fetched!");
            firbaseConfigFetchSuccess = YES;
            [_remoteConfig activateFetched];
        } else {
            NSLog(@"Config not fetched");
            firbaseConfigFetchSuccess = NO;
            NSLog(@"Error %@", error.localizedDescription);
        }
        //        [self logEventApiWithName:name  parameters:ApiParams];
        
    }];
}
//+ (void)logEventApiWithName:(NSString *)name parameters:(nullable NSDictionary<NSString *,id> *)parameters{
//    if ([name isEqualToString:@"display_ad_view"] ||
//        [name isEqualToString:@"display_ad_click"] ||
//        [name isEqualToString:@"display_ad_engagement_view"] ||
//        [name isEqualToString:@"youtube_video_view"]  ||
//        [name isEqualToString:@"display_ad_ignored"]){
//
//        return;
//    }
//
//
//    if (firbaseConfigFetchSuccess){
//        if (!_remoteConfig[kEnableAnalyticApiKey].boolValue) {
//            NSLog(@"ANALYTIC API DISABLED");
//            return ;
//        }
//        if (![_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue isEqualToString:@""]){
//            NSLog(@"OONA API alt URL: %@ Analytic name: %@, param: %@",_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue,name,parameters);
//
//            [[NetworkModule instance]sendAnalyticswithAlternateUrl:_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue named:name parameter:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//                NSLog(@"API alt Analytics api success");
//            } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                NSLog(@"API alt Analytics api fail");
//            }];
//            return ;
//        }
//    }
//
//    NSLog(@"OONA API Analytic name: %@, param: %@",name,parameters);
//
//    [[NetworkModule instance]sendAnalyticsNamed:name parameter:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"API Analytics api success");
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"API Analytics api fail");
//    }];
//}
+ (void)addToAnalyticsQueueNamed:(NSString *)taskName param:(NSDictionary *)taskParam {
    if (_events == nil){
        _events = NSMutableArray.array;
    }
    [_events addObject:@{@"timestamp":[[NetworkModule instance]getTimeStampNow],
                             @"event":taskName,
                             @"parameters":taskParam}];
    if (_events.count >= queueSendCount){
        NSLog(@"exceed queue length.");
        [self sendAnalyticsQueue];
    }
    
//    NSLog(@"analytics event arr : %@\n",_events);
    [self restoreSystemAnalyticQueue];
}

- (void)sendAnalyticsQueue {
    NSLog(@"trigger send");
    if (sending){
        NSLog(@"sending ");
        return;
    }
    if ( (_events == nil) || (_events.count == 0) ){
        NSLog(@"no ev send");
        return;
    }
    sending = YES;
    
    NSMutableDictionary *param ;
    __block long eventCount = _events.count ;
    
    param = @{
              @"events":_events,
              @"device" : @{
                      @"deviceId" : [UserData deviceID],
                      @"deviceOS" : @"iOS",
                      @"deviceModel" : [NetworkModule hardwareString],
                      },
              @"user":@{
                      @"userId":[UserData userID],
                      @"age":@([UserData userAge]),
                      @"gender" : [UserData gender],
                      }
              
              }.mutableCopy;
    
    if (firbaseConfigFetchSuccess){
        if (!_remoteConfig[kEnableAnalyticApiKey].boolValue) {
            NSLog(@"ANALYTIC API DISABLED");
            return ;
        }
        if (![_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue isEqualToString:@""]){
//            NSLog(@"OONA API alt URL: %@ Analytic name: %@, param: %@",_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue,name,parameters);
        
            [[NetworkModule instance]sendAnalyticsQueueWithAlternateUrl:_remoteConfig[kAlternateAnalyticApiUrlKey].stringValue param:param success:^(NSURLSessionDataTask *task, id responseObject) {
                NSLog(@"API alt Analytics queue api success");
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"API alt Analytics api fail");
            }];
            return ;
        }
    }
//    NSLog(@"send analytic queue: %@",param);
    [[NetworkModule instance]sendAnalyticsQueue:param success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"analytic queue res : %@",responseObject);
        NSRange r;
        r.location = 0;
        r.length = eventCount;
        [_events removeObjectsInRange:r];
        //[self syncAnalyticQueueToSystems];
        
        [self syncAnalyticQueueToSystems];
        sending = NO;
        NSLog(@"remainning queue: %@",_events);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        sending = NO;
    }];
}*/
+ (void)restoreSystemAnalyticQueue {
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"analytic_queue"] == nil){
        _events = [[NSMutableArray alloc]init];
        return ;
    }
    _events = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"analytic_queue"]];
}/*

- (void)syncAnalyticQueueToSystems {
    [[NSUserDefaults standardUserDefaults] setObject:_events forKey:@"analytic_queue"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}*/

@end

