//
//  OONAFirBase.h
//  
//
//  Created by jack on 11/4/2018.
//

@import Firebase;
@import FirebaseAnalytics;
@import FirebaseRemoteConfig;
#import "Constants.h"

@interface OONAFirBase : NSObject

@property (class, retain) NSMutableArray *events;


+ (void)logEventWithName:(NSString *)name parameters:(nullable NSDictionary<NSString *,id> *)parameters;
+ (void)sendAnalyticsQueue;
@end

