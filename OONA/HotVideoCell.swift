//
//  HotVideoCell.swift
//  OONA
//
//  Created by Jack on 14/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import FSPagerView

class HotVideoCell: FSPagerViewCell {
    @IBOutlet weak var img: UIImageView!{
        didSet {
            img?.sd_setImage(with: URL.init(string: "https://placekitten.com/600/400") , completed: nil)
        }
    }
    
    @IBOutlet weak var watchNowButton: UIButton!{
        didSet{
            self.watchNowButton.layer.cornerRadius = self.watchNowButton.frame.size.height/2
        }
    }
    override func awakeFromNib() {
        
    }
}
