//
//  PreviewViewController+Youtube.swift
//  OONA
//
//  Created by nicholas on 7/29/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import YoutubePlayer_in_WKWebView
let defaultPlayerVars : [String : Any] = [
    "controls" : 0,
    "playsinline" : 1,
    "autohide" : 1,
    "showinfo" : 0,
    "modestbranding" : 1,
    "origin" : "http://www.youtube.com"
]

extension PreviewViewController {
    
    func prepareYoutubeView() {
        self.previewYoutubeView.isHidden = true
        previewYoutubeView.delegate = self
    }
    
    func playerViewPreferredWebViewBackgroundColor(_ playerView: WKYTPlayerView) -> UIColor {
        return UIColor.clear
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        print("become rdy")
        if (currentVideoMode == .youtube){
            previewYoutubeView.playVideo()
        }
//        previewYoutubeView.playVideo()
    }
    
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        self.youtubePlayerChangeToState(state:state)
    }
    
    func youtubePlayerChangeToState(state: WKYTPlayerState) {
        self.currentWKYTplayerState = state
//        print("YouTube did change to state", state.rawValue)
        switch state{
//        case .paused:
//            if self.progressBarBeingSliding  {
//                self.playButton.imageView?.image = UIImage.init(named: "pause")
//            }else {
//                self.playButton.imageView?.image = UIImage.init(named: "play")
//            }
//        case .ended:
//            //            self.youtubePlayerView.seek(toSeconds: 0, allowSeekAhead: true)
//            if (currentPlayer == .youtube) {
//                self.playNextVideo()
//            }
//        case .playing:
//            self.playButton.imageView?.image = UIImage.init(named: "pause")
//            self.videoPlaying()
        default:
//            print("YT default case")
            break
        }
        
        if (state == .playing) {
            self.youtubeLoadingBg.alpha = 0
            self.youtubeLoadingGif.alpha = 0
            self.youtubeLoadingGif.stopAnimatingGif()
            self.previewViewWrapper.cancelLoading()
        } else {
            self.youtubeLoadingBg.alpha = 1
            self.youtubeLoadingGif.alpha = 1
            self.youtubeLoadingGif.startAnimatingGif()
        }
    }
    
    func playerView(_ playerView: WKYTPlayerView, didPlayTime playTime: Float) {
        //        print("YT didPlayTime",playTime)
        //        print("YT current PlayTime",playerView.currentTime())
        playerView.getDuration { (duration, error) in
            self.previewYoutubeView.getPlayerState({ (state, error) in
                self.youtubePlayerChangeToState(state:state)
            })
        }
        
    }
    
//    func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
//        self.previewViewWrapper.cancelLoading()
//        print("YouTube Error: \(error.rawValue)")
//    }
    
    func playVideo(youtubeID: String) {
        let playerVars = defaultPlayerVars
        print("youtubeID: \(youtubeID), player state: \(self.currentWKYTplayerState.rawValue)")
        currentVideoId = youtubeID
        if (self.currentVideoId == youtubeID) {
            if currentYoutubeID != youtubeID {
                currentYoutubeID = youtubeID
                previewYoutubeView.stopVideo()
                //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.previewYoutubeView.load(withVideoId: youtubeID, playerVars: playerVars)
                self.previewYoutubeView.delegate = self
                //}
                showPreviewLayout(mode: .youtube)
                previewViewWrapper.isHidden = false
            }
        }
    }
}
