//
//  PreviewViewController.swift
//  OONA
//
//  Created by nicholas on 7/29/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BrightcovePlayerSDK
import YoutubePlayer_in_WKWebView
import MBProgressHUD
import SwiftyJSON


enum VideoOwner {
    case unknown
    case channel
    case episode
}

class PreviewModel: NSObject {
    var title: String?
    var subtitle: String?
    var previewDescription: String?
    var videoOwner: VideoOwner = .unknown
    var video: Video?
    
    override init() {
        super.init()
    }
    
    convenience init(title: String?,
                     subtitle: String?,
                     description: String?,
                     owner: VideoOwner,
                     video: Video?) {
        self.init()
        if let _title = title { self.title = _title }
        if let _subtitle = subtitle { self.subtitle = _subtitle }
        if let _description = description { self.previewDescription = _description }
        if let _video = video { self.video = _video }
        self.videoOwner = owner
    }
}

class PreviewViewController: BaseViewController, BCOVPlaybackControllerDelegate, WKYTPlayerViewDelegate {
    
    var contentView: UIView?
    
    var currentYoutubeID: String?
    var currentVideoId: String?
    
    var currentWKYTplayerState: WKYTPlayerState = .unknown
    
    let manager: BCOVPlayerSDKManager = BCOVPlayerSDKManager.shared()
    var playbackService: BCOVPlaybackService?
    var playbackController: BCOVPlaybackController?
    var fairplayProxy : BCOVFPSBrightcoveAuthProxy?
    
    var previewItem: BehaviorRelay<PreviewModel?> = BehaviorRelay<PreviewModel?>(value: nil)
    
    var selectedItem: PublishRelay<PreviewModel?> = PublishRelay<PreviewModel?>()
    
    var previewPlayer : AVPlayer?
    var previewLayer : AVPlayerLayer?
    
    var currentVideoMode: VideoMode = .none
    
    /**player views section**/
    @IBOutlet weak var previewViewWrapper: UIView!
    @IBOutlet weak var previewVideoView: UIView!
    
    @IBOutlet weak var previewYouTubeWrapper: UIView!
    @IBOutlet weak var previewYoutubeView: WKYTPlayerView!
    @IBOutlet weak var youtubeLoadingGif: UIImageView!
    @IBOutlet weak var youtubeLoadingBg: UIImageView!
    
    /**description section**/
    @IBOutlet weak var descriptionWrapper: UIView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionSubTitle: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var videoTypeIndicator: UIImageView!
    
    @IBOutlet weak var descriptionViewLeading: NSLayoutConstraint!
    
    var registeredObservers: Array<String> = Array()
    let AVPlayerKVOKeyStatus = "status"
    let AVPlayerKVOKeyTimeControlStatus = "timeControlStatus"
    let AVPlayerKVOKeyRate = "rate"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        DispatchQueue.global(qos: .userInteractive).async(execute: { [weak self] in
//            var images: [UIImage] = [UIImage]()
//            for i in 1...8 {
//                if let image = UIImage(named: "youtube_loading_\(i)") { images.append(image) }
//            } // use of #imageLiterals here
//            let animation = UIImage.animatedImage(with: images, duration: 3)
//            DispatchQueue.main.async(execute: {
//                self?.youtubeLoadingGif.image = animation
//            })
//        })

        loadAllViews()
        
        self.previewItem.subscribe(onNext: { [weak self] (previewItem) in
            guard let _previewItem = previewItem,
                let _video = _previewItem.video else {
                    self?.hideDescription()
                    self?.hidePreviewWrapper()
                    self?.stopPreview()
                    self?.currentVideoMode = .none
                    return
            }
            if let _ = self?.playPreview(with: _video) {
                self?.previewViewWrapper.startLoading()
            }
            self?.loadDescription()
        }).disposed(by: self.disposeBag)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopPreview()
        self.previewViewWrapper.cancelLoading()
    }
    
    @IBAction func clickPreview(_ sender: UIButton) {
        self.stopPreview()
        self.selectedItem.accept(self.previewItem.value)
//        prepareToSendVideo()
    }
    
//    func prepareToSendVideo() {
//        if let item = self.previewItem.value,
//            let video = item.video {
//            var urlString: String?
//
//            switch item.videoOwner {
//            case .channel:
//                urlString = APPURL.StreamingInfo(channelId: String(video.channelId))
//            case .episode:
//                urlString = APPURL.StreamingInfo(channelId: String(video.channelId), episodeId: String(video.episodeId))
//            default:
//                break
//            }
//
//            guard let _ = urlString else { return }
//
//            OONAApi.shared.postRequest(url: urlString!,
//                                       parameter: [:]).responseJSON{ (response) in
//                print("streamingInfo ", response)
//                switch response.result {
//                case .success(let value):
//                    let json = JSON(value)
//                    let code  = json["code"].int
//                    let status = json["status"].string
//                    APIResponseCode.checkResponse(withCode:code?.description ,
//                                                  withStatus: status,
//                                                  completion: {
//                                                    [weak self] message , _success in
//                                                    if(_success){
//                                                        video.update(json:json)
//                                                        self?.selectedVideo.accept(video)
//                                                    }
//                    })
//
//                case .failure(let error):
//                    print(error)
//                }
//            }
//        }
//    }
    
    deinit {
//        print("Deallocating \(self)")
        self.removeObservers()
    }
    
    private func removeObservers() {
        if self.registeredObservers.contains(AVPlayerKVOKeyStatus) {
            previewPlayer?.removeObserver(self, forKeyPath: AVPlayerKVOKeyStatus)
            self.registeredObservers.removeAll( where: { $0 == AVPlayerKVOKeyStatus })
        }
        
        if #available(iOS 10.0, *) {
            if self.registeredObservers.contains(AVPlayerKVOKeyTimeControlStatus) {
                previewPlayer?.removeObserver(self, forKeyPath: AVPlayerKVOKeyTimeControlStatus)
                self.registeredObservers.removeAll( where: { $0 == AVPlayerKVOKeyTimeControlStatus })
            }
        } else {
            if self.registeredObservers.contains(AVPlayerKVOKeyRate) {
                previewPlayer?.removeObserver(self, forKeyPath: AVPlayerKVOKeyRate)
                self.registeredObservers.removeAll( where: { $0 == AVPlayerKVOKeyRate })
            }
        }
    }
}


// MARK: - Player controls
extension PreviewViewController {
    func playPreview(with video: Video) -> Bool {
        //set video to local used later when playing
        print("\(#function)")
        
//        self.previewLayer?.player?.replaceCurrentItem(with: nil)
        self.stopPreview()
        if let _urlString = video.streamingUrl {
            if let _url = URL(string: _urlString) {
                self.playStreamingPreview(with: _url)
                self.showPreviewLayout(mode: .streamingLink)
                return true
            } else {
                print("URL Invalid")
                return false
            }
        } else if let _brightcoveID = video.brightcoveId {
            print("No URL")
            self.playBrightCovePreview(with: _brightcoveID)
            self.showPreviewLayout(mode: .brightCove)
            return true
        } else if let _youtubeID = video.youtubeId {
            self.playYouTubePreview(with: _youtubeID)
            self.showPreviewLayout(mode: .youtube)
            print("Youtube ID: ",_youtubeID)
            return true
        }
        return false
    }
    
    func stopPreview() {
        print("\(#function)")

        self.previewPlayer?.pause()
        self.previewPlayer?.cancelPendingPrerolls()
        self.previewPlayer?.replaceCurrentItem(with: nil)
        self.previewYoutubeView?.stopVideo()
        self.previewYoutubeView?.load(withVideoId: "", playerVars:defaultPlayerVars)
    }
    
    func playStreamingPreview(with url: URL) {
        enableNormalLayout()
        previewPlayer?.replaceCurrentItem(with: AVPlayerItem(url: url))
        previewPlayer?.play()
    }
    
    func playYouTubePreview(with youtubeID: String?) {
        enableBackgorundPlayingLayout()
        //TODO_OONA : youtube layout
        //PlayerHelper.shared.playVideo(video: self.video!)
        
        if let _youtubeID = youtubeID {
            self.playVideo(youtubeID:_youtubeID)
        }
    }
}

// MARK: - UI Update
extension PreviewViewController {
    
    func showPreviewLayout(mode: VideoMode) {
        self.previewViewWrapper.isHidden = false
        self.currentVideoMode = mode
        if mode == .youtube {
            self.previewYouTubeWrapper.isHidden = false
            self.previewYoutubeView.isHidden = false
            self.previewVideoView.isHidden = true
        } else {
            self.previewYouTubeWrapper.isHidden = true
            self.previewYoutubeView.isHidden = true
            self.previewVideoView.isHidden = false
        }
    }
    
    func showPreviewView() {
        print("showPreview")
        self.previewVideoView.isHidden = false
        self.previewViewWrapper.isHidden = false
    }
    
    func hidePreviewWrapper() {
        self.previewViewWrapper.isHidden = true
    }
    
    func showDescription() {
        self.descriptionWrapper.isHidden = false
    }
    
    func hideDescription() {
        self.descriptionWrapper.isHidden = true
    }
    
    func enableBackgorundPlayingLayout() {
        //previewRightConstraint.constant = 0
        //youtubeView.isHidden = true
        self.hidePreviewWrapper()
        self.view.layoutIfNeeded()
    }
    
    func enableNormalLayout() {
        self.view.layoutIfNeeded()
    }
}

// MARK: - UI configurations
extension PreviewViewController {
    func loadAllViews() {
        initPlayerView()
        initBcoPlayerView()
        prepareYoutubeView()
        descriptionTextView.centerVertically()
        hidePreviewWrapper()
    }
        
    func loadDescription() {
        if self.descriptionWrapper.isHidden {
            self.showDescription()
        }
        if let _video = self.previewItem.value?.video {
            self.videoTypeIndicator.image = UIImage(named: _video.type.rawValue)
        }
        
        self.descriptionTitle.text = self.previewItem.value?.title ?? ""
        self.descriptionSubTitle.text = self.previewItem.value?.subtitle ?? ""
        self.descriptionTextView.text = self.previewItem.value?.previewDescription ?? ""
    }
}

