//
//  PreviewViewController+Brightcove.swift
//  OONA
//
//  Created by nicholas on 7/30/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import BrightcovePlayerSDK
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture
import AVKit

extension PreviewViewController {
    
    func playbackController(_ controller: BCOVPlaybackController!, didAdvanceTo session: BCOVPlaybackSession!) {
        print("PreviewViewController Debug - Advanced to new session.")
        session.player.isMuted = true
        session.player.currentItem?.preferredForwardBufferDuration = 1;
        self.previewLayer!.player?.replaceCurrentItem(with: nil)
        self.previewLayer?.player = session.player
        if self.previewViewWrapper.isHidden {
            self.previewViewWrapper.isHidden = false
        }
    }
    
    func playbackController(_ controller: BCOVPlaybackController!, playbackSession session: BCOVPlaybackSession!, didProgressTo progress: TimeInterval) {
        print("ViewController Debug - didProgressTo.2",progress.description)
        if let currentItem = session.player?.currentItem,
            currentItem.isPlaybackLikelyToKeepUp {
            self.previewViewWrapper.cancelLoading()
        }
        PlayerHelper.shared.currentDuration = Int(((progress.isNaN || progress.isInfinite) ? 0 : progress))
    }
    
    func initPlayerView() {
        previewPlayer = AVPlayer()
        previewLayer = AVPlayerLayer(player: previewPlayer)
        previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.frame = self.previewVideoView.bounds
        self.previewVideoView.layer.addSublayer(previewLayer!)
        self.previewViewWrapper.isHidden = false
        
        if let _previewPlayer = previewPlayer {
            if !self.registeredObservers.contains(AVPlayerKVOKeyStatus) {
                _previewPlayer.addObserver(self, forKeyPath: AVPlayerKVOKeyStatus, options: [.old, .new], context: nil)
                self.registeredObservers.append(AVPlayerKVOKeyStatus)
            }
            if #available(iOS 10.0, *) {
                if !self.registeredObservers.contains(AVPlayerKVOKeyTimeControlStatus) {
                    _previewPlayer.addObserver(self, forKeyPath: AVPlayerKVOKeyTimeControlStatus, options: [.old, .new], context: nil)
                    self.registeredObservers.append(AVPlayerKVOKeyTimeControlStatus)
                }
            } else {
                if !self.registeredObservers.contains(AVPlayerKVOKeyRate) {
                    _previewPlayer.addObserver(self, forKeyPath: AVPlayerKVOKeyRate, options: [.old, .new], context: nil)
                    self.registeredObservers.append(AVPlayerKVOKeyRate)
                }
            }
        }
    }
    
    
    public func initBcoPlayerView() {
        
        fairplayProxy = BCOVFPSBrightcoveAuthProxy.init(publisherId: nil, applicationId: nil)
        let basicSessionProvider :BCOVBasicSessionProvider = manager.createBasicSessionProvider(with: nil) as! BCOVBasicSessionProvider
        
        let fairplaySessionProvider = manager.createFairPlaySessionProvider(withApplicationCertificate: nil, authorizationProxy: fairplayProxy!, upstreamSessionProvider: basicSessionProvider)
        playbackController = manager.createPlaybackController(with: fairplaySessionProvider, viewStrategy: nil)
        playbackController?.delegate = self
        playbackController?.isAutoAdvance = true
        playbackController?.isAutoPlay = true
        
        // Prevents the Brightcove SDK from making an unnecessary AVPlayerLayer
        // since the AVPlayerViewController already makes one
        playbackController?.options = [ kBCOVAVPlayerViewControllerCompatibilityKey : true ]
        
        playbackService = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        
//        self.playbackController?.setVideos([BCOVVideo.init(hlsSourceURL: URL.init(string: "https://content.jwplatform.com/manifests/yp34SRmf.m3u8")!)] as NSFastEnumeration)
//        self.playbackController?.play()
        self.playbackController?.delegate = self
        
    }
    
    public func playBrightCovePreview(with brightcoveID: String) {
        enableNormalLayout()
        previewPlayer?.play()
        currentVideoId = brightcoveID
        if let service = bcovService() {
            service.findVideo(withVideoID: brightcoveID, parameters: [:], completion: { [weak self] (video, jsonResponse, error) in
                //            video.
                if (self?.currentVideoId == brightcoveID){
                    // check still in same video
                    if let _video = video {
                        self?.playbackController?.setVideos([_video] as NSFastEnumeration)
                        self?.playbackController?.play()
                        self?.playbackController?.delegate = self
                    }
                }else{
                    print("canceled brightcove ",brightcoveID, self?.currentVideoId as Any)
                }
            })
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object as AnyObject? === previewPlayer {
            if keyPath == "status" {
                if let _previewPlayer = previewPlayer {
                    self.previewViewWrapper.cancelLoading()
                    if _previewPlayer.status == .readyToPlay {
                        _previewPlayer.play()
                        showPreviewView()
                    } else {
                        self.showAlert(title: "Oops", message: "Unable to load video")
                    }
                }
            } else if keyPath == "timeControlStatus" {
                if #available(iOS 10.0, *) {
                    if let _previewPlayer = previewPlayer {
                        if _previewPlayer.timeControlStatus == .playing {
                            //videoCell?.muteButton.isHidden = false
                            showPreviewView()
                            self.previewViewWrapper.cancelLoading()
                        } else {
                            // videoCell?.muteButton.isHidden = true
                        }
                    }
                }
            } else if keyPath == "rate" {
                if let _previewPlayer = previewPlayer {
                    if _previewPlayer.rate > 0 {
                        //videoCell?.muteButton.isHidden = false
                    } else {
                        //videoCell?.muteButton.isHidden = true
                    }
                }
            }
        }
    }
    
    func bcovService() -> BCOVPlaybackService? {
//        let service = BCOVPlaybackService.init(accountId: PlayerHelper.shared.brightcoveAccountId, policyKey: PlayerHelper.shared.brightcovePolicyKey)
        return BCOVPlaybackService(accountId: PlayerHelper.shared.brightcoveAccountId,
                                   policyKey: PlayerHelper.shared.brightcovePolicyKey)
    }
}

