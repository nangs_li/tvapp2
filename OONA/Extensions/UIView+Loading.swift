//
//  UIView+Loading.swift
//  OONA
//
//  Created by nicholas on 9/9/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
let spinningAnimationKey: String = "rotationAnimation"

extension UIView {
    func startLoading() {
        DispatchQueue.main.async {
            self.cancelLoading()
            print(#function)
            let loadingView = self.prepareLoadingView()
            loadingView.addSpinningAnimation(spinningAnimationKey,
                                             to: loadingView.loadingAnimationView)
        }
    }
    
    func startLoading(above view: UIView? = nil) {
        DispatchQueue.main.async {
            self.cancelLoading()
            print(#function)
            if let _view = view,
                _view.isDescendant(of: self),
                let indexOfExchangeView = self.subviews.firstIndex(of: _view) {
                let loadingView = self.prepareLoadingView(at: indexOfExchangeView+1)
                loadingView.addSpinningAnimation(spinningAnimationKey,
                                                 to: loadingView.loadingAnimationView)
                print("Loading view hierarchy did move above \(String(describing: type(of: _view)))")
            }
        }
    }
    
    func startLoading(below view: UIView? = nil) {
        DispatchQueue.main.async {
            self.cancelLoading()
            print(#function)
            if let _view = view,
                _view.isDescendant(of: self),
                let indexOfExchangeView = self.subviews.firstIndex(of: _view) {
                let loadingView = self.prepareLoadingView(at: indexOfExchangeView-1)
                print("Loading view hierarchy did move below \(String(describing: type(of: _view)))")
                loadingView.addSpinningAnimation(spinningAnimationKey,
                                                 to: loadingView.loadingAnimationView)
            }
        }
    }
    
    func cancelLoading() {
        let foundLoadingViews = self.findLoadingViews()
        guard foundLoadingViews.count > 0 else { return }
        print(#function)
        DispatchQueue.main.async {
            foundLoadingViews.forEach( { $0.removeFromSuperview() })
            print("Remains loading views: \(foundLoadingViews.count)")
        }
    }
    
    func findLoadingViews() -> [OONALoadingView] {
        return self.subviews.filter( { $0.isMember(of: OONALoadingView.self) } ) as! [OONALoadingView]
    }
    
    private func prepareLoadingView(at index: Int? = nil) -> OONALoadingView {
        
        let maxEdge = self.getMaxEdge()
        let loadingView = OONALoadingView.makeLoadingView()
        
        if let _index = index {
            self.insertSubview(loadingView,
                               at: min(self.subviews.count, max(0, _index)))
        } else {
            self.addSubview(loadingView)
        }
        
        loadingView.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(8)
            make.height.width.equalTo(maxEdge)
        })
        
        return loadingView
    }
    
    func getMaxEdge() -> CGFloat {
        var maxEdge = max(self.frame.size.width, self.frame.size.height)
        maxEdge = min(max(maxEdge * 0.5, 35), 70)
        return maxEdge
    }
}

class OONALoadingView: UIView {
    lazy var loadingAnimationView: UIImageView = {
        let loadingImageView = UIImageView()
        loadingImageView.image = UIImage(named: "loading-sheet")
        loadingImageView.contentMode = .scaleAspectFit
        return loadingImageView
    }()
    
    lazy var loadingLabel: UILabel = {
        let loadingLabel = UILabel()
        loadingLabel.text = LanguageHelper.localizedString("Loading")
        loadingLabel.textAlignment = .center
        loadingLabel.font = UIFont(name: "Montserrat-Regular", size: 12)
        loadingLabel.textColor = .white
        return loadingLabel
    }()
    
    let disposeBag = DisposeBag()
    static func makeLoadingView() -> OONALoadingView {
        let customView = OONALoadingView()
        customView.backgroundColor = .black
        customView.layer.cornerRadius =  0.15 * 50
//        customView.isUserInteractionEnabled = false
        let loadingAnimationView = customView.loadingAnimationView
        customView.addSubview(loadingAnimationView)
        
        let loadingLabel = customView.loadingLabel
        customView.addSubview(loadingLabel)
        
        loadingAnimationView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(5)
            make.centerX.equalToSuperview()
        }
        
        loadingLabel.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview().inset(5)
            make.top.equalTo(loadingAnimationView.snp.bottom).offset(5)
        }
        
        NotificationCenter.default.rx
            .notification(UIApplication.didBecomeActiveNotification)
            .subscribe(onNext: { [weak weakCustomView = customView] _ in
                if let checkedView = weakCustomView {
                    checkedView.addSpinningAnimation(spinningAnimationKey, to: checkedView.loadingAnimationView)
                }
            })
            .disposed(by: customView.disposeBag)
        
        return customView
    }
    
    func addSpinningAnimation(_ animationKey: String, to view: UIView) {
        let duration: Double = 0.5
        let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.byValue = Double.pi * 1.0
        rotationAnimation.duration = duration
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = Float.infinity
        if view.layer.animation(forKey: animationKey) == nil {
            view.layer.add(rotationAnimation, forKey: animationKey)
        }
    }
    
    deinit {
        print("Deallocating \(self)")
    }
}
