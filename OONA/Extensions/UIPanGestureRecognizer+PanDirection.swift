//
//  UIPanGestureRecognizer+PanDirection.swift
//  OONA
//
//  Created by nicholas on 7/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation


extension UIPanGestureRecognizer {
    func panDirectionForTouchBegan() -> PanDirection {
        
        let panVelocity = velocity(in: view)
        let vertical = abs(panVelocity.y) > abs(panVelocity.x)
        switch (vertical, panVelocity.x, panVelocity.y) {
        case (true, _, let y) where y < 0:
            print("[pan] vertical get")
            return .panUp
        case (true, _, let y) where y > 0:
            print("[pan] vertical get")
            return .panDown
        case (false, let x, _) where x > 0:
            print("[pan] horizontal get")
            return .panRight
        case (false, let x, _) where x < 0:
            print("[pan] horizontal get")
            return .panLeft
        default:
            print("[pan] no direction get")
            return .none
        }
    }
    
}
