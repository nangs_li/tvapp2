//
//  UIViewController+Utilities.swift
//  OONA
//
//  Created by nicholas on 8/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

extension UIViewController {
    func topMostViewController() -> UIViewController? {
        guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController else {
            return nil
        }
        
        var topController = rootViewController
        
        while let newTopController = topController.presentedViewController {
            topController = newTopController
        }
        
        return topController
    }
}
