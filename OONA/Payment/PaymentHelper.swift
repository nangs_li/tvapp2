//
//  PaymentHelper.swift
//  OONA
//
//  Created by Jack on 14/10/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import StoreKit

class PaymentHelper : NSObject, SKProductsRequestDelegate {
    static var shared = PaymentHelper()
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count != 0 {
               for product in response.products {
                   productsArray.append(product)
                    print(product.localizedTitle)
               }
                if response.invalidProductIdentifiers.count != 0 {
                   print(response.invalidProductIdentifiers.description)
                }
//               tblProducts.reloadData()
           }
           else {
               print("There are no products.")
           }
    }
    
    
    var productIDs: Set<String> = ["oona.mobile.usd1"]
    var selectedProductIndex: Int!
      
    var transactionInProgress = false
    var productsArray: Array<SKProduct> = []
    
    
    
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            
            let productRequest = SKProductsRequest(productIdentifiers: productIDs )
     
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            print("Cannot perform In App Purchases.")
        }
    }
    
    func buyAction () {
        let payment = SKPayment(product: self.productsArray[0] )
        SKPaymentQueue.default().add(payment)
        self.transactionInProgress = true
    }
    func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!) {
        for transaction in transactions as! [SKPaymentTransaction] {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
//                delegate.didBuyColorsCollection(selectedProductIndex)
     
     
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                transactionInProgress = false
     
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
}
