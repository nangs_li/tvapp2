//
//  RadioHelper.swift
//  OONA
//
//  Created by nicholas on 7/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RxCocoa
import RxSwift

class RadioChannelList: Pagination<RadioChannel> {
    /// For identify which category of episode list belongs to.
    var categoryID: Int
    
    init(categoryID: Int) {
//        super.init
        self.categoryID = categoryID
    }
    
}

class RadioEpisodeList: Pagination<RadioEpisode> {
    /// For identify which category of episode list belongs to.
    var channelId: Int
    
    init(channelId: Int) {
//        super.init
        self.channelId = channelId
    }
    
}

final class RadioHelper: NSObject {
    static let shared = RadioHelper()
    public var _radioChannel: RadioChannelList
    public var _radioEpisode: RadioEpisodeList
    let disposeBag = DisposeBag()
    var loading : Bool = false
    var selectedCategoryID: BehaviorRelay<Int> = BehaviorRelay(value: NSNotFound)
    var radioChannel : PublishRelay<RadioChannelList> = PublishRelay()
    
    override init() {
        self._radioChannel = RadioChannelList(categoryID: NSNotFound)
        self._radioEpisode = RadioEpisodeList(channelId: NSNotFound)
    }
    
    func getCategoryList(type: String,
                         success: @escaping (([Category]) -> Void),
                         failure: ((Error?) -> Void)?) {
        OONAApi.shared.getRequest(url: APPURL.RadioCategory,
                                  parameter: ["type":type])
            .responseJSON{ [weak self] (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            let RadioCategory: [Category] = JSON(json["categories"])
                                                                .oonaModelArray(action: { (category: Category) in
                                                                    var newCategory = category
                                                                    
                                                                    if let categoryID = self?.selectedCategoryID.value {
                                                                        if categoryID == newCategory.id {
                                                                            newCategory.selected = true
                                                                        }
                                                                    }
                                                                    return newCategory
                                                                })
                                                            success(RadioCategory)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                        }
                                                        
                        })
                    }
                    
                case .failure(let error):
                    print(error)
                    failure?(error)
                }
        }
    }
    
    
    func getChannelList(radioChannel: RadioChannelList,
                        success: @escaping ((Int, [RadioChannel]) -> Void),
                        failure: ((Error?) -> Void)?) -> Void {
        
        //        let updatedOffset: Int = fetchMode == .nextPage ? RadioEpisode.nextRequestRange.location : 0
        var parameter: [String: Any]! = nil
        if(radioChannel.categoryID == -99){
            parameter = ["categoryId": radioChannel.categoryID,
                         "offset": radioChannel.nextRequestRange.location,
                         "filter":"favourite"]
        } else if(radioChannel.categoryID == -94){
            parameter = ["offset": radioChannel.nextRequestRange.location]
        } else {
            parameter = ["categoryId": radioChannel.categoryID,
                         "offset": radioChannel.nextRequestRange.location,
                         "filter":"category"]
        }
        OONAApi.shared.postRequest(url: APPURL.RadioChannelList(),
                                   parameter: parameter)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { message , _success in
                                                        if(_success){
                                                            let channels = JSON(json["channels"]).oonaModelArray() as [RadioChannel]
                                                            success(radioChannel.categoryID, channels)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                            failure?(nil)
                                                        }
                        })
                    }
                case .failure(let error):
                    print(error)
                    failure?(error)
                    
                }
        }
    }
    
    func getEpisodeList(channelId: Int,
                        getFullList: Bool,
                        success: @escaping ((Int,RadioChannelInfo,RadioLive,[RadioEpisode]) -> Void),
                        failure: ((Error?) -> Void)?) -> Void {
        _radioEpisode.channelId = channelId
        //        let updatedOffset: Int = fetchMode == .nextPage ? RadioEpisode.nextRequestRange.location : 0
       var parameter: [String: Any]! = nil
       if(RadioHelper.shared.selectedCategoryID.value == -99 && !getFullList){
            parameter = ["channelId": _radioEpisode.channelId,
             "chunkSize": _radioEpisode.nextRequestRange.length,
             "offset": _radioEpisode.nextRequestRange.location,
                                  "filter":"favourite"]
        } else if(getFullList) {
            parameter = ["channelId": _radioEpisode.channelId,
                       "chunkSize": _radioEpisode.nextRequestRange.length,
                       "offset": _radioEpisode.nextRequestRange.location]
        } else {
            parameter = ["channelId": _radioEpisode.channelId,
            "chunkSize": _radioEpisode.nextRequestRange.length,
            "offset": _radioEpisode.nextRequestRange.location]
        }
        OONAApi.shared.postRequest(url: APPURL.RadioEpisodeList(channelId: String(channelId)),
                                   parameter: parameter)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { message , _success in
                                                        if(_success){
                                                            let channelInfo : RadioChannelInfo = RadioChannelInfo.init(json: json["channelInfo"])!
                                                            let live : RadioLive = RadioLive.init(json: json["live"])
                                                            let episode = JSON(json["episodes"]).oonaModelArray() as [RadioEpisode]
                                                            success(self._radioEpisode.channelId,channelInfo ,live,episode)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                            failure?(nil)
                                                        }
                        })
                    }
                case .failure(let error):
                    print(error)
                    failure?(error)
                    
                }
        }
    }
    
    func addFavouriteRadio(channelId: String,
                           success: @escaping ((Bool) -> Void),
                           failure: ((Error?) -> Void)?) {
        OONAApi.shared.putRequest(url: APPURL.AddFavouriteRadio(channelId: channelId),
                                   parameter: ["type": "type"])
            .responseJSON{(response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "RefreshChannelData"), object: self,
                                                                                                                             userInfo: ["type":"HideLoading"])
                                                        if(isSuccess){
                                                            
                                                            success(true)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                            success(false)
                                                        }
                                                        
                        })
                    }
                    
                case .failure(let error):
                    print(error)
                    failure?(error)
                }
        }
    }
    
    
    func addFavouriteRadio(channelId: String,episodeId: String,
                              success: @escaping ((Bool) -> Void),
                              failure: ((Error?) -> Void)?) {
        OONAApi.shared.putRequest(url: APPURL.RemoveFavouriteRadio(channelId: channelId, episodeId: episodeId),
                                   parameter: ["type": "type"])
            .responseJSON{(response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "RefreshChannelData"), object: self,
                                                                       userInfo: ["type":"HideLoading"])
                                                     
                                                        if(isSuccess){
                                                            
                                                            success(true)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                            success(false)
                                                        }
                                                        
                        })
                    }
                    
                case .failure(let error):
                    print(error)
                    failure?(error)
                }
        }
    }
    
    func removeFavouriteRadio(channelId: String,
                              success: @escaping ((Bool) -> Void),
                              failure: ((Error?) -> Void)?) {
           OONAApi.shared.deleteRequest(url: APPURL.AddFavouriteRadio(channelId: channelId),
                                      parameter: ["type": "type"])
               .responseJSON{(response) in
                   switch response.result {
                       
                   case .success(let value):
                       let json = JSON(value)
                       if let code = json["code"].int,
                           let status = json["status"].string {
                           APIResponseCode.checkResponse(withCode: code.description,
                                                         withStatus: status,
                                                         completion: { (message, isSuccess) in
                                                            NotificationCenter.default.post(name: Notification.Name(rawValue: "RefreshChannelData"), object: self,
                                                                                                                                 userInfo: ["type":"HideLoading"])
                                                           if(isSuccess){
                                                               
                                                               success(true)
                                                           } else {
                                                               print("Failure on check response with message: \(message ?? "unknown error")")
                                                               success(false)
                                                           }
                                                           
                           })
                       }
                       
                   case .failure(let error):
                       print(error)
                       failure?(error)
                   }
           }
       }
       
       
       func removeFavouriteRadio(channelId: String,episodeId: String,
                                 success: @escaping ((Bool) -> Void),
                                 failure: ((Error?) -> Void)?) {
           OONAApi.shared.deleteRequest(url: APPURL.RemoveFavouriteRadio(channelId: channelId, episodeId: episodeId),
                                      parameter: ["type": "type"])
               .responseJSON{(response) in
                   switch response.result {
                       
                   case .success(let value):
                       let json = JSON(value)
                       if let code = json["code"].int,
                           let status = json["status"].string {
                           APIResponseCode.checkResponse(withCode: code.description,
                                                         withStatus: status,
                                                         completion: { (message, isSuccess) in
                                                            NotificationCenter.default.post(name: Notification.Name(rawValue: "RefreshChannelData"), object: self,
                                                                                                                                 userInfo: ["type":"HideLoading"])
                                                           if(isSuccess){
                                                               
                                                               success(true)
                                                           } else {
                                                               print("Failure on check response with message: \(message ?? "unknown error")")
                                                               success(false)
                                                           }
                                                           
                           })
                       }
                       
                   case .failure(let error):
                       print(error)
                       failure?(error)
                   }
           }
       }
    
    func isSameCategory(_ categoryID: Int) -> Bool {
            return categoryID == _radioChannel.categoryID
        }
        
        func requestChannelList(categoryID: Int) {
            if self._radioChannel.categoryID != NSNotFound {
                if !isSameCategory(categoryID) {
                    print("Performing new channels request")
                    self._radioChannel.categoryID = categoryID
                    self._radioChannel.itemList.removeAll()
                } else {
                    print("Performing next channels request")
                }
            }
            else {
                print("Performing new channels request")
                self._radioChannel.categoryID = categoryID
            }
            /// Publish rx signal
            self.radioChannel.accept(self._radioChannel)
            if (!loading) {
                loading = true
                
                self.getChannelList(radioChannel: self._radioChannel, success: { (categoryID, channelList) in
                    self.loading = false
                    self.updateChannelList(channelList: channelList, categoryID: categoryID)
                }) { (Error) in
                    
                }
                
            }
        }
        
        public func updateChannelList(channelList: [RadioChannel],
                                       categoryID: Int) {
            
            /// Check xploreEpisode is exist
            if self._radioChannel.categoryID != NSNotFound {
                print("Updating RadioChannelList for category: \(categoryID)")
                if categoryID == self._radioChannel.categoryID {
                    print("Appending RadioChannelList for category: \(categoryID)")
                    self._radioChannel.itemList.append(contentsOf: channelList)
                } else {
                    print("Replacing RadioChannelList for category from: \(_radioChannel.categoryID) to: \(categoryID)")
                    self._radioChannel.categoryID = categoryID
                    self._radioChannel.itemList = channelList
                }
                
    //            self._xploreEpisode.hasNextPage = (episodeList.count == self._xploreEpisode.nextRequestRange.length)
                self._radioChannel.hasNextPage = (channelList.count != 0)
                
                /// Publish rx signal
                self.radioChannel.accept(self._radioChannel)
            }
        }
        
}
