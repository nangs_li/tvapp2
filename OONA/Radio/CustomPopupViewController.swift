//
//  RadioChannelViewController.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class CustomPopupViewContoller: BaseViewController {
    @IBOutlet weak var closeRadioButton: UIButton!
    @IBOutlet weak var keepRadioButton: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var popUpDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setUpView() {
        self.keepRadioButton.layer.masksToBounds = true;
        self.keepRadioButton.layer.borderWidth = 1;
        self.keepRadioButton.layer.borderColor = UIColor.white.cgColor
        self.popUpView.layer.masksToBounds = true;
        self.popUpView.layer.borderWidth = 0.2;
        self.popUpView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func pressCloseRadioButton(_ sender: Any) {
        let notificationName2 = Notification.Name(rawValue: "PlayOrPauseMusic")
        NotificationCenter.default.post(name: notificationName2, object: self,
                                        userInfo: ["play":false,"type":"live"])
        NotificationCenter.default.post(name: notificationName2, object: self,
                                        userInfo: ["play":false,"type":"podcase"])
        let notificationName = Notification.Name(rawValue: "ClosePlayer")
               NotificationCenter.default.post(name: notificationName, object: self,
                                               userInfo: ["play":false])
        let notificationName3 = Notification.Name(rawValue: "RadioMoveToOtherPage")
        NotificationCenter.default.post(name: notificationName3, object: self,
        userInfo: ["play":false,"type":"podcase"])
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressKeepListing(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    deinit {
        //        print("Deallocating \(self)")
    }
}

