//
//  RadioChannelViewController.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh
import MediaPlayer

public enum RadioPlayerType {
    case Live
    case Podcast
}

class RadioPlayerViewController: BaseViewController {
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bigImageView: UIImageView!
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var liveImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var blurImageView: UIImageView!
    @IBOutlet weak var playButtonLeading: NSLayoutConstraint!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var likeOrUnLikeButton: UIButton!
    
    var radioPlayerType: RadioPlayerType = .Podcast
    var radioViewController:NewRadioViewController! = nil
    
    var play: Bool = false
    let player: FRadioPlayer = FRadioPlayer.shared
    var sliderValue:Float = 0 {
        didSet {
            setUpSliderValue(slidervalue: sliderValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if((self.radioViewController) != nil){
        if(radioPlayerType == .Live){
            setUpLivePlayer()
        } else {
            setUpPodCastPlayer()
        }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setUpSliderValue(slidervalue:Float) {
        slider?.value = sliderValue
    }
    
    @IBAction func sliderChange(_ sender: UISlider) {
        let avPlayer = self.player.player
        guard let duration  = avPlayer?.currentItem?.asset.duration else{
            return
        }
        let seconds = sender.value * Float(CMTimeGetSeconds(duration))
        let targetTime:CMTime = CMTimeMake(value: Int64(seconds), timescale: 1)
        self.player.player?.seek(to: targetTime)
        
    }
    
    func setUpPodCastPlayer() {
        self.headerLabel.text = self.radioViewController?.radioChannelInfo.name
        self.topTitleLabel.text = self.radioViewController?.radioChannelInfo.name
        self.titleLabel.text = self.radioViewController?.playingRadioEpisode.name
        slider.isUserInteractionEnabled = true
        slider.minimumTrackTintColor = .red
        slider.maximumTrackTintColor = UIColor.init(rgb: 0xa6a6a6)
        slider.setThumbImage(UIImage(named:"sliderThumb"), for: .normal)
        slider.setThumbImage(UIImage(named:"sliderThumb"), for: .highlighted)
        playButtonLeading.constant = 125
        self.view.layoutIfNeeded()
        self.liveImageView.isHidden = true
        self.descLabel.isHidden = false
        previousButton.isHidden = false
        nextButton.isHidden = false
        blurImageView.blurImage()
        let imageUrl = transformURLString(self.radioViewController.playingRadioEpisode.imageUrl)
        if let url = imageUrl?.url {
            self.bigImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
            self.blurImageView?.sd_setImage(with: url,
                                                          placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        self.likeButton.isHidden = false
        self.shareButton.isHidden = false
        setUpLikeOrUnlikeButton()
        
        if(play){
            playButton.setImage(UIImage(named: "pause_bigradio") , for:.normal)
            
        } else{
            playButton.setImage(UIImage(named: "play_bigradio"), for:.normal)
            
        }
        
    }
    
    func setUpLivePlayer() {
        self.slider.value = Float(1.0)
        self.sliderValue = Float(1.0)
        self.headerLabel.text = self.radioViewController?.radioChannelInfo.name
        self.topTitleLabel.text = ""
        self.titleLabel.text = self.radioViewController?.radioLive.name
        slider.minimumTrackTintColor = .red
        slider.maximumTrackTintColor = UIColor.init(rgb: 0xa6a6a6)
        slider.setThumbImage(UIImage(), for: .normal)
        slider.isUserInteractionEnabled = false
        playButtonLeading.constant = 26
        self.view.layoutIfNeeded()
        self.liveImageView.isHidden = false
        self.descLabel.isHidden = true
        previousButton.isHidden = true
        nextButton.isHidden = true
        blurImageView.blurImage()
        let imageUrl = transformURLString(self.radioViewController.radioChannelInfo.imageUrl)
        if let url = imageUrl?.url {
            self.bigImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
            self.blurImageView?.sd_setImage(with: url,
                                                                     placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        
        self.likeButton.isHidden = true
        self.shareButton.isHidden = true
        if(play){
            playButton.setImage(UIImage(named: "pause_bigradio") , for:.normal)
            
        } else{
            playButton.setImage(UIImage(named: "play_bigradio"), for:.normal)
            
        }
    }
    
    @IBAction func clickPlayButton(_ sender: Any) {
        var play:Bool
        if(playButton.currentImage == UIImage(named: "play_bigradio")){
            playButton.setImage(UIImage(named: "pause_bigradio") , for:.normal)
            play = true
        } else if(playButton.currentImage == UIImage(named: "pause_bigradio")){
            playButton.setImage(UIImage(named: "play_bigradio"), for:.normal)
            play = false
        } else {
            playButton.setImage(UIImage(named: "pause_bigradio") , for:.normal)
            play = true
        }
        let notificationName = Notification.Name(rawValue: "PlayOrPauseMusic")
        if(self.radioPlayerType == .Live){
            NotificationCenter.default.post(name: notificationName, object: self,
            userInfo: ["play":play,"type": "live","radioLive":self.radioViewController.radioLive])
            //            radioLive.play = play
        } else {
            radioViewController.playingRadioEpisode.play = play
            NotificationCenter.default.post(name: notificationName, object: self,
            userInfo: ["play":play,"type":"podcase","radioEpisode":self.radioViewController.playingRadioEpisode])
        }
       
    }
    
    @IBAction func clickShareButton(_ sender: Any) {
        self.likeOrUnLikeButton.setTitle((self.radioViewController.radioChannelInfo.isFavourite) ? "Remove Channel To My Radio":"Add Channel To My Radio", for: .normal)
        self.shareView.isHidden = !self.shareView.isHidden
        
    }
    
    @IBAction func clickLikeButton(_ sender: Any) {
        let like:Bool = likeButton.setButtonLikeorUnLikeImage()
        self.view.startLoading()
               if(like){
                   RadioHelper.shared.addFavouriteRadio(channelId: String(self.radioViewController.radioChannelInfo.id),episodeId: String(self.radioViewController.playingRadioEpisode.id), success: { (Bool) in
                       self.radioViewController.playingRadioEpisode.isFavourite = true
                       self.setUpLikeOrUnlikeButton()
                       self.view.cancelLoading()
                   }) { (Error) in
                       self.view.cancelLoading()
                   }
               } else {
                   
                   RadioHelper.shared.removeFavouriteRadio(channelId: String(self.radioViewController.radioChannelInfo.id),episodeId: String(self.radioViewController.playingRadioEpisode.id), success: { (Bool) in
                       self.radioViewController.playingRadioEpisode.isFavourite = false
                       self.setUpLikeOrUnlikeButton()
                       self.view.cancelLoading()
                   }) { (Error) in
                       self.view.cancelLoading()
                   }
               }
       
    }
    
    @IBAction func likeOrUnLikeChannelButton(_ sender: Any) {
        self.shareView.isHidden = true
        self.view.startLoading()
               if(!self.radioViewController.radioChannelInfo.isFavourite){
                          RadioHelper.shared.addFavouriteRadio(channelId: String(self.radioViewController.radioChannelInfo.id), success: { (Bool) in
                              
                              
                              self.view.cancelLoading()
                          }) { (Error) in
                              self.view.cancelLoading()
                          }
                      } else {
                          
                          RadioHelper.shared.removeFavouriteRadio(channelId: String(self.radioViewController.radioChannelInfo.id), success: { (Bool) in
                             
                              
                              self.view.cancelLoading()
                          }) { (Error) in
                              self.view.cancelLoading()
                          }
                      }
    }
    
    @IBAction func pressPreviousButton(_ sender: Any) {
        let avPlayer = self.player.player
        guard (avPlayer?.currentItem?.duration) != nil else{
           return
       }
        
        let playerCurrentTime = CMTimeGetSeconds((avPlayer?.currentTime())!)
       var newTime = playerCurrentTime - 10

       if newTime < 0 {
           newTime = 0
       }
        let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        avPlayer?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    @IBAction func pressNextButton(_ sender: Any) {
        let avPlayer = self.player.player
         guard let duration  = avPlayer?.currentItem?.asset.duration else{
            return
        }
        let playerCurrentTime = CMTimeGetSeconds((avPlayer?.currentTime())!)
        let newTime = playerCurrentTime + 10

        if newTime < (CMTimeGetSeconds(duration) - 10) {

            let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
            avPlayer?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)

        }
    }
    
    deinit {
        //        print("Deallocating \(self)")
    }
    
    func setUpLikeOrUnlikeButton() {
        if((radioViewController) != nil){
        if(radioViewController.playingRadioEpisode.isFavourite){
            likeButton.setImage(UIImage(named: "wishlist-on"), for: .normal)
        } else {
            likeButton.setImage(UIImage(named: "wishlist-off") , for: .normal)
        }
        }
    }
    
    func transformURLString(_ string: String) -> URLComponents? {
           guard let urlPath = string.components(separatedBy: "?").first else {
               return nil
           }
           var components = URLComponents(string: urlPath)
           if let queryString = string.components(separatedBy: "?").last {
               components?.queryItems = []
               let queryItems = queryString.components(separatedBy: "&")
               for queryItem in queryItems {
                   guard let itemName = queryItem.components(separatedBy: "=").first,
                         let itemValue = queryItem.components(separatedBy: "=").last else {
                           continue
                   }
                   components?.queryItems?.append(URLQueryItem(name: itemName, value: itemValue))
               }
           }
           return components!
       }
}

extension UIImageView{
    func blurImage()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
}

