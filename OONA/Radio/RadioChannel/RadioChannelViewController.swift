//
//  RadioChannelViewController.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class RadioChannelViewController: BaseViewController, UIScrollViewDelegate {
   @IBOutlet weak var collectionView: UICollectionView! { didSet { collectionView.backgroundColor = nil } }
    
    let radioChannelDatasource: RadioChannelDatasource = RadioChannelDatasource()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.addRXObservers()
        //self.loadPreviewView()
       // self.setupCollectionViewPullUpLoadMoreFooter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}

// MARK: - Config Views
extension RadioChannelViewController {
    private func setupCollectionViewPullUpLoadMoreFooter() {
        let loadMoreFooter = MJRefreshAutoNormalFooter()
        loadMoreFooter.refreshingBlock = { [weak self] in
            self?.loadMoreData()
        }
        self.collectionView.mj_footer = loadMoreFooter
    }
}

// MARK: - Functional methods
extension RadioChannelViewController {
    
    private func addRXObservers() {
        // Bind dataSource to collectionView
        radioChannelDatasource.bindDataSource(to: collectionView)
        
        // Subscribe data change from Episode manager on data has been updated
        RadioHelper.shared.radioChannel.subscribe(onNext: { [weak self] (radioChannel) in
            self?.endLoadMoreData(with: radioChannel)
            self?.radioChannelDatasource.channelList.accept(radioChannel.itemList)
        }).disposed(by: self.disposeBag)
        
       
        
        //
        RadioHelper.shared.selectedCategoryID.filter({ $0 != NSNotFound })
            .subscribe(onNext: {(categoryID) in
                
                var sameCategory: Bool = false
                
                sameCategory = RadioHelper.shared.isSameCategory(categoryID)
                
                if !sameCategory {
                    RadioHelper.shared.requestChannelList(categoryID: categoryID)
                } else {
                    print("Ignoring same category being selected")
                }
                
        }).disposed(by: self.disposeBag)
        
    }
    
    func loadMoreData() {
        RadioHelper.shared.requestChannelList(categoryID: RadioHelper.shared.selectedCategoryID.value)
    }
    
    func endLoadMoreData(with radioChannelList: RadioChannelList) {
        if radioChannelList.hasNextPage {
            self.collectionView.mj_footer?.endRefreshing()
        } else {
            self.collectionView.mj_footer?.endRefreshingWithNoMoreData()
        }
    }
    
    
    func reloadContent() {
        (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).invalidateLayout()
        self.collectionView.reloadData()
    }
}

extension RadioChannelViewController {
    
    func updateCellVisibilityInCollectionView(with item: Int) {
        self.collectionView.scrollToItem(at: IndexPath(item: item,
                                                       section: 0),
                                         at: .top, animated: true)
    }

}
