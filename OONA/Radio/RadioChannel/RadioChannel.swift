//
//  Episode.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct RadioChannel: OONAModel, Equatable {
    static func == (lhs: RadioChannel, rhs: RadioChannel) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id : Int = 0
    var name : String = ""
    var subtitle : String = ""
    var description : String = ""
    var imageUrl : String = ""
    var streamingUrl : String = ""
   // var length : String = ""
   // var no : String = ""
   // var isFavourite : Bool = false
   // var url : String = ""
   // var isYoutube : Bool = false
   // var type : String = ""
  //  var video : Video?
    
    
    init(){
        
    }
    
    init?(id: Int) {
        //use in first load when we only have stored the id
        self.id = id
    }
    
    init?(json: JSON) {
        if let _id: Int = json["id"].int {
            self.id = _id
        }
        if let _name: String = json["name"].string {
            self.name = _name
        }
        if let _subtitle: String  = json["subtitle"].string {
            self.subtitle = _subtitle
        }
        if let _description: String  = json["description"].string {
            self.description = _description
        }
        
        if let _imageUrl: String = json["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        if let _url: String = json["streamingUrl"].string {
            self.streamingUrl = _url
        }
        
    }
    
}
