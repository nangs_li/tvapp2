//
//  RadioChannelCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class RadioChannelCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var overlayImageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var chanelname: UILabel!
    @IBOutlet weak var underline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(with channel: RadioChannel) {
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: channel.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        descriptionLabel.text = channel.subtitle
        chanelname.text = channel.name
        
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
    
}
