//
//  RadioChannelDatasource.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import SwiftyJSON

class RadioChannelDatasource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    static let cellIdentifier: String = "RadioChannelCell"
    
    var radioPlayerView: UIView!
    var channelList: BehaviorRelay<[RadioChannel]>
    var previewEpisode: BehaviorRelay<RadioChannel?>
    var previousSelectedCell: RadioChannelCell?
    let disposeBag = DisposeBag()
    var cView : UICollectionView?
    var lastContentOffset: CGFloat = 0
    let player: FRadioPlayer = FRadioPlayer.shared
    
    override init() {
        self.channelList = BehaviorRelay<[RadioChannel]>(value: [RadioChannel]())
        self.previewEpisode = BehaviorRelay<RadioChannel?>(value: nil)
        super.init()
    }
    
    deinit {
        print("Deallocating \(self)")
    }
    
    func bindDataSource(to collectionView: UICollectionView) {
        // Register Cell Nib for collectionView
        collectionView.register(UINib(nibName: "RadioChannelCell", bundle: nil),
                                forCellWithReuseIdentifier: RadioChannelDatasource.cellIdentifier)
        // Set collectionView content inset
        collectionView.contentInset = UIEdgeInsets(top: lineSpacing,
                                                   left: interitemSpacing,
                                                   bottom: 0,
                                                   right: interitemSpacing)
        // Assign collectionView delegate and datasource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Subscribe rx update from episodeList to perform reloadData
        self.channelList.subscribe(onNext: { [weak collectionView, weak self] (episodeList) in
            self?.previousSelectedCell = nil
            collectionView?.reloadData()
        }).disposed(by: self.disposeBag)
        
        print("did add RXObserver to collectionView")
    }
    
    func selectedSameEpisode(episodeID: Int) -> Bool {
        if let _previewEpisode = self.previewEpisode.value {
            return _previewEpisode.id == episodeID
        }
        return false
    }
    
    // MARK: - CollectionView Delegate, Datasource and Layout
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channelList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RadioChannelCell = collectionView.dequeueReusableCell(withReuseIdentifier: RadioChannelDatasource.cellIdentifier,
                                                                         for: indexPath) as! RadioChannelCell
        let channel: RadioChannel = self.channelList.value[indexPath.item]
        
        cell.configure(with: channel)
        cell.overlayImageView.isHidden = true
        cell.updateCellSelection(selection: selectedSameEpisode(episodeID: channel.id))
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         print("indexPath \(indexPath)")
        let channel: RadioChannel = self.channelList.value[indexPath.item]
        let notificationName = Notification.Name(rawValue: "ChannelListClick")
        NotificationCenter.default.post(name: notificationName, object: self,
                                        userInfo: ["indexPath":indexPath,"channel":channel])

    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    private let interitemSpacing: CGFloat = 19
    private let lineSpacing: CGFloat = 10
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
//        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        let height = (width) + 17 * 2 + 17//label height + 17 space
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if((self.player.radioURL) != nil){
            if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
                //reach bottom
                self.radioPlayerView.isHidden = true
            }else if (scrollView.contentOffset.y < 0){
                //reach top
            } else if (self.lastContentOffset > scrollView.contentOffset.y) {
                // move up
                self.radioPlayerView.isHidden = false
            }
            else if (self.lastContentOffset < scrollView.contentOffset.y) {
               // move down
                 self.radioPlayerView.isHidden = true
            }

            print("self.lastContentOffset",self.lastContentOffset)
            // update the new position acquired
            self.lastContentOffset = scrollView.contentOffset.y
        }
    }
}
