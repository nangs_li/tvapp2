
//
//  Episode.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct RadioChannelInfo: OONAModel {
    
    var id : Int = 0
    var name : String = ""
    var subtitle : String = ""
    var thumbnailUrl : String = ""
    var imageUrl : String = ""
    var isFavourite : Bool = false
  
    init(){
        
    }
    
    init?(id: Int) {
        //use in first load when we only have stored the id
        self.id = id
    }
    
    init?(json: JSON) {
        if let _id: Int = json["id"].int {
            self.id = _id
        }
        if let _name: String = json["name"].string {
            self.name = _name
        }
        if let _subtitle: String = json["subtitle"].string {
            self.subtitle = _subtitle
        }
        if let _thumbnailUrl: String = json["thumbnailUrl"].string {
            self.thumbnailUrl = _thumbnailUrl
        }
        if let _imageUrl: String = json["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        if let _isFavourite: Bool = json["isFavourite"].int == 1 ?  true : false {
            self.isFavourite = _isFavourite
        }
        
    }
    
}

