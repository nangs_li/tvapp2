//
//  RadioChannelCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class FooterViewCell: UICollectionViewCell {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var moreEpisodersButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func moreEpisodersButton(_ sender: Any) {
        let notificationName8 = Notification.Name(rawValue: "ClickMoreEpisodesButton")
        NotificationCenter.default.post(name: notificationName8, object: self,
                                               userInfo: ["type":"ShowLoading"])

    }
    
    
  
    
}
