//
//  RadioChannelCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class AirNowCell: UICollectionViewCell {
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var channelNameLabel: UILabel!
    var radioLive: RadioLive = RadioLive()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with radioLive: RadioLive) {
        self.radioLive = radioLive
        self.channelNameLabel.text = radioLive.name
        self.playButton.setButtonPlayorPauseImageFromPlay(play: radioLive.play)
    }
    
    @IBAction func pressPlayButton(_ sender: Any) {
        var play:Bool = true
        if(playButton.currentImage == UIImage(named: "play")){
            play = true
        } else if(playButton.currentImage == UIImage(named: "pause")){
            play = false
        } else {
            play = true
        }
        let notificationName = Notification.Name(rawValue: "PlayOrPauseMusic")
        NotificationCenter.default.post(name: notificationName, object: self,
                                        userInfo: ["play":play,"type":"live","radioLive":self.radioLive])
    }
    
}
