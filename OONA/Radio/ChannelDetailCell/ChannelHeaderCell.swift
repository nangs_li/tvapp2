//
//  RadioChannelCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class ChannelHeaderCell: UICollectionViewCell {
   
    @IBOutlet weak var channelImageView: UIImageView!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var channelDesc: UILabel!
    @IBOutlet weak var channelDetailDesc: UILabel!
    var radioChannelInfo:RadioChannelInfo = RadioChannelInfo()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with radioChannelInfo: RadioChannelInfo) {
        self.radioChannelInfo = radioChannelInfo
        if let url = URL.init(string: radioChannelInfo.imageUrl){
            self.channelImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        channelName.text = radioChannelInfo.name
        channelDesc.text = radioChannelInfo.subtitle
        if(radioChannelInfo.isFavourite){
            likeButton.setImage(UIImage(named: "wishlist-on"), for: .normal)
        } else {
            likeButton.setImage(UIImage(named: "wishlist-off") , for: .normal)
        }
    }
   
    @IBAction func clickLikeButton(_ sender: Any) {
        let like:Bool = likeButton.setButtonLikeorUnLikeImage()
//        let notificationName = Notification.Name(rawValue: "LikeOrUnLike")
//        NotificationCenter.default.post(name: notificationName, object: self,
//                                        userInfo: ["like":like,"type":"allchannel"])
        let notificationName5 = Notification.Name(rawValue: "ShowLoading")
        let notificationName6 = Notification.Name(rawValue: "HideLoading")

        NotificationCenter.default.post(name: notificationName5, object: self,
                                        userInfo: ["type":"ShowLoading"])

        if(like){
        RadioHelper.shared.addFavouriteRadio(channelId: String(self.radioChannelInfo.id), success: { (Bool) in
            self.radioChannelInfo.isFavourite = true
            self.configure(with: self.radioChannelInfo)
            NotificationCenter.default.post(name: notificationName6, object: self,
            userInfo: ["type":"HideLoading"])
        }) { (Error) in
            NotificationCenter.default.post(name: notificationName6, object: self,
                       userInfo: ["type":"HideLoading"])
        }
        } else {
            
            RadioHelper.shared.removeFavouriteRadio(channelId: String(self.radioChannelInfo.id), success: { (Bool) in
                self.radioChannelInfo.isFavourite = false
                self.configure(with: self.radioChannelInfo)
                NotificationCenter.default.post(name: notificationName6, object: self,
                           userInfo: ["type":"HideLoading"])
            }) { (Error) in
                NotificationCenter.default.post(name: notificationName6, object: self,
                           userInfo: ["type":"HideLoading"])
            }
        }
    }
}
