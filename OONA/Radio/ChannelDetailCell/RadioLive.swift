//
//  Video.swift
//  OONA
//
//  Created by Jack on 10/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit

struct RadioLive : OONAModel{
    
    
    var name: String?
    var streamingUrl: String?
    var play : Bool = false
    
    init(){ }
    
    
    init(json: JSON) {
        
        if let _name: String = json["name"].string {
            self.name = _name
        }
        
        if let _streamingUrl: String = json["streamingUrl"].string {
            self.streamingUrl = _streamingUrl
        }
        
    }
    
}
