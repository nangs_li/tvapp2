//
//  Video.swift
//  OONA
//
//  Created by Jack on 10/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit


struct RadioEpisode : OONAModel , Equatable{
    static func == (lhs: RadioEpisode, rhs: RadioEpisode) -> Bool {
           return lhs.id == rhs.id
       }
    
    var id : Int = 0
    var name: String = ""
    var descriptions: String = ""
    var timeInfo: String = ""
    var length: String = ""
    var no: String = ""
    var imageUrl: String = ""
    var thumbnailUrl: String = ""
    var streamingUrl: String = ""
    var isFavourite: Bool = false
    var play : Bool = false
    var showShareView : Bool = false
    
    init(){ }
    
    
    init(json: JSON) {
        
        if let _id: Int = json["id"].int {
            self.id = _id
        }
        
        if let _name: String = json["name"].string {
            self.name = _name
        }
        
        if let _descriptions: String = json["descriptions"].string {
            self.descriptions = _descriptions
        }
        
        if let _timeInfo: String = json["timeInfo"].string {
            self.timeInfo = _timeInfo
        }
        
        if let _length: String = json["length"].string {
            self.length = _length
        }
        if let _no: String = json["no"].string {
            self.no = _no
        }
        if let _imageUrl: String = json["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        if let _thumbnailUrl: String = json["thumbnailUrl"].string {
            self.thumbnailUrl = _thumbnailUrl
        }
        if let _streamingUrl: String = json["streamingUrl"].string {
            self.streamingUrl = _streamingUrl
        }
        if let _isFavourite: Bool = json["isFavourite"].int == 1 ?  true : false {
            self.isFavourite = _isFavourite
        }
        
    }
    
}



