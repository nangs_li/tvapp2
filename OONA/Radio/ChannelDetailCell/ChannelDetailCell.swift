//
//  RadioChannelCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class ChannelDetailCell: UICollectionViewCell {
    @IBOutlet weak var episodeImageView: UIImageView!
    @IBOutlet weak var episodeTitleLabel: UILabel!
    @IBOutlet weak var episodeDescLabel: UILabel!
    @IBOutlet weak var episodeTimeInfo: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var likeOrUnLikeButton: UIButton!
    var radioEpisode:RadioEpisode = RadioEpisode()
    var radioChannelInfo:RadioChannelInfo = RadioChannelInfo()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with radioChannelInfo: RadioChannelInfo,radioEpisode: RadioEpisode) {
        
        self.radioChannelInfo = radioChannelInfo
        self.radioEpisode = radioEpisode
        self.shareView.isHidden = !self.radioEpisode.showShareView
        self.likeOrUnLikeButton.isHidden = !self.radioEpisode.showShareView
        self.likeOrUnLikeButton.setTitle((self.radioEpisode.isFavourite) ? "Remove Episode To My Radio":"Add Episode To My Radio", for: .normal)
        if let url = URL.init(string: self.radioEpisode.imageUrl){
                  self.episodeImageView?.sd_setImage(with: url,
                                                     placeholderImage: UIImage(named: "channel-placeholder-image"))
              }
              episodeTitleLabel.text = self.radioEpisode.name
             // episodeDescLabel.text = radioEpisode.descriptions
              episodeTimeInfo.text = self.radioEpisode.timeInfo
              playButton.setButtonPlayorPauseImageFromPlay(play: radioEpisode.play)
        
    }
    
    @IBAction func clickPlayButton(_ sender: Any) {
        var play:Bool = true
        if(playButton.currentImage == UIImage(named: "play")){
            play = true
        } else if(playButton.currentImage == UIImage(named: "pause")){
            play = false
        } else {
            playButton.setImage(UIImage(named: "pause") , for: .normal)
            play = true
        }
        let notificationName = Notification.Name(rawValue: "PlayOrPauseMusic")
        NotificationCenter.default.post(name: notificationName, object: self,
                                        userInfo: ["play":play,"type":"podcase","radioEpisode":self.radioEpisode])
    }
    @IBAction func clickLikeButton(_ sender: Any) {
        self.radioEpisode.showShareView = false
        self.configure(with: self.radioChannelInfo, radioEpisode: self.radioEpisode)
        let notificationName5 = Notification.Name(rawValue: "ShowLoading")
        let notificationName6 = Notification.Name(rawValue: "HideLoading")
        
        NotificationCenter.default.post(name: notificationName5, object: self,
                                        userInfo: ["type":"ShowLoading"])
        
        if(!self.radioEpisode.isFavourite){
            RadioHelper.shared.addFavouriteRadio(channelId: String(self.radioChannelInfo.id),episodeId: String(self.radioEpisode.id), success: { (Bool) in
                self.radioEpisode.isFavourite = true
                self.configure(with: self.radioChannelInfo, radioEpisode: self.radioEpisode)
                NotificationCenter.default.post(name: notificationName6, object: self,
                                                userInfo: ["type":"HideLoading"])
            }) { (Error) in
                NotificationCenter.default.post(name: notificationName6, object: self,
                                                userInfo: ["type":"HideLoading"])
            }
        } else {
            
            RadioHelper.shared.removeFavouriteRadio(channelId: String(self.radioChannelInfo.id),episodeId: String(self.radioEpisode.id), success: { (Bool) in
                self.radioEpisode.isFavourite = false
                self.configure(with: self.radioChannelInfo, radioEpisode: self.radioEpisode)
                NotificationCenter.default.post(name: notificationName6, object: self,
                                                userInfo: ["type":"HideLoading"])
            }) { (Error) in
                NotificationCenter.default.post(name: notificationName6, object: self,
                                                userInfo: ["type":"HideLoading"])
            }
        }
    }
    

    @IBAction func clickShareButton(_ sender: Any) {
        
        self.radioEpisode.showShareView = !self.radioEpisode.showShareView
        self.configure(with: self.radioChannelInfo, radioEpisode: self.radioEpisode)
        
        
    }
}
