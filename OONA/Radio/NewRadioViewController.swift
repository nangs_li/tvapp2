//
//  XploreViewController.swift
//  OONA
//
//  Created by nicholas on 7/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit
import RxCocoa
import EMAlertController
import MediaPlayer
import AVFoundation
import CoreMedia

class NewRadioViewController: BaseViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var channelListDetailCollectionView: UICollectionView!
    @IBOutlet weak var chanelDetailLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var radioContentView: UIView!
    @IBOutlet weak var radioPlayerView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var radioTitleLabel: UILabel!
    @IBOutlet weak var radioDescLabel: UILabel!
    @IBOutlet weak var slider: CustomSlider!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerTitleLabel: UILabel!
    @IBOutlet weak var liveButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var radioTitleLeading: NSLayoutConstraint!
    var radioPlayerType: RadioPlayerType = .Podcast
    var categories: [Category] = [Category]()
    var clickMoreEpisodesButton = false
    var hasLoaded = false
    var isAddTimer = false
    //ChannelDetail variable
    @IBOutlet weak var nextButtonWidth: NSLayoutConstraint!
    var radioChannelInfo:RadioChannelInfo = RadioChannelInfo()
    var radioLive:RadioLive = RadioLive()
    var radioEpisode:[RadioEpisode] = [RadioEpisode]()
    var playingRadioEpisode:RadioEpisode = RadioEpisode()
    var playingRadioEpisodeId:Int = 0
    var radioPlayerViewController:RadioPlayerViewController = UIStoryboard(name: "NewRadio", bundle: nil).instantiateViewController(withIdentifier: "RadioPlayerViewController") as! RadioPlayerViewController
    
    
    // Singleton ref to player
    let player: FRadioPlayer = FRadioPlayer.shared
    var currentChannel: RadioChannel = RadioChannel()
    var lastContentOffset: CGFloat = 0
    

    let categoryViewController: CategoryViewController = {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
    }()
    
    let radioChannelViewController: RadioChannelViewController = {
        return UIStoryboard(name: "NewRadio", bundle: nil).instantiateViewController(withIdentifier: "RadioChannelViewController") as! RadioChannelViewController
    }()
    
     // MARK: - CollectionView Delegate, Datasource and Layout
        static let channelDetailCell: String = "ChannelDetailCell"
        static let channelHeaderCell: String = "ChannelHeaderCell"
        static let airNowcell: String = "AirNowCell"
        static let headerViewCell: String = "HeaderViewCell"
        static let footerViewCell: String = "FooterViewCell"
        //var episodeList: BehaviorRelay<[Episode]> = BehaviorRelay<[Episode]>(value: [Episode]())
        private let interitemSpacing: CGFloat = 19
        private let lineSpacing: CGFloat = 10

        // MARK: - CollectionView Delegate, Datasource and Layout
        func numberOfSections(in collectionView: UICollectionView) -> Int{
            return 3
        }
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
            return (section == 2) ? self.radioEpisode.count : 1
        }
       
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            var cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: NewRadioViewController.channelDetailCell,
            for: indexPath)
            if(indexPath.section == 0){
                let channelHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: NewRadioViewController.channelHeaderCell,
                                                                                        for: indexPath) as! ChannelHeaderCell
                channelHeaderCell.configure(with: radioChannelInfo)
                channelHeaderCell.layoutIfNeeded()

                cell = channelHeaderCell
            }else if(indexPath.section == 1){
                let airNowCell = collectionView.dequeueReusableCell(withReuseIdentifier: NewRadioViewController.airNowcell,
                                                                                                     for: indexPath) as! AirNowCell
                airNowCell.layoutIfNeeded()
                airNowCell .configure(with: self.radioLive)
                airNowCell.backgroundColor = self.grayUIColor()
                airNowCell.isHidden = (self.radioLive.name == nil) ? true : false
                
                cell = airNowCell
            }else if(indexPath.section == 2){
                let channelDetailCell = collectionView.dequeueReusableCell(withReuseIdentifier: NewRadioViewController.channelDetailCell,
                                                                                                                for: indexPath) as! ChannelDetailCell
                let radioEpisode: RadioEpisode = self.radioEpisode[indexPath.item]
                channelDetailCell.layoutIfNeeded()
                channelDetailCell.backgroundColor = self.grayUIColor()
                channelDetailCell.configure(with: radioChannelInfo,radioEpisode: radioEpisode)
                cell = channelDetailCell
            }
            return cell
        }
        
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                 print("didSelectItemAt indexPath \(indexPath)")
            
                
            }
            
            // CollectionView flow layout
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                return interitemSpacing
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
                return lineSpacing
            }
            
            func collectionView(_ collectionView: UICollectionView,
                                layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {
                var height:CGFloat = 140.8
                if(indexPath.section == 0){
                    height = 3.3*32
                }else if(indexPath.section == 1){
                    height = 2.2*32
                    if(self.radioLive.name == nil){
                        height = 1
                    }
                }else if(indexPath.section == 2){
                    height = 4.4*32
                    if(self.radioEpisode.count == 0){
                        height = 0
                    }
                }
                return CGSize(width: collectionView.frame.width - interitemSpacing * 2, height: height)
            }
    
             func collectionView(_ collectionView: UICollectionView,
                                            viewForSupplementaryElementOfKind kind: String,
                                            at indexPath: IndexPath) -> UICollectionReusableView {
                   var reusableview:UICollectionViewCell!
                    
                   //分区头
                if kind == UICollectionView.elementKindSectionHeader{
                    let headerViewCell:HeaderViewCell = (collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                                         withReuseIdentifier: "HeaderViewCell", for: indexPath) as? HeaderViewCell)!
                  if(indexPath.section == 1){
                      headerViewCell.headerLabel.text = "Airing now"
                  } else if(indexPath.section == 2){
                     headerViewCell.headerLabel.text = "Podcast Episode"
                  }
                  reusableview = headerViewCell
                }
                
                if kind == UICollectionView.elementKindSectionFooter{
                    let footerViewCell:FooterViewCell  = (collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                                          withReuseIdentifier: "FooterViewCell", for: indexPath) as? FooterViewCell)!
                    reusableview = footerViewCell
                    let yourColor : UIColor = footerViewCell.moreEpisodersButton.titleLabel!.textColor
                    footerViewCell.moreEpisodersButton.layer.masksToBounds = true
                    footerViewCell.moreEpisodersButton.layer.borderColor = yourColor.cgColor
                    footerViewCell.moreEpisodersButton.layer.cornerRadius = 2.5
                    footerViewCell.moreEpisodersButton.layer.borderWidth = 1.0
                  
                }
                 
                   return reusableview
               }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
           
            var height:CGFloat = 0
            if(section == 0){
                height = 0
            }else if(section == 1){
                height = 1.5*32
                if(self.radioLive.name == nil){
                   height = 0
                }
            }else if(section == 2){
                height = 1.5*32
                if(self.radioEpisode.count == 0){
                   height = 0
                }
            }

               return CGSize(width: collectionView.frame.width, height: height)
           }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        var heightValue: CGFloat = 0.0
        if(!self.radioChannelInfo.isFavourite && RadioHelper.shared.selectedCategoryID.value == -99 && section == 2 && !clickMoreEpisodesButton && self.radioEpisode.count > 0){
            heightValue = 80.0 + 64
        }
         return CGSize(width: collectionView.frame.width, height:heightValue)
    }
    
        
        func bindDataSource(to collectionView: UICollectionView) {
               // Register Cell Nib for collectionView
            collectionView.register(UINib(nibName: NewRadioViewController.headerViewCell, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NewRadioViewController.headerViewCell)
            collectionView.register(UINib(nibName: NewRadioViewController.footerViewCell, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: NewRadioViewController.footerViewCell)
            collectionView.register(UINib(nibName: NewRadioViewController.channelDetailCell, bundle: nil),forCellWithReuseIdentifier: NewRadioViewController.channelDetailCell)
            collectionView.register(UINib(nibName: NewRadioViewController.channelHeaderCell, bundle: nil),forCellWithReuseIdentifier: NewRadioViewController.channelHeaderCell)
            collectionView.register(UINib(nibName: NewRadioViewController.airNowcell, bundle: nil),forCellWithReuseIdentifier: NewRadioViewController.airNowcell)// Set collectionView content inset
               collectionView.contentInset = UIEdgeInsets(top: 0,
                                                          left: 0,
                                                          bottom: 0,
                                                          right: 0)
               // Assign collectionView delegate and datasource
               collectionView.delegate = self
               collectionView.dataSource = self
               
               // Subscribe rx update from episodeList to perform reloadData
    //           self.episodeList.subscribe(onNext: { [weak collectionView, weak self] (episodeList) in
    //
    //               collectionView?.reloadData()
    //           }).disposed(by: self.disposeBag)
               
               print("did add RXObserver to collectionView")
           }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        if((self.player.radioURL) != nil){
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            self.radioPlayerView.isHidden = true
        }else if (scrollView.contentOffset.y < 0){
            //reach top
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            // move up
            self.radioPlayerView.isHidden = false
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y) {
           // move down
             self.radioPlayerView.isHidden = true
        }

        print("self.lastContentOffset",self.lastContentOffset)
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.radioPlayerViewController.newRadioViewController = self
        // Set the delegate for the radio player
        player.delegate = self
        setupRemoteTransportControls()
        self.bindDataSource(to: self.channelListDetailCollectionView)
        self.setUpRadioPlayerView()
        // Do any additional setup after loading the view.
        
        let notificationName = Notification.Name(rawValue: "ChannelListClick")
        var _ = NotificationCenter.default.rx
                   .notification(notificationName)
                   .takeUntil(self.rx.deallocated)
                   .subscribe(onNext: { notification in
                       
                    let userInfo = notification.userInfo as! [String: AnyObject]
                    let indexPath = userInfo["indexPath"] as! IndexPath
                    self.currentChannel = userInfo["channel"] as! RadioChannel
                    print("notification indexPath\(indexPath)")
                    print("notification currentChannel\(self.currentChannel)")
                    self.radioPlayerViewController.newRadioViewController = self
                    self.callAPiRefreshChannelData(getFullList:false)
                    self.showChannelListDetailCollectionView(show: true)
                              
                   })
        
        let notificationName2 = Notification.Name(rawValue: "PlayOrPauseMusic")
        var _ = NotificationCenter.default.rx
                   .notification(notificationName2)
                   .takeUntil(self.rx.deallocated)
                   .subscribe(onNext: { notification in
                       
                    let userInfo = notification.userInfo as! [String: AnyObject]
                    let play = userInfo["play"] as! Bool
                    if let type = userInfo["type"] as? String {
                        self.radioPlayerType = (type == "live") ? .Live : .Podcast
                    }
                    print("PlayOrPauseMusic play \(play)")
                    if(self.radioPlayerView.isHidden){
                        self.radioPlayerView.isHidden = false
                    }
                    
                    if(play){
                        
                        if let radioEpisode = userInfo["radioEpisode"] as? RadioEpisode {
                            if(self.player.radioURL != NSURL(string:radioEpisode.streamingUrl) as URL?){
                            self.player.radioURL = NSURL(string:radioEpisode.streamingUrl) as URL?
                            }
                            self.playingRadioEpisode = radioEpisode
                            self.playingRadioEpisodeId = radioEpisode.id
                           
                        }
                        if let radioLive = userInfo["radioLive"] as? RadioLive {
                            if(self.player.radioURL != NSURL(string:radioLive.streamingUrl!) as URL?){
                            self.player.radioURL = NSURL(string:radioLive.streamingUrl!) as URL?
                            }
                        }

                      self.player.play()
                        
                    } else {
                        self.player.pause()
                    }
                    
                    if(self.radioPlayerType == .Live){
                        self.setUpRadioPlayerViewLiveType()
                        //self.radioLive.play = play
                    } else {
                        
                        self.setUpRadioPlayerViewPodcastType()
                        self.playingRadioEpisode.play = play
                    }
                    self.radioPlayerViewController.play = play
                    self.channelListDetailCollectionView.reloadData()
                    self.updateNowPlaying()
                   
                   })
            
        let notificationName3 = Notification.Name(rawValue: "ClosePlayer")
        var _ = NotificationCenter.default.rx
            .notification(notificationName3)
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: { notification in
                self.radioPlayerView.isHidden = true
                self.player.radioURL = nil
                
            })
            
        let notificationName5 = Notification.Name(rawValue: "ShowLoading")
        var _ = NotificationCenter.default.rx
            .notification(notificationName5)
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: { notification in
                self.view.startLoading()
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                   self.view.cancelLoading()
                }
        })
        
        let notificationName6 = Notification.Name(rawValue: "HideLoading")
        var _ = NotificationCenter.default.rx
            .notification(notificationName6)
            .takeUntil(self.rx.deallocated)
            .subscribe(onNext: { notification in
                self.view.cancelLoading()
                
        })
        
        let notificationName7 = Notification.Name(rawValue: "RefreshChannelData")
               var _ = NotificationCenter.default.rx
                          .notification(notificationName7)
                          .takeUntil(self.rx.deallocated)
                          .subscribe(onNext: { notification in
                          self.callAPiRefreshChannelData(getFullList:self.clickMoreEpisodesButton)
                           
                                     
                          })

        let notificationName8 = Notification.Name(rawValue: "ClickMoreEpisodesButton")
        var _ = NotificationCenter.default.rx
                   .notification(notificationName8)
                   .takeUntil(self.rx.deallocated)
                   .subscribe(onNext: { notification in
                       
                    self.clickMoreEpisodesButton = true
                    print("ClickMoreEpisodesButton")
                    self.callAPiRefreshChannelData(getFullList: self.clickMoreEpisodesButton)
                              
                   })
        
    }
    
    func callAPiRefreshChannelData(getFullList:Bool) {
        RadioHelper.shared.getEpisodeList(channelId: self.currentChannel.id,getFullList: getFullList, success: { (Int, radioChannelInfo, radioLive, radioEpisode) in
                               self.radioChannelInfo = radioChannelInfo
                               self.radioLive = radioLive
                               self.radioEpisode = radioEpisode
                               
                               self.chanelDetailLabel.text = self.radioChannelInfo.name
                               self.channelListDetailCollectionView.reloadData()
                           }) { (Error) in
                               
                           }
    }
    
    func setUpRadioPlayerViewPodcastType(){
        self.liveButtonWidth.constant = 0
        self.radioTitleLeading.constant = 0
        self.radioPlayerView.layoutSubviews()
        let imageUrl = transformURLString(playingRadioEpisode.imageUrl)
        if let url = imageUrl?.url {
            self.playerImageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                self.updateNowPlaying()
            })
            
        }
        self.playerTitleLabel.text = playingRadioEpisode.name
        self.radioDescLabel.text = playingRadioEpisode.descriptions
        self.nextButton.isHidden = false
        self.previousButton.isHidden = false
        self.nextButtonWidth.constant = 40
    }
    
    func setUpRadioPlayerViewLiveType(){
        self.nextButtonWidth.constant = 0
        self.liveButtonWidth.constant = 30
        self.radioTitleLeading.constant = 8
        self.radioPlayerView.layoutSubviews()
        let imageUrl = transformURLString(radioChannelInfo.imageUrl)
        if let url = imageUrl?.url {
            self.playerImageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                self.updateNowPlaying()
            })
        }
        self.radioDescLabel.text = radioChannelInfo.subtitle
        self.playerTitleLabel.text = radioLive.name
        self.nextButton.isHidden = true
        self.previousButton.isHidden = true
        self.slider.value = Float(1.0)
        self.radioPlayerViewController.sliderValue = Float(1.0)
    }
    
    func setUpRadioPlayerView() {
        slider.minimumTrackTintColor = .red
        slider.maximumTrackTintColor = UIColor.init(rgb: 0xa6a6a6)
        slider.setThumbImage(UIImage(), for: .normal)
        slider.isUserInteractionEnabled = false
        radioPlayerView.clipsToBounds = true
        radioPlayerView.layer.cornerRadius = 5
        radioPlayerView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMinYCorner,.layerMaxXMaxYCorner,.layerMinXMinYCorner]
        let tapGesture = UITapGestureRecognizer()
        self.radioPlayerView.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.subscribe(onNext: { recognizer in
            self.radioPlayerViewController.radioPlayerType = self.radioPlayerType
            self.radioPlayerViewController.radioViewController = self
            for episode:RadioEpisode in self.radioEpisode {
                if(self.playingRadioEpisodeId == episode.id){
                    self.playingRadioEpisode = episode
                }
            }
            self.present(self.radioPlayerViewController, animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    @IBAction func pressPlayButton(_ sender: Any) {
        let play:Bool = playButton .setButtonPlayorPauseImage()
        let notificationName = Notification.Name(rawValue: "PlayOrPauseMusic")
              
        if(self.radioPlayerType == .Live){
            NotificationCenter.default.post(name: notificationName, object: self,
            userInfo: ["play":play,"type": "live","radioLive":self.radioLive])
        } else {
            playingRadioEpisode.play = play
            NotificationCenter.default.post(name: notificationName, object: self,
            userInfo: ["play":play,"type":"podcase","radioEpisode":self.playingRadioEpisode])
        }
       
    }
    
    @IBAction func pressPreviousButton(_ sender: Any) {
        let avPlayer = self.player.player
        guard (avPlayer?.currentItem?.duration) != nil else{
            return
        }
        
        let playerCurrentTime = CMTimeGetSeconds((avPlayer?.currentTime())!)
        var newTime = playerCurrentTime - 10
        
        if newTime < 0 {
            newTime = 0
        }
        let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        avPlayer?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    @IBAction func pressNextButton(_ sender: Any) {
        let avPlayer = self.player.player
        guard let duration  = avPlayer?.currentItem?.asset.duration else{
            return
        }
        let playerCurrentTime = CMTimeGetSeconds((avPlayer?.currentTime())!)
        let newTime = playerCurrentTime + 10

        if newTime < (CMTimeGetSeconds(duration) - 10) {

            let time2: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
            avPlayer?.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)

        }
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(!hasLoaded){
            self.setupViews()
            self.addRXObserver()
            self.loadCategoryList()
            hasLoaded = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        if(self.clickMoreEpisodesButton){
            
            self.clickMoreEpisodesButton = false
            self.callAPiRefreshChannelData(getFullList: self.clickMoreEpisodesButton)
        } else {
            
            RadioHelper.shared._radioChannel = RadioChannelList(categoryID: RadioHelper.shared.selectedCategoryID.value)
            RadioHelper.shared.getChannelList(radioChannel: RadioHelper.shared._radioChannel, success: { (categoryID, channelList) in
                RadioHelper.shared.updateChannelList(channelList: channelList, categoryID: categoryID)
                self.showChannelListDetailCollectionView(show: false)
                
                          
                           }) { (Error) in
                               
                           }
           
           
            
        }
    }
    
    func grayUIColor() -> UIColor {
        return UIColor(
            red: 28.0 / 255.0,
            green: 32.0 / 255.0,
            blue: 34.0 / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    deinit {
//     print("Deallocating \(self)")
        RadioHelper.shared.selectedCategoryID.accept(NSNotFound)
    }
    
    func transformURLString(_ string: String) -> URLComponents? {
        guard let urlPath = string.components(separatedBy: "?").first else {
            return nil
        }
        var components = URLComponents(string: urlPath)
        if let queryString = string.components(separatedBy: "?").last {
            components?.queryItems = []
            let queryItems = queryString.components(separatedBy: "&")
            for queryItem in queryItems {
                guard let itemName = queryItem.components(separatedBy: "=").first,
                      let itemValue = queryItem.components(separatedBy: "=").last else {
                        continue
                }
                components?.queryItems?.append(URLQueryItem(name: itemName, value: itemValue))
            }
        }
        return components!
    }
    
}

extension NewRadioViewController {
    
// MARK: - Configure Views
    private func setupViews() {
        self.loadCategoryView()
        self.loadRadioEpisodeView()
    }
    
    private func loadCategoryView() {
        // Add Child View Controller
        self.addChild(self.categoryViewController)
        self.categoryViewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let categoryVCView: UIView = self.categoryViewController.view
        self.categoriesView.addSubview(categoryVCView)
        // Configure Child View
        categoryVCView.frame = self.categoriesView.bounds
        categoryVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.categoryViewController.didMove(toParent: self)
    }
    
    private func loadRadioEpisodeView() {
        radioChannelViewController.radioChannelDatasource.radioPlayerView = self.radioPlayerView
        // Add Child View Controller
        self.addChild(self.radioChannelViewController)
        self.radioChannelViewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let RadioEpisodeVCView: UIView = self.radioChannelViewController.view
        self.radioContentView.addSubview(RadioEpisodeVCView)
        // Configure Child View
        RadioEpisodeVCView.frame = self.radioContentView.bounds
        RadioEpisodeVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.radioChannelViewController.didMove(toParent: self)
    }
}


extension NewRadioViewController {
    
// MARK: - Functional Methods
    func addRXObserver() {
        categoryViewController.selectedCategoryID.subscribe(onNext: { (categoryID) in
            print("Category changed to: \(categoryID)")
            RadioHelper.shared.selectedCategoryID.accept(categoryID)
        }).disposed(by: self.disposeBag)
        
    }
    
    func setDefaultCategory() {
        if self.categories.count > 0 {
            let category: Category = categories[0]
            self.categoryViewController.setCategory(cat: category)
        }
    }
    
    func loadCategoryList() {
        RadioHelper.shared.getCategoryList(type: "radio",
                                            success: { [weak self] (categories) in
                                                self?.categories = categories
                                                if categories.count > 0 {
                                                    self?.categoryViewController.category = categories[1]
                                                }
                                                self?.categoryViewController.categoryList = categories
            },
                                            failure: nil)
    }
    
    func reloadContent() {
        self.radioChannelViewController.reloadContent()
    }
    
    func showChannelListDetailCollectionView(show: Bool){
        self.channelListDetailCollectionView.backgroundColor = .clear
        self.channelListDetailCollectionView.isHidden = !show
        self.radioChannelViewController.view.isHidden = show
        self.categoryViewController.view.isHidden = show
        self.chanelDetailLabel.isHidden = !show
        self.backImageView.isHidden = !show
        self.backButton.isHidden = !show
        self.radioContentView.isHidden = show
        self.categoriesView.isHidden = show
        self.channelListDetailCollectionView.reloadData()
    }
    
}

// MARK: - FRadioPlayerDelegate

extension NewRadioViewController: FRadioPlayerDelegate {

    func radioPlayer(_ player: FRadioPlayer, playerStateDidChange state: FRadioPlayerState) {
        //playerTitleLabel.text = state.description
    }
    
    func radioPlayer(_ player: FRadioPlayer, playbackStateDidChange state: FRadioPlaybackState) {
       
        if(player.isPlaying){
            playButton.setImage(UIImage(named: "pause") , for: .normal)
            if(!isAddTimer&&self.radioPlayerType == .Podcast){
                 let avPlayer = self.player.player
                                              avPlayer?.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main, using: { time in
                                                  if let duration = avPlayer?.currentItem?.duration {
                                                      let duration = CMTimeGetSeconds(duration), time = CMTimeGetSeconds(time)
                                                      let progress = (time/duration)
                                                      
                                                      print("player progress:",progress)
                                                      self.slider.value = Float(progress)
                                                      self.radioPlayerViewController.sliderValue = Float(self.slider.value)
                                                      
                                                  }
                                              })
            }
            
        } else if(playButton.currentImage == UIImage(named: "pause")){
            playButton.setImage(UIImage(named: "play"), for: .normal)
            
        }
        
    }
    
    func radioPlayer(_ player: FRadioPlayer, metadataDidChange artistName: String?, trackName: String?) {
        //track = Track(artist: artistName, name: trackName)
    }
    
    func radioPlayer(_ player: FRadioPlayer, itemDidChange url: URL?) {
        //track = nil
    }
    
    func radioPlayer(_ player: FRadioPlayer, metadataDidChange rawValue: String?) {
       // infoContainer.isHidden = (rawValue == nil)
    }
    
    func radioPlayer(_ player: FRadioPlayer, artworkDidChange artworkURL: URL?) {
        
        // Please note that the following example is for demonstration purposes only, consider using asynchronous network calls to set the image from a URL.
        guard let artworkURL = artworkURL, let data = try? Data(contentsOf: artworkURL) else {
//
            return
        }

    }
}


// MARK: - Remote Controls / Lock screen

extension NewRadioViewController {
    
    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()
        
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.playCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.player.play()
            return .success
        }
        commandCenter.pauseCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.player.pause()
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            if(self?.radioPlayerType == .Podcast){
                self?.pressNextButton(self!.nextButton!)
            }
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            if(self?.radioPlayerType == .Podcast){
                self?.pressPreviousButton(self!.previousButton!)
            }
            return .success
        }
        
    }
    
    func updateNowPlaying() {
    
        // Define Now Playing Info
        var nowPlayingInfo = [String : Any]()
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = self.playerTitleLabel.text
        nowPlayingInfo[MPMediaItemPropertyArtist] = (self.radioPlayerType == .Live) ? "live": self.playerTitleLabel.text
        
        if let image = self.playerImageView.image {
            nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size, requestHandler: { _ -> UIImage in
                return image
            })
        }
        
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
}

public extension UIButton {
    func setButtonPlayorPauseImage() -> Bool {
        var play:Bool = true
        if(self.currentImage == UIImage(named: "play")){
            self.setImage(UIImage(named: "pause") , for: .normal)
            play = true
        } else if(self.currentImage == UIImage(named: "pause")){
            self.setImage(UIImage(named: "play"), for: .normal)
            play = false
        } else {
            self.setImage(UIImage(named: "pause") , for: .normal)
            play = true
        }
        return play
    }
    
    func setButtonLikeorUnLikeImage() -> Bool {
        var like:Bool = true
        if(self.currentImage == UIImage(named: "wishlist-on")){
           // self.setImage(UIImage(named: "wishlist-off") , for: .normal)
            like = false
        } else if(self.currentImage == UIImage(named: "wishlist-off")){
          //  self.setImage(UIImage(named: "wishlist-on"), for: .normal)
            like = true
        } else {
           // self.setImage(UIImage(named: "wishlist-off") , for: .normal)
            like = false
        }
        return like
    }
    
    func setButtonPlayorPauseImageFromPlay(play:Bool){
        if(play){
            self.setImage(UIImage(named: "pause") , for: .normal)
        } else {
            self.setImage(UIImage(named: "play"), for: .normal)
        }
    }

}
