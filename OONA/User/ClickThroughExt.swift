//
//  ClickThroughExt.swift
//  OONA
//
//  Created by Mike Wong on 30/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
class ClickThroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}
