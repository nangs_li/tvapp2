//
//  EditProfileViewController.swift
//  OONA
//
//  Created by Jack on 2/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import CropViewController
import SwiftyJSON
import Alamofire

class EditProfileViewController : BaseViewController,CropViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func backToMenuAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func finishEditingAction(_ sender: Any) {
        self.submit()
    }
    
    var userProfile : UserProfile?
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var pickerData: [String] = [String]()
    
    let datePicker = UIDatePicker()
    var email : String = ""
    var password : String = ""
    var gender : Gender = .Male
    var userActionType : UserActionType!
    
    var newImage : UIImage = UIImage()
    
    var dateSelected : String = ""
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: GradientButton!
    
    @IBOutlet weak var editProfileLabel: UILabel!
    @IBOutlet weak var profilePicDescLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var genderDescLabel: UILabel!
    @IBOutlet weak var birthdayDescLabel: UILabel!
    
    @IBOutlet weak var txtDatePicker: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var genderBtn: UIButton!
    @IBOutlet weak var genderTxt: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBAction func chooseImageButtonAction(_ sender: Any) {
        self.chooseImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerData = [LanguageHelper.localizedString("male"),
                      LanguageHelper.localizedString("female"),
                      LanguageHelper.localizedString("other")]
        genderBtn.titleLabel?.lineBreakMode = .byClipping
        
//        let attributes = [
//            NSAttributedString.Key.foregroundColor: UIColor.white,
//            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 12)!
//        ]
        
        //txtDatePicker.attributedPlaceholder = NSAttributedString(string: "Birthday", attributes:attributes)
        
        let bottomLine = CALayer()
        
        txtDatePicker.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtDatePicker.layer.addSublayer(bottomLine)
        txtDatePicker.layer.masksToBounds = true
        
        txtFirstName.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtFirstName.layer.addSublayer(bottomLine)
        txtFirstName.layer.masksToBounds = true
        
        txtLastName.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtLastName.layer.addSublayer(bottomLine)
        txtLastName.layer.masksToBounds = true
        
        genderTxt.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        genderTxt.layer.addSublayer(bottomLine)
        genderTxt.layer.masksToBounds = true
        
        setupDatePicker()
        loadProfileData()
    }
    
    
    func loadProfileData(){
        if let _profile =  UserHelper.shared.currentUser.profile{
            
            /*@IBOutlet weak var txtFirstName: UITextField!
             @IBOutlet weak var txtLastName: UITextField!
             @IBOutlet weak var txtGender: UITextField!
             @IBOutlet weak var txtBirthday: UITextField!*/
            profileImage.sd_setImage(with: URL.init(string: _profile.imageUrl ?? ""), completed: nil)
            txtFirstName.text = _profile.firstName
            txtLastName.text = _profile.lastName
            if let _gender = _profile.gender{
                if(_gender=="M"){
                    gender = .Male
                    genderTxt.text = LanguageHelper.localizedString("male")
                }else if(_gender=="F"){
                    gender = .Female
                    genderTxt.text = LanguageHelper.localizedString("female")
                }else{
                    gender = .Other
                    genderTxt.text = LanguageHelper.localizedString("other")
                }
            }
            
            if let _birthdate = _profile.birthDate{
                if(_birthdate != ""){
                    txtDatePicker.text = _birthdate
                    dateSelected = _birthdate
                }else{
                    txtDatePicker.text = LanguageHelper.localizedString("pick_a_date")
                }
            }
        }
    }
    
    
    //FOR  GENDER
    
   
    
    
    @IBAction func genderTap(_ sender: Any) {
        picker = UIPickerView.init()
        
        self.picker.delegate = self
        self.picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height * 0.4, width: UIScreen.main.bounds.size.width, height: 250)
        
        self.view.addSubview(picker)
        
        //ToolBar
        toolBar.sizeToFit()
        //   datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        
        let doneButton = UIBarButtonItem(title: LanguageHelper.localizedString("done").capitalizingFirstLetter(),
                                         style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: LanguageHelper.localizedString("cancel").capitalizingFirstLetter(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height * 0.4, width: UIScreen.main.bounds.size.width, height: 50))
        //toolBar.barStyle = .blackTranslucent
        toolBar.items = [UIBarButtonItem.init(title: LanguageHelper.localizedString("done").capitalizingFirstLetter(),  style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
        
        //auto set female
        picker.selectRow(1, inComponent:0, animated:true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onDoneButtonTapped() {
        let selectedValue = pickerData[picker.selectedRow(inComponent: 0)]
        genderBtn.titleLabel?.text = selectedValue
        genderTxt.text = selectedValue
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        self.view.endEditing(true)
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("gender selected:",row)
    }
    
    //END OF PART FOR GENDER
    
    //Part for birthday picker
    func setupDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //   datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        
        let doneButton = UIBarButtonItem(title: LanguageHelper.localizedString("done").capitalizingFirstLetter(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: LanguageHelper.localizedString("cancel").capitalizingFirstLetter(), style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtDatePicker.text = formatter.string(from: datePicker.date)
        dateSelected = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        print("txtDatePicker selected:",datePicker.date)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func next(_ sender: Any) {
        
        var firstName : String = ""
        var lastName : String = ""
        var gender : String = ""
        var date : String = ""
        var phone : String = ""
        //register
        if let _firstName = txtFirstName.text{
            firstName = _firstName
        }
        if let _lastName = txtLastName.text{
            lastName = _lastName
        }
        if let _gender = genderTxt.text{
            if(_gender=="Male"){
                
                gender = "M"
                
            }else if(_gender=="Female"){
                
                gender = "F"
                
            }else{
                
                gender = _gender
            }
            
        }
        
        if let _date = txtDatePicker.text{
            date = _date
        }
        
        if(self.userActionType == .register){
            UserHelper.shared.Register(firstName: firstName, lastName: lastName, email: email, phone: phone, gender: gender, birthDate: date, password: password, success: { userId in
                //if let _email = email{
                self.showConfirmation(userId : userId, email: self.email)
                //}
                
            }, failure: { error, code, message in
                if let _msg : String = message as? String{
                    self.showAlert(title: "", message : _msg)
                }
            })
            
        }else{
            //update user info..
            
            UserHelper.shared.updateUserProfile(photo : "" , firstName: firstName, lastName: lastName, email: email, phone: phone, gender: gender, birthDate: date, success : {
                self.showAlert(title: "", message : "Update successfully ")
                
            } , failure :{error , code, message in
                self.showAlert(title: "", message : message as! String)
            })
            
            /*
             UserHelper.shared.register(firstName: firstName, lastName: lastName, gender: gender, birthDate: date, phone: phone, email: email, password: password, success: { user in
             if let userId = user.userId{
             self.showConfirmation(userId : userId)
             
             }
             }, failure: { error, code, message in
             if let _msg : String = message as? String{
             self.showAlert(title: "", message : _msg)
             }
             })*/
            
        }
    }
    func chooseImage() {
        
        
        let picker = MyImagePickerViewController()
        picker.delegate = self
        
        let actionSheet = UIAlertController(title: "Choose photo", message: nil, preferredStyle: .actionSheet)
        
        let gallery = UIAlertAction.init(title:  "Choose from gallery", style: .default) { (action) in
            picker.sourceType = .photoLibrary
            picker.navigationBar.barStyle = .blackTranslucent
            picker.navigationBar.tintColor = UIColor.white
            picker.navigationBar.barTintColor = Color.redOONAColor()
            picker.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white
            ]
            
            self.present(picker, animated: true)
        }
        
        let takePhoto = UIAlertAction.init(title:  "Take a photo", style: .default) { (action) in
            picker.sourceType = .camera
            AppDelegate.isUsingCamera = true
            self.present(picker, animated: true)
        }
        let cancel = UIAlertAction.init(title:  "Cancel", style: .cancel) { (action) in
//            picker.sourceType = .camera
//            AppDelegate.isUsingCamera = true
//            self.present(picker, animated: true)
        }
        actionSheet.addAction(gallery)
        actionSheet.addAction(takePhoto)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true)
            
        

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        AppDelegate.isUsingCamera = false
        
        picker.dismiss(animated: true) {
            if let image = info[.originalImage] as? UIImage {
                self.cropImage(image: image)
                print("image found")
                //do something with an image
                
            } else {
                print("Not able to get an image")
            }
            
        }
        
    }
    func submit() {
        var genderText = ""
        switch gender {
        case .Male :
            genderText = "M"
        case .Female:
            genderText = "F"
        case .Other:
            genderText = "O"
        default :
            genderText = ""
        }
        let param = ["firstName":txtFirstName.text ?? "",
                     "lastName":txtLastName.text ?? "",
                     "gender":genderText ,
                     "birthDate":txtDatePicker.text ?? "",
                     ]
        
        
        let firstName = txtFirstName.text!
        let lastName  = txtLastName.text!
        
        //  Converted to Swift 5 by Swiftify v5.0.30213 - https://objectivec2swift.com/
        let timestamp = Int(Date().timeIntervalSince1970)
        let name = "oona-profile"
        let imagename = "\(name)-\(timestamp).png"
        let dateString = dateSelected
        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append((firstName.data(using: String.Encoding.utf8, allowLossyConversion: false))!, withName: "firstName")
                if (self.newImage.pngData() != nil) {
                    multipartFormData.append(self.newImage.pngData()!, withName: "photo", fileName: imagename, mimeType: "image/png")
                }
                multipartFormData.append((lastName.data(using: String.Encoding.utf8, allowLossyConversion: false))!, withName: "lastName")
                multipartFormData.append((genderText.data(using: String.Encoding.utf8, allowLossyConversion: false))!, withName: "gender")
                multipartFormData.append((dateString.data(using: String.Encoding.utf8, allowLossyConversion: false))!, withName: "birthDate")
//                multip
        },usingThreshold: UInt64.init(),
            to: APPURL.UpdateUserProfile,
            method: .post, headers: OONAApi.shared.requestHeader(),
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseData { response in
                        debugPrint(response)
                        self.refreshUserDatas()
                        self.dismiss(animated: true, completion: nil)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    
                }
        }
        )
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        AppDelegate.isUsingCamera = false
        picker.dismiss(animated: true, completion: nil)
    }
    func cropImage(image:UIImage) {
         // Load an image
        let cropViewController = CropViewController(croppingStyle: .circular, image: image)
        cropViewController.delegate = self
        self.present(cropViewController, animated: true, completion: nil)
    }
    
   
    func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        newImage = image
        profileImage.image = image
        cropViewController.dismiss(animated: true, completion: nil)
        print("cropped 4")
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        newImage = image
        profileImage.image = image
        cropViewController.dismiss(animated: true, completion: nil)
        print("cropped 5")
    }
    func showConfirmation(userId: Int, email: String){
        
        let signUpConfirmationViewController : ConfirmationViewController  =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
        signUpConfirmationViewController.userId = userId;
        signUpConfirmationViewController.email = email;
        self.navigationController?.pushViewController(signUpConfirmationViewController, animated: true)
        
    }
    
    override func appLanguageDidUpdate() {
        self.backButton.setTitle(LanguageHelper.localizedString("back_to_profile_text").escapeHTMLEntities(),
                                 for: .normal)
        self.confirmButton.setTitle(LanguageHelper.localizedString("save"),
                                    for: .normal)
        self.editProfileLabel.text = LanguageHelper.localizedString("edit_profile_text")
        self.profilePicDescLabel.text = LanguageHelper.localizedString("change_profile_photo_text")
        
        self.firstNameLabel.text = LanguageHelper.localizedString("first_name")
        self.lastNameLabel.text = LanguageHelper.localizedString("last_name")
        self.genderDescLabel.text = LanguageHelper.localizedString("gender_text")
        self.birthdayDescLabel.text = LanguageHelper.localizedString("birthday_text")
        
        self.loadProfileData()
        pickerData = [LanguageHelper.localizedString("male"),
                      LanguageHelper.localizedString("female"),
                      LanguageHelper.localizedString("other")]
        
    }

    
}
extension UIImagePickerController {
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape // Since your app is in only landscape mode
    }
}
class MyImagePickerViewController: UIImagePickerController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override public var shouldAutorotate: Bool {
        return false
    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape // Since your app is in only landscape mode
    }
    
}
