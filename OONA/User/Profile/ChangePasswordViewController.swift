//
//  ChangePasswordViewController.swift
//  OONA
//
//  Created by Jack on 3/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
class ChangePasswordViewController : BaseViewController  {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButtom: GradientButton!
    
    @IBOutlet weak var changePasswordTitleLabel: UILabel!
    
    @IBOutlet weak var currentPasswordLabel: UILabel!
    @IBOutlet weak var currentPasswordField: BorderedTextField! {
        didSet {
            currentPasswordField.setItalicPlaceHolder(text: LanguageHelper.localizedString("current_password_text"))
            currentPasswordField.rx.controlEvent(.editingChanged).subscribe(onNext: { [unowned self] in
                let text : String = self.currentPasswordField.text ?? ""
                if (text.count == 0) {
                    self.currentPasswordLabel.isHidden = true
                }else{
                    self.currentPasswordLabel.isHidden = false
                }
            }).disposed(by: disposeBag)
        }
    }
    
    @IBOutlet weak var newPasswordLabel: UILabel!
    @IBOutlet weak var newPasswordField: BorderedTextField!{
        didSet {
            newPasswordField.setItalicPlaceHolder(text: LanguageHelper.localizedString("new_password_text"))
            newPasswordField.rx.controlEvent(.editingChanged).subscribe(onNext: { [unowned self] in
                let text : String = self.newPasswordField.text ?? ""
                if (text.count == 0) {
                    self.newPasswordLabel.isHidden = true
                }else{
                    self.newPasswordLabel.isHidden = false
                }
            }).disposed(by: disposeBag)
        }
    }
    
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var confirmPasswordFIeld: BorderedTextField!{
        didSet {
            confirmPasswordFIeld.setItalicPlaceHolder(text: LanguageHelper.localizedString("retype_password_text"))
            confirmPasswordFIeld.rx.controlEvent(.editingChanged).subscribe(onNext: { [unowned self] in
                let text : String = self.confirmPasswordFIeld.text ?? ""
                if (text.count == 0) {
                    self.confirmPasswordLabel.isHidden = true
                }else{
                    self.confirmPasswordLabel.isHidden = false
                }
            }).disposed(by: disposeBag)
        }
    }
    
    
    
    override func awakeFromNib() {
         super.awakeFromNib()
        
    }
    
    
    
    @IBAction func backToMenuAction(_ sender: Any) {
        self.exit()
        
    }
    func exit() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func appLanguageDidUpdate() {
        
        currentPasswordField.setItalicPlaceHolder(text: LanguageHelper.localizedString("current_password_text"))
        newPasswordField.setItalicPlaceHolder(text: LanguageHelper.localizedString("new_password_text"))
        confirmPasswordFIeld.setItalicPlaceHolder(text: LanguageHelper.localizedString("retype_password_text"))
        
        self.changePasswordTitleLabel.text = LanguageHelper.localizedString("change_password_text")
        self.currentPasswordLabel.text = LanguageHelper.localizedString("current_password_text")
        self.newPasswordLabel.text = LanguageHelper.localizedString("new_password_text")
        self.confirmPasswordLabel.text = LanguageHelper.localizedString("retype_password_text")
        self.backButton.setTitle(LanguageHelper.localizedString("back_text").capitalizingFirstLetter(), for: .normal)
        self.confirmButtom.setTitle(LanguageHelper.localizedString("confirm_text").capitalizingFirstLetter(), for: .normal)
    }
    
    @IBAction func finishEditingAction(_ sender: Any) {
        
        let param : [String : String] = ["currentPassword" : currentPasswordField.text ?? "" , "newPassword" : newPasswordField.text ?? "" ]
        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)
        OONAApi.shared.postRequest(url: APPURL.ChangePassword, parameter: encryptedParam!)
            .responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let JSON : NSDictionary = response.result.value as? NSDictionary {
                        let code : Int? = JSON["code"] as? Int
                        let status : String? = JSON["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    
                                    self.exit()
                                   self.showAlert(title: "",message: "Your password has been changed.")
                                    
                                }else{
                                    if (code == 2022){
                                        //[self showErrorWithMessage:str(@"resend_verify_code")];
//                                        self.showAlert(title: "",message:  Localizer.shared.localized("resend_verify_code"))
                                        
                                    }else{
                                        if let _message = message{
                                            self.showAlert(title: "",message: _message)
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
    
    
    
}
