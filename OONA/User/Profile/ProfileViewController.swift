//
//  File.swift
//  OONA
//
//  Created by Jack on 2/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import  UIKit 
class ProfileViewController : BaseViewController  {
    typealias ProfilePageCompletionBlock = (() ->())
    var completionBlock: ProfilePageCompletionBlock?
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var changePasswordButton: UIButton!{
        didSet {
            changePasswordButton.layer.borderColor = UIColor.white.cgColor
            changePasswordButton.layer.borderWidth = 1.0
            changePasswordButton.layer.cornerRadius = 10.0
        }
    }
    @IBOutlet weak var logoutButton: UIButton!{
        didSet {
            logoutButton.layer.borderColor = UIColor.white.cgColor
            logoutButton.layer.borderWidth = 1.0
            logoutButton.layer.cornerRadius = 10.0
        }
    }
    @IBOutlet weak var nameDescLabel: UILabel!
    @IBOutlet weak var birthdatDescLabel: UILabel!
    @IBOutlet weak var genderDescLabel: UILabel!
    
    @IBOutlet weak var loginEmailDescLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func logoutAction(_ sender: Any) {
        
    }
    
    
    
    @IBAction func changePasswordAction(_ sender: Any) {
        self.showChangePassword()
    }
    
    @IBOutlet weak var yourProfileLb: UILabel!{
        didSet {
            yourProfileLb.text = LanguageHelper.localizedString("your_profile_text")
        }
        
    }
    
    @IBOutlet weak var userNameLb: UILabel!
    @IBOutlet weak var userBirthdayLb: UILabel!
    @IBOutlet weak var userGenderLb: UILabel!
    @IBOutlet weak var userEmailLb: UILabel!
    

    @IBOutlet weak var editProfileButton: UIButton!{
        didSet {
            editProfileButton.layer.borderColor = UIColor.white.cgColor
            editProfileButton.layer.borderWidth = 1.0
            editProfileButton.layer.cornerRadius = 10.0
        }
    }
    @IBAction func referCodeAction(_ sender: Any) {
        self.showReferralCodeEnterVC()
    }
    
    @IBAction func editProfileAction(_ sender: Any) {
        showEditProfile()
    }
    
    @IBAction func backToMenuAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.completionBlock?()
        }
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        UserHelper.shared.cleanUserData()
        self.dismiss(animated: true, completion: {
            self.completionBlock?()
        })
    }
    override func awakeFromNib() {
         super.awakeFromNib()
        
    }
    override func viewDidLoad () {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        self.updateUserViews()
        self.refreshUserDatas()
    }
    
    override func updateUserViews() {
//        UserHelper.shared.currentUser?.profile.
        print("update user views")
        let userProfile : UserProfile? = (UserHelper.shared.currentUser.profile)
        let firstName = userProfile?.firstName ?? ""
        let lastName = userProfile?.lastName ?? ""
        let fullName : String =  ( firstName + " " + lastName )
        
        let genderText = ( userProfile?.gender?.lowercased() == "m" ? LanguageHelper.localizedString("male_text") : LanguageHelper.localizedString("female_text"))
        userNameLb.text = fullName
        userBirthdayLb.text = userProfile?.birthDate
        userGenderLb.text = userProfile?.gender
        userEmailLb.text = userProfile?.email
        profileImage.sd_setImage(with: URL.init(string: userProfile?.imageUrl ?? ""), completed: nil)
        
    }
    
    override func appLanguageDidUpdate() {
        self.yourProfileLb.text = LanguageHelper.localizedString("your_profile_text")
        self.editProfileButton.setTitle(LanguageHelper.localizedString("edit_profile_text"), for: .normal)
        
        self.nameDescLabel.text = LanguageHelper.localizedString("name_text")
        
        self.birthdatDescLabel.text = LanguageHelper.localizedString("birthday_text")
        self.genderDescLabel.text = LanguageHelper.localizedString("gender_text")
        //TODO: Localization
//        self.loginEmailDescLabel.text = LanguageHelper.localizedString("gender_text")
        
        self.backButton.setTitle(LanguageHelper.localizedString("back_to_menu_text").escapeHTMLEntities(),
                                 for: .normal)
        self.changePasswordButton.setTitle(LanguageHelper.localizedString("change_password_text"),
                                 for: .normal)
        self.logoutButton.setTitle(LanguageHelper.localizedString("log_out_text"),
                                 for: .normal)
    }
}
