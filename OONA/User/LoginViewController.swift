//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import RxLocalizer


class LoginViewController: BaseViewController{ //actually.. login and register and confirm
    @IBOutlet weak var email: UITextField!{
        didSet {
            email.rx.controlEvent(.editingChanged).subscribe(onNext: { [unowned self] in
                let text : String = self.email.text ?? ""
                if (text.count == 0) {
                    self.emailLabel.isHidden = true
                }else{
                    self.emailLabel.isHidden = false
                }
            }).disposed(by: disposeBag)
        }
    }
    @IBOutlet weak var password: UITextField!{
        didSet {
            password.rx.controlEvent(.editingChanged).subscribe(onNext: { [unowned self] in
                let text : String = self.password.text ?? ""
                if (text.count == 0) {
                    self.passwordLabel.isHidden = true
                }else{
                    self.passwordLabel.isHidden = false
                }
            }).disposed(by: disposeBag)
        }
    }
    @IBOutlet weak var errEmail: UILabel!
    @IBOutlet weak var errPassword: UILabel!
    @IBOutlet weak var leftTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var upperPasswordButton: UIButton!
    
    @IBOutlet weak var passwordButton: UIButton!
    
    var userActionType : UserActionType = UserActionType.none
    var isConfirmation : Bool = false //is a confirmation case..
    var confirmEmail = "" //to be used in confirmation case..
    var confirmPassword = ""  //to be used inconfirmation case..
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadViewIfNeeded()
//        self.setupUIs()
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setupUIs()
    }
    public func setupUIs() {
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 13)!
        ]
        let bottomLine = CALayer()
        
        let color = Color.darkRedColorWithHex()
        
        //        email.layer.addBorder(edge: .bottom, color: color, thickness: 1.0)
        //        email.layer.addSublayer(bottomLine)
        //        email.layer.masksToBounds = true
        //
        //        password.layer.addBorder(edge: .bottom, color: color, thickness: 1.0)
        //        password.layer.addSublayer(bottomLine)
        //        password.layer.masksToBounds = true
        //
        
        //        email.setItalicPlaceHolder(text: "New Password")
     
        
        //        email.setItalicPlaceHolder(text: "")
        
        
        //
        let colors : [CGColor] =  [Color.OONAButtonLeft().cgColor, Color.OONAButtonMid().cgColor, Color.OONAButtonRight().cgColor]
        //set up the words and stuff
        if(userActionType == .register){
            
            leftTitle.text = LanguageHelper.localizedString("email_sign_up_text")
            forgetPasswordButton.isHidden = true
            
            if(isConfirmation){
                
                backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(), for: .normal)
                confirmButton.setTitle(LanguageHelper.localizedString("confirm_text"), for: .normal)
                
                leftTitle.text = LanguageHelper.localizedString("confirmation_text")
                
                //                errEmail.text = "To make sure you don't type the wrong one"
                errEmail.textColor = UIColor.lightGray
                errEmail.isHidden = false
                //                errPassword.text = "In case you typed the wrong one ;)"
                errPassword.textColor = UIColor.lightGray
                errPassword.isHidden = false
                
                email.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("password"),
                    attributes:attributes)
                password.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("retype_password_text"), attributes:attributes)
                emailLabel.text = LanguageHelper.localizedString("password").capitalizingFirstLetter()
                passwordLabel.text = LanguageHelper.localizedString("retype_password_text")
                
                upperPasswordButton.isHidden = false
                passwordButton.isHidden = false
                email.isSecureTextEntry = true
                password.isSecureTextEntry = true
            }else{
                backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(), for: .normal)
                confirmButton.setTitle(LanguageHelper.localizedString("next_text"), for: .normal)
                
                
                upperPasswordButton.isHidden = true
                passwordButton.isHidden = true
                upperPasswordButton.setImage(UIImage(named: "eye_off"), for: .normal)
                upperPasswordButton.setImage(UIImage(named: "eye_on"), for: .selected)
                passwordButton.setImage(UIImage(named: "eye_off"), for: .normal)
                passwordButton.setImage(UIImage(named: "eye_on"), for: .selected)
                
                errEmail.text = LanguageHelper.localizedString("we_area_sending_you_a_verification_email_text")
                errEmail.textColor = UIColor.lightGray
                errEmail.isHidden = false
                errPassword.text = LanguageHelper.localizedString("minimum_eight_characters_text")
                errPassword.textColor = UIColor.lightGray
                errPassword.isHidden = false
                
                email.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("email").capitalizingFirstLetter(), attributes:attributes)
                password.attributedPlaceholder = NSAttributedString(string:LanguageHelper.localizedString("retype_email_text"), attributes:attributes)
                
                //TODO: Localization
//                LanguageHelper.localizedString(<#T##localizeKey: String##String#>)
                emailLabel.text = "Email address"
                
                passwordLabel.text = LanguageHelper.localizedString("retype_email_text")
                email.isSecureTextEntry = false
                password.isSecureTextEntry = false
            }
            
        }else{
            //login..
            self.leftTitle.text = LanguageHelper.localizedString("email_sign_up_text")
            backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(),
                                for: .normal)
            confirmButton.setTitle(LanguageHelper.localizedString("next_text"),
                                   for: .normal)
            self.forgetPasswordButton.setTitle(LanguageHelper.localizedString("forgot_password_text"),
                                               for: .normal)
            upperPasswordButton?.isHidden = true
            passwordButton?.isHidden = true
            
            upperPasswordButton.setImage(UIImage(named: "eye_off"), for: .normal)
            upperPasswordButton.setImage(UIImage(named: "eye_on"), for: .selected)
            passwordButton.setImage(UIImage(named: "eye_off"), for: .normal)
            passwordButton.setImage(UIImage(named: "eye_on"), for: .selected)
            
            email.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("type_registered_email_text"), attributes:attributes)
            password.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("enter_password_text"), attributes:attributes)
            
            errEmail.isHidden = true
            errPassword.isHidden = true
        }
    }
    @IBAction func transferCodeButton(_ sender: Any) {
        self.showTransferCodeEnterVC()
    }
    
    @IBAction func upperPasswordVisible(_ sender: Any) {
        email.isSecureTextEntry = !email.isSecureTextEntry
        upperPasswordButton.isSelected = email.isSecureTextEntry
    }
    @IBAction func passwordVisible(_ sender: UIButton) {
        password.isSecureTextEntry = !password.isSecureTextEntry
        passwordButton.isSelected = password.isSecureTextEntry
    }
    
    func login(){
        
        if let _email = email.text{
            if let _password = password.text{
                UserHelper.shared.loginWithEmail(email : _email, password: _password, success: { response in
                    //self.showAlert(title: "",message: "login success")
                    
                    self.loginSuccess()
                    
                }, failure: { errorType , code ,message  in
                    
                    if(errorType == .signupConfirmation){
                        //for login confirmation..
                        if let userId : Int = message as? Int{
                            if let email = self.email.text{
                                self.showConfirmation(userId: userId, email: email)
                            }
                        }
                        
                    }else if(errorType == .showError){
                        //for showing error..
                        
                        if (code == 1008){
                            if let _message = message{
                                self.showAlert(title: "", message: _message as! String)
                            }
                        }
                        else if (code == 2032){
                            //LanguageHelper.localizedString("you_entered_wrong_email_or_password_please_double_check_and_try_again")
                            self.showAlert(title: "", message:  LanguageHelper.localizedString("error_2032"))
                        }
                        else if (code == 2035) {
                            //LanguageHelper.localizedString("the_email_and_password_you_entered_did_not_match_our_records_please_double_check_and_try_again")
                            self.showAlert(title: "", message:  LanguageHelper.localizedString("error_2035"))
                        }else if (code == 2034) {
                            //LanguageHelper.localizedString("the_email_and_password_you_entered_did_not_match_our_records_please_double_check_and_try_again")
                            self.showAlert(title: "", message:  LanguageHelper.localizedString("error_2034"))
                        }
                        else{
                            if let _message = message{
                                self.showAlert(title: "", message: _message as! String)
                            }
                        }
                        
                    }
                })
            }else{
                //TODO: Localization
                // LanguageHelper.localizedString("Please enter password")
                self.showAlert(title: "",message: "Please enter password")
            }
        }else{
            //TODO: Localization
            // LanguageHelper.localizedString("Please enter Email")
            self.showAlert(title: "", message: "Please enter Email")
        }
    }
    
    func initView(){
        errEmail.isHidden = true
        errPassword.isHidden = true
        errEmail.isHidden = true
        errEmail.isHidden = true
    }
    
    //should be in utitity.. but whatever.
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    @IBAction func login(_ sender: UIButton) {
        initView()
        
        
        if(userActionType == .login){
            login()
        }else if(userActionType == .register){
            
            if(isConfirmation){
                //confirmation flow (check  pw )
                var hasError = false
                errEmail.text = ""
                errPassword.text = ""
                
                if(email.text != password.text){
                    email.textColor = UIColor.red
                    errEmail.text  = LanguageHelper.localizedString("you_typed_a_different_password_text")
                    errEmail.textColor = UIColor.red
                    errEmail.isHidden = false
                    hasError = true
                }

                
                print(email.text, " vs ", password.text)
                
                if(email.text == ""){
                    errEmail.text = LanguageHelper.localizedString("empty_password_text")
                    hasError = true
                    print("error 2")
                }
          
                
                if(password.text == ""){
                    errPassword.text = LanguageHelper.localizedString("empty_retype_password_text")
                    hasError = true
                    print("error 4")
                }else if(password.text?.count ?? 0 < 8){
                    errPassword.text = LanguageHelper.localizedString("password_length_error_text")
                    hasError = true
                    print("error 5")
                }
                if(!hasError){
                    
                    //step3 : enter your info
                    let yourInfovc = UIStoryboard(name: "User", bundle: nil).instantiateViewController(withIdentifier: "YourInfoViewController") as! YourInfoViewController
                    //yourInfovc.view.backgroundColor = .clear
                    yourInfovc.email = confirmEmail
                    yourInfovc.password = confirmPassword
                    
                    yourInfovc.userActionType = self.userActionType
                    yourInfovc.modalPresentationStyle = .overCurrentContext
                    yourInfovc.modalTransitionStyle = .crossDissolve
                    self.navigationController?.pushViewController(yourInfovc, animated: true)
                }else{
                    errEmail.isHidden = false
                    errPassword.isHidden = false
                }
                
            }else{
                // email case
                var isValid = true
                print(email.text, " vs ", password.text)
                if(email.text != password.text){
                    email.textColor = UIColor.red
                    errEmail.text  = LanguageHelper.localizedString("you_typed_a_different_email_text")
                    errEmail.textColor = UIColor.red
                    
                    isValid = false
                    print("error 1")
                }
                
                if(email.text == ""){
                    
                    errEmail.text = LanguageHelper.localizedString("empty_email_text")
                    isValid = false
                    print("error 2")
                }
                if let email = email.text{
                    if(!isValidEmail(emailStr: email)){
                        errEmail.text = LanguageHelper.localizedString("wrong_email_format_text")
                        isValid = false
                        print("error 3")
                    }
                }
                
                if(password.text == ""){
                    errPassword.text = LanguageHelper.localizedString("empty_email_text")
                    isValid = false
                    print("error 4")
                }else if(password.text?.count ?? 0 < 8){
                    errPassword.text = LanguageHelper.localizedString("password_length_error_text")
                    isValid = false
                    print("error 5")
                }
                
                if (isValid) {
                    //step2 : confirm email and password
                    let lvc : LoginViewController  =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    //lvc.userId = userId
                    lvc.userActionType = self.userActionType
                    lvc.isConfirmation = true
                    lvc.confirmEmail = email.text ?? ""
                    lvc.confirmPassword = password.text ?? ""
                    self.navigationController?.pushViewController(lvc, animated: true)
                }else{
                    errEmail.isHidden = false
                    errPassword.isHidden = false
                }
            }
        }
    }
    
    func showConfirmation(userId: Int, email: String){
        
        let signUpConfirmationViewController : ConfirmationViewController  =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
        signUpConfirmationViewController.userId = userId;
        signUpConfirmationViewController.email = email;
        self.navigationController?.pushViewController(signUpConfirmationViewController, animated: true)
        
    }
    
    @IBAction func backToMenu(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func forgetPassword(_ sender: UIButton) {
        let forgetPasswordViewController : ForgetPasswordViewController  =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        //forgetPasswordViewController.view.backgroundColor = .clear
        self.navigationController?.pushViewController(forgetPasswordViewController, animated: true)
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeLeft
        
    }
    override var shouldAutorotate: Bool {
        return false
    }
    
}
class BorderedTextField : UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 13)!
        ]
        let bottomLine = CALayer()
        
        let color = Color.darkRedColorWithHex()
        
        self.layer.addBorder(edge: .bottom, color: color, thickness: 1.0)
        self.layer.addSublayer(bottomLine)
        self.layer.masksToBounds = true
    }
    public func setItalicPlaceHolder(text:String) {
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 13)!
        ]
        let bottomLine = CALayer()
        
        let color = Color.darkRedColorWithHex()
        self.attributedPlaceholder = NSAttributedString(string: text, attributes:attributes)
    }
}
