//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import SwiftyJSON
class ForgetPasswordViewController: BaseViewController{
    @IBOutlet weak var passwordTitleLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var confirmButton: GradientButton!
    @IBOutlet weak var passwordFieldDescLabel: UILabel!
    @IBOutlet weak var email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 15)!
        ]
        
        email.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("type_registered_email_text"), attributes:attributes)
        
    }
    
    
    @IBAction func resetPasswordAction(_ sender: Any) {
        if (email.text!.count > 0) {
            OONAApi.shared.postRequest(url: APPURL.ForgetPassword, parameter: ["email":email.text])
                .responseJSON { (response) in
                    print("response forgetpw",response.result)

                    switch response.result {

                    case .success(let value):
                        let json = JSON(value)
                        if let code = json["code"].int,
                            let status = json["status"].string {
                            APIResponseCode.checkResponse(withCode: code.description,
                                                          withStatus: status,
                                                          completion: { (message, isSuccess) in
                                                            if(isSuccess){
    //                                                            self.refreshUserDatas()
                                                                self.navigationController?.popViewController(animated: true)
                                                            } else {

                                                            }

                            })
                        }

                    case .failure(let error):
                        print(error)

                    }
            }
        }
    }
    
    override func appLanguageDidUpdate() {
        self.passwordTitleLabel.text = LanguageHelper.localizedString("forgot_password_title_text")
        self.backButton.setTitle(LanguageHelper.localizedString("back_text").escapeHTMLEntities(),
                                 for: .normal)
        
        self.passwordFieldDescLabel.text = LanguageHelper.localizedString("we_will_send_you_a_new_password_text")
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 15)!
        ]
        
        email.attributedPlaceholder = NSAttributedString(string: LanguageHelper.localizedString("type_registered_email_text"),
                                                         attributes:attributes)
        self.confirmButton.setTitle(LanguageHelper.localizedString("reset_password_text"), for: .normal)
        //TODO: button layer need to update
//        self.confirmButton.layoutIfNeeded()
//        self.confirmButton.layer.layoutSublayers()
//        self.confirmButton.layer.displayIfNeeded()
    }
    
    @IBAction func backToMenu(_ sender: UIButton) {
        //TODO_OONA .. if this vc is presented , dismiss
        self.navigationController?.popViewController(animated: true)
    }
}
