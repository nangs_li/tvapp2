//
//  User.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct UserProfile : OONAModel{
    var code : String?
    var inviteCode : String?
    var inviteMessage : String?
    var parentInviteCode : String?
    var firstName : String?
    var lastName : String?
    var phone : String?
    var gender : String?
    var email : String?
    var birthDate : String?
    var imageUrl : String?
    var inviteImage : String?
    var watching : String?
    var dailyPoints : JSON?
    var reward : JSON?
    var rank : NSDictionary?
    var favouriteCategories : NSDictionary?
    
    var myPointBalance : Int?
    var totalDailyPoint: Int?
    var earnedDailyPoint: Int?
    var pointsExpireSoon: Int?
    var pointExpireTime: Double?
    var pointExpireMessage: String?
    
    
    
    /*
    "status" : "success",
    "code" : XX,
    "inviteCode" : "XXXXX",
    "parentInviteCode" : "XXXXX",
    "firstName" : "XXX",
    "lastName" : "XXX",
    "phone" : "XXXXXX",
    "gender" : "M",
    "email" : "XXX@XXX.com",
    "birthDate" : "2017-12-15",
    "imageUrl" : "http://www.xxx.com/xxx.jpg",
    "inviteImage": "https://www.xxx.com/xxx.jpg",
    "watching" : "24 hrs",
    "dailyPoints" : {
        "maximum" : XXXX,
        "earnedInPeriod" : XXX
    },
    "points" : {
        "balance" : XXX,
        "expireSoon" : XXX,
        "expireIn" : XXX,  (Unit : seconds)
    },
    "reward" : { (only appear when user upgrade to next level)
        "code" : XXXX,
        "points" : XXX,
    },
    "rank" : {
        "name" : "Rookie",
        "progress" : 40,
        "note" : "40 hours to\nPro level"
    },
    "favouriteCategories" : [1,6,13,16,14,3,4,5,15,7,8,2,10,9,11,12]
     */
    
    init(){
        
    }
    
    
    init(json: JSON) {
        
        if let _inviteCode: String = json["profile"]["inviteCode"].string {
            self.inviteCode = _inviteCode
        }
        
        if let _inviteMessage: String = json["profile"]["inviteMessage"].string {
            self.inviteMessage = _inviteMessage
        }
        if let _parentInviteCode: String = json["profile"]["parentInviteCode"].string {
            self.parentInviteCode = _parentInviteCode
        }
        
        if let _firstName: String = json["profile"]["firstName"].string {
            self.firstName = _firstName
        }
        
        if let _lastName: String = json["profile"]["lastName"].string {
            self.lastName = _lastName
        }
        
        if let _phone: String = json["profile"]["phone"].string {
            self.phone = _phone
        }
        
        if let _gender: String = json["profile"]["gender"].string {
            self.gender = _gender
        }
        if let _email: String = json["profile"]["email"].string {
            self.email = _email
        }
        if let _birthDate: String = json["profile"]["birthDate"].string {
            self.birthDate = _birthDate
        }
        
        if let _imageUrl: String = json["profile"]["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        
        if let _inviteImage: String = json["profile"]["inviteImage"].string {
            let url = URL.init(string: _inviteImage)
            SDWebImagePrefetcher.shared.prefetchURLs([url!])
            self.inviteImage = _inviteImage
        }
        
        if let _watching: String = json["watching"].string {
            self.watching = _watching
        }
        
        print(json["points"]["balance"])
        if let _points: Int = json["points"]["balance"].int {
            self.myPointBalance = _points
            print(_points)
        }
        
        print(json["points"]["expireSoon"])
        if let _expireSoon = json["points"]["expireSoon"].int {
            self.pointsExpireSoon = _expireSoon
            print(_expireSoon)
        }
        
        print(json["points"]["expireIn"])
        if let _expireIn = json["points"]["expireIn"].double {
            self.pointExpireTime = _expireIn
            print(_expireIn)
        }
        if let _expireMsg = json["points"]["expireMessage"].string {
            self.pointExpireMessage = _expireMsg
            print("expireMsg",_expireMsg)
        }
        self.dailyPoints = json["dailyPoints"]
        print(json["dailyPoints"])
        if let _dailyPoints: JSON = self.dailyPoints {
            if let _maximum = _dailyPoints["maximum"].int {
                self.totalDailyPoint = _maximum
                print(_maximum)
            }
            
            if let _earnedInPeriod = _dailyPoints["earnedInPeriod"].int {
                self.earnedDailyPoint = _earnedInPeriod
                print(_earnedInPeriod)
            }
        }
        
        
        
        if let _reward = json["reward"].dictionary {
            //if let _maximum = json["maximum"].string {
            //}
            //if let _earnedInPeriod = json["earnedInPeriod"].string {
            //}
        }
        
        if let _rank = json["rank"].dictionary {
            
            //if let _maximum = json["maximum"].string {
            //}
            //if let _earnedInPeriod = json["earnedInPeriod"].string {
            //}
            //if let _earnedInPeriod = json["earnedInPeriod"].string {
            //}
            
        }
        
        if let _favouriteCategories = json["favouriteCategories"].dictionary {
            //self.favouriteCategories = _favouriteCategories
            //if let _dailyPoints = json["dailyPoints"].dictionary {
            //}
        }
        
    }
}
