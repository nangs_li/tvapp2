//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import RxLocalizer

class ConfirmationViewController: BaseViewController, UITextFieldDelegate{
    
    var userId: Int!
    var email: String!
    var confirmButton: UIButton!
    var userActionType : UserActionType = UserActionType.none
    
    
    @IBOutlet weak var digitTextField1: UITextField!
    @IBOutlet weak var digitTextField2: UITextField!
    @IBOutlet weak var digitTextField3: UITextField!
    @IBOutlet weak var digitTextField4: UITextField!
    @IBOutlet weak var digitTextField5: UITextField!
    @IBOutlet weak var digitTextField6: UITextField!
    
    @IBOutlet weak var digitView1: UIView!
    @IBOutlet weak var digitView2: UIView!
    @IBOutlet weak var digitView3: UIView!
    @IBOutlet weak var digitView4: UIView!
    @IBOutlet weak var digitView5: UIView!
    @IBOutlet weak var digitView6: UIView!
    
    var currentPass : String?
    var newPass  : String?
    var currentPasswordValid : Bool = false
    var newPassValid : Bool  = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var bottomBorder = CALayer()
        let bottomLine = CALayer()
        
        digitTextField1.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField1.layer.addSublayer(bottomLine)
        
        digitTextField1.layer.masksToBounds = true // the most important line of code
        
        digitTextField2.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField2.layer.addSublayer(bottomLine)
        
        digitTextField2.layer.masksToBounds = true // the most important line of code
        
        digitTextField3.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField3.layer.addSublayer(bottomLine)
        
        digitTextField3.layer.masksToBounds = true // the most important line of code
        
        digitTextField4.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField4.layer.addSublayer(bottomLine)
        
        digitTextField4.layer.masksToBounds = true // the most important line of code
        
        digitTextField5.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField5.layer.addSublayer(bottomLine)
        
        digitTextField5.layer.masksToBounds = true // the most important line of code
        
        digitTextField6.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        
        digitTextField6.layer.addSublayer(bottomLine)
        
        digitTextField6.layer.masksToBounds = true // the most important line of code
        
        digitTextField1.delegate = self;
        digitTextField2.delegate = self;
        digitTextField3.delegate = self;
        digitTextField4.delegate = self;
        digitTextField5.delegate = self;
        digitTextField6.delegate = self;
        
        digitTextField1.tag = 1;
        digitTextField2.tag = 2;
        digitTextField3.tag = 3;
        digitTextField4.tag = 4;
        digitTextField5.tag = 5;
        digitTextField6.tag = 6;
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        self.confirm()
        return true
    }
    
    func keyboardInputShouldDelete(_ textField: UITextField?) -> Bool {
        // on deleteing value from Textfield
        
        let prevTag = (textField?.tag ?? 0) - 1
        // Try to find prev responder
        let prevResponder = textField?.superview?.viewWithTag(prevTag)
        if prevResponder == nil {
            return true
        }
        if prevResponder != nil {
            // Found next responder, so set it.
            prevResponder?.becomeFirstResponder()
        }
        textField?.text = ""
        //errorLabel.hidden = true TODO_OONA : error
        return true
    }
    

    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       // NSLog(@"%@ was pressed",string);
        var textCount = 0
        if let _textCount = textField.text?.count{
            textCount = _textCount
        }
        
        if ((textCount == 0) && (string.count > 0))
        {
            textField.text = string;
            let nextTag = textField.tag;
            enterField(tag: nextTag, string: string)
            return false
        }else if ((textCount >= 1) && (string.count > 0)){
            //FOR MAXIMUM 1 TEXT
            let nextTag = textField.tag + 1;
            enterField(tag: nextTag, string: string)
            
            return false
        }else if ((textCount >= 2) && (string.count == 0)){
            // on deleteing value from Textfield
            
            let prevTag = textField.tag - 1;
            // Try to find prev responder
            if let txt = self.view.viewWithTag(prevTag) as? UITextField {
                //Your code
                txt.becomeFirstResponder()
                textField.text = string;
            }
            return false
        }
        return true
    }
    
    func enterField(tag: Int, string: String){
        switch(tag){
            case 1:
                digitTextField2.becomeFirstResponder()
                digitTextField1.text = string
            case 2:
                digitTextField3.becomeFirstResponder()
                digitTextField2.text = string
            case 3:
                digitTextField4.becomeFirstResponder()
                digitTextField3.text = string
            case 4:
                digitTextField5.becomeFirstResponder()
                digitTextField4.text = string
            case 5:
                digitTextField6.becomeFirstResponder()
                digitTextField5.text = string
            case 6:
                digitTextField6.becomeFirstResponder()
                digitTextField6.text = string
                confirm()
            default:
                break;
        }
    }
    
    
    func reset() -> Void{
        digitTextField1.text = "";
        digitTextField1.becomeFirstResponder()
        digitTextField2.text = "";
        digitTextField3.text = "";
        digitTextField4.text = "";
        digitTextField5.text = "";
        digitTextField6.text = "";
    
    }
    
    func getPinCode()->String{
        let txt1 : String = digitTextField1.text ?? ""
        let txt2 : String  = digitTextField2.text ?? ""
        let txt3 : String  = digitTextField3.text ?? ""
        let txt4 : String  = digitTextField4.text ?? ""
        let txt5 : String  = digitTextField5.text ?? ""
        let txt6 : String  = digitTextField6.text ?? ""
        
        let pincode = txt1 + txt2 + txt3 + txt4 + txt5  + txt6
        
        return pincode
    }
    
    
    @IBAction func loginFacebook(_ sender: UIButton) {
        confirm()
    }

            
    func confirm(){
        
        let pincode = getPinCode()
        
        //let userId = UserHelper.shared.currentUser?.userId
        
        
        let param = ["id":self.userId, "code":pincode] as [String : Any]
        
        OONAApi.shared.postRequest(url: APPURL.Passcode, parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON : NSDictionary = response.result.value as? NSDictionary {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                if let accessToken : String =  JSON["accessToken"] as? String{
                                    
                                    let user = User.init(userId: self.userId, accessToken: accessToken, inviteCode: "", expiresOn: nil, setLoginByEmail: true)
                                    
                                    UserHelper.shared.currentUser = user
                                    
                                    //if let user = UserHelper.shared.currentUser{
                                    UserHelper.shared.getUserProfile(success: {userProfile in
                                        UserHelper.shared.login()
                                        self.loginSuccess()
                                    }, failure: {code , msg in
                                        
                                    })
                                    //}
                                }
                                /*
                                [UserData setUserToken:[responseObject valueForKey:@"accessToken"]];
                                    [UserData setLoginByEmail:YES];
                                    
                                    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
                                    
                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                    UIViewController *viewController = [storyboard instantiateInitialViewController];
                                    LandscapeViewController *navCon = [[LandscapeViewController alloc]initWithRootViewController:viewController] ;
                                    self.window.rootViewController = navCon;
                                    [self.window makeKeyAndVisible];
                                */
                            }else{
                                if (code == 2022){
                                    //[self showErrorWithMessage:str(@"resend_verify_code")];
                                    self.showAlert(title: "",message:  Localizer.shared.localized("resend_verify_code"))
                                    
                                }else{
                                    if let _message = message{
                                        self.showAlert(title: "",message: _message)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
}




extension CALayer{
    func addBorder(edge:UIRectEdge, color:UIColor, thickness:CGFloat){
        
        let borders = CALayer()
        
        switch edge {
        case .top:
            borders.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness);
            break
        case .bottom:
            borders.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness);
        case .left:
            borders.frame = CGRect(x: 0, y: 0 + thickness, width: thickness, height: frame.height - thickness * 2);
        case .right:
            borders.frame = CGRect(x: frame.width - thickness, y: 0 + thickness, width: thickness, height: frame.height - thickness * 2);
        default:
            break
        }
        
        borders.backgroundColor = color.cgColor;
        
        self.addSublayer(borders);
    }
}

