//
//  EmailConfirmSixDigitViewController.swift
//  OONA
//
//  Created by Mike Wong on 6/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class EmailConfirmSixDigitViewController: UIViewController {

  
    @IBOutlet weak var sixDigitConfirm: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        sixDigitConfirm.defaultTextAttributes.updateValue(17.1,
                                                          forKey: NSAttributedString.Key.kern)
    }
        // Do any additional setup after loading the view.
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
