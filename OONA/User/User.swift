//
//  User.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct User {
    var inviteCode : String?
    var accessToken : String?
    var userId : Int?
    var expiresOn : Date?
    var setLoginByEmail : Bool = false
    var isLogin : Bool = false
    var profile : UserProfile?
    
    init(){
        
    }
    
    init(withDict obj:NSDictionary){
        if let _userId : Int = obj["userId"] as? Int{
            self.userId = _userId
        }
        if let _inviteCode : String = obj["inviteCode"] as? String{
            self.inviteCode = _inviteCode
        }
        if let _accessToken : String = obj["accessToken"] as? String{
            self.accessToken = _accessToken
        }
        
        if let _expiresOn : Date = obj["expiresOn"] as? Date{
            self.expiresOn = _expiresOn
        }
        if let _setLoginByEmail : Bool = obj["setLoginByEmail"] as? Bool{
            self.setLoginByEmail = _setLoginByEmail
        }
        self.isLogin = true
        
        UserDefaults.standard.set(userId, forKey: "user-userId")
        UserDefaults.standard.set(inviteCode, forKey: "user-inviteCode")
        UserDefaults.standard.set(accessToken, forKey: "user-accessToken")
        UserDefaults.standard.set(setLoginByEmail, forKey: "user-setLoginByEmail")
        UserDefaults.standard.set(expiresOn, forKey: "user-expiresOn")
        
        
    }
   
    
    init(userId: Int, accessToken: String, inviteCode: String, expiresOn: Date? , setLoginByEmail: Bool){
        self.userId = userId
        self.inviteCode = inviteCode
        self.accessToken = accessToken
        self.setLoginByEmail = setLoginByEmail
        self.expiresOn = expiresOn
        self.isLogin = true
    }
    
    func setUser(user : User){
        
    }
}
