//
//  UserHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Bugsnag
import SwiftyJSON
import RxLocalizer
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import RxSwift
import RxCocoa

class UserHelper: NSObject {
    static let shared = UserHelper()
    
    var userLogin: PublishRelay<User?> = PublishRelay<User?>()
    var userLogout: PublishRelay<Bool> = PublishRelay<Bool>()
    var userProfile : PublishRelay<UserProfile?> = PublishRelay<UserProfile?>()
    var currentUser : User = User()
    
    func login() {
        //if let _user = self.currentUser{
        self.currentUser.isLogin = true
        //if (currentUser.isLogin){
            self.userLogin.accept(self.currentUser)
        //}
    }
    
    func cleanUserData(){
        // TODO : send logout API
        
        UserDefaults.standard.set(nil, forKey: "user-inviteCode")
        UserDefaults.standard.set(nil, forKey: "user-accessToken")
        UserDefaults.standard.set(nil, forKey: "user-expiresOn")
        UserDefaults.standard.set(nil, forKey: "user-setLoginByEmail")
        UserDefaults.standard.set(nil, forKey: "user-userId")
        self.currentUser.isLogin = false
        self.userLogout.accept(true)
        
        self.getUserProfile(success: { (profile) in
            
        }) { (error, errorStr) in
            
        }
    }
    
    func getSavedUserInfo(success :@escaping (UserProfile) -> (), failure : @escaping ((Int?, String?) -> ())){
        //first load only.
        print("getSavedUserInfo")
        if(!self.isLogin()){
            
            var inviteCode : String?
            var accessToken : String?
            var expiresOn : Date?
            
//            self.cleanUserData() //for testing use...
            
            /**get data from user standard*/
            
            if let _inviteCode = UserDefaults.standard.string(forKey: "user-inviteCode"){
                inviteCode = _inviteCode
            }
            if let _accessToken = UserDefaults.standard.string(forKey: "user-accessToken"){
                accessToken = _accessToken
            }
            if let _expiresOn = UserDefaults.standard.object(forKey: "user-expiresOn"){
                expiresOn = _expiresOn as? Date
            }
            let setLoginByEmail: Bool = UserDefaults.standard.bool(forKey: "user-setLoginByEmail")
            
            let userId = UserDefaults.standard.integer(forKey: "user-userId")
            
            print("device stored info: ",userId,"\n",accessToken)
            
            if(userId != nil && accessToken != nil){
                var user = User(userId: userId, accessToken: accessToken ?? "", inviteCode: inviteCode ?? "",expiresOn:expiresOn,  setLoginByEmail: setLoginByEmail)
                
                self.currentUser = user
                self.userLogin.accept(user)
                self.getUserProfile(success: { userProfile in
                    self.login()
                        success(userProfile)
                    
                }, failure: { errorCode,msg   in  failure(errorCode, msg)})
            }
            
        }else{
            if let profile = self.currentUser.profile{
            }else{
                if(currentUser.isLogin){
                //}
                //if let user = self.currentUser{
                    //self.login(user: user)
                    self.getUserProfile(success: { userProfile in
                        
                        success(userProfile)
                        // print("success fetch userProfile" )
                        
                    }, failure: {code, msg  in
                        
                        failure(code, msg)
                        
                    })
                }
            }
        }
        
    }
    
    func currentUserProfile() -> UserProfile? {
        return self.currentUser.profile
    }
    
    func isLogin()->Bool{
        //return self.user
        //return (currentUser != nil)
        return currentUser.isLogin
    }
    func setDeviceID(deviceId: Int) {
        let deviceIdString = deviceId.description
        Bugsnag.configuration()?.setUser(deviceIdString, withName: "", andEmail: "")
        UserDefaults.standard.set(deviceId, forKey: "device_id")
        UserDefaults.standard.synchronize()
    }
    func loginWithEmail( email : String , password: String,  success : @escaping (User) -> (), failure : @escaping (ErrorAction, Int?, Any?) -> ()) {
        var param : [String : String] = ["email":email, "password":password]
        if let _encrypedParam = DeviceHelper.shared.getEncryptedData(param: param){
            param = _encrypedParam
        }
            
        OONAApi.shared.postRequest(url: APPURL.Login, parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON : NSDictionary = response.result.value as? NSDictionary {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                //response
                                self.setOldData(response: JSON)// for old app
                                
                                let user : User = User.init(withDict: JSON)
                                self.currentUser = user
                                self.getUserProfile(success: {  userProfile in
                                    self.login()
                                    success(user)
                                }, failure: {code,msg  in
                                    
                                    failure(ErrorAction.showError, code, msg)
                                })
                                //}, failure: {
                                //TODO_OONA : set old app model ..
                            }else{
                                if (code == 2037){
                                    //TODO_OONA : signup
                                    let userId : Int = JSON["userId"] as? Int ?? 0
                                    failure(.signupConfirmation, code, userId)
                                }
                                else{
                                    failure(.showError, code, message)
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    func Register(firstName: String, lastName: String, email: String, phone: String, gender: String, birthDate: String,password: String,  success : @escaping (Int) -> (), failure : @escaping (ErrorAction, Int?, Any?) -> ()){
        var encrypedPassword = password
        
        let param = ["firstName":firstName,"lastName":lastName,"email":email,"phone":phone,"gender":gender, "birthDate":birthDate,"password":password]
        guard let encrptedParam = DeviceHelper.shared.getEncryptedData(param: param) else { return  }
        
        
        OONAApi.shared.postRequest(url: APPURL.Register, parameter: encrptedParam).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON : NSDictionary = response.result.value as? NSDictionary {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                if let userId : Int = JSON["userId"] as? Int {
                                    success(userId)
                                }else{
                                    failure(.showError, code, message)
                                }
                            }else{
                                failure(.showError, code, message)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func updateUserProfile(photo : String , firstName: String, lastName: String, email: String, phone: String, gender: String, birthDate: String, success : @escaping () -> (), failure : @escaping (ErrorAction, Int?, Any?) -> ()){
        OONAApi.shared.postRequest(url: APPURL.UserProfile, parameter: ["photo":photo,"firstName":firstName,"lastName":lastName,"email":email,"phone":phone,"gender":gender, "birthDate":birthDate]).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON : NSDictionary = response.result.value as? NSDictionary {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                //response
                                //self.setOldData(response: JSON)// for old app
                                
                                //let user : User nnnn = User.init(withDict: JSON)
                                
                                success()
                                //}, failure: {
                                //TODO_OONA : set old app model ..
                            }else{
                                failure(.showError, code, message)
                            }
                        })
                    }
                }
            }
        }
    }
    
    func loginWithFacebookApi(fbParam : [String : Any?], success :@escaping (NSDictionary) -> (), failure : @escaping ((Int?, Any) -> ())){
        
        OONAApi.shared.postRequest(url: APPURL.facebookLogin, parameter: fbParam).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                success(JSON)
                            }else{
                                failure(code, message)
                            }
                        })
                    }
                }
            }else{
                failure(0, response.result.error)
            }
        }
        
    }

    /**Facebook**/
    func loginWithFacebook(success :@escaping (User) -> (), failure : @escaping ((Int?, Any) -> ())){
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //fbLoginManager.loginBehavior = FBSDKLoginBehavior.Browser
        
        //***step 1 : facebook login
        
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], handler: { (result, error) -> Void in
            if (error == nil){
                if let _fbloginresult : FBSDKLoginManagerLoginResult = result{
                    //_fbloginresult.logOut()
                    if(!_fbloginresult.isCancelled) {
                        
                        //***step 2 : fetch user
                        
                        if((FBSDKAccessToken.current()) != nil){
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, gender, email, locale, birthday,location,picture"]).start(completionHandler: { (connection, result, _error) -> Void in
                                if (_error == nil){
                                    
                                    if let _param : NSDictionary = result as! NSDictionary{
                                        
                                        let fbParam = self.getFacebookLoginParam(param: _param)
                                        //step 3 : post to OONA API
                                        
                                        self.loginWithFacebookApi(fbParam: fbParam ,success: { response in
                                            print("login fb graph",response)
                                            self.setOldData(response: response)// for old app
                                            
                                            let user: User = User.init(withDict: response)
                                            self.currentUser = user
                                            
                                            self.getUserProfile(success: {  userProfile in
                                                //if let _user = self.currentUser{
                                                //if(!self.currentUser.isLogin){
                                                    self.login()
                                                //}
                                               // }
                                                success(user)
                                            }, failure: {code,msg  in
                                                
                                                failure(code, msg ?? "")

                                                
                                            })
                                            //}, failure: {
                                                //failure(code , _error)
                                                
                                           // })
                                            
                                            
                                            
                                        }, failure: { code, error in
                                            failure(code , _error)
                                        })
                                    }
                                }else{
                                    failure(0, _error)
                                }
                            })
                        }
                        //} else if(fbloginresult.grantedPermissions.contains(["public_profile", "email"])) {
                        //fbLoginManager.logOut()
                        //}
                    }else{
                        //failure(9, "user canceled action")
                    }
                }
            }else{
                failure(0, error)
            }
        })
    }
    
    func setOldData(response: NSDictionary){
        
        //unknown flow from old app that doesn't know if it's useful or not but implement anyway
        
        if response.value(forKey: "inviteCode") != nil {
            let code = response.value(forKey: "inviteCode") as? String
            UserDefaults.standard.set(code, forKey: "inviteCode")
        }
        /*
        if newDeviceSignIn {
            newDeviceSignInDone = true
            //                    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"inviteCodeActivated"] ;
            UserDefaults.standard.set("YES", forKey: "Chatbot_Finished")
        }*/
        //
        if response.value(forKey: "points") != nil {
            if let value : String = response.value(forKey: "points") as? String  {
                UserData.setUserPoints(value)
            }
        }
        if response.value(forKey: "accessToken") != nil {
            if let value  : String = response.value(forKey: "accessToken") as? String  {
                UserData.setUserToken(value)
            }
        }
        if response.value(forKey: "userId") != nil {
            if let value : String   = response.value(forKey: "userId")  as? String {
                UserData.setUserID(value)
            }
            
            //FIRAnalytics.userID = UserData.userID()
            //TODO_OONA : FIRAnalytics
        }

    }
    
    func setOldUserProfileData(json: JSON){
        
        
        if let value: String = json["inviteCode"].string {
            UserData.setUserPoints(value)

        }
        
        if let value: String = json["points"].string {
            UserData.setUserPoints(value)
        }
        
        
        if let value: String = json["accessToken"].string {
            UserData.setUserToken(value)
        }
        
        if let value: String = json["userId"].string {
                UserData.setUserID(value)
        }
        
        
        if let value: String = json["firstName"].string {
                UserData.setUserFirstName(value)
        }
    
        if let value: String = json["lastName"].string {
                UserData.setUserLastName(value)
        }
        if let value: String = json["email"].string {
                UserData.setUserEmail(value)
        }
        if let value: String = json["gender"].string {
                UserData.setUserGender(value)
        }
        if let value: String = json["phone"].string {
                UserData.setUserPhone(value)
        }
        
        if let value: String = json["birthDate"].string {
            UserData.setUserDateOfBirth(value)
        }
        if let value: String = json["imageUrl"].string {
            UserData.setuserImageURL(value)
        }
        
        if let rank = json["rank"].dictionary {
            if let name : String   = rank["name"]?.string {
                UserData.setRankName(name)
            }
        }
        
        if let reward = json["reward"].dictionary {
            UserData.setUserRewards(reward)
        }
        
        if let favouriteCategories = json["favouriteCategories"].string {
            UserDefaults.standard.set(true, forKey: "chatbot_user_tvcat_finished")
        }
        
        /*
        if response.value(forKey: "inviteCode") != nil {
            let code = response.value(forKey: "inviteCode") as? String
            UserDefaults.standard.set(code, forKey: "inviteCode")
        }
        
        if response.value(forKey: "points") != nil {
            if let value : String = response.value(forKey: "points") as? String  {
                UserData.setUserPoints(value)
            }
        }
        if response.value(forKey: "accessToken") != nil {
            if let value  : String = response.value(forKey: "accessToken") as? String  {
                UserData.setUserToken(value)
            }
        }
        if response.value(forKey: "userId") != nil {
            if let value : String   = response.value(forKey: "userId")  as? String {
                UserData.setUserID(value)
            }
        }
        
        
        if response.value(forKey: "firstName") != nil {
            if let value : String   = response.value(forKey: "firstName")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "lastName") != nil {
            if let value : String   = response.value(forKey: "lastName")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "email") != nil {
            if let value : String   = response.value(forKey: "email")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "gender") != nil {
            if let value : String   = response.value(forKey: "gender")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "phone") != nil {
            if let value : String   = response.value(forKey: "phone")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "birthDate") != nil {
            if let value : String   = response.value(forKey: "birthDate")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "imageUrl") != nil {
            if let value : String   = response.value(forKey: "imageUrl")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "points") != nil {
            if let value : String   = response.value(forKey: "points")  as? String {
                UserData.setUserID(value)
            }
        }
        if response.value(forKey: "rank") != nil {
            if let rank : NSDictionary   = response.value(forKey: "rank")  as? NSDictionary {
                if rank.value(forKey: "name") != nil {
                    if let name : String   = rank.value(forKey: "name")  as? String {
                        UserData.setUserID(name)
                    }
                }
            }
        }
        
        if response.value(forKey: "inviteCode") != nil {
            if let inviteCode : String   = response.value(forKey: "inviteCode")  as? String {
                UserData.setUserID(inviteCode)
            }
        }
        
        if response.value(forKey: "reward") != nil {
            if let value : String   = response.value(forKey: "reward")  as? String {
                UserData.setUserID(value)
            }
        }
        
        
        if response.value(forKey: "favouriteCategories") != nil {
            if let value : String   = response.value(forKey: "chatbot_user_tvcat_finished")  as? String {
                UserData.setUserID(value)
            }
        }*/
        
        
    }
    
    func setOldData(){
        /*
        if ([responseObject valueForKey:@"inviteCode"]!= nil){
            NSString *code = [responseObject valueForKey:@"inviteCode"];
            [[NSUserDefaults standardUserDefaults] setObject:code forKey:@"inviteCode"] ;
        }
        if (newDeviceSignIn){
            newDeviceSignInDone = YES ;
            //                    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"inviteCodeActivated"] ;
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"Chatbot_Finished"];
        }
        //
        if ([responseObject valueForKey:@"points"] != nil)
        [UserData setUserPoints:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"points"]]];
        if ([responseObject valueForKey:@"accessToken"] != nil)
        [UserData setUserToken: [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"accessToken"]]];
        if ([responseObject valueForKey:@"userId"] != nil ){
            [UserData setUserID:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"userId"]]];
            
            [FIRAnalytics setUserID:[UserData userID]];
        }
         //TODO_OONA:  [FIRAnalytics setUserID:[UserData userID]];
         ;
         //TODO_OONA:  [self getUserInfo];
         
         TODO_OONA : set old data
        */
        
    }
    
    func getFacebookLoginParam(param: NSDictionary)->[String : Any]{
        
        
        let email = param.value(forKey: "email")
        let first_name = param.value(forKey: "first_name")
        let last_name = param.value(forKey: "last_name")
        var gender = ""
        if let _gender : String = param.value(forKey: "gender") as? String{
            if(_gender == "male"){
                gender = "M"
            }else if(_gender == "female"){
                gender = "F"
            }
        }
    
        let id : String = param.value(forKey: "id") as! String
        let pictureURL = "https://graph.facebook.com/" + id + "/picture?width=640&height=640"
        return ["email":email,
            "firstName":first_name,
            "lastName":last_name,
            "gender":gender,
            "id":id,
            "photoUrl":pictureURL,
            "userOtherInfo":param
        ]
        
    }
    
    /**Google**/
    func loginWithGoogle(user: GIDGoogleUser, success :@escaping (User) -> (), failure : @escaping ((Int?, Any) -> ())){
        
        let param = self.getGoogleLoginParam(user: user)
        
        self.loginWithGoogleApi(param: param ,success: { response in
            let user: User = User.init(withDict: response)
            self.currentUser = user
            self.setOldData(response: response)// for old app
            self.getUserProfile(success: {  userProfile in
                self.login()
                
                success(user)
            }, failure: {_,_  in })
            
            
            
        }, failure: { code, error in
            failure(code , error)
        })
    }
    
    func getGoogleLoginParam(user: GIDGoogleUser)->[String:Any?]{
        
        let userId = user.userID;
        let givenName = user.profile.givenName;
        let familyName = user.profile.familyName;
        let email = user.profile.email;
        var pictureURL = "";
        if (user.profile.hasImage) {
            pictureURL = user.profile.imageURL(withDimension: 640).absoluteString;
        }
        
        let params = ["id":userId,
                      "firstName":givenName,
                      "lastName":familyName,
                      "photoUrl":pictureURL,
                      "email":email
        ];
        
        return params;
    }
    
    func loginWithGoogleApi(param : [String : Any], success :@escaping (NSDictionary) -> (), failure : @escaping ((Int?, Any) -> ())){
        
        OONAApi.shared.postRequest(url: APPURL.googleLogin, parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                success(JSON)
                            }else{
                                failure(code, message)
                            }
                        })
                    }
                }
            }else{
                failure(0, response.result.error)
            }
        }
        
    }
    /**Instagram**/
    
    func getInstagramUser(authToken : String , code: String, success :@escaping (User) -> (), failure : @escaping ((Int?, Any) -> ())){
        /*let param = ["client_id":InstagramIds.InstgramClientId,
                    "client_secret":InstagramIds.InstgramClientSecret,
                    "grant_type":"authorization_code",
                    "redirect_uri":InstagramIds.InstgramRedirectUrl
        ]
        OONAApi.shared.postRequest(url: APPURL.Instagram(), parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                   success(JSON)
                }
            }else{
                failure(0, response.result.error)
            }
        }
        let redirectURI = InstagramIds.InstgramRedirectUrl
        let clientID = InstagramIds.InstgramClientId
        let clientSecret = InstagramIds.InstgramClientSecret
        let code = code*/
        
       // let urlString = "https://api.instagram.com/oauth/access_token"
        
        
        OONAApi.shared.getRequest(url: APPURL.getInstagramApi(access_token: authToken), parameter: [:]).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                    //let code : Int? = JSON["code"] as? Int
                    //let status : String? = JSON["status"] as? String
                    //if(code != nil && status != nil){
                        //APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                         //   message , _success in
                         //   if(_success){
                                let param = self.getInstagramParam(userDict: JSON)
                                self.loginWithInstagramApi(param: param, success: { result in
                                    
                                    self.setOldData(response: result)// for old app
                                    
                                    let user = User.init(withDict: result as NSDictionary)
                                    self.currentUser = user

                                    self.getUserProfile(success: {  userProfile in
                                        self.login()
                                        success(user)
                                    }, failure: {code,msg  in
                                        failure(code, msg)
                                    })
                                }, failure: { code, message in
                                    failure(code, message)
                                })
                          //  }else{
                             //   failure(code, message)
                          //  }
                        //})
                    //}
                }
            }else{
                failure(0, response.result.error)
            }
        }
        
        
    }
    
    func getInstagramParam(userDict: NSDictionary)-> [String : Any?]{
        //userDict.value(forKey: "user")
        var firstName = ""
        var lastName = ""
        var id = ""
        var profile_picture = ""
        
        if let userObj : NSDictionary = userDict.value(forKey: "data") as! NSDictionary{
            var firstName = ""
            var lastName = ""
            if let full_name:String = userObj.value(forKey: "full_name") as? String{
                
                let fullnameArr = full_name.components(separatedBy: " ")
                
                if let ___firstName : String = fullnameArr[0]  as String{
                    firstName = ___firstName
                }
                if let ___lastName : String = fullnameArr[0]  as String{
                    lastName = ___lastName
                }
            }
            
            id = userObj.value(forKey: "id") as? String ?? ""
            profile_picture = userObj.value(forKey: "profile_picture") as? String ?? ""
            
        }
    
        let result : [String : Any?] =
            ["id":id,
             "firstName":firstName,
             "lastName":lastName,
             "photoUrl":profile_picture]
        return result
        
    }
    
    
    func loginWithInstagramApi(param : [String : Any], success :@escaping (NSDictionary) -> (), failure : @escaping ((Int?, Any) -> ())){
        
        OONAApi.shared.postRequest(url: APPURL.instagramLogin, parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                success(JSON)
                            }else{
                                failure(code, message)
                            }
                        })
                    }
                }
            }else{
                failure(0, response.result.error)
            }
        }
        
    }
    
    func getUserProfile(success :@escaping (UserProfile) -> (), failure : @escaping ((Int?, String?) -> ())){
//        print("getUserProfile")
//        if let user = self.currentUser{
        OONAApi.shared.postRequest(url: APPURL.UserProfile, parameter: [:]).responseJSON{ (response) in
//                print("getUserProfile",response.value)
                if let json = (try? JSON(data: response.data!)) as? JSON {
                    let code  = json["code"].int
                    let status = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                //let _video = Video.init(json: json, chType: channel.type, chId: channel.id, epId: 0, _imageUrl: episode.imageUrl)
                                //completion(true,_video, nil)
                                self.setOldUserProfileData(json: json)
                                if let userProfile : UserProfile = UserProfile.init(json: json){
                                    self.userProfile.accept(userProfile)
                                    self.currentUser.profile = userProfile
                                    success(userProfile)
                                }
                            }else{
                                failure(code, message)
                            }
                        })
                    }else{
                        //failure..
                        failure(code, nil)
                    }
                }else{
                    
                    //failure..
                    failure(0, "fail to parse json")
                }
            }
//        }else{
//            failure(0, "fail to get current user")
//            print("fail to get current user")
//        }
    }
    
}

