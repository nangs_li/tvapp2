//
//  DesignableView.swift
//  OONA
//
//  Created by Mike Wong on 25/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation


@IBDesignable
class GradientButton: UIButton {
    let gradientLayer = CAGradientLayer()
    
    var buttonFrameObservation: NSKeyValueObservation?
    
    /*
     There is an issue with retrieving runtime attributes. Hardcode value 10 for now.
     */
    var radius : CGFloat = 10
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return radius
        }
        set {
            radius = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor =  UIColor.lightGray {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
        
    @IBInspectable
    var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var startGradientColor: UIColor? {
        didSet {
            setGradient(startGradientColor: startGradientColor, midGradientColor: midGradientColor, endGradientColor: endGradientColor)
        }
    }
    
    
    @IBInspectable
    var midGradientColor: UIColor? {
        didSet {
            setGradient(startGradientColor: startGradientColor, midGradientColor: midGradientColor, endGradientColor: endGradientColor)
        }
    }
    
    @IBInspectable
    var endGradientColor: UIColor? {
        didSet {
            setGradient(startGradientColor: startGradientColor, midGradientColor: midGradientColor, endGradientColor: endGradientColor)
        }
    }
    
    func setGradient(startGradientColor: UIColor?, midGradientColor: UIColor?, endGradientColor: UIColor?) {
        if let startGradientColor = startGradientColor, let midGradientColor = midGradientColor, let endGradientColor = endGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [startGradientColor.cgColor, midGradientColor.cgColor, endGradientColor.cgColor]
            gradientLayer.locations = [0.0, 0.01, 1.0]
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = radius
            layer.insertSublayer(gradientLayer, at: 0)
            if buttonFrameObservation == nil {
                buttonFrameObservation = observe(\.self.bounds, options: [.old, .new], changeHandler: { (button, bounds) in
                    button.gradientLayer.frame = button.bounds
                })
            }
        } else {
            gradientLayer.removeFromSuperlayer()
            buttonFrameObservation?.invalidate()
            buttonFrameObservation = nil
        }
    }
    
    deinit {
        print("Deallocating \(type(of: self))")
        buttonFrameObservation?.invalidate()
        buttonFrameObservation = nil
    }
}
class VerticalGradientButton : GradientButton {
    @IBInspectable
    override var cornerRadius: CGFloat {
        get {
            return 0
        }
        set {
            radius = newValue
        }
    }
    
    
    override internal func setGradient(startGradientColor: UIColor?, midGradientColor: UIColor?, endGradientColor: UIColor?) {
        if let startGradientColor = startGradientColor, let midGradientColor = midGradientColor, let endGradientColor = endGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [startGradientColor.cgColor, midGradientColor.cgColor, endGradientColor.cgColor]
            gradientLayer.locations = [0.0, 0.01, 1.0]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = radius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
}
