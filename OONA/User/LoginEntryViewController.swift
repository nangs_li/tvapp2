//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn


class LoginEntryViewController: BaseViewController, GIDSignInDelegate, GIDSignInUIDelegate{
    typealias LoginPageCompletionBlock = (() ->())
    var completionBlock: LoginPageCompletionBlock?
    
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var loginTitleLabel: UILabel!
    @IBOutlet weak var loginSubTitleLabel: UILabel!
    
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signUpDescLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set delegates
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        /*
        let signUpConfirmationViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "SignUpConfirmationViewController") as! SignUpConfirmationViewController
        signUpConfirmationViewController.view.backgroundColor = .clear
        self.navigationController?.pushViewController(signUpConfirmationViewController, animated: true)*/
        
    }
    
    

    @IBAction func loginFacebook(_ sender: UIButton) {
        
        UserHelper.shared.loginWithFacebook(success: {user in
            //login success
            //self.dismiss(animated: true)
//            self.completionBlock?()
            self.loginSuccess()
            
        }, failure: {code,error  in
            self.handleError(code: code, error: error)
        })
    }
    
    func handleError(code: Int?, error: Any){
        
        //TODO_OONA : error handling to a class
        if let _error : NSError = error as? NSError{
            self.showAlert(title: "",message: _error.localizedDescription)
        }else if let _error : String = error as? String{
            
            //the user canceled the sign-in flow
            if(_error != "the user canceled the sign-in flow"){
                self.showAlert(title: "",message: _error)
            }
            
            if(code==1007){
            //TODO_OONA: back to inv code  [self backToInvCode];
            }
        }else{
            self.showAlert(title: "",message: "unkown")
        }
    }
    
    /**google**/
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {

        if let _error = error{
            self.handleError(code: nil,  error: _error.localizedDescription)
            
        }else{
            UserHelper.shared.loginWithGoogle(user: user, success: { user in
                
                self.dismiss(animated: true)
                //TODO_OONA : rx / notification for the app to login
                
            }, failure: { code,error  in
                self.handleError(code: code, error: error)
            })
        }
    }
    
    
    /*
     func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
     guard error == nil else {
     
     print("Error while trying to redirect : \(error)")
     return
     }
     print("Successful Redirection")
     }
     
     
     //MARK: GIDSignIn Delegate
     // Finished disconnecting |user| from the app successfully if |error| is |nil|.
     public func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!)
     {
     
     }*/
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        //self.dismiss(animated: true, completion: nil)
//        self.completionBlock?()
        loginSuccess()
    }
    
    @IBAction func loginWithInstagram(_ sender: UIButton) {
        
        let instagramViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
        instagramViewController.view.backgroundColor = .clear
        instagramViewController.completionBlock  = { user in
            //login success and return a user
            //userDict = userDict
            //self.dismiss(animated: true)
//            self.completionBlock?()
            self.loginSuccess()
        }
        self.present(instagramViewController, animated: true, completion:nil)
    }
    
    @IBAction func loginWithGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func signin(_ sender: UIButton) {
        //self.performSegue(withIdentifier: "pushLogin", sender: self)
        
        let loginViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginViewController.view.backgroundColor = .clear
        
        loginViewController.userActionType = .register
//        loginViewController.setupUIs()
        //loginViewController.modalPresentationStyle = .overCurrentContext
        //loginViewController.modalTransitionStyle = .crossDissolve
        //self.present(loginViewController , animated: true, completion: nil)
        self.navigationController?.pushViewController(loginViewController, animated: true)
//        loginViewController.setupUIs()
    }
    @IBAction func register(_ sender: UIButton) {
        
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.userActionType = .login
//        vc.setupUIs()
        vc.view.backgroundColor = .clear
        //loginViewController.modalPresentationStyle = .overCurrentContext
        //loginViewController.modalTransitionStyle = .crossDissolve
        //self.present(loginViewController , animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func transferCodeAction(_ sender: Any) {
        self.dismiss(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            
            let yourInfovc : ReferralEnterViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateViewController(withIdentifier: "ReferralEnterViewController") as! ReferralEnterViewController
            //yourInfovc.view.backgroundColor = .clear
            yourInfovc.transferCode()
            yourInfovc.modalPresentationStyle = .overCurrentContext
            yourInfovc.modalTransitionStyle = .crossDissolve
            PlayerHelper.shared.presentVC(vc: yourInfovc)
        }
        
    }
    @IBAction func backToMenu(_ sender: UIButton) {
        //self.dismiss(animated: true)
//        self.completionBlock?()
        loginSuccess()
    }
    @IBOutlet weak var transferCodeButton: UIButton!{
//        if DeviceHelper.shared.allowInputTransferCode {
        didSet {
            transferCodeButton.isHidden = !DeviceHelper.shared.allowInputTransferCode
        }
//        }
    }
    
    @IBOutlet weak var transferCodeLine: UIView!{
        didSet {
            transferCodeLine.isHidden = !DeviceHelper.shared.allowInputTransferCode
        }
        //        }
    }
    @IBAction func Register(_ sender: UIButton) {
        
    }
    
    override func appLanguageDidUpdate() {
        self.loginTitleLabel.text = LanguageHelper.localizedString("login_text")
        self.loginSubTitleLabel.text = LanguageHelper.localizedString("to_enjoy_free_oona_tv_text")
        self.transferCodeButton.setTitle(LanguageHelper.localizedString("have_transfer_code_text").escapeHTMLEntities(), for: .normal)
        
        self.backButton.setTitle(LanguageHelper.localizedString("back_to_menu_text").escapeHTMLEntities(), for: .normal)
        
        self.signUpDescLabel.text = LanguageHelper.localizedString("dont_have_an_account_text")
        self.signUpButton.setTitle(LanguageHelper.localizedString("sign_up_dot_text").escapeHTMLEntities(), for: .normal)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag) {
            self.completionBlock?()
        }
    }
}
