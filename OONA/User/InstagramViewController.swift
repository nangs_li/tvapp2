//
//  MenuViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
/*

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources
import AVKit
import BrightcovePlayerSDK
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn*/


class InstagramViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var loginWebView: UIWebView!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    
    typealias IgCompletionBlock = (_ result :User) ->()
    var completionBlock:IgCompletionBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loginWebView.scrollView.isScrollEnabled = true;
        loginWebView.scalesPageToFit = true;
        loginWebView.delegate = self
        
        unSignedRequest()
        
        //guard let cb = self.completionBlock else {return}
        //cb(nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - unSignedRequest
    func unSignedRequest () {
        
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [InstagramIds.InstgramAuthUrl,InstagramIds.InstgramClientId,InstagramIds.InstgramRedirectUrl, InstagramIds.InstgramScope])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.loadRequest(urlRequest)
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(InstagramIds.InstgramRedirectUrl) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            
            //let codeRange = requestURLString.range(of: "code=")
            var code = ""
            //let code = requestURLString.substring(from: codeRange.location + codeRange.length)
            if let queryCode = getQueryStringParameter(url: requestURLString, param: "code"){
                code = queryCode
            }
            
            handleAuth(authToken: requestURLString.substring(from: range.upperBound), code:code)
            
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String, code : String)  {
        UserHelper.shared.getInstagramUser(authToken : authToken, code: code, success: {
            user in
            //go back to login entry vc
            self.dismiss(animated: true)
            guard let cb = self.completionBlock else {return}
            cb(user)
        }, failure: {
            code, msg in
            if let _msg : String = msg as? String{
                self.showAlert(title: "",message: _msg)
            }
        } )
    }
    
    func makePostRequest(code:String){
        
        
        /*
        NSString *post = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",INSTAGRAM_CLIENT_ID,INSTAGRAM_CLIENTSERCRET,INSTAGRAM_REDIRECT_URI,code];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *requestData = [NSMutableURLRequest requestWithURL:
            [NSURL URLWithString:@"https://api.instagram.com/oauth/access_token"]];
        [requestData setHTTPMethod:@"POST"];
        [requestData setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [requestData setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [requestData setHTTPBody:postData];
        
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:requestData returningResponse:&response error:&requestError];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        [self handleResponse:dict];*/
    }
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
     @IBAction func backButtonAction(_sender: UIButton) {
        self.dismiss(animated: true)
    }

}

