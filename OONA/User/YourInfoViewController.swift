//
//  YourInfoViewController.swift
//  OONA
//
//  Created by Mike Wong on 26/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class YourInfoViewController: BaseViewController , UIPickerViewDelegate, UIPickerViewDataSource {
   
    var userProfile : UserProfile?
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var pickerData: [String] = [String]()
    
    let datePicker = UIDatePicker()
    var email : String = ""
    var password : String = ""
    var userActionType : UserActionType!
    
    @IBOutlet weak var txtDatePicker: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var genderBtn: UIButton!
    @IBOutlet weak var genderTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerData = ["Male", "Female", "Other"]
        genderBtn.titleLabel?.lineBreakMode = .byClipping
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Italic", size: 12)!
        ]
        
        //txtDatePicker.attributedPlaceholder = NSAttributedString(string: "Birthday", attributes:attributes)
        self.updateUserViews()
        let bottomLine = CALayer()
        
        txtDatePicker.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtDatePicker.layer.addSublayer(bottomLine)
        txtDatePicker.layer.masksToBounds = true
        
        txtFirstName.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtFirstName.layer.addSublayer(bottomLine)
        txtFirstName.layer.masksToBounds = true
        
        txtLastName.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        txtLastName.layer.addSublayer(bottomLine)
        txtLastName.layer.masksToBounds = true
        
        genderTxt.layer.addBorder(edge: .bottom, color: UIColor.red, thickness: 1.0)
        genderTxt.layer.addSublayer(bottomLine)
        genderTxt.layer.masksToBounds = true
        
        setupDatePicker()
        loadProfileData()
    }
    
    
    func loadProfileData(){
        if let _profile =  UserHelper.shared.currentUser.profile{
            
            /*@IBOutlet weak var txtFirstName: UITextField!
            @IBOutlet weak var txtLastName: UITextField!
            @IBOutlet weak var txtGender: UITextField!
            @IBOutlet weak var txtBirthday: UITextField!*/
            txtFirstName.text = _profile.firstName
            txtLastName.text = _profile.lastName
            if let gender = _profile.gender{
                if(gender=="M"){
                    genderTxt.text = "Male"
                }else if(gender=="F"){
                    genderTxt.text = "Female"
                }else{
                    genderTxt.text = "Other"
                }
            }
            if let _birthdate = _profile.birthDate{
                if(_birthdate != ""){
                    txtDatePicker.text = _birthdate
                }
            }
        }
    }
    
    
    //FOR  GENDER
    
    @IBAction func dismiss(){
        self.dismiss(animated: true)
    }

    
    
    
    @IBAction func genderTap(_ sender: Any) {
        picker = UIPickerView.init()
        
        self.picker.delegate = self
        self.picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height * 0.4, width: UIScreen.main.bounds.size.width, height: 250)
     
        self.view.addSubview(picker)

        //ToolBar
        toolBar.sizeToFit()
        //   datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height * 0.4, width: UIScreen.main.bounds.size.width, height: 50))
        //toolBar.barStyle = .blackTranslucent
        toolBar.items = [UIBarButtonItem.init(title: "Done",  style: UIBarButtonItem.Style.plain, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
        
        //auto set female
        picker.selectRow(1, inComponent:0, animated:true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onDoneButtonTapped() {
        let selectedValue = pickerData[picker.selectedRow(inComponent: 0)]
        genderBtn.titleLabel?.text = selectedValue
        genderTxt.text = selectedValue
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        self.view.endEditing(true)

        
    }
  
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    //END OF PART FOR GENDER
    
    //Part for birthday picker
    func setupDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //   datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)

        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtDatePicker.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func next(_ sender: Any) {
        
        var firstName : String = ""
        var lastName : String = ""
        var gender : String = ""
        var date : String = ""
        var phone : String = ""
        //register
        if let _firstName = txtFirstName.text{
            firstName = _firstName
        }
        if let _lastName = txtLastName.text{
            lastName = _lastName
        }
        if let _gender = genderTxt.text{
            if(_gender=="Male"){
                
                gender = "M"
                
            }else if(_gender=="Female"){
                
                gender = "F"
                
            }else{
                
                gender = _gender
            }
            
        }
        
        if let _date = txtDatePicker.text{
            date = _date
        }
        
        if(self.userActionType == .register){
            UserHelper.shared.Register(firstName: firstName, lastName: lastName, email: email, phone: phone, gender: gender, birthDate: date, password: password, success: { userId in
                //if let _email = email{
                self.showConfirmation(userId : userId, email: self.email)
                //}
                
            }, failure: { error, code, message in
                if let _msg : String = message as? String{
                    self.showAlert(title: "", message : _msg)
                }
            })
            
        }else{
            //update user info..
            
            UserHelper.shared.updateUserProfile(photo : "" , firstName: firstName, lastName: lastName, email: email, phone: phone, gender: gender, birthDate: date, success : {
                self.showAlert(title: "", message : "Update successfully ")
                
            } , failure :{error , code, message in
                self.showAlert(title: "", message : message as! String)
            })
            
            /*
            UserHelper.shared.register(firstName: firstName, lastName: lastName, gender: gender, birthDate: date, phone: phone, email: email, password: password, success: { user in
                if let userId = user.userId{
                    self.showConfirmation(userId : userId)
                    
                }
            }, failure: { error, code, message in
                if let _msg : String = message as? String{
                    self.showAlert(title: "", message : _msg)
                }
            })*/
            
        }
    }
    
    
    func showConfirmation(userId: Int, email: String){
        
        let signUpConfirmationViewController : ConfirmationViewController  =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
        signUpConfirmationViewController.userId = userId;
        signUpConfirmationViewController.email = email;
        self.navigationController?.pushViewController(signUpConfirmationViewController, animated: true)
        
    }
    
}
