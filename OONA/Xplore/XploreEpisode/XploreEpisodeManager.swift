//
//  XploreEpisodeManager.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift
import RxCocoa

class XploreEpisode: Pagination<Episode> {
    /// For identify which category of episode list belongs to.
    var categoryID: Int
    
    init(categoryID: Int) {
//        super.init
        self.categoryID = categoryID
    }
    
}

class XploreEpisodeManager: NSObject {
    var xploreEpisode : PublishRelay<XploreEpisode> = PublishRelay()
    
    private var _xploreEpisode: XploreEpisode
    let disposeBag = DisposeBag()
    var loading : Bool = false
    override init() {
        self._xploreEpisode = XploreEpisode(categoryID: NSNotFound)
        super.init()
    }
    
    func isSameCategory(_ categoryID: Int) -> Bool {
        return categoryID == _xploreEpisode.categoryID
    }
    
    func requestEpisodeList(categoryID: Int) {
        if self._xploreEpisode.categoryID != NSNotFound {
            if !isSameCategory(categoryID) {
                print("Performing new episodes request")
                self._xploreEpisode.categoryID = categoryID
                self._xploreEpisode.itemList.removeAll()
            } else {
                print("Performing next episodes request")
            }
        }
        else {
            print("Performing new episodes request")
            self._xploreEpisode.categoryID = categoryID
        }
        /// Publish rx signal
        self.xploreEpisode.accept(self._xploreEpisode)
        if (!loading) {
            loading = true
            
            self.getEpisodeList(currentXploreEpisode: self._xploreEpisode,
                                completion: { [weak self] (success, newEpisodeList) in
                                    self?.loading = false
                                    if success {
                                        self?.updateEpisodeList(episodeList: newEpisodeList!, categoryID: categoryID)
                                    }
            })
        }
    }
    
    private func updateEpisodeList(episodeList: [Episode],
                                   categoryID: Int) {
        
        /// Check xploreEpisode is exist
        if self._xploreEpisode.categoryID != NSNotFound {
            print("Updating EpisodeList for category: \(categoryID)")
            if categoryID == self._xploreEpisode.categoryID {
                print("Appending EpisodeList for category: \(categoryID)")
                self._xploreEpisode.itemList.append(contentsOf: episodeList)
            } else {
                print("Replacing EpisodeList for category from: \(_xploreEpisode.categoryID) to: \(categoryID)")
                self._xploreEpisode.categoryID = categoryID
                self._xploreEpisode.itemList = episodeList
            }
            
//            self._xploreEpisode.hasNextPage = (episodeList.count == self._xploreEpisode.nextRequestRange.length)
            self._xploreEpisode.hasNextPage = (episodeList.count != 0)
            
            /// Publish rx signal
            self.xploreEpisode.accept(self._xploreEpisode)
        }
    }
    
    func getEpisodeList(currentXploreEpisode: XploreEpisode,
                        completion: ((_ success: Bool, _ episodeList: [Episode]?) -> Void)?) {
        
        XploreHelper.shared.getEpisodeList(xploreEpisode: currentXploreEpisode,
                                           success: { (episodeCategoryID, newEpisodeList) in
                                            completion?(true, newEpisodeList)
        },
                                           failure: { (error) in
                                            completion?(false, nil)
        })
    }
    
    deinit {
        print("Deallocating \(self)")
    }
}
