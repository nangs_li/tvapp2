//
//  XploreEpisodeViewController.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MJRefresh

class XploreEpisodeViewController: BaseViewController, UIScrollViewDelegate {
    let previewHeight: CGFloat = 170
    @IBOutlet weak var collectionView: UICollectionView! { didSet { collectionView.backgroundColor = nil } }
    var previewController: PreviewViewController = { return PreviewViewController(nibName: "PreviewViewController", bundle: nil) }()
    
    @IBOutlet weak var previewView: UIView!
    let xploreEpisodeDatasource: XploreEpisodeDatasource = XploreEpisodeDatasource()
    let xploreEpisodeManager: XploreEpisodeManager = XploreEpisodeManager()
    
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.addRXObservers()
        self.loadPreviewView()
        self.xploreEpisodeDatasource.episodeManager = self.xploreEpisodeManager
        self.setupCollectionViewPullUpLoadMoreFooter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.previewController.stopPreview()
    }
    
    deinit {
//        print("Deallocating \(self)")
    }
}

// MARK: - Config Views
extension XploreEpisodeViewController {
    private func setupCollectionViewPullUpLoadMoreFooter() {
        let loadMoreFooter = MJRefreshAutoNormalFooter()
        loadMoreFooter.refreshingBlock = { [weak self] in
            self?.loadMoreData()
        }
        self.collectionView.mj_footer = loadMoreFooter
    }
}

// MARK: - Functional methods
extension XploreEpisodeViewController {
    
    private func addRXObservers() {
        // Bind dataSource to collectionView
        xploreEpisodeDatasource.bindDataSource(to: collectionView)
        
        // Subscribe data change from Episode manager on data has been updated
        xploreEpisodeManager.xploreEpisode.subscribe(onNext: { [weak self] (xploreEpisode) in
            self?.endLoadMoreData(with: xploreEpisode)
            self?.xploreEpisodeDatasource.episodeList.accept(xploreEpisode.itemList)
        }).disposed(by: self.disposeBag)
        
        xploreEpisodeDatasource.previewEpisode.subscribe(onNext: { [weak self] (episode) in
            
            self?.updatePreviewShowHide(with: episode)
        }).disposed(by: self.disposeBag)
        
        //
        XploreHelper.shared.selectedCategoryID.filter({ $0 != NSNotFound })
            .subscribe(onNext: { [weak self] (categoryID) in
                
                var sameCategory: Bool = false
                if let checkedSelf = self {
                    sameCategory = checkedSelf.xploreEpisodeManager.isSameCategory(categoryID)
                }
                if !sameCategory {
                    self?.updatePreviewShowHide(with: nil)
                    self?.xploreEpisodeManager.requestEpisodeList(categoryID: categoryID)
                } else {
                    print("Ignoring same category being selected")
                }
                
        }).disposed(by: self.disposeBag)
        
        previewController.selectedItem.subscribe(onNext: { [weak self] (selectedItem) in
//            self?.shouldPlayVideo(video: selectedItem?.video)
            self?.prepareToPlay(previewItem: selectedItem)
        }).disposed(by: self.disposeBag)
    }
    
    func loadMoreData() {
        self.xploreEpisodeManager.requestEpisodeList(categoryID: XploreHelper.shared.selectedCategoryID.value)
    }
    
    func endLoadMoreData(with xploreEpisode: XploreEpisode) {
        if xploreEpisode.hasNextPage {
            self.collectionView.mj_footer?.endRefreshing()
        } else {
            self.collectionView.mj_footer?.endRefreshingWithNoMoreData()
        }
    }
    
    func prepareToPlay(previewItem: PreviewModel?) {
        if let item = previewItem,
            let video = item.video {
            XploreHelper.shared.updateStreamingInfo(with: video,
                                                    videoOwner: item.videoOwner) { (success, video, error) in
//                                                        if success {
                                                            self.shouldPlayVideo(video: video)
//                                                        }
            }
        }
    }
    
    func shouldPlayVideo(video: Video?) {
        XploreHelper.shared.selectedVideo.accept(video)
    }
    
    func reloadContent() {
        (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).invalidateLayout()
        self.collectionView.reloadData()
    }
}

extension XploreEpisodeViewController {
    
    func updatePreviewShowHide(with episode: Episode?) {
        
        let previewItem = PreviewModel(title: episode?.name,
                                       subtitle: nil,
                                       description: episode?.description,
                                       owner: .episode,
                                       video: episode?.video)
        
        UIView.transition(with: self.view,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.collectionViewTopConstraint.constant = (episode != nil) ? self.previewHeight : 0
                            if episode != nil {
                                self.view.layoutIfNeeded()
                                self.previewController.previewItem.accept(previewItem)
                            } else {
                                self.previewController.previewItem.accept(previewItem)
                                self.view.layoutIfNeeded()
                            }
        })
    }
    
    func updateCellVisibilityInCollectionView(with item: Int) {
        self.collectionView.scrollToItem(at: IndexPath(item: item,
                                                       section: 0),
                                         at: .top, animated: true)
    }
    
    private func loadPreviewView() {
        // Add Child View Controller
        self.addChild(self.previewController)
        self.previewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let previewVCView: UIView = self.previewController.view
        self.previewView.addSubview(previewVCView)
        // Configure Child View
        previewVCView.frame = self.previewView.bounds
        previewVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.previewController.didMove(toParent: self)
    }
}
