//
//  XploreEpisodeCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class XploreEpisodeCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var overlayImageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var underline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func click() {
        // print("clicked")
    }
    
    func configure(with episode: Episode) {
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: episode.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        descriptionLabel.text = episode.name
        
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
    /*
    func setInActive(){
        self.underline.isHidden = true
    }
    
    func setActive(){
        self.underline.isHidden = false
    }*/
    
}
