//
//  XploreEpisodeDatasource.swift
//  OONA
//
//  Created by nicholas on 7/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import SwiftyJSON

class XploreEpisodeDatasource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    static let cellIdentifier: String = "XploreEpisodeCell"
    var unlockedVideoIdList: [Int] = ( UserDefaults.standard.array(forKey: "unlockedVideoIdList") ?? [] )as! [Int]
    
    var episodeList: BehaviorRelay<[Episode]>
    var previewEpisode: BehaviorRelay<Episode?>
    var previousSelectedCell: XploreEpisodeCell?
    let disposeBag = DisposeBag()
    var cView : UICollectionView?
    weak var episodeManager: XploreEpisodeManager?
    
    override init() {
        self.episodeList = BehaviorRelay<[Episode]>(value: [Episode]())
        self.previewEpisode = BehaviorRelay<Episode?>(value: nil)
        super.init()
    }
    
    deinit {
        print("Deallocating \(self)")
    }
    
    func bindDataSource(to collectionView: UICollectionView) {
        // Register Cell Nib for collectionView
        collectionView.register(UINib(nibName: "XploreEpisodeCell", bundle: nil),
                                forCellWithReuseIdentifier: XploreEpisodeDatasource.cellIdentifier)
        // Set collectionView content inset
        collectionView.contentInset = UIEdgeInsets(top: lineSpacing,
                                                   left: interitemSpacing,
                                                   bottom: 0,
                                                   right: interitemSpacing)
        // Assign collectionView delegate and datasource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Subscribe rx update from episodeList to perform reloadData
        self.episodeList.subscribe(onNext: { [weak collectionView, weak self] (episodeList) in
            self?.previousSelectedCell = nil
            collectionView?.reloadData()
        }).disposed(by: self.disposeBag)
        
        print("did add RXObserver to collectionView")
    }
    
    func selectedSameEpisode(episodeID: Int) -> Bool {
        if let _previewEpisode = self.previewEpisode.value {
            return _previewEpisode.id == episodeID
        }
        return false
    }
    
    // MARK: - CollectionView Delegate, Datasource and Layout
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return episodeList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: XploreEpisodeCell = collectionView.dequeueReusableCell(withReuseIdentifier: XploreEpisodeDatasource.cellIdentifier,
                                                                         for: indexPath) as! XploreEpisodeCell
        let episode: Episode = self.episodeList.value[indexPath.item]
        
        cell.configure(with: episode)
        cell.overlayImageView.isHidden = true
        var locked = false
        if (DeviceHelper.shared.isEssentialUiOnly) {
           if (indexPath.row % 3 == 2) {
                if !unlockedVideoIdList.contains(self.episodeList.value[indexPath.item].id) {
                    locked = true
                }
            }
            
        }
        
        if locked {
            cell.overlayImageView.isHidden = false
        }else{
            
        }
//        print("\(String(describing: type(of: self)))Cell frame: \(cell.frame)")
//        cell.setInActive()
        
        cell.updateCellSelection(selection: selectedSameEpisode(episodeID: episode.id))
        
        return cell
    }
    func tryUnlock(videoId:Int) {
            let message = "Are you sure to use 100 tcoins to unlock this video?\n Current Balance: \(UserHelper.shared.currentUserProfile()?.myPointBalance ?? 0)"
            let popup = CustomPopup(message: message)

              popup?.button1.setTitle("Yes", for: .normal)
              popup?.button2.setTitle("No", for: .normal)
                //    CGRect buttonFrame = popup.button1.frame;
                //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
                //    popup.button1.frame = buttonFrame;
                let popupBlock = popup
                //    popup.backBtn. hidden  = YES;
                popup?.setupTwoButton()
                popup?.completionButton1 = {
                    
                    OONAApi.shared.postRequest(url: APPURL.unlockItem(), parameter: [:])
                              .responseJSON { (response) in
                                  switch response.result {
                                      
                                  case .success(let value):
                                      let json = JSON(value)
                                      if let code = json["code"].int,
                                          let status = json["status"].string {
                                          APIResponseCode.checkResponse(withCode: code.description,
                                                                        withStatus: status,
                                                                        completion: { (message, isSuccess) in
                                                                          if(isSuccess){
                                                                               self.unlockedVideoIdList.append(videoId)
                                                                                                  UserDefaults.standard.set(self.unlockedVideoIdList, forKey: "unlockedVideoIdList")
                                                                                                  self.cView?.reloadData()
                                                                              //                    self.gameCollectionView.reloadData()
                                                                                                  popupBlock?.dismiss()
                                                                          } else {
                                                                               let message = "You dont have enough tcoin to unlock"
                                                                                                     let popup = CustomPopup(message: message)

                                                                                                       popup?.button1.setTitle("OK", for: .normal)
                                                                                                       
                                                                        
                                                                                                         let popupBlock = popup
                                                                                                         //    popup.backBtn. hidden  = YES;
                                                                                                         popup?.setupOneButton()
                                                                                                         popup?.completionButton1 = {
                                                                                                             popupBlock?.dismiss()
                                                                                                    
                                                                                                             
                                                                                                         }
                                                                                                   
                                                                                                         popup?.alpha = 0
                                                                                                         let window = UIApplication.shared.keyWindow!
                                                                                                         window.addSubview(popup!) // Add your view here
                                                                                                         
                                                                                                         
                                                                                                         UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                                                                                                             popup?.alpha = 1
                                                                                                         })
                                                                                             
                                                                          }
                                                                          
                                          })
                                      }
                                  case .failure(let error):
                                      print(error)
                                      
                                      
                                      
                                      
                                  }
                          }
                    
                    
                   
                    
    //              PlayerHelper.shared.showXplore.accept(true)
                    
                }
                  popup?.completionButton2 = {
                      
                        popupBlock?.dismiss()
                          
                      
                  }
                popup?.alpha = 0
                let window = UIApplication.shared.keyWindow!
                window.addSubview(popup!) // Add your view here
                
                
                UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                    popup?.alpha = 1
                })
        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cView = collectionView
        if self.episodeList.value.count > 0 && indexPath.item < self.episodeList.value.count {
            collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
            var locked = false
            if (DeviceHelper.shared.isEssentialUiOnly) {
                if (indexPath.row % 3 == 2) {
                           if !unlockedVideoIdList.contains(self.episodeList.value[indexPath.item].id) {
                               locked = true
                           }
                 }
                       
                   }
                   
                  
            
            
            if (locked) {
                // locked. go buy
//                PlayerHelper.shared.showTcoinHome.accept(true)
                self.tryUnlock(videoId: self.episodeList.value[indexPath.row].id)
//                tryUnlock
            }else{
            
                let episode = self.episodeList.value[indexPath.item]
                let cell = collectionView.cellForItem(at: indexPath) as! XploreEpisodeCell
                if let _episode = self.previewEpisode.value {
                    if _episode == episode {
                        self.previewEpisode.accept(nil)
                        cell.updateCellSelection(selection: false)
                        previousSelectedCell = nil
                        return
                    }
                }
                
                self.previewEpisode.accept(episode)
                previousSelectedCell?.updateCellSelection(selection: false)
                previousSelectedCell = cell
                cell.updateCellSelection(selection: selectedSameEpisode(episodeID: episode.id))
            }
        }
        
    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    private let interitemSpacing: CGFloat = 10
    private let lineSpacing: CGFloat = 10
    private let numberOfRows: CGFloat = 3
    private let numberOfColumns: CGFloat = 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
//        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
        let height = (width / (16/9)) + 17//label height
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation: CGPoint = scrollView.panGestureRecognizer.translation(in: scrollView.panGestureRecognizer.view)
        if abs(translation.y) > 5 {
            if let _ = self.previewEpisode.value {
                self.previewEpisode.accept(nil)
                self.previousSelectedCell?.updateCellSelection(selection: false)
                self.previousSelectedCell = nil
            }
        }
    }
}
