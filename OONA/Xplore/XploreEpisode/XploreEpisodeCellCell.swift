//
//  XploreEpisodeCellCell.swift
//  OONA
//
//  Created by nicholas on 7/26/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage

class XploreEpisodeCellCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var underline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with episode: Episode) {
        img?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: episode.imageUrl){
            
            self.img?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        text.text = episode.name
        
    }
    func setInActive(){
        self.underline.isHidden = true
    }
    func setActive(){
        self.underline.isHidden = false
    }

}
