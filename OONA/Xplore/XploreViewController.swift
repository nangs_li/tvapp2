//
//  XploreViewController.swift
//  OONA
//
//  Created by nicholas on 7/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

class XploreViewController: BaseViewController {

    @IBOutlet weak var categoriesView: UIView!
    
    @IBOutlet weak var xploreContentView: UIView!
    
    var categories: [Category] = [Category]()
    var hasLoaded = false
    
    let categoryViewController: CategoryViewController = {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
    }()
    
    let xploreEpisodeViewController: XploreEpisodeViewController = {
        return UIStoryboard(name: "Xplore", bundle: nil).instantiateViewController(withIdentifier: "XploreEpisodeViewController") as! XploreEpisodeViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(!hasLoaded){
            self.setupViews()
            self.addRXObserver()
            self.loadCategoryList()
            hasLoaded = true
        }
    }
    
    
    deinit {
//     print("Deallocating \(self)")
        XploreHelper.shared.selectedCategoryID.accept(NSNotFound)
    }
}

extension XploreViewController {
    
// MARK: - Configure Views
    private func setupViews() {
        self.loadCategoryView()
        self.loadXploreEpisodeView()
    }
    
    private func loadCategoryView() {
        // Add Child View Controller
        self.addChild(self.categoryViewController)
        self.categoryViewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let categoryVCView: UIView = self.categoryViewController.view
        self.categoriesView.addSubview(categoryVCView)
        // Configure Child View
        categoryVCView.frame = self.categoriesView.bounds
        categoryVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.categoryViewController.didMove(toParent: self)
    }
    
    private func loadXploreEpisodeView() {
        // Add Child View Controller
        self.addChild(self.xploreEpisodeViewController)
        self.xploreEpisodeViewController.willMove(toParent: self)
        
        // Add child VC's view to parent
        let xploreEpisodeVCView: UIView = self.xploreEpisodeViewController.view
        self.xploreContentView.addSubview(xploreEpisodeVCView)
        // Configure Child View
        xploreEpisodeVCView.frame = self.xploreContentView.bounds
        xploreEpisodeVCView.snp.makeConstraints { (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        }
        // Register child VC
        self.xploreEpisodeViewController.didMove(toParent: self)
    }
}


extension XploreViewController {
    
// MARK: - Functional Methods
    func addRXObserver() {
        categoryViewController.selectedCategoryID.subscribe(onNext: { (categoryID) in
            print("Category changed to: \(categoryID)")
            XploreHelper.shared.selectedCategoryID.accept(categoryID)
        }).disposed(by: self.disposeBag)
        
//        XploreHelper.shared.selectEpisode.subscribe(onNext: { [weak self] (_) in self?.exitXplore() }).disposed(by: self.disposeBag)
    }
    
    func setDefaultCategory() {
        if self.categories.count > 0 {
            let category: Category = categories[0]
            self.categoryViewController.setCategory(cat: category)
        }
    }
    
    func loadCategoryList() {
        XploreHelper.shared.getCategoryList(type: "explore",
                                            success: { [weak self] (categories) in
                                                self?.categories = categories
                                                if categories.count > 0 {
                                                    self?.categoryViewController.category = categories[0]
                                                }
                                                self?.categoryViewController.categoryList = categories
            },
                                            failure: nil)
    }
    
    func prepareForFirstAppear() {
        self.setDefaultCategory()
        self.reloadContent()
    }
    
    func reloadContent() {
        self.xploreEpisodeViewController.reloadContent()
    }
    
    func exitXplore() {
        self.dismiss(animated: true,
                     completion: nil)
    }
}
