//
//  XploreHelper.swift
//  OONA
//
//  Created by nicholas on 7/19/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RxCocoa
import RxSwift

class XploreHelper: NSObject {
    static let shared: XploreHelper = { return XploreHelper() }()
    
    var selectedCategoryID: BehaviorRelay<Int> = BehaviorRelay(value: NSNotFound)
    var selectedVideo: PublishRelay<Video?> = PublishRelay<Video?>()
    var current : [Int]  = []
    var selectedLockedVideo : Int = 2
    /// Call this function to request category list in first row
    ///
    /// - Parameters:
    ///   - type: Category types in home / tv / explore.
    ///   - success: Call back will fire when request is success but could still be contain some other error even request were made successfully.
    ///   - failure: Call back fill fire when request encountered error or failed to made.
    func getCategoryList(type: String,
                         success: @escaping (([Category]) -> Void),
                         failure: ((Error?) -> Void)?) {
        OONAApi.shared.postRequest(url: APPURL.Category,
                                   parameter: ["type": type])
            .responseJSON{ [weak self] (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            let xploreCategory: [Category] = JSON(json["categories"])
                                                                .oonaModelArray(action: { (category: Category) in
                                                                    var newCategory = category
                                                                    
                                                                    if let categoryID = self?.selectedCategoryID.value {
                                                                        if categoryID == newCategory.id {
                                                                            newCategory.selected = true
                                                                        }
                                                                    }
                                                                    return newCategory
                                                                })
                                                            success(xploreCategory)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                        }
                                                        
                        })
                    }
                    
                case .failure(let error):
                    print(error)
                    failure?(error)
                }
        }
    }
    
    /// Call this function for request episode list
    ///
    /// - Parameters:
    ///   - fetchMode: This can switch in between making new episode list request and load more episode by providing parameter in refresh / nextPage
    ///   - xploreEpisode: This object contains
    ///   - success: Call back will fire when request is success but could still be contain some other error even request were made successfully.
    ///   - failure: Call back fill fire when request encountered error or failed to made.
    func getEpisodeList(xploreEpisode: XploreEpisode,
                        success: @escaping ((Int, [Episode]) -> Void),
                        failure: ((Error?) -> Void)?) -> Void {
        
//        let updatedOffset: Int = fetchMode == .nextPage ? xploreEpisode.nextRequestRange.location : 0
        let parameter: [String: Any] =
            ["categoryId": xploreEpisode.categoryID,
             "chunkSize": xploreEpisode.nextRequestRange.length,
             "offset": xploreEpisode.nextRequestRange.location]
        OONAApi.shared.postRequest(url: APPURL.ExploreEpisode(),
                                   parameter: parameter)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { message , _success in
                                                        if(_success){
                                                            let episodes = JSON(json["episodes"]).oonaModelArray() as [Episode]
                                                            success(xploreEpisode.categoryID, episodes)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                            failure?(nil)
                                                        }
                        })
                    }
                case .failure(let error):
                    print(error)
                    failure?(error)
                    
                }
        }
    }
    
    /// Inject ad into selected video
    ///
    /// - Parameters:
    ///   - video: video to inject
    ///   - videoOwner: identical video is belong to channel or episode
    ///   - completion: will call this block no matter success or failure
    func updateStreamingInfo(with video: Video,
                             videoOwner: VideoOwner,
                             completion: @escaping (_ success: Bool, _ result: Video?, _ error: Error?) -> Void) {
        var urlString: String?
        
        switch videoOwner {
        case .channel:
            urlString = APPURL.StreamingInfo(channelId: String(video.channelId))
        case .episode:
            urlString = APPURL.StreamingInfo(channelId: String(video.channelId), episodeId: String(video.episodeId))
        default: break
        }
        
        guard let _ = urlString else { return }
        
        OONAApi.shared.postRequest(url: urlString!,
                                   parameter: [:]).responseJSON{ (response) in
                                    print("streamingInfo ",response)
                                    switch response.result {
                                    case .success(let value):
                                        let json = JSON(value)
                                        let code  = json["code"].int
                                        let status = json["status"].string
                                        APIResponseCode.checkResponse(withCode:code?.description ,
                                                                      withStatus: status,
                                                                      completion: {
                                                                        message , _success in
                                                                        print("streamingInfo ", message as Any)
                                                                        if(_success){
                                                                            video.update(json:json)
                                                                        }
                                                                        completion(_success, video, nil)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        completion(false, video, error)
                                    }
        }
    }

}
