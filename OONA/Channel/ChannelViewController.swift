//
//  ChannelViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources

class ChannelViewController: BaseViewController{
    
    struct ChannelTask {
        var id :String
    }
    typealias channelSelectedBlock = (_ cbaction : CBAction , _ channel :Channel, _ episode : Episode?) ->()
    var completionBlock:channelSelectedBlock?
    var cbaction:CBAction = CBAction.none
    
    @IBOutlet weak var nowPlaying: UILabel!
    var channelList = [Channel]()
    var channel : Channel?
    var category : Category?
    var offset : Int = 0
    var loadingChannelList : Bool = false
    var channelItems : Observable = Observable.just([])
    var items : Observable<[Channel]> = Observable.just([])
    
    /*now playing view*/
    @IBOutlet weak var nowPlayingView : UIView!
    @IBOutlet weak var nowPlayingViewImage : UIImageView!
    @IBOutlet weak var nowPlayingViewText : UILabel!
    @IBOutlet weak var nowPlayingConstraint : NSLayoutConstraint!
    @IBOutlet weak var nowPlayingSeperatorViewLeftPadding : NSLayoutConstraint!
    
    @IBOutlet weak var nowPlayingViewSeperator : UIView!
    @IBOutlet weak var cvViewLeftPadding: NSLayoutConstraint!

    @IBOutlet weak var cv: UICollectionView! {
        didSet {
            self.cv.rx.itemSelected.subscribe(onNext: { indexPath in
                
                //set cell active if onclick , otherwise set inactive
                for i in 0...self.channelList.count {
                    if let cell = self.cv.cellForItem(at: IndexPath(row: i, section: 0)) as? ChannelCell{
                        if(i == indexPath.row){
                            cell.setActive()
                        }else{
                            cell.setInActive()
                        }
                    }
                }
                
                guard let cb = self.completionBlock else { return }
                let channel = self.channelList[indexPath.row]
                //set selected item to loacl instance
                self.channel = channel
                EpisodeHelper.shared.currentEpisode = nil//reset selected episode..
                
                cb(.select, channel, nil)
                
            }).disposed(by: self.disposeBag)
            self.cv.rx.contentOffset.subscribe(onNext: {scrollingOffset in
                let pxToEnd = self.cv.contentSize.width - scrollingOffset.x - self.cv.frame.width
//                print(self.cv.contentSize.width,"    ",pxToEnd)
                if (  pxToEnd  <= self.cv.frame.width*0.7){
                    print("need to fetch more channels")
                    self.loadChannelList()
                }
            }).disposed(by: self.disposeBag)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cv.register(UINib(nibName: "ChannelCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCell")
        
        //let video = PlayerHelper.shared.getVideo()
        //loadNowPlayingView(video : video)
        //loadChannelList()
        
        loadChannelList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        //loadNowPlayingView(video : video)
        //only if no
        let video = PlayerHelper.shared.getVideo()
        loadNowPlayingView(video : video)
        
    }
    
    
    override func getLeftConstraint() -> CGFloat{
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C")
                return APPSetting.leftConstraintForInnerPageSafearea + 10
            case 1334:
                //print("iPhone 6/6S/7/8")
                return APPSetting.leftConstraintForInnerPageSafearea + 10
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return APPSetting.leftConstraintForInnerPageSafearea + 10
            case 2436: break
            //print("iPhone X, XS")
            case 2688: break
            //print("iPhone XS Max")
            case 1792: break
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        
        return APPSetting.leftConstraintForInnerPage 
        
    }
    
    func loadNowPlayingView(video : Video?){
        //if hasSubMenu(){
            //e.g. music .. no display now playing
        //    disableNowPlaying()
        //}
        //else if let _video = video   {
        if let _video = video   {
            nowPlayingConstraint.constant = APPSetting.episodeChannelObjectWidth
            cvViewLeftPadding.constant = APPSetting.defaultCvViewLeftPadding
            
            nowPlayingViewText.text = _video.channelName
            if let _imageUrl = _video.imageUrl{
                nowPlayingViewImage.sd_setImage(with: URL.init(string: _imageUrl), placeholderImage: UIImage(named: "channel-placeholder-image"))
            }
            nowPlayingViewSeperator.isHidden = false
            nowPlayingViewImage.layer.cornerRadius = APPSetting.defaultCornerRadius
            nowPlayingView.isHidden = false
            self.view.layoutIfNeeded()
            
        }else{
            disableNowPlaying()
        }
    }
    
    
    func disableNowPlaying(){
        
        //disable now playing view if no now playing video
        self.nowPlayingConstraint.constant = 0.0
        //self.nowPlayingViewLeftPadding.constant = 0.0
        //self.nowPlayingSeperatorViewLeftPadding.constant = 0.0\
        self.cvViewLeftPadding.constant = 0.0
        nowPlayingViewSeperator.isHidden = true
        nowPlayingView.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    func loadChannelList() {
        if loadingChannelList {
            print("get ch list alrdy loading")
            return
        }
        loadingChannelList = true
        
        if(self.channel==nil){
            if let currentChannel = ChannelHelper.shared.currentChannel{
                self.channel = currentChannel
            }
        }
        
        var categoryId : String? = nil
        var filterType : ChannelSetting.channelListFilterType = ChannelSetting.channelListFilterType.none
        
        let subMenu : SubMenu = MenuHelper.shared.getCurrentSubMenu()
        
        if(subMenu == .home){
            if let _cat = category{
                filterType = ChannelSetting.channelListFilterType(rawValue:_cat.id) ?? .none
                categoryId = ""
            }
        }else if(subMenu == .music){
            filterType = ChannelSetting.channelListFilterType.category
            self.category = Category.init(id: 15 , name: "Music", _type: .category)
            categoryId = self.category?.id.description
        }else{
            if let _cat = category{
                filterType = ChannelSetting.channelListFilterType.category
                categoryId = _cat.id.description
            }
        }
        offset = channelList.count
        
        ChannelHelper.shared.getChannelList(open: false, categoryId : categoryId, currentChannelId: nil, filterType: filterType, offset: offset, sortBy: nil, success: { response in
            print("get ch list success")
            let channels = response
            self.channelList.append(contentsOf: channels)
            
            guard let cb = self.completionBlock else { return }
            if(self.channelList.count > 0){
                let firstChannel = self.channelList[0]
                
                //check if channel exist in current list..
                var existChannel = false
                
                
                if(self.channel==nil){
                    self.channel = firstChannel
                }else{
                    for channel in self.channelList{
                        if(channel.id == self.channel!.id){
                            existChannel = true
                        }
                    }
                    if(!existChannel){
                        //if channel not exist in the channellist select the first channel
                        self.channel = firstChannel
                    }
                }
                
                self.channel = firstChannel
                
                if(self.offset == 0){
                    //not load more
                    cb(.load, firstChannel, nil)
                }
            }
            
            self.appendData(dataToAppend: channels)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.loadingChannelList = false
            }
            
        }, failure: { code , message in
            if let _msg = message{
                self.showAlert(title: "", message : _msg)
            }
            
        })
    }
    private func prependData(dataToPrepend : [Channel]) {
        let newObserver = Observable.just(dataToPrepend)
        items = Observable.combineLatest(items, newObserver) {
            $1+$0
        }
        bindData()
    }
    private func bindData(){
        
        self.cv.dataSource = nil
        
        items
            .bind(to: self.cv.rx.items){ (collectionView, row, _channel) in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = self.cv.dequeueReusableCell(withReuseIdentifier: "ChannelCell", for: indexPath) as! ChannelCell
                
                cell.configure(with: _channel)
                
                if let selectedChannelId = self.channel?.id{
                    if(_channel.id == selectedChannelId){
                        cell.setActive()                
                    }else{
                        cell.setInActive()
                    }
                }else{
                    cell.setInActive()
                }
                
                return cell
            }
            .disposed(by: self.disposeBag)
        
    }
    
    
    private func appendData(dataToAppend : [Channel]) {

        let newObserver = Observable.just(dataToAppend)
        items = Observable.combineLatest(items, newObserver) {
            $0+$1
        }
        bindData()
    }
    
    @IBAction func clickNowPlaying(_ sender: UIButton) {
        guard let cb = self.completionBlock else { return }
        //insert empty channel
        cb(.nowPlaying, Channel.init(), nil)
    }
    
    override func appLanguageDidUpdate() {
        self.nowPlaying.text = LanguageHelper.localizedString("NOW_PLAYING")
    }
}
