//
//  ChannelHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON
class ChannelHelper: NSObject {
    static let shared = ChannelHelper()
    
    var NormalChannelMode = true
    var channelId : Int = 0
    var offset: Int  = 0
    var chunkSize : Int = 0
    var VideoTutorialShowing = false
    var result = [Channel]()
    var currentChannelListCache : [[String: Any]] = [[String: Any]]()  //use to cache channels
    var currentChannelList : [Channel]? //use in first load of the app to revisit previous channel
    
    var currentChannel : Channel?
    
    
    
    func getCurrentChannelFromStorage()->Channel?{
        
        /*
        if let data = UserDefaults().data(forKey: "currentChannel"){
            if let channel = NSKeyedUnarchiver.unarchiveObject(with: data) as? Channel {
                self.currentChannel = channel
                return channel
            }else{
                return nil
            }
        }else{
            return nil
        }*/
        
        let currentChannelId = UserDefaults().integer(forKey: "currentChannelId")
        if(currentChannelId==0){ //nil
            return nil
        }else{
            return Channel.init(id: currentChannelId)
        }
        //}else{
        //    return nil
        //}
    }
    
    func clearChannelFromStorage(){
        //UserDefaults.standard.set(episode.id, forKey:"currentEpisodeId")
        UserDefaults.standard.removeObject(forKey: "currentChannelId")
        
    }
    
    func setCurrentChannel(channel : Channel){
        self.currentChannel = channel
        //let data  = NSKeyedArchiver.archivedData(withRootObject: channel)
        //let defaults = UserDefaults.standard
        let userStandard = UserDefaults.standard
        print("save: " + channel.id.description)
        userStandard.set(channel.id, forKey:"currentChannelId")
    }
    
    func getChannel() -> Channel?{
        return currentChannel
    }
    func updateVideoByStreamingInfo(video: Video,channel : Channel, completion: @escaping (_ success: Bool, _ result: Video? , _ error: Error?) -> Void){
        self.channelId = channel.id
        /*TODO???
         let chID = "\(ChannelList[buttonTag]["id"])"
         if chID.hasPrefix("-") {
         return
         }*/
        
        /*TODO :" firebase*/
        
        
        /*TODO :" YOUtube no screenshot
         if (YTMode){
         [self notAllowScreenShot];
         
         }*/
        /*TODO: explainer
         
         if (LiveMode) {
         if (![[NSUserDefaults standardUserDefaults] valueForKey:@"home_channel_live"]) {
         [self initXplainer:@"explainer1a" withEngImage:@"explainer1a_en" withKey:@"home_channel_live"];
         }
         } else if ([chObject[@"type"] isEqualToString:@"linear"]) {
         if (![[NSUserDefaults standardUserDefaults] valueForKey:@"home_channel_vod"]) {
         [self initXplainer:@"explainer3a" withEngImage:@"explainer3a_en" withKey:@"home_channel_vod"];
         }
         }
         */
        
        OONAApi.shared.postRequest(url: APPURL.StreamingInfo(channelId: channel.id.description), parameter:[:]).responseJSON{ (response) in
            print("channel helper streamingInfo ",response)
            if(response.result.isSuccess){
                //  if let JSON = response.result.value as! NSDictionary? {
                if let json = (try? JSON(data: response.data!)) as? JSON {
                    
                    let code  = json["code"].int
                    let status = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                video.update(json:json)
                                video.videoName = json["channelName"].stringValue
                                
                                //PlayerHelper.shared.playVideo(video: video)
                                //NotificationCenter.default.post(name: Notification.Name("playVideo"), object:self, userInfo:["url":video.streamingUrl])
                                
                                completion(true,video, nil)
                                
                            }
                        })
                    }
                }
            }
        }
    }
    func getVideoByChannel(channel : Channel, completion: @escaping (_ success: Bool, _ result: Video? , _ error: Error?) -> Void){
        self.channelId = channel.id
        /*TODO???
         let chID = "\(ChannelList[buttonTag]["id"])"
         if chID.hasPrefix("-") {
         return
         }*/
        
        /*TODO :" firebase*/
        
        
        /*TODO :" YOUtube no screenshot
         if (YTMode){
         [self notAllowScreenShot];
         
         }*/
        /*TODO: explainer
         
         if (LiveMode) {
         if (![[NSUserDefaults standardUserDefaults] valueForKey:@"home_channel_live"]) {
         [self initXplainer:@"explainer1a" withEngImage:@"explainer1a_en" withKey:@"home_channel_live"];
         }
         } else if ([chObject[@"type"] isEqualToString:@"linear"]) {
         if (![[NSUserDefaults standardUserDefaults] valueForKey:@"home_channel_vod"]) {
         [self initXplainer:@"explainer3a" withEngImage:@"explainer3a_en" withKey:@"home_channel_vod"];
         }
         }
         */
        
        OONAApi.shared.postRequest(url: APPURL.StreamingInfo(channelId: channel.id.description), parameter:[:]).responseJSON{ (response) in
            print(response)
            if(response.result.isSuccess){
                //  if let JSON = response.result.value as! NSDictionary? {
                if let json = (try? JSON(data: response.data!)) as? JSON {
                    
                    let code  = json["code"].int
                    let status = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                
                                let video = Video.init(json: json, chType: channel.type, chId: channel.id, epId: 0, _imageUrl : channel.imageUrl,vdoName:channel.name)
                                //PlayerHelper.shared.playVideo(video: video)
                                //NotificationCenter.default.post(name: Notification.Name("playVideo"), object:self, userInfo:["url":video.streamingUrl])
                                
                                completion(true,video, nil)
                                
                            }
                        })
                    }
                }
            }
        }
    }
    
    
    func getChannelList(open: Bool ,categoryId: String?, currentChannelId: String?, filterType : ChannelSetting.channelListFilterType , offset:Int? , sortBy: String? , success :@escaping ([Channel]) -> (), failure : @escaping ((Int?, String?) -> ())){
        
        var param:[String: Any] =  ["filter": filterType.description, "categoryId":categoryId ?? ""]
        
        if let _offset = offset{
            param["offset"] = _offset.description
        }
        if (DeviceHelper.shared.geoBlock) {
            if categoryId == "15" {
                param["videoType"] = "youtubePlaylists"
            }
        }
        if(open){
            if let _categoryId = categoryId{
                var channelDict : [String: Any] =  [:]
                channelDict["categoryId"] = _categoryId.description
                param["open"] = channelDict
                //print("open!!" + channelDict.description)
            }
        }
        
        var getResultFromCache = false
        
            //get the channel from cache
        
            for currentChannelListCacheParam in currentChannelListCache {
                
                if(currentChannelListCacheParam["filter"] as? String == param["filter"] as? String && currentChannelListCacheParam["categoryId"] as? String == param["categoryId"] as? String && currentChannelListCacheParam["offset"] as? String == param["offset"] as? String && currentChannelListCacheParam["open"] as? String == param["open"] as? String){
                    
                    print("get cache channel list : filter " + param["filter"].debugDescription + " categoryId" + param["categoryId"].debugDescription + " offset" + param["categoryId"].debugDescription + " open" +  param["open"].debugDescription)
                    
                    if let _channel = currentChannelListCacheParam["result"] as? [Channel]{
                        getResultFromCache = true
                        success(_channel)
                    }
                }
                
            }
        
        //if the channel not found from cahce, get from api
        
        if(!getResultFromCache){
            
            OONAApi.shared.postRequest(url: APPURL.Channel, parameter: param).responseJSON{ (response) in
                if(response.result.isSuccess){
                    
                    if let JSON = response.result.value as! NSDictionary? {
                        let code : Int? = JSON["code"] as? Int
                        let status : String? = JSON["status"] as? String
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    if let channels :  [NSDictionary] = JSON["channels"] as? [NSDictionary]{
                                        var _result = [Channel]()
                                        for _channel in channels {
                                            if(_channel.count > 0){
                                                _result.append(Channel.init(withDict: _channel))
                                            }
                                        }
                                        param["result"] = _result
                                        
                                        self.currentChannelListCache.append(param)
                                        self.currentChannelList = self.result
                                        
                                        success(_result)
                                    }
                                }else{
                                    failure(code, message)
                                }
                            })
                        }else{
                            //failure(0, "")
                        }
                    }
                }
            }
        }
        
    }
    
    func getFirstChannelByCategory(categoryId: String?  , filterType : ChannelSetting.channelListFilterType , success :@escaping (Channel) -> (), failure : @escaping ((Any) -> ())){
        let param:[String: String] =  ["filter": filterType.description]
        OONAApi.shared.postRequest(url: APPURL.Channel, parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let JSON = response.result.value as! NSDictionary? {
                    let code : Int? = JSON["code"] as? Int
                    let status : String? = JSON["status"] as? String
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                if let channels :  [NSDictionary] = JSON["channels"] as? [NSDictionary]{
                                    self.result = []
                                    var count = 0
                                    var result = Channel.init()
                                    for _channel in channels {
                                        if(count==0){
                                            result = Channel.init(withDict: _channel)
                                        }
                                        count = count + 1
                                    }
                                    success(result)
                                }
                            }
                        })
                    }
                }
            }else{
                //failure(nil)
            }
        }
    }
    
    func getNextChannelByCategory(categoryId: String?  , filterType : ChannelSetting.channelListFilterType , success :@escaping (Channel) -> (), failure : @escaping ((Any) -> ())){
        
            var currentIndex = 0
            var nextIndex : Int? = nil
            if let _currentChannelList = currentChannelList{
                
                for channel in _currentChannelList
                {
                    print(self.currentChannel?.id.description ?? "no stuff")
                    if channel.id == self.currentChannel?.id {
                        nextIndex = currentIndex
                        success(_currentChannelList[nextIndex ?? 0])
                    }
                    
                    currentIndex += 1
                }
                
                if(nextIndex==nil){
                    failure("channel not found")
                }
            }
    }
    
}

