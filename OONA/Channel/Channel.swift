//
//  ChannelHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct Channel: OONAModel, Equatable  {
    static func == (lhs: Channel, rhs: Channel) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id : Int = 0
    var isFavourite : Bool = false
    var imageUrl : String = ""
    var description : String = ""
    var url : String = ""
    var name : String = ""
    var isYoutube : Bool?
    var type : String = ""
    var streamingUrl : String?
    var brightcoveId : String?
    var video : Video?
    var seasons : [String] = []
    
    
    init(){
    }
    
    init?(id: Int) {
        //use in first load when we only have stored the id
        self.id = id
    }
    
    init?(json: JSON) {
        if let _id = json["id"].int {
            self.id = _id
        }
        if let _isFavourite = json["isFavourite"].bool {
            self.isFavourite = _isFavourite
        }
        if let _imageUrl = json["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        if let _description = json["description"].string {
            self.description = _description
        }
        if let _url = json["url"].string {
            self.url = _url
        }
        if let _name = json["name"].string {
            self.name = _name
        }
        if let _isYoutube = json["isYoutube"].bool{
            self.isYoutube = _isYoutube
        }
        if let _type = json["type"].string {
            self.type = _type
            print("the type " + _type)
        }
        
        
        if let _preview = json["preview"].dictionary {
            var previewBrightCoveId: Int?
            var previewStreamingUrl: String?
            var previewEpisodeId: Int = 0
            var previewYoutubeId: String?
            var previewName: String = ""
            var previewImageUrl: String = ""
            var previewChannelName: String = ""
            var previewVideoName: String = ""
            
            if let _brightcoveId = _preview["brightcoveId"]?.int {
                //self.brightcoveId = _brightcoveId
                previewBrightCoveId = _brightcoveId
            }
            if let _streamingUrl = _preview["streamingUrl"]?.string {
                //self.streamingUrl = _streamingUrl
                previewStreamingUrl = _streamingUrl
            }
            if let _youtubeId = _preview["youtubeId"]?.string {
                //self.streamingUrl = _streamingUrl
                previewYoutubeId = _youtubeId
            }
            if let _channelName = _preview["channelName"]?.string {
                //self.streamingUrl = _streamingUrl
                previewChannelName = _channelName
            }
            if let _videoName : String  = _preview["episodeName"]?.string {
                
                previewVideoName = _videoName
            }else{
                previewVideoName = previewChannelName
            }
            if let _episodeId = _preview["episodeId"]?.int {
                //self.streamingUrl = _streamingUrl
                previewEpisodeId = _episodeId
            }
            if let _imageUrl = json["imageUrl"].string {
                //self.streamingUrl = _streamingUrl
                previewImageUrl = _imageUrl
            }
            
            let videoType = VideoType(rawValue: self.type) ?? .youtube//default youtube
//
//            streamingUrl: previewStreamingUrl, brightcoveId: previewBrightCoveId?.description, youtubeId: previewYoutubeId, channelName: previewChannelName, imageUrl: previewImageUrl, videoType: videoType, _channelId: self.id, _episodeId: previewEpisodeId
            
            self.video = Video(streamingUrl: previewStreamingUrl, brightcoveId: previewBrightCoveId?.description, youtubeId: previewYoutubeId, videoName: previewVideoName, channelName: previewChannelName, imageUrl: previewImageUrl, videoType: videoType, _channelId: self.id, _episodeId: previewEpisodeId)
        }
    }

    init(withDict obj:NSDictionary){
        if let _id : Int = obj["id"] as? Int{
            self.id = _id
        }
        if let _isFavourite : Bool  = obj["isFavourite"] as? Bool{
            self.isFavourite = _isFavourite
        }
        if let _imageUrl : String  = obj["imageUrl"] as? String{
            self.imageUrl = _imageUrl
        }
        if let _description : String  = obj["description"] as? String{
            self.description = _description
        }
        if let _url : String  = obj["url"] as? String{
            self.url = _url
        }
        if let _name : String  = obj["name"] as? String{
            self.name = _name
        }
        if let _isYoutube : Bool  = obj["isYoutube"] as? Bool{
            self.isYoutube = _isYoutube
        }
        if let _type : String  = obj["type"] as? String{
            self.type = _type
            print("the type" + _type)
        }


        if let _preview : NSDictionary = obj["preview"] as? NSDictionary {
            var previewBrightCoveId : Int?
            var previewStreamingUrl : String?
            var previewEpisodeId : Int = 0
            var previewYoutubeId : String?
            var previewName : String = ""
            var previewImageUrl : String = ""
            var previewChannelName : String = ""
            var previewVideoName: String = ""
            if let _brightcoveId : Int  = _preview["brightcoveId"] as? Int{
                //self.brightcoveId = _brightcoveId
                previewBrightCoveId = _brightcoveId
            }
            if let _streamingUrl : String  = _preview["streamingUrl"] as? String{
                //self.streamingUrl = _streamingUrl
                previewStreamingUrl = _streamingUrl
            }
            if let _youtubeId : String  = _preview["youtubeId"] as? String{
                //self.streamingUrl = _streamingUrl
                previewYoutubeId = _youtubeId
            }
            if let _channelName : String  = _preview["channelName"] as? String{
                //self.streamingUrl = _streamingUrl
                previewChannelName = _channelName
            }
            if let _videoName : String  = _preview["episodeName"] as? String {
                
                previewVideoName = _videoName
            }else{
                previewVideoName = previewChannelName
            }
            if let _episodeId : Int  = _preview["episodeId"] as? Int{
                //self.streamingUrl = _streamingUrl
                previewEpisodeId = _episodeId
            }
            if let _imageUrl : String  = obj["imageUrl"] as? String{
                //self.streamingUrl = _streamingUrl
                previewImageUrl = _imageUrl
            }

            let videoType = VideoType.init(rawValue: self.type) ?? .vod//default youtube
            print("channel init video, preview name",previewVideoName)
            self.video = Video.init(streamingUrl: previewStreamingUrl, brightcoveId: previewBrightCoveId?.description, youtubeId: previewYoutubeId, videoName: previewVideoName, channelName: previewChannelName, imageUrl: previewImageUrl, videoType: videoType, _channelId: self.id, _episodeId: previewEpisodeId)
        }

    }
}


//use for episode page.
struct LiveChannel {
    
    /*live channel*/
    var ImageUrl : String?
    var StreaimgLink : String?
    var BrightcoveId : String?
    
    init(){
    }
    
    init(withDict obj:NSDictionary){
        if let _preview : String  = obj["preview"] as? String {
            if let json = (try? JSON(parseJSON: _preview)) as? JSON {
                if let _brightcoveId : String  = obj["brightcoveId"] as! String{
                    self.BrightcoveId = _brightcoveId
                }
                if let _streamingUrl : String  = obj["streamingUrl"] as! String{
                    self.StreaimgLink = _streamingUrl
                }
            }
        }
    }
    
}

