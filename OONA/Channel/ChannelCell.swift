//
//  ChannelCell.swift
//  OONA
//
//  Created by Vick on 14/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SDWebImage

class ChannelCell: UICollectionViewCell {
    override func awakeFromNib() {
        
    }
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var underline: UIView!
    
    @IBAction func click() {
       // print("clicked")
    }

    func configure(with channel: Channel) {
        img?.layer.cornerRadius = APPSetting.defaultCornerRadius
        //text.textContainer.lineBreakMode = .byCharWrapping
        //text.textContainer.maximumNumberOfLines = 1
        self.img?.sd_setImage(with: URL.init(string: channel.imageUrl), placeholderImage: UIImage(named: "channel-placeholder-image"))
        text.text = channel.name

    }
    func setInActive(){
        self.underline.isHidden = true
    }
    func setActive(){
        self.underline.isHidden = false
    }
}
