//
//  Video.swift
//  OONA
//
//  Created by Jack on 10/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit

enum VideoType : String{
    case live = "live"
    case vod = "linear"
    case youtube = "youtube"
}

class Video {
    
    let urlKey = "url"
    let ytKey = "youtubeId"
    let bcoIdKey = "brightcoveId"
    let videoAdsKey = "videoAds"
    let displayAdsKey = "displayAds"
    let prerollAdsKey = "preroll"
    let midrollAdsKey = "midroll"
    let channelNameKey = "channelName"
    let imageUrlKey = "imageUrl"

    var videoName : String = ""
    var channelName: String
    var channelId: Int
    var episodeId: Int
    var type : VideoType
    var imageUrl: String?
    var streamingUrl: String?
    var brightcoveId: String?
    var youtubeId: String?
    var videoAdsJson : [String : JSON]
    var displayAdsJson : [String : JSON]
    var prerollAds : VideoAds
    var midrollAds : VideoAds
    var displayAdInterval : DisplayAdInfo
//    var videoAds : [String : JSON]
    
//    required init?(json: JSON) {
//        self.channelName = json[channelNameKey].string ?? ""
//        self.channelId = json["channelID"].int ?? 0
//        self.episodeId = json["episodeID"].int ?? 0
//        
//        
//    }
    
    init(json:JSON,chType:String,chId:Int,epId:Int,_imageUrl:String,vdoName:String){
        
        channelName = json[channelNameKey].string ?? ""
        channelId = chId ?? 0
        episodeId = epId ?? 0
        
        print("chType: "+chType.description)
        print("vdoName: ",vdoName)
        type = VideoType.init(rawValue: chType) ?? .vod
        
        imageUrl = json[imageUrlKey].string ?? _imageUrl
        streamingUrl = json[urlKey].string
        brightcoveId = json[bcoIdKey].string
        videoAdsJson = json[videoAdsKey].dictionary ?? [:]
        displayAdsJson = json[displayAdsKey].dictionary ?? [:]
        
        prerollAds = VideoAds.init(with: videoAdsJson[prerollAdsKey], adType: .preroll)
        midrollAds = VideoAds.init(with: videoAdsJson[midrollAdsKey], adType: .midroll)
        displayAdInterval = DisplayAdInfo.init(with: displayAdsJson["regularInterval"])
        videoName = vdoName
//        if (type == .vod) {
//            videoName = json["episodeName"].string ?? ""
//        }else{
//            videoName = channelName
//        }
//        midrollAds = videoAdsJson[midrollAdsKey]
    }
    func update(json:JSON){
        
        videoAdsJson = json[videoAdsKey].dictionary ?? [:]
        displayAdsJson = json[displayAdsKey].dictionary ?? [:]
        
        prerollAds = VideoAds.init(with: videoAdsJson[prerollAdsKey], adType: .preroll)
        midrollAds = VideoAds.init(with: videoAdsJson[midrollAdsKey], adType: .midroll)
        displayAdInterval = DisplayAdInfo.init(with: displayAdsJson["regularInterval"])
        //        midrollAds = videoAdsJson[midrollAdsKey]
    }
    init(streamingUrl : String? , brightcoveId : String? , youtubeId : String?,videoName:String, channelName : String, imageUrl : String, videoType : VideoType, _channelId: Int ,_episodeId: Int){
        
        self.streamingUrl = streamingUrl
        
        self.brightcoveId = brightcoveId
        
        self.youtubeId = youtubeId
        
        //self.videoAdsJson = videoAdsJson
        
        //self.prerollAds = prerollAds
        
        //self.midrollAds = midrollAds
        
        self.channelName = channelName
        
        self.imageUrl = imageUrl
        
        //TODO_OONA
        self.channelId = _channelId
        self.episodeId = _episodeId
        type = videoType
        
        videoAdsJson =  [:]
        
        prerollAds = VideoAds.init(with: nil, adType: .preroll)
        midrollAds = VideoAds.init(with: nil, adType: .midroll)
        
        displayAdsJson =  [:]
        displayAdInterval = DisplayAdInfo.init(with: nil)
    }
}
