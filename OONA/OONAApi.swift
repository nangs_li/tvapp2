//
//  NetworkModule.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import os.log
class OONAApi: NSObject {
    static let shared = OONAApi()
    
     var apiUrl = ""
     var apiRegion = ""
    
    public func currentDomain() -> String{
        //            return Domains.dev
        print("currentDomain: ",apiUrl)
//        apiUrl = "https://api.watchoona.com/"
        return apiUrl
    }
    public func currentRegion() -> String{
        //            return Domains.dev
        print("currentDomain: ",apiUrl)
        //        apiUrl = "api-us.oonatv.com"
        return apiRegion
    }
    public func setDomain(url:String, region:String) {
        //            return Domains.dev
        apiUrl = url
        apiRegion = region
    }
    
    
    public func analytics(eventName:String,
                          eventParameters:[String:String],
                          success : @escaping (Any) -> (),
                          failure : ((NSError) -> ())? = nil) {
        
//        Alamofire.request(APPURL.MultiAnalyticsEvent, method: .post, parameters: eventParameters).responseJSON{
        postRequest(url: APPURL.MultiAnalyticsEvent, parameter: eventParameters).responseJSON {
            response in switch response.result {
                case .success:
                print(response)
                case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    
    
    func getRequest(url:String, parameter:[String:Any]) -> DataRequest {
        os_log("[OONA] [API] getRequest url: %@",url)
        os_log("[OONA] [API] getRequest parameter: %@",parameter)
        return Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: requestHeader()).responseJSON(completionHandler: { (response) in
            
            print("[OONA] [API] getRequest response" , response )
            
        })
        
    }
    
    
    
    func deleteRequest(url:String, parameter:[String:Any]) -> DataRequest {
        os_log("[OONA] [API] postRequest url: %@",url)
        os_log("[OONA] [API] postRequest parameter: %@",parameter)
        return Alamofire.request(url, method: .delete, parameters: parameter, encoding: JSONEncoding.default, headers: requestHeader()).responseJSON(completionHandler: { (response) in
            
            print("[OONA] [API] postRequest response" , response )
            
        })
        
    }
    
    func putRequest(url:String, parameter:[String:Any]) -> DataRequest {
        os_log("[OONA] [API] putRequest url: %@",url)
        os_log("[OONA] [API] putRequest parameter: %@",parameter)
        return Alamofire.request(url, method: .put, parameters: parameter, encoding: JSONEncoding.default, headers: requestHeader()).responseJSON(completionHandler: { (response) in
            
            print("[OONA] [API] putRequest response" , response )
            
        })
        
    }
    
    func postRequest(url:String, parameter:[String:Any]) -> DataRequest {
        os_log("[OONA] [API] postRequest url: %@",url)
        os_log("[OONA] [API] postRequest parameter: %@",parameter)
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: requestHeader()).responseJSON(completionHandler: { (response) in
            
                print("[OONA] [API] postRequest response" , response )
            
        })

    }

    
    
    func postRequestWithHeader(url:String, parameter:[String:Any]) -> DataRequest{
        os_log("[OONA] [API] postRequest url: %@",url)
        os_log("[OONA] [API] postRequest parameter: %@",parameter)
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: requestHeader()).responseJSON(completionHandler: { (response) in
            
            print("[OONA] [API] postRequest response" , response )
            
        })
        
    }
    
    public func requestHeader () -> HTTPHeaders{
        var headers: HTTPHeaders = [:]
        
        if UserHelper.shared.currentUser.isLogin{ //is loginned
        //if let _user = UserHelper.shared.currentUser{ //is loginned
            let _user = UserHelper.shared.currentUser
                if let _accessToken = _user.accessToken{
                    headers  = [
                        "Authorization": "Bearer " + _accessToken ,
                        "uuid": DeviceHelper.shared.UUID ?? "",
                        "Content-Type": "application/json"
                    ]
                }
            
        }else{
            
            headers  = [
                "uuid": DeviceHelper.shared.UUID ?? "",
                "Content-Type": "application/json"
            ]
            
        }
        return headers
    }
    


}
