//
//  ChannelViewController.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources

class CategoryViewController: BaseViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    //max value
    let maxValue: Int = 10000
    
    
    var categoryList = [Category]() { didSet {
        (self.cv?.collectionViewLayout as? UICollectionViewFlowLayout)?.invalidateLayout()
        self.cv.reloadData()
        } }
    
    var category: Category?
    var selectedCategoryID: PublishRelay<Int> = PublishRelay()
    var selectedIndex: Int?
    
    @IBOutlet weak var backButtonLeftPadding: NSLayoutConstraint!
    @IBOutlet weak var leftViewLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonWrapper: UIView!
    
    typealias categorySelectedBlock = (_ category: Category?) ->()
    var completionBlock:categorySelectedBlock?
    
    var backButtonEnabled: Bool { get { return self.isBackButtonEnabled() }}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let catType = CategoryHelper.shared.currentCategoryType
        
        self.cv.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        self.cv.delegate = self
        self.cv.dataSource = self
        
        if(catType != "music" ){ // if music then no show..
            CategoryHelper.shared.getCategoryList(type: catType, success: { [weak self] response in
                let categories = response
                self?.categoryList = categories
                
                if categories.count > 0{
                    //default select first one..
                    let cat = categories[0]
                    self?.selectedCategoryID.accept(cat.id)
                    self?.category = cat
                    self?.cv.reloadData()
                    self?.completionBlock?(cat)
                    
                }
                
                if let checkSelf = self {
                    //update selectedCategoryID and collectionView offset
                    checkSelf.updateSelectedIDForSelectedCategory()
                    checkSelf.updateCollectionViewToSelectionBounds()
                }
                }, failure: { error in })
        }
        
            //at least have some padding..
        self.backButtonLeftPadding.constant = self.getLeftConstraint()
        //if self.backButtonLeftPadding.constant == 0 {
        //    self.backButtonLeftPadding.constant = 10 //for iphone6,7,8
        //}
        
    }
    
    @IBAction func clickBack() {
        self.hideBackView()
        guard let cb = self.completionBlock else { return }
        cb(nil)
    }
}

// MARK: - Functional methods
extension CategoryViewController {
    func setBackButtonText(text: String) {
        DispatchQueue.main.async {
            self.backButton.setTitle(text, for: .normal)
        }
    }
    func disableCategory(){
        self.cv.isHidden = true
        //self.backButtonWrapper.isHidden = true

    }
    func enableBackView() {
        self.cv.isHidden = true
        self.backButtonWrapper.isHidden = false
        
        /*
        //don't know why the left constraint isn't work.. so use the width to handle safearea instead
        var result = 55
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
            //print("iPhone X, XS")
                result = 80
            case 2688:
            //print("iPhone XS Max")
                result = 80
            case 1792:
                result = 80
            //print("iPhone XR")
            default: break
                //print("Unknown")
            }
        }
        
        self.leftViewLayoutConstraint.constant = CGFloat(result)
         */
    }
    
    func hideBackView() {
        self.cv.isHidden = false
        self.backButtonWrapper.isHidden = true
        self.backButton.setTitle("", for: .normal)
        //self.leftViewLayoutConstraint.constant = 0
    }
    
    func isBackButtonEnabled() -> Bool {
        return !self.backButtonWrapper.isHidden
    }
    
    func updateSelectedIDForSelectedCategory() {
        if let selectedCat = self.category {
            var index = 0
            for _category in self.categoryList {
                if selectedCat.id == _category.id {
                    self.selectedIndex = self.getScrollToIndex(currentRow: index)
                }
                index = index + 1
            }
        }
        //default value
    }
    
    func updateCollectionViewToSelectionBounds() {
        if let _selectedIndex = self.selectedIndex {
            self.cv.scrollToItem(at: IndexPath(item: _selectedIndex, section: 0), at: .left, animated: false)
            self.cv.reloadData()
        } else {
            //no selected index , get default one
            if self.categoryList.count > 0 {
                let scrollToIndex = self.getScrollToIndex(currentRow: 0)
                self.cv.scrollToItem(at: IndexPath(item: scrollToIndex, section: 0), at: .left, animated: false)
            }
        }
    }
    
    func getIndex(currentRow: Int) -> Int {
        return currentRow % categoryList.count
    }
    
    func getScrollToIndex(currentRow: Int) -> Int {
        var toIndex = 0
        
        if currentRow < categoryList.count && categoryList.count > 5 {
            //if first section , scroll to second
            toIndex = currentRow + categoryList.count
        } else {
            //if first section , scroll to second
            toIndex = currentRow
        }
        return toIndex
    }
    
    func setCategory(cat: Category) {
        self.category = cat
        self.updateSelectedIDForSelectedCategory()
        self.updateCollectionViewToSelectionBounds()
        self.selectedCategoryID.accept(cat.id)
    }
    
}

// MARK: - CollectionView
extension CategoryViewController {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //auto layout not working in this part , so calcualte and the cell size here.
        var size = CGSize()
        let dataSourceCount = categoryList.count > 5 ? maxValue : categoryList.count
        if dataSourceCount > 0 && indexPath.item < dataSourceCount {
            
            size = CGSize(width: categoryList[getIndex(currentRow: indexPath.item)].name.esimatedWidth(withConstrainedHeight: APPSetting.defaultCategoryCellHeight, font: APPSetting.font.montserratXS) + APPSetting.defaultCategoryCellReservedSpace,
                          height: APPSetting.defaultCategoryCellHeight)
            
            
            /* // The code below is to handle item width divided by view width
            if dataSourceCount <= 5 {
                let numberOfColumn: CGFloat = CGFloat(dataSourceCount)
                let safeAreaInset: CGFloat = collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
                let boundsWidth: CGFloat = (UIDevice.current.screenType == .iPhone_XSMax || UIDevice.current.screenType == .iPhone_XR) ? UIScreen.main.bounds.width : collectionView.frame.width
                size = CGSize(width: ((boundsWidth - CGFloat((numberOfColumn - 1) * 10) - safeAreaInset) / CGFloat(dataSourceCount)),
                              height: size.height)
            }
             */
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var numberOfItem = self.categoryList.count
        if numberOfItem > 5 {
            numberOfItem = maxValue
        }
        return numberOfItem
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.cv.dequeueReusableCell(withReuseIdentifier: "CategoryCell",
                                               for: indexPath) as! CategoryCell
        let dataSourceCount = categoryList.count > 5 ? maxValue : categoryList.count
        
        if dataSourceCount > 0 && indexPath.item < dataSourceCount {
        
            let _category: Category = self.categoryList[getIndex(currentRow: indexPath.item)]
            
            cell.configure(with: _category)
            
            
            if let _selectedCat = self.category {
                if _category.id == _selectedCat.id {
                    print("\(String(describing: type(of: self))): cell active \(_category.id)")
                    cell.setActive()
                } else {
                    cell.setInActive()
                }
            } else {
                cell.setInActive()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //set cell active if onclick , otherwise set inactive
        let category = self.categoryList[getIndex(currentRow: indexPath.item)]
        self.category = category
        self.selectedCategoryID.accept(category.id)
        self.cv.reloadData()
        
        self.selectedIndex = getScrollToIndex(currentRow: indexPath.item)
        if let selectedIndex = self.selectedIndex {
            print("\(String(describing: type(of: self))): select index " + String(selectedIndex))
            self.cv.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .left, animated: true)
        }
        self.completionBlock?(category)
    }
}
