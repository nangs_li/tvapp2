//
//  ChannelCell.swift
//  OONA
//
//  Created by Vick on 14/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation

class CategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lbl.layer.masksToBounds = true;
        self.lbl.layer.cornerRadius = APPSetting.defaultCornerRadius
    }
    
    func configure(with category: Category) {
        lbl.text = category.name
    }
    
    func setInActive(){
        //self.lbl.backgroundColor  = UIColor.clear
        self.lbl.textColor = UIColor.white
    }
    func setActive(){
        //self.lbl.backgroundColor  = UIColor.white
        self.lbl.textColor = UIColor.red
        
        //TODO_OONA: font bold
    }
}
