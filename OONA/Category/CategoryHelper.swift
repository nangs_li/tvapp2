//
//  CategoryHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON
//import PromiseKit

class CategoryHelper: NSObject {
    static let shared = CategoryHelper()
    var selectedCategoryID = Int()
    var currentCategoryType = "tv" //default value : tv
    //func getSelectedChannelId(){
        //return self.selectedChannelId
    //}
    
    func storeCurrentCateogry(){
        if(selectedCategoryID != nil && selectedCategoryID != 0){
            let userStandard = UserDefaults.standard
            userStandard.set(selectedCategoryID, forKey:"currentCategoryId")
        }else{
            clearCategoryFromStorage()
        }
    }
    
    func getCurrentCategoryFromStorage()->Category?{
        let currentChannelId = UserDefaults().integer(forKey: "currentCategoryId")
        if(currentChannelId==0){ //nil
            return nil
        }else{
            return Category.init(_id: currentChannelId)
        }
    }
    
    func getCurrentCategoryIdFromStorage()->Int?{
        let currentChannelId = UserDefaults().integer(forKey: "currentCategoryId")
        if(currentChannelId==0){ //nil
            return nil
        }else{
            return currentChannelId
        }
    }
    func clearCategoryFromStorage(){
        //UserDefaults.standard.set(episode.id, forKey:"currentEpisodeId")
        UserDefaults.standard.removeObject(forKey: "currentCategoryId")
        
    }
    
    func setCurrentCategory(_categoryId : Int){
        self.selectedCategoryID = _categoryId
        //let data  = NSKeyedArchiver.archivedData(withRootObject: channel)
        //let defaults = UserDefaults.standard
        let userStandard = UserDefaults.standard
        //print("save: " + channel.id.description)
        userStandard.set(_categoryId, forKey:"currentCategoryId")
    }
    
    func getCategoryList(type: String?, success: @escaping ([Category]) -> (), failure: @escaping ((Any) -> ())){
        let _type = type ?? ""
        OONAApi.shared.postRequest(url: APPURL.Category, parameter: ["type": _type]).responseJSON{ [weak self] (response) in
            
            switch response.result {
                
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string {
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { (message, isSuccess) in
                                                    if(isSuccess){
                                                        let xploreCategory: [Category] = JSON(json["categories"])
                                                            .oonaModelArray(action: { (category: Category) in
                                                                var newCategory = category
                                                                
                                                                if let categoryID = self?.selectedCategoryID {
                                                                    if categoryID == newCategory.id {
                                                                        newCategory.selected = true
                                                                    }
                                                                }
                                                                return newCategory
                                                            })
                                                        success(xploreCategory)
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                    }
                                                    
                    })
                }
                
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
    //TODO:
//    func requestFromCategory<T>(parameters: Dictionary<String, Any>) -> Promise<T> {
//
//        return Promise<T> { seal in
//            OONAApi.shared.postRequest(url: APPURL.Category, parameter: parameters).responseJSON{ [weak self] (response) in
//
//                switch response.result {
//
//                case .success(let value):
//                    let json = JSON(value)
//                    seal.fulfill(json)
//
//                case .failure(let error):
//                    print(error)
//                    seal.reject(error)
//                }
//            }
//
//        }
//    }
    
}

