//
//  Category.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct Category: OONAModel, Equatable { //or filter
    
    var id: Int = 0
    var name: String = ""
    var type : CategoryType = .category //TODO_OONA : actually category should seperate with filter.. but todo
    var selected: Bool = false
    
    init?(_id: Int){
        self.id = _id
    }
    
    init?(json: JSON) {
        if let _id = json["id"].int { self.id = _id }
        if let _name = json["name"].string { self.name = _name }
    }
    
    init(id: Int, name: String, _type: CategoryType = .category) {
        self.id = id
        self.name = name
        self.type = _type
    }
    
//    static func == (lhs: Category, rhs: Category) -> Bool {
//        return lhs.id == rhs.id
//    }
}
