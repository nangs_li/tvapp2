//
//  TCoinDetailViewModel.swift
//  OONA
//
//  Created by nicholas on 8/5/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation


struct TCoinDetailViewModel {
    var tCoinBalance: Int = 0
    var tCoinsExpires: Int = 0
    var tCoinsValidTimeLeft: Double = 0
    var earnedDailyTCoin: Int = 0
    var totalDailyTCoin: Int = 0
    var tcoinExpireMessage: String?
    var remainDailyTCoin: Int {
        let remain: Int = totalDailyTCoin - earnedDailyTCoin
        return remain
    }
    
    init(tCoinBalance: Int?,
         tCoinsExpires: Int?,
         tCoinsValidTimeLeft: Double?,
         earnedDailyTCoin: Int?,
         totalDailyTCoin: Int?,
         tCoinsExpireMsg: String?) {
        if let _balance = tCoinBalance { self.tCoinBalance = _balance }
        if let _tCoinsExpires = tCoinsExpires { self.tCoinsExpires = _tCoinsExpires }
        if let _tCoinsValidTimeLeft = tCoinsValidTimeLeft { self.tCoinsValidTimeLeft = _tCoinsValidTimeLeft }
        if let _earnedDailyTCoin = earnedDailyTCoin { self.earnedDailyTCoin = _earnedDailyTCoin }
        if let _totalDailyTCoin = totalDailyTCoin { self.totalDailyTCoin = _totalDailyTCoin }
        if let _tCoinsExpireMsg = tCoinsExpireMsg { self.tcoinExpireMessage = _tCoinsExpireMsg }
        
    }
    
}
