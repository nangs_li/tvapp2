//
//  SystemMessageViewController.swift
//  OONA
//
//  Created by Jack on 12/9/2019.
//  Copyright © 2019 OONA. All rights reserved.
//


import Foundation
import AVKit
import RxSwift
import RxCocoa
import RxLocalizer
import RxGesture

class SystemMessageViewController :BaseViewController {
    
    var addedTcoin : Int?
    var totalTcoin : Int?
    var message : String = ""
    var htmlMode : Bool = false
    @IBOutlet var lblCongras : UILabel!
    @IBOutlet var lblLeftTcoins : UILabel!
    @IBOutlet var lblRighttcoins : UILabel!
    @IBOutlet var tcoinImage : UIImageView!
    typealias tcoinsSelectBlock = (_ result :Any?) ->()
    var completionBlock:tcoinsSelectBlock?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //formatter the tcoin ammount to eg '2,000,00'
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        //tcoinImage.image = UIImage.gifImageWithName("tcoins-spinonly")
        
//        tcoinImage.animate(withGIFNamed: "tcoins-spinonly")
        
//        if let _addedTcoin = addedTcoin {
            //if let __addedTcoin = formatter.string(from: NSNumber(value: _addedTcoin)){
//            let addedTCoinValue = String(_addedTcoin)
            let toastString: String = message
            if (htmlMode) {
                self.lblCongras.set(html: message)
            }else{
                lblCongras.text = toastString
            }
    //            let mutableAttributedString: NSMutableAttributedString = NSMutableAttributedString(string: toastString)
        
//            // Get current attributes of label
//            if let attributedText = lblCongras.attributedText {
//                let existingAttributes = attributedText.attributes(at: 0, effectiveRange: nil)
//                mutableAttributedString.addAttributes(existingAttributes,
//                                                      range: NSRange(location: 0, length: toastString.count))
//            }
//
//            // Assign attribute to desired range of strings
//            if let rangeToColor: Range = toastString.range(of: addedTCoinValue) {
//                mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(rangeToColor, in: toastString))
//            }
//

//            lblCongras.attributedText = mutableAttributedString
            //}
//        }
//        if let _totalTcoin = totalTcoin{
//            lblLeftTcoins.text =  String(_totalTcoin)
//            //            lblRighttcoins.text = formatter.string(from: NSNumber(value: _totalTcoin))
//        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    public func setMessage(text:String) {
        htmlMode = false
        message = text
//        self.lblCongras.text = text 
    }
    public func htmlString(html:String) {
        htmlMode = true
        message = html
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func openWallet(_ sender: UIButton) {
        guard let cb = self.completionBlock else {return}
        cb(nil)
    }
    
}
