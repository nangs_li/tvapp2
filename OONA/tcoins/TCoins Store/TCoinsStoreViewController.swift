//
//  TCoinsStoreViewController.swift
//  OONA
//
//  Created by nicholas on 10/11/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class TCoinsStoreViewController: TCoinsStoreBaseViewController {
    /// UI properties
    @IBOutlet weak var headerRightView: UIView!
    @IBOutlet weak var searchField: UITextField!
    
    @IBOutlet weak var headerBalanceLabel: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
    @IBOutlet weak var bottomPopOverView: UIView!
    @IBOutlet weak var popOverTitle: UILabel!
    @IBOutlet weak var popOverSubTitle: UILabel!
    @IBOutlet weak var popOverBottomConstraintY: NSLayoutConstraint!
    
    var categoryViewController: OONACategoryViewController?
    
    @IBOutlet weak var itemCollectionView: UICollectionView!
    
    
    /// Data properties
    var itemList: [StoreItemForSaleDetailModel] = []
    
    var selectedCategoryID: Int = -1
    
    /// Network
    weak var currentItemListRequest: DataRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.configUIs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        if self.currentItemListRequest == nil {
            self.fetchGiftList(by: self.selectedCategoryID)
        }
    }
    
    @IBAction func walletButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func popOverTapAction(_ sender: Any) {
        print(#function)
    }
    
    @IBAction func leftButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func rightButtonAction(_ sender: Any) {
        let detailView: TCoinsStoreItemDetailViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "TCoinsStoreItemDetailViewController") as! TCoinsStoreItemDetailViewController
        PlayerHelper.shared.presentVC(vc: detailView)
    }
    
    @IBAction func backAction(_ sender: Any) {
        let vc: TCoinsDetailViewController = self.CreateTcoinsDetailPage()
        PlayerHelper.shared.presentVC(vc: vc)
    }
    
    override func appLanguageDidUpdate() {
        self.searchField.placeholder = LanguageHelper.localizedString("search_searchfield_placeholder")
        self.fetchCategoryList()
    }
    
    override func updateUserViews() {
        self.headerBalanceLabel.text = UtilityHelper.shared.formateredNumber(with: UserHelper.shared.currentUser.profile?.myPointBalance)
    }
}

// MARK: - Functional methods
extension TCoinsStoreViewController {
    func createCellViewModel(_ itemData: StoreItemForSaleDetailModel) -> StoreItemCellViewModel {
        let viewModel: StoreItemCellViewModel = StoreItemCellViewModel(imageURL: itemData.coverImageURL,
                                                                       itemName: itemData.itemName,
                                                                       itemDescription: itemData.itemDescription,
                                                                       itemValue: String(itemData.points))
        return viewModel
    }
    
    private func configUIs() {
        
        //TODO: Make clear button visible
//        self.headerRightView.layer.cornerRadius = 16
//        self.headerRightView.layer.masksToBounds = true
            
//        if let clearButton: UIButton = self.searchField.value(forKeyPath: "clearButton") as? UIButton {
//            let size: CGSize = clearButton.image(for: .highlighted) != nil ? clearButton.image(for: .highlighted)!.size : clearButton.frame.size
//            let whiteImage = UIImage(color: .white, size: size)
//            clearButton.setImage(whiteImage, for: .normal)
//            clearButton.setImage(whiteImage, for: .highlighted)
//            self.searchField.layoutSubviews()
//        }
        self.headerBalanceLabel.text = UtilityHelper.shared.formateredNumber(with: UserHelper.shared.currentUser.profile?.myPointBalance)
        self.setupCategoryView()
        self.setupCollectionView()
        self.setupPopOverView()
        }
    
    private func setupCategoryView() {
        let categoryVC: OONACategoryViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "OONACategoryViewController") as! OONACategoryViewController
        
        self.addChild(categoryVC)
        
        self.categoryView.addSubview(categoryVC.view)
        
        categoryVC.selectedCategoryID.subscribe(onNext: { [weak self] (selectedCategoryID) in
            self?.selectedCategoryID = selectedCategoryID
            self?.fetchGiftList(by: selectedCategoryID)
        }).disposed(by: self.disposeBag)
        
        categoryViewController = categoryVC
    }
    
    private func setupCollectionView() {
        self.itemCollectionView.delegate = self
        self.itemCollectionView.dataSource = self
        
        self.itemCollectionView.register(UINib.init(nibName: "TCoinsStoreItemCell", bundle: nil), forCellWithReuseIdentifier: "TCoinsStoreItemCell")
    }
    
    private func setupPopOverView() {
        // Simply set constant to zero to bring view up
        self.popOverBottomConstraintY.constant = UIScreen.main.bounds.size.height - (self.bottomPopOverView.frame.height + 21)
        self.bottomPopOverView.layer.cornerRadius = 8
        self.bottomPopOverView.layer.borderWidth = 1
        self.bottomPopOverView.layer.borderColor = UIColor.init(red: 20, green: 22, blue: 24).cgColor
        self.bottomPopOverView.isHidden = true
    }
}

// MARK: - UIs update
extension TCoinsStoreViewController {
    
    private func updateCategories(categories: [Category]) {
        self.categoryViewController?.categoryList = categories
    }
    
    private func updateGiftList(giftList: [StoreItemForSaleDetailModel]) {
        self.itemList = giftList
        self.itemCollectionView.reloadData()
    }
}

// MARK: - CollectionView delegates, datasource and layout
private let interitemSpacing: CGFloat = 0
private let lineSpacing: CGFloat = 10 //10
private let numberOfRows: CGFloat = 1
private let numberOfColumns: CGFloat = 3.5
extension TCoinsStoreViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TCoinsStoreItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TCoinsStoreItemCell",
                                                                                for: indexPath) as! TCoinsStoreItemCell
        if self.itemList.count > 0 && indexPath.item < self.itemList.count
        {
            let storeItem: StoreItemForSaleDetailModel = self.itemList[indexPath.item]
            cell.configureCell(newStoreItem: self.createCellViewModel(storeItem))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.itemList.count > 0 && indexPath.item < self.itemList.count
        {
            let storeItem: StoreItemForSaleDetailModel = self.itemList[indexPath.item]
            let itemDetailPage: TCoinsStoreItemDetailViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "TCoinsStoreItemDetailViewController") as! TCoinsStoreItemDetailViewController
            let requestItem: StoreRequestItem = StoreRequestItem(itemID: storeItem.itemID,
                                                                 itemType: .ForSale)
            itemDetailPage.requestItem = requestItem
//            PlayerHelper.shared.presentVC(vc: itemDetailPage)
            self.updateActiveChild(activeChild: itemDetailPage)
        }
    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = (viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
        //        let height = (viewHeight - (lineSpacing * numberOfRows)) / numberOfRows // This formular is make item in rows divided by view height
//        let height = (width / (16/9))//label height
        let height = viewHeight
        let size = CGSize(width: width, height: height)
        print("Item Cell size: \(size)")
        print("View height: \(viewHeight)")
        return size
//        return CGSize(width: width, height: height)
    }
}

// MARK: - Network requests
extension TCoinsStoreViewController {
    
    private func fetchCategoryList() {
            OONAApi.shared.getRequest(url: APPURL.tcoinsStoreCategoryList(), parameter: [:]).responseJSON { [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int, let status = json["status"].string
                    {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if isSuccess
                                                        {
                                                            var categories = JSON(json["categories"]).oonaModelArray() as [Category]
                                                            if categories.count > 0
                                                            {
                                                                categories.insert(Category(id: -1,
                                                                                           name: LanguageHelper.localizedString("all")),
                                                                                  at: 0)
                                                            }
                                                            self?.updateCategories(categories: categories)
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
    
    private func fetchGiftList(by categoryID: Int) {
        self.currentItemListRequest?.cancel()
//        self.currentItemListRequest = nil
        //Remove all current data
        self.updateGiftList(giftList: [])
        var parameters = [String: Any]()
        
        if categoryID > -1
        {
            parameters["categoryId"] = categoryID
        }
        
        let request = OONAApi.shared.postRequest(url: APPURL.tcoinsStoreItemList(),
                                                 parameter: parameters)
        self.currentItemListRequest = request
        
        request.responseJSON { [weak self] (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int, let status = json["status"].string
                {
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { (message, isSuccess) in
                                                    if isSuccess
                                                    {
                                                        let giftList = JSON(json["gifts"]).oonaModelArray() as [StoreItemForSaleDetailModel]
                                                        self?.updateGiftList(giftList: giftList)
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                    }
                                                    self?.currentItemListRequest = nil
                                                    
                    })
                }
            case .failure(let error):
                print(error)
                self?.currentItemListRequest = nil
                break
            }
        }
    }
}
