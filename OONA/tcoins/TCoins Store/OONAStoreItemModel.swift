//
//  TCoinsStoreItemModel.swift
//  OONA
//
//  Created by nicholas on 10/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

// Store item base model
class TCoinsStoreItemBaseModel: NSObject, OONAModel {
    var itemID: Int
    var itemName: String
    var coverImageURL: String
    var validUntil: String
    var expiresIn: TimeInterval
    var itemDescription: String?
    var terms: String?
    var howToUse: String?
    
    required init?(json: JSON) {
        self.itemID = json["id"].intValue
        self.itemName = json["name"].stringValue
        self.coverImageURL = json["imageUrl"].stringValue
        self.itemDescription = json["description"].string
        self.validUntil = json["validUntil"].stringValue
        self.expiresIn = json["expiresIn"].doubleValue
        self.terms = json["terms"].string
        self.howToUse = json["howToUse"].string
    }
}


// Gift item
class StoreItemForSaleModel: TCoinsStoreItemBaseModel {
    
    var points: Int
//    var overlayImageURL: String?
    var remainStock: Int
    var itemType: String
    var validForDays: Int
    
    required init?(json: JSON) {
        self.points = json["points"].intValue
//        self.overlayImageURL = json["overlayUrl"].string
        self.remainStock = json["stock"].intValue
        self.itemType = json["type"].stringValue
        self.validForDays = json["validForDays"].intValue
        super.init(json: json)
    }
}

// Voucher item
class StoreItemOwnedModel: TCoinsStoreItemBaseModel {
    
    var voucherCode: String
    
    required init?(json: JSON) {
        self.voucherCode = json["voucherCode"].stringValue
        super.init(json: json)
    }
}
