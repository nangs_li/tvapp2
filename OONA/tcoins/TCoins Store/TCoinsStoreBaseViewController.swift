//
//  TCoinsStoreBaseViewController.swift
//  OONA
//
//  Created by nicholas on 10/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class TCoinsStoreBaseViewController: BaseViewController {
    var activeChildVC: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func updateActiveChild(activeChild: UIViewController?) {
        // Detach last active vc
        if let _activeVC = self.activeChildVC {
            self.detachVCFromLeftMenu(vc: _activeVC)
        }
        
        // Attach new active vc
        if let vc = activeChild {
            self.attachVCToLeftMenu(vc: vc)
        }
    }
    
    func attachVCToLeftMenu(vc: UIViewController) {
        if !self.children.contains(vc) {
            self.addChild(vc)
        }
        vc.willMove(toParent: self)
        
        // TODO:
        let subview: UIView = vc.view
        if !self.view.subviews.contains(subview) {
            self.view.addSubview(subview)
            subview.frame = self.view.bounds
            subview.snp.makeConstraints { (make) in
                make.top.trailing.bottom.leading.equalToSuperview()
            }
        }
        vc.didMove(toParent: self)
        self.activeChildVC = vc
    }
    
    func detachVCFromLeftMenu(vc: UIViewController) {
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
        self.activeChildVC = nil
    }

}
