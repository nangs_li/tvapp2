//
//  TCoinsStoreItemDetailViewModel.swift
//  OONA
//
//  Created by nicholas on 10/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

// Default
class StoreItemDetailBaseViewModel: NSObject {
    var headerTitle: String?
    var tcoinsBalance: String
    
    var termAndConditionText: String?
    var howToUseText: String?
    
    var descriptionTitle: String?
    var itemDetailText: String?
    
    var previewImages: [String] = []
    var iconURLString: String = ""
    var providerTitle: String?
    var providerSubTitle: String?
    var shareTitle: String?
    var redeemTitle: String?
        
    override init() {
        self.tcoinsBalance = UtilityHelper.shared.formateredNumber(with: UserHelper.shared.currentUser.profile?.myPointBalance)
        self.termAndConditionText = LanguageHelper.localizedString("daily_bid_terms_and_conditions")
        self.howToUseText = LanguageHelper.localizedString("how_to_use")
        self.shareTitle = LanguageHelper.localizedString("right_menu_share").capitalizingFirstLetter()
        self.redeemTitle = LanguageHelper.localizedString("redeem").capitalizingFirstLetter()
    }
    
    convenience init(headerTitle: String = "",
                     itemDetailText: String?,
                     previewImages: [String] = [],
                     iconURLString: String = "",
                     providerTitle: String?,
                     providerSubTitle: String?) {
        self.init()
        
        self.headerTitle = headerTitle
        self.itemDetailText = itemDetailText
        self.previewImages = previewImages
        self.iconURLString = iconURLString
        self.providerTitle = providerTitle
        self.providerSubTitle = providerSubTitle
    }
}


// Voucher for sale
class ItemForSaleDetailViewModel: StoreItemDetailBaseViewModel {
    var itemValue: String?
    
    var redeemText: String?
    
    override init() {
        self.redeemText = LanguageHelper.localizedString("redeem")
    }
    
    convenience init(headerTitle: String = "",
                     itemDetailText: String?,
                     previewImages: [String] = [],
                     iconURLString: String = "",
                     providerTitle: String?,
                     providerSubTitle: String?,
                     itemValue: String) {
        
        self.init(headerTitle: headerTitle,
                  itemDetailText: itemDetailText,
                  previewImages: previewImages,
                  iconURLString: iconURLString,
                  providerTitle: providerTitle,
                  providerSubTitle: providerSubTitle)
//        self.redeemText = LanguageHelper.localizedString("redeem")
        self.itemValue = itemValue
    }
}

// Voucher already purchased
class ItemOwnedDetailViewModel: StoreItemDetailBaseViewModel {
    
    var voucherTitle: String?
    var voucherCode: String?
    
    var expireDate: String?
    
    override init() {
        self.voucherTitle = LanguageHelper.localizedString("tcoin_store_voucher_title")
    }
    
    convenience init(headerTitle: String = "",
                     itemDetailText: String?,
                     previewImages: [String] = [],
                     iconURLString: String = "",
                     providerTitle: String?,
                     providerSubTitle: String?,
                     voucherCode: String,
                     expireDate: String) {
        
        self.init(headerTitle: headerTitle,
                  itemDetailText: itemDetailText,
                  previewImages: previewImages,
                  iconURLString: iconURLString,
                  providerTitle: providerTitle,
                  providerSubTitle: providerSubTitle)
//        self.voucherTitle = LanguageHelper.localizedString("tcoin_store_voucher_title")
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMMM yyyy, HH:mm"
//        let date = dateFormatter.date(from: expireDate)
        
        self.expireDate = "\(LanguageHelper.localizedString("tcoin_store_item_detail_expire")) \(expireDate)"
    }
}

