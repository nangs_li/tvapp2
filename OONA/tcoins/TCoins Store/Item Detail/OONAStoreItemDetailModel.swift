//
//  TCoinsStoreItemDetailModel.swift
//  OONA
//
//  Created by nicholas on 10/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

// Gift item detail
class StoreItemForSaleDetailModel: StoreItemForSaleModel {
    var merchant: OONAMerchantModel?
    var isAllowRedeem: Bool = false
    var redeemedVouchers: Int
    
    required init?(json: JSON) {
        self.merchant = OONAMerchantModel(json: json["merchant"])
        self.isAllowRedeem = json["isAllowRedeem"].boolValue
        self.redeemedVouchers = json["redeemedVouchers"].intValue
        super.init(json: json)
    }
}

// Voucher item detail
class StoreItemOwnedDetailModel: StoreItemOwnedModel {
    var merchant: OONAMerchantModel?

    required init?(json: JSON) {
        super.init(json: json)
        self.merchant = OONAMerchantModel(json: json["merchant"])
    }
}
