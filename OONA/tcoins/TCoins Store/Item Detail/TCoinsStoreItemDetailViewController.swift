//
//  TCoinsStoreItemDetailViewController.swift
//  OONA
//
//  Created by nicholas on 10/17/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON
//import QuartzCore
import Kingfisher
import Alamofire

struct StoreRequestItem {
    enum StoreItemType {
        case ForSale
        case Owned
    }
    
    let itemID: Int
    let itemType: StoreItemType
    
    init(itemID: Int,
         itemType: StoreItemType) {
        self.itemID = itemID
        self.itemType = itemType
    }
}

class TCoinsStoreItemDetailViewController: TCoinsStoreBaseViewController {
    
    /// UI Properties
    @IBOutlet weak var headerRightView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerBalanceLabel: UILabel!
    
    @IBOutlet weak var termAndConditionButton: UIButton!
    @IBOutlet weak var howToUseButton: UIButton!
    
    // Voucher info
    @IBOutlet weak var voucherCodeContainer: UIView!
    @IBOutlet weak var voucherTitleLabel: UILabel!
    @IBOutlet weak var voucherCodeLabel: UILabel!
    
    // Item description
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var itemDetailTextView: UITextView!
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var providerSubLabel: UILabel!
    
    @IBOutlet weak var shareButton: GradientButton!
    @IBOutlet weak var itemValueLabel: UILabel!
    
    
    @IBOutlet weak var redeemContainer: UIView!
    @IBOutlet weak var gradientBackground: VerticalGradientButton!
    @IBOutlet weak var redeemLabel: UILabel!
    @IBOutlet weak var expireTimeLabel: UILabel!
    
    @IBOutlet weak var itemPreviewCollectionView: UICollectionView!
    
    @IBOutlet weak var descriptionViewTopConstraintY: NSLayoutConstraint!
    
    /// Data properties
    // Pass in from out side
    var requestItem: StoreRequestItem?
    
    // Fetch from network
    var storeItem: TCoinsStoreItemBaseModel?
    
    // Create from storeItem
    private var storeItemViewModel: StoreItemDetailBaseViewModel = StoreItemDetailBaseViewModel() {
        didSet {
            self.updatePreviewCollectionView()
        }
    }
    
    /// Network property
    weak var currentItemRedeemRequest: DataRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupCollectionView()
        self.setupUIs()
        self.loadItemDetail()
    }
    
    private func setupCollectionView() {
        self.itemPreviewCollectionView.delegate = self
        self.itemPreviewCollectionView.dataSource = self
        
        self.itemPreviewCollectionView.register(UINib.init(nibName: "ImagePreviewCell", bundle: nil),
                                                forCellWithReuseIdentifier: "ImagePreviewCell")
    }
    
    private func setupUIs() {
        
        self.shareButton.layer.cornerRadius = 4
        self.shareButton.layer.masksToBounds = true
        
        self.gradientBackground.setGradient(startGradientColor: .init(red: 64, green: 10, blue: 0),
                                            midGradientColor: .init(red: 84, green: 9, blue: 0),
                                            endGradientColor: .init(red: 89, green: 27, blue: 5))
    }
    
    private func loadItemDetail() {
        self.defaultUI()
        guard let _requestItem = self.requestItem else { return }
        switch _requestItem.itemType {
        case .ForSale:
            // Fetch item detail
            self.fetchItemDetail(itemID: _requestItem.itemID)
        case .Owned:
            // Fetch purchased item detail
            self.fetchOwnedItemDetail(voucherID: _requestItem.itemID)
        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        print(#function)
    }
    
    @IBAction func redeemButtonAction(_ sender: Any) {
        guard let _requestItem = self.requestItem,
            let _curremtDetailItem = self.storeItem as? StoreItemForSaleDetailModel else { return }
        
        if UserHelper.shared.isLogin() {
            var title: String = "One more step to redeem!"
            var message: String = "Please confirm you will redeem \(_curremtDetailItem.itemName) for \(_curremtDetailItem.points) tcoins"
            if _curremtDetailItem.redeemedVouchers > 0 {
                title = "You have this voucher in My Wallet!"
                message = "It's okay, you can have multiple vouchers, Do you still want to redeem \(_curremtDetailItem.itemName) voucher for \(_curremtDetailItem.points) tcoins"
            }
            
            let alertControlelr = UIAlertController(title: title,
                                                    message: message,
                                                    preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: LanguageHelper.localizedString("cancel"), style: .cancel, handler: nil)
            
            let confirmAction = UIAlertAction(title: LanguageHelper.localizedString("tcoin_store_item_redeem_confirm"), style: .default, handler: { [weak self] (alertAction) in
                self?.requestItemRedeem(itemID: _requestItem.itemID)
            })
            
            alertControlelr.addAction(cancelAction)
            alertControlelr.addAction(confirmAction)
            self.present(alertControlelr,
                         animated: true,
                         completion: nil)
        } else {
            self.showLogin { [weak self] in
//                self?.redeemButtonAction(nil)
            }
        }
        
    }
    
    @IBAction func showTNC(_ sender: Any) {
        let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerPage.title = LanguageHelper.localizedString("daily_bid_terms_and_conditions")
        disclaimerPage.customBackAction = { (vc) in
            vc?.removeFromParent()
            vc?.view.removeFromSuperview()
        }
        
        self.updateActiveChild(activeChild: disclaimerPage)
        disclaimerPage.disclaimerPage.view.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.45) {  [weak weakPage = disclaimerPage] in
            weakPage?.disclaimerPage.textToDisplay.accept(self.storeItem?.terms)
            weakPage?.disclaimerPage.view.isHidden = false
        }
    }
    
    @IBAction func showHowToUse(_ sender: Any) {
        let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "DisclaimerViewController") as! DisclaimerViewController
        disclaimerPage.title = LanguageHelper.localizedString("how_to_use")
        disclaimerPage.customBackAction = { (vc) in
            vc?.removeFromParent()
            vc?.view.removeFromSuperview()
        }
        
        self.updateActiveChild(activeChild: disclaimerPage)
        disclaimerPage.disclaimerPage.view.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.45) { [weak weakPage = disclaimerPage] in
            weakPage?.disclaimerPage.textToDisplay.accept(self.storeItem?.howToUse)
            weakPage?.disclaimerPage.view.isHidden = false
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
    override func appLanguageDidUpdate() {
        self.loadItemDetail()
    }
    
    override func updateUserViews() {
        
        if let itemDetail = self.storeItem
        {
            let _itemViewModel = self.createItemViewModel(itemDetail)
            print("Tcoins Store User balance: \(_itemViewModel.tcoinsBalance)")
            self.updatePage(_itemViewModel)
        }
    }
}

// MARK: - Functional methods
extension TCoinsStoreItemDetailViewController {
    
    private func defaultUI() {
        self.redeemContainer.isHidden = true
        self.expireTimeLabel.isHidden = true
        
        self.updatePage(StoreItemDetailBaseViewModel())
    }
    
    private func updatePreviewCollectionView() {
        self.itemPreviewCollectionView.reloadData()
        self.itemPreviewCollectionView.isScrollEnabled = self.storeItemViewModel.previewImages.count > 0
        self.itemPreviewCollectionView.bounces = self.storeItemViewModel.previewImages.count > 0
    }
    
    private func updatePage(_ detailViewModel: StoreItemDetailBaseViewModel) {
        
        self.headerTitleLabel.text = detailViewModel.headerTitle
        self.headerBalanceLabel.text = detailViewModel.tcoinsBalance
        self.termAndConditionButton.setTitle(detailViewModel.termAndConditionText, for: .normal)
        self.howToUseButton.setTitle(detailViewModel.howToUseText, for: .normal)
        self.descriptionLabel.text = detailViewModel.descriptionTitle
        self.itemDetailTextView.text = detailViewModel.itemDetailText
        self.providerLabel.text = detailViewModel.providerTitle
        self.providerSubLabel.text = detailViewModel.providerSubTitle
        self.shareButton.setTitle(detailViewModel.shareTitle, for: .normal)
        
        // Set image for provider iconView
        let encodedURLString: String = self.storeItemViewModel.iconURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? detailViewModel.iconURLString
        let url = URL(string: encodedURLString)
        print("set item image for: \(detailViewModel.providerTitle)")
        self.iconImageView.kf.indicatorType = .activity
        self.iconImageView.kf.setImage(
            with: url,
            placeholder: nil,//UIImage(named: "placeholder-bidding"),
            options: [
                .processor(RoundCornerImageProcessor(cornerRadius: 7)),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage])
        { (result) in
            switch result {
            case .success(let success):
                print("download item image for: \(detailViewModel.providerTitle), success.")
            //                print(success)
            case .failure(let error):
                print("download item image for: \(detailViewModel.providerTitle), failure.")
                //                print(error)
            }
        }
        
        
        switch self.requestItem?.itemType {
        case .ForSale:
            if let forSaleDetailModel = detailViewModel as? ItemForSaleDetailViewModel {
                self.updatePageForSale(forSaleDetailModel)
            }
        case .Owned:
            if let ownedDetailModel = detailViewModel as? ItemOwnedDetailViewModel {
                self.updatePageForOwned(ownedDetailModel)
            }
        default: break
        }
    }
    
    // Will only called by updatePage:
    private func updatePageForSale(_ detailViewModel: ItemForSaleDetailViewModel) {
        self.descriptionViewTopConstraintY.constant = -145
        self.redeemContainer.isHidden = false
        self.expireTimeLabel.isHidden = true
        self.itemValueLabel.text = detailViewModel.itemValue
        self.redeemLabel.text = detailViewModel.redeemTitle
    }
    
    // Will only called by updatePage:
    private func updatePageForOwned(_ detailViewModel: ItemOwnedDetailViewModel) {
        self.descriptionViewTopConstraintY.constant = 0
        self.redeemContainer.isHidden = true
        self.expireTimeLabel.isHidden = false
        self.voucherTitleLabel.text = detailViewModel.voucherTitle
        self.voucherCodeLabel.text = detailViewModel.voucherCode
        self.expireTimeLabel.text = detailViewModel.expireDate
    }
    
    private func showRedeemCompletionAction() {
        UserHelper.shared.getUserProfile(success: { (userProfile) in
            
        }) { (code, errorMessage) in
            
        }
        
        let alertControlelr = UIAlertController(title: "Redeem success!",
                                                message: "You can check voucher in My Wallet!",
                                                preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: LanguageHelper.localizedString("ok"), style: .default, handler: { [weak self] (alertAction) in
            self?.backAction(self)
        })
        
//        alertControlelr.addAction(cancelAction)
        alertControlelr.addAction(confirmAction)
        self.present(alertControlelr,
                     animated: true,
                     completion: nil)
    }
    
    private func createItemViewModel(_ itemDetail: TCoinsStoreItemBaseModel) -> StoreItemDetailBaseViewModel {
        var detailViewModel: StoreItemDetailBaseViewModel = StoreItemDetailBaseViewModel()
        if let _requestItem = self.requestItem
        {
            switch _requestItem.itemType
            {
            case .ForSale:
                if let _itemDetail: StoreItemForSaleDetailModel = itemDetail as? StoreItemForSaleDetailModel
                {
                    print("Creating view model ForSale")
                    detailViewModel =
                        ItemForSaleDetailViewModel(headerTitle: _itemDetail.itemName ?? "",
                                                   itemDetailText: _itemDetail.itemDescription ?? "",
                                                   previewImages: [_itemDetail.coverImageURL],
                                                   iconURLString: _itemDetail.merchant?.imageURL ?? "",
                                                   providerTitle: _itemDetail.merchant?.name,
                                                   providerSubTitle: nil,
                                                   itemValue: _itemDetail.points.description)
                }
            case .Owned:
                if let _itemDetail: StoreItemOwnedDetailModel = itemDetail as? StoreItemOwnedDetailModel
                {
                    print("Creating view model Owned")
                    detailViewModel =
                        ItemOwnedDetailViewModel(headerTitle: _itemDetail.itemName ?? "",
                                                 itemDetailText: _itemDetail.itemDescription ?? "",
                                                 previewImages: [_itemDetail.coverImageURL],
                                                 iconURLString: _itemDetail.merchant?.imageURL ?? "",
                                                 providerTitle: _itemDetail.merchant?.name,
                                                 providerSubTitle: nil,
                                                 voucherCode: _itemDetail.voucherCode,
                                                 expireDate: _itemDetail.validUntil)
                }
            }
        }
        return detailViewModel
    }
}


// MARK: - CollectionView delegates, datasource and layout
private let interitemSpacing: CGFloat = 0
private let lineSpacing: CGFloat = 5
private let numberOfRows: CGFloat = 1
private let numberOfColumns: CGFloat = 1
extension TCoinsStoreItemDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.storeItemViewModel.previewImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ImagePreviewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewCell",
                                                                        for: indexPath) as! ImagePreviewCell
        
        if self.storeItemViewModel.previewImages.count > 0 && indexPath.item < self.storeItemViewModel.previewImages.count
        {
            let imageURLString: String = self.storeItemViewModel.previewImages[indexPath.item]
            cell.updateImage(with: imageURLString)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.storeItemViewModel.previewImages.count > 0 && indexPath.item < self.storeItemViewModel.previewImages.count
        {
            let imageURLString: String = self.storeItemViewModel.previewImages[indexPath.item]
            let previewPage: OONALargeImagePreviewViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "OONALargeImagePreviewViewController") as! OONALargeImagePreviewViewController
            previewPage.imageURLString = imageURLString
            previewPage.descriptionText = self.storeItemViewModel.headerTitle
            previewPage.modalPresentationStyle = .overCurrentContext
            previewPage.modalTransitionStyle = .crossDissolve
            self.present(previewPage, animated: true, completion: nil)
        }
    }
    
    // CollectionView flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth: CGFloat = collectionView.frame.width
        let viewHeight: CGFloat = collectionView.bounds.height
        let width = viewWidth//(viewWidth - (interitemSpacing * (numberOfColumns + 1))) / numberOfColumns
        let height = viewHeight
        return CGSize(width: width, height: height)
    }
}

// MARK: - Network requests
extension TCoinsStoreItemDetailViewController {
    private func fetchItemDetail(itemID: Int) {
        OONAApi.shared.getRequest(url: APPURL.tcoinStoreItemDetail(itemID), parameter: [:]).responseJSON { [weak self] (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string
                {
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { (message, isSuccess) in
                                                    if isSuccess
                                                    {
                                                        if let itemDetail: StoreItemForSaleDetailModel = StoreItemForSaleDetailModel(json: json),
                                                            let _itemViewModel = self?.createItemViewModel(itemDetail)
                                                        {
                                                            self?.storeItemViewModel = _itemViewModel
                                                            self?.storeItem = itemDetail
                                                            self?.updatePage(_itemViewModel)
                                                        }
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                    }
                                                    
                    })
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func fetchOwnedItemDetail(voucherID: Int) {
        OONAApi.shared.getRequest(url: APPURL.tcoinStorePurchasedVoucherDetail(voucherID), parameter: [:]).responseJSON { [weak self] (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if let code = json["code"].int,
                    let status = json["status"].string
                {
                    APIResponseCode.checkResponse(withCode: code.description,
                                                  withStatus: status,
                                                  completion: { (message, isSuccess) in
                                                    if isSuccess
                                                    {
                                                        if let itemDetail: StoreItemOwnedDetailModel = StoreItemOwnedDetailModel(json: json),
                                                            let _itemViewModel = self?.createItemViewModel(itemDetail)
                                                        {
                                                            self?.storeItemViewModel = _itemViewModel
                                                            self?.storeItem = itemDetail
                                                            self?.updatePage(_itemViewModel)
                                                        }
                                                    } else {
                                                        print("Failure on check response with message: \(message ?? "unknown error")")
                                                    }
                                                    
                    })
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func requestItemRedeem(itemID: Int) {
        if self.currentItemRedeemRequest == nil {
            let postRequest = OONAApi.shared.postRequest(url: APPURL.tcoinsStoreRedeemItem(), parameter: ["giftId" : itemID])
            
            postRequest.responseJSON { [weak self] (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string
                    {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if isSuccess
                                                        {
//                                                            if let itemDetail: StoreItemOwnedDetailModel = StoreItemOwnedDetailModel(json: json), let _storeItem = self?.createStoreItem(itemDetail)
//                                                            {
//                                                                self?.storeItemViewModel = _itemViewModel
//                                                                self?.storeItem = itemDetail
//                                                                self?.updatePage(_itemViewModel)
//                                                            }
                                                            self?.showRedeemCompletionAction()
                                                        } else {
                                                            print("Failure on check response with message: \(message ?? "unknown error")")
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                }
            }
            self.currentItemRedeemRequest = postRequest
        }
    }
}
