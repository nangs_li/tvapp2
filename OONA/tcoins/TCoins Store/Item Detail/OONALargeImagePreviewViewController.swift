//
//  OONALargeImagePreviewViewController.swift
//  OONA
//
//  Created by nicholas on 10/22/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Kingfisher

class OONALargeImagePreviewViewController: TCoinsStoreBaseViewController {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var imageURLString: String? = ""
    var descriptionText: String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateUI(with: self.imageURLString ?? "",
                      self.descriptionText)
        self.previewImageView.layer.cornerRadius = 10
        
    }
    
    func updateUI(with urlString: String,
                  _ descriptionText: String?) {
        self.descriptionLabel.text = self.descriptionText
        
            let encodedURLString: String = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? urlString
            let url = URL(string: encodedURLString)
            print("set item image for: \(urlString)")
            self.previewImageView.kf.indicatorType = .activity
            self.previewImageView.kf.setImage(
                with: url,
                placeholder: nil,//UIImage(named: "placeholder-bidding"),
                options: [
                    .processor(RoundCornerImageProcessor(cornerRadius: 10)),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage])
            { (result) in
                switch result {
                case .success(let success):
                    print("download item image for: \(urlString), success.")
    //                print(success)
                case .failure(let error):
                    print("download item image for: \(urlString), failure.")
    //                print(error)
                }
            }
        }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
