//
//  ImagePreviewCell.swift
//  OONA
//
//  Created by nicholas on 10/18/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Kingfisher

class ImagePreviewCell: UICollectionViewCell {
    var imageURLString: String = ""
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = .white
    }
    
    func updateImage(with urlString: String) {
        let encodedURLString: String = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? urlString
        let url = URL(string: encodedURLString)
        print("set item image for: \(urlString)")
        self.previewImageView.kf.indicatorType = .activity
        self.previewImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder-bidding"),
            options: [
//                .processor(RoundCornerImageProcessor(cornerRadius: 4.5)),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage])
        { (result) in
            switch result {
            case .success(let success):
                print("download item image for: \(urlString), success.")
//                print(success)
            case .failure(let error):
                print("download item image for: \(urlString), failure.")
//                print(error)
            }
        }
        
        self.imageURLString = urlString
    }
}
