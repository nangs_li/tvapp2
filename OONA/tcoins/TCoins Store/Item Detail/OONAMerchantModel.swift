//
//  OONAMerchantModel.swift
//  OONA
//
//  Created by nicholas on 10/21/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SwiftyJSON

class OONAMerchantModel: NSObject, OONAModel {
    var id: Int
    var name: String?
    var imageURL: String?
    
    required init?(json: JSON) {
        if let _id = json["id"].int
        {
            self.id = _id
            self.name = json["name"].string
            self.imageURL = json["imageUrl"].string
        } else
        {
            return nil
        }
    }
}
