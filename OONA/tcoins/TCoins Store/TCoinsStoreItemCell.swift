//
//  TCoinsStoreItemCell.swift
//  OONA
//
//  Created by nicholas on 10/17/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import Kingfisher

class StoreItemCellViewModel: NSObject {
    var previewImageURL: String = ""
    var itemName: String = "-"
    var itemDescription: String?
    var itemValue: String = "-"
    
    convenience init(imageURL: String,
                     itemName: String?,
                     itemDescription: String?,
                     itemValue: String?) {
        self.init()
        self.previewImageURL = imageURL
        self.itemName = itemName ?? "-"
        self.itemDescription = itemDescription
        self.itemValue = itemValue ?? "-"
    }
}

class TCoinsStoreItemCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemValueLabel: UILabel!
    
    
    var storeItem: StoreItemCellViewModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(newStoreItem: StoreItemCellViewModel) {
        let encodedURLString: String = newStoreItem.previewImageURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? newStoreItem.previewImageURL
        let url = URL(string: encodedURLString)
        print("set item image for: \(newStoreItem.itemName)")
        self.previewImageView.kf.indicatorType = .activity
        self.previewImageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholder-bidding"),
            options: [
                .processor(RoundCornerImageProcessor(cornerRadius: 4)),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage])
        { (result) in
            switch result {
            case .success(let success):
                print("download item image for: \(newStoreItem.itemName), success.")
//                print(success)
            case .failure(let error):
                print("download item image for: \(newStoreItem.itemName), failure.")
//                print(error)
            }
        }
        
        self.itemLabel.text = newStoreItem.itemName
        self.itemDescriptionLabel.text = newStoreItem.itemDescription
        self.itemValueLabel.text = newStoreItem.itemValue
        
        self.storeItem = newStoreItem
    }
    
}
