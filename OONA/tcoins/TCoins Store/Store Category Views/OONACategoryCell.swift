//
//  OONACategoryCell.swift
//  OONA
//
//  Created by nicholas on 10/16/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit

class OONACategoryCell: UICollectionViewCell {
    
    var category: Category? {
        didSet {
            self.configure(with: category)
        }
    }
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var underline: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func configure(with category: Category?) {
        self.title.text = category?.name
//        self.category = category
    }
    
    func updateCellSelection(selection: Bool) {
        self.underline.isHidden = !selection
    }
}
