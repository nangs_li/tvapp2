//
//  BidItemCell.swift
//  OONA
//
//  Created by Jack on 4/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//



import UIKit
import SDWebImage

class BidItemCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var availabilityBadge: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func click() {
        // print("clicked")
    }
    
    func configure(with bidItem: BidItem) {
        self.backgroundColor = UIColor.clear
        previewImageView?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        print("bid item")
        print(bidItem.imageUrl)
        print(bidItem.name)
        if let url = URL.init(string: bidItem.imageUrl){
            self.previewImageView?.sd_setImage(with: url,
                                               placeholderImage: UIImage(named: "placeholder-bidding"))
        }
        
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        let openDateFormat = DateFormatter()
        openDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var dateText = ""
        let bidOpenDate = openDateFormat.date(from: bidItem.opensAt)
        
        if (bidOpenDate == nil) {
            
            dateText = ""
        }else{
            
            let userCalendar = Calendar.current
            
            //            dateFormatter.dateFormat = "dd/MM/yy hh:mm:ss"
            let nowDate = DeviceHelper.shared.serverTime
            
            let humanDateFormat = DateFormatter()
            humanDateFormat.dateFormat = "MMM dd"
            
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: nowDate, to: bidOpenDate!)
            
            
            
  
            
            if timeDifference.day! < 1{
                dateText = ""
            }else{
                dateText = ("" + humanDateFormat.string(from: bidOpenDate!))
            }
        
            
        }
        descriptionLabel?.text = dateText
    }
    
    func updateCellSelection(selection: Bool) {
        
    }
    /*
     func setInActive(){
     self.underline.isHidden = true
     }
     
     func setActive(){
     self.underline.isHidden = false
     }*/
    
}
