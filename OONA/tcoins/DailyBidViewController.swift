//
//  DailyBidViewController.swift
//  OONA
//
//  Created by nicholas on 7/8/19.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class DailyBidViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var hasLoad = false
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemList.count
    }
    
    @IBOutlet weak var collectionViewTrailing: NSLayoutConstraint!
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BidItemCell", for: indexPath) as! BidItemCell
//        if (cell == nil) {
//            cell = UINib(nibName: "BidItemCell", bundle: nil).instantiate(withOwner: self, options: nil)
//        }
        cell.backgroundColor = UIColor.darkGray
        cell.configure(with: self.itemList[indexPath.row])
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
        self.selectBidItem(tag: indexPath.row)
//        self.itemList[indexPath.row]
    }
    var currentItemIndex : Int = 0
    @IBOutlet weak var opensAtView: UIView!
    @IBOutlet weak var opensAtLabel: UILabel!
    
    @IBOutlet weak var availabilityBadge: UIImageView!
    @IBOutlet weak var bidAndWinLabel: UILabel!
    @IBOutlet weak var termAndConditionButton: UIButton!
    @IBOutlet weak var howDoesItWorkButton: UIButton!
    
    @IBOutlet weak var biddingNotAvailableLabel: UILabel!
    
    @IBOutlet weak var bidWinnerButton: UIButton!
    @IBAction func bidWinnerAction(_ sender: Any) {
        
    }
    let changeFeedback = UISelectionFeedbackGenerator()
    
    let dateFormatter: DateFormatter = { let dfm = DateFormatter()
        dfm.dateFormat = "dd MMM"
        return dfm
    }()
    
    @IBOutlet weak var noBidView: UIView!
    func selectBidItem(tag:Int) {
        
        self.availabilityBadge.isHidden = true
        var sameItem = false
        if (currentItemIndex == tag) {
            sameItem = true
        }
        if (!sameItem) {
        AnalyticHelper.shared.logEventToAllPlatform(name: "bidding_item_close", parameters: ["bid_id":self.itemList[currentItemIndex].itemId])
        }
        currentItemIndex = tag
        
        self.mainBidItemLabel.text = self.itemList[tag].name
        if (!sameItem) {
        AnalyticHelper.shared.logEventToAllPlatform(name: "bidding_item_open", parameters: ["bid_id":self.itemList[currentItemIndex].itemId])
        }
        
        let url : URL = URL.init(string:  self.itemList[tag].imageUrl)!
        self.mainBidItemImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder-bidding"))
        let startDate = DeviceHelper.shared.serverTime.addingTimeInterval(TimeInterval(self.itemList[tag].expiresIn))
        
        defaultString = "\(LanguageHelper.localizedString("bid_availability_message_1")) \(dateFormatter.string(from: startDate)) \(LanguageHelper.localizedString("bid_good_luck"))!"
        self.bidTextLabel.text = defaultString
        if (self.itemList[tag].alreadyBid) {
            //TODO: Localization
//            LanguageHelper.localizedString("bid_placed_message")
            
            self.bidTextLabel.text = "You have successfully placed a bid on this item. \nGood luck."
            self.bidAmountField.text = self.itemList[tag].bidAmount?.description
            self.formatBidAmountField()
            self.bidAmountField.isUserInteractionEnabled = false
            self.amountSlider.isHidden = true
            self.bidNowButton.isHidden = true
            self.bidAddButton.isHidden = true
            self.bidMinisButton.isHidden = true
        }else{
            self.bidAmountField.isUserInteractionEnabled = true
            self.amountSlider.isHidden = false
            self.bidNowButton.isHidden = false
            self.bidAddButton.isHidden = false
            self.bidMinisButton.isHidden = false
        }
        if (self.itemList[tag].opensAt == "") {
            self.opensAtView.isHidden = true
            self.availabilityBadge.isHidden = false
            
        }else{
            let opensAtStr = self.itemList[tag].opensAt
            self.opensAtView.isHidden = false
            
            let requestedComponent: Set<Calendar.Component> = [.month,
                                                               .day,
                                                               .hour,
                                                               .minute,
                                                               .second]
            
            let openDateFormat = DateFormatter()
            openDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let bidOpenDate = openDateFormat.date(from: opensAtStr)
            let userCalendar = Calendar.current
            
//            dateFormatter.dateFormat = "dd/MM/yy hh:mm:ss"
            let nowDate = DeviceHelper.shared.serverTime
            
            let humanDateFormat = DateFormatter()
            humanDateFormat.dateFormat = "MMM dd"
            
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: nowDate, to: bidOpenDate!)
            
            var timeText = " \(LanguageHelper.localizedString("tomorrow"))"
            
            print("Bid item remain time to open: \(timeDifference.day)")
            
            if timeDifference.day! < 0 {
                // tommorow
                self.availabilityBadge.isHidden = false
            } else if timeDifference.day! < 1 {
                // tommorow
                timeText = " \(LanguageHelper.localizedString("tomorrow"))"
            }else{
                timeText = (" \(humanDateFormat.string(from: bidOpenDate!))")
            }
            
            
            //TODO: Localization
//            LanguageHelper.localizedString("bid_item_not_available_now_1")
            //            LanguageHelper.localizedString("bid_item_not_available_now_2")
            // This BID open on (date), win tcoins and pls BID on open date. Good luck!
            var string = (LanguageHelper.localizedString("bid_item_not_available_now_1") + timeText + LanguageHelper.localizedString("bid_item_not_available_now_2") )
            if let notice = self.itemList[tag].notice {
                string = notice
            }
            self.opensAtLabel.text = string
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (collectionView.frame.size.width - 50), height: ((collectionView.frame.size.width - 50) * 9 / 16) )
    }
    @IBOutlet weak var userTcoinLabel: UILabel!
    @IBOutlet weak var mainBidItemInfo: UILabel!{
        didSet{
            mainBidItemInfo.text = ""
        }
    }
    @IBOutlet weak var mainBidItemLabel: UILabel!
    @IBOutlet weak var mainBidItemImage: UIImageView!
    @IBOutlet weak var bidTextLabel: UILabel!
    var defaultString = LanguageHelper.localizedString("bid_high_msg")
    @IBOutlet weak var amountSlider: UISlider!{
        didSet {
            amountSlider.setThumbImage(UIImage(named:"bid_slider"), for: .normal)
            amountSlider.setThumbImage(UIImage(named:"bid_slider"), for: .highlighted)
//            amountSlider.tintColor = UIColor.init(rgb: 0xd8d8d8)
            
        }
    }
    override func updateUserViews() {
        var balance: String = ""
        if let tcoinBalance = UserHelper.shared.currentUser.profile?.myPointBalance {
            balance = UtilityHelper.shared.formateredNumber(with: Int(tcoinBalance))
        }
        self.userTcoinLabel.text = balance
        userTcoin = UserHelper.shared.currentUser.profile?.myPointBalance ?? 0
    }
    @IBOutlet weak var bidAmountField: UITextField!{
        didSet {
            bidAmountField.keyboardType = .numberPad
        }
    }
    @IBOutlet weak var bidNowButton: UIButton!{
        didSet {
            bidNowButton.layer.cornerRadius = 5.0
            bidNowButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var bidItemCollectionView: UICollectionView!
    
    @IBAction func bidNow(_ sender: Any) {
        var amountString: NSString = ""
        
        if let _amount: NSString = self.bidAmountField.text as NSString?,
            _amount.trimmingCharacters(in: .whitespaces).count > 0
        {
            amountString = _amount.replacingOccurrences(of: ",", with: "") as NSString
            
            let pointToBid = amountString.integerValue
            if pointToBid > 0 {
                ptToBid = pointToBid
//                self.placeBid(tag: currentItemIndex, points: pointToBid)
                self.showContactInputView()
            }
        }
    }
    var ptToBid : Int = 0
    @IBAction func bidMinus(_ sender: Any) {
        
        if userTcoin > 0 {
            let trySliderValue: Float = amountSlider.value - 0.02
            let tryBaseValue: Float = amountSlider.value - 1/Float(userTcoin)
            changeFeedback.selectionChanged()
//            let convertedTcoin: Float = Float(userTcoin)
//            print("updated slider value: \(trySliderValue)")
//            print("updated base value: \(tryBaseValue)")
//            print("result tcoin by slider: \(trySliderValue * convertedTcoin)")
//            print("result tcoin by base: \(tryBaseValue * convertedTcoin)")
//            print("final slider value: \(max(0, min(trySliderValue, tryBaseValue)))")
            amountSlider.value = max(0, min(trySliderValue, tryBaseValue))
            self.updateBidAmount()
        }
    }
    
    @IBAction func bidAdd(_ sender: Any) {
        
        if userTcoin > 0 {
            let trySliderValue: Float = amountSlider.value + 0.02
            let tryBaseValue: Float = amountSlider.value + 1/Float(userTcoin)
            changeFeedback.selectionChanged()
//            let convertedTcoin: Float = Float(userTcoin)
//            print("updated slider value: \(trySliderValue)")
//            print("updated base value: \(tryBaseValue)")
//            print("result tcoin by slider: \(trySliderValue * convertedTcoin)")
//            print("result tcoin by base: \(tryBaseValue * convertedTcoin)")
//            print("final slider value: \(min(1, max(trySliderValue, tryBaseValue)))")
            amountSlider.value = min(1, max(trySliderValue, tryBaseValue))
            self.updateBidAmount()
        }
    }
    @IBAction func showWinner(_ sender: Any) {
        
        let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        disclaimerPage.privacyURL = URL(string: APPURL.biddingWinner())
        disclaimerPage.modalTransitionStyle = .crossDissolve
        disclaimerPage.title = LanguageHelper.localizedString("daily_winner")
        self.present(disclaimerPage, animated: true, completion: nil)
    }
    @IBAction func showTNC(_ sender: Any) {
        
        let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        disclaimerPage.privacyURL = URL(string: APPURL.biddingTermsAndConditions())
        disclaimerPage.modalTransitionStyle = .crossDissolve
        disclaimerPage.title = LanguageHelper.localizedString("daily_bid_terms_and_conditions")
        self.present(disclaimerPage, animated: true, completion: nil)
    }
    @IBAction func showTutorial(_ sender: Any) {
//        self.playTutorial(tutorial: .tcoinBid)
        let disclaimerPage = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        disclaimerPage.privacyURL = URL(string: APPURL.howDoesItWork())
        disclaimerPage.modalTransitionStyle = .crossDissolve
        disclaimerPage.title = LanguageHelper.localizedString("how_to_use")

        self.present(disclaimerPage, animated: true, completion: nil)
    }
    
    
    
    @IBOutlet weak var bidMinisButton: UIButton!
    @IBOutlet weak var bidAddButton: UIButton!
    @IBOutlet weak var bidInputView: UIView! {
        didSet {
            bidInputView.layer.cornerRadius = 3.0
            bidInputView.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
            bidInputView.layer.borderWidth = 2.0
        }
    }
    
    var userTcoin : Int = UserHelper.shared.currentUser.profile?.myPointBalance ?? 0
    var itemList : [BidItem] = []
    @IBAction func backAction(_ sender: Any) {
        let vc : TCoinsDetailViewController = self.CreateTcoinsDetailPage()
        PlayerHelper.shared.presentVC(vc: vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(!hasLoad){
            
            self.bidItemCollectionView.register(UINib(nibName: "BidItemCell", bundle: nil), forCellWithReuseIdentifier: "BidItemCell")
            
            self.bidItemCollectionView.delegate = self
            self.bidItemCollectionView.dataSource = self
            
            self.fetchItemList()
            self.bidAmountField.text = "0"
            amountSlider.value = 0
            amountSlider.rx.controlEvent([.valueChanged])
                .subscribeOn(MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .asObservable()
                .subscribe(onNext: { _ in
                    self.updateBidAmount()
                }).disposed(by: disposeBag)
            bidAmountField.rx.controlEvent([.editingChanged])
                .subscribeOn(MainScheduler.instance)
                .observeOn(MainScheduler.instance)
                .asObservable()
                .subscribe(onNext: { _ in
                    defer { self.formatBidAmountField() }
                    self.updateBidSlider()
                }).disposed(by: disposeBag)
            
            switch UIDevice.current.screenType {
            case .iPhone_XR :
                fallthrough
            case .iPhones_X_XS :
                fallthrough
            case .iPhone_XSMax :
                print("Gg")
            default:
                self.setupIphoneXlayouts()
            }
            hasLoad = true
        }
    }
    
    
    func placeBid(tag:Int,points:Int,name:String,phone:String) {
        let itemToBid = self.itemList[tag]
        itemToBid.print()
        print("itemToBid",itemToBid)
        let param =  ["itemId":itemToBid.itemId.description,
                      "points":points,
                      "name":name,
                      "phone":phone ] as [String : Any]
        
        OONAApi.shared.postRequest(url: APPURL.PlaceBid, parameter:param)
            .responseJSON { (response) in
                if(response.result.isSuccess){
                    if let json = (try? JSON(data: response.data!)) as? JSON {
                        let code : Int? = json["code"].int
                        let status : String? = json["status"].string
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                print("response.result",json)
                               
                                    self.refreshUserDatas()
                                    self.fetchItemList()
                                //TODO: Localization
//                                LanguageHelper.localizedString("bid_placed_message")
                                    self.showMessage(message: "You have successfully placed a bid on this item. \nGood Luck.")
//                                    self.itemList = []
//                                    for _item in bidItems {
//                                        print("bid json",_item)
//                                        self.itemList.append(BidItem.init(json: _item))
//
//
//                                    }
//                                    print("bid item list",self.itemList.count)
//                                    self.bidItemCollectionView.reloadData()
//                                    self.selectBidItem(tag: 0)
                                
                                
                                
                            }
                            
                            
                            
                        })
                        
                    }
                }
        }
    }
    func setupIphoneXlayouts() {
        print ("UIDevice.current.hasNotch")
        self.collectionViewTrailing.constant = 20
        self.view.layoutIfNeeded()
    }
    func fetchItemList() {
        print("fetchItemList")
        OONAApi.shared.postRequest(url: APPURL.BidItemList, parameter: [:])
            .responseJSON { (response) in
                if(response.result.isSuccess){
                    if let json = (try? JSON(data: response.data!)) as? JSON {
                        let code : Int? = json["code"].int
                        let status : String? = json["status"].string
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                print("response.result",json)
                                if let bidItems : [JSON] = json["items"].array {
                                    
                                    if (bidItems.count > 0) {
                                        print("bidItems  > 0")
                                        self.noBidView.isHidden = true
                                    }else{
                                        print("bidItems  <= 0")
                                        self.noBidView.isHidden = false
                                    }
                                    
                                    self.itemList = []
                                    
                                    bidItems.forEach { (json) in
                                        print("bid json",json)
                                        let item = BidItem(json: json)
                                        if item.opensAt == "" {
                                            self.itemList.append(item)
                                        }
                                    }
                                    
                                    
                                    print("bid item list",self.itemList.count)
                                    self.bidItemCollectionView.reloadData()
                                    if self.itemList.count > 0 {
                                        self.selectBidItem(tag: self.currentItemIndex)
                                    }
                                }
                                
                            }
                        })
                        
                    }
                }else{
//                    if (self.itemList.count >= 0) {
//                        self.noBidView.isHidden = false
//                    }else{
                        self.noBidView.isHidden = false
//                    }
                }
        }
    }
    func showMessage(message:String) {
        //  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
        
            let popup = CustomPopup(message: message)
            //    CustomPopup *popup = [[CustomPopup alloc] initWithTextFieldTitle:message AndMessage:message];
            //    popup.button2.hidden = YES;
        popup?.button1.setTitle(LanguageHelper.localizedString("ok"), for: .normal)
            //    CGRect buttonFrame = popup.button1.frame;
            //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
            //    popup.button1.frame = buttonFrame;
            let popupBlock = popup
            //    popup.backBtn. hidden  = YES;
        popup?.setupOneButton()
        popup?.completionButton1 = {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    popupBlock?.dismiss()
                    
                })
            }
        popup?.alpha = 0
        let window = UIApplication.shared.keyWindow!
        window.addSubview(popup!) // Add your view here
        
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            popup?.alpha = 1
        })
        

    }
    func showContactInputView() {
        //TODO: Localization
        let controller = UIAlertController(title: "Contact Information", message: "Please enter your contact information to let us to inform you if you win", preferredStyle: .alert)
        controller.addTextField { (textField) in
            textField.placeholder = "Name"
            
            textField.text = (UserHelper.shared.currentUserProfile()?.firstName ?? "") + " " + (UserHelper.shared.currentUserProfile()?.lastName ?? "")
        }
        controller.addTextField { (textField) in
            textField.placeholder = "Phone number"
            textField.keyboardType = UIKeyboardType.phonePad
            textField.text = UserHelper.shared.currentUserProfile()?.phone
//            textField.isSecureTextEntry = true
            
        }
        let okAction = UIAlertAction(title: LanguageHelper.localizedString("confirm_text"), style: .default) { (_) in
            let name = controller.textFields?[0].text
            let phone = controller.textFields?[1].text
            
            print(phone, name)
            
            self.placeBid(tag: self.currentItemIndex, points: self.ptToBid,name: name ?? "demoName",phone: phone ?? "12345678")
            
        }
        controller.addAction(okAction)
        let cancelAction = UIAlertAction(title: LanguageHelper.localizedString("cancel"), style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }
    func updateBidAmount() {
        let bidAmount : Int = Int(self.amountSlider.value * Float(self.userTcoin))
        self.bidAmountField.text = "\(bidAmount)"
        self.formatBidAmountField()
    }
    
    func formatBidAmountField() {
        
        if let _text = self.bidAmountField.text,
            _text.trimmingCharacters(in: .whitespaces).count > 0 {
            let numberString = _text.replacingOccurrences(of: ",", with: "")
            let updatedBalance = UtilityHelper.shared.formateredNumber(with: Int(numberString))
            self.bidAmountField.text = updatedBalance
        }
    }
    
    func updateBidSlider() {
        var bidAmount: Float = 0
        if let _amount = self.bidAmountField.text {
            let amountString: NSString = _amount.replacingOccurrences(of: ",", with: "") as NSString
            bidAmount = (amountString as NSString).floatValue
            if Int(bidAmount) > userTcoin {
                self.bidAmountField.text = "\(userTcoin)"
                bidAmount = Float(userTcoin)
            }
        }
        self.amountSlider.value = (bidAmount / Float(userTcoin))
    }
    
//    private func formateredNumber(with number: Int?) -> String {
//        var convertedNumber: String = ""
//        if let _number = number {
//            let numberFormatter = NumberFormatter()
//            numberFormatter.groupingSeparator = ","
//            numberFormatter.numberStyle = .decimal
//            convertedNumber = numberFormatter.string(for: _number) ?? String(_number)
////            print(convertedNumber as Any)
//        }
//        return convertedNumber
//    }
    
    override func appLanguageDidUpdate() {
        self.bidAndWinLabel.text = LanguageHelper.localizedString("bid_n_win")
        self.termAndConditionButton.setTitle(LanguageHelper.localizedString("daily_bid_terms_and_conditions"), for: .normal)
        self.howDoesItWorkButton.setTitle(LanguageHelper.localizedString("how_to_use"), for: .normal)
        
        self.biddingNotAvailableLabel.text = LanguageHelper.localizedString("sorry_no_bid_available")
        self.bidWinnerButton.setTitle(LanguageHelper.localizedString("daily_winner"), for: .normal)
        self.defaultString = LanguageHelper.localizedString("bid_high_msg")
        self.bidTextLabel.text = self.defaultString
        if self.itemList.count > 0 {
            self.selectBidItem(tag: self.currentItemIndex)
        }
    }
}
