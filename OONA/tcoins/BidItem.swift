//
//  BidItem.swift
//  OONA
//
//  Created by Jack on 5/8/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit



class BidItem {
    
    let keyID = "id"
    let keyName = "name"
    let keyImageURL = "imageUrl"
    let keyOpensAt = "opensAt"
    let keyMyBid = "myBid"
    let keyNotice = "noticeKey"
    let keyExpiresIn = "noticeKey"
    
    var itemId: Int
    var name: String
    var imageUrl: String
    var opensAt: String
    var expiresIn: Double
    var alreadyBid : Bool = false
    var openToBid : Bool = true
    var bidAmount : Int?
    var notice: String?

    init(json:JSON){
        itemId = json[keyID].int ?? -1
        name = json[keyName].string ?? ""
        imageUrl = json[keyImageURL].string ?? ""
        opensAt = json[keyOpensAt].string ?? ""
        notice = json[keyNotice].string
        expiresIn = json[keyExpiresIn].double ?? 0
        
        if let myBidJSON = json[keyMyBid].dictionary {
            alreadyBid = true
            bidAmount = myBidJSON["points"]?.int
        }
        if let _ = json[keyOpensAt].string {
            alreadyBid = false
            
        }
    }
    func print(){
        Swift.print("name",self.name)
        Swift.print("itemid",self.itemId)
        Swift.print("imageUrl",self.imageUrl)
        Swift.print("opensAt",self.opensAt)
        Swift.print("alreadyBid",self.alreadyBid)
        
    }
    
}

