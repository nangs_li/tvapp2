    //
    //  TcoinsDetailViewController.swift
    //  OONA
    //
    //  Created by nicholas on 7/8/19.
    //  Copyright © 2019 OONA. All rights reserved.
    //

    import UIKit
    import UICircularProgressRing
    import RxSwift
    import RxCocoa

    enum TCoinDisplayingState {
        case tcoinsHidden
        case displayingTabBar
        case displayingDetail
    }



    class TCoinsDetailViewController: BaseViewController {
        var hasLoad = false
        var currentTCoinState: TCoinDisplayingState { get { return tcoinState } }
        private var tcoinState: TCoinDisplayingState = .tcoinsHidden
        var tcoinsFirstItem: DispatchWorkItem?
        var tcoinsSecondItem: DispatchWorkItem?
        var tcoinDetailViewModel: BehaviorRelay<TCoinDetailViewModel?> = BehaviorRelay<TCoinDetailViewModel?>(value: nil)
        
        @IBOutlet weak var tcoinsMainContentView: UIView!
        @IBOutlet weak var tcoinsTabView: UIView!
        
        /// TCoins Tab
        // UI elements
        @IBOutlet weak var tcoinGif: UIImageView!
        @IBOutlet weak var tcoinsRewardTabTitleLabel: UILabel!
        @IBOutlet weak var tcoinTabBarValueLabel: UILabel!
        @IBOutlet weak var tcoinTabBarEarnedDailyRewardsLabel: UILabel!
        @IBOutlet weak var tcoinTabBarTotalDailyRewardsLabel: UILabel!
        @IBOutlet weak var tcoinTabBarRemainDailyRewardsLabel: UILabel!
        @IBOutlet weak var bitButtonFortcoinView: UIButton! {
            didSet {
                if (DeviceHelper.shared.isEssentialUiOnly) {
                    bitButtonFortcoinView.isHidden = true
                }else {
                bitButtonFortcoinView.imageView?.sd_setImage(with: URL.init(string: DeviceHelper.shared.bidThumbnailImageUrl), placeholderImage: UIImage.init(named: "biddingtab"))
                
                }
            }
        }
        // Constraints
        @IBOutlet weak var tcoinTabBarDailytcoinsBarConstraint: NSLayoutConstraint!
        
        
        /// TCoins Detail
        // Left Column
        @IBOutlet weak var tcoinsCircleView: UICircularProgressRing!
        @IBOutlet weak var tcoinsRewardTitleLabel: UILabel!
        @IBOutlet weak var tcoinsValueLabel: UILabel!
        @IBOutlet weak var expiresDescOfTcoinsLabel: UILabel!
        @IBOutlet weak var expiringValueOfTcoinsLabel: UILabel!
        @IBOutlet weak var expiringTimeOfTcoinsLabel: UILabel!
        @IBOutlet weak var dailytcoinsFromLabel: UILabel!
        @IBOutlet weak var dailytcoinsToLabel: UILabel!
        @IBOutlet weak var remainDailyRewardsLabel: UILabel!
        @IBOutlet weak var tcoinDailytcoinBarSuperView: UIView!
        
        
        // Right Column
        @IBOutlet weak var dailyBidContainer: UIView! {
            didSet {
                //            configLargeImageView(view: &dailyBidContainer,
                //                                      borderColor: UIColor(red: 214, green: 23, blue: 0))
            }
        }
        @IBOutlet weak var tcoinsStoreContainer: UIView! {
            didSet {
                //            configLargeImageView(view: &tcoinsStoreContainer,
                //                                      borderColor: UIColor(red: 10, green: 90, blue: 144))
            }
        }
        @IBOutlet weak var dailyBidImageView: UIImageView!{
            didSet {
                //            dailyBidImageView.sd_setImage(with: URL.init(string: DeviceHelper.shared.bidImageUrl), completed: nil)
                //            dailyBidImageView!.sd_setImage(with: URL.init(string: DeviceHelper.shared.bidImageUrl), placeholderImage: UIImage.init(named: "bidding_bg_1"), options: nil, context: nil)
                if (DeviceHelper.shared.isEssentialUiOnly) {
                    dailyBidImageView.image = UIImage.init(named: "video_unlock")
                }else{
                    dailyBidImageView.sd_setImage(with: URL.init(string: DeviceHelper.shared.bidImageUrl), placeholderImage: UIImage.init(named: "bidding_bg_1"))
                }
            }
        }
        @IBOutlet weak var tcoinsStoreImageView: UIImageView!{
            didSet {
                if (DeviceHelper.shared.isEssentialUiOnly) {
                    tcoinsStoreImageView.image = UIImage.init(named: "game_unlock")
                }else{
                       if (DeviceHelper.shared.isEssentialUiOnly) {
                                       tcoinsStoreImageView.image = UIImage.init(named: "game_unlock")
                                   }else{
                                        tcoinsStoreImageView.sd_setImage(with: URL.init(string: DeviceHelper.shared.storeImageUrl), placeholderImage: UIImage.init(named: "tcoins_store_bg"))
                                   }
                }
            }
        }
        
        // Constraints
        @IBOutlet weak var mainContenViewLeadingSafeArea: NSLayoutConstraint!
        @IBOutlet weak var mainContentViewTrailingSafeArea: NSLayoutConstraint!
        
        @IBOutlet weak var leftColumnWidthConstraint: NSLayoutConstraint!{
            didSet {
                if UIDevice.current.screenType == .iPhones_6_6s_7_8 {
                    self.leftColumnWidthConstraint.constant = 120
                }
            }
        }
        @IBOutlet weak var tcoinDailytcoinsBarConstraint: NSLayoutConstraint!
        @IBOutlet weak var tcoinsStoreContainerWidthConstraint: NSLayoutConstraint!
        @IBOutlet weak var tcoinsDailyBidContainerTrailingConstraint: NSLayoutConstraint!
        
        deinit {
            //        print("Deallocating tcoins Detail Page")
        }
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
        }
        var watchButton : UIButton = UIButton()
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            //         dailyBidImageView.sd_setImage(with: URL.init(string: DeviceHelper.shared.bidImageUrl), placeholderImage: UIImage.init(named: "bidding_bg_1"), options: nil, completed: nil)
            if(!hasLoad){
                
                // Do any additional setup after loading the view.
                updateMainContentViewConstraintsForDeviceOrientation()
                tcoinGif.animate(withGIFNamed: "tcoins-spinonly-1time")
                // TODO:
                
                
                self.tcoinDetailViewModel.subscribe(onNext: { [weak self] (tcoinViewModel) in
                    if let _tvm = tcoinViewModel {
                        self?.updateTCoinTabView(with: _tvm)
                        self?.updateTCoinDetailView(with: _tvm)
                    }
                }).disposed(by: self.disposeBag)
                hasLoad = true
                
                
                
                watchButton.sd_setImage(with: URL.init(string: DeviceHelper.shared.watchButtonJson["imageUrl"].stringValue), for: .normal, completed: nil)
                watchButton.rx.tap
                    .subscribe(onNext: { [weak self] in
                        PlayerHelper.shared.showTutorialVideo.accept(.tcoinBid)
                    })
                    .disposed(by: disposeBag)
                
                //                {.tcoinBid
                if (!DeviceHelper.shared.isEssentialUiOnly) {
                    dailyBidContainer.addSubview(watchButton)
                }
                
                
            }
        }
        func resizeWatchButton() {
            let imageWidth = dailyBidImageView.frame.width
            let imageHeight = dailyBidImageView.frame.height
            
            let originX : CGFloat = CGFloat(DeviceHelper.shared.watchButtonJson["originX"].floatValue) * CGFloat(imageWidth)
            let originY : CGFloat = CGFloat(DeviceHelper.shared.watchButtonJson["originY"].floatValue) * CGFloat(imageHeight)
            let width : CGFloat = CGFloat(DeviceHelper.shared.watchButtonJson["width"].floatValue) * CGFloat(imageWidth)
            let height : CGFloat = CGFloat(DeviceHelper.shared.watchButtonJson["height"].floatValue) * CGFloat(imageHeight)
            watchButton.frame = CGRect.init(x: originX,
                                            y: originY,
                                            width: width,
                                            height: height)
            //        print("watchbutton",originX,originY,width,height)
        }
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            if (!DeviceHelper.shared.isEssentialUiOnly) {
                self.tcoinsStoreContainerWidthConstraint.constant =
                    (self.tcoinsMainContentView.frame.width - self.dailyBidContainer.frame.origin.x - 10) / 2
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                self.resizeWatchButton()
            }
            
        }
        override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            resizeWatchButton()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
                self.resizeWatchButton()
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            configTCoinsCircleRing(circleView: &tcoinsCircleView)
            
        }
        
        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            print("tcoins Detail Page did close")
        }
        
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            updateMainContentViewConstraintsForDeviceOrientation()
            super.viewWillTransition(to: size, with: coordinator)
        }
        
        override func updateUserViews() {
            if let userProfile = UserHelper.shared.currentUser.profile {
                self.updateTCoinInfo(for: userProfile)
            }
        }
        
        /// Open Daily Bid page
        @IBAction func dailyBidButtonClicked(_ sender: UIButton) {
            if DeviceHelper.shared.isEssentialUiOnly{
                //            self.unlockVideo()
                let message = "Please choose a video to unlock."
                        let popup = CustomPopup(message: message)

                          popup?.button1.setTitle("OK", for: .normal)
                          
                            //    CGRect buttonFrame = popup.button1.frame;
                            //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
                            //    popup.button1.frame = buttonFrame;
                            let popupBlock = popup
                            //    popup.backBtn. hidden  = YES;
                            popup?.setupOneButton()
                            popup?.completionButton1 = {
                                popupBlock?.dismiss()
                                PlayerHelper.shared.showXplore.accept(true)
                                
                            }
                      
                            popup?.alpha = 0
                            let window = UIApplication.shared.keyWindow!
                            window.addSubview(popup!) // Add your view here
                            
                            
                            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                                popup?.alpha = 1
                            })
                
                
                
            }else{
                self.goToBid()
            }
        }
        
        /// Open tcoins Store page
        @IBAction func tcoinsStoreButtonClicked(_ sender: UIButton) {
            //        let storyboard = UIStoryboard(name: "Catalog", bundle: nil)
            //        let vc = storyboard.instantiateViewController(withIdentifier: "CatalogListViewController")
            //        self.navigationController?.pushViewController(vc, animated: true)
            if DeviceHelper.shared.isEssentialUiOnly{
                PlayerHelper.shared.showGames.accept(true)
            }else{
                self.goToStore()
                // go to store
            }
            
        }
        func unlockVideo() {
            let message = "Are you sure to use 100 tcoins to unlock a video?"
            let popup = CustomPopup(message: message)
            
            popup?.button1.setTitle("Yes", for: .normal)
            popup?.button2.setTitle("No", for: .normal)
            //    CGRect buttonFrame = popup.button1.frame;
            //    buttonFrame.origin.x = (popup.popupView.frame.size.width/2)-(buttonFrame.size.width/2);
            //    popup.button1.frame = buttonFrame;
            let popupBlock = popup
            //    popup.backBtn. hidden  = YES;
            popup?.setupTwoButton()
            popup?.completionButton1 = {
                
                popupBlock?.dismiss()
                PlayerHelper.shared.showXplore.accept(true)
                
            }
            popup?.completionButton2 = {
                
                popupBlock?.dismiss()
                
                
            }
            popup?.alpha = 0
            let window = UIApplication.shared.keyWindow!
            window.addSubview(popup!) // Add your view here
            
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
                popup?.alpha = 1
            })
        }
        func unlockGames() {
            
        }
        func goToBid() {
            let vc: UIViewController = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "DailyBidViewController")
            
            // CreateTcoinsDetailPage
            PlayerHelper.shared.presentVC(vc: vc)
        }
        func goToStore() {
            let vc = UIStoryboard(name: "tcoins", bundle: nil).instantiateViewController(withIdentifier: "TCoinsStoreViewController")
            
            PlayerHelper.shared.presentVC(vc: vc)
        }
        override func appLanguageDidUpdate() {
            self.tcoinsRewardTitleLabel.text = LanguageHelper.localizedString("tcoins_Reward")
            self.tcoinsRewardTabTitleLabel.text = LanguageHelper.localizedString("tcoins_Reward")
            self.expiresDescOfTcoinsLabel.text = LanguageHelper.localizedString("points_expires")
            
            if let _tvm = self.tcoinDetailViewModel.value {
                self.updateTCoinTabView(with: _tvm)
                self.updateTCoinDetailView(with: _tvm)
            }
        }
    }


    // MARK: - View animations
    extension TCoinsDetailViewController {
        func animateDailyBar(from earned: Int,
                             to owned: Int,
                             for displayType: TCoinDisplayingState) {
            switch displayType {
            case .displayingTabBar:
                self.prepareForAnimation(for: displayType)
                
                tcoinsFirstItem = DispatchWorkItem { [weak self] in
                    self?.tcoinGif.animationRepeatCount = 1
                    self?.tcoinGif.animationDuration = 2
                    self?.tcoinGif.animate(withGIFNamed: "tcoins-spinonly-1time")
                    //after one more second , the coin gif will end and dailytcoins bar will animate
                }
                
                tcoinsSecondItem = DispatchWorkItem { [weak self] in
                    if let _ = self?.tcoinGif.isAnimating {
                        print("stoping gif animation")
                        self?.tcoinGif.stopAnimatingGIF()
                    }
                    
                    UIView.animate(withDuration: 0.7) {
                        //161 is tcoins view width
                        let barWidth: CGFloat = CGFloat(earned) / CGFloat(owned) * 161
                        self?.tcoinTabBarDailytcoinsBarConstraint.constant = barWidth
                        self?.view.layoutIfNeeded()
                        //reset the bar to original position.
                    }
                }
                DispatchQueue.main.async(execute: tcoinsFirstItem!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: tcoinsSecondItem!)
                
            case .displayingDetail:
                self.prepareForAnimation(for: displayType)
                
                tcoinsFirstItem = DispatchWorkItem { [weak self] in
                    self?.playValueChangeAnimation()
                }
                
                ///TODO: Bug!! frame width may not correct
                let referenceWidth: CGFloat = self.tcoinDailytcoinBarSuperView.frame.width
                tcoinsSecondItem = DispatchWorkItem { [weak self] in
                    if let _ = self?.tcoinGif.isAnimating { self?.tcoinGif.stopAnimatingGIF() }
                    
                    UIView.animate(withDuration: 0.7) {
                        
                        ///TODO: Bug!! frame width may not correct
                        let barWidth: CGFloat = CGFloat(earned) / CGFloat(owned) * referenceWidth
                        self?.tcoinDailytcoinsBarConstraint.constant = barWidth
                        self?.view.layoutIfNeeded()
                        //reset the bar to original position.
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: tcoinsFirstItem!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: tcoinsSecondItem!)
            default: print("\(#function): Nothing to do")
            }
        }
        
        func playValueChangeAnimation() {
            if let tcoinModel = self.tcoinDetailViewModel.value {
                print("playing tcoinsCircleView animation")
                tcoinsCircleView.startProgress(to: CGFloat(tcoinModel.tCoinBalance - tcoinModel.tCoinsExpires),
                                               duration: 1)
            }
        }
        
        func prepareForAnimation(for displayType: TCoinDisplayingState) {
            if displayType == .displayingTabBar || displayType == .displayingDetail {
                switch displayType {
                case .displayingTabBar:
                    // Reset daily bar in tab bar
                    if (self.tcoinTabBarDailytcoinsBarConstraint.constant != 0) {
                        self.tcoinTabBarDailytcoinsBarConstraint.constant = 0
                        self.view.layoutIfNeeded()
                    }
                    
                    if self.tcoinGif.isAnimating { self.tcoinGif.stopAnimatingGIF() }
                    
                case .displayingDetail:
                    // Reset daily bar in detail
                    if (self.tcoinDailytcoinsBarConstraint.constant != 0) {
                        self.tcoinDailytcoinsBarConstraint.constant = 0
                        self.view.layoutIfNeeded()
                    }
                    print("reseting tcoinsCircleView value")
                    tcoinsCircleView.resetProgress()
                default:
                    print("\(#function) nothing to do")
                }
                
                
                if let _item = tcoinsFirstItem {
                    if !_item.isCancelled {
                        _item.cancel()
                        tcoinsFirstItem = nil
                    }
                }
                
                if let _item = tcoinsSecondItem {
                    if !_item.isCancelled {
                        _item.cancel()
                        tcoinsSecondItem = nil
                    }
                }
            }
        }
    }

    extension TCoinsDetailViewController {
        
        /// Configure UI for circular progress ring
        ///
        /// - Parameter circleView: Pass the circular view to be configurated.
        private func configTCoinsCircleRing(circleView: inout UICircularProgressRing) {
            circleView.shouldShowValueText = false
            circleView.style = .ontop
            circleView.innerRingColor = UIColor(red: 128, green: 222, blue: 234)
            var outerRingColor = UIColor(red: 211, green: 211, blue: 211).withAlphaComponent(0.33)
            // empty 211 211 211
            if let item = self.tcoinDetailViewModel.value, item.tCoinBalance > 0 {
                outerRingColor = UIColor(red: 239, green: 104, blue: 23)
            }
            circleView.outerRingColor = outerRingColor
            
        }
        
        /// Update MainContentView leading and trailing constraints for device rotated
        private func updateMainContentViewConstraintsForDeviceOrientation() {
            if (UIDevice.current.hasNotch) {
                if UIDevice.current.orientation == .landscapeRight {
                    print("Orientation changed to Right")
                    mainContenViewLeadingSafeArea.constant = 10
                    mainContentViewTrailingSafeArea.constant = 44
                } else {
                    print("Orientation changed to Left")
                    mainContenViewLeadingSafeArea.constant = 10
                    //                mainContentViewTrailingSafeArea.constant = 10
                }
                view.layoutIfNeeded()
            }
        }
        
        private func configLargeImageView(view: inout UIView, borderColor: UIColor) {
            view.layer.cornerRadius = 4.2
            view.layer.borderWidth = 2
            view.layer.borderColor = borderColor.cgColor
        }
        
        
        // Call this for display half
        func showTCoinTabBar() {
            tcoinsTabView.alpha = 1
            tcoinsMainContentView.alpha = 0
            tcoinState = .displayingTabBar
            //        if currentTCoinState == .tcoinsHidden {
            
            if let _currentTcoin = self.tcoinDetailViewModel.value {
                self.animateDailyBar(from: _currentTcoin.earnedDailyTCoin,
                                     to: _currentTcoin.totalDailyTCoin,
                                     for: tcoinState)
            }
            //        }
            //        tcoinState = .displayingTabBar
            print("Showing TCoins Tab Bar")
        }
        
        // Call this to display fully expanded
        func showTCoinDetailView() {
            tcoinsTabView.alpha = 0
            tcoinsMainContentView.alpha = 1
            tcoinState = .displayingDetail
            if let _currentTcoin = self.tcoinDetailViewModel.value {
                self.animateDailyBar(from: _currentTcoin.earnedDailyTCoin,
                                     to: _currentTcoin.totalDailyTCoin,
                                     for: tcoinState)
            }
            print("Showing TCoins Detail")
        }
        
        func resetTCoinView(){
            //reset the bar to original position.
            print("reset tcoin view")
            self.tcoinTabBarDailytcoinsBarConstraint.constant = 0
            self.tcoinDailytcoinsBarConstraint.constant = 0
            self.view.layoutIfNeeded()
            if self.tcoinGif.isAnimating { self.tcoinGif.stopAnimatingGIF() }
            if let _tcoinsItem = tcoinsFirstItem {
                _tcoinsItem.cancel()
                tcoinsFirstItem = nil
            }
            if let _tcoinsItem = tcoinsSecondItem {
                _tcoinsItem.cancel()
                tcoinsSecondItem = nil
            }
            tcoinsTabView.alpha = 0
            tcoinsMainContentView.alpha = 0
            tcoinState = .tcoinsHidden
            //        tcoinsCircleView.value = 0
            print("reseting tcoinsCircleView value")
            tcoinsCircleView.resetProgress()
        }
        
        func transitTCoinTabBar(toAlpha alpha: CGFloat) {
            self.tcoinsTabView.alpha = alpha
        }
        
        func transitTCoinDetailView(toAlpha alpha: CGFloat) {
            self.tcoinsMainContentView.alpha = alpha
        }
        
        func updateTCoinInfo(for userProfile: UserProfile) {
            let _tcoinDetailModel = TCoinDetailViewModel(tCoinBalance: userProfile.myPointBalance,
                                                         tCoinsExpires: userProfile.pointsExpireSoon,
                                                         tCoinsValidTimeLeft: userProfile.pointExpireTime,
                                                         earnedDailyTCoin: userProfile.earnedDailyPoint,
                                                         totalDailyTCoin: userProfile.totalDailyPoint,
                                                         tCoinsExpireMsg: userProfile.pointExpireMessage)
            //        let _tcoinDetailModel = TCoinDetailViewModel(tCoinBalance: 38000,
            //                                                     tCoinsExpires: 8000,
            //                                                     tCoinsValidTimeLeft: 43200,
            //                                                     earnedDailyTCoin: 7000,
            //                                                     totalDailyTCoin: 7000)
            self.tcoinDetailViewModel.accept(_tcoinDetailModel)
        }
        
        func updateTCoinTabView(with tcoinModel: TCoinDetailViewModel) -> Void {
            
            self.tcoinTabBarValueLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.tCoinBalance)
            self.tcoinTabBarEarnedDailyRewardsLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.earnedDailyTCoin)
            self.tcoinTabBarTotalDailyRewardsLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.totalDailyTCoin)
            
            self.tcoinTabBarRemainDailyRewardsLabel.text = "\(UtilityHelper.shared.formateredNumber(with: tcoinModel.remainDailyTCoin)) \(LanguageHelper.localizedString("more_for_Daily_Rewards"))"
        }
        
        func updateTCoinDetailView(with tcoinModel: TCoinDetailViewModel) -> Void {
            self.tcoinsValueLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.tCoinBalance)
            self.expiringValueOfTcoinsLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.tCoinsExpires)
            self.expiringTimeOfTcoinsLabel.text = self.remainTimeToExpire(sec: tcoinModel.tCoinsValidTimeLeft)
            self.dailytcoinsFromLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.earnedDailyTCoin)
            self.dailytcoinsToLabel.text = UtilityHelper.shared.formateredNumber(with: tcoinModel.totalDailyTCoin)
            self.remainDailyRewardsLabel.text = "\(UtilityHelper.shared.formateredNumber(with: tcoinModel.remainDailyTCoin)) \(LanguageHelper.localizedString("more_for_Daily_Rewards"))"
            self.tcoinsCircleView.maxValue = CGFloat(tcoinModel.tCoinBalance)
            
            if let _msg = tcoinModel.tcoinExpireMessage {
                self.expiringTimeOfTcoinsLabel.text = _msg
            }
            print("expiringTimeOfTcoinsLabel",tcoinModel.tcoinExpireMessage)
            let outerRingColor = (tcoinModel.tCoinBalance > 0) ? UIColor(red: 239, green: 104, blue: 23) : UIColor(red: 211, green: 211, blue: 211).withAlphaComponent(0.33)
            self.tcoinsCircleView.outerRingColor = outerRingColor
            //        self.tcoinsCircleView.layoutIfNeeded()
            self.playValueChangeAnimation()
        }
        
        //    private func formateredNumber(with number: Int?) -> String {
        //        let numberFormatter = NumberFormatter()
        //        numberFormatter.groupingSeparator = ","
        //        numberFormatter.numberStyle = .decimal
        //        return numberFormatter.string(for: number) ?? "-"
        //    }
        
        private func remainTimeToExpire(sec: Double) -> String {
            return "\(LanguageHelper.localizedString("expires_in")) \(self.convertRemainTime(from: sec))"
        }
        
        private func convertRemainTime(from seconds: Double) -> String {
            
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.year, .month, .day, .hour, .minute]
            var formatteredString: String? = ""
            formatter.unitsStyle = .full
            formatteredString = formatter.string(from: TimeInterval(seconds))
            print(formatteredString?.capitalizingFirstLetter() as Any)
            return formatteredString ?? "-"
        }
    }

    extension String {
        func capitalizingFirstLetter() -> String {
            return prefix(1).uppercased() + self.lowercased().dropFirst()
        }
        
        mutating func capitalizeFirstLetter() {
            self = self.capitalizingFirstLetter()
        }
    }

