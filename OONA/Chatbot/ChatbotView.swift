//
//  ChatbotView.swift
//  OONA
//
//  Created by Jack on 30/7/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import GhostTypewriter
import RxSwift
import RxCocoa
import SwiftyJSON
enum Gender {
    case Male
    case Female
    case Other
}
enum Question {
    case TcoinTutorialSpend
    case Tcoin2000Earned
    case ContactInfo
    case ignoreAd
    case threeTimeGame
    
}
class ChatbotView: UIView {
    
    
    var name :String = "Jack"
    var gender : Gender = .Male
    public var currentQuestion : Question = .TcoinTutorialSpend
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBAction func dismissAction(_ sender: Any) {
        
        if isDismissable {
        
            dismiss()
        }
        
    }
    
    
    @IBOutlet weak var q1_icon: UIImageView!
    @IBOutlet weak var q2_icon: UIImageView!
    @IBOutlet weak var q3_icon: UIImageView!
    @IBOutlet weak var q4_icon: UIImageView!
    
    // Questions
    @IBOutlet weak var q1_1View: UIView!
    @IBOutlet weak var q1_1lb: UILabel!
    
    @IBOutlet weak var q1_2View: UIView!
    @IBOutlet weak var q1_2Lb: UILabel!
    
    
    @IBOutlet weak var q1_3View: UIView!
    @IBOutlet weak var q1_3Lb: UILabel!
    
    
    @IBOutlet weak var a1View: UIView!
    @IBOutlet weak var a1Lb: UILabel!
    
    @IBOutlet weak var q2_1View: UIView!
    @IBOutlet weak var q2_1Lb: UILabel!
    
    @IBOutlet weak var q2_2View: UIView!
    @IBOutlet weak var q2_2Lb: UILabel!
    
    @IBOutlet weak var q2_3View: UIView!
    @IBOutlet weak var q2_3Lb: UILabel!
    
    @IBOutlet weak var q2_4View: UIView!
    @IBOutlet weak var q2_4Lb: UILabel!
    
    @IBOutlet weak var q2_5View: UIView!
    @IBOutlet weak var q2_5Lb: UILabel!
    
    @IBOutlet weak var q2_6View: UIView!
    @IBOutlet weak var q2_6Lb: UILabel!
    
    @IBOutlet weak var q2_7View: UIView!
    @IBOutlet weak var q2_7Lb: UILabel!
    
    
    @IBOutlet weak var a2View: UIView!
    @IBOutlet weak var a2Lb: UILabel!
    
    @IBOutlet weak var q3_1View: UIView!
    @IBOutlet weak var q3_1Lb: UILabel!
    
    @IBOutlet weak var q3_2View: UIView!
    @IBOutlet weak var q3_2Lb: UILabel!
    
    
    
    
    
    @IBOutlet weak var a3View: UIView!
    @IBOutlet weak var a3Lb: UILabel!
    
    @IBOutlet weak var q4View: UIView!
    @IBOutlet weak var q4Lb: UILabel!
    
    @IBOutlet weak var a4View: UIView!
    @IBOutlet weak var a4Lb: UILabel!
    
    
    @IBOutlet weak var A1AnsView: UIView!
    @IBOutlet weak var a1AnsTextField: UITextField!
    @IBOutlet weak var A1AnsSubmitButton: UIButton!
    
    @IBAction func A1AnsSubmitAction(_ sender: Any) {
        self.endEditing(true)
        self.name = a1AnsTextField.text ?? ""
        A1AnsView.isHidden = true
        a1Lb.text = name
        a1View.isHidden = false
        
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ2()
        case .TcoinTutorialSpend:
            self.goTcoinQ2()
        case .Tcoin2000Earned:
            self.goTcoinQ2()
            
        default:
            dismiss()
        }
    }
    
    @IBOutlet weak var q2TopConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var A2AnsView: UIView!
    
    
    
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBOutlet weak var q2_2_tcoinView: UIView!
    
    
    @IBOutlet weak var tcoinTutorial50RewardAnsView: UIView!
    @IBOutlet weak var tcoinTutorialQ1AnsView: UIView!
    @IBOutlet weak var tcoinTutorialQ2AnsView: UIView!
    @IBOutlet weak var tcoinTutorialDismissAnsView: UIView!
    @IBOutlet weak var tcoinTutorial50RewardButton: UIButton!
    
    @IBOutlet weak var tcoinTutorialDismissButton: UIButton!
    
    @IBOutlet weak var TTQ1YesButton: UIButton!
    @IBOutlet weak var TTQ1NoButton: UIButton!
    
    
    @IBOutlet weak var TTQ2YesButton: UIButton!
    @IBOutlet weak var TTQ2NoButton: UIButton!
    
    @IBAction func tcoin50RewardAction(_ sender: Any) {
        
    }
    
    @IBAction func tcoinTutorialQ1YesAction(_ sender: Any) {
//        a1Lb.text = "Yes"
//        a1View.isHidden = false
//
//        TTRewardQ1Ans = true
//        tcoinTutorialSecondStageFinished()
        dismiss()
        
//        goTcoinQ2()
//        tcoinTutorialQ1AnsView.isHidden = true
    }
    
    @IBAction func tcoinTutorialQ1NoAction(_ sender: Any) {
//        tcoinTutorialFirstStageFinished()
//        a1Lb.text = "Oh! Tell me how"
//        a1View.isHidden = false
//
//        TTRewardQ1Ans = false
//        goTcoinQ2()
//        tcoinTutorialQ1AnsView.isHidden = true
        dismiss()
    }
    
    @IBAction func tcoinTutorialQ2YesAction(_ sender: Any) {
        // play video

        PlayerHelper.shared.playTutorial.accept("2")
        dismiss()
    }
    
    @IBAction func tcoinTutorialQ2NoAction(_ sender: Any) {
        // call tut api id = 2
        dismiss()
        self.requestQ2Reward(tutorialResponse: false)
        tcoinTutorialSecondStageFinished()
    }
    
    
    @IBAction func tcoinTutorialDismissOkayAction(_ sender: Any) {
        dismiss()
        
    }
    var TTRewardQ1Ans : Bool = false
    var TT2000RewardQ2Ans : Bool = false
    
    @IBAction func A2MaleAction(_ sender: Any) {
       A2AnsView.isHidden = true
        gender =  .Male
        a2Lb.text = LanguageHelper.localizedString("male")
        a2View.isHidden = false
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ3()
        case .TcoinTutorialSpend:
            self.goTcoinQ3()
        case .Tcoin2000Earned:
            print("gg")
        default:
            print("gg")
        }
        
    }
    
    @IBAction func A2FemaleAction(_ sender: Any) {
        A2AnsView.isHidden = true
        gender =  .Female
        a2Lb.text = "Female"
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ3()
        case .TcoinTutorialSpend:
            self.goTcoinQ3()
        case .Tcoin2000Earned:
            print("gg")
        default:
            print("gg")
        }
    }
    
    @IBAction func A2OtherAction(_ sender: Any) {
        A2AnsView.isHidden = true
        gender =  .Other
        a2Lb.text = "Other"
        a2View.isHidden = false
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ3()
        case .TcoinTutorialSpend:
            self.goTcoinQ3()
        case .Tcoin2000Earned:
            print("gg")
        default:
            print("gg")
        }
    }
    
    
    let disposeBag = DisposeBag()
    @IBOutlet weak var A3AnsView: UIView!
    @IBOutlet weak var A3AnsTextField: UITextField!
    @IBAction func A3AnsSubmitButton(_ sender: Any) {
        self.endEditing(true)
        a3Lb.text = A3AnsTextField.text
        a3View.isHidden = false
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ4()
        case .TcoinTutorialSpend:
//            self.goTcoinQ3()
            dismiss()
        case .Tcoin2000Earned:
            print("gg")
        default:
            print("gg")
        }
        
        A3AnsView.isHidden = true
    }
    
    @IBOutlet weak var A4AnsView: UIView!
    @IBAction func A4DoneButton(_ sender: Any) {
        dismiss()
    }
    
    @IBOutlet weak var A4DoneButton: UIButton!
    
    
    @IBOutlet weak var tcoin2000Q1AnsView: UIView!
    @IBOutlet weak var tcoin2000Q2AnsView: UIView!
    @IBOutlet weak var tcoin2000Q3AnsView: UIView!
    @IBOutlet weak var tcoin2000Q4AnsView: UIView!
    
    @IBOutlet weak var a2TopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tcoin2000Q1PositiveButton: UIButton!
    @IBOutlet weak var tcoin2000Q1NegativeButton: UIButton!
    
    @IBAction func tcoin2000Q1PositiveAction(_ sender: Any) {
        a1Lb.text = "HOW? SHOW ME"
        a1View.isHidden = false
        tcoin2000Q1AnsView.isHidden = true
        TT2000RewardQ2Ans = true
        
        goTcoinQ2()
        
     
        
        // play video and get 500 coin, then dismiss
    }
    @IBAction func tcoin2000Q1NegativeAction(_ sender: Any) {
        a1Lb.text = "Later"
        a1View.isHidden = false
        TT2000RewardQ2Ans = false
        tcoin2000Q1AnsView.isHidden = true
        goTcoinQ2()
    }
    
    @IBOutlet weak var tcoin2000Q2NegativeButton: UIButton!
    @IBOutlet weak var tcoin2000Q2PositiveButton: UIButton!
    
    @IBAction func tcoin2000Q2PositiveAction(_ sender: Any) {
            // play video and get 500 tcoin
            // then go q4
        
        PlayerHelper.shared.playTutorial.accept("3")
        
        dismiss()
//        a2Lb.text = "See the video"
//        a2View.isHidden = false
//
//        tcoin2000Q2AnsView.isHidden = true
//        a2TopConstraint.constant = -175
////        a2TopConstraint = NSLayoutConstraint (item: a2View,
////                                                 attribute: .top,
////                                                 relatedBy: .equal,
////                                                 toItem: q2_2View,
////                                                 attribute: .bottom,
////                                                 multiplier: 1,
////                                                 constant: 5)
//        self.layoutIfNeeded()
//        TT2000RewardQ2Ans = true
//        goTcoinQ3()
    }
    @IBAction func tcoin2000Q2NegativeAction(_ sender: Any) {
        dismiss()
//        a2Lb.text = "Sponsor now"
//        a2View.isHidden = false
//        a2TopConstraint.constant = -175
//        tcoin2000Q2AnsView.isHidden = true
//
//        TT2000RewardQ2Ans = false
//        goTcoinQ3()
    }
    
    @IBOutlet weak var tcoin2000Q3PositiveButton: UIButton!
    @IBOutlet weak var tcoin2000Q3NegativeButton: UIButton!
    
    @IBAction func tcoin2000Q3PositiveAction(_ sender: Any) {
        // INVITE NOW
//        dismiss()
//        goToSponsor()
        PlayerHelper.shared.playTutorial.accept("3")
        
        dismiss()
//        PlayerHelper.shared.present
    }
    @IBAction func tcoin2000Q3NegativeAction(_ sender: Any) {
        dismiss()
    }
    
    @IBOutlet weak var tcoin2000Q4PositiveButton: UIButton!
    @IBOutlet weak var tcoin2000Q4NegativeButton: UIButton!
    
    @IBAction func tcoin2000Q4PositiveAction(_ sender: Any) {
        // INVITE NOW
//        goToSponsor()
        if (currentQuestion == .ignoreAd) {
            PlayerHelper.shared.playTutorial.accept("2")
        }else{
            PlayerHelper.shared.playTutorial.accept("1")
            tcoinTutorialSecondStageFinished()
            
        }
        dismiss()
    }
    
    @IBAction func tcoin2000Q4NegativeAction(_ sender: Any) {
//        dismiss()
        if (currentQuestion == .ignoreAd) {
            PlayerHelper.shared.playTutorial.accept("2")
        }else{
            PlayerHelper.shared.playTutorial.accept("1")
            tcoinTutorialSecondStageFinished()
            
        }
        dismiss()
    }
    
    func goToQ1() {
        q1_icon.isHidden = false
        q1_1View.isHidden = false
        self.animateLabel(label: q1_1lb)
    }
    func goToQ2() {
        q2_icon.isHidden = false
        q2_1View.isHidden = false
        self.animateLabel(label:q2_1Lb)
    }
    func goToQ3() {
        q3_icon.isHidden = false
        q3_1View.isHidden = false
        self.animateLabel(label:q3_1Lb)
    }
    func goToQ4 () {
        q4_icon.isHidden = false
        q4View.isHidden = false
        self.animateLabel(label:q4Lb)
    }

    func goTcoinQ1() {
        q1_icon.isHidden = false
        q1_1View.isHidden = false
        self.animateLabel(label: q1_1lb)
    }
    func goTcoinQ2() {
        
        
//        requestQ1Reward(tutorialResponse: TTRewardQ1Ans)
        q2_icon.isHidden = false
        q2_1View.isHidden = false
//        self.scrollView.scrollRectToVisible(<#T##rect: CGRect##CGRect#>, animated: <#T##Bool#>)

        
        
        self.animateLabel(label:q2_1Lb)
    }
    func goTcoinQ3() {
        q3_icon.isHidden = false
        q3_1View.isHidden = false
        self.animateLabel(label:q3_1Lb)
    }
    func goTcoinQ4 () {
        q4_icon.isHidden = false
        q4View.isHidden = false
        self.animateLabel(label:q4Lb)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUIs()
//        currentQuestion = .TcoinTutorial
        
        
    }
    public func startChatbot() {
        self.initContents()
    }
    func setupUIs() {
        self.stylingQuestionViews(view:q1_1View)
        self.stylingQuestionViews(view:q1_2View)
        self.stylingQuestionViews(view:q1_3View)
        self.stylingQuestionViews(view:q2_1View)
        self.stylingQuestionViews(view:q2_2View)
        self.stylingQuestionViews(view:q2_3View)
        self.stylingQuestionViews(view:q2_4View)
        self.stylingQuestionViews(view:q2_5View)
        self.stylingQuestionViews(view:q2_6View)
        self.stylingQuestionViews(view:q2_7View)
        self.stylingQuestionViews(view:q3_1View)
        self.stylingQuestionViews(view:q3_2View)
        self.stylingQuestionViews(view:q2_2_tcoinView)
        self.stylingQuestionViews(view:q4View)
        
        self.stylingAnswerViews(view: a1View)
        self.stylingAnswerViews(view: a2View)
        self.stylingAnswerViews(view: a3View)
        self.stylingAnswerViews(view: a4View)
        
        self.stylingAnswerButton(button: tcoinTutorial50RewardButton)
        self.stylingAnswerButton(button: tcoinTutorialDismissButton)
        self.stylingAnswerButton(button: TTQ1YesButton)
        self.stylingFocusAnswerButton(button: TTQ1NoButton)
        self.stylingFocusAnswerButton(button: TTQ2YesButton)
        self.stylingAnswerButton(button: TTQ2NoButton)
        
        self.stylingFocusAnswerButton(button: tcoin2000Q1PositiveButton)
        self.stylingAnswerButton(button: tcoin2000Q1NegativeButton)
        
        self.stylingFocusAnswerButton(button: tcoin2000Q2PositiveButton)
        self.stylingAnswerButton(button: tcoin2000Q2NegativeButton)
        
        self.stylingFocusAnswerButton(button: tcoin2000Q3PositiveButton)
        self.stylingAnswerButton(button: tcoin2000Q3NegativeButton)
        
        self.stylingFocusAnswerButton(button: tcoin2000Q4PositiveButton)
        self.stylingAnswerButton(button: tcoin2000Q4NegativeButton)
        
        
        A1AnsView.isHidden = true
        A2AnsView.isHidden = true
        A3AnsView.isHidden = true
        A4AnsView.isHidden = true
        
        tcoinTutorialQ1AnsView.isHidden = true
        tcoinTutorialQ2AnsView.isHidden = true
        tcoinTutorialDismissAnsView.isHidden = true
        tcoinTutorial50RewardAnsView.isHidden = true
        
        tcoin2000Q1AnsView.isHidden = true
        tcoin2000Q2AnsView.isHidden = true
        tcoin2000Q3AnsView.isHidden = true
        tcoin2000Q4AnsView.isHidden = true
        
        
        q1_icon.isHidden = true
        q2_icon.isHidden = true
        q3_icon.isHidden = true
        q4_icon.isHidden = true
        
        maleButton.backgroundColor = Color.redOONAColor()
        femaleButton.backgroundColor = Color.redOONAColor()
        otherButton.backgroundColor = Color.redOONAColor()
        A4DoneButton.backgroundColor = Color.redOONAColor()
        maleButton.layer.cornerRadius = maleButton.frame.size.height / 2
        femaleButton.layer.cornerRadius = femaleButton.frame.size.height / 2
        otherButton.layer.cornerRadius = otherButton.frame.size.height / 2
        A4DoneButton.layer.cornerRadius = A4DoneButton.frame.size.height / 2
        
        
        let datePicker = UIDatePicker()
        
        
 
        
        
        var yearComponent = DateComponents()
        yearComponent.year = -13
        
        let theCalendar = Calendar.current
        let prevDate = theCalendar.date(byAdding: yearComponent, to: Date(),wrappingComponents: false)
        
        datePicker.date = prevDate!
        datePicker.datePickerMode = .date
        
        let maxDateString = "Jan 01, 1900"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let theMinimumDate = dateFormatter.date(from: maxDateString)

        datePicker.maximumDate = Date.init()
        datePicker.minimumDate = theMinimumDate
        
        datePicker.rx.value.changed
            .subscribe({ (date) in
                self.A3AnsTextField.text = self.getformattedDate(datePicker.date)
        }).disposed(by: disposeBag)
        
        A3AnsTextField.inputView = datePicker
      
        
        
    }
    
    func getformattedDate(_ date: Date?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var formattedDate: String? = nil
        if let date = date {
            formattedDate = dateFormatter.string(from: date)
        }
        return formattedDate
    }

    func stylingQuestionViews(view:UIView) {
        view.isHidden = true
        view.layer.cornerRadius = 15.0
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
        
//        kCALayerMaxXMaxYCorner  |kCALayerMinXMinYCorner | kCALayerMinXMaxYCorner;
    }
    func stylingFocusAnswerButton(button:UIButton) {
        button.backgroundColor = Color.redOONAColor()
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = button.frame.size.height / 2
    }
    func stylingAnswerButton(button:UIButton) {
        button.backgroundColor = UIColor.white
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = button.frame.size.height / 2
    }
    func stylingAnswerViews(view:UIView) {
        view.isHidden = true
        view.layer.cornerRadius = 15.0
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner]
        view.backgroundColor = Color.redOONAColor()
        //        kCALayerMaxXMaxYCorner  |kCALayerMinXMinYCorner | kCALayerMinXMaxYCorner;
    }
    var isDismissable : Bool = true
    func initContents() {
//        self.animateLabel(label: q2_2Lb)
        switch currentQuestion {
        case .ContactInfo:
            self.goToQ1()
            isDismissable = true
        case .TcoinTutorialSpend:
            isDismissable = false
            if UserDefaults.standard.integer(forKey: "game_tutorial_stage") == 0 {
                self.goTcoinQ1()
//                self.tcoin2000Q4PositiveButton.setTitle(LanguageHelper.localizedString("play_game_three_times_answer_1"), for: .normal)
//                self.tcoin2000Q4NegativeButton.setTitle(LanguageHelper.localizedString("play_game_three_times_answer_2"), for: .normal)
            }
            if UserDefaults.standard.integer(forKey: "game_tutorial_stage") == 1 {
                q2TopConstraints.constant = -80
                self.layoutIfNeeded()
                self.goTcoinQ2()
            }
            if UserDefaults.standard.integer(forKey: "game_tutorial_stage") > 1 {
                self.dismiss()
            }
        case .ignoreAd:
            isDismissable = false
            self.goTcoinQ1()
//            self.tcoin2000Q4PositiveButton.setTitle(LanguageHelper.localizedString("ignore_ad_answer_1"), for: .normal)
//            self.tcoin2000Q4NegativeButton.setTitle(LanguageHelper.localizedString("ignore_ad_answer_2"), for: .normal)
        case .Tcoin2000Earned:
            isDismissable = false
            self.goTcoinQ1()
            
        default:
            dismiss()
        }
        

        
    }
    func tcoinTutorialFirstStageFinished() {
        UserDefaults.standard.set(1, forKey: "game_tutorial_stage")
    }
    func tcoinTutorialSecondStageFinished() {
        UserDefaults.standard.set(2, forKey: "game_tutorial_stage")
    }
    func animateFinished(label:UILabel) {
        
        switch currentQuestion {
        case .ContactInfo:
            self.contactInfoLabelAnimatedFinished(label: label)
        case .TcoinTutorialSpend:
            self.tcoinTutorialLabelAnimatedFinished(label: label)
        case .Tcoin2000Earned:
//            print("gg")
            self.tcoin2000RewardLabelAnimatedFinished(label:label)
        case .ignoreAd:
            self.adIgnoreQuestionAnimateFinished(label:label)
        case .threeTimeGame:
            self.adIgnoreQuestionAnimateFinished(label:label)
        default:
            print("gg")
        }
            
    }
    func tcoinTutorialLabelAnimatedFinished(label:UILabel) {
        switch label {
        case q1_1lb:
            q1_2View.isHidden = false
            self.animateLabel(label:q1_2Lb)
        case q1_2Lb:
            // go to q1 ans UI
            self.tcoin2000Q4PositiveButton.setTitle(LanguageHelper.localizedString("play_game_three_times_answer_1"), for: .normal)
            self.tcoin2000Q4NegativeButton.setTitle(LanguageHelper.localizedString("play_game_three_times_answer_2"), for: .normal)
            tcoin2000Q4AnsView.isHidden = false
            
//        case q2_1Lb:
//            q2_2View.isHidden = false
//            self.animateLabel(label:q2_2Lb)
//        case q2_2Lb:
//            if (TTRewardQ1Ans){
////                dismiss()
//                q2_2_tcoinView.isHidden = false
//                self.scrollView.scrollRectToVisible(q2_2_tcoinView.frame, animated: true)
//                tcoinTutorialDismissAnsView.isHidden = false
//            }else{
//                q2_3View.isHidden = false
//                self.animateLabel(label:q2_3Lb)
//            }
//        case q2_3Lb:
//            q2_4View.isHidden = false
//            self.animateLabel(label:q2_4Lb)
//        case q2_4Lb:
//            q2_5View.isHidden = false
//            self.animateLabel(label:q2_5Lb)
//        case q2_5Lb:
//            q2_6View.isHidden = false
////            q2_2_tcoinView.isHidden = false
//            self.scrollView.scrollRectToVisible(q2_6View.frame, animated: true)
//            self.tcoinTutorialLabelAnimatedFinished(label:q2_6Lb)
////            self.A2AnsView.isHidden = false
//        case q2_6Lb:
//            // go to q2 ans UI
////            self.A2AnsView.isHidden = false
//            tcoinTutorialQ2AnsView.isHidden = false
//        case q3_1Lb:
//            q3_2View.isHidden = false
//            self.animateLabel(label:q3_2Lb)
//        case q3_2Lb:
//            tcoinTutorialDismissAnsView.isHidden = false
////            self.A3AnsView.isHidden = false
//        // go to q3 ans UI
        case q4Lb:
            A4AnsView.isHidden = false
        default:
            print("gg")
        }
    }
    func contactInfoLabelAnimatedFinished(label:UILabel) {
        switch label {
        case q1_1lb:
            q1_2View.isHidden = false
            self.animateLabel(label:q1_2Lb)
        case q1_2Lb:
            // go to q1 ans UI
            tcoinTutorial50RewardAnsView.isHidden = false
//            A1AnsView.isHidden = false
            
        case q2_1Lb:
            q2_2View.isHidden = false
            self.animateLabel(label:q2_2Lb)
        case q2_2Lb:
            q2_3View.isHidden = false
            self.animateLabel(label:q2_3Lb)
        case q2_3Lb:
            // go to q2 ans UI
            self.A2AnsView.isHidden = false
            
        case q3_1Lb:
            q3_2View.isHidden = false
            self.animateLabel(label:q3_2Lb)
        case q3_2Lb:
            
            self.A3AnsView.isHidden = false
        // go to q3 ans UI
        case q4Lb:
            A4AnsView.isHidden = false
        default:
            print("gg")
        }
        
    }
    func tcoin2000RewardLabelAnimatedFinished(label:UILabel) {
        switch label {
        case q1_1lb:
            q1_2View.isHidden = false
            self.animateLabel(label:q1_2Lb)
        case q1_2Lb:
            // go to q1 ans UI
            self.tcoin2000Q1PositiveButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answer_1"), for: .normal)
            self.tcoin2000Q1NegativeButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answer_2"), for: .normal)
            tcoin2000Q1AnsView.isHidden = false
            
        case q2_1Lb:
//            q2_2View.isHidden = false
//            self.animateLabel(label:q2_2Lb)
            if (TT2000RewardQ2Ans) {
                self.tcoin2000Q2PositiveButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answered_1_answer_1"), for: .normal)
                self.tcoin2000Q2NegativeButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answered_1_answer_2"), for: .normal)
                tcoin2000Q2AnsView.isHidden = false
            }else{
                self.tcoin2000Q3PositiveButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answered_2_answer_1"), for: .normal)
                self.tcoin2000Q3NegativeButton.setTitle(LanguageHelper.localizedString("earn_2000_tcoins_answered_2_answer_2"), for: .normal)
                tcoin2000Q3AnsView.isHidden = false
            }
            

        
      
        default:
            print("gg")
        }
    }
    func adIgnoreQuestionAnimateFinished(label:UILabel) {
        switch label {
        case q1_1lb:
//            q1_2View.isHidden = false
//            self.animateLabel(label:q1_2Lb)
            self.tcoin2000Q4PositiveButton.setTitle(LanguageHelper.localizedString("ignore_ad_answer_1"), for: .normal)
            self.tcoin2000Q4NegativeButton.setTitle(LanguageHelper.localizedString("ignore_ad_answer_2"), for: .normal)
            self.tcoin2000Q4NegativeButton.titleLabel?.adjustsFontSizeToFitWidth = true
            self.tcoin2000Q4NegativeButton.titleLabel?.minimumScaleFactor = 0.5
            tcoin2000Q4AnsView.isHidden = false
//        case q1_2Lb:
            // go to q1 ans UI
            
//            tcoin2000Q4AnsView.isHidden = false
            
     
            
            
        default:
            print("gg")
        }
    }

    func animateLabel(label:UILabel) {
        label.text = ""
        var text = ""
        switch currentQuestion {
        case .ContactInfo:
            text = self.getQuestion(label: label)
        case .TcoinTutorialSpend :
            text = self.playGameThreeTimeQuestion(label: label)
        case .Tcoin2000Earned:
            text = self.earn2000TcoinQuestion(label: label)
        case .ignoreAd:
            text = self.ignoreAdQuestion(label:label)
        case .threeTimeGame:
            text = self.playGameThreeTimeQuestion(label: label)
        default:
            print("gg")
        }
        
        
        // New code using Timer class
        
        let characters = text.map { $0 }
        var index = 0
        
        Timer.scheduledTimer(withTimeInterval: 0.04, repeats: true, block: { [weak self] timer in
            if index < text.count {
                let char = characters[index]
                label.text! += "\(char)"
                index += 1
                
//                self?.scrollView.setContentOffset(CGPoint(x: label.superview!.frame.maxX, y: label.superview!.frame.maxY), animated: true)
                
                self!.scrollView.scrollRectToVisible(label.superview!.frame, animated: false)
                
                print("scroll to",label.frame)
            } else {
                timer.invalidate()
                self?.animateFinished(label: label)
            }
        })
        
        /**
         // Older Code using Run loop to block the UI
         for char in appName {
         animatingLabel.text! += "\(char)"
         RunLoop.current.run(until: Date()+0.12)
         }
         */
        
    }
    func getQuestion(label:UILabel) -> String{
        var questionText : String = ""
        switch label {
        case q1_1lb:
            questionText = "Hi!"
        case q1_2Lb:
            questionText = "What is your name?"
        case q2_1Lb:
            questionText = "Hi \(name)"
        case q2_2Lb:
            questionText = "I work for you now"
        case q2_3Lb:
            questionText = "Are you male or female?"
        case q3_1Lb:
            questionText = "We have free personalized gifts waiting for you."
        case q3_2Lb:
            questionText = "When is your birthday?"
        case q4Lb:
            questionText = "Thank you \(name)! Congratulations for making your OONA app more personalized"
        
        default:
            questionText = ""
        }
        return questionText
    }
    func gameTcoinQuestionQ1(label:UILabel) -> String {
        
//        let ans = "Yes"\"oh! Tell me how"
        var questionText : String = ""
        switch label {
        case q1_1lb:
            questionText = "Wow you played well!"
        case q1_2Lb:
            questionText = "Do you know you can use your tcoins to WIN cash and prizes?"
            
            
        case q2_1Lb:
            if (TTRewardQ1Ans) {
                questionText = "Great! 😃\nHave fun and get more tcoins and good luck for Today Bidding Prize."
            }else{
                 questionText = "All the tcoins you making are for you to WIN cash and prizes."
            }
        case q2_2Lb:
            if (TTRewardQ1Ans) {
                questionText = "Here a small gift for you 😃\nAn extra 50 tcoins"
            }else{
                questionText = "It's simple.\nClick the \"t\"icon in menu,\nUse your tcoins to bid and win today prize."
            }
            
        case q2_3Lb:
            questionText = "The highest bid today win the prize, that's so simple so have fun, get more tcoins and Bid everyday."
        case q2_4Lb:
            questionText = "Good luck!!"
        case q2_5Lb:
            questionText = "And here a small gift for you 😉 \n50 extra bonus tcoins to wish you luck"
            
            // Okay
            
        case q3_1Lb:
            questionText = "I have an extra 250 tcoins BONUS\nIf you watch the short video \"HOW tcoins work\""
            
        case q3_2Lb:
            questionText = "You want to watch now??"
            
            
            
        default:
            questionText = ""
        }
        return questionText
        
        
//        return question
    }
    func earn2000TcoinQuestion(label:UILabel) -> String {
        
        
        var questionText : String = ""
        switch label {
        case q1_1lb:
            questionText = LanguageHelper.localizedString("earn_2000_tcoins_question_line_1")//"Wow, 2000 tcoin already!"
        case q1_2Lb:
            questionText = LanguageHelper.localizedString("earn_2000_tcoins_question_line_2")//"Get your BONUS tcoins from friends"
        case q1_3Lb:
            questionText = LanguageHelper.localizedString("earn_2000_tcoins_question_line_3")//"And win CASH / PRIZES today"
            
        case q2_1Lb:
            if TT2000RewardQ2Ans {
                questionText = LanguageHelper.localizedString("earn_2000_tcoins_answered_1_question")//"Sponsor friends, and get 1000 and 1000 Tcoins everyday"
            }else{
                questionText = LanguageHelper.localizedString("earn_2000_tcoins_answered_2_question")//"OK, I remind you tomorrow"
            }
       
        default:
            questionText = ""
        }
        return questionText
        
        
        //        return question
    }
    func ignoreAdQuestion(label:UILabel) -> String {

        var questionText : String = ""
        switch label {
        case q1_1lb:
            questionText = LanguageHelper.localizedString("ignore_ad_question")//"Hey! Get more tcoin to WIN today"
            
        default:
            questionText = ""
        }
        return questionText
        
        
        //        return question
    }
    func playGameThreeTimeQuestion(label:UILabel) -> String {
        
        
        var questionText : String = ""
        switch label {
        case q1_1lb:
            questionText = LanguageHelper.localizedString("play_game_three_times_toast_line_1")// "Well play!"
        case q1_2Lb:
            questionText = LanguageHelper.localizedString("play_game_three_times_toast_line_2") //"Use your tcoin and win CASH and PRIZES"
        default:
            questionText = ""
        }
        return questionText
        
        
        //        return question
    }
    func requestQ1Reward(tutorialResponse:Bool) {
        let name = "tutorial-watched"
        let tutRes : String = tutorialResponse ? "yes" : "no"
        
        let param = ["tutorialId":"1",
                    "tutorialResponse":tutRes]
//        self.totalTcoinEarned = 0
        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)!
        
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            
                                                        } else {
                                                            
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    
                }
        }
    }
    func requestQ2Reward(tutorialResponse:Bool) {
        let name = "game-rewards"
        let tutRes : String = tutorialResponse ? "yes" : "no"
        
        let param = ["tutorialId":"2",
                     "tutorialResponse":tutRes]
        //        self.totalTcoinEarned = 0
        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)!
        
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            
                                                        } else {
                                                            
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    
                }
        }
    }
    
    func request2000TcoinSponsorRewardQ1(tutorialResponse:Bool) {
        let name = "tutorial-watched"
        let tutRes : String = tutorialResponse ? "yes" : "no"
        
        let param = ["tutorialId":"3",
                     "tutorialResponse":tutRes]
        //        self.totalTcoinEarned = 0
        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)!
        
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            
                                                        } else {
                                                            
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    
                }
        }
    }
    func request2000TcoinSponsorRewardQ2(tutorialResponse:Bool) {
        let name = "tutorial-watched"
        let tutRes : String = tutorialResponse ? "yes" : "no"
        
        let param = ["tutorialId":"4",
                     "tutorialResponse":tutRes]
        //        self.totalTcoinEarned = 0
        let encryptedParam = DeviceHelper.shared.getEncryptedData(param: param)!
        
        OONAApi.shared.postRequest(url: APPURL.requestReward(event: name), parameter: encryptedParam)
            .responseJSON { (response) in
                switch response.result {
                    
                case .success(let value):
                    let json = JSON(value)
                    if let code = json["code"].int,
                        let status = json["status"].string {
                        APIResponseCode.checkResponse(withCode: code.description,
                                                      withStatus: status,
                                                      completion: { (message, isSuccess) in
                                                        if(isSuccess){
                                                            
                                                        } else {
                                                            
                                                        }
                                                        
                        })
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    
                }
        }
    }
    func goToSponsor() {
        let vc : ReferralViewController = UIStoryboard(name: "Referral", bundle: nil).instantiateInitialViewController() as! ReferralViewController
        self.dismiss()
        //            vc.referral()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            
            PlayerHelper.shared.presentVC(vc: vc)
            
        }
    }
    func dismiss() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = CGRect.init(x: -1 * self.frame.size.width, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        }) { (finished) in
            self.removeFromSuperview()
//            PlayerHelper.shared.resetYTView()
            
        }
    }
}
