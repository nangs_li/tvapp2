//
//  EpisodeViewController.swift
//  OONA
//
//  Created by Vick on 11/6/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift
import RxDataSources

class EpisodeViewController: BaseViewController{
    
    struct ChannelTask {
        var id :String
    }
    typealias episodeSelectedBlock = (_ action : CBAction , _ episode :Episode?) ->()
    var completionBlock:episodeSelectedBlock?
    
    var episodeList = [Episode]()
    var channel = Channel.init()
    var offset : Int = 0
    var episode : Episode?
    var loadingEpisodeList : Bool = false
    var episodeItems : Observable = Observable.just([])
    var items : Observable<[Episode]> = Observable.just([])
    
    @IBOutlet weak var nowPlaying: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var cv: UICollectionView! {
        didSet {
            self.cv.rx.itemSelected.subscribe(onNext: { indexPath in
                //set cell active if onclick , otherwise set inactive
                for i in 0...self.episodeList.count {
                    if let cell = self.cv.cellForItem(at: IndexPath(row: i, section: 0)) as? EpisodeCell{
                        if(i == indexPath.row){
                            cell.setActive()
                        }else{
                            cell.setInActive()
                        }
                    }
                }
                
                guard let cb = self.completionBlock else { return }
                let episode = self.episodeList[indexPath.row]
                //set selected item to loacl instance
                self.episode = episode
                cb(.load,episode)
                
            }).disposed(by: self.disposeBag)
            
            self.cv.rx.contentOffset.subscribe(onNext: {scrollingOffset in
                let pxToEnd = self.cv.contentSize.width - scrollingOffset.x - self.cv.frame.width
                //                print(self.cv.contentSize.width,"    ",pxToEnd)
                if (  pxToEnd  <= self.cv.frame.width*0.7){
                    print("need to fetch more episodes")
                    self.loadEpisodeList()
                }
            }).disposed(by: self.disposeBag)
            
        }
    }
    
    
    
    func setChannelCell(channel: Channel)
    {
        DispatchQueue.main.async {
            self.img?.sd_setImage(with: URL.init(string: channel.imageUrl), placeholderImage: UIImage(named: "channel-placeholder-image"))
            self.img?.layer.cornerRadius = APPSetting.defaultCornerRadius;
            self.text.text = channel.name
        }
    }
    
    func loadEpisodeList(){
        
        if loadingEpisodeList {
            print("get ch list alrdy loading")
            return
        }
        self.setChannelCell(channel: self.channel)
        
        if(self.episode==nil){
            if let currentEpisode = EpisodeHelper.shared.currentEpisode{
                self.episode = currentEpisode
            }
        }
        loadingEpisodeList = true
        
        if(self.channel.type == ChannelSetting.channelType.mix.rawValue){
            EpisodeHelper.shared.getSeriesList(channelId: channel.id.description, success: { episodes,liveChannel in
                //self.episodeList = episodes
                self.episodeList.append(contentsOf: episodes)
                
                //print("episode offset " + self.offset.description)
                
                if let _episode : Episode = self.episodeList[0]{
                    if(self.episode==nil){
                        self.episode = _episode
                    }
                    guard let cb = self.completionBlock else { return }
                    cb(.select, _episode)
                }
                
                
                self.appendData(dataToAppend: episodes)
                
//                let items = Observable.just(self.episodeList)
//                items.bind(to: self.cv.rx.items){ (collectionView, row, _episode) in
//                    let indexPath = IndexPath(row: row, section: 0)
//
//                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell",
//                                                           for: indexPath) as! EpisodeCell
//                    cell.configure(with: _episode)
//                    if(_episode.id==self.episode?.id){
//                        cell.setActive()
//                    }else{
//                        cell.setInActive()
//                    }
//                    return cell
//                    }
//                    .disposed(by: self.disposeBag)
            }, failure: { error in })
        }else{
            /**old code**/
            
            self.offset = self.episodeList.count
            print("episode offset " + self.offset.description)
            
            EpisodeHelper.shared.getEpisodeList(open: false, channelId: channel.id.description, selectedEpisodeId: nil, offset: self.offset, success: { response in
                let episodes = response
                //self.episodeList = episodes
                
                self.episodeList.append(contentsOf: episodes)
                
                //print("episode offset 2 " + self.episodeList.count.description)
                if let _episode : Episode = self.episodeList[0]{
                    if(self.offset==0){
                        if(self.episode==nil){
                            self.episode = _episode
                        }
                        guard let cb = self.completionBlock else { return }
                        cb(.load, _episode)
                    }
                    
                }
                
                //print("did fetch more")
                self.appendData(dataToAppend: episodes)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.loadingEpisodeList = false
                }
 
                
            }, failure: { error in })
            
        }
    }
    
    private func appendData(dataToAppend : [Episode]) {
        
        self.cv.dataSource = nil
        self.cv.delegate = nil
        
        let newObserver = Observable.just(dataToAppend)
        items = Observable.combineLatest(items, newObserver) {
            $0+$1
        }
        print("did append more")
        bindData()
    }
    
    
    func bindData(){
        
        items
            .bind(to: self.cv.rx.items){ (collectionView, row, _episode) in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell",
                                                       for: indexPath) as! EpisodeCell
                cell.configure(with: _episode)
                
                if(_episode.id==self.episode?.id){
                    cell.setActive()
                }else{
                    cell.setInActive()
                }
                return cell
            }
            .disposed(by: self.disposeBag)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        EpisodeHelper.shared.changeEpisode.subscribe(onNext: { [weak self] (ep) in
            if let checkedSelf = self {
                checkedSelf.episode = ep
               if let selectedIndex = checkedSelf.episodeList.firstIndex(of: ep) {
                print("Total number of episodes: \(checkedSelf.episodeList.count) Selected Index: \(selectedIndex)")
                for itemIndex in 0...checkedSelf.episodeList.count-1 {
                    
                    /* //TODO:
                     this loop will not go throught all cell in collection as reusuable cells mechanism,
                     current known issue is when item is just out of collection bounds in -1/+1 will not be updated
                    */
                    if let cell = self?.cv.cellForItem(at: IndexPath(item: itemIndex, section: 0) ) as? EpisodeCell {
                        print("Item Index: \(itemIndex)")
                        if cell.episodeObj == ep {
                            cell.setActive()
                            checkedSelf.cv.scrollToItem(at: IndexPath(item: itemIndex, section: 0), at: .right, animated: false)
                            print("Set Cell Active for Item Index: \(itemIndex)")
                        } else {
                            cell.setInActive()
                        }
                    }
                }
                
                }
            }
        }).disposed(by: self.disposeBag)
        
        self.setChannelCell(channel: self.channel)
        
        self.cv.register(UINib(nibName: "EpisodeCell", bundle: nil), forCellWithReuseIdentifier: "EpisodeCell")
        self.loadEpisodeList()
    }
    
    @IBAction func clickNowPlaying(_ sender: UIButton) {
        guard let cb = self.completionBlock else { return }
        cb(.nowPlaying, nil)
    }
    
    override func appLanguageDidUpdate() {
        self.nowPlaying.text = LanguageHelper.localizedString("NOW_PLAYING")
    }
}
