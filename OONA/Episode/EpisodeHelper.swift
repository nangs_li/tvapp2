//
//  EpisodeHelper.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON
import RxCocoa
import RxSwift

class EpisodeHelper: NSObject {
    static let shared = EpisodeHelper()
    
    var NormalChannelMode = true
    var channelId : Int = 0
    var offset: Int  = 0
    var chunkSize : Int = 0
    var currentEpisodeSequence : Int = 0
    var VideoTutorialShowing = false
    var result = [Episode]()
    var currentEpisode : Episode?
    var currentEpisodeList : [Episode]? //use in first load of the app to revisit previous channel
    var currentEpisodeListCache : [[String: Any]] = [[String: Any]]()  //use to cache episodes
    let changeEpisode: PublishRelay<Episode> = PublishRelay<Episode>()
    func getCurrentEpisodeFromStorage()->Episode?{
        
        let currentEpisodeId = UserDefaults().integer(forKey: "currentEpisodeId")
        if(currentEpisodeId==0){ //nil
            return nil
        }else{
            return Episode.init(id: currentEpisodeId)
        }
        
    }
    
    func clearEpisodeFromStorage(){
        //UserDefaults.standard.set(episode.id, forKey:"currentEpisodeId")
        UserDefaults.standard.removeObject(forKey: "currentEpisodeId")
        
    }
    
    func setCurrentEpisode(episode : Episode){
        self.currentEpisode = episode
        self.changeEpisode.accept(episode)
        /*
        self.currentEpisode = episode
        let data  = NSKeyedArchiver.archivedData(withRootObject: self.currentEpisode)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey:"currentEpisode" )*/
        
        UserDefaults.standard.set(episode.id, forKey:"currentEpisodeId")

        
    }
    
    func getEpisode() -> Episode?{
        return currentEpisode
    }
    func updateVideoByStreamingInfo(video:Video, channel : Channel ,episode : Episode , completion: @escaping (_ success: Bool, _ result: Video? , _ error: Error?) -> Void){
        OONAApi.shared.postRequest(url: APPURL.StreamingInfo(channelId: channel.id.description, episodeId:episode.id.description), parameter:[:]).responseJSON{ [weak self](response) in
            if(response.result.isSuccess){
                print("streamingInfo ",response)
                //                if let json = (try? JSON(data: response.data!)) as? JSON {
                if let data = response.data {
                    let json = JSON(data)
                    let code  = json["code"].int
                    let status = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                print("b4 episode helper update video by st info", video.videoName)
                                print("b4 episode helper update video by st info", episode.name)
                                //                                let _video = Video.init(json: json, chType: channel.type, chId: channel.id, epId: 0, _imageUrl: episode.imageUrl)
                                video.update(json:json)
                                video.videoName = episode.name
                                print("aft episode helper update video by st info", video.videoName)
                                completion(true,video, nil)
                            }
                        })
                    }else{
                        
                        completion(false, nil, nil)
                    }
                }else{
                    
                    completion(false,  nil, nil)
                }
            }else{
                
                completion(false,  nil, nil)
            }
        }
    }
    func getVideoByEpisode(channel : Channel ,episode : Episode , completion: @escaping (_ success: Bool, _ result: Video? , _ error: Error?) -> Void){
        OONAApi.shared.postRequest(url: APPURL.StreamingInfo(channelId: channel.id.description, episodeId:episode.id.description), parameter:[:]).responseJSON{ (response) in
            if(response.result.isSuccess){
                
//                if let json = (try? JSON(data: response.data!)) as? JSON {
                if let data = response.data {
                    let json = JSON(data)
                    let code  = json["code"].int
                    let status = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                print("episode helper ep name ",episode.name)
                                let _video = Video.init(json: json, chType: channel.type, chId: channel.id, epId: episode.id, _imageUrl: episode.imageUrl,vdoName: episode.name)
                                
                                completion(true,_video, nil)
                            }
                        })
                    }else{
                        
                        completion(false, nil, nil)
                    }
                }else{
                    
                    completion(false,  nil, nil)
                }
            }else{
                
                completion(false,  nil, nil)
            }
        }
    }
    
    
    func getSeriesList(channelId: String?, success :@escaping ([Episode], LiveChannel) -> (), failure : @escaping ((Any) -> ())){
        let param:[String: String] =  ["channelId":channelId ?? ""]
        OONAApi.shared.postRequest(url: APPURL.Series(channelId: channelId ?? ""), parameter: param).responseJSON{ (response) in
            if(response.result.isSuccess){
                if let value = response.value {
                    let json = JSON(value)
                    let code : Int? = json["code"].int
                    let status : String? = json["status"].string
                    if(code != nil && status != nil){
                        APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                            message , _success in
                            if(_success){
                                let series = JSON(json["series"]).oonaModelArray() as [Episode]
                                var liveChannel = LiveChannel()
                                
                                if let _liveChannel:  NSDictionary = json["liveChannelInfo"].dictionaryObject as NSDictionary? {
                                    //only one live channel
                                    //self.result.append(LiveChannel.init(withDict: liveChannels)
                                    liveChannel = LiveChannel.init(withDict: _liveChannel)
                                }
                                
                                success(series , liveChannel)
                            }
                        })
                    }
                }
            }else{
                //failure()
            }
        }
    }
    
    func getEpisodeList(open: Bool , channelId: String?, selectedEpisodeId: String? , offset:Int?,  success :@escaping ([Episode]) -> (), failure : @escaping ((Any) -> ())){
        //var param:[String: Any] =  ["currentEpisodeId":selectedEpisodeId ?? ""]
        var param:[String: Any] =  [:]
        
        if(open){
            if let _selectedEpisodeId = selectedEpisodeId{
                var episodeDict : [String: Any] =  [:]
                episodeDict["episodeId"] = _selectedEpisodeId.description
                param["open"] = episodeDict
                //print(episodeDict.description)
            }
        }
        
        if let _offset = offset{
            param["offset"] = _offset.description
        }
        
        var paramCache = param //param use to call api , paramCache use to store
        paramCache["channelId"] = channelId
        paramCache["currentEpisodeId"] = selectedEpisodeId
        
        var getResultFromCache = false
        
        //get the episode from cache
        
        for currentEpisodeListCacheParam in currentEpisodeListCache {
            
            if(currentEpisodeListCacheParam["currentEpisodeId"] as? String == paramCache["currentEpisodeId"] as? String && currentEpisodeListCacheParam["open"] as? String == paramCache["open"] as? String && currentEpisodeListCacheParam["channelId"] as? String == paramCache["channelId"] as? String && currentEpisodeListCacheParam["offset"] as? String == paramCache["offset"] as? String ){
                
                print("get cache episode list : currentEpisodeId " + paramCache["currentEpisodeId"].debugDescription + " channelId" + paramCache["channelId"].debugDescription + " offset" + paramCache["offset"].debugDescription + " open" +  paramCache["open"].debugDescription)
                
                if let _channel = currentEpisodeListCacheParam["result"] as? [Episode]{
                    getResultFromCache = true
                    success(_channel)
                }
            }
            
        }
        
        //if the episode not found from cahce, get from api
        
        if(!getResultFromCache){
            
            OONAApi.shared.postRequest(url: APPURL.Episode(channelId: channelId ?? ""), parameter: param).responseJSON{ (response) in
                if(response.result.isSuccess){
                    if let value = response.value {
                        let json = JSON(value)
                        let code : Int? = json["code"].int
                        let status : String? = json["status"].string
                        if(code != nil && status != nil){
                            APIResponseCode.checkResponse(withCode:code?.description , withStatus: status, completion: {
                                message , _success in
                                if(_success){
                                    self.result = JSON(json["episodes"]).oonaModelArray() as [Episode]
                                    self.currentEpisodeListCache.append(paramCache)
                                    self.currentEpisodeList = self.result
                                    success(self.result)
                                }
                            })
                        }
                    }
                }else{
                    //failure()
                }
            }
        }
    }
    
    func getFirstEpisodeListByChannel(channel : Channel, success : @escaping (Episode) -> (), failure : @escaping ((Any) -> ())){
        
        self.currentEpisodeSequence = 0
        
        if(channel.type == ChannelSetting.channelType.mix.rawValue){
            EpisodeHelper.shared.getSeriesList(channelId: channel.id.description, success: { episodes,liveChannel in
                //self.episodeList = episodes
                
                if let _episode : Episode = episodes[0]{
                    //guard let cb = self.completionBlock else { return }
                    success(_episode)
                }
                
            }, failure: { error in failure(error) })
        }else{
            
            EpisodeHelper.shared.getEpisodeList(open: true, channelId: channel.id.description, selectedEpisodeId: nil, offset: 0, success: { response in
                let episodes = response
                //self.episodeList = episodes
                if (episodes.count > 0) {
                    if let _episode : Episode = episodes[0]{
                        success(_episode)
                    }
                }else{
                    failure(0)
                }
            }, failure: { error in  failure(error) })
            
        }
    }
    
    func getPreviousEpisodeListByChannel(channel : Channel, success : @escaping (Episode, Int, [Episode]) -> (), failure : @escaping ((Any) -> ())){
        
        if(channel.type == ChannelSetting.channelType.mix.rawValue){
            EpisodeHelper.shared.getSeriesList(channelId: channel.id.description, success: { episodes, liveChannel in
                //self.episodeList = episodes
                
                var previousIndex: Int? = nil
                
                if let _currentEpisode = self.currentEpisode, let currentIndex = episodes.firstIndex(of: _currentEpisode) {
                    previousIndex = max(0, currentIndex - 1)
                    success(episodes[previousIndex!], previousIndex!, episodes)
                    print("currentIndex: \(currentIndex) previousIndex: \(previousIndex!.description)")
                    return
                }
                
//                episodes.forEach({ (episode) in
//                    print("episode.id"+episode.id.description)
//                    if episode == self.currentEpisode
//                    {
//                        previousIndex = min(currentIndex - 1, max(0, currentIndex - 1))
//                        success(episode, previousIndex!, episodes)
//                        print("currentIndex: \(currentIndex) previousIndex: \(previousIndex!.description)")
//                        return
//                    }
//                    currentIndex += 1
//                })
                
                if (previousIndex == nil) {
                    failure("episode not found")
                }
                
            }, failure: { error in failure(error) })
        }else{
            
            EpisodeHelper.shared.getEpisodeList(open: false, channelId: channel.id.description, selectedEpisodeId: nil, offset: 0, success: { response in
                let episodes = response
                
                
                var previousIndex: Int? = nil
                if let _currentEpisode = self.currentEpisode, let currentIndex = episodes.firstIndex(of: _currentEpisode) {
                    previousIndex = max(0, currentIndex - 1)
                    success(episodes[previousIndex!], previousIndex!, episodes)
                    print("currentIndex: \(currentIndex) previousIndex: \(previousIndex!.description)")
                    return
                }
//                episodes.forEach({ (episode) in
//                    print("episode.id"+episode.id.description)
//                    if episode == self.currentEpisode
//                    {
//                        previousIndex = min(currentIndex - 1, max(0, currentIndex - 1))
//                        success(episode, previousIndex!, episodes)
//                        print("currentIndex: \(currentIndex) previousIndex: \(previousIndex!.description)")
//                        return
//
//                    }
//                    currentIndex += 1
//                })
                
                if (previousIndex == nil) {
                    //alright.. turns out if this stuff doesn't have a current episode id... that make it's the first channel(playing the preview object inside the channel id ) , which make it playing 0 + 1
                    
                    if (episodes.count >= 1) {
                        success(episodes[1], 1, episodes)
                    } else {
                        failure("episode not found")
                    }
                }
                
            }, failure: { error in  failure(error) })
            
        }
    }
    
    func getNextEpisodeListByChannel(channel : Channel, success :@escaping (Episode, Int, [Episode]) -> (), failure : @escaping ((Any) -> ())){
        
        if(channel.type == ChannelSetting.channelType.mix.rawValue){
            EpisodeHelper.shared.getSeriesList(channelId: channel.id.description, success: { episodes,liveChannel in
                //self.episodeList = episodes
                
//                var currentIndex = 0
                var nextIndex: Int? = nil
                
                if let _currentEpisode = self.currentEpisode, let currentIndex = episodes.firstIndex(of: _currentEpisode) {
                    nextIndex = min(episodes.count - 1, currentIndex + 1)
                    success(episodes[nextIndex!], nextIndex!, episodes)
                    print("currentIndex: \(currentIndex) nextIndex: \(nextIndex!.description)")
                    return
                }
                
//                episodes.forEach({ (episode) in
//                    print("episode.id"+episode.id.description)
//                    if episode == self.currentEpisode {
//                        nextIndex = currentIndex + 1
//                        print("currentIndex: \(currentIndex) nextIndex: \(nextIndex!.description)")
//                        if nextIndex! > (episodes.count - 1){
//                            //thats the end of the list..go back to the first one
//                            success(episodes[0])
//                            return
//                        } else {
//                            success(episodes[nextIndex!])
//                            return
//                        }
//                    } else {
//                    }
//                    currentIndex += 1
//                })
                
                if(nextIndex==nil){
                    failure("episode not found")
                }
                
            }, failure: { error in failure(error) })
        }else{
            
            EpisodeHelper.shared.getEpisodeList(open: false, channelId: channel.id.description, selectedEpisodeId: nil, offset: 0, success: { response in
                let episodes = response
                //self.episodeList = episodes
                
//                var currentIndex = 0
                var nextIndex : Int? = nil
                
                if let _currentEpisode = self.currentEpisode, let currentIndex = episodes.firstIndex(of: _currentEpisode) {
                    nextIndex = min(episodes.count - 1, currentIndex + 1)
                    success(episodes[nextIndex!], nextIndex!, episodes)
                    print("currentIndex: \(currentIndex) nextIndex: \(nextIndex!.description)")
                    return
                }

                
//                episodes.forEach({ (episode) in
//                    print("episode.id"+episode.id.description)
//                    if episode == self.currentEpisode {
//                        nextIndex = currentIndex + 1
//                        print("currentIndex: \(currentIndex) nextIndex: \(nextIndex!.description)")
//                        if nextIndex! > (episodes.count - 1){
//                            //thats the end of the list..go back to the first one
//                            success(episodes[0])
//                            return
//                        }else{
//                            success(episodes[nextIndex!])
//                            return
//                        }
//                    } else {
//                    }
//                    currentIndex += 1
//                })
                
                if(nextIndex==nil){
                    //alright.. turns out if this stuff doesn't have a current episode id... that make it's the first channel(playing the preview object inside the channel id ) , which make it playing 0 + 1
                    
                    if (episodes.count >= 1){
                        success(episodes[1], 1, episodes)
                    }else{
                        failure("episode not found")
                    }
                }
                
                
            }, failure: { error in  failure(error) })
            
        }
    }
    
    
    
}

