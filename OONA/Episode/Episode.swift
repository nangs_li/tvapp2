//
//  Episode.swift
//  OONA
//
//  Created by Vick on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//
import Alamofire
import Foundation
import SwiftyJSON

struct Episode: OONAModel, Equatable {
    static func == (lhs: Episode, rhs: Episode) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id : Int = 0
    var name : String = ""
    var description : String = ""
    var length : String = ""
    var no : String = ""
    var isFavourite : Bool = false
    var imageUrl : String = ""
    var url : String = ""
    var isYoutube : Bool = false
    var type : String = ""
    var video : Video?
    
    
    init(){
        
    }
    
    init?(id: Int) {
        //use in first load when we only have stored the id
        self.id = id
    }
    
    init?(json: JSON) {
        if let _id: Int = json["id"].int {
            self.id = _id
        }
        if let _name: String = json["name"].string {
            self.name = _name
        }
        if let _description: String  = json["description"].string {
            self.description = _description
        }
        if let _length: String = json["length"].string {
            self.length = _length
        }
        if let _isFavourite: Bool = json["isFavourite"].bool {
            self.isFavourite = _isFavourite
        }
        if let _imageUrl: String = json["imageUrl"].string {
            self.imageUrl = _imageUrl
        }
        if let _url: String = json["url"].string {
            self.url = _url
        }
        if let _type: String = json["type"].string {
            self.type = _type
        }
        if let _no: String = json["no"].string {
            self.no = _no
        }
        
        if let _video = json["preview"].dictionary {
            var previewBrightCoveId: Int?
            var previewStreamingUrl: String?
            var previewYoutubeId: String?
            var previewImageUrl: String = ""
            var previewChannelName: String = ""
            var previewChannelId: Int = 0
            var previewVideoName : String = ""
            
            var previewVideoType : VideoType = VideoType.vod //default value
            
            
            if let _brightcoveId = _video["brightcoveId"]?.int {
                
                previewBrightCoveId = _brightcoveId
               // previewVideoType = VideoType.vod
            }
            if let _streamingUrl : String  = _video["streamingUrl"]?.string {
                
                previewStreamingUrl = _streamingUrl
               // previewVideoType = VideoType.live
            }
            if let _youtubeId : String  = _video["youtubeId"]?.string {
                
                previewYoutubeId = _youtubeId
                //previewVideoType = VideoType.youtube
            }
            
            previewVideoType = VideoType.init(rawValue: self.type) ?? .vod //default youtube
            
            
            if let _channelId : Int  = _video["channelId"]?.int{
                previewChannelId = _channelId
            }
            
            if let _channelName : String  = _video["channelName"]?.string {
                
                previewChannelName = _channelName
            }
            
            if let _imageUrl : String  = _video["imageUrl"]?.string {
                
                previewImageUrl = _imageUrl
            }
            
            if let _videoName : String  = _video["episodeName"]?.string {
                
                previewVideoName = _videoName
            }
            
            print("episode init video")
            self.video = Video.init(streamingUrl: previewStreamingUrl, brightcoveId: previewBrightCoveId?.description, youtubeId: previewYoutubeId, videoName:previewVideoName, channelName:  previewChannelName, imageUrl: previewImageUrl, videoType: previewVideoType, _channelId: previewChannelId, _episodeId: id)
            
        }
        
    }
    
}
