//
//  EpisodeCell.swift
//  OONA
//
//  Created by Vick on 14/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import SDWebImage

class EpisodeCell: UICollectionViewCell {
    override func awakeFromNib() {
        
    }
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var underline: UIView!
    
    var episodeObj : Episode?
    
    @IBAction func click() {
       // print("clicked")
    }

    func configure(with episode: Episode) {
        self.episodeObj = episode
        img?.layer.cornerRadius = APPSetting.defaultCornerRadius;
        
        if let url = URL.init(string: episode.imageUrl){
            
            self.img?.sd_setImage(with: url, placeholderImage: UIImage(named: "channel-placeholder-image"))
        }
        text.text = episode.name

    }
    func setInActive(){
        self.underline.isHidden = true
    }
    func setActive(){
        self.underline.isHidden = false
    }
}
