//
//  FirebaseHelper.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class FirebaseHelper : NSObject {
    private func logEvent(name:String,parameters:[String: String]) {
        NSLog("[OONA] [Analytics] Firebase log event named: %@ \n parameters : \n %@ \n", name, parameters)
        Analytics.logEvent(name, parameters: parameters)
    }
}
