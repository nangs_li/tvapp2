//
//  APIAnalyticHelper.swift
//  OONA
//
//  Created by Jack on 9/5/2019.
//  Copyright © 2019 OONA. All rights reserved.
//


import Foundation


class APIAnalyticHelper : NSObject {
    private func logEvent(name:String,parameters:[String: String]) {
        NSLog("[OONA] [Analytics] API log event named: %@ \n parameters : \n %@ \n", name, parameters)
//        Analytics.logEvent(name, parameters: parameters)
    }
}
